
VERSION=`----`
CURRENTDATE=`date`

#export VERSION=`----`
#export CURRENTDATE=`date`

echo '**** ⚡️ Wake up bot (CI Builds Frontend Instance) ****'
echo '**** ⚡️ Connect to CanastaRosa.com'
ssh  -T  $credentials "cd /home/ubuntu/frontend-server/CanastaRosa  &&  sudo rm -rf package-lock.json core  && git stash && git fetch origin && git pull origin develop --no-edit && git push origin master && npm i && npm run build && sudo pm2 restart ecosystem.json --env production"
whoami
pwd
echo '**** ⚡️ Done. ****'
echo '**** ⚡️ Visit https://canastarosa.com. ****'
#curl -X POST -H 'Content-type: application/json' --data '{"text":"Version Canasta Rosa ('$VERSION') - Has been deployed."}' https://hooks.slack.com/services/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

