require('dotenv').config();
require('@babel/register');
const info = require('./sys');

//Imprimir informacion current use of Cpu
info.getInfoSystem();

switch (process.env.NODE_ENV) {
  case 'environments':
    module.exports = require('./index.environments.js');
    break;
  case 'docker':
      module.exports = require('./index.devdocker.js');
      break;
  case 'production':
  case 'maintenance':
    module.exports = require('./index.prod.js');
    break;

  default:
    module.exports = require('./index.dev.js');
}
