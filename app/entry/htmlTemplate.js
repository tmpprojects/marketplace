import React from 'react';
import { Helmet } from 'react-helmet';
import { Provider } from 'react-redux';
import serialize from 'serialize-javascript';
import { StaticRouter } from 'react-router-dom';
import { renderRoutes } from 'react-router-config';
import { renderToString, renderToNodeStream } from 'react-dom/server';
import Routes from './routes';
import { facebookPixelID, googleMapsID, googleGTM } from '../config';

export default (req, store, context, manifests) => {
  const NODE_ENV =
    process.env.NODE_ENV === 'production' ? 'production' : 'development';
  const IS_PRODUCTION = NODE_ENV === 'production';
  const fullUrlFirst = `${req.protocol}://${req.get('host')}${req.originalUrl}`;
  const fullUrl = fullUrlFirst.replace('?', '/?');

  // Construct App static HTML
  const markup = renderToString(
    <Provider store={store}>
      <StaticRouter context={context} location={req.url}>
        {renderRoutes(Routes)}
      </StaticRouter>
    </Provider>,
  );

  // HTML 'head' management
  const helmet = Helmet.renderStatic();

  // Get the app 'chunks' that need to be loaded
  // and get server request headers
  const SplitPointsScript = `
		<script>
			window.splitPoints = ${JSON.stringify(context.splitPoints)};
			window.serverSideHeaders = ${JSON.stringify(req.headers)};
		</script>
	`;

  const googleTagManager = `
		<!-- Google Tag Manager -->
		<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
		new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
		j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
		'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
		})(window,document,'script','dataLayer','${googleGTM}');</script>
		<!-- End Google Tag Manager -->
	`;

  const googleTagManagerBody = `
		<!-- Google Tag Manager (noscript) -->
		<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=${googleGTM}"
		height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
		<!-- End Google Tag Manager (noscript) -->
	`;

  const freshChat = `
		<script>
		function initFreshChat() {
			window.fcWidget.init({
			"config": {
				"cssNames": {
					"widget": "custom_fc_frame"
				}
			},
			token: "83a11ad6-e6e7-4cff-934c-7ccb7522e091",
			host: "https://wchat.freshchat.com"
			});
		}
		function initialize(i,t){var e;i.getElementById(t)?initFreshChat():((e=i.createElement("script")).id=t,e.async=!0,e.src="https://wchat.freshchat.com/js/widget.js",e.onload=initFreshChat,i.head.appendChild(e))}function initiateCall(){initialize(document,"freshchat-js-sdk")}window.addEventListener?window.addEventListener("load",initiateCall,!1):window.attachEvent("load",initiateCall,!1);
		function openSupport(){
			window.fcWidget.open();
			window.fcWidget.show();
		}
		</script>
	`;

  // Google Maps API
  const googleMaps = `
		<script
			src="https://maps.googleapis.com/maps/api/js?key=${googleMapsID}&libraries=places"
			async defer
		></script>
	`;

  // Styles
  let mainStyles = [];

  // Try to get css chunks from 'assets manifest'.
  if (manifests.client) {
    const manifest = manifests.client;
    mainStyles = context.splitPoints.reduce((chunkStyles, splitPoint) => {
      return manifest[`${splitPoint}.css`]
        ? [...chunkStyles, manifest[`${splitPoint}.css`]]
        : chunkStyles;
    }, []);
  }

  // Write html <link /> tags based on found css chunks.
  mainStyles = mainStyles.reduce(
    (availableStyles, sectionStyle) =>
      `${availableStyles}
			<link 
				rel="stylesheet" 
				href="${sectionStyle}"
			/>`,
    '',
  );

  //Remove query params in search
  function canonicalURL(viewURL) {
    const resURL = viewURL.split('/?');
    if (resURL.length > 1) {
      return `${resURL[0]}`;
    } else {
      return viewURL;
    }
  }

  // Write response HTML
  return `
		<!doctype html>
		<html ${helmet.htmlAttributes.toString()}>
			<head>
				
				
				<meta charset="utf-8">
				<meta http-equiv="X-UA-Compatible" content="IE=edge" />
				<meta name="viewport" content="width=device-width, initial-scale=1">
				<meta name="theme-color" content="#E63976"/>
				<meta 
					name="google-site-verification" 
					content="YSIHizBLS4LVpKHeW7oIuY_a3FzZ6HO6INal92LRJ3s" 
				/>
				<meta 
					name="google-site-verification" 
					content="1HfDaSwOErxY9TDWh3jscGK677Q1G5LY7mwcnxBE1ec" 
				/>
				<meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
				<meta http-equiv="Pragma" content="no-cache" />
				<meta http-equiv="Expires" content="0" />

				${helmet.title.toString()}
				${helmet.meta.toString()}
				${helmet.script.toString()}
				${helmet.link.toString()}

				<link rel="shortcut icon" href="/favicon.ico" />
				<link rel="icon" type="image/png" href="/favicon.png" />
				<link rel="apple-touch-icon" href="/apple-touch-icon.png" />
				<link rel="canonical" href="${canonicalURL(fullUrl)}"/>
				
				<script src="https://polyfill.io/v2/polyfill.min.js?features=IntersectionObserver"></script>
				${process.env.ENABLE_GOOGLE_ANALYTICS ? googleTagManager : ''}

				<!-- Main Styles -->
				${mainStyles}
				<style>
					.pac-container{
						z-index: 10000!important;
					}
					.custom_fc_frame {left: 10px !important;bottom: 10px !important;}
				</style>
				<link crossorigin href='https://www.canastarosa.com' rel='preconnect' />
				<link crossorigin href='https://www.js.stripe.com' rel='preconnect' />
				<link crossorigin href='https://www.connect.facebook.net' rel='preconnect' />
				<link crossorigin href='https://www.google-analytics.com' rel='preconnect' />
				<link crossorigin href='https://www.googletagmanager.com' rel='preconnect' />
				<link crossorigin href='https://www.secure.mlstatic.com' rel='preconnect' />
				<link crossorigin href='https://www.static.criteo.net' rel='preconnect' />
				<link crossorigin href='https://www.maps.googleapis.com' rel='preconnect' />
				<!-- Google Tag Manager -->
					<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
					new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
					j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
					'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
					})(window,document,'script','dataLayer','GTM-PZTHL2N');</script>
				<!-- End Google Tag Manager -->
			</head>

			<body ${helmet.bodyAttributes.toString()}>
			<!-- Google Tag Manager (noscript) -->
			<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PZTHL2N"
			height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
			<!-- End Google Tag Manager (noscript) -->
			<div>San Pancho - environment</div>

			<!-- Dieta Keto, pan keto cdmx, sugarbearhair mexico, Great Ful, Great ful cbd, amarea obleas, La rosenda, Regalos, flores, regalos únicos, tiendas en México, Superfood  -->

				${IS_PRODUCTION ? googleTagManagerBody : ''}
				
				<div id="app-container">${markup}</div>

				<!-- APP CHUNKS -->
				${SplitPointsScript}
				<!-- /APP CHUNKS -->

				<!-- FRESH CHAT -->
				${freshChat}
				<!-- /FRESH CHAT -->

				<!-- GOOGLE MAPS -->
				${googleMaps}
				<!-- /GOOGLE MAPS -->

				<!-- REDUX STORE -->
				<script>
					window.INIT_STATE = ${serialize(store.getState())}
					window.INIT_NODE = { 
						"context": ${JSON.stringify(context)}
					 }
				</script>
				<!-- /REDUX STORE -->

				<!-- STATIC CONTEXT -->
				<script>
					window.context = ${serialize(context)}
				</script>
				<!-- /STATIC CONTEXT -->
				<!-- APP -->
				<!-- Test -->
				<script src="${
          !manifests.client ? '/client/main.js' : manifests.client['main.js']
        }"></script>
				<!-- /APP restart -->
				<script src='//cdn.freshmarketer.com/718632/1768847.js'></script>
			</body>
		</html>
	`;
};
