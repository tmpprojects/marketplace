import React from 'react';
import { Route, Redirect } from 'react-router-dom';

import {
  App,
  HomePage,
  CategoryPage,
  InterestPage,
  TermsConditionsPage,
  AvisoPrivacidadPage,
  PasswordRecoveryPage,
  AccountActivationPage,
  SearchResultsPage,
  NotFoundPage,
  NotFoundArticle,
  AboutUsPage,
  AboutUs,
  AboutUsSell,
  AboutUsTeam,
  Faqs,
  Jobs,
  StorePage,
  StoreCreatePage,
  StoresListPage,
  ProductPage,
  InspireHomePage,
  InspireSinglePage,
  InspireSearchResultsPage,
  UserProfilePage,
  BasicUserProfile,
  OrderListCustomer,
  OrderDetailCustomer,
  UserReviews,
  ShoppingCartPage,
  // CheckoutShipping,
  // CheckoutPayment,
  CheckoutOrderDetails,
  CheckoutReview,
  CheckoutSuccessPage,
  Cart,
  StoreAppPage,
  StoreSettings,
  StoreFaqsForm,
  StoreAccounts,
  StoreGalleryForm,
  StoreAddressForm,
  StoreSettingsForm,
  StoreSections,
  SectionsForm,
  StoreOrders,
  StoreDashboard,
  StoreMovements,
  OrderListVendor,
  OrderDetailVendor,
  StoreProducts,
  ProductsList,
  ProductEdit,
  ProductDetailsForm,
  ProductStockForm,
  ProductGalleryFormNew,
  ProductShippingForm,
  ValidateImprovements,
  Improvements,
  CRProPage,
  CRProSchool,
  CRProServices,
  CRProWorkshops,
  CRProStore,
  AwardsPage,
  Discover,
  Landing,
  Support,
  MyPaymentsMethods,
  Plans,
  HotSaleLanding,
  HotSaleCategory,
  VisaLanding,
  VisaCategory,
  BazarLanding,
  BazarCategory,
  SearchErrorPage,
  ShippingPoliciesPage,
} from './Bundles';

// Define App Routes
const RedirectToHome = () => <Redirect to="/" />;

const routes = (
  <Route component={App}>
    {/*----- APP -----*/}
    <Route exact path="/" component={HomePage} />
    <Route exact path="/category/:categoryName" component={CategoryPage} />
    <Route exact path="/interest/:interestName" component={InterestPage} />
    <Route exact path="/search" component={SearchResultsPage} />
    <Route exact path="/search/:term" component={SearchResultsPage} />
    <Route path="/search/:section/:term" component={SearchResultsPage} />
    <Route exact path="/error/search" component={SearchErrorPage} />
    <Route exact path="/legales" component={TermsConditionsPage} />
    <Route path="/legales/terminos-condiciones" component={TermsConditionsPage} />
    <Route path="/legales/politicas-de-envio" component={ShippingPoliciesPage} />

    <Route path="/legales/privacidad" component={AvisoPrivacidadPage} />
    <Route path="/account/password-recovery/:key" component={PasswordRecoveryPage} />
    <Route path="/account/success" component={AccountActivationPage} />
    <Route path="/about-us" component={AboutUsPage}>
      <Route exact path="/about-us" component={AboutUs} />
      <Route path="/about-us/team" component={AboutUsTeam} />
      <Route path="/about-us/faqs" component={Faqs} />
      <Route path="/about-us/sell" component={AboutUsSell} />
      <Route path="/about-us/jobs" component={Jobs} />
    </Route>
    {/*----- SHOPPING CART -----*/}
    <Route path="/cart" component={ShoppingCartPage}>
      <Route exact path="/cart" component={Cart} />
    </Route>
    <Route path="/checkout" component={ShoppingCartPage}>
      <Route path="/checkout/shipping-payment" component={CheckoutOrderDetails} />
      {/* <Route path="/checkout/shipping" component={CheckoutShipping} />
      <Route path="/checkout/payment" component={CheckoutPayment} /> */}
      <Route path="/checkout/review" component={CheckoutReview} />
    </Route>
    <Route path="/order-success" component={CheckoutSuccessPage} />
    {/*----- USER PROFILE -----*/}
    <Route path="/users" component={UserProfilePage}>
      <Route exact path="/users" component={BasicUserProfile} />
      <Route path="/users/profile" component={BasicUserProfile} />
      <Route exact path="/users/orders" component={OrderListCustomer} />
      <Route path="/users/orders/:orderID" component={OrderDetailCustomer} />
      <Route path="/users/reviews" component={UserReviews} />
      <Route path="/users/cards" component={MyPaymentsMethods} />
    </Route>
    {/*----- STORE APP -----*/}
    <Route path="/my-store" component={StoreAppPage}>
      <Route exact path="/my-store/improvements" component={Improvements} />

      <Route path="/my-store/settings" component={StoreSettings}>
        <Route exact path="/my-store/settings" component={StoreSettingsForm} />
        <Route path="/my-store/settings/info" component={StoreSettingsForm} />
        <Route path="/my-store/settings/faqs" component={StoreFaqsForm} />
        <Route path="/my-store/settings/payments" component={StoreAccounts} />
        <Route path="/my-store/settings/gallery" component={StoreGalleryForm} />
        <Route path="/my-store/settings/addresses" component={StoreAddressForm} />
      </Route>

      <Route exact path="/my-store/dashboard" component={StoreDashboard} />
      <Route exact path="/my-store/movements" component={StoreMovements} />

      <Route path="/my-store/orders" component={StoreOrders}>
        <Route exact path="/my-store/orders" component={OrderListVendor} />
        {/* <Route exact path="/my-store/orders/:filter" component={OrderListVendor} /> */}
        <Route path="/my-store/orders/:orderID" component={OrderDetailVendor} />
      </Route>

      <Route path="/my-store/sections" component={StoreSections}>
        <Route exact path="/my-store/sections" component={SectionsForm} />
      </Route>

      <Route path="/my-store/products" component={StoreProducts}>
        <Route exact path="/my-store/products" component={ProductsList} />

        <Route path="/my-store/products/edit/:slug" component={ProductEdit}>
          <Route
            exact
            path="/my-store/products/edit/:slug"
            component={ProductDetailsForm}
          />
          <Route
            path="/my-store/products/edit/:slug/stock"
            component={ProductStockForm}
          />
          <Route
            path="/my-store/products/edit/:slug/gallery"
            component={ProductGalleryFormNew}
          />
          <Route
            path="/my-store/products/edit/:slug/shipping"
            component={ProductShippingForm}
          />
        </Route>
      </Route>

      <Route path="/my-store/subscription" component={Plans} />
    </Route>
    {/*----- MARKET -----*/}
    <Route exact path="/stores" component={StoresListPage} />
    <Route path="/stores/create" component={StoreCreatePage} />
    <Route path="/stores/:store/products/:slug" component={ProductPage} />
    <Route path="/stores/:slug" component={StorePage} />
    {/*----- INSPIRE -----*/}
    <Route exact path="/inspire" component={InspireHomePage} />
    <Route path="/inspire/article/:articleSlug" component={InspireSinglePage} />
    <Route path="/inspire/tag/:term" component={InspireSearchResultsPage} />
    {/*----- CANASTA PRO -----*/}
    <Route exact path="/pro" component={CRProPage}>
      <Route path="/pro" component={CRProServices} />
    </Route>
    <Route path="/pro/:section" component={CRProPage}>
      <Route path="/pro/servicios" component={CRProServices} />
      <Route path="/pro/servicios/:service" component={CRProServices} />
      <Route path="/pro/cursos" component={CRProSchool} />
      <Route path="/pro/eventos" component={CRProWorkshops} />
      <Route path="/pro/store" component={CRProStore} />
    </Route>
    {/*----- CANASTA ROSA AWARDS -----*/}
    <Route exact path="/awards" component={AwardsPage} />
    {/* /*------ LANDING --------*/}
    <Route exact path="/landing/:store" component={Landing} />
    {/*----- SEO PAGE -----*/}
    <Route path="/descubre" component={Discover} />
    {/*----- SUPPORT PAGE -----*/}
    <Route path="/soporte" component={Support} />
    {/* /*------ HOT SALE/ANNIVERSARY SALES/BUEN FIN--------*/}
    <Route exact path="/buen-fin" component={HotSaleLanding} />
    <Route exact path="/buen-fin/:categoryName" component={HotSaleCategory} />
    {/* /*------ VISA --------*/}
    {/* <Route exact path="/visa" component={VisaLanding} />
    <Route exact path="/visa/:categoryName" component={VisaCategory} /> */}
    {/* /*------ Bazar Promo --------*/}
    {/* <Route exact path="/bazar/:bazarName" component={BazarLanding} />
    <Route
      exact
      path="/bazar/:bazarName/category/:categoryName"
      component={BazarCategory}
    /> */}
    {/*----- REDIRECT / 404/ ETC... -----*/}
    <Route path="/404" component={NotFoundPage} />
    <Route component={NotFoundPage} />
    <Route component={RedirectToHome} />
  </Route>
);

/**
 * getChildRoutes()
 * A recursive function that parse a list of child routes
 * and construct a descriptive object based on each route.
 * @param {array} childRoutes : An array of routes
 * @return {array} Return an array of route properties
 */
const getChildRoutes = (childRoutes) =>
  React.Children.map(
    childRoutes,
    ({ props: { exact, path, component, children } }) => ({
      exact,
      path,
      component,
      loadData: component.loadData || null,
      routes: children ? getChildRoutes(children) : children,
    }),
  );

// Define an array of routes
const routesArray = [
  {
    exact: routes.props.exact,
    path: routes.props.path,
    component: routes.props.component,
    loadData: routes.props.component.loadData || null,
    routes: getChildRoutes(routes.props.children),
  },
];

// Export list of routes
export default routesArray;
