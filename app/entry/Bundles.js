import syncComponent from '../scripts/components/hocs/syncComponent';

export const App = syncComponent('App', require('../scripts/App'));
export const HomePage = syncComponent(
  'HomePage',
  require('../scripts/Pages/HomePage'),
);
export const Discover = syncComponent(
  'Discover',
  require('../scripts/Pages/Discover'),
);
export const TermsConditionsPage = syncComponent(
  'TermsConditionsPage',
  require('../scripts/Pages/TermsConditionsPage'),
);
export const ShippingPoliciesPage = syncComponent(
  'TermsConditionsPage',
  require('../scripts/Pages/ShippingPoliciesPage'),
);

export const AvisoPrivacidadPage = syncComponent(
  'AvisoPrivacidadPage',
  require('../scripts/Pages/AvisoPrivacidadPage'),
);
export const PasswordRecoveryPage = syncComponent(
  'PasswordRecoveryPage',
  require('../scripts/Pages/PasswordRecoveryPage'),
);
export const AccountActivationPage = syncComponent(
  'AccountActivationPage',
  require('../scripts/Pages/AccountActivationPage'),
);
export const NotFoundPage = syncComponent(
  'NotFoundPage',
  require('../scripts/Pages/NotFoundPage'),
);
export const NotFoundArticle = syncComponent(
  'NotFoundArticle',
  require('../scripts/Pages/NotFoundArticle'),
);
export const AboutUsPage = syncComponent(
  'AboutUsPage',
  require('../scripts/Pages/AboutUsPage'),
);
export const Improvements = syncComponent(
  'Improvements',
  require('../scripts/Pages/Improvements'),
);
export const ValidateImprovements = syncComponent(
  'Improvements',
  require('../scripts/Pages/ValidateImprovements'),
);

export const AboutUs = syncComponent(
  'AboutUs',
  require('../scripts/components/AboutUs/AboutUs'),
);
export const AboutUsSell = syncComponent(
  'AboutUsSell',
  require('../scripts/components/AboutUs/AboutUsSell'),
);
export const AboutUsTeam = syncComponent(
  'AboutUsTeam',
  require('../scripts/components/AboutUs/AboutUsTeam'),
);
export const Faqs = syncComponent(
  'Faqs',
  require('../scripts/components/AboutUs/Faqs'),
);
export const Jobs = syncComponent(
  'Jobs',
  require('../scripts/components/AboutUs/Jobs'),
);

// Shopping Cart
export const ShoppingCartPage = syncComponent(
  'ShoppingCartPage',
  require('../scripts/Pages/ShoppingCartPage'),
);
export const Cart = syncComponent(
  'Cart',
  require('../scripts/components/ShoppingCart/Cart'),
);
// export const CheckoutShipping = syncComponent(
//     'CheckoutShipping',
//     require('../scripts/components/ShoppingCart/CheckoutShipping')
// );
// export const CheckoutPayment = syncComponent(
//     'CheckoutPayment',
//     require('../scripts/components/ShoppingCart/CheckoutPayment')
// );
export const CheckoutOrderDetails = syncComponent(
  'CheckoutOrderDetails',
  require('../scripts/components/ShoppingCart/CheckoutOrderDetails'),
);
export const CheckoutReview = syncComponent(
  'CheckoutReview',
  require('../scripts/components/ShoppingCart/CheckoutReview'),
);
export const CheckoutSuccessPage = syncComponent(
  'CheckoutSuccessPage',
  require('../scripts/Pages/CheckoutSuccessPage'),
);

// User Profile
export const UserProfilePage = syncComponent(
  'UserProfilePage',
  require('../scripts/Pages/UserProfilePage'),
);
export const BasicUserProfile = syncComponent(
  'BasicUserProfile',
  require('../scripts/components/UserProfile/BasicUserProfile'),
);
export const OrderListCustomer = syncComponent(
  'OrderListCustomer',
  require('../scripts/components/Orders/OrderListCustomer'),
);
export const OrderDetailCustomer = syncComponent(
  'OrderDetailCustomer',
  require('../scripts/components/Orders/OrderDetailCustomer'),
);
export const UserReviews = syncComponent(
  'UserReviews',
  require('../scripts/components/UserProfile/UserReviews'),
);
export const MyPaymentsMethods = syncComponent(
  'MyPaymentsMethods',
  require('../scripts/components/UserProfile/MyPaymentsMethods'),
);

// STORE APP
export const StoreAppPage = syncComponent(
  'StoreAppPage',
  require('../scripts/Pages/StoreAppPage'),
);
export const StoreSettings = syncComponent(
  'StoreSettings',
  require('../scripts/components/StoreApp/StoreSettings'),
);
export const StoreFaqsForm = syncComponent(
  'StoreFaqsForm',
  require('../scripts/components/StoreApp/Store/StoreFaqsForm'),
);
export const StoreAccounts = syncComponent(
  'StoreAccounts',
  require('../scripts/components/StoreApp/Store/StoreAccounts'),
);
export const StoreGalleryForm = syncComponent(
  'StoreGalleryForm',
  require('../scripts/components/StoreApp/Store/StoreGalleryForm'),
);
export const StoreAddressForm = syncComponent(
  'StoreAddressForm',
  require('../scripts/components/StoreApp/Store/StoreAddressForm'),
);
export const StoreSettingsForm = syncComponent(
  'StoreSettingsForm',
  require('../scripts/components/StoreApp/Store/StoreSettingsForm'),
);
export const StoreDashboard = syncComponent(
  'StoreDashboard',
  require('../scripts/components/StoreApp/StoreDashboard'),
);
export const StoreMovements = syncComponent(
  'StoreMovements',
  require('../scripts/components/StoreApp/StoreMovements'),
);
export const StoreOrders = syncComponent(
  'StoreOrders',
  require('../scripts/components/StoreApp/StoreOrders'),
);
export const OrderListVendor = syncComponent(
  'OrderListVendor',
  require('../scripts/components/Orders/OrderListVendor'),
);
export const OrderDetailVendor = syncComponent(
  'OrderDetailVendor',
  require('../scripts/components/Orders/OrderDetailVendor'),
);
export const StoreSections = syncComponent(
  'StoreSections',
  require('../scripts/components/StoreApp/StoreSections'),
);
export const SectionsForm = syncComponent(
  'SectionsForm',
  require('../scripts/components/StoreApp/Sections/SectionsForm'),
);
export const StoreProducts = syncComponent(
  'StoreProducts',
  require('../scripts/components/StoreApp/StoreProducts'),
);
export const ProductsList = syncComponent(
  'ProductsList',
  require('../scripts/components/StoreApp/Products/ProductsList'),
);
export const ProductEdit = syncComponent(
  'ProductEdit',
  require('../scripts/components/StoreApp/Products/ProductEdit'),
);
export const ProductDetailsForm = syncComponent(
  'ProductDetailsForm',
  require('../scripts/components/StoreApp/Products/ProductDetailsForm'),
);
export const ProductStockForm = syncComponent(
  'ProductStockForm',
  require('../scripts/components/StoreApp/Products/ProductStockForm'),
);
export const ProductGalleryFormNew = syncComponent(
  'ProductGalleryFormNew',
  require('../scripts/components/StoreApp/Products/ProductGalleryFormNew'),
);
export const ProductShippingForm = syncComponent(
  'ProductShippingForm',
  require('../scripts/components/StoreApp/Products/ProductShippingForm'),
);
export const Plans = syncComponent(
  'Plans',
  require('../../app/scripts/components/StoreApp/Plans/Plans/Plans'),
);

// Search & Category
export const CategoryPage = syncComponent(
  'CategoryPage',
  require('../scripts/Pages/CategoryPage'),
);

export const InterestPage = syncComponent(
  'InterestPage',
  require('../scripts/Pages/InterestPage'),
);
export const SearchResultsPage = syncComponent(
  'SearchResultsPage',
  require('../scripts/Pages/SearchResultsPage'),
);
export const SearchErrorPage = syncComponent(
  'SearchErrorPage',
  require('../scripts/Pages/SearchErrorPage/SearchErrorPage'),
);

// Stores
export const StoreCreatePage = syncComponent(
  'StoreCreatePage',
  require('../scripts/Pages/StoreCreatePage'),
);
export const StoresListPage = syncComponent(
  'StoresListPage',
  require('../scripts/Pages/StoresListPage'),
);
export const StorePage = syncComponent(
  'StorePage',
  require('../scripts/Pages/StorePage'),
);
export const ProductPage = syncComponent(
  'ProductPage',
  require('../scripts/Pages/ProductPage'),
);

// Inspire
export const InspireHomePage = syncComponent(
  'InspireHomePage',
  require('../scripts/Pages/InspireHomePage'),
);
export const InspireSinglePage = syncComponent(
  'InspireSinglePage',
  require('../scripts/Pages/InspireSinglePage'),
);
export const InspireSearchResultsPage = syncComponent(
  'InspireSearchResultsPage',
  require('../scripts/Pages/InspireSearchResultsPage'),
);

// Canasta Pro
export const CRProPage = syncComponent(
  'CRProPage',
  require('../scripts/Pages/CRProPage'),
);
export const CRProServices = syncComponent(
  'CRProServices',
  require('../scripts/components/CRPro/Services'),
);
export const CRProSchool = syncComponent(
  'CRProSchool',
  require('../scripts/components/CRPro/School'),
);
export const CRProWorkshops = syncComponent(
  'CRProWorkshops',
  require('../scripts/components/CRPro/Workshops'),
);
export const CRProStore = syncComponent(
  'CRProStore',
  require('../scripts/components/CRPro/Store'),
);

// CR AWARDS
export const AwardsPage = syncComponent(
  'AwardsPage',
  require('../scripts/Pages/AwardsPage'),
);

// COLLECTIONS
export const Landing = syncComponent(
  'Landing',
  require('../scripts/Pages/Landing/Landing'),
);

//Support
export const Support = syncComponent(
  'Support',
  require('../scripts/Pages/Support/Support'),
);

// HOT SALE
export const HotSaleLanding = syncComponent(
  'HotSaleLanding',
  require('../scripts/Pages/HotSale/HotSaleLanding'),
);

export const HotSaleCategory = syncComponent(
  'HotSaleCategory',
  require('../scripts/Pages/HotSale/HotSaleCategory'),
);

// HOT SALE
export const VisaLanding = syncComponent(
  'VisaLanding',
  require('../scripts/Pages/Visa/VisaLanding'),
);

export const VisaCategory = syncComponent(
  'VisaCategory',
  require('../scripts/Pages/Visa/VisaCategory'),
);

//BAZAR
export const BazarLanding = syncComponent(
  'BazarLanding',
  require('../scripts/Pages/Bazar/BazarLanding'),
);
export const BazarCategory = syncComponent(
  'BazarCategory',
  require('../scripts/Pages/Bazar/BazarCategory'),
);
