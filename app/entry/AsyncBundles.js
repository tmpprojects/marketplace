import asyncComponent from '../scripts/components/hocs/asyncComponent';

// APP / GENERAL
export const App = asyncComponent(() =>
  import(/* webpackChunkName: "App" */ 'scripts/App'),
);
export const HomePage = asyncComponent(() =>
  import(/* webpackChunkName: "HomePage" */ 'scripts/Pages/HomePage'),
);
export const Discover = asyncComponent(() =>
  import(/* webpackChunkName: "Discover" */ 'scripts/Pages/Discover'),
);

export const CategoryPage = asyncComponent(() =>
  import(/* webpackChunkName: "CategoryPage" */ 'scripts/Pages/CategoryPage'),
);

export const InterestPage = asyncComponent(() =>
  import(/* webpackChunkName: "InterestPage" */ 'scripts/Pages/InterestPage'),
);

export const SearchResultsPage = asyncComponent(() =>
  import(
    /* webpackChunkName: "SearchResultsPage" */ 'scripts/Pages/SearchResultsPage'
  ),
);
export const SearchErrorPage = asyncComponent(() =>
  import(
    /* webpackChunkName: "SearchErrorPage" */ 'scripts/Pages/SearchErrorPage/SearchErrorPage'
  ),
);
export const TermsConditionsPage = asyncComponent(() =>
  import(
    /* webpackChunkName: "TermsConditionsPage" */ 'scripts/Pages/TermsConditionsPage'
  ),
);

export const ShippingPoliciesPage = asyncComponent(() =>
  import(
    /* webpackChunkName: "TermsConditionsPage" */ 'scripts/Pages/ShippingPoliciesPage'
  ),
);
export const AvisoPrivacidadPage = asyncComponent(() =>
  import(
    /* webpackChunkName: "AvisoPrivacidadPage" */ 'scripts/Pages/AvisoPrivacidadPage'
  ),
);
export const PasswordRecoveryPage = asyncComponent(() =>
  import(
    /* webpackChunkName: "PasswordRecoveryPage" */ 'scripts/Pages/PasswordRecoveryPage'
  ),
);
export const AccountActivationPage = asyncComponent(() =>
  import(
    /* webpackChunkName: "AccountActivationPage" */ 'scripts/Pages/AccountActivationPage'
  ),
);
export const NotFoundPage = asyncComponent(() =>
  import(/* webpackChunkName: "NotFoundPage" */ 'scripts/Pages/NotFoundPage'),
);
export const AboutUsPage = asyncComponent(() =>
  import(/* webpackChunkName: "AboutUsPage" */ 'scripts/Pages/AboutUsPage'),
);
export const AboutUs = asyncComponent(() =>
  import(/* webpackChunkName: "AboutUs" */ 'scripts/components/AboutUs/AboutUs'),
);
export const AboutUsSell = asyncComponent(() =>
  import(
    /* webpackChunkName: "AboutUsSell" */ 'scripts/components/AboutUs/AboutUsSell'
  ),
);
export const AboutUsTeam = asyncComponent(() =>
  import(
    /* webpackChunkName: "AboutUsTeam" */ 'scripts/components/AboutUs/AboutUsTeam'
  ),
);
export const Faqs = asyncComponent(() =>
  import(/* webpackChunkName: "Faqs" */ 'scripts/components/AboutUs/Faqs'),
);

export const Jobs = asyncComponent(() =>
  import(/* webpackChunkName: "Jobs" */ 'scripts/components/AboutUs/Jobs'),
);

// SHOPPING CART
export const ShoppingCartPage = asyncComponent(() =>
  import(
    /* webpackChunkName: "ShoppingCartPage" */ 'scripts/Pages/ShoppingCartPage'
  ),
);
export const Cart = asyncComponent(() =>
  import(/* webpackChunkName: "Cart" */ 'scripts/components/ShoppingCart/Cart'),
);
// export const CheckoutShipping = asyncComponent(() =>
//     import ( /* webpackChunkName: "CheckoutShipping" */ 'scripts/components/ShoppingCart/CheckoutShipping')
// );
// export const CheckoutPayment = asyncComponent(() =>
//     import ( /* webpackChunkName: "CheckoutPayment" */ 'scripts/components/ShoppingCart/CheckoutPayment')
// );
export const CheckoutOrderDetails = asyncComponent(() =>
  import(
    /* webpackChunkName: "CheckoutOrderDetails" */ 'scripts/components/ShoppingCart/CheckoutOrderDetails'
  ),
);
export const CheckoutReview = asyncComponent(() =>
  import(
    /* webpackChunkName: "CheckoutReview" */ 'scripts/components/ShoppingCart/CheckoutReview'
  ),
);
export const CheckoutSuccessPage = asyncComponent(() =>
  import(
    /* webpackChunkName: "CheckoutSuccessPage" */ 'scripts/Pages/CheckoutSuccessPage'
  ),
);

// USER PROFILE
export const UserProfilePage = asyncComponent(() =>
  import(/* webpackChunkName: "UserProfilePage" */ 'scripts/Pages/UserProfilePage'),
);
export const BasicUserProfile = asyncComponent(() =>
  import(
    /* webpackChunkName: "BasicUserProfile" */ 'scripts/components/UserProfile/BasicUserProfile'
  ),
);
export const OrderListCustomer = asyncComponent(() =>
  import(
    /* webpackChunkName: "OrderListCustomer" */ 'scripts/components/Orders/OrderListCustomer'
  ),
);
export const OrderDetailCustomer = asyncComponent(() =>
  import(
    /* webpackChunkName: "OrderDetailCustomer" */ 'scripts/components/Orders/OrderDetailCustomer'
  ),
);
export const UserReviews = asyncComponent(() =>
  import(
    /* webpackChunkName: "UserReviews" */ 'scripts/components/UserProfile/UserReviews'
  ),
);
export const MyPaymentsMethods = asyncComponent(() =>
  import(
    /* webpackChunkName: "MyPaymentsMethods" */ 'scripts/components/UserProfile/MyPaymentsMethods'
  ),
);

// STORE APP
export const StoreAppPage = asyncComponent(() =>
  import(/* webpackChunkName: "StoreAppPage" */ 'scripts/Pages/StoreAppPage'),
);
export const StoreSettings = asyncComponent(() =>
  import(
    /* webpackChunkName: "StoreSettings" */ 'scripts/components/StoreApp/StoreSettings'
  ),
);
export const StoreFaqsForm = asyncComponent(() =>
  import(
    /* webpackChunkName: "StoreFaqsForm" */ 'scripts/components/StoreApp/Store/StoreFaqsForm'
  ),
);
export const StoreAccounts = asyncComponent(() =>
  import(
    /* webpackChunkName: "StoreAccounts" */ 'scripts/components/StoreApp/Store/StoreAccounts'
  ),
);
export const StoreGalleryForm = asyncComponent(() =>
  import(
    /* webpackChunkName: "StoreGalleryForm" */ 'scripts/components/StoreApp/Store/StoreGalleryForm'
  ),
);
export const StoreAddressForm = asyncComponent(() =>
  import(
    /* webpackChunkName: "StoreAddressForm" */ 'scripts/components/StoreApp/Store/StoreAddressForm'
  ),
);
export const StoreSettingsForm = asyncComponent(() =>
  import(
    /* webpackChunkName: "StoreSettingsForm" */ 'scripts/components/StoreApp/Store/StoreSettingsForm'
  ),
);
export const StoreDashboard = asyncComponent(() =>
  import(
    /* webpackChunkName: "StoreDashboard" */ 'scripts/components/StoreApp/StoreDashboard'
  ),
);
export const StoreMovements = asyncComponent(() =>
  import(
    /* webpackChunkName: "StoreMovements" */ 'scripts/components/StoreApp/StoreMovements'
  ),
);
export const StoreOrders = asyncComponent(() =>
  import(
    /* webpackChunkName: "StoreOrders" */ 'scripts/components/StoreApp/StoreOrders'
  ),
);
export const OrderListVendor = asyncComponent(() =>
  import(
    /* webpackChunkName: "OrderListVendor" */ 'scripts/components/Orders/OrderListVendor'
  ),
);
export const OrderDetailVendor = asyncComponent(() =>
  import(
    /* webpackChunkName: "OrderDetailVendor" */ 'scripts/components/Orders/OrderDetailVendor'
  ),
);
export const StoreSections = asyncComponent(() =>
  import(
    /* webpackChunkName: "StoreSections" */ 'scripts/components/StoreApp/StoreSections'
  ),
);
export const SectionsForm = asyncComponent(() =>
  import(
    /* webpackChunkName: "SectionsForm" */ 'scripts/components/StoreApp/Sections/SectionsForm'
  ),
);
export const StoreProducts = asyncComponent(() =>
  import(
    /* webpackChunkName: "StoreProducts" */ 'scripts/components/StoreApp/StoreProducts'
  ),
);
export const ProductsList = asyncComponent(() =>
  import(
    /* webpackChunkName: "ProductsList" */ 'scripts/components/StoreApp/Products/ProductsList'
  ),
);
export const ProductEdit = asyncComponent(() =>
  import(
    /* webpackChunkName: "ProductEdit" */ 'scripts/components/StoreApp/Products/ProductEdit'
  ),
);
export const ProductDetailsForm = asyncComponent(() =>
  import(
    /* webpackChunkName: "ProductDetailsForm" */ 'scripts/components/StoreApp/Products/ProductDetailsForm'
  ),
);
export const ProductStockForm = asyncComponent(() =>
  import(
    /* webpackChunkName: "ProductStockForm" */ 'scripts/components/StoreApp/Products/ProductStockForm'
  ),
);
export const ProductGalleryFormNew = asyncComponent(() =>
  import(
    /* webpackChunkName: "ProductGalleryFormNew" */ 'scripts/components/StoreApp/Products/ProductGalleryFormNew'
  ),
);
export const ProductShippingForm = asyncComponent(() =>
  import(
    /* webpackChunkName: "ProductShippingForm" */ 'scripts/components/StoreApp/Products/ProductShippingForm'
  ),
);
export const Plans = asyncComponent(() =>
  import(
    /* webpackChunkName: "Plans" */ 'scripts/components/StoreApp/Plans/Plans/Plans'
  ),
);

// STORES
export const StoreCreatePage = asyncComponent(() =>
  import(/* webpackChunkName: "StoreCreatePage" */ 'scripts/Pages/StoreCreatePage'),
);
export const StoresListPage = asyncComponent(() =>
  import(/* webpackChunkName: "StoresListPage" */ 'scripts/Pages/StoresListPage'),
);
export const StorePage = asyncComponent(() =>
  import(/* webpackChunkName: "StorePage" */ 'scripts/Pages/StorePage'),
);
export const ProductPage = asyncComponent(() =>
  import(/* webpackChunkName: "ProductPage" */ 'scripts/Pages/ProductPage'),
);

export const Improvements = asyncComponent(() =>
  import(/* webpackChunkName: "ProductPage" */ 'scripts/Pages/Improvements'),
);

export const ValidateImprovements = asyncComponent(() =>
  import(/* webpackChunkName: "ProductPage" */ 'scripts/Pages/ValidateImprovements'),
);

// INSPIRE
export const InspireHomePage = asyncComponent(() =>
  import(/* webpackChunkName: "InspireHomePage" */ 'scripts/Pages/InspireHomePage'),
);
export const InspireSinglePage = asyncComponent(() =>
  import(
    /* webpackChunkName: "InspireSinglePage" */ 'scripts/Pages/InspireSinglePage'
  ),
);
export const InspireSearchResultsPage = asyncComponent(() =>
  import(
    /* webpackChunkName: "InspireSearchResultsPage" */ 'scripts/Pages/InspireSearchResultsPage'
  ),
);

//CANASTA PRO

export const CRProPage = asyncComponent(() =>
  import(/* webpackChunkName: "CRProPage" */ 'scripts/Pages/CRProPage'),
);

export const CRProServices = asyncComponent(() =>
  import(
    /* webpackChunkName: "CRProServices" */ 'scripts/components/CRPro/Services'
  ),
);

export const CRProSchool = asyncComponent(() =>
  import(/* webpackChunkName: "CRProSchool" */ 'scripts/components/CRPro/School'),
);

export const CRProWorkshops = asyncComponent(() =>
  import(
    /* webpackChunkName: "CRProWorkshops" */ 'scripts/components/CRPro/Workshops'
  ),
);

export const CRProStore = asyncComponent(() =>
  import(/* webpackChunkName: "CRProStore" */ 'scripts/components/CRPro/Store'),
);

export const AwardsPage = asyncComponent(() =>
  import(/* webpackChunkName: "AwardsPage" */ 'scripts/Pages/AwardsPage'),
);

// COLLECTIONS
export const Landing = asyncComponent(() =>
  import(/* webpackChunkName: "Landing" */ 'scripts/Pages/Landing/Landing'),
);

//SUPPORT
export const Support = asyncComponent(() =>
  import(/* webpackChunkName: "Support" */ 'scripts/Pages/Support/Support'),
);

//HOT SALE
export const HotSaleLanding = asyncComponent(() =>
  import(
    /* webpackChunkName: "HotSaleLanding" */ 'scripts/Pages/HotSale/HotSaleLanding'
  ),
);
export const HotSaleCategory = asyncComponent(() =>
  import(
    /* webpackChunkName: "HotSaleCategory" */ 'scripts/Pages/HotSale/HotSaleCategory'
  ),
);
//VISA
export const VisaLanding = asyncComponent(() =>
  import(/* webpackChunkName: "VisaLanding" */ 'scripts/Pages/Visa/VisaLanding'),
);
export const VisaCategory = asyncComponent(() =>
  import(/* webpackChunkName: "VisaCategory" */ 'scripts/Pages/Visa/VisaCategory'),
);

//BAZAR
export const BazarLanding = asyncComponent(() =>
  import(/* webpackChunkName: "BazarLanding" */ 'scripts/Pages/Bazar/BazarLanding'),
);
export const BazarCategory = asyncComponent(() =>
  import(
    /* webpackChunkName: "BazarCategory" */ 'scripts/Pages/Bazar/BazarCategory'
  ),
);
