import React from 'react';
import moment from 'dayjs';
import 'dayjs/locale/es';
import LocalizedFormat from 'dayjs/plugin/localizedFormat';
import isBetween from 'dayjs/plugin/isBetween';
import thunk from 'redux-thunk';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import * as Sentry from '@sentry/browser';
import { Elements } from '@stripe/react-stripe-js';
import { loadStripe } from '@stripe/stripe-js';
import { BrowserRouter, Route } from 'react-router-dom';
import { renderRoutes } from 'react-router-config';
import { createStore, applyMiddleware, compose } from 'redux';
import LogRocket from 'logrocket';
import setupLogRocketReact from 'logrocket-react';

import config from './config';
import Routes from './entry/routes';
import reducers from './scripts/Reducers';
import * as Bundles from './entry/Bundles';
import axiosInstance from './scripts/Utils/axiosClient';
import {
  initRavenInstance,
  getRavenMiddleware,
} from './scripts/Utils/errorUtils';

const stripePromise = loadStripe(config.stripeAuth.publicKey);

// Init LogRocket (Service for logging bugs and issues)
LogRocket.init('n9mio6/canasta-rosa');
setupLogRocketReact(LogRocket);
LogRocket.getSessionURL((sessionURL) => {
  Sentry.configureScope((scope) => {
    scope.setExtra('sessionURL', sessionURL);
  });
});

// Default time locale for all the application
moment.locale('es');
moment.extend(LocalizedFormat);
moment.extend(isBetween);

// Activate Sentry
const SentryInstance = initRavenInstance();
const sentryMiddleware = getRavenMiddleware(SentryInstance);

// Create Client Side Redux Store
const composeEnhancers =
  process.env.NODE_ENV === 'development' &&
  typeof window !== 'undefined' &&
  window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__
    ? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__
    : compose;

const store = createStore(
  reducers,
  window.INIT_STATE,
  composeEnhancers(
    applyMiddleware(
      thunk.withExtraArgument(axiosInstance),
      LogRocket.reduxMiddleware(),
      sentryMiddleware
    )
  )
);

//
const context = window.context;

// Application
const Application = ({ match }) => (
  <React.Fragment>
    {renderRoutes(Routes, { staticContext: context })}
  </React.Fragment>
);

// Because DOM nodes are already rendered on document,
// use Hydrate instead of Render to 'inject' event-listeners.
const render = function () {
  ReactDOM.hydrate(
    <Provider store={store}>
      <Elements stripe={stripePromise}>
        <BrowserRouter>
          <Route component={Application} />
        </BrowserRouter>
      </Elements>
    </Provider>,
    document.querySelector('#app-container')
  );
};

// Load Bundle Chunks
const splitPoints = window.splitPoints || [];
const componentPromises = splitPoints.map((chunk) =>
  Bundles[chunk].loadComponent()
);
Promise.all(componentPromises.slice(0, 1))
  .then(render)
  .catch((error) => {
    console.log('Error Loading JS Chunk File: ');
    console.log(error);
    Sentry.captureException(error);
  });

// Webpack Hot Module Replacement API
if (module.hot) {
  module.hot.accept(() => {
    const nextRootReducer = require('./scripts/Reducers/index').default;
    store.replaceReducer(nextRootReducer);
    render();
  });
}
