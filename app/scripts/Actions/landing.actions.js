import { landingActionTypes } from '../Constants/landing.constants';
import { LocalRedis } from './localRedis';

const fetchData = (landingStore) => async (dispatch, getState, api) => {
  dispatch({
    type: landingActionTypes.FETCH_DATA_REQUEST,
  });
  let response;
  // console.log(`/api/v1/marketing/landing-page/${landingStore}/`);
  try {
    response = await LocalRedis(
      api,
      `/api/v1/marketing/landing-page/${landingStore}/`,
    );
  } catch (error) {
    dispatch({
      type: landingActionTypes.FETCH_DATA_FAILURE,
      error: true,
      payload: error,
    });
    throw error;
  }
  //sucess
  dispatch({
    type: landingActionTypes.FETCH_DATA_SUCCESS,
    payload: response.data,
  });
  return response;
};
const fetchListData = () => async (dispatch, getState, api) => {
  dispatch({
    type: landingActionTypes.FETCH_LIST_REQUEST,
  });
  let response;
  // console.log(`/api/v1/marketing/landing-page/${landingStore}/`);
  try {
    response = await LocalRedis(
      api,
      '/api/v1/marketing/landing-pages/?frontend=web',
    );
  } catch (error) {
    dispatch({
      type: landingActionTypes.FETCH_LIST_FAILURE,
      error: true,
      payload: error,
    });
    throw error;
  }
  //sucess
  dispatch({
    type: landingActionTypes.FETCH_LIST_SUCCESS,
    payload: response.data,
  });
  return response;
};
export const landingActions = {
  fetchData,
  fetchListData,
};
