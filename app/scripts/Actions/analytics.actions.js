import { analyticsActionTypes } from '../Constants';

const trackListingImpressions = (list, products) => async (
  dispatch,
  getState,
  api,
) => {
  dispatch({
    type: analyticsActionTypes.TRACK_LIST_IMPRESSIONS_REQUEST,
  });

  // API call configuration
  let response;
  const requestOptions = {
    headers: {
      'Content-Type': 'application/json',
    },
  };

  // Make/Handle API call
  try {
    response = await api.post(
      '/api/v1/analytics/log/listing-impression',
      JSON.stringify({
        listing: list,
        products,
      }),
      requestOptions,
    );
  } catch (error) {
    dispatch({
      type: analyticsActionTypes.TRACK_LIST_IMPRESSIONS_FAILURE,
      error: true,
      payload: error,
    });
    throw error;
  }

  // Dispatch Login Success
  dispatch({
    type: analyticsActionTypes.TRACK_LIST_IMPRESSIONS_SUCCESS,
    payload: response.data,
  });
  return response;
};

const trackProductImpression = (slug, queryParams) => async (
  dispatch,
  getState,
  api,
) => {
  dispatch({
    type: analyticsActionTypes.TRACK_PRODUCT_IMPRESSION_REQUEST,
  });

  // API call configuration
  let response;
  const requestOptions = {
    headers: {
      'Content-Type': 'application/json',
    },
  };

  let utm = '';
  utm = queryParams;
  if (queryParams === '') {
    utm = 'none';
  }

  // Make/Handle API call
  try {
    response = await api.post(
      '/api/v1/analytics/log/detail-impression',
      JSON.stringify({
        product: slug,
        query_params: utm,
      }),
      requestOptions,
    );
  } catch (error) {
    dispatch({
      type: analyticsActionTypes.TRACK_PRODUCT_IMPRESSION_FAILURE,
      error: true,
      payload: error,
    });
    throw error;
  }

  // Dispatch Login Success
  dispatch({
    type: analyticsActionTypes.TRACK_PRODUCT_IMPRESSION_SUCCESS,
    payload: response.data,
  });
  return response;
};

const trackProductClick = (list, slug) => async (dispatch, getState, api) => {
  dispatch({
    type: analyticsActionTypes.TRACK_CLICKS_REQUEST,
  });

  // API call configuration
  let response;
  const requestOptions = {
    headers: {
      'Content-Type': 'application/json',
    },
  };

  // Make/Handle API call
  try {
    response = await api.post(
      '/api/v1/analytics/log/product-click',
      JSON.stringify({
        listing: list,
        product: slug,
      }),
      requestOptions,
    );
  } catch (error) {
    dispatch({
      type: analyticsActionTypes.TRACK_CLICKS_FAILURE,
      error: true,
      payload: error,
    });
    throw error;
  }

  // Dispatch Login Success
  dispatch({
    type: analyticsActionTypes.TRACK_CLICKS_SUCCESS,
    payload: response.data,
  });
  return response;
};

/**---------------------------------------
            EXPORT ACTIONS
-----------------------------------------*/
export const analyticsActions = {
  trackListingImpressions,
  trackProductImpression,
  trackProductClick,
};
