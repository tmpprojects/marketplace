//import { BitlyClient } from 'bitly-react';
//const bitly = new BitlyClient('b188e6eaad33b8be4c7dc3b6e7ec2988df46e078', {});
import axios from 'axios';

import { appActionTypes } from '../Constants';
import { LocalRedis, ModifyCache } from './localRedis';
import { getUserZipCode } from '../Reducers/users.reducer';
import { insertKeyToQueryParam } from '../Utils/queryParamsUtils';

/**---------------------------------------
            APP STATE ACTIONS
-----------------------------------------*/
const isMobile = (flag = true) => async (dispatch, getState, api) => {
  // Dispatch Success
  dispatch({
    type: appActionTypes.IS_MOBILE_SUCCESS,
    flag,
  });
};
const setAppSection = (section) => async (dispatch, getState, api) => {
  // Request
  dispatch({
    type: appActionTypes.SET_APP_SECTION,
    section,
  });

  return section;
};

/**---------------------------------------
            'GLOBAL' APP DATA
-----------------------------------------*/
// MARKETPLACE CATEGORIES
const getMarketCategories = () => async (dispatch, getState, api) => {
  // Request
  dispatch({
    type: appActionTypes.GET_MARKET_CATEGORIES_REQUEST,
  });

  // API call configuration
  let response;
  try {
    // response = await api.get('/api/v1/market/categories/');
    response = await LocalRedis(
      api,
      '/api/v1/market/categories/?version=2&active=true',
      'GET_MARKET_CATEGORIES_REQUEST',
    );
  } catch (error) {
    dispatch({
      type: appActionTypes.GET_MARKET_CATEGORIES_FAILURE,
      error: true,
      payload: error,
    });
    throw error;
  }

  // Dispatch Success
  dispatch({
    type: appActionTypes.GET_MARKET_CATEGORIES_SUCCESS,
    categories: response.data,
  });
  return response;
};

//MARKETPLACE INTERESTS
const getMarketInterests = () => async (dispatch, getState, api) => {
  // Request
  dispatch({
    type: appActionTypes.GET_MARKET_INTERESTS_REQUEST,
  });

  // API call configuration
  let response;
  try {
    // response = await api.get('/api/v1/market/interest/');
    response = await LocalRedis(
      api,
      '/api/v1/market/interest/?active=true',
      'GET_MARKET_INTERESTS_REQUEST',
    );
  } catch (error) {
    dispatch({
      type: appActionTypes.GET_MARKET_INTERESTS_FAILURE,
      error: true,
      payload: error,
    });
    throw error;
  }

  // Dispatch Success
  dispatch({
    type: appActionTypes.GET_MARKET_INTERESTS_SUCCESS,
    interests: response.data,
  });
  return response;
};

// MARKETPLACE STORES
const getStoresList = (queryString = '', slug) => async (
  dispatch,
  getState,
  api,
) => {
  // Request
  dispatch({
    type: appActionTypes.STORES_LIST_REQUEST,
  });
  // API call configuration
  let response;
  let queryParams = queryString
    ? queryString
    : '?ordering=-created&page=1&page_size=3&has_cover=true&has_photo=true';
  const zipCode = getUserZipCode(getState());
  queryParams += zipCode ? `&order_zipcode=${zipCode}` : '';

  //
  let apiEndpoint;
  let localRedisKey;
  if (slug === 'envio-nacional') {
    apiEndpoint = `/api/v1/market/stores/${queryParams}&zone=national`;
    localRedisKey = `${queryParams}_zone_national`;
  } else {
    apiEndpoint = `/api/v1/market/stores/${queryParams}`;
    localRedisKey = `${queryParams}`;
  }

  try {
    //response = await api.get(apiEndpoint);
    response = await LocalRedis(
      api,
      apiEndpoint,
      `STORES_LIST_REQUEST___${localRedisKey}`,
    );
  } catch (error) {
    dispatch({
      type: appActionTypes.STORES_LIST_FAILURE,
      error: true,
      payload: error,
    });
    throw error;
  }

  // Dispatch Success
  dispatch({
    type: appActionTypes.STORES_LIST_SUCCESS,
    stores: response.data,
  });
  return response;
};
// PICKUP SCHEDULES
const getPickupSchedules = () => async (dispatch, getState, api) => {
  // Request
  dispatch({
    type: appActionTypes.GET_PICKUP_SCHEDULES_REQUEST,
  });

  // API call configuration
  let response;
  try {
    //response = await api.get('/api/v1/shipping/general-shipping-schedules/');
    response = await LocalRedis(
      api,
      '/api/v1/shipping/general-shipping-schedules/',
      'GET_PICKUP_SCHEDULES_REQUEST',
    );
  } catch (error) {
    dispatch({
      type: appActionTypes.GET_PICKUP_SCHEDULES_FAILURE,
      error: true,
      payload: error,
    });
    throw error;
  }

  // Dispatch Success
  dispatch({
    type: appActionTypes.GET_PICKUP_SCHEDULES_SUCCESS,
    payload: response.data,
  });
  return response;
};
// SHIPPING METHODS
const getShippingMethods = () => async (dispatch, getState, api) => {
  // Request
  dispatch({
    type: appActionTypes.GET_SHIPPING_METHODS_REQUEST,
  });

  // API call configuration
  let response;
  try {
    //response = await api.get('/api/v1/shipping/methods/');
    response = await LocalRedis(
      api,
      '/api/v1/shipping/methods/',
      'GET_SHIPPING_METHODS_REQUEST',
    );
  } catch (error) {
    dispatch({
      //error: error.data,
      type: appActionTypes.GET_SHIPPING_METHODS_FAILURE,
    });
    throw error;
  }

  // Dispatch Success
  dispatch({
    type: appActionTypes.GET_SHIPPING_METHODS_SUCCESS,
    shippingMethods: response.data,
  });
  return response;
};
// PAYMENT METHODS
const getPaymentMethods = () => async (dispatch, getState, api) => {
  // Request
  dispatch({
    type: appActionTypes.GET_PAYMENT_METHODS_REQUEST,
  });

  // API call configuration
  let response;
  try {
    // response = await api.get('/api/v1/payment/methods/');
    response = await LocalRedis(
      api,
      '/api/v1/payment/methods/',
      'GET_PAYMENT_METHODS_REQUEST',
    );
  } catch (error) {
    dispatch({
      error,
      type: appActionTypes.GET_PAYMENT_METHODS_FAILURE,
    });
    throw error;
  }

  // Dispatch Success
  dispatch({
    type: appActionTypes.GET_PAYMENT_METHODS_SUCCESS,
    paymentMethods: response.data,
  });
  return response;
};

/**---------------------------------------
            HOMEPAGE DATA
-----------------------------------------*/
// MAIN BANNERS
const fetchBanners = () => async (dispatch, getState, api) => {
  // Request
  dispatch({
    type: appActionTypes.FETCH_MARKET_BANNERS_REQUEST,
  });

  // API call configuration

  let response;
  try {
    response = await LocalRedis(
      api,
      '/api/v1/featured/market/banner/?frontend=desktop',
      'FETCH_MARKET_BANNERS_REQUEST',
    );

    // if (!response) {

    //   window.localStorage.setItem('FETCH_MARKET_BANNERS_REQUEST', response);
    //   console.log("si hay en local")
    // }else{
    //   console.log("no hay en local")
    // }
  } catch (error) {
    dispatch({
      type: appActionTypes.FETCH_MARKET_BANNERS_FAILURE,
      error: true,
      payload: error,
    });
    throw error;
  }

  // Dispatch Success
  dispatch({
    type: appActionTypes.FETCH_MARKET_BANNERS_SUCCESS,
    banners: response.data,
  });
  return response;
};
// FEATURED PRODUCTS
const getFeaturedProducts = () => async (dispatch, getState, api) => {
  // Request
  dispatch({
    type: appActionTypes.FEATURED_PRODUCTS_REQUEST,
  });

  // API call configuration
  let response;
  try {
    //const zipCode = getUserZipCode(getState());
    const queryParams = '';
    // zipCode
    //   ? insertKeyToQueryParam('zipcode', zipCode, '')
    //   : '';


    //response = await api.get('/api/v1/featured/market/products/');
    response = await LocalRedis(
      api,
      `/api/v1/featured/market/products/${queryParams}`,
      'FEATURED_PRODUCTS_REQUEST',
    );
  } catch (error) {
    dispatch({
      type: appActionTypes.FEATURED_PRODUCTS_FAILURE,
      error: true,
      payload: error,
    });
    throw error;
  }

  // Dispatch Success
  dispatch({
    type: appActionTypes.FEATURED_PRODUCTS_SUCCESS,
    featuredProducts: response.data,
  });
  return response;
};
//FEATURES STORES
const getFeaturedStores = () => async (dispatch, getState, api) => {
  // Request
  dispatch({
    type: appActionTypes.FEATURED_STORES_REQUEST,
  });

  // API call configuration
  let response;
  try {
    //response = await api.get('/api/v1/featured/market/stores/');
    response = await LocalRedis(
      api,
      '/api/v1/featured/market/stores/',
      'FEATURED_STORES_REQUEST',
    );
  } catch (error) {
    dispatch({
      type: appActionTypes.FEATURED_STORES_FAILURE,
      error: true,
      payload: error,
    });
    throw error;
  }

  // Dispatch Success
  dispatch({
    type: appActionTypes.FEATURED_STORES_SUCCESS,
    featuredStores: response.data,
  });
  return response;
};
// RANDOM PRODUCTS
const getRandomProducts = () => async (dispatch, getState, api) => {
  // Request
  dispatch({
    type: appActionTypes.GET_RANDOM_PRODUCTS_REQUEST,
  });

  // API call configuration
  let response;
  try {
    // response = await api.get(
    //   '/api/v1/market/products/?ordering=_random&page_size=12'
    // );
    response = await LocalRedis(
      api,
      '/api/v1/market/products/?ordering=_random&page_size=12',
      'GET_RANDOM_PRODUCTS_REQUEST',
    );
  } catch (error) {
    dispatch({
      error,
      type: appActionTypes.GET_RANDOM_PRODUCTS_FAILURE,
    });
    throw error;
  }

  // Dispatch Success
  dispatch({
    type: appActionTypes.GET_RANDOM_PRODUCTS_SUCCESS,
    products: response.data.results,
  });
  return response;
};

/**---------------------------------------
                SEARCH
-----------------------------------------*/
// SEARCH ACTIONS
const openSearchBar = () => async (dispatch, getState, api) => {
  // Dispatch Success
  dispatch({
    type: appActionTypes.OPEN_SEARCH_BAR,
  });
};
const closeSearchBar = () => async (dispatch, getState, api) => {
  // Dispatch Success
  dispatch({
    type: appActionTypes.CLOSE_SEARCH_BAR,
  });
};
const getSearchStores = (queryString) => async (dispatch, getState, api) => {
  // Request
  dispatch({
    type: appActionTypes.GET_SEARCH_STORES_REQUEST,
  });

  // API call configuration
  let response;
  try {
    let queryParams = queryString ? queryString : '';
    const zipCode = getUserZipCode(getState());
    queryParams += zipCode ? `&zipcode=${zipCode}` : '';
    //response = await api.get(`/api/v1/search/${queryParams}&type=stores`);

    response = await LocalRedis(
      api,
      `/api/v1/search/${queryParams}&type=stores`,
      `GET_SEARCH_STORES_REQUEST___${queryParams}`,
    );
  } catch (error) {
    dispatch({
      error,
      type: appActionTypes.GET_SEARCH_STORES_FAILURE,
    });
    throw error;
  }

  // Dispatch Success
  dispatch({
    type: appActionTypes.GET_SEARCH_STORES_SUCCESS,
    results: response.data,
  });
  return response;
};
const getSearchProducts = (queryString) => async (dispatch, getState, api) => {
  // Request
  dispatch({
    type: appActionTypes.GET_SEARCH_PRODUCTS_REQUEST,
  });

  // API call configuration
  let response;
  try {
    let queryParams = queryString ? queryString : '';
    const zipCode = getUserZipCode(getState());
    queryParams += zipCode ? `&zipcode=${zipCode}` : '';
    //response = await api.get(`/api/v1/search/${queryParams}&type=products`);

    response = await LocalRedis(
      api,
      `/api/v1/search/${queryParams}&type=products`,
      `GET_SEARCH_PRODUCTS_REQUEST___${queryParams}`,
    );
  } catch (error) {
    dispatch({
      error,
      type: appActionTypes.GET_SEARCH_PRODUCTS_FAILURE,
    });
    throw error;
  }

  // Dispatch Success
  dispatch({
    type: appActionTypes.GET_SEARCH_PRODUCTS_SUCCESS,
    results: response.data,
  });
  return response;
};
const getSearchArticles = (queryString) => async (dispatch, getState, api) => {
  // Request
  dispatch({
    type: appActionTypes.GET_SEARCH_ARTICLES_REQUEST,
  });

  // API call configuration
  let response;
  try {
    // response = await api.get(`/api/v1/search/${queryString}&type=articles`);
    response = await LocalRedis(
      api,
      `/api/v1/search/${queryString}&type=articles`,
      `GET_SEARCH_ARTICLES_REQUEST___${queryString}`,
    );
  } catch (error) {
    dispatch({
      error,
      type: appActionTypes.GET_SEARCH_ARTICLES_FAILURE,
    });
    throw error;
  }

  // Dispatch Success
  dispatch({
    type: appActionTypes.GET_SEARCH_ARTICLES_SUCCESS,
    results: response.data,
  });
  return response;
};
const getCategoryResults = (queryString, slug) => async (
  dispatch,
  getState,
  api,
) => {
  // Request
  dispatch({
    type: appActionTypes.GET_CATEGORY_RESULTS_REQUEST,
  });
  let apiEndpoint;
  let localRedisKey;
  let queryParams = queryString ? queryString : '';
  const zipCode = getUserZipCode(getState());
  queryParams += zipCode ? `&zipcode=${zipCode}` : '';

  if (slug === 'envio-nacional') {
    apiEndpoint = `/api/v1/market/products/${queryParams}&national_shipping=true`;
    localRedisKey = `${queryParams}_national_shipping`;
  } else {
    apiEndpoint = `/api/v1/market/products/${queryParams}`;
    localRedisKey = `${queryParams}`;
  }
  // API call configuration
  let response;
  try {
    // response = await api.get(apiEndpoint);
    response = await LocalRedis(
      api,
      apiEndpoint,
      `GET_CATEGORY_RESULTS_REQUEST___${localRedisKey}`,
    );
  } catch (error) {
    dispatch({
      error,
      type: appActionTypes.GET_CATEGORY_RESULTS_FAILURE,
    });
    throw error;
  }

  // Dispatch Success
  dispatch({
    type: appActionTypes.GET_CATEGORY_RESULTS_SUCCESS,
    payload: response.data,
  });
  return response;
};
const clearSearchResults = () => async (dispatch, getState, api) => {
  // Request
  dispatch({
    type: appActionTypes.CLEAR_SEARCH_RESULTS,
  });
};

// ALL PRODUCTS
const getAllProducts = () => async (dispatch, getState, api) => {
  // Request
  dispatch({
    type: appActionTypes.GET_ALL_PRODUCTS_REQUEST,
  });

  // API call configuration
  let response;
  try {
    //response = await api.get('/api/v1/market/products/');
    response = await LocalRedis(
      api,
      '/api/v1/market/products/',
      'GET_ALL_PRODUCTS_REQUEST',
    );
  } catch (error) {
    dispatch({
      error,
      type: appActionTypes.GET_ALL_PRODUCTS_FAILURE,
    });
    throw error;
  }

  // Dispatch Success
  dispatch({
    type: appActionTypes.GET_ALL_PRODUCTS_SUCCESS,
    products: response.data,
  });
  return response;
};

/**---------------------------------------
                COUPON
-----------------------------------------*/
const coupon = (email) => async (dispatch, getState, api) => {
  dispatch({
    type: appActionTypes.COUPON_REQUEST,
  });

  // API call configuration
  let response;
  const requestOptions = {
    headers: {
      'Content-Type': 'application/json',
    },
  };

  // Make/Handle API call
  try {
    response = await api.post(
      '/api/v1/marketing/home-coupon/',
      JSON.stringify(email),
      requestOptions,
    );
  } catch (error) {
    dispatch({
      type: appActionTypes.COUPON_FAILURE,
      error: true,
      payload: error,
    });
    //throw error;
  }

  // Dispatch Login Success
  dispatch({
    type: appActionTypes.COUPON_SUCCESS,
    payload: response.data,
  });
  return response;
};

// SHIPPING ZONES FILTERS
const getShippingZones = () => async (dispatch, getState, api) => {
  // Request
  dispatch({
    type: appActionTypes.GET_SHIPPING_ZONES_REQUEST,
  });

  // API call configuration
  let response;
  try {
    //response = await api.get('/api/v1/market/filters/shipping_zones/');
    response = await LocalRedis(
      api,
      '/api/v1/market/filters/shipping_zones/',
      'GET_SHIPPING_ZONES_REQUEST',
    );
  } catch (error) {
    dispatch({
      type: appActionTypes.GET_SHIPPING_ZONES_FAILURE,
      error,
    });
    throw error;
  }

  // Dispatch Success
  dispatch({
    type: appActionTypes.GET_SHIPPING_ZONES_SUCCESS,
    shippingZones: response.data,
  });
  return response;
};

/**---------------------------------------
              JOB OPENINGS
-----------------------------------------*/

const getJobOpenings = () => async (dispatch, getState, api) => {
  // Request
  dispatch({
    type: appActionTypes.GET_JOB_OPENINGS_REQUEST,
  });

  // API call configuration

  let response;
  try {
    response = await LocalRedis(
      api,
      '/api/v1/vacancies/',
      'GET_JOB_OPENINGS_REQUEST',
    );

    // if (!response) {

    //   window.localStorage.setItem('FETCH_MARKET_BANNERS_REQUEST', response);
    //   console.log("si hay en local")
    // }else{
    //   console.log("no hay en local")
    // }
  } catch (error) {
    dispatch({
      type: appActionTypes.GET_JOB_OPENINGS_FAILURE,
      error: true,
      payload: error,
    });
    throw error;
  }

  // Dispatch Success
  dispatch({
    type: appActionTypes.GET_JOB_OPENINGS_SUCCESS,
    payload: response.data,
  });
  return response;
};

//Create URL Bitly
const createBitly = (longURL) => async (dispatch, getState, api) => {
  // Request
  dispatch({
    type: appActionTypes.CREATE_BITLY_REQUEST,
  });

  // API call configuration
  let response;
  try {
    const convertURL = encodeURIComponent(longURL);
    response = await axios
      .get(`/shorturl?url=${convertURL}`)
      .then((e) => e.data.url);
  } catch (error) {
    dispatch({
      type: appActionTypes.CREATE_BITLY_FAILURE,
      error,
    });
    response = '';
    //throw error;
  }
  // Dispatch Success
  dispatch({
    type: appActionTypes.CREATE_BITLY_SUCCESS,
  });
  return response;
};

/**---------------------------------------
              JOB OPENINGS
-----------------------------------------*/

const getStoresListVisa = () => async (dispatch, getState, api) => {
  // Request
  dispatch({
    type: appActionTypes.GET_STORES_VISA_REQUEST,
  });

  // API call configuration

  let response;
  try {
    response = await LocalRedis(api, '/api/v1/market/stores_hard/?page_size=14');
  } catch (error) {
    dispatch({
      type: appActionTypes.GET_STORES_VISA_FAILURE,
      error: true,
      payload: error,
    });
    throw error;
  }

  // Dispatch Success
  dispatch({
    type: appActionTypes.GET_STORES_VISA_SUCCESS,
    payload: response.data,
  });
  return response;
};

// BAZAR STORES LIST
const getBazarStoresList = (queryString = '', slug) => async (
  dispatch,
  getState,
  api,
) => {
  // Request
  dispatch({
    type: appActionTypes.STORES_BAZAR_LIST_REQUEST,
  });
  // API call configuration
  let response;
  let queryParams = queryString
    ? queryString
    : '?ordering=-created&page=1&page_size=3&has_cover=true&has_photo=true';
  const zipCode = getUserZipCode(getState());
  queryParams += zipCode ? `&order_zipcode=${zipCode}` : '';
  queryParams += `&products__category__slug=${slug}`;

  //
  let apiEndpoint;
  let localRedisKey;
  if (slug === 'envio-nacional') {
    apiEndpoint = `/api/v1/market/stores/${queryParams}&zone=national`;
    localRedisKey = `${queryParams}_zone_national`;
  } else {
    apiEndpoint = `/api/v1/market/stores/${queryParams}`;
    localRedisKey = `${queryParams}`;
  }

  try {
    //response = await api.get(apiEndpoint);
    response = await LocalRedis(
      api,
      apiEndpoint,
      `STORES_LIST_REQUEST___${localRedisKey}`,
    );
  } catch (error) {
    dispatch({
      type: appActionTypes.STORES_BAZAR_LIST_FAILURE,
      error: true,
      payload: error,
    });
    throw error;
  }

  // Dispatch Success
  dispatch({
    type: appActionTypes.STORES_BAZAR_LIST_SUCCESS,
    stores: response.data,
  });
  return response;
};

/**---------------------------------------
              PROHIBITED WORDS
-----------------------------------------*/

const getProhibitedWords = () => async (dispatch, getState, api) => {
  // Request
  dispatch({
    type: appActionTypes.GET_PROHIBITED_WORDS_REQUEST,
  });

  // API call configuration
  let response;
  try {
    response = await LocalRedis(api, '/api/v1/project/badwords/');
  } catch (error) {
    dispatch({
      type: appActionTypes.GET_PROHIBITED_WORDS_FAILURE,
      error: true,
      payload: error,
    });
    throw error;
  }

  // Dispatch Success
  dispatch({
    type: appActionTypes.GET_PROHIBITED_WORDS_SUCCESS,
    payload: response.data,
  });
  return response;
};

/**---------------------------------------
            EXPORT ACTIONS
-----------------------------------------*/
export const appActions = {
  fetchBanners,
  getMarketCategories,
  getMarketInterests,
  getStoresList,
  getSearchStores,
  getSearchProducts,
  getSearchArticles,
  clearSearchResults,
  getCategoryResults,
  getFeaturedProducts,
  getFeaturedStores,
  getRandomProducts,
  isMobile,
  setAppSection,
  openSearchBar,
  closeSearchBar,
  getPickupSchedules,
  getShippingMethods,
  getPaymentMethods,
  coupon,
  getAllProducts,
  getShippingZones,
  getJobOpenings,

  createBitly,

  getStoresListVisa,
  getBazarStoresList,
  getProhibitedWords,
};
