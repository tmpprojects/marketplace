import { default as reduxActions } from 'redux-form/lib/actions';
import { SHOPPING_CART_FORM_CONFIG } from '../Utils/shoppingCart/shoppingCartFormConfig';
import { shoppingCartActionTypes } from '../Constants';
import { getUserZipCode } from '../Reducers/users.reducer';
import { getCartProductsByStoreAndShipping } from '../Reducers/shoppingCart.reducer';
import { appActions } from './app.actions';
import { trackWithGTM } from '../Utils/trackingUtils';
import axios from 'axios';

const { change } = reduxActions;

const updateCartProduct = (product, paymentMethod = undefined) => async (
  dispatch,
  getState,
  api,
) => {
  // Request
  dispatch({
    type: shoppingCartActionTypes.UPDATE_CART_PRODUCT_REQUEST,
  });

  // API call configuration
  let response;
  const requestOptions = {
    headers: {
      'Content-Type': 'application/json',
    },
  };
  try {
    const zipCode = getUserZipCode(getState());
    let queryParams = '?';
    queryParams = zipCode
      ? `${queryParams}shipping_postal_code=${zipCode}&`
      : queryParams;
    queryParams = paymentMethod
      ? `${queryParams}payment_method=${paymentMethod}&`
      : queryParams;
    response = await api.post(
      `/api/v1/cart/?${queryParams}`,
      JSON.stringify(product),
      requestOptions,
    );
  } catch (error) {
    dispatch({
      type: shoppingCartActionTypes.UPDATE_CART_PRODUCT_FAILURE,
      error: true,
      payload: error,
    });
    throw error;
  }

  // Dispatch Success
  // dispatch({
  //     type: shoppingCartActionTypes.UPDATE_CART_PRODUCT,
  //     product: response.data
  // });
  // Update Cart
  dispatch(fetchShoppingCart());

  // Return promise
  return response;
};

const setShoppingCartUpdateStatus = (flag = undefined) => async (
  dispatch,
  getState,
  api,
) => {
  const status = flag !== undefined ? flag : false;
  // Dispatch Success
  dispatch({
    type: shoppingCartActionTypes.SET_CART_UPDATE_STATUS_REQUEST,
    status,
  });
  return status;
};

const fetchShoppingCart = (paymentMethod = undefined) => async (
  dispatch,
  getState,
  api,
) => {
  // Request
  dispatch({
    type: shoppingCartActionTypes.GET_CART_PRODUCTS_REQUEST,
  });
  //
  //dispatch(setShoppingCartUpdateStatus(false));

  // API call configuration
  let response;
  try {
    // Construct query params
    const zipCode = getUserZipCode(getState());
    let queryParams = '?';
    queryParams = zipCode
      ? `${queryParams}shipping_postal_code=${zipCode}&`
      : queryParams;
    queryParams = paymentMethod
      ? `${queryParams}payment_method=${paymentMethod}&`
      : queryParams;
    response = await api.get(`/api/v1/cart/${queryParams}`);
  } catch (error) {
    dispatch({
      type: shoppingCartActionTypes.GET_CART_PRODUCTS_FAILURE,
      error: true,
      payload: error,
    });

    // ATENTION!
    // We´re not throwing an error because this action is called in several places,
    // and Backend API sends an 500 error if something goes wrong.
    // This should be refactored.
    //throw error // <---- Uncomment this line when ready.
    return response;
  }

  // Dispatch Success
  dispatch({
    type: shoppingCartActionTypes.GET_CART_PRODUCTS,
    products: response.data,
    _store: getState(),
  });

  // Update shipping form orders with new cart information
  setTimeout(() => {
    dispatch(
      change(
        SHOPPING_CART_FORM_CONFIG.formName,
        'orders',
        getCartProductsByStoreAndShipping({
          ...getState(),
          cart: {
            ...getState().cart,
            products: response.data,
          },
        }),
      ),
    );
  }, 0);

  // Update status flag
  //dispatch(setShoppingCartUpdateStatus(true));*/

  //
  return response;
};

const getPriceByZipCode = (zipCode, paymentMethod = undefined) => async (
  dispatch,
  getState,
  api,
) => {
  // Request
  dispatch({
    type: shoppingCartActionTypes.GET_PRICE_BY_ZIPCODE_REQUEST,
  });

  // API call configuration
  let response;
  try {
    // Construct query params
    let queryParams = '?';
    queryParams = zipCode
      ? `${queryParams}shipping_postal_code=${zipCode}&`
      : queryParams;
    queryParams = paymentMethod
      ? `${queryParams}payment_method=${paymentMethod}&`
      : queryParams;
    response = await api.get(`/api/v1/cart/${queryParams}`);
  } catch (error) {
    dispatch({
      type: shoppingCartActionTypes.GET_PRICE_BY_ZIPCODE_FAILURE,
      error: true,
      payload: error,
    });
    throw error;
  }

  // Dispatch Success
  // dispatch({
  //     type: shoppingCartActionTypes.GET_PRICE_BY_ZIPCODE,
  //     zipCode: response.data
  // });
  // Update Cart
  dispatch(fetchShoppingCart());

  return response;
};

/**
 * removeFromCart()
 * Removes a product from the cart,
 * Track event with analytics,
 * Update shopping cart.
 * @param {object} product | Product Object
 */
const removeFromCart = (product) => async (dispatch, getState, api) => {
  // Request
  dispatch({
    type: shoppingCartActionTypes.REMOVE_FROM_CART_REQUEST,
  });

  // API call configuration
  let response;
  try {
    const productAttributeID = product.attribute ? `${product.attribute}/` : '';
    response = await api.delete(
      `/api/v1/cart/${product.slug}/${productAttributeID}`,
    );
  } catch (error) {
    dispatch({
      type: shoppingCartActionTypes.REMOVE_FROM_CART_FAILURE,
      error: true,
      payload: error,
    });
    throw error;
  }

  // Track Event
  const productModel = {
    id: product.product.id.toString(),
    name: product.product.name,
    variant: product.product.physical_properties.size.display_name,
    brand: product.product.store.name,
    quantity: product.quantity,
    dimension1: product.product.store.slug,
    //category: product.product.category.length ? <---- Pedir este datos en la API
    //    product.product.category[0] : '',
    //variant: 'Black', TODO: Add variant name/description,
    //Ecostalitos para pan hacer prueba de 2 de quantity, id: 14111
  };

  trackWithGTM('eec.remove', {
    actionField: {
      list: 'Cart',
    },
    products: [productModel],
  });

  // Dispatch Success
  // dispatch({
  //     type: shoppingCartActionTypes.REMOVE_FROM_CART_SUCCESS,
  //     product
  // });
  // Update Cart Products
  dispatch(fetchShoppingCart());

  // Return axios promise
  return response;
};

/**
 * changeAddress()
 * @param {string} address | Addres ID
 */
const changeShippingAddress = (address) => async (dispatch, getState, api) => {
  // Request
  dispatch({
    type: shoppingCartActionTypes.CHANGE_ADDRESS,
    address,
  });
  //
  dispatch(change(SHOPPING_CART_FORM_CONFIG.formName, 'addressShipping', address));

  dispatch(
    change(
      SHOPPING_CART_FORM_CONFIG.formName,
      'selectedShippingAddress',
      address.uuid,
    ),
  );

  // Reload content
  globalThis.shippingAddressObserver.broadcast(address.zip_code);

  //
  window.localStorage.setItem('shippingAddress', JSON.stringify(address));

  return Promise.resolve(address);
};

const changeShippingMethod = (shippingMethod) => async (dispatch, getState, api) => {
  // Request
  dispatch({
    type: shoppingCartActionTypes.CHANGE_SHIPPING_METHOD,
    shippingMethod,
  });
};
const sendOrder = (order) => async (dispatch, getState, api) => {
  // Request
  dispatch({
    type: shoppingCartActionTypes.SEND_ORDER_REQUEST,
  });

  // API call configuration
  const requestOptions = {
    headers: {
      'Content-Type': 'application/json',
    },
  };

  let response;
  try {
    response = await api.post(
      '/api/v1/order/group/',
      JSON.stringify(order),
      requestOptions,
    );
  } catch (error) {
    dispatch({
      type: shoppingCartActionTypes.SEND_ORDER_FAILURE,
      error: true,
      payload: error,
    });
    throw error;
  }

  // Dispatch Success
  dispatch({
    type: shoppingCartActionTypes.SEND_ORDER_SUCCESS,
    payload: response.data,
  });

  // Update Cart
  dispatch(fetchShoppingCart());

  // Return Promise
  return response;
};
const sendPayPalOrder = (order) => async (dispatch, getState, api) => {
  // Request
  dispatch({
    type: shoppingCartActionTypes.SEND_ORDER_REQUEST,
  });

  // API call configuration
  const requestOptions = {
    headers: {
      'Content-Type': 'application/json',
    },
  };

  // Send Order to backend server
  return api
    .post('/api/v1/order/group/', order, requestOptions)
    .then((response) => response)
    .catch((error) => {
      dispatch({
        type: shoppingCartActionTypes.SEND_ORDER_FAILURE,
        error: true,
        payload: error,
      });
      throw error;
    });
};
const payPalAuthorize = (paymentData) => async (dispatch, getState, api) => {
  // Request
  dispatch({
    type: shoppingCartActionTypes.PAYPAL_AUTHORIZE_REQUEST,
  });

  // Send Order to backend server
  return api
    .get('/api/v1/payment/paypal/execute-payment/', paymentData)
    .then((response) => {
      // Dispatch Success
      dispatch({
        type: shoppingCartActionTypes.SEND_ORDER_SUCCESS,
        payload: response.data,
      });

      // // Update Cart
      // dispatch(fetchShoppingCart());

      return response;
    })
    .catch((error) => {
      dispatch({
        type: shoppingCartActionTypes.PAYPAL_AUTHORIZE_FAILURE,
        error: true,
        payload: error,
      });
      return error;
    });
};
const paymentSuccess = (order) => async (dispatch, getState, api) => {
  // Request
  // dispatch({
  //     type: shoppingCartActionTypes.SEND_ORDER_REQUEST,
  // });

  // API call configuration
  const requestOptions = {
    headers: {
      'Content-Type': 'application/json',
    },
  };

  // Send Order to backend server
  return api
    .post('/api/v1/order/group/', order, requestOptions)
    .then((response) => {
      // Dispatch Success
      dispatch({
        type: shoppingCartActionTypes.SEND_ORDER_SUCCESS,
        payload: response.data,
      });

      // Update Cart
      dispatch(fetchShoppingCart());

      return response;
    })
    .catch((error) => {
      dispatch({
        type: shoppingCartActionTypes.SEND_ORDER_FAILURE,
        error: true,
        payload: error,
      });
    });
};
const paymentError = (order) => async (dispatch, getState, api) => {
  // Request
  // dispatch({
  //     type: shoppingCartActionTypes.SEND_ORDER_REQUEST,
  // });

  // API call configuration
  const requestOptions = {
    headers: {
      'Content-Type': 'application/json',
    },
  };

  // Send Order to backend server
  return api
    .post('/api/v1/payment/paypal/cancel-payment/', order, requestOptions)
    .then((response) => response)
    .catch((error) => {
      dispatch({
        type: shoppingCartActionTypes.SEND_ORDER_FAILURE,
        error: true,
        payload: error,
      });
    });
};
const paymentCancel = (order) => async (dispatch, getState, api) => {
  // Request
  // dispatch({
  //     type: shoppingCartActionTypes.SEND_ORDER_REQUEST,
  // });

  // API call configuration
  const requestOptions = {
    headers: {
      'Content-Type': 'application/json',
    },
  };

  // Send Order to backend server
  // Encviar paymentID
  return api
    .post('/api/v1/payment/paypal/cancel-payment/', order, requestOptions)
    .then((response) => response)
    .catch((error) => {
      dispatch({
        type: shoppingCartActionTypes.SEND_ORDER_FAILURE,
        error: true,
        payload: error,
      });
    });
};
const changePaymentMethod = (paymentMethod) => async (dispatch, getState, api) => {
  // Request
  dispatch({
    type: shoppingCartActionTypes.CHANGE_PAYMENT_METHOD,
    paymentMethod,
  });
};
const emptyShoppingCart = () => async (dispatch, getState, api) => {
  // Request
  dispatch({
    type: shoppingCartActionTypes.EMPTY_SHOPPING_CART,
  });
};

//VERIFY PROMO COUPON
const verifyPromoCoupon = (coupon) => async (dispatch, getState, api) => {
  // Request
  dispatch({
    type: shoppingCartActionTypes.VERIFY_PROMO_CODE_REQUEST,
  });

  // API call configuration
  let response;
  const requestOptions = {
    headers: {
      'Content-Type': 'application/json',
    },
  };
  try {
    response = await api.post(
      '/api/v1/cart/check-code/',
      { code: coupon },
      requestOptions,
    );
  } catch (error) {
    dispatch({
      type: shoppingCartActionTypes.VERIFY_PROMO_CODE_FAILURE,
      error,
    });
    throw error;
  }

  // Dispatch Success
  dispatch({
    type: shoppingCartActionTypes.VERIFY_PROMO_CODE_SUCCESS,
    coupon,
    couponDetails: response.data,
  });

  //dispatch(fetchShoppingCart())
  return response;
};

const cleanCouponData = () => (dispatch, getState, api) => {
  dispatch({
    type: shoppingCartActionTypes.CLEAN_COUPON_DATA,
  });
};

const setAutofillCouponToFalse = () => (dispatch, getState, api) => {
  dispatch({
    type: shoppingCartActionTypes.SET_AUTOFILL_COUPON_FALSE,
    payload: false,
  });
};

const fetchPaymentErrorMessages = () => async (dispatch, getState, api) => {
  // Request
  dispatch({
    type: shoppingCartActionTypes.GET_PAYMENT_ERRORS_MESSAGES_REQUEST,
  });

  // API call configuration
  let response;
  try {
    response = await axios.get(
      'https://canastarosa.s3.us-east-2.amazonaws.com/json-services/PaymentErrors.json',
    );
  } catch (error) {
    console.log(error);
  }

  // Dispatch Success
  dispatch({
    type: shoppingCartActionTypes.GET_PAYMENT_ERRORS_MESSAGES_SUCCESS,
    paymentErrorMessages: response.data.errors,
  });
  return response;
};

export const shoppingCartActions = {
  // Cart Manipulation
  emptyShoppingCart,
  removeFromCart,
  fetchShoppingCart,
  setShoppingCartUpdateStatus,
  updateCartProduct,
  changeShippingAddress,
  changeShippingMethod,
  changePaymentMethod,
  getPriceByZipCode,

  // MercadoPago & Bank Transfer
  sendOrder,

  // PAY PAL
  sendPayPalOrder,
  payPalAuthorize,
  paymentSuccess,
  paymentError,
  paymentCancel,

  //PROMO COUPON
  verifyPromoCoupon,
  cleanCouponData,
  setAutofillCouponToFalse,

  //PaymentErrorsMessages
  fetchPaymentErrorMessages,
};
