import { mystoreActionTypes, storeActionTypes } from '../Constants';
import { trackWithFacebookPixel } from '../Utils/trackingUtils';
import { LocalRedis, ModifyCache } from './localRedis';
import { getUserZipCode } from '../Reducers/users.reducer';

async function loadStore(api, dispatch, endpoint, params) {
  let response;
  try {
    //response = await api.get(endpoint);
    response = await LocalRedis(api, endpoint, `GET_STORE_FAILURE___${params}`);
  } catch (error) {
    dispatch({
      type: storeActionTypes.GET_STORE_FAILURE,
      error: true,
      payload: error,
    });
    throw error;
  }

  return response;
}

// GET STORE DATA
const fetchStore = (slug = '') => async (dispatch, getState, api) => {
  // Login Request
  dispatch({
    type: storeActionTypes.GET_STORE_REQUEST,
  });

  // Make/Handle API call
  const response = await loadStore(
    api,
    dispatch,
    `/api/v1/market/stores/${slug}/`,
    slug,
  );

  // Success
  dispatch({
    type: storeActionTypes.GET_STORE_SUCCESS,
    payload: response.data,
  });
  return response;
};

// FETCH PRODUCTS
const fetchProducts = (store_slug, filter = '') => async (
  dispatch,
  getState,
  api,
) => {
  // Dispatch Action
  dispatch({
    type: storeActionTypes.FETCH_PRODUCTS_REQUEST,
  });

  // Make/Handle API call
  let response;
  try {
    const zipCode = getUserZipCode(getState());
    let queryParams = zipCode ? `zipcode=${zipCode}&` : '';
    queryParams += `ordering=-created_date&page_size=100${filter}`;

    //response = await api.get(
    //  `/api/v1/market/stores/${store_slug}/products/?ordering=-created_date&page_size=500${filter}`
    // );
    response = await LocalRedis(
      api,
      `/api/v1/market/stores/${store_slug}/products/?${queryParams}`,
      `FETCH_PRODUCTS_REQUEST___${store_slug}_${filter}`,
    );
  } catch (error) {
    dispatch({
      type: storeActionTypes.FETCH_PRODUCTS_FAILURE,
      error: true,
      payload: error,
    });
    throw error;
  }

  // Dispatch Success
  dispatch({
    type: storeActionTypes.FETCH_PRODUCTS_SUCCESS,
    payload: response.data,
  });
  return response;
};

// CREATE STORE
const createStore = (formData) => async (dispatch, getState, api) => {
  // Dispatch Action
  dispatch({
    type: storeActionTypes.GET_STORE_REQUEST,
  });

  // API call configuration
  let response;
  const requestOptions = {
    headers: {
      'Content-Type': 'application/json',
    },
  };

  const newStoreTemplate = {
    ...formData,
    fiscal_registry: 'without-rfc',
  };

  // Make/Handle API call
  try {
    response = await api.post(
      '/api/v1/market/mystore/',
      JSON.stringify(newStoreTemplate),
      requestOptions,
    );
  } catch (error) {
    dispatch({
      type: storeActionTypes.GET_STORE_FAILURE,
      error: true,
      payload: error,
    });
    throw error;
  }

  // Register Facebook Event
  // trackWithFacebookPixel('track', 'AccountActivation');
  // trackWithFacebookPixel('track', 'AccountActivation', {
  //     ...productDescription,
  //     value: productDescription.price,
  //     currency: 'MXN'
  // });

  // Dispatch Login Success
  dispatch({
    type: mystoreActionTypes.GET_MYSTORE_SUCCESS,
    payload: response.data,
  });
  return response;
};

// FETCH PRODUCT DETAIL
const fetchProductDetail = (storeSlug, productSlug) => async (
  dispatch,
  getState,
  api,
) => {
  // Dispatch Action
  dispatch({
    type: storeActionTypes.FETCH_PRODUCT_DETAIL_REQUEST,
  });

  // Make/Handle API call
  let response;
  try {
    const zipCode = getUserZipCode(getState());
    const queryParams = zipCode ? `?zipcode=${zipCode}` : '';

    //response = await api.get(`/api/v1/market/stores/${storeSlug}/products/${productSlug}/`);
    response = await LocalRedis(
      api,
      `/api/v1/market/stores/${storeSlug}/products/${productSlug}/${queryParams}`,
      `FETCH_PRODUCT_DETAIL_REQUEST___${storeSlug}_${productSlug}`,
    );
  } catch (error) {
    dispatch({
      type: storeActionTypes.FETCH_PRODUCT_DETAIL_FAILURE,
      error: true,
      payload: error,
    });
    throw error;
  }

  // Dispatch Login Success
  dispatch({
    type: storeActionTypes.FETCH_PRODUCT_DETAIL_SUCCESS,
    payload: response,
  });
  return response;
};

// FETCH PRODUCT DETAIL
const resetProductDetail = () => async (dispatch, getState, api) => {
  // Dispatch Action
  dispatch({
    type: storeActionTypes.FETCH_PRODUCT_DETAIL_REQUEST,
  });
};

// FETCH STORE SECTIONS
const fetchStoreSections = (store_slug) => async (dispatch, getState, api) => {
  // Dispatch Action
  dispatch({
    type: storeActionTypes.FETCH_STORE_SECTIONS_REQUEST,
  });

  // Make/Handle API call
  let response;
  try {
    response = await api.get(`/api/v1/market/stores/${store_slug}/sections/`);
  } catch (error) {
    dispatch({
      type: storeActionTypes.FETCH_STORE_SECTIONS_FAILURE,
      error: true,
      payload: error,
    });
    throw error;
  }

  // Dispatch Login Success
  dispatch({
    type: storeActionTypes.FETCH_STORE_SECTIONS_SUCCESS,
    payload: response.data,
  });
  return response;
};

//GET ALL REVIEWS FROM STORE
const getStoreReviews = (store_slug, queryString) => async (
  dispatch,
  getState,
  api,
) => {
  dispatch({
    type: storeActionTypes.GET_STORE_REVIEWS_REQUEST,
  });

  // Make/Handle API call
  let response;
  let query = '';
  if (queryString) {
    query = queryString;
  }

  response = await api
    .get(`/api/v1/reviews/reviews/stores/${store_slug}/${query}`)
    .catch((error) =>
      dispatch({
        type: storeActionTypes.GET_STORE_REVIEWS_FAILURE,
        error,
      }),
    );

  // Dispatch Response
  dispatch({
    type: storeActionTypes.GET_STORE_REVIEWS_SUCCESS,
    payload: response.data,
    store: store_slug,
  });
};

//GET PENDING REVIEWS OF USER FROM STORE
const getPendingStoreReviews = (store_slug) => async (dispatch, getState, api) => {
  dispatch({
    type: storeActionTypes.GET_PENDING_STORE_REVIEWS_REQUEST,
  });

  // Make/Handle API call
  let response;
  response = await api
    .get(
      `/api/v1/reviews/my-reviews/stores/${store_slug}/?is_approved=null&page_size=100`,
    )
    .catch((error) =>
      dispatch({
        type: storeActionTypes.GET_PENDING_STORE_REVIEWS_FAILURE,
        error,
      }),
    );

  // Dispatch Response
  dispatch({
    type: storeActionTypes.GET_PENDING_STORE_REVIEWS_SUCCESS,
    payload: response.data,
  });
};

//GET ALL REVIEWS FROM A PRODUCT
const getProductReviews = (product, queryString) => async (
  dispatch,
  getState,
  api,
) => {
  dispatch({
    type: storeActionTypes.GET_PRODUCT_REVIEWS_REQUEST,
  });

  // Make/Handle API call
  let response;
  let query = '';
  if (queryString) {
    query = queryString;
  }

  response = await api
    .get(
      `/api/v1/reviews/reviews/stores/${product.store_slug}/products/${product.product_slug}/${query}`,
    )
    .catch((error) =>
      dispatch({
        type: storeActionTypes.GET_PRODUCT_REVIEWS_FAILURE,
        error,
      }),
    );

  // Dispatch Response
  dispatch({
    type: storeActionTypes.GET_PRODUCT_REVIEWS_SUCCESS,
    payload: response.data,
    product: product.product_slug,
  });
};

//GET PENDING REVIEWS OF USER FROM PRODUCT
const getPendingProductReviews = (store_slug, product_slug) => async (
  dispatch,
  getState,
  api,
) => {
  dispatch({
    type: storeActionTypes.GET_PENDING_PRODUCT_REVIEWS_REQUEST,
  });

  // Make/Handle API call
  let response;
  response = await api
    .get(
      `/api/v1/reviews/my-reviews/stores/${store_slug}/products/${product_slug}/?is_approved=null&page_size=100`,
    )
    .catch((error) =>
      dispatch({
        type: storeActionTypes.GET_PENDING_PRODUCT_REVIEWS_FAILURE,
        error,
      }),
    );

  // Dispatch Response
  dispatch({
    type: storeActionTypes.GET_PENDING_PRODUCT_REVIEWS_SUCCESS,
    payload: response.data,
  });
};
// EXPORT INDEX
export const storeActions = {
  fetchStore,
  fetchProducts,
  fetchProductDetail,
  fetchStoreSections,
  createStore,
  resetProductDetail,
  getStoreReviews,
  getPendingStoreReviews,
  getProductReviews,
  getPendingProductReviews,
};
