import axios from 'axios';
import { mystoreActionTypes } from '../Constants';

function callApi(apiMethod, dispatch, endpoint, onFailure, data = {}, headers = {}) {
  let response;
  try {
    response = apiMethod(endpoint, data, headers);
  } catch (error) {
    onFailure(error);
    throw error;
  }
  return response;
}

// GET MYSTORE DATA
const fetchMyStore = () => async (dispatch, getState, api) => {
  // Login Request
  dispatch({
    type: mystoreActionTypes.GET_MYSTORE_REQUEST,
  });

  // Make/Handle API call
  const response = await callApi(
    api.get,
    dispatch,
    '/api/v1/market/mystore/',
    (error) => {
      dispatch({
        type: mystoreActionTypes.GET_MYSTORE_FAILURE,
        error: true,
        payload: error,
      });
    },
  );

  if (response.data.fiscal_data_default) {
    dispatch(fetchFiscalData(response.data.fiscal_data_default));
  }

  // Success
  dispatch({
    type: mystoreActionTypes.GET_MYSTORE_SUCCESS,
    payload: response.data,
  });
  return response;
};

// UPDATE MYSTORE
const updateMyStore = (userData = {}) => async (dispatch, getState, api) => {
  // Request
  dispatch({
    type: mystoreActionTypes.UPDATE_MYSTORE_REQUEST,
  });

  // API call configuration
  let response;
  const requestOptions = {
    headers: {
      'Content-Type': 'application/json',
    },
  };

  // Make/Handle API call
  try {
    response = await api.patch(
      '/api/v1/market/mystore/',
      JSON.stringify(userData),
      requestOptions,
    );
  } catch (error) {
    dispatch({
      type: mystoreActionTypes.UPDATE_MYSTORE_FAILURE,
      error,
    });
    throw error;
  }

  // Dispatch Success
  dispatch({
    type: mystoreActionTypes.UPDATE_MYSTORE_SUCCESS,
    payload: response.data,
  });
  return response;
};

// GET MYSTORE SECTIONS
const fetchMyStoreSections = () => async (dispatch, getState, api) => {
  // Login Request
  dispatch({
    type: mystoreActionTypes.GET_MYSTORE_SECTIONS_REQUEST,
  });

  // Make/Handle API call
  const response = await callApi(
    api.get,
    dispatch,
    '/api/v1/market/mystore/sections/',
    () => {
      dispatch({
        type: mystoreActionTypes.GET_MYSTORE_SECTIONS_FAILURE,
        error: true,
        payload: error,
      });
    },
  );

  // Success
  dispatch({
    type: mystoreActionTypes.GET_MYSTORE_SECTIONS_SUCCESS,
    payload: response.data,
  });
  return response;
};

// ADD MYSTORE SECTION
const addMyStoreSection = (sectionName) => async (dispatch, getState, api) => {
  // Login Request
  dispatch({
    type: mystoreActionTypes.ADD_MYSTORE_SECTION_REQUEST,
  });

  // Extra Headers & Params
  const requestOptions = {
    headers: {
      'Content-Type': 'application/json',
    },
  };

  // Make/Handle API call
  const response = await callApi(
    api.post,
    dispatch,
    '/api/v1/market/mystore/sections/',
    () => {
      dispatch({
        type: mystoreActionTypes.ADD_MYSTORE_SECTION_FAILURE,
        error: true,
        payload: error,
      });
    },
    { name: sectionName },
    requestOptions,
  );

  // Success
  dispatch({
    type: mystoreActionTypes.ADD_MYSTORE_SECTION_SUCCESS,
    payload: response.data,
  });
  return response;
};

// DELETE SECTION
const deleteSection = (sectionSlug = '') => async (dispatch, getState, api) => {
  // Request
  dispatch({
    type: mystoreActionTypes.DELETE_MYSTORE_SECTION_REQUEST,
  });

  // Make/Handle API call
  let response;
  try {
    response = await api.delete(`/api/v1/market/mystore/sections/${sectionSlug}/`);
  } catch (error) {
    dispatch({
      type: mystoreActionTypes.DELETE_MYSTORE_SECTION_FAILURE,
      error: true,
      payload: error,
    });
    throw error;
  }

  // Dispatch Success
  dispatch({
    type: mystoreActionTypes.DELETE_MYSTORE_SECTION_SUCCESS,
    payload: sectionSlug,
  });
  return response;
};

// UPDATE SECTION
const updateSection = (section = {}) => async (dispatch, getState, api) => {
  // Request
  dispatch({
    type: mystoreActionTypes.UPDATE_SECTION_REQUEST,
  });

  // API call configuration
  let response;
  const requestOptions = {
    headers: {
      'Content-Type': 'application/json',
    },
  };

  // Make/Handle API call
  try {
    response = await api.put(
      `/api/v1/market/mystore/sections/${section.slug}/`,
      JSON.stringify(section),
      requestOptions,
    );
  } catch (error) {
    dispatch({
      type: mystoreActionTypes.UPDATE_SECTION_FAILURE,
      error,
    });
    throw error;
  }

  // Dispatch Success
  dispatch({
    type: mystoreActionTypes.UPDATE_SECTION_SUCCESS,
    payload: response.data,
  });
  return response;
};

// GET MYSTORE FAQs
const fetchMyStoreFaqs = () => async (dispatch, getState, api) => {
  // Login Request
  dispatch({
    type: mystoreActionTypes.GET_MYSTORE_FAQS_REQUEST,
  });

  // Make/Handle API call
  const response = await callApi(
    api.get,
    dispatch,
    '/api/v1/market/mystore/faq/',
    () => {
      dispatch({
        type: mystoreActionTypes.GET_MYSTORE_FAQS_FAILURE,
        error: true,
        payload: { error: 'error' },
      });
    },
  );

  // Success
  dispatch({
    type: mystoreActionTypes.GET_MYSTORE_FAQS_SUCCESS,
    payload: response.data,
  });
  return response;
};

// ADD MYSTORE FAQs
const addMyStoreFaqs = (question) => async (dispatch, getState, api) => {
  // Login Request
  dispatch({
    type: mystoreActionTypes.ADD_MYSTORE_FAQS_REQUEST,
  });

  // Extra Headers & Params
  const requestOptions = {
    headers: {
      'Content-Type': 'application/json',
    },
  };

  // Make/Handle API call
  const response = await callApi(
    api.post,
    dispatch,
    '/api/v1/market/mystore/faq/',
    () => {
      dispatch({
        type: mystoreActionTypes.ADD_MYSTORE_FAQS_FAILURE,
        error: true,
        payload: { error: 'error' },
      });
    },
    question,
    requestOptions,
  );

  // Success
  dispatch({
    type: mystoreActionTypes.ADD_MYSTORE_FAQS_SUCCESS,
    payload: response.data,
  });
  return response;
};

// DELETE FAQS
const deleteFaq = (faqID = '') => async (dispatch, getState, api) => {
  // Request
  dispatch({
    type: mystoreActionTypes.DELETE_MYSTORE_FAQ_REQUEST,
  });

  // Make/Handle API call
  let response;
  try {
    response = await api.delete(`/api/v1/market/mystore/faq/${faqID}/`);
  } catch (error) {
    dispatch({
      type: mystoreActionTypes.DELETE_MYSTORE_FAQ_FAILURE,
      error: true,
      payload: { error: 'error' },
    });
    throw error;
  }

  // Dispatch Success
  dispatch({
    type: mystoreActionTypes.DELETE_MYSTORE_FAQ_SUCCESS,
    payload: faqID,
  });
  return response;
};

// UPDATE FAQ
const updateFaq = (faq = {}) => async (dispatch, getState, api) => {
  // Request
  dispatch({
    type: mystoreActionTypes.UPDATE_FAQ_REQUEST,
  });

  // API call configuration
  let response;
  const requestOptions = {
    headers: {
      'Content-Type': 'application/json',
    },
  };

  // Make/Handle API call
  try {
    response = await api.put(
      `/api/v1/market/mystore/faq/${faq.faq_id}/`,
      JSON.stringify(faq),
      requestOptions,
    );
  } catch (error) {
    dispatch({
      type: mystoreActionTypes.UPDATE_FAQ_FAILURE,
      error,
    });
    throw error;
  }

  // Dispatch Success
  dispatch({
    type: mystoreActionTypes.UPDATE_FAQ_SUCCESS,
    ...response.data,
  });
  return response;
};

// GET MYSTORE CATEGORIES
const fetchMyStoreCategories = () => async (dispatch, getState, api) => {
  // Login Request
  dispatch({
    type: mystoreActionTypes.GET_MYSTORE_CATEGORIES_REQUEST,
  });

  // Make/Handle API call
  const response = await callApi(
    api.get,
    dispatch,
    '/api/v1/market/categories/?version=2',
    () => {
      dispatch({
        type: mystoreActionTypes.GET_MYSTORE_CATEGORIES_FAILURE,
        error: true,
        payload: { error: 'error' },
      });
    },
  );

  // Success
  dispatch({
    type: mystoreActionTypes.GET_MYSTORE_CATEGORIES_SUCCESS,
    payload: response.data,
  });
  return response;
};

// UPDATE STORE PROFILE PHOTO
const uploadProfilePhoto = (userData = {}) => async (dispatch, getState, api) => {
  // Dispatch Request
  dispatch({
    type: mystoreActionTypes.UPDATE_PROFILE_PHOTO_REQUEST,
  });

  // API call configuration
  let response;
  const config = {
    headers: {
      'Content-Type': 'multipart/form-data',
    },
    onUploadProgress: (progressEvent) => {
      console.log(Math.round((progressEvent.loaded * 100) / progressEvent.total));
    },
  };

  // Make/Handle API call
  try {
    response = await api.patch('/api/v1/market/mystore/', userData, config);
  } catch (error) {
    dispatch({
      type: mystoreActionTypes.UPDATE_PROFILE_PHOTO_FAILURE,
      error: true,
      payload: error,
    });
    throw error;
  }

  // Dispatch Success
  dispatch({
    type: mystoreActionTypes.UPDATE_PROFILE_PHOTO_SUCCESS,
    payload: response.data,
  });
  return response;
};

// UPLOAD IMAGES TO STORE GALLERY
const addStorePhoto = (storeData) => async (dispatch, getState, api) => {
  // Dispatch Request
  dispatch({
    type: mystoreActionTypes.ADD_STORE_IMAGE_REQUEST,
  });

  // API call configuration
  let response;
  const config = {
    headers: {
      'Content-Type': 'multipart/form-data',
    },
    onUploadProgress: (progressEvent) => {
      console.log(Math.round((progressEvent.loaded * 100) / progressEvent.total));
    },
  };

  // Make/Handle API call
  try {
    response = await api.post('/api/v1/market/mystore/gallery/', storeData, config);
  } catch (error) {
    dispatch({
      type: mystoreActionTypes.ADD_STORE_IMAGE_FAILURE,
      error: true,
      payload: error,
    });
    throw error;
  }

  // Dispatch Success
  dispatch({
    type: mystoreActionTypes.ADD_STORE_IMAGE_SUCCESS,
    payload: response.data,
  });
  return response;
};

// DELETE STORE IMAGES
const deleteStorePhoto = (storeData = {}) => async (dispatch, getState, api) => {
  // Dispatch Request
  dispatch({
    type: mystoreActionTypes.DELETE_STORE_IMAGE_REQUEST,
  });

  // API call configuration
  let response;
  const config = {
    headers: {
      'Content-Type': 'multipart/form-data',
    },
  };

  // Make/Handle API call
  try {
    response = await api.delete(
      `/api/v1/market/mystore/gallery/${storeData.src}/`,
      storeData,
      config,
    );
  } catch (error) {
    dispatch({
      type: mystoreActionTypes.DELETE_STORE_IMAGE_FAILURE,
      error: true,
      payload: error,
    });
    throw error;
  }

  // Dispatch Success
  dispatch({
    type: mystoreActionTypes.DELETE_STORE_IMAGE_SUCCESS,
    payload: storeData.photo,
  });
  return response;
};

// UPDATE STORE PHOTO
const updateStorePhoto = ({ file, order }) => async (dispatch, getState, api) => {
  // Dispatch Request
  dispatch({
    type: mystoreActionTypes.UPDATE_STORE_IMAGE_REQUEST,
  });

  // Make/Handle API call
  let response;
  try {
    response = await api.patch(`/api/v1/market/mystore/gallery/${file}/`, {
      order,
    });
  } catch (error) {
    dispatch({
      type: mystoreActionTypes.UPDATE_STORE_IMAGE_FAILURE,
      error: true,
      payload: error,
    });
    throw error;
  }

  // Dispatch Success
  dispatch({
    type: mystoreActionTypes.UPDATE_STORE_IMAGE_SUCCESS,
    payload: response.data,
  });
  return response;
};

// CREATE PRODUCT
const addProduct = (userData = {}) => async (dispatch, getState, api) => {
  // Request
  dispatch({
    type: mystoreActionTypes.ADD_PRODUCT_REQUEST,
  });

  // API call configuration
  let response;
  let apiCall;
  const requestOptions = {
    headers: {
      'Content-Type': 'application/json',
    },
  };

  // Make/Handle API call
  try {
    apiCall = await api.post(
      '/api/v1/market/mystore/products/',
      JSON.stringify(userData),
      requestOptions,
    );
    response = apiCall.data;

    // Normalize shipping options values
    switch (response.physical_properties.size.display_name) {
      case 'Sobre':
        response.physical_properties.size = '1';
        break;
      case 'Pequeño':
        response.physical_properties.size = '2';
        break;
      case 'Mediano':
        response.physical_properties.size = '3';
        break;
      default:
        break;
    }
    switch (response.physical_properties.is_available.display_name) {
      case 'Disponible en Stock':
        response.physical_properties.is_available = '1';
        break;
      case 'Disponible Bajo Pedido':
        response.physical_properties.is_available = '2';
        break;
      case 'Disponible en Stock y Bajo Pedido':
        response.physical_properties.is_available = '3';
        break;
      default:
    }

    if (response.status.value === 'draft') {
      response.status = false;
    } else {
      response.status = true;
    }
  } catch (error) {
    dispatch({
      type: mystoreActionTypes.ADD_PRODUCT_FAILURE,
      error: true,
      payload: error,
    });
    throw error;
  }

  // Dispatch Success
  // dispatch({
  //     type: mystoreActionTypes.ADD_PRODUCT_SUCCESS,
  //     payload: response
  // });
  // Dispatch Success
  dispatch(fetchProductList());

  return apiCall;
};

// FETCH PRODUCT DETAILS
const fetchProduct = (slug) => async (dispatch, getState, api) => {
  // Request
  dispatch({
    type: mystoreActionTypes.FETCH_PRODUCT_REQUEST,
  });

  // Make/Handle API call
  let apiCall;
  let response;
  const productTemplate = {
    attribute_stock: [],
    attribute_list: [],
    physical_properties: {
      size: '1',
      life_time: '0',
      shipping_methods: '1',
      is_available: '1',
      fabrication_time: 1,
      shipping_schedules: [],
    },
    hasShippingDetails: false,
  };

  try {
    apiCall = await api.get(`/api/v1/market/mystore/products/${slug}/`);
    response = apiCall.data;

    // Normalize shipping options values
    response.physical_properties.life_time =
      `${response.physical_properties.life_time}` || '0';
    switch (response.physical_properties.size.display_name) {
      case 'Sobre':
        response.physical_properties.size = '1';
        break;
      case 'Pequeño':
        response.physical_properties.size = '2';
        break;
      case 'Mediano':
        response.physical_properties.size = '3';
        break;
      default:
    }
    switch (response.physical_properties.is_available.display_name) {
      case 'Disponible en Stock':
        response.physical_properties.is_available = '1';
        break;
      case 'Disponible Bajo Pedido':
        response.physical_properties.is_available = '2';
        break;
      case 'Disponible en Stock y Bajo Pedido':
        response.physical_properties.is_available = '3';
        break;
      default:
    }

    // Status
    if (response.status.value === 'draft') {
      response.status = false;
    } else {
      response.status = true;
    }

    // Attributes
    response.attribute_stock = response.attributes;
    response.attribute_list = response.attributes_menu;
    //
  } catch (error) {
    dispatch({
      type: mystoreActionTypes.FETCH_PRODUCT_FAILURE,
      error: true,
      payload: error,
    });
    throw error;
  }

  // Dispatch Success
  dispatch({
    type: mystoreActionTypes.FETCH_PRODUCT_SUCCESS,
    payload: response,
  });
  return apiCall;
};

// EDIT PRODUCT
const updateProduct = (formData = {}) => async (dispatch, getState, api) => {
  // Request
  dispatch({
    type: mystoreActionTypes.EDIT_PRODUCT_REQUEST,
  });

  // API call configuration
  let response;
  let apiCall;
  const requestOptions = {
    headers: {
      'Content-Type': 'application/json',
    },
  };
  const {
    hasShippingDetails = true,
    physical_properties,
    work_schedules,
    ...productData
  } = formData;
  try {
    // Save Shipping Properties
    const shippingPropertiesVerb = api.put;
    apiCall = await shippingPropertiesVerb(
      `/api/v1/market/mystore/products/${productData.slug}/properties/`,
      JSON.stringify({
        is_available: physical_properties.is_available,
        fabrication_time: Math.round(physical_properties.fabrication_time),
        size: hasShippingDetails ? physical_properties.size : 1,
        shipping_schedules: physical_properties?.shipping_schedules?.map(
          (s) => s.id,
        ),
        shipping: physical_properties?.shipping?.map((s) => s.slug),
        life_time: hasShippingDetails ? physical_properties.life_time : 0,
        weight: physical_properties.weight,
        is_fragile: physical_properties.is_fragile,
      }),
      requestOptions,
    );
    response = {
      ...response,
      physical_properties: apiCall.data,
      hasShippingDetails: true,
    };

    // Save Product Updates
    apiCall = await api.patch(
      `/api/v1/market/mystore/products/${productData.slug}/`,
      JSON.stringify({
        ...productData,
        currency: 'MXN',
      }),
      requestOptions,
    );
    response = {
      ...response,
      ...apiCall.data,
    };

    // Normalize shipping options values
    response.physical_properties.life_time = `${response.physical_properties.life_time}`;
    switch (response.physical_properties.size.display_name) {
      case 'Sobre':
        response.physical_properties.size = '1';
        break;
      case 'Pequeño':
        response.physical_properties.size = '2';
        break;
      case 'Mediano':
        response.physical_properties.size = '3';
        break;
      default:
    }
    switch (response.physical_properties.is_available.display_name) {
      case 'Disponible en Stock':
        response.physical_properties.is_available = '1';
        break;
      case 'Disponible Bajo Pedido':
        response.physical_properties.is_available = '2';
        break;
      case 'Disponible en Stock y Bajo Pedido':
        response.physical_properties.is_available = '3';
        break;
      default:
    }
    if (response.status.value === 'draft') {
      response.status = false;
    } else {
      response.status = true;
    }
    //
  } catch (error) {
    dispatch({
      type: mystoreActionTypes.EDIT_PRODUCT_FAILURE,
      error: true,
      payload: error,
    });
    throw error;
  }

  // if (
  //   !response.status
  // ) {
  //   await axios.post(
  //     '/tools/integrations/slack/product-category-request/', {
  //       id: productData.id,
  //       name: productData.name,
  //       store: productData.store.name,
  //       store_slug: productData.store.slug,
  //       slug: productData.slug
  //     }
  //   );
  // }

  dispatch(fetchProduct(productData.slug));

  // Dispatch Success
  dispatch({
    type: mystoreActionTypes.EDIT_PRODUCT_SUCCESS,
    payload: response,
  });
  return apiCall;
};

// FETCH PRODUCT ATTRIBUTES
const fetchProductAttributesList = (slug) => async (dispatch, getState, api) => {
  // Request
  dispatch({
    type: mystoreActionTypes.FETCH_PRODUCT_ATTRIBUTES_REQUEST,
  });

  // Make/Handle API call
  let response;
  try {
    response = await api.get(
      `api/v1/market/mystore/products/${slug}/attributes/menu/`,
    );
  } catch (error) {
    dispatch({
      type: mystoreActionTypes.FETCH_PRODUCT_ATTRIBUTES_FAILURE,
      error: true,
      payload: error,
    });
    throw error;
  }

  // Dispatch Success
  dispatch({
    type: mystoreActionTypes.FETCH_PRODUCT_ATTRIBUTES_SUCCESS,
    payload: response.data,
  });
  return response;
};

// FETCH ATTRIBUTES TYPES
const fetchAttributeTypes = (slug) => async (dispatch, getState, api) => {
  // Request
  dispatch({
    type: mystoreActionTypes.FETCH_ATTRIBUTE_TYPES_REQUEST,
  });

  // Make/Handle API call
  let response;
  try {
    response = await api.get('api/v1/market/products/attributes/');
  } catch (error) {
    dispatch({
      type: mystoreActionTypes.FETCH_ATTRIBUTE_TYPES_FAILURE,
      error: true,
      payload: error,
    });
    throw error;
  }

  // Dispatch Success
  dispatch({
    type: mystoreActionTypes.FETCH_ATTRIBUTE_TYPES_SUCCESS,
    payload: response.data,
  });
  return response;
};

// UPDATE PRODUCT ATTRIBUTES
const updateProductAttributes = ({ slug, attributes: data }) => async (
  dispatch,
  getState,
  api,
) => {
  // Request
  dispatch({
    type: mystoreActionTypes.UPDATE_ATTRIBUTES_REQUEST,
  });

  // Make/Handle API call
  let response;
  const requestOptions = {
    headers: {
      'Content-Type': 'application/json',
    },
  };
  try {
    response = await api.post(
      `api/v1/market/mystore/products/${slug}/attributes/`,
      JSON.stringify(data),
      requestOptions,
    );
    //
  } catch (error) {
    dispatch({
      type: mystoreActionTypes.UPDATE_ATTRIBUTES_FAILURE,
      error: true,
      payload: error,
    });
    throw error;
  }

  // Dispatch Success
  dispatch({
    type: mystoreActionTypes.UPDATE_ATTRIBUTES_SUCCESS,
    payload: response.data,
  });
  return response;
};

// UPLOAD IMAGES TO PRODUCT GALLERY
const addProductPhoto = (productData = {}) => async (dispatch, getState, api) => {
  // Dispatch Request
  dispatch({
    type: mystoreActionTypes.ADD_PRODUCT_IMAGE_REQUEST,
  });

  // API call configuration
  let response;
  const config = {
    headers: {
      'Content-Type': 'multipart/form-data',
    },
    onUploadProgress: (progressEvent) => {
      console.log(Math.round((progressEvent.loaded * 100) / progressEvent.total));
    },
  };

  // Make/Handle API call
  try {
    response = await api.post(
      `/api/v1/market/mystore/products/${productData.get('slug')}/photos/`,
      productData,
      config,
    );
    //
  } catch (error) {
    dispatch({
      type: mystoreActionTypes.ADD_PRODUCT_IMAGE_FAILURE,
      error: true,
      payload: error,
    });
    throw error;
  }

  // Dispatch Success
  dispatch({
    type: mystoreActionTypes.ADD_PRODUCT_IMAGE_SUCCESS,
    payload: response.data,
  });
  return response;
};

// DELETE PODUCT IMAGES
const deleteProductPhoto = (productData = {}) => async (dispatch, getState, api) => {
  // Dispatch Request
  dispatch({
    type: mystoreActionTypes.DELETE_PRODUCT_IMAGE_REQUEST,
  });

  // API call configuration
  let response;
  const config = {
    headers: {
      'Content-Type': 'multipart/form-data',
    },
  };

  // Make/Handle API call
  try {
    response = await api.delete(
      `/api/v1/market/mystore/products/${productData.slug}/photos/${productData.src}/`,
      productData,
      config,
    );
  } catch (error) {
    dispatch({
      type: mystoreActionTypes.DELETE_PRODUCT_IMAGE_FAILURE,
      error: true,
      payload: error,
    });
    throw error;
  }

  // Dispatch Success
  dispatch({
    type: mystoreActionTypes.DELETE_PRODUCT_IMAGE_SUCCESS,
    payload: productData.photo,
  });
  return response;
};

// UPDATE PODUCT IMAGE
const updateProductPhoto = (slug, file, order) => async (
  dispatch,
  getState,
  api,
) => {
  // Dispatch Request
  dispatch({
    type: mystoreActionTypes.UPDATE_PRODUCT_IMAGE_REQUEST,
  });

  // Make/Handle API call
  let response;
  try {
    response = await api.patch(
      `/api/v1/market/mystore/products/${slug}/photos/${file}/`,
      { order },
    );
    //
  } catch (error) {
    dispatch({
      type: mystoreActionTypes.UPDATE_PRODUCT_IMAGE_FAILURE,
      error: true,
      payload: error,
    });
    throw error;
  }

  // Dispatch Success
  dispatch({
    type: mystoreActionTypes.UPDATE_PRODUCT_IMAGE_SUCCESS,
    payload: response.status,
  });
  return response;
};

// DELETE PRODUCT
const deleteProduct = (slug = '') => async (dispatch, getState, api) => {
  // Request
  dispatch({
    type: mystoreActionTypes.DELETE_PRODUCT_REQUEST,
  });
  // Make/Handle API call
  let response;
  try {
    response = await api.delete(`/api/v1/market/mystore/products/${slug}/`);
  } catch (error) {
    dispatch({
      type: mystoreActionTypes.DELETE_PRODUCT_FAILURE,
      error: true,
      payload: error,
    });
    throw error;
  }

  // Dispatch Success
  dispatch({
    type: mystoreActionTypes.DELETE_PRODUCT_SUCCESS,
    payload: slug,
  });
  return response;
};

// GET PRODUCTS LIST
const fetchProductList = (searchQuery) => async (dispatch, getState, api) => {
  // Request
  dispatch({
    type: mystoreActionTypes.GET_PRODUCTS_REQUEST,
  });

  // Make/Handle API call
  let response;
  let query = '?ordering=-created_date&page_size=12';
  if (searchQuery !== '') {
    query = searchQuery;
  }

  try {
    response = await api.get(`/api/v1/market/mystore/products/?${searchQuery}`);
  } catch (error) {
    dispatch({
      type: mystoreActionTypes.GET_PRODUCTS_FAILURE,
      error: true,
      payload: error,
    });
    throw error;
  }

  // Dispatch Success
  dispatch({
    type: mystoreActionTypes.GET_PRODUCTS_SUCCESS,
    payload: response.data,
  });
  return response;
};

// GET SHIPPING PROPERTIES
const fetchMyStoreShippingOptions = (productSlug) => async (
  dispatch,
  getState,
  api,
) => {
  // Request
  dispatch({
    type: mystoreActionTypes.GET_SHIPPING_OPTIONS_REQUEST,
  });

  // Make/Handle API call
  let response;
  try {
    response = await api.options(
      `/api/v1/market/mystore/products/${productSlug}/properties/`,
    );
  } catch (error) {
    dispatch({
      type: mystoreActionTypes.GET_SHIPPING_OPTIONS_FAILURE,
      error: true,
      payload: error,
    });
    throw error;
  }

  // Dispatch Success
  dispatch({
    type: mystoreActionTypes.GET_SHIPPING_OPTIONS_SUCCESS,
    payload: response.data,
  });
  return response;
};

// GET MERCADO PAGO PAYMENT LINK
const getMercadoPagoPaymentLink = () => async (dispatch, getState, api) => {
  // Request
  dispatch({
    type: mystoreActionTypes.GET_PAYMENT_LINK_REQUEST,
  });

  // Make/Handle API call
  const response = await api.get('/api/v1/payment/mercadopago/account-link/');
  response.catch((error) => {
    dispatch({
      type: mystoreActionTypes.GET_PAYMENT_LINK_FAILURE,
      error: true,
      payload: error,
    });
    throw error;
  });

  // Dispatch Success
  dispatch({
    type: mystoreActionTypes.GET_PAYMENT_LINK_SUCCESS,
    payload: response.data,
  });
  return response;
};

// DELETE PAYMENT METHOD
const deletePaymentMethod = (paymentProvider) => async (dispatch, getState, api) => {
  // Request
  dispatch({
    type: mystoreActionTypes.DELETE_PAYMENT_METHOD_REQUEST,
  });

  // Make/Handle API call
  const response = await api.delete(`/api/v1/payment/${paymentProvider}/myaccount/`);
  response.catch((error) => {
    dispatch({
      type: mystoreActionTypes.DELETE_PAYMENT_METHOD_FAILURE,
      error: true,
      payload: error,
    });
    throw error;
  });

  // Dispatch Success
  dispatch({
    type: mystoreActionTypes.DELETE_PAYMENT_METHOD_SUCCESS,
    payload: response.data,
  });
  return response;
};

// GET FISCAL REGISTRIES
const getFiscalRegistries = (data) => async (dispatch, getState, api) => {
  // Request
  dispatch({
    type: mystoreActionTypes.GET_FISCAL_REGISTRIES_REQUEST,
  });

  // Make/Handle API call
  let response;
  try {
    response = await api.get('api/v1/billing/fiscal-registries/', data);
  } catch (error) {
    dispatch({
      type: mystoreActionTypes.GET_FISCAL_REGISTRIES_FAILURE,
      error: true,
      payload: error,
    });
    throw error;
  }

  // Dispatch Success
  dispatch({
    type: mystoreActionTypes.GET_FISCAL_REGISTRIES_SUCCESS,
    payload: response.data,
  });
  return response;
};

// ADD FISCAL DATA
const addFiscalData = (data) => async (dispatch, getState, api) => {
  // Request
  dispatch({
    type: mystoreActionTypes.ADD_FISCAL_DATA_REQUEST,
  });

  // Make/Handle API call
  let response;
  try {
    response = await api
      .post('/api/v1/billing/my-store/fiscal-data/', data)
      .catch((err) => {
        //Get info for Error 400
        response = err.response.data;
        return response;
      });
  } catch (error) {
    dispatch({
      type: mystoreActionTypes.ADD_FISCAL_DATA_FAILURE,
      error: true,
      payload: error,
    });
  }

  dispatch({
    type: mystoreActionTypes.ADD_FISCAL_DATA_SUCCESS,
    payload: response.data,
  });

  return response;
};

// ADD FISCAL DATA
const updateFiscalData = (data) => async (dispatch, getState, api) => {
  // Request
  dispatch({
    type: mystoreActionTypes.UPDATE_FISCAL_DATA_REQUEST,
  });

  // Make/Handle API call
  let response;
  try {
    response = await api
      .put('/api/v1/billing/my-store/fiscal-data/', data)
      .catch((err) => {
        //Get info for Error 400
        response = err.response.data;
        return response;
      });
  } catch (error) {
    dispatch({
      type: mystoreActionTypes.UPDATE_FISCAL_DATA_FAILURE,
      error: true,
      payload: error,
    });
  }

  // Dispatch Success improvements
  //   dispatch({
  //     type: mystoreActionTypes.SET_MYSTORE_IMPROVEMENTS,
  //     payload: { url: '', update: true }
  //   });

  // Dispatch Success
  dispatch({
    type: mystoreActionTypes.UPDATE_FISCAL_DATA_SUCCESS,
    payload: response,
  });

  return response;
};

// GET FISCAL DATA
const fetchFiscalData = (registryId) => async (dispatch, getState, api) => {
  // Request
  dispatch({
    type: mystoreActionTypes.FETCH_FISCAL_DATA_REQUEST,
  });

  // Make/Handle API call
  let response;
  try {
    response = await api.get('api/v1/billing/my-store/fiscal-data/');
  } catch (error) {
    dispatch({
      type: mystoreActionTypes.FETCH_FISCAL_DATA_FAILURE,
      error: true,
      payload: error,
    });
  }

  // Dispatch Success
  dispatch({
    type: mystoreActionTypes.FETCH_FISCAL_DATA_SUCCESS,
    payload: response.data,
  });
  return response;
};

// GET INVOICE OPTIONS
const getInvoiceOptions = () => async (dispatch, getState, api) => {
  // ¡¡¡TODO: Remove this line when
  // implementing this functionality on the main app.!!!
  //return;

  // Request
  dispatch({
    type: mystoreActionTypes.GET_INVOICE_OPTIONS_REQUEST,
  });

  // Make/Handle API call
  let response;
  try {
    response = await api.options('/api/v1/billing/my-store/fiscal-data/');
  } catch (error) {
    dispatch({
      type: mystoreActionTypes.GET_INVOICE_OPTIONS_FAILURE,
      error: true,
      payload: error,
    });
  }
  // Dispatch Success
  dispatch({
    type: mystoreActionTypes.GET_INVOICE_OPTIONS_SUCCESS,
    payload: {
      entities: response.data.actions.POST.entity_type.choices,
      states: response.data.actions.POST.state.choices,
      regimes: response.data.actions.POST.regime.choices,
    },
  });
  return response;
};

// ADD BANK ACCOUNT
const addBankAccount = (data) => async (dispatch, getState, api) => {
  // Request
  dispatch({
    type: mystoreActionTypes.ADD_BANK_ACCOUNT_REQUEST,
  });

  // Make/Handle API call
  let response;
  try {
    response = await api.post('api/v1/payment/bank-transfer/account/', data);
  } catch (error) {
    dispatch({
      type: mystoreActionTypes.ADD_BANK_ACCOUNT_FAILURE,
      error: true,
      payload: error,
    });
    throw error;
  }

  // Dispatch Success
  dispatch({
    type: mystoreActionTypes.ADD_BANK_ACCOUNT_SUCCESS,
    payload: response.data,
  });
  return response;
};

// GET BANK ACCOUNT
const fetchBankAccount = (data) => async (dispatch, getState, api) => {
  // Request
  dispatch({
    type: mystoreActionTypes.FETCH_BANK_ACCOUNT_REQUEST,
  });

  // Make/Handle API call
  let response;
  try {
    response = await api.get('api/v1/payment/bank-transfer/account/', data);
  } catch (error) {
    dispatch({
      type: mystoreActionTypes.FETCH_BANK_ACCOUNT_FAILURE,
      error: true,
      payload: error,
    });
  }

  // Dispatch Success
  dispatch({
    type: mystoreActionTypes.FETCH_BANK_ACCOUNT_SUCCESS,
    payload: response.data,
  });
  return response;
};

// GET BANK ACCOUNT OPTIONS
const getBankAccountOptions = () => async (dispatch, getState, api) => {
  // ¡¡¡TODO: Remove this line when
  // implementing this functionality on the main app.!!

  // Request
  dispatch({
    type: mystoreActionTypes.GET_BANK_ACCOUNT_OPTIONS_REQUEST,
  });

  // Make/Handle API call
  let response;
  try {
    response = await api.options('/api/v1/payment/bank-transfer/account/');
  } catch (error) {
    dispatch({
      type: mystoreActionTypes.GET_BANK_ACCOUNT_OPTIONS_FAILURE,
      error: true,
      payload: error,
    });
  }
  // Dispatch Success
  dispatch({
    type: mystoreActionTypes.GET_BANK_ACCOUNT_OPTIONS_SUCCESS,
    payload: {
      type_account: response.data.actions.POST.account_number_type.choices,
      banks: response.data.actions.POST.bank.choices,
    },
  });
  return response;
};

// GET MOVEMENTS (PAYMENTS)
const getStoreMovements = (query = '') => async (dispatch, getState, api) => {
  // Login Request
  dispatch({
    type: mystoreActionTypes.GET_MYSTORE_MOVEMENTS_REQUEST,
  });

  // Make/Handle AXIOS
  let response;
  try {
    response = await api.get(
      query !== ''
        ? `api/v1/payment/my-store/payouts/${query}`
        : 'api/v1/payment/my-store/payouts/?ordering=-created&page_size=200',
    );
  } catch (error) {
    dispatch({
      type: mystoreActionTypes.GET_MYSTORE_MOVEMENTS_FAILURE,
      error: true,
      payload: error,
    });
  }

  // Success
  dispatch({
    type: mystoreActionTypes.GET_MYSTORE_MOVEMENTS_SUCCESS,
    payload: response.data,
  });
  return response;
};

// ADD VACATIONS
const addMyStoreVacations = (userData = {}) => async (dispatch, getState, api) => {
  // Request
  dispatch({
    type: mystoreActionTypes.ADD_MYSTORE_VACATIONS_REQUEST,
  });

  // API call configuration
  let response;
  const requestOptions = {
    headers: {
      'Content-Type': 'application/json',
    },
  };

  // Make/Handle API call
  try {
    response = await api.post(
      '/api/v1/market/mystore/vacation-calendar/',
      JSON.stringify(userData),
      requestOptions,
    );
  } catch (error) {
    dispatch({
      type: mystoreActionTypes.ADD_MYSTORE_VACATIONS_FAILURE,
      error,
    });
    throw error;
  }

  // Dispatch Success
  dispatch({
    type: mystoreActionTypes.ADD_MYSTORE_VACATIONS_SUCCESS,
    payload: response.data,
  });
  return response;
};

//DELETE VACATIONS
const deleteMyStoreVacations = (rangeId) => async (dispatch, getState, api) => {
  // Request
  dispatch({
    type: mystoreActionTypes.DELETE_MYSTORE_VACATIONS_REQUEST,
  });

  // API call configuration
  let response;
  const requestOptions = {
    headers: {
      'Content-Type': 'application/json',
    },
  };

  // Make/Handle API call
  try {
    response = await api.delete(
      `/api/v1/market/mystore/vacation-calendar/${rangeId}/`,
      requestOptions,
    );
  } catch (error) {
    dispatch({
      type: mystoreActionTypes.DELETE_MYSTORE_VACATIONS_FAILURE,
      error,
    });
    throw error;
  }

  // Dispatch Success
  dispatch({
    type: mystoreActionTypes.DELETE_MYSTORE_VACATIONS_SUCCESS,
    payload: rangeId,
  });
  return response;
};

// GET VACATIONS
const getMyStoreVacations = () => async (dispatch, getState, api) => {
  // Request
  dispatch({
    type: mystoreActionTypes.GET_MYSTORE_VACATIONS_REQUEST,
  });

  // API call configuration
  let response;
  const requestOptions = {
    headers: {
      'Content-Type': 'application/json',
    },
  };

  // Make/Handle API call
  try {
    response = await api.get(
      '/api/v1/market/mystore/vacation-calendar/',
      requestOptions,
    );
  } catch (error) {
    dispatch({
      type: mystoreActionTypes.GET_MYSTORE_VACATIONS_FAILURE,
      error,
    });
    throw error;
  }

  // Dispatch Success
  dispatch({
    type: mystoreActionTypes.GET_MYSTORE_VACATIONS_SUCCESS,
    payload: response.data,
  });
  return response;
};

// UPDATE VACATIONS
const updateMyStoreVacations = (userData = {}) => async (
  dispatch,
  getState,
  api,
) => {
  // Request
  dispatch({
    type: mystoreActionTypes.UPDATE_MYSTORE_VACATIONS_REQUEST,
  });

  // API call configuration
  let response;
  const requestOptions = {
    headers: {
      'Content-Type': 'application/json',
    },
  };

  // Make/Handle API call
  try {
    response = await api.put(
      `/api/v1/market/mystore/vacation-calendar/${userData.range_id}/`,
      JSON.stringify(userData),
      requestOptions,
    );
  } catch (error) {
    dispatch({
      type: mystoreActionTypes.UPDATE_MYSTORE_VACATIONS_FAILURE,
      error,
    });
    throw error;
  }

  // Dispatch Success
  dispatch({
    type: mystoreActionTypes.UPDATE_MYSTORE_VACATIONS_SUCCESS,
    payload: response.data,
  });
  return response;
};

//SET_MYSTORE_IMPROVEMENTS

const setMyStoreImprovements = (improvements = {}) => async (dispatch, getState) => {
  // Request
  dispatch({
    type: mystoreActionTypes.SET_MYSTORE_IMPROVEMENTS,
    payload: improvements,
  });
};

// ADD REFERRER CODE (COOKIE)
const addReferrerCodeCookie = (fakeJson = {}) => async (dispatch) => {
  // Request
  dispatch({
    type: mystoreActionTypes.ADD_REFERRER_COOKIE_CODE_SUCCESS,
    payload: fakeJson,
  });
};

// ADD REFERRER CODE
const addReferrerCode = (referrerCode = {}) => async (dispatch, getState, api) => {
  // Request
  dispatch({
    type: mystoreActionTypes.ADD_REFERRER_CODE_REQUEST,
  });

  // Make/Handle API call
  let response;
  const requestOptions = {
    headers: {
      'Content-Type': 'application/json',
    },
  };
  try {
    response = await api.post(
      '/api/v1/marketing/enter-invite-code/',
      JSON.stringify(referrerCode),
      requestOptions,
    );
    // .catch(error => {
    //   dispatch({
    //     type: mystoreActionTypes.ADD_REFERRER_CODE_FAILURE,
    //     error: true,
    //     payload: error.response.data
    //   });
    //   throw error;
    // });
  } catch (error) {
    dispatch({
      type: mystoreActionTypes.ADD_REFERRER_CODE_FAILURE,
      error: true,
      payload: error,
    });
    throw error;
  }
  // Dispatch Success
  dispatch({
    type: mystoreActionTypes.ADD_REFERRER_CODE_SUCCESS,
    payload: response.data,
  });
  return response;
};

//GET MY ENTERED REFERRER CODES
const getEnteredReferrerCodes = () => async (dispatch, getState, api) => {
  // Login Request
  dispatch({
    type: mystoreActionTypes.GET_ENTERED_REFERRER_CODES_REQUEST,
  });

  // Make/Handle API call
  const response = await callApi(
    api.get,
    dispatch,
    '/api/v1/marketing/entered-invite-codes/',
    () => {
      dispatch({
        type: mystoreActionTypes.GET_ENTERED_REFERRER_CODES_FAILURE,
        error: true,
        payload: 'error',
      });
    },
  );

  // Success
  dispatch({
    type: mystoreActionTypes.GET_ENTERED_REFERRER_CODES_SUCCESS,
    payload: response.data,
  });
  return response;
};

// FETCH IVA LISR
const fetchIVAList = () => async (dispatch, getState, api) => {
  // Request
  dispatch({
    type: mystoreActionTypes.GET_LIST_IVA_REQUEST,
  });

  // Make/Handle API call
  let response;
  try {
    response = await api.get('/api/v1/market/available-iva/');
  } catch (error) {
    dispatch({
      type: mystoreActionTypes.GET_LIST_IVA_FAILURE,
      error: true,
      payload: error,
    });
    throw error;
  }

  // Dispatch Success
  dispatch({
    type: mystoreActionTypes.GET_LIST_IVA_SUCCESS,
    payload: response.data,
  });
  return response;
};

// GET PLANS
const getMyStorePlans = () => async (dispatch, getState, api) => {
  // Request
  dispatch({
    type: mystoreActionTypes.GET_MYSTORE_PLANS_REQUEST,
  });

  // API call configuration
  let response;
  const requestOptions = {
    headers: {
      'Content-Type': 'application/json',
    },
  };

  // Make/Handle API call
  try {
    response = await api.get('/api/v1/store-plans/plans/', requestOptions);
  } catch (error) {
    dispatch({
      type: mystoreActionTypes.GET_MYSTORE_PLANS_FAILURE,
      error,
    });
    throw error;
  }

  // Dispatch Success
  dispatch({
    type: mystoreActionTypes.GET_MYSTORE_PLANS_SUCCESS,
    payload: response.data,
  });
  return response;
};

// START FREE TRIAL
const startFreeTrial = () => async (dispatch, getState, api) => {
  // Request
  dispatch({
    type: mystoreActionTypes.ADD_MYSTORE_FREE_PLAN_REQUEST,
  });

  // API call configuration
  let response;
  const requestOptions = {
    headers: {
      'Content-Type': 'application/json',
    },
  };

  // Make/Handle API call
  try {
    response = await api.post('/api/v1/store-plans/try-trial/', requestOptions);
  } catch (error) {
    dispatch({
      type: mystoreActionTypes.ADD_MYSTORE_FREE_PLAN_FAILURE,
      error,
    });
    throw error;
  }

  // Dispatch Success
  dispatch({
    type: mystoreActionTypes.ADD_MYSTORE_FREE_PLAN_SUCCESS,
    payload: response.status,
  });
  return response;
};
// CANCEL PRO PLAN
const cancelPlan = () => async (dispatch, getState, api) => {
  // Request
  dispatch({
    type: mystoreActionTypes.CANCEL_MYSTORE_PLAN_REQUEST,
  });

  // API call configuration
  let response;
  const requestOptions = {
    headers: {
      'Content-Type': 'application/json',
    },
  };

  // Make/Handle API call
  try {
    response = await api.post(
      '/api/v1/store-plans/cancel-subscription/',
      requestOptions,
    );
  } catch (error) {
    dispatch({
      type: mystoreActionTypes.CANCEL_MYSTORE_PLAN_FAILURE,
      error,
    });
    throw error;
  }

  // Dispatch Success
  dispatch({
    type: mystoreActionTypes.CANCEL_MYSTORE_PLAN_SUCCESS,
    payload: response.status,
  });
  return response;
};
// PAYPAL SUBSCRIPTION
const subscribeToPayPal = (slug) => async (dispatch, getState, api) => {
  // Request
  dispatch({
    type: mystoreActionTypes.GET_MYSTORE_PAYPAL_SUBSCRIPTION_REQUEST,
  });

  return api
    .get(`/api/v1/store-plans/paypal-subscription/${slug}/`)
    .then((response) => {
      // Dispatch Success
      dispatch({
        type: mystoreActionTypes.GET_MYSTORE_PAYPAL_SUBSCRIPTION_SUCCESS,
        payload: response.data,
      });

      return response;
    })
    .catch((error) => {
      dispatch({
        type: mystoreActionTypes.GET_MYSTORE_PAYPAL_SUBSCRIPTION_FAILURE,
        error,
      });
      return error;
    });
};

// GET STRIPE PLANS
const getStripePlans = () => async (dispatch, getState, api) => {
  // Request
  dispatch({
    type: mystoreActionTypes.GET_STRIPE_PLANS_REQUEST,
  });

  return api
    .get('/api/v1/store-plans/stripe/plans/')
    .then((response) => {
      // Dispatch Success
      dispatch({
        type: mystoreActionTypes.GET_STRIPE_PLANS_SUCCESS,
        payload: response.data,
      });

      return response;
    })
    .catch((error) => {
      dispatch({
        type: mystoreActionTypes.GET_STRIPE_PLANS_FAILURE,
        error,
      });
      return error;
    });
};
// GET SAVED CARDS (STRIPE)
const getMySavedCards = () => async (dispatch, getState, api) => {
  // Request
  dispatch({
    type: mystoreActionTypes.GET_SAVED_CARDS_REQUEST,
  });

  return api
    .get('/api/v1/payment/stripe/my-cards/')
    .then((response) => {
      // Dispatch Success
      dispatch({
        type: mystoreActionTypes.GET_SAVED_CARDS_SUCCESS,
        payload: response.data,
      });

      return response;
    })
    .catch((error) => {
      dispatch({
        type: mystoreActionTypes.GET_SAVED_CARDS_FAILURE,
        error,
      });
      return error;
    });
};
// DELETE SAVED CARD (STRIPE)
const deleteSavedCard = (cardId) => async (dispatch, getState, api) => {
  // Request
  dispatch({
    type: mystoreActionTypes.DELETE_SAVED_CARD_REQUEST,
  });

  return api
    .delete(`/api/v1/payment/stripe/my-card/${cardId}/`)
    .then((response) => {
      // Dispatch Success
      dispatch({
        type: mystoreActionTypes.DELETE_SAVED_CARD_SUCCESS,
        payload: response.status,
      });

      return response;
    })
    .catch((error) => {
      dispatch({
        type: mystoreActionTypes.DELETE_SAVED_CARD_FAILURE,
        error,
      });
      return error;
    });
};
// CREATE STRIPE SUBSCRIPTION
const createStripeSubscription = (data) => async (dispatch, getState, api) => {
  const requestOptions = {
    headers: {
      'Content-Type': 'application/json',
    },
  };
  // Request
  dispatch({
    type: mystoreActionTypes.CREATE_STRIPE_SUBSCRIPTION_REQUEST,
  });

  return api
    .post('/api/v1/store-plans/stripe/subscription/', JSON.stringify(data))
    .then((response) => {
      // Dispatch Success
      dispatch({
        type: mystoreActionTypes.CREATE_STRIPE_SUBSCRIPTION_SUCCESS,
        payload: response.status,
      });

      return response;
    })
    .catch((error) => {
      dispatch({
        type: mystoreActionTypes.CREATE_STRIPE_SUBSCRIPTION_FAILURE,
        error,
      });
      return error;
    });
};
//SAVE CARD STRIPE
const saveCard = (data) => async (dispatch, getState, api) => {
  const requestOptions = {
    headers: {
      'Content-Type': 'application/json',
    },
  };
  // Request
  dispatch({
    type: mystoreActionTypes.SAVE_CARD_REQUEST,
  });

  return api
    .post('/api/v1/payment/stripe/save-card/', JSON.stringify(data))
    .then((response) => {
      // Dispatch Success
      dispatch({
        type: mystoreActionTypes.SAVE_CARD_SUCCESS,
        payload: response.status,
      });

      return response;
    })
    .catch((error) => {
      dispatch({
        type: mystoreActionTypes.SAVE_CARD_FAILURE,
        error,
      });
      return error;
    });
};
//SET DEFAULT SAVED CARD STRIPE
const setDefaultSavedCard = (id) => async (dispatch, getState, api) => {
  const requestOptions = {
    headers: {
      'Content-Type': 'application/json',
    },
  };
  // Request
  dispatch({
    type: mystoreActionTypes.SET_DEFAULT_SAVED_CARD_REQUEST,
  });

  return api
    .post('/api/v1/payment/stripe/set-default-card/', JSON.stringify(id))
    .then((response) => {
      // Dispatch Success
      dispatch({
        type: mystoreActionTypes.SET_DEFAULT_SAVED_CARD_SUCCESS,
        payload: response.status,
      });

      return response;
    })
    .catch((error) => {
      dispatch({
        type: mystoreActionTypes.SET_DEFAULT_SAVED_CARD_FAILURE,
        error,
      });
      return error;
    });
};
// GET USER PROFILE
const getUserProfile = () => async (dispatch, getState, api) => {
  // Request
  dispatch({
    type: mystoreActionTypes.GET_USER_PROFILE_REQUEST,
  });

  // API call configuration
  let response;
  const requestOptions = {
    headers: {
      'Content-Type': 'application/json',
    },
  };

  // Make/Handle API call
  try {
    response = await api.get('/api/v1/user/profile/', requestOptions);
  } catch (error) {
    dispatch({
      type: mystoreActionTypes.GET_USER_PROFILE_FAILURE,
      error,
    });
    throw error;
  }

  // Dispatch Success
  dispatch({
    type: mystoreActionTypes.GET_USER_PROFILE_SUCCESS,
    payload: response.data,
  });
  return response;
};
// UPDATE STORE STATUS
const updateStoreStatus = (status = '') => async (dispatch, getState, api) => {
  // Request
  dispatch({
    type: mystoreActionTypes.UPDATE_STORE_STATUS_REQUEST,
  });

  // API call configuration
  let response;
  const requestOptions = {
    headers: {
      'Content-Type': 'application/json',
    },
  };

  // Make/Handle API call
  try {
    response = await api.post(
      '/api/v1/market/mystore/change-status/',
      JSON.stringify(status),
    );
  } catch (error) {
    dispatch({
      type: mystoreActionTypes.UPDATE_STORE_STATUS_FAILURE,
      error,
    });
    throw error;
  }

  // Dispatch Success
  dispatch({
    type: mystoreActionTypes.UPDATE_STORE_STATUS_SUCCESS,
    payload: response,
  });

  return response;
};
//

// GET MYSTORE INTERESTS
const fetchMyStoreInterests = () => async (dispatch, getState, api) => {
  // Login Request
  dispatch({
    type: mystoreActionTypes.GET_MYSTORE_INTERESTS_REQUEST,
  });

  // Make/Handle API call
  const response = await callApi(
    api.get,
    dispatch,
    '/api/v1/market/interest/',
    () => {
      dispatch({
        type: mystoreActionTypes.GET_MYSTORE_INTERESTS_FAILURE,
        error: true,
        payload: { error: 'error' },
      });
    },
  );

  // Success
  dispatch({
    type: mystoreActionTypes.GET_MYSTORE_INTERESTS_SUCCESS,
    payload: response.data,
  });
  return response;
};
// EXPORT INDEX
export const myStoreActions = {
  fetchMyStore,
  fetchMyStoreSections,
  fetchMyStoreCategories,
  fetchMyStoreShippingOptions,
  addMyStoreSection,
  deleteSection,
  updateSection,
  fetchMyStoreFaqs,
  addMyStoreFaqs,
  updateFaq,
  deleteFaq,
  updateMyStore,
  uploadProfilePhoto,
  addStorePhoto,
  deleteStorePhoto,
  updateStorePhoto,
  getStoreMovements,
  addMyStoreVacations,
  getMyStoreVacations,
  deleteMyStoreVacations,
  updateMyStoreVacations,
  fetchMyStoreInterests,

  addProduct,
  updateProduct,
  updateProductAttributes,
  fetchProduct,
  fetchProductAttributesList,
  deleteProduct,
  addProductPhoto,
  deleteProductPhoto,
  updateProductPhoto,
  fetchProductList,
  fetchAttributeTypes,

  getMercadoPagoPaymentLink,
  deletePaymentMethod,
  getFiscalRegistries,
  addFiscalData,
  updateFiscalData,
  fetchFiscalData,
  getInvoiceOptions,
  addBankAccount,
  fetchBankAccount,
  getBankAccountOptions,

  setMyStoreImprovements,

  // Referral program
  addReferrerCode,
  addReferrerCodeCookie,
  getEnteredReferrerCodes,

  // Taxes
  fetchIVAList,

  //Plans
  getMyStorePlans,
  startFreeTrial,
  cancelPlan,
  subscribeToPayPal,
  getStripePlans,
  getMySavedCards,
  deleteSavedCard,
  createStripeSubscription,
  saveCard,
  setDefaultSavedCard,
  getUserProfile,

  updateStoreStatus,
};
