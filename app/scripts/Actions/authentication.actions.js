import { authenticationActionTypes } from '../Constants';

// SOCIAL LOGIN
const socialLogin = (accessToken = {}, network = '') => async (
  dispatch,
  getState,
  api,
) => {
  // Dipatch Request
  dispatch({
    type: authenticationActionTypes.SOCIAL_LOGIN_REQUEST,
    status: 'loading',
  });

  // Make/Handle API call
  let response;
  try {
    response = await api.post('/api/v1/auth/social-auth/facebook/', accessToken);
  } catch (error) {
    dispatch({
      type: authenticationActionTypes.SOCIAL_LOGIN_FAILURE,
      payload: error,
    });
    throw error;
  }

  // Dispatch Login Success
  dispatch({
    type: authenticationActionTypes.SOCIAL_LOGIN_SUCCESS,
    payload: response.data,
  });
  return response;
};

export const authenticationActions = {
  socialLogin,
};
