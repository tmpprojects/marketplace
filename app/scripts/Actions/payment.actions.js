import { paymentActionTypes } from '../Constants';
import config from '../../config';

// CREATE MERCADO PAGO CUSTOMER
const createMPCustomer = (cardToken) => async (dispatch, getState, api) => {
  // Request
  // dispatch({
  //     type: userActionTypes.PASSWORD_UPDATE_REQUEST,
  // })

  // API call configuration
  let response;
  const requestOptions = {
    headers: {
      'Content-Type': 'application/json',
    },
  };

  // Make/Handle API call
  try {
    response = await api.post(
      '/api/v1/payment/mercadopago/customers/cards/',
      JSON.stringify(cardToken),
      requestOptions,
    );
  } catch (error) {
    // dispatch({
    //     type: userActionTypes.PASSWORD_UPDATE_FAILURE,
    //     error: true,
    //     payload: error
    // })
    throw error;
  }

  // Dispatch Login Success
  // dispatch({
  //     type: paymentActionTypes.CREATE_MP_CUSTOMER,
  //     payload: response.data
  // })
  return response;
};

//
export const paymentActions = {
  createMPCustomer,
};
