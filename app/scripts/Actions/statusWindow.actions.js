export const statusWindowActions = {
  close,
  open,
};

export const statusWindowConstants = {
  CLOSE: 'STATUS_WINDOW_CLOSE',
  OPEN: 'STATUS_WINDOW_OPEN',
};

function open({ type: msg_type, message }) {
  return {
    type: statusWindowConstants.OPEN,
    msg_type: msg_type,
    message,
    shouldDisplay: true,
  };
}

function close() {
  return {
    type: statusWindowConstants.CLOSE,
    message: '',
    msg_type: '',
    shouldDisplay: false,
  };
}
