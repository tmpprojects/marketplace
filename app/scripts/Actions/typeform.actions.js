export const typeformConstanst = {
  OPEN: 'TYPEFORM_OPEN',
  CLOSE: 'TYPEFORM_CLOSE',
  ADD: 'TYPEFORM_ADD',
};

export const typeformActions = {
  open,
  close,
};

function open() {
  return {
    type: typeformConstanst.OPEN,
    isShown: true,
  };
}

function close() {
  return {
    type: typeformConstanst.CLOSE,
    isShown: false,
  };
}
