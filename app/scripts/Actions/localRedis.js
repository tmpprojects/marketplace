if (!process.env.CLIENT) {
  if (process.env.REDIS_ENABLED === 'true') {
    var redis = require('redis');
    var md5 = require('md5');
  }
  //console.log('SERVER');
}
import { LIMIT_CACHE } from './../Constants/config.constants';

const allowed = [
  'api/v1/market/stores/',
  'api/v1/market/products/',
  'api/v1/featured/market/stores/',
  'api/v1/featured/market/products/',
  'api/v1/market/categories/',
  'api/v1/inspire/articles/',
  'api/v1/inspire/articles-featured',
  'api/v1/inspire/tags/',
  'api/v1/featured/market/banner/',
  //'api/v1/search/',
];

export const LocalRedis = async (api, element, key, params = {}) => {
  //console.log('api',api.defaults.headers.cookie)
  const headersCookie = api.defaults.headers.cookie;
  const keyMD5 = `${element}${headersCookie}`;

  if (process.env.REDIS_ENABLED === 'false') {
    return api.get(element, params);
  }
  if (!process.env.CLIENT) {
    if (
      headersCookie !== '' &&
      headersCookie !== undefined &&
      headersCookie !== null
    ) {
      console.log('headersCookie *** md5 :', md5(keyMD5));
      // console.log('element',element)
    } else {
      return api.get(element, params);
    }
    // this creates a new client
    const client = redis.createClient();
    let urlReturnCache = false;
    for (const isAllowed of allowed) {
      if (element.includes(isAllowed)) {
        urlReturnCache = element;
        break;
      }
    }
    if (urlReturnCache) {
      // Cache generator
      //console.log('Connected redis');
      await client.on('connect', async () => {
        //console.log('Redis client connected');
        client.get(keyMD5, async (err, reply) => {
          if (!err && reply !== null && reply !== '') {
            console.log('Redis endpoint exist and return');
            return JSON.parse(reply);
          } else {
            //console.log('tratar de guardar', process.env.HOST_BACKEND + element);
            const parseResults = await api.get(
              process.env.HOST_BACKEND + element,
              params,
            );
            //console.log(parseResults);
            const setValue = await client.set(
              keyMD5,
              JSON.stringify(parseResults.data),
              'EX',
              process.env.REDIS_LIMIT_SEC,
            );
            console.log('Redis endpoint saved!');
            return parseResults;
          }
        });
      });
    } else {
      //If not allowed
      return api.get(element, params);
    }
    return await api.get(element, params);
  } else {
    //If client
    return api.get(element, params);
  }
};

export function ModifyCache() {
  if (process.env.CLIENT) {
    if (typeof Storage !== 'undefined') {
      // window.localStorage.clear();
      window.localStorage.setItem(1, 'login_user_timeout');
    }
  }
}
