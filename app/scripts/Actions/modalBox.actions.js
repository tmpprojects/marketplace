export const modalBoxActions = {
  success,
  error,
  close,
  open,
};

export const modalBoxConstants = {
  SUCCESS: 'ALERT_SUCCESS',
  ERROR: 'ALERT_ERROR',
  CLOSE: 'MODAL_CLOSE',
  OPEN: 'OPEN',
};

function success(message) {
  return { type: modalBoxConstants.SUCCESS, message };
}

function error(message) {
  return { type: modalBoxConstants.ERROR, message };
}

function open(windowType) {
  return { type: modalBoxConstants.OPEN, window: windowType };
}

function close() {
  return { type: modalBoxConstants.CLOSE, window: null };
}
