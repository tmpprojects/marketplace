import axios from 'axios';
import { default as reduxActions } from 'redux-form/lib/actions';

import { SHOPPING_CART_FORM_CONFIG } from '../Utils/shoppingCart/shoppingCartFormConfig';
import { shoppingCartActions } from './shoppingCart.actions';
import { userActionTypes } from '../Constants';
import config from '../../config';
const { change } = reduxActions;

// LOGIN
const login = (email, password) => async (dispatch, getState, api) => {
  // Login Request
  dispatch({
    type: userActionTypes.LOGIN_REQUEST,
  });

  // API call configuration
  let response;
  const requestOptions = {
    headers: {
      'Content-Type': 'application/json',
    },
  };

  // Make/Handle API call
  try {
    response = await api.post(
      '/api/v1/auth/login/cookie/',
      JSON.stringify({ email: email.trim(), password: password.trim() }),
      requestOptions,
    );
  } catch (error) {
    dispatch({
      type: userActionTypes.LOGIN_FAILURE,
      error: true,
      payload: error,
    });
    throw error;
  }

  // Dispatch Login Success
  dispatch({
    type: userActionTypes.LOGIN_SUCCESS,
    payload: response.data,
  });
  return response;
};

// LOGIN GUEST
const loginAsGuest = () => async (dispatch, getState, api) => {
  // Login Request
  dispatch({
    type: userActionTypes.SET_USER_AS_GUEST_REQUEST,
  });

  // Dispatch Login Success
  dispatch({
    type: userActionTypes.SET_USER_AS_GUEST_SUCCESS,
    payload: true,
  });
  return Promise.resolve();
};

// LOGOUT
const logout = () => async (dispatch, getState, api) => {
  dispatch({
    type: userActionTypes.LOGOUT_REQUEST,
  });

  let response;
  // Make/Handle API call
  try {
    response = await api.get('/api/v1/auth/logout/');
  } catch (error) {
    dispatch({
      type: userActionTypes.LOGOUT_FAILURE,
      payload: error.response,
    });
    throw error;
  }

  dispatch({
    type: userActionTypes.LOGOUT_SUCCESS,
  });
  return response;
};

// REGISTER USER
const register = (user) => async (dispatch, getState, api) => {
  // Login Request
  dispatch({
    type: userActionTypes.REGISTER_REQUEST,
  });

  // API call configuration
  let response;
  const requestOptions = {
    headers: {
      'Content-Type': 'application/json',
    },
  };
  const data = {
    first_name: user.first_name,
    last_name: user.last_name,
    email: user.email,
    password: user.password,
  };

  // Make/Handle API call
  try {
    response = await api.post('/api/v1/user/', JSON.stringify(data), requestOptions);
  } catch (error) {
    dispatch({
      type: userActionTypes.REGISTER_FAILURE,
      error: true,
      payload: error,
    });
    throw error;
  }

  // Dispatch Login Success
  dispatch({
    type: userActionTypes.REGISTER_SUCCESS,
    payload: response.data,
  });
  return response;
};

// GET USER STATUS
const getCurrentUser = () => async (dispatch, getState, api) => {
  // Login Request
  dispatch({
    type: userActionTypes.GET_CURRENT_USER_REQUEST,
  });

  // Make/Handle API call
  let response;
  try {
    // Validate if there is an active session
    const preresponse = await api.get('/api/v1/auth/user-is-authenticated/');
    if (preresponse.data['is-authenticated']) {
      response = await api.get('/api/v1/user/profile/');
    }
  } catch (error) {
    dispatch({
      type: userActionTypes.GET_CURRENT_USER_FAILURE,
      error: error.response.data,
    });
    throw error;
  }

  // Dispatch Login Success
  dispatch({
    type: userActionTypes.GET_CURRENT_USER_SUCCESS,
    payload: response.data,
  });

  dispatch(getUserCreditCards());

  return response;
};

// PASSWORD RECOVER
const passwordRecover = (userData) => async (dispatch, getState, api) => {
  // Login Request
  dispatch({
    type: userActionTypes.PASSWORD_RECOVER_REQUEST,
  });

  // API call configuration
  let response;
  const requestOptions = {
    headers: {
      'Content-Type': 'application/json',
    },
  };

  // Make/Handle API call
  try {
    response = await api.post(
      '/api/v1/user/forgot-password/',
      JSON.stringify(userData),
      requestOptions,
    );
  } catch (error) {
    dispatch({
      type: userActionTypes.PASSWORD_RECOVER_FAILURE,
      error: true,
      payload: error,
    });
    throw error;
  }

  // Dispatch Login Success
  dispatch({
    type: userActionTypes.PASSWORD_RECOVER_SUCCESS,
    payload: response.data,
  });
  return response;
};

// RESET PASSWORD
const resetPassword = (passwordData, key) => async (dispatch, getState, api) => {
  // Login Request
  dispatch({
    type: userActionTypes.PASSWORD_RECOVER_REQUEST,
  });

  // API call configuration
  let response;
  const requestOptions = {
    headers: {
      'Content-Type': 'application/json',
    },
  };

  // Make/Handle API call
  try {
    response = await api.post(
      `/api/v1/user/forgot-password/${key}/`,
      JSON.stringify(passwordData),
      requestOptions,
    );
  } catch (error) {
    dispatch({
      type: userActionTypes.PASSWORD_RECOVER_FAILURE,
      error: true,
      payload: error,
    });
    throw error;
  }

  // Dispatch Login Success
  dispatch({
    type: userActionTypes.PASSWORD_RECOVER_SUCCESS,
    payload: response.data,
  });
  return response;
};

// UPDATE PASSWORD
const updatePassword = (passwordData) => async (dispatch, getState, api) => {
  // Login Request
  dispatch({
    type: userActionTypes.PASSWORD_UPDATE_REQUEST,
  });

  // API call configuration
  let response;
  const requestOptions = {
    headers: {
      'Content-Type': 'application/json',
    },
  };

  // Make/Handle API call
  try {
    response = await api.put(
      '/api/v1/user/reset-password/',
      JSON.stringify(passwordData),
      requestOptions,
    );
  } catch (error) {
    dispatch({
      type: userActionTypes.PASSWORD_UPDATE_FAILURE,
      error: true,
      payload: error,
    });
    throw error;
  }

  // Dispatch Login Success
  dispatch({
    type: userActionTypes.PASSWORD_UPDATE_SUCCESS,
    payload: response.data,
  });
  return response;
};

// ADDRESS ACTIONS
const addAddress = (addressData) => async (dispatch, getState, api) => {
  dispatch({
    type: userActionTypes.ADDRESS_ADD_REQUEST,
  });

  // API call configuration
  let response;
  const requestOptions = {
    headers: {
      'Content-Type': 'application/json',
    },
  };

  // Make/Handle API call
  try {
    response = await api.post(
      '/api/v1/user/address/',
      JSON.stringify(addressData),
      requestOptions,
    );
  } catch (error) {
    dispatch({
      type: userActionTypes.ADDRESS_ADD_FAILURE,
      payload: error,
    });
    throw error;
  }

  // Dispatch Results
  dispatch({
    type: userActionTypes.ADDRESS_ADD_SUCCESS,
    payload: response.data,
  });

  // Update shipping address
  dispatch(shoppingCartActions.changeShippingAddress(response.data));

  // Return Promise
  return response;
};
const addGuestAddress = (addressData) => async (dispatch, getState, api) => {
  dispatch({
    type: userActionTypes.ADDRESS_ADD_REQUEST,
  });

  // Dispatch Results
  dispatch({
    type: userActionTypes.ADDRESS_ADD_SUCCESS,
    payload: addressData,
  });

  // Update shipping address
  dispatch(shoppingCartActions.changeShippingAddress(addressData));

  // Return Promise
  return Promise.resolve(addressData);
};

const deleteGuestAddress = () => (dispatch, getState) => {
  dispatch({
    type: userActionTypes.ADDRESS_GUEST_REMOVE_SUCCESS,
  });
};
const updateAddress = ({ uuid, ...addressData }) => async (
  dispatch,
  getState,
  api,
) => {
  dispatch({
    type: userActionTypes.ADDRESS_UPDATE_REQUEST,
  });

  // API call configuration
  let response;
  const requestOptions = {
    headers: {
      'Content-Type': 'application/json',
    },
  };

  // Make/Handle API call
  try {
    response = await api.patch(
      `/api/v1/user/address/${uuid}/`,
      JSON.stringify(addressData),
      requestOptions,
    );
  } catch (error) {
    dispatch({
      type: userActionTypes.ADDRESS_UPDATE_FAILURE,
      payload: error,
    });
    console.log('errior in update', error);
    throw error;
  }

  // Dispatch Response
  dispatch({
    type: userActionTypes.ADDRESS_UPDATE_SUCCESS,
    payload: response.data,
  });

  // Update shipping address
  dispatch(shoppingCartActions.changeShippingAddress(response.data));

  return response;
};
const deleteAddress = (addressId) => async (dispatch, getState, api) => {
  dispatch({
    type: userActionTypes.ADDRESS_REMOVE_REQUEST,
  });

  // API call configuration
  let response;
  const requestOptions = {
    headers: {
      'Content-Type': 'application/json',
    },
  };

  // Make/Handle API call
  try {
    response = await api.delete(
      `/api/v1/user/address/${addressId}/`,
      JSON.stringify({ address_uid: addressId }),
    );
  } catch (error) {
    dispatch({
      type: userActionTypes.ADDRESS_REMOVE_FAILURE,
      payload: error,
    });
    throw error;
  }

  // Dispatch Response
  dispatch({
    type: userActionTypes.ADDRESS_REMOVE_SUCCESS,
    payload: addressId,
  });
};
const listAddresses = () => async (dispatch, getState, api) => {
  dispatch({
    type: userActionTypes.ADDRESS_LIST_REQUEST,
  });

  // Make/Handle API call
  let response;
  try {
    response = await api.get('/api/v1/user/address/');
  } catch (error) {
    dispatch({
      type: userActionTypes.ADDRESS_LIST_FAILURE,
      payload: error,
    });
    throw error;
  }

  // Dispatch Response
  dispatch({
    type: userActionTypes.ADDRESS_LIST_SUCCESS,
    payload: response.data,
  });
};
const getAddressDetailsByZipCode = (zipCode) => async (dispatch, getState, api) => {
  dispatch({
    type: userActionTypes.GET_ADDRESS_DETAILS_BY_ZIPCODE_REQUEST,
  });

  // Make/Handle API call
  //let response;
  //try {
  const response = await api
    .get(`/api/v1/shipping/locations/${zipCode}`)
    .catch((error) =>
      dispatch({
        type: userActionTypes.GET_ADDRESS_DETAILS_BY_ZIPCODE_FAILURE,
        payload: error,
      }),
    );
  //throw error;
  //}

  // Dispatch Response
  dispatch({
    type: userActionTypes.GET_ADDRESS_DETAILS_BY_ZIPCODE_SUCCESS,
    payload: response.data,
  });

  return response;
};

// REVIEWS ACTIONS
const addReview = (productPurchase) => async (dispatch, getState, api) => {
  dispatch({
    type: userActionTypes.REVIEWS_ADD_REQUEST,
  });

  // API call configuration
  let response;
  const requestOptions = {
    headers: {
      'Content-Type': 'application/json',
    },
  };

  // Make/Handle API call
  try {
    response = await api.post(
      `/api/v1/reviews/my-reviews/order_product/${productPurchase.order_product}/`,
      JSON.stringify(productPurchase),
      requestOptions,
    );
  } catch (error) {
    dispatch({
      type: userActionTypes.REVIEWS_ADD_FAILURE,
      payload: error,
    });
    throw error;
  }

  // Sent event notification via Slack.
  axios
    .post('/tools/integrations/slack/approve-review-request/', {
      id: productPurchase.order.uid,
      name: productPurchase.product.name,
      store: productPurchase.product.store.name,
      comment: productPurchase.comment,
      rating: productPurchase.rating,
      image: productPurchase.product.photo.small || '',
    })
    .catch((error) => console.log('Slack Review Error: ', error.response));

  // Dispatch Results
  dispatch({
    type: userActionTypes.REVIEWS_ADD_SUCCESS,
    payload: response.data,
    currentReview: productPurchase,
  });

  // Return Promise
  return response;
};
const updateReview = ({ uuid, ...addressData }) => async (
  dispatch,
  getState,
  api,
) => {
  dispatch({
    type: userActionTypes.ADDRESS_UPDATE_REQUEST,
  });

  // API call configuration
  let response;
  const requestOptions = {
    headers: {
      'Content-Type': 'application/json',
    },
  };

  // Make/Handle API call
  try {
    response = await api.patch(
      `/api/v1/user/address/${uuid}/`,
      JSON.stringify(addressData),
      requestOptions,
    );
  } catch (error) {
    dispatch({
      type: userActionTypes.ADDRESS_UPDATE_FAILURE,
      payload: error,
    });
    throw error;
  }

  // Dispatch Response
  dispatch({
    type: userActionTypes.ADDRESS_UPDATE_SUCCESS,
    payload: response.data,
  });

  return response;
};
const deleteReview = (addressId) => async (dispatch, getState, api) => {
  dispatch({
    type: userActionTypes.ADDRESS_REMOVE_REQUEST,
  });

  // API call configuration
  let response;
  const requestOptions = {
    headers: {
      'Content-Type': 'application/json',
    },
  };

  // Make/Handle API call
  try {
    response = await api.delete(
      `/api/v1/user/address/${addressId}/`,
      JSON.stringify({ address_uid: addressId }),
    );
  } catch (error) {
    dispatch({
      type: userActionTypes.ADDRESS_REMOVE_FAILURE,
      payload: error,
    });
    throw error;
  }

  // Dispatch Response
  dispatch({
    type: userActionTypes.ADDRESS_REMOVE_SUCCESS,
    payload: addressId,
  });
};

// const listReviews = (queryString = '') => async (dispatch, getState, api) => {
//     dispatch({
//         type: userActionTypes.REVIEWS_LIST_REQUEST,
//     });

//     let query = '';
//     if (queryString !== '') {
//         query = queryString;
//     }
//     // Make/Handle API call
//     let response;
//     //try {
//         response = await api.get(`/api/v1/reviews/my-purchased-products/${query}`)
//         .catch(error =>
//             dispatch({
//                 type: userActionTypes.REVIEWS_LIST_FAILURE,
//                 error
//             })
//         );
//         //throw error;
//     //}

//     // Dispatch Response
//     dispatch({
//         type: userActionTypes.REVIEWS_LIST_SUCCESS,
//         payload: response.data
//     });
// };

const getUserReviews = (queryString = '', reviewType = 'pending') => async (
  dispatch,
  getState,
  api,
) => {
  dispatch({
    type: userActionTypes.GET_USER_REVIEWS_REQUEST,
  });

  let query = '';
  if (queryString !== '') {
    query = queryString;
  }
  // Make/Handle API call
  const response = await api
    .get(`/api/v1/reviews/my-purchased-products/${query}`)
    .catch((error) =>
      dispatch({
        type: userActionTypes.GET_USER_REVIEWS_FAILURE,
        error,
      }),
    );

  // Dispatch Response
  dispatch({
    type: userActionTypes.GET_USER_REVIEWS_SUCCESS,
    payload: response.data,
    reviewType,
  });

  return response;
};

// UPDATE USER PROFILE
const updateUserInfo = (formData) => async (dispatch, getState, api) => {
  // Dipatch Request
  dispatch({
    type: userActionTypes.UPDATE_INFO_REQUEST,
  });

  // Make/Handle API call
  let response;
  try {
    const { profile_photo, ...userInfo } = formData;
    response = await api.patch('/api/v1/user/profile/', formData);
  } catch (error) {
    dispatch({
      type: userActionTypes.UPDATE_INFO_FAILURE,
      error,
    });
    throw error;
  }

  // Dispatch Login Success
  dispatch({
    type: userActionTypes.UPDATE_INFO_SUCCESS,
    payload: response.data,
  });
  return response;
};

// CREDITCARDS
const addCreditCard = (token) => async (dispatch, getState, api) => {
  dispatch({
    type: userActionTypes.CREDITCARD_ADD_REQUEST,
  });

  // API call configuration
  let response;
  const requestOptions = {
    headers: {
      'Content-Type': 'application/json',
    },
  };

  // Make/Handle API call
  try {
    /**
     * This is the model we must send to the backend.
     * all fields are required.
     * {
        id | {int} card token
        payment_method | {string} visa|master|amex
        first_six_digits | {int} First 6 card´s digits
        last_four_digits | {int} Last 6 card´s digits
        expiration_year | {int} Expiration year (four digits)
        expiration_month | {int} Expiration month 1-12
      }
     */
    response = await api.post(
      '/api/v1/payment/mercadopago/customers/cards/',
      JSON.stringify(token),
      requestOptions,
    );
  } catch (error) {
    dispatch({
      type: userActionTypes.CREDITCARD_ADD_FAILURE,
      payload: error,
    });
    throw error;
  }

  // Dispatch Results
  dispatch({
    type: userActionTypes.CREDITCARD_ADD_SUCCESS,
    payload: response.data,
  });

  // Update user credit cards list
  //dispatch(getUserCreditCards());

  // Return Promise
  return response;
};

const removeCreditCard = (creditCard) => async (dispatch, getState, api) => {
  // Dispatch Login Success
  dispatch({
    type: userActionTypes.CREDITCARD_REMOVE_REQUEST,
    // number: creditCard.number
  });
  //return response
  let response;
  try {
    response = await api.delete(
      `/api/v1/payment/mercadopago/customers/cards/${creditCard}/`,
    );
  } catch (error) {
    dispatch({
      type: userActionTypes.CREDITCARD_ADD_FAILURE,
      error: true,
      payload: error,
    });
    throw error;
  }
  // Dispatch Success
  dispatch({
    type: userActionTypes.CREDITCARD_ADD_SUCCESS,
    payload: creditCard,
  });
  return response;
};

const getUserCreditCards = () => async (dispatch, getState, api) => {
  dispatch({
    type: userActionTypes.GET_USER_CREDIT_CARDS_REQUEST,
  });

  // Make/Handle API call
  let response;
  response = await api
    .get('api/v1/payment/mercadopago/customers/cards/')
    .catch((error) =>
      dispatch({
        type: userActionTypes.GET_USER_CREDIT_CARDS_FAILURE,
        error,
      }),
    );

  // Dispatch Response
  dispatch({
    type: userActionTypes.GET_USER_CREDIT_CARDS_SUCCESS,
    payload: response.data,
  });
};

const editCreditCard = (id) => async (dispatch, getState, api) => {
  // Request
  dispatch({
    type: userActionTypes.EDIT_CREDIT_CARD_REQUEST,
  });

  // API call configuration
  let response;
  const requestOptions = {
    headers: {
      'Content-Type': 'application/json',
    },
  };
  const data = {
    is_default: true,
  };

  // Make/Handle API call
  try {
    response = await api.put(
      `api/v1/payment/mercadopago/customers/cards/${id}/`,
      JSON.stringify(data),
      requestOptions,
    );
  } catch (error) {
    dispatch({
      type: userActionTypes.EDIT_CREDIT_CARD_FAILURE,
      error: true,
      payload: error,
    });
    throw error;
  }

  // Dispatch Success
  dispatch({
    type: userActionTypes.EDIT_CREDIT_CARD_SUCCESS,
    payload: response.data,
  });
  return response;
};
//
export const userActions = {
  login,
  loginAsGuest,
  logout,
  register,

  passwordRecover,
  resetPassword,
  updatePassword,
  getCurrentUser,
  updateUserInfo,

  addAddress,
  addGuestAddress,
  deleteGuestAddress,
  deleteAddress,
  updateAddress,
  listAddresses,
  getAddressDetailsByZipCode,

  addReview,
  deleteReview,
  updateReview,
  // listReviews,
  getUserReviews,

  addCreditCard,
  removeCreditCard,
  getUserCreditCards,
  editCreditCard,
};
