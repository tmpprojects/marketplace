import { inspireActionTypes } from '../Constants/inspire.constants';
import { LocalRedis, ModifyCache } from './localRedis';
import Cookies from 'universal-cookie';

const resetArticles = () => async (dispatch, getState, api) => {
  // Login Request
  dispatch({
    type: inspireActionTypes.RESET_ARTICLES,
  });
};

const fetchArticles = (queryData = {}) => async (dispatch, getState, api) => {
  // Login Request
  dispatch({
    type: inspireActionTypes.FETCH_ARTICLES_REQUEST,
  });

  // API call configuration
  let response;
  const data = {};
  ({
    page: data.page = 1,
    page_size: data.page_size = 20,
    // tags__name: data.tag_term = '',
    // search: data.search_term = ''
  } = queryData);
  // Make/Handle API call
  try {
    //response = await api.get('/api/v1/inspire/articles/', { params: data });
    response = await LocalRedis(
      api,
      '/api/v1/inspire/articles/',
      'FETCH_ARTICLES_REQUEST',
      { params: data },
    );
  } catch (error) {
    dispatch({
      type: inspireActionTypes.FETCH_ARTICLES_FAILURE,
      error: true,
      payload: error,
    });
    throw error;
  }

  // Dispatch Login Success
  dispatch({
    type: inspireActionTypes.FETCH_ARTICLES_SUCCESS,
    payload: response.data,
  });
  return response;
};

const fetchDetail = (slug = '') => async (dispatch, getState, api) => {
  // Login Request
  dispatch({
    type: inspireActionTypes.FETCH_DETAIL_REQUEST,
  });

  // API call configuration
  let response;

  // Make/Handle API call
  try {
    //response = await api.get(`/api/v1/inspire/articles/${slug}/`);
    response = await LocalRedis(
      api,
      `/api/v1/inspire/articles/${slug}/`,
      `FETCH_DETAIL_REQUEST___${slug}`,
    );
  } catch (error) {
    dispatch({
      type: inspireActionTypes.FETCH_DETAIL_FAILURE,
      error: true,
      payload: error,
    });
    throw error;
  }

  // Dispatch Login Success
  dispatch({
    type: inspireActionTypes.FETCH_DETAIL_SUCCESS,
    payload: response.data,
  });
  return response;
};

const fetchBanners = () => async (dispatch, getState, api) => {
  // Login Request
  dispatch({
    type: inspireActionTypes.FETCH_BANNERS_REQUEST,
  });

  // Make/Handle API call
  let response;
  try {
    //response = await api.get('/api/v1/inspire/articles-featured/?page=1');
    response = await LocalRedis(
      api,
      '/api/v1/inspire/articles-featured/?page=1',
      'FETCH_BANNERS_REQUEST',
    );
  } catch (error) {
    dispatch({
      type: inspireActionTypes.FETCH_BANNERS_FAILURE,
      error: true,
      payload: error,
    });
    throw error;
  }

  // Dispatch Login Success
  dispatch({
    type: inspireActionTypes.FETCH_BANNERS_SUCCESS,
    payload: response.data,
  });
  return response;
};

// ARTICLES SEARCH
const resetSearch = () => async (dispatch, getState, api) => {
  // Login Request
  dispatch({
    type: inspireActionTypes.RESET_SEARCH,
  });
};

const getSearchResults = (queryData) => async (dispatch, getState, api) => {
  // Request
  dispatch({
    type: inspireActionTypes.SEARCH_ARTICLES_REQUEST,
  });

  // API call configuration
  let response;
  const data = {};
  ({
    page: data.page = 1,
    page_size: data.page_size = 20,
    tag: data.tags__name = '',
    category: data.categories__name = '',
    search: data.search = '',
  } = queryData);

  try {
    response = await api.get('/api/v1/inspire/articles/', { params: data });
  } catch (error) {
    dispatch({
      type: inspireActionTypes.SEARCH_ARTICLES_FAILURE,
      error,
    });
    throw error;
  }

  // Dispatch Success
  dispatch({
    type: inspireActionTypes.SEARCH_ARTICLES_SUCCESS,
    payload: response.data,
  });
  return response;
};

// FETCH TAGS
const fetchTags = () => async (dispatch, getState, api) => {
  // Request
  dispatch({
    type: inspireActionTypes.GET_TAGS_REQUEST,
  });

  // API call configuration
  let response;
  try {
    //response = await api.get('/api/v1/inspire/tags/');
    response = await LocalRedis(api, '/api/v1/inspire/tags/', 'GET_TAGS_REQUEST');
  } catch (error) {
    dispatch({
      type: inspireActionTypes.GET_TAGS_FAILURE,
      error,
    });
    throw error;
  }

  // Dispatch Success
  dispatch({
    type: inspireActionTypes.GET_TAGS_SUCCESS,
    results: response.data,
  });
  return response;
};

export const inspireActions = {
  resetArticles,
  fetchArticles,
  fetchDetail,
  fetchBanners,
  fetchTags,
  resetSearch,
  getSearchResults,
};
