import { storeActionTypes, PRODUCT_TYPES } from '../Constants';
import { formatDateToUTC } from '../Utils/dateUtils';
import { formatShippingSchedules, formatWorkSchedules } from '.';

// Initial State
const loadingStatus = {
  idle: {
    error: false,
    loading: false,
  },
  loading: {
    error: false,
    loading: true,
  },
  error: {
    error: true,
    loading: false,
  },
};
const defautlState = {
  data: {},
  products: [],
  productDetail: {
    ...loadingStatus.loading,
  },
  sections: {
    ...loadingStatus.idle,
    sections: [],
  },
  reviews: {
    store: {
      ...loadingStatus.idle,
      current_store: '',
      count: 0,
      npages: 0,
      pages: [],
      next: null,
      previous: null,
      results: [],
    },
    product: {
      ...loadingStatus.idle,
      current_product: '',
      count: 0,
      npages: 0,
      pages: [],
      next: null,
      previous: null,
      results: [],
    },
  },
  pending_reviews: {
    store: {
      ...loadingStatus.idle,
      count: 0,
      npages: 0,
      pages: [],
      next: null,
      previous: null,
      results: [],
    },
    product: {
      ...loadingStatus.idle,
      count: 0,
      npages: 0,
      pages: [],
      next: null,
      previous: null,
      results: [],
    },
  },
};

// Export Reducers
export default function store(state = defautlState, action) {
  switch (action.type) {
    // GET STORE
    case storeActionTypes.GET_STORE_REQUEST: {
      return {
        ...state,
        data: {
          ...state.data,
          ...loadingStatus.loading,
        },
      };
    }
    case storeActionTypes.GET_STORE_SUCCESS: {
      const raw = action.payload;
      const socialLinks = [];
      const storeData = Object.keys(raw)
        .filter((key) => {
          // Exclude object members with keys begining with 'link_'.
          // (Social Networks Links)
          if (/^link_\w+$/i.test(key)) {
            // Fill Array with valid social links
            if (raw[key] !== '') {
              socialLinks.push({
                name: key.split(/link_/)[1],
                link: raw[key],
              });
            }
            return false;
          }
          return true;

          // Create new Object from filtered array
        })
        .reduce((obj, key) => {
          obj[key] = raw[key];
          return obj;
        }, {});

      // Add social links array to store data object
      storeData.social_links = socialLinks;

      return {
        ...state,
        data: {
          loaded: true,
          ...storeData,
          ...loadingStatus.idle,
        },
      };
    }

    case storeActionTypes.GET_STORE_FAILURE:
      return {
        ...state,
        data: {
          ...state.data,
          ...loadingStatus.error,
        },
      };

    // STORE CREATE
    case storeActionTypes.CREATE_REQUEST:
      return {
        ...state,
        data: {
          ...state.data,
          ...loadingStatus.loading,
        },
      };
    case storeActionTypes.CREATE_SUCCESS:
      return {
        ...state,
        data: {
          ...action.payload,
          ...loadingStatus.loading,
        },
      };
    case storeActionTypes.CREATE_FAILURE:
      return {
        ...state,
        data: {
          ...state.data,
          ...loadingStatus.error,
        },
      };

    // FETCH PRODUCTS
    case storeActionTypes.FETCH_PRODUCTS_REQUEST:
      return {
        ...state,
        products: {
          loadingStatus: {
            error: false,
            loading: true,
          },
        },
      };
    case storeActionTypes.FETCH_PRODUCTS_SUCCESS:
      return {
        ...state,
        products: {
          ...action.payload,

          loadingStatus: {
            error: false,
            loading: false,
          },
        },
      };
    case storeActionTypes.FETCH_PRODUCTS_FAILURE:
      return {
        ...state,
        products: {
          loadingStatus: {
            error: true,
            loading: false,
          },
        },
      };

    // FETCH PRODUCTS DETAIL
    case storeActionTypes.FETCH_PRODUCT_DETAIL_REQUEST:
      return {
        ...state,
        productDetail: {
          loaded: false,
          ...loadingStatus.loading,
        },
      };
    case storeActionTypes.FETCH_PRODUCT_DETAIL_SUCCESS:
      if (action.payload.data) {
        return {
          ...state,
          productDetail: {
            loaded: true,
            ...action.payload.data,
            ...loadingStatus.idle,
          },
        };
      } else {
        return {
          ...state,
          productDetail: {},
        };
      }
    case storeActionTypes.FETCH_PRODUCT_DETAIL_FAILURE:
      return {
        ...state,
        productDetail: {
          loaded: false,
          ...loadingStatus.error,
          error: action.payload,
        },
      };

    // FETCH STORE SECTIONS
    case storeActionTypes.FETCH_STORE_SECTIONS_REQUEST:
      return {
        ...state,
        sections: {
          sections: [],
          ...loadingStatus.loading,
        },
      };
    case storeActionTypes.FETCH_STORE_SECTIONS_SUCCESS:
      return {
        ...state,
        sections: {
          sections: action.payload,
          ...loadingStatus.idle,
        },
      };
    case storeActionTypes.FETCH_STORE_SECTIONS_FAILURE:
      return {
        ...state,
        sections: {
          ...loadingStatus.error,
        },
      };

    // GET ALL REVIEWS (APPROVED) FROM STORE
    case storeActionTypes.GET_STORE_REVIEWS_REQUEST:
      return {
        ...state,
      };
    case storeActionTypes.GET_STORE_REVIEWS_SUCCESS:
      let storeReviews = [...action.payload.results];

      //In case are more reviews from the same store
      //Add more reviews (not repeated) to the current reviews array
      if (state.reviews.store.current_store === action.store) {
        const newStoreReviews = storeReviews.filter(
          (r) =>
            state.reviews.store.results.findIndex((sr) => sr.id === r.id) === -1,
        );
        storeReviews = [...state.reviews.store.results, ...newStoreReviews];
      }

      return {
        ...state,
        reviews: {
          store: {
            ...action.payload,
            current_store: action.store,
            results: storeReviews,
          },
          product: {
            ...state.reviews.product,
          },
        },
      };
    case storeActionTypes.GET_STORE_REVIEWS_FAILURE:
      return {
        ...state,
      };

    //GET PENDING REVIEWS OF USER FROM STORE
    case storeActionTypes.GET_PENDING_STORE_REVIEWS_REQUEST:
      return {
        ...state,
      };
    case storeActionTypes.GET_PENDING_STORE_REVIEWS_SUCCESS:
      return {
        ...state,
        pending_reviews: {
          ...state.pending_reviews,
          store: {
            ...state.pending_reviews.store,
            ...action.payload,
          },
        },
      };
    case storeActionTypes.GET_PENDING_STORE_REVIEWS_FAILURE:
      return {
        ...state,
      };

    // GET ALL REVIEWS (APPROVED) FROM PRODUCT
    case storeActionTypes.GET_PRODUCT_REVIEWS_REQUEST:
      return {
        ...state,
      };

    case storeActionTypes.GET_PRODUCT_REVIEWS_SUCCESS:
      let productReviews = [...action.payload.results];

      //In case are more reviews from the same product
      if (state.reviews.product.current_product === action.product) {
        //Add more reviews (not repeated) to the current reviews array
        const newProductReviews = productReviews.filter(
          (r) =>
            state.reviews.product.results.findIndex((pr) => pr.id === r.id) === -1,
        );
        productReviews = [...state.reviews.product.results, ...newProductReviews];
      }

      return {
        ...state,
        reviews: {
          product: {
            ...action.payload,
            current_product: action.product,
            results: productReviews,
          },
          store: {
            ...state.reviews.store,
          },
        },
      };

    case storeActionTypes.GET_PRODUCT_REVIEWS_FAILURE:
      return {
        ...state,
      };

    //GET PENDING REVIEWS OF USER FROM STORE
    case storeActionTypes.GET_PENDING_PRODUCT_REVIEWS_REQUEST:
      return {
        ...state,
      };
    case storeActionTypes.GET_PENDING_PRODUCT_REVIEWS_SUCCESS:
      return {
        ...state,
        pending_reviews: {
          ...state.pending_reviews,
          product: {
            ...state.pending_reviews.product,
            ...action.payload,
          },
        },
      };
    case storeActionTypes.GET_PENDING_PRODUCT_REVIEWS_FAILURE:
      return {
        ...state,
      };

    // DEFAULT
    default:
      return state;
  }
}

/*---------------------------------------------------
    SELECTORS
---------------------------------------------------*/
export const getProductDetail = ({ store }) => {
  const { productDetail } = store;

  if (!productDetail.loaded) {
    return {
      loading: true,
      ...productDetail,
    };
  }

  let customProps = {};
  if (productDetail.product_type.value === PRODUCT_TYPES.PHYSICAL) {
    const minimumShippingDate = formatDateToUTC(
      new Date(productDetail.physical_properties.minimum_shipping_date),
    );
    const maximumShippingDate = formatDateToUTC(
      new Date(productDetail.physical_properties.maximum_shipping_date),
    );

    customProps = {
      physical_properties: {
        ...productDetail.physical_properties,
        maximum_shipping_date: maximumShippingDate,
        minimum_shipping_date: minimumShippingDate,
        shipping_schedules: formatShippingSchedules(
          productDetail.physical_properties.shipping_schedules,
        ),
      },
      productMadeOnDemand:
        productDetail.physical_properties.is_available.value === 2,
    };
  }

  return {
    loading: false,
    ...productDetail,
    work_schedules: formatWorkSchedules(productDetail.work_schedules),
    ...customProps,
  };
};
export const getStoreDetail = ({ store }) => {
  const { data: storeDetail } = store;

  return {
    loading: storeDetail.loaded || true,
    ...storeDetail,
    work_schedules: !storeDetail.loaded
      ? []
      : formatWorkSchedules(storeDetail.work_schedules),
  };
};
