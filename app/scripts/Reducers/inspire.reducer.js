import { inspireActionTypes } from '../Constants/inspire.constants';

const idleLoadStatus = {
  loading: false,
  error: null,
};
const defaultState = {
  articles: {},
  single: {
    loading: true,
    error: false,
  },
  banners: [],
  search: {
    ...idleLoadStatus,
  },
  tags: {
    ...idleLoadStatus,
  },
  error: null,
};

export function inspire(state = defaultState, action) {
  switch (action.type) {
    // FETCH ARTICLES
    case inspireActionTypes.RESET_ARTICLES:
      return {
        ...state,
        articles: {},
      };
    case inspireActionTypes.FETCH_ARTICLES_REQUEST:
      return {
        ...state,
      };
    case inspireActionTypes.FETCH_ARTICLES_SUCCESS: {
      let results = [...action.payload.results];
      if (state.articles.results) {
        results = [...state.articles.results, ...results];
      }

      return {
        ...state,
        articles: {
          ...action.payload,
          results,
        },
      };
    }
    case inspireActionTypes.FETCH_ARTICLES_FAILURE:
      return {
        ...state,
        error: action.payload,
      };

    // FETCH ARTICLE DETAIL
    case inspireActionTypes.FETCH_DETAIL_REQUEST:
      return {
        ...state,
        single: {
          error: false,
          loading: true,
        },
      };
    case inspireActionTypes.FETCH_DETAIL_SUCCESS:
      return {
        ...state,
        single: {
          ...idleLoadStatus,
          ...action.payload,
        },
      };
    case inspireActionTypes.FETCH_DETAIL_FAILURE:
      return {
        ...state,
        single: {
          error: action.payload,
          loading: false,
        },
      };

    // FETCH BANNERS
    case inspireActionTypes.FETCH_BANNERS_REQUEST:
      return {
        ...state,
      };
    case inspireActionTypes.FETCH_BANNERS_SUCCESS:
      return {
        ...state,
        banners: action.payload,
      };
    case inspireActionTypes.FETCH_BANNERS_FAILURE:
      return {
        ...state,
        error: action.payload,
      };

    // FETCH SEARCH RESULTS
    case inspireActionTypes.RESET_SEARCH:
      return {
        ...state,
        search: {},
      };
    case inspireActionTypes.SEARCH_ARTICLES_REQUEST:
      return {
        ...state,
        search: {
          ...state.search,
          loading: true,
          error: null,
        },
      };
    case inspireActionTypes.SEARCH_ARTICLES_SUCCESS: {
      let searchResults = [...action.payload.results];
      if (state.search.results) {
        searchResults = [...state.search.results, ...searchResults];
      }

      return {
        ...state,
        search: {
          ...idleLoadStatus,
          ...action.payload,
          results: searchResults,
        },
      };
    }
    case inspireActionTypes.SEARCH_ARTICLES_FAILURE:
      return {
        ...state,
        search: {
          ...state.search,
          loading: false,
          error: action.error,
        },
      };

    // FETCH TAGS
    case inspireActionTypes.GET_TAGS_REQUEST:
      return {
        ...state,
        tags: {
          ...state.tags,
          loading: true,
          error: null,
        },
      };
    case inspireActionTypes.GET_TAGS_SUCCESS:
      return {
        ...state,
        tags: {
          ...idleLoadStatus,
          results: action.results,
        },
      };
    case inspireActionTypes.GET_TAGS_FAILURE:
      return {
        ...state,
        tags: {
          ...state.tags,
          loading: false,
          error: action.error,
        },
      };

    // DEFAULT
    default:
      return state;
  }
}
