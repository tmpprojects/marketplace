import { statusWindowConstants } from '../Actions/statusWindow.actions';

const defaultState = {
  type: '',
  message: '',
  shouldDisplay: false,
};
export function statusWindow(state = defaultState, action) {
  switch (action.type) {
    case statusWindowConstants.CLOSE:
      return {
        type: action.msg_type,
        message: action.message,
        shouldDisplay: false,
      };
    case statusWindowConstants.OPEN:
      return {
        type: action.msg_type,
        message: action.message,
        shouldDisplay: true,
      };

    default:
      return state;
  }
}
