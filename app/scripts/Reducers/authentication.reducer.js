import { authenticationActionTypes, userActionTypes } from '../Constants';

const initialState = {
  isLogged: false,
};

export function authentication(state = initialState, action) {
  switch (action.type) {
    case userActionTypes.LOGIN_REQUEST:
      return {
        ...state,
        isLogged: false,
      };
    case userActionTypes.LOGIN_SUCCESS:
      return {
        ...state,
        isLogged: true,
      };
    case userActionTypes.LOGIN_FAILURE:
      return {
        ...state,
        isLogged: false,
      };

    ///////

    case userActionTypes.LOGIN_GUEST_REQUEST:
      console.log('LOGIN_GUEST_REQUEST <<-- ');
      return {
        ...state,
        isLogged: true,
      };
    case userActionTypes.LOGIN_GUEST_SUCCESS:
      console.log('LOGIN_GUEST_SUCCESS <<-- ');
      return {
        ...state,
        isLogged: true,
      };
    case userActionTypes.LOGIN_GUEST_FAILURE:
      return {
        ...state,
        isLogged: false,
      };

    /////////

    case userActionTypes.LOGOUT_REQUEST:
      return state;
    case userActionTypes.LOGOUT_SUCCESS:
      return initialState;
    case userActionTypes.LOGOUT_FAILURE:
      return state;

    // SOCIAL LOGIN
    case authenticationActionTypes.SOCIAL_LOGIN_REQUEST:
      return {
        ...state,
      };
    case authenticationActionTypes.SOCIAL_LOGIN_SUCCESS:
      return {
        ...state,
        isLogged: true,
        user: {
          ...state.user,
          ...action.payload,
        },
      };
    case authenticationActionTypes.SOCIAL_LOGIN_FAILURE:
      return {
        ...state,
        isLogged: false,
        user: {},
      };

    default:
      return state;
  }
}
