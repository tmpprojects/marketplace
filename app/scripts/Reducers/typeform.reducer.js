import { typeformConstanst } from '../Actions/typeform.actions';

const defaultState = {
  type: '',
  isShown: false,
};

export function typeform(state = defaultState, action) {
  switch (action.type) {
    case typeformConstanst.OPEN:
      return {
        type: action.type,
        isShown: action.isShown,
      };
    case typeformConstanst.CLOSE:
      return {
        type: action.type,
        isShown: action.isShown,
      };
    default:
      return state;
  }
}
