import { landingActionTypes } from '../Constants/landing.constants';

const idleLoadStatus = {
  loading: false,
  error: null,
};
const defaultState = {
  loading: true,
  error: null,
  allData: {},
  landingsList: {
    results: [],
    loading: true,
    error: null,
    previouslyLoaded: false,
  },
};

export function landing(state = defaultState, action) {
  switch (action.type) {
    case landingActionTypes.FETCH_DATA_REQUEST:
      return {
        ...state,
        // loading: true,
        // error: false
      };
    case landingActionTypes.FETCH_DATA_SUCCESS:
      {
        const wholeData = action.payload;
        if (wholeData) {
          return {
            ...state,
            ...idleLoadStatus,
            allData: wholeData,
          };
        }
      }
      break;
    case landingActionTypes.FETCH_DATA_FAILURE: {
      return {
        ...state,
        ...idleLoadStatus,
        error: action.payload,
      };
    }
    case landingActionTypes.FETCH_LIST_REQUEST:
      return {
        ...state,
        landingsList: {
          ...state.landingsList,
          loading: true,
          error: null,
        },
      };
    case landingActionTypes.FETCH_LIST_SUCCESS:
      {
        const wholeData = action.payload;
        if (wholeData) {
          return {
            ...state,
            ...idleLoadStatus,
            landingsList: {
              results: wholeData,
              ...idleLoadStatus,
              previouslyLoaded: true,
            },
          };
        }
      }
      break;
    case landingActionTypes.FETCH_LIST_FAILURE: {
      return {
        ...state,
        landingsList: {
          ...state.landingsList,
          ...idleLoadStatus,
          error: action.payload,
        },
      };
    }
    default:
      return state;
  }
}
