import { formatShippingSchedules } from '.';
import {
  appActionTypes,
  APP_SECTIONS,
  SHIPPING_METHODS,
  PRODUCT_TYPES,
} from '../Constants';

// Default States.
const loadingStatus = {
  previouslyLoaded: false,
  idle: {
    error: false,
    loading: false,
  },
  loading: {
    error: false,
    loading: true,
  },
  error: {
    error: true,
    loading: false,
  },
};

// Initial State
const initialState = {
  section: APP_SECTIONS.MARKET,
  pickupSchedules: { schedules: [], ...loadingStatus.loading },
  shippingMethods: { results: [], ...loadingStatus.loading },
  shippingZones: [],
  shippingMethodsMarket: [],
  paymentMethods: { results: [], ...loadingStatus.loading },
  marketBanners: {
    banners: [],
    ...loadingStatus.loading,
  },
  marketCategories: {
    categories: [],
    ...loadingStatus.loading,
  },
  marketInterests: {
    interests: [],
    ...loadingStatus.loading,
  },
  searchResults: {
    stores: {
      results: [],
      ...loadingStatus.loading,
    },
    products: {
      results: [],
      ...loadingStatus.loading,
    },
    articles: {
      results: [],
      ...loadingStatus.loading,
    },
  },
  categoryResults: {
    results: [],
    category: '',
    ...loadingStatus.loading,
  },
  storesList: {
    results: [],
    ...loadingStatus.loading,
  },
  featuredProducts: {
    products: [],
    ...loadingStatus.loading,
  },
  featuredStores: {
    stores: [],
    ...loadingStatus.loading,
  },
  randomProducts: {
    products: [],
    ...loadingStatus.loading,
  },
  allProducts: {
    products: [],
    ...loadingStatus.loading,
  },
  message: '',
  jobOpenings: {
    results: [],
    ...loadingStatus.loading,
  },
  storesListVisa: {},
  storesBazarList: {},
  prohibitedWords: {
    ...loadingStatus.loading,
    list: [],
  },
};

// Reducer Constructor
export function app(state = initialState, action) {
  switch (action.type) {
    // VERSION MOBILE
    case appActionTypes.IS_MOBILE_SUCCESS:
      return {
        ...state,
        isMobile: action.flag,
      };
    // SET APP SECTION
    case appActionTypes.SET_APP_SECTION:
      return {
        ...state,
        section: action.section,
      };
    // SHOW/HIDE SEARCH BAR
    case appActionTypes.OPEN_SEARCH_BAR:
      return {
        ...state,
        openSearchBar: true,
      };
    case appActionTypes.CLOSE_SEARCH_BAR:
      return {
        ...state,
        openSearchBar: false,
      };

    // PICKUP SCHEDULES
    case appActionTypes.GET_PICKUP_SCHEDULES_REQUEST:
      return {
        ...state,
        pickupSchedules: { ...state.pickupSchedules, ...loadingStatus.loading },
      };
    case appActionTypes.GET_PICKUP_SCHEDULES_SUCCESS:
      return {
        ...state,
        pickupSchedules: {
          schedules: formatShippingSchedules(action.payload),
          ...loadingStatus.idle,
          previouslyLoaded: true,
        },
      };
    case appActionTypes.GET_PICKUP_SCHEDULES_FAILURE:
      return {
        ...state,
        pickupSchedules: { ...state.pickupSchedules, ...loadingStatus.error },
      };

    // SHIPPING METHODS
    case appActionTypes.GET_SHIPPING_METHODS_REQUEST:
      return {
        ...state,
        shippingMethods: {
          ...state.shippingMethods,
          ...loadingStatus.loading,
        },
      };
    case appActionTypes.GET_SHIPPING_METHODS_SUCCESS:
      return {
        ...state,
        shippingMethods: {
          ...state.shippingMethods,
          results: action.shippingMethods.sort((a, b) => {
            // Map results against config settings
            const methods = [
              Object.keys(SHIPPING_METHODS).filter(
                (method) => SHIPPING_METHODS[method].slug === a.slug,
              ),
              Object.keys(SHIPPING_METHODS).filter(
                (method) => SHIPPING_METHODS[method].slug === b.slug,
              ),
            ];

            // Check if any of the shipping methods to compare is empty
            const hasEmptyShippingMethod = methods.reduce((c, d) => {
              if (!c) {
                return d.filter((s) => s.length);
              }
              return c;
            }, false);

            // Sort items
            if (!hasEmptyShippingMethod) {
              return SHIPPING_METHODS[methods[0]].order >
                SHIPPING_METHODS[methods[1]].order
                ? 1
                : SHIPPING_METHODS[methods[0]].order <
                  SHIPPING_METHODS[methods[1]].order
                ? -1
                : 0;
            }
            return -1;
          }),
          ...loadingStatus.idle,
          previouslyLoaded: true,
        },
      };
    case appActionTypes.GET_SHIPPING_METHODS_FAILURE:
      return {
        ...state,
        shippingMethods: { ...state.shippingMethods, error: action.error },
      };

    // SHIPPING METHODS MARKET
    case appActionTypes.GET_SHIPPING_METHODS_MARKET_REQUEST:
      return {
        ...state,
      };
    case appActionTypes.GET_SHIPPING_METHODS_MARKET_SUCCESS:
      return {
        ...state,
        shippingMethodsMarket: action.shippingMethodsMarket,
      };
    case appActionTypes.GET_SHIPPING_METHODS_MARKET_FAILURE:
      return {
        ...state,
        error: action.error,
      };

    // PAYMENT METHODS
    case appActionTypes.GET_PAYMENT_METHODS_REQUEST:
      return {
        ...state,
        paymentMethods: {
          ...state.paymentMethods,
          ...loadingStatus.loading,
        },
      };
    case appActionTypes.GET_PAYMENT_METHODS_SUCCESS:
      return {
        ...state,
        paymentMethods: {
          results: action.paymentMethods,
          ...loadingStatus.idle,
          previouslyLoaded: true,
        },
      };
    case appActionTypes.GET_PAYMENT_METHODS_FAILURE:
      return {
        ...state,
        paymentMethods: {
          ...state.paymentMethods,
          ...loadingStatus.error,
          error: action.error,
        },
      };

    // HOMEPAGE BANNERS
    case appActionTypes.FETCH_MARKET_BANNERS_REQUEST:
      return {
        ...state,
        marketBanners: {
          ...state.marketBanners,
          ...loadingStatus.loading,
        },
      };
    case appActionTypes.FETCH_MARKET_BANNERS_FAILURE:
      return {
        ...state,
        marketBanners: {
          ...state.marketBanners,
          ...loadingStatus.error,
          error: action.error,
        },
      };
    case appActionTypes.FETCH_MARKET_BANNERS_SUCCESS:
      return {
        ...state,
        marketBanners: {
          ...state.marketBanners,
          banners: action.banners,
          ...loadingStatus.idle,
          previouslyLoaded: true,
        },
      };

    // MARKET CATEGORIES
    case appActionTypes.GET_MARKET_CATEGORIES_REQUEST:
      return {
        ...state,
        marketCategories: {
          ...state.marketCategories,
          ...loadingStatus.loading,
        },
      };
    case appActionTypes.GET_MARKET_CATEGORIES_SUCCESS:
      return {
        ...state,
        marketCategories: {
          ...state.marketCategories,
          categories: [...action.categories],
          ...loadingStatus.idle,
          previouslyLoaded: true,
        },
      };
    case appActionTypes.GET_MARKET_CATEGORIES_FAILURE:
      return {
        ...state,
        marketCategories: {
          ...state.marketCategories,
          ...loadingStatus.error,
          error: action.error,
        },
      };

    //MARKET INTERESTS
    case appActionTypes.GET_MARKET_INTERESTS_REQUEST:
      return {
        ...state,
        marketInterests: {
          ...state.marketInterests,
          ...loadingStatus.loading,
        },
      };
    case appActionTypes.GET_MARKET_INTERESTS_SUCCESS:
      return {
        ...state,
        marketInterests: {
          ...state.marketInterests,
          interests: [...action.interests],
          ...loadingStatus.idle,
          previouslyLoaded: true,
        },
      };
    case appActionTypes.GET_MARKET_INTERESTS_FAILURE:
      return {
        ...state,
        marketInterests: {
          ...state.marketInterests,
          ...loadingStatus.error,
          error: action.error,
        },
      };

    // SEARCH RESULTS
    case appActionTypes.CLEAR_SEARCH_RESULTS:
      return {
        ...state,
        searchResults: {
          results: {},
          ...loadingStatus.idle,
        },
      };

    case appActionTypes.GET_SEARCH_STORES_REQUEST:
      return {
        ...state,
        searchResults: {
          ...state.searchResults,
          stores: { ...loadingStatus.loading },
        },
      };
    case appActionTypes.GET_SEARCH_STORES_SUCCESS:
      return {
        ...state,
        searchResults: {
          ...state.searchResults,
          stores: {
            ...action.results,
            ...loadingStatus.idle,
            previouslyLoaded: true,
          },
        },
      };
    case appActionTypes.GET_SEARCH_STORES_FAILURE:
      return {
        ...state,
        searchResults: {
          ...state.searchResults,
          stores: { ...loadingStatus.error, error: action.error },
        },
      };

    case appActionTypes.GET_SEARCH_PRODUCTS_REQUEST:
      return {
        ...state,
        searchResults: {
          ...state.searchResults,
          products: { ...loadingStatus.loading },
        },
      };
    case appActionTypes.GET_SEARCH_PRODUCTS_SUCCESS:
      return {
        ...state,
        searchResults: {
          ...state.searchResults,
          products: {
            ...action.results,
            ...loadingStatus.idle,
            previouslyLoaded: true,
          },
        },
      };
    case appActionTypes.GET_SEARCH_PRODUCTS_FAILURE:
      return {
        ...state,
        searchResults: {
          ...state.searchResults,
          products: { ...loadingStatus.error, error: action.error },
        },
      };
    case appActionTypes.GET_SEARCH_ARTICLES_REQUEST:
      return {
        ...state,
        searchResults: {
          ...state.searchResults,
          articles: { ...loadingStatus.loading },
        },
      };
    case appActionTypes.GET_SEARCH_ARTICLES_SUCCESS:
      return {
        ...state,
        searchResults: {
          ...state.searchResults,
          articles: {
            ...action.results,
            ...loadingStatus.idle,
            previouslyLoaded: true,
          },
        },
      };
    case appActionTypes.GET_SEARCH_ARTICLES_FAILURE:
      return {
        ...state,
        searchResults: {
          ...state.searchResults,
          articles: { ...loadingStatus.error, error: action.error },
        },
      };

    // CATEGORY RESULTS
    case appActionTypes.GET_CATEGORY_RESULTS_REQUEST:
      return {
        ...state,
        categoryResults: {
          ...state.categoryResults,
          ...loadingStatus.loading,
        },
      };
    case appActionTypes.GET_CATEGORY_RESULTS_SUCCESS:
      return {
        ...state,
        categoryResults: {
          ...state.categoryResults,
          ...action.payload,
          previouslyLoaded: true,
          ...loadingStatus.idle,
        },
      };
    case appActionTypes.GET_CATEGORY_RESULTS_FAILURE:
      return {
        ...state,
        categoryResults: {
          ...state.categoryResults,
          ...loadingStatus.error,
          error: action.error,
        },
      };

    // STORES LIST
    case appActionTypes.STORES_LIST_REQUEST:
      return {
        ...state,
        storesList: {
          ...state.storesList,
          ...loadingStatus.loading,
        },
      };
    case appActionTypes.STORES_LIST_SUCCESS:
      return {
        ...state,
        storesList: {
          ...state.storesList,
          ...action.stores,
          ...loadingStatus.idle,
          previouslyLoaded: true,
        },
      };
    case appActionTypes.STORES_LIST_FAILURE:
      return {
        ...state,
        storesList: {
          ...state.storesList,
          ...loadingStatus.error,
          error: action.error,
        },
      };

    // FEATURED PROUCTS
    case appActionTypes.FEATURED_PRODUCTS_REQUEST:
      return {
        ...state,
        featuredProducts: {
          ...state.featuredProducts,
          ...loadingStatus.loading,
        },
      };
    case appActionTypes.FEATURED_PRODUCTS_SUCCESS:
      return {
        ...state,
        featuredProducts: {
          ...state.featuredProducts,
          products: action.featuredProducts.filter(
            (p) => p.product.product_type.value === PRODUCT_TYPES.PHYSICAL,
          ),
          ...loadingStatus.idle,
          previouslyLoaded: true,
        },
      };
    case appActionTypes.FEATURED_PRODUCTS_FAILURE:
      return {
        ...state,
        featuredProducts: {
          ...state.featuredProducts,
          ...loadingStatus.error,
          error: action.error,
        },
      };

    // FEATURED STORES
    case appActionTypes.FEATURED_STORES_REQUEST:
      return {
        ...state,
        featuredStores: {
          ...state.featuredStores,
          ...loadingStatus.loading,
        },
      };
    case appActionTypes.FEATURED_STORES_SUCCESS:
      return {
        ...state,
        featuredStores: {
          ...state.featuredStores,
          stores: action.featuredStores,
          ...loadingStatus.idle,
          previouslyLoaded: true,
        },
      };
    case appActionTypes.FEATURED_STORES_FAILURE:
      return {
        ...state,
        featuredStores: {
          ...state.featuredStores,
          ...loadingStatus.error,
          error: action.error,
        },
      };

    // RANDOM PRODUCTS
    case appActionTypes.GET_RANDOM_PRODUCTS_REQUEST:
      return {
        ...state,
        randomProducts: {
          ...state.randomProducts,
          ...loadingStatus.loading,
        },
      };
    case appActionTypes.GET_RANDOM_PRODUCTS_SUCCESS:
      return {
        ...state,
        randomProducts: {
          ...state.randomProducts,
          products: action.products,
          ...loadingStatus.idle,
        },
      };
    case appActionTypes.GET_RANDOM_PRODUCTS_FAILURE:
      return {
        ...state,
        randomProducts: {
          ...state.randomProducts,
          ...loadingStatus.error,
          error: action.error,
        },
      };

    // ALL PRODUCTS
    case appActionTypes.GET_ALL_PRODUCTS_REQUEST:
      return {
        ...state,
        allProducts: {
          ...state.allProducts,
          ...loadingStatus.loading,
        },
      };
    case appActionTypes.GET_ALL_PRODUCTS_SUCCESS:
      return {
        ...state,
        allProducts: {
          ...state.allProducts,
          products: action.products,
          ...loadingStatus.idle,
        },
      };
    case appActionTypes.GET_ALL_PRODUCTS_FAILURE:
      return {
        ...state,
        allProducts: {
          ...state.allProducts,
          ...loadingStatus.error,
          error: action.error,
        },
      };

    // COUPON
    case appActionTypes.COUPON_REQUEST:
      return {
        ...state,
      };
    case appActionTypes.COUPON_SUCCESS:
      return {
        ...state,
        message: action.payload,
      };
    case appActionTypes.COUPON_FAILURE:
      return {
        ...state,
        message: action.payload,
      };

    // SHIPPING ZONES
    case appActionTypes.GET_SHIPPING_ZONES_REQUEST:
      return {
        ...state,
      };
    case appActionTypes.GET_SHIPPING_ZONES_SUCCESS:
      return {
        ...state,
        shippingZones: action.shippingZones,
      };
    case appActionTypes.GET_SHIPPING_ZONES_FAILURE:
      return {
        ...state,
        error: action.error,
      };

    // JOB OPENINGS
    case appActionTypes.GET_JOB_OPENINGS_REQUEST:
      return {
        ...state,
        jobOpenings: {
          ...state.jobOpenings,
          ...loadingStatus.loading,
        },
      };
    case appActionTypes.GET_JOB_OPENINGS_SUCCESS:
      return {
        ...state,
        jobOpenings: {
          ...state.jobOpenings,
          results: action.payload,
          ...loadingStatus.idle,
        },
      };
    case appActionTypes.GET_JOB_OPENINGS_FAILURE:
      return {
        ...state,
        jobOpenings: {
          ...state.jobOpenings,
          ...loadingStatus.error,
          error: action.error,
        },
      };
    // Stores Visa
    case appActionTypes.GET_STORES_VISA_REQUEST:
      return {
        ...state,
        storesListVisa: {
          ...state.storesListVisa,
          ...loadingStatus.loading,
        },
      };
    case appActionTypes.GET_STORES_VISA_SUCCESS:
      return {
        ...state,
        storesListVisa: {
          ...state.storesListVisa,
          ...loadingStatus.idle,
          list: action.payload,
        },
      };
    case appActionTypes.GET_STORES_VISA_FAILURE:
      return {
        ...state,
        status: 'error',
        storesListVisa: {
          ...loadingStatus.error,
        },
      };

    //BAZAR STORES
    // STORES LIST
    case appActionTypes.STORES_BAZAR_LIST_REQUEST:
      return {
        ...state,
        storesBazarList: {
          ...state.storesBazarList,
          ...loadingStatus.loading,
        },
      };
    case appActionTypes.STORES_BAZAR_LIST_SUCCESS:
      return {
        ...state,
        storesBazarList: {
          ...state.storesBazarList,
          ...action.stores,
          ...loadingStatus.idle,
          previouslyLoaded: true,
        },
      };
    case appActionTypes.STORES_BAZAR_LIST_FAILURE:
      return {
        ...state,
        storesBazarList: {
          ...state.storesBazarList,
          ...loadingStatus.error,
          error: action.error,
        },
      };

    // Prohibited words list
    case appActionTypes.GET_PROHIBITED_WORDS_REQUEST:
      return {
        ...state,
        prohibitedWords: {
          ...state.prohibitedWords,
          ...loadingStatus.loading,
        },
      };
    case appActionTypes.GET_PROHIBITED_WORDS_SUCCESS:
      return {
        ...state,
        prohibitedWords: {
          ...state.prohibitedWords,
          ...loadingStatus.idle,
          previouslyLoaded: true,
          list: action.payload,
        },
      };
    case appActionTypes.GET_PROHIBITED_WORDS_FAILURE:
      return {
        ...state,
        prohibitedWords: {
          ...state.prohibitedWords,
          ...loadingStatus.error,
          error: action.error,
        },
      };

    // DEFAULT CASE
    default:
      return {
        ...state,
      };
  }
}
