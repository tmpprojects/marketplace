// UTITLITIES AND LIBRARIES
import { combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form';

import {
  WEEK_OBJECT,
  formatHour24To12,
  addHourPeriodSuffix,
} from '../Utils/dateUtils';

// LIST OF INDIVIDUAL REDUCERS
import { authentication } from './authentication.reducer';
import { registration } from './registration.reducer';
import users, * as fromUsers from './users.reducer';
import { modalBox } from './modalBox.reducer';
import { statusWindow } from './statusWindow.reducer';
import store, * as fromStore from './store.reducer';
import myStore, * as fromMyStore from './mystore.reducer';
import { inspire } from './inspire.reducer';
import { app } from './app.reducer';
import shoppingCart, * as fromShoppingCart from './shoppingCart.reducer';
import orders, * as fromOrders from './orders.reducer';
import { landing } from './landing.reducer';
import { typeform } from './typeform.reducer';

// EXPORT REDUX STORE MODEL
export default combineReducers({
  form: formReducer,
  auth: authentication,
  registration,
  users,
  modalBox,
  statusWindow,
  inspire,
  store,
  myStore,
  cart: shoppingCart,
  app,
  orders,
  landing,
  typeform,
});

// INFORMATION FORMAT AND TRSNFORMATION UTILITIES
export const formatWorkSchedules = (schedules) => {
  const orderedSchedules = schedules.map((schedule) => ({
    value: schedule.week_day.value,
    name: WEEK_OBJECT.find((a) => a.value === schedule.week_day.value).name,
    open: {
      name: formatHour24To12(schedule.start.split(':').slice(0, -1).join(':')),
      value: schedule.start.split(':').slice(0, -1).join(':'),
    },
    close: {
      name: formatHour24To12(schedule.end.split(':').slice(0, -1).join(':')),
      value: schedule.end.split(':').slice(0, -1).join(':'),
    },
  }));

  return orderedSchedules.sort((a, b) => {
    if (a.value > b.value) return 1;
    else if (a.value < b.value) return -1;
    return 0;
  });
};

export const formatShippingSchedules = (schedules) => {
  const classNames = ['morning', 'evening', 'afternoon'];

  // Order schedules by hour
  const orderedSchedules = schedules.sort((a, b) => {
    if (a > b) return 1;
    else if (a < b) return -1;
    return 0;
  });

  // return formatted schedules
  return orderedSchedules.map((schedule, index) => {
    // Format time display.
    const pickupStart = addHourPeriodSuffix(
      schedule.collection_start.split(':').slice(0, -1).join(':'),
    );
    const pickupEnd = addHourPeriodSuffix(
      schedule.collection_end.split(':').slice(0, -1).join(':'),
    );
    const deliveryStart = addHourPeriodSuffix(
      schedule.delivery_start.split(':').slice(0, -1).join(':'),
    );
    const deliveryEnd = addHourPeriodSuffix(
      schedule.delivery_end.split(':').slice(0, -1).join(':'),
    );

    // Return schedules list.
    return {
      id: schedule.id,
      value: schedule.id,
      className: classNames[index],
      schedules: {
        limit_to_order: schedule.limit_to_order,
        pickupStart,
        pickupEnd,
        pickup: `${schedule.name} ${pickupStart} - ${pickupEnd}`,
        deliveryStart,
        deliveryEnd,
        delivery: `${schedule.name} ${deliveryStart} - ${deliveryEnd}`,
      },
    };
  });
};

// STORE SELECTORS
export const getProductDetail = (state) => fromStore.getProductDetail(state);
export const getStoreDetail = (state) => fromStore.getStoreDetail(state);

// MY STORE SELECTORS
export const getMyStoreDetail = (state) => fromMyStore.getMyStoreDetail(state);
export const getProductDetailForm = (state) =>
  fromMyStore.getProductDetailForm(state);

// USER PROFILE SELECTORS
export const getAddress = (state) => fromUsers.getAddress(state);
export const getAddressesList = (state) => fromUsers.getAddressesList(state);
export const getUserProfile = (state) => fromUsers.getUserProfile(state);

// SHOPPING CART SELECTORS
export const getCartProducts = (state) => fromShoppingCart.getCartProducts(state);
export const getCartProductsByStoreAndShipping = (state) =>
  fromShoppingCart.getCartProductsByStoreAndShipping(state);
export const getCartGrandTotal = (state) =>
  fromShoppingCart.getCartGrandTotal(state);
export const getCartProductsCount = (state) =>
  fromShoppingCart.getCartProductsCount(state);
export const getCartProductsTotal = (state) =>
  fromShoppingCart.getCartProductsTotal(state);
export const allProductsInCartHaveWithDiscount = (state) =>
  fromShoppingCart.allProductsInCartHaveWithDiscount(state);
