import { mystoreActionTypes, SHIPPING_METHODS } from '../Constants';
import { formatShippingSchedules, formatWorkSchedules } from '.';

// Default States.
const loadingStatus = {
  idle: {
    error: false,
    loading: false,
    updating: false,
  },
  loading: {
    error: false,
    loading: true,
  },
  error: {
    error: true,
    loading: false,
    updating: false,
  },
  updating: {
    error: false,
    updating: true,
  },
};

const defautlState = {
  data: {
    ...loadingStatus.loading,
  },
  products: {},
  active_product: {
    ...loadingStatus.loading,
  },
  active_store: {},
  sections: [],
  categories: [],
  attribute_types: [],
  faqs: [],
  fiscal_registries: [],
  fiscal_data: {},
  bank_account: {},
  invoice_options: {},
  bank_account_options: {},
  movements: {},
  vacations: [],
  store_plans: {},
  store_status: {},
  interests: [],
};

/*---------------------------------------------------
    REDUCER CONSTRUCTOR
---------------------------------------------------*/
export default function myStore(state = defautlState, action) {
  switch (action.type) {
    // case mystoreActionTypes.ADD_REFERRER_CODE_REQUEST:
    //   return {
    //     ...state,
    //     data: action.payload
    //   };

    // SET MYSTORE IMPROVEMENTS

    case mystoreActionTypes.SET_MYSTORE_IMPROVEMENTS:
      return {
        ...state,
        improvements: action.payload,
      };

    // GET MYSTORE
    case mystoreActionTypes.GET_MYSTORE_REQUEST:
      return {
        ...state,
        data: {
          ...state.data,
          ...loadingStatus.loading,
        },
      };
    case mystoreActionTypes.GET_MYSTORE_SUCCESS:
      return {
        ...state,
        data: {
          ...action.payload,
          ...loadingStatus.idle,
        },
      };
    case mystoreActionTypes.GET_MYSTORE_FAILURE:
      return {
        ...state,
        status: 'error',
        data: {
          ...loadingStatus.error,
        },
      };

    // UPDATE MYSTORE
    case mystoreActionTypes.UPDATE_MYSTORE_REQUEST:
      return {
        ...state,
        data: {
          ...state.data,
          ...loadingStatus.updating,
        },
      };
    case mystoreActionTypes.UPDATE_MYSTORE_SUCCESS:
      return {
        ...state,
        data: {
          ...action.payload,
          ...loadingStatus.idle,
        },
      };
    case mystoreActionTypes.UPDATE_MYSTORE_FAILURE:
      return {
        ...state,
        data: {
          ...state.data,
          ...loadingStatus.error,
          error: action.error,
        },
      };

    // GET SECTIONS
    case mystoreActionTypes.GET_MYSTORE_SECTIONS_REQUEST:
      return {
        ...state,
      };
    case mystoreActionTypes.GET_MYSTORE_SECTIONS_SUCCESS:
      return {
        ...state,
        sections: action.payload,
      };
    case mystoreActionTypes.GET_MYSTORE_SECTIONS_FAILURE:
      return {
        ...state,
        status: 'error',
      };

    // ADD SECTION
    case mystoreActionTypes.ADD_MYSTORE_SECTION_REQUEST:
      return {
        ...state,
      };
    case mystoreActionTypes.ADD_MYSTORE_SECTION_SUCCESS:
      return {
        ...state,
        sections: [action.payload, ...state.sections],
      };
    case mystoreActionTypes.ADD_MYSTORE_SECTION_FAILURE:
      return {
        ...state,
        status: 'error',
      };

    // DELETE SECTION
    case mystoreActionTypes.DELETE_MYSTORE_SECTION_REQUEST:
      return {
        ...state,
      };
    case mystoreActionTypes.DELETE_MYSTORE_SECTION_SUCCESS:
      return {
        ...state,
        sections: state.sections.filter(
          (section) => section.slug !== action.payload,
        ),
      };
    case mystoreActionTypes.DELETE_MYSTORE_SECTION_FAILURE:
      return {
        ...state,
        status: 'error',
      };

    // UPDATE SECTION
    case mystoreActionTypes.UPDATE_SECTION_REQUEST:
      return {
        ...state,
      };
    case mystoreActionTypes.UPDATE_SECTION_SUCCESS:
      return {
        ...state,
        sections: state.sections.map((item) => {
          if (item.id !== action.payload.id) {
            return item;
          }
          return action.payload;
        }),
      };
    case mystoreActionTypes.UPDATE_SECTION_FAILURE:
      return {
        ...state,
        error: action.error,
      };

    // GET FAQS
    case mystoreActionTypes.GET_MYSTORE_FAQS_REQUEST:
      return {
        ...state,
      };
    case mystoreActionTypes.GET_MYSTORE_FAQS_SUCCESS:
      return {
        ...state,
        faqs: action.payload,
      };
    case mystoreActionTypes.GET_MYSTORE_FAQS_FAILURE:
      return {
        ...state,
        status: 'error',
      };

    // ADD FAQS
    case mystoreActionTypes.ADD_MYSTORE_FAQS_REQUEST:
      return {
        ...state,
      };
    case mystoreActionTypes.ADD_MYSTORE_FAQS_SUCCESS:
      return {
        ...state,
        faqs: [action.payload, ...state.faqs],
      };
    case mystoreActionTypes.ADD_MYSTORE_FAQS_FAILURE:
      return {
        ...state,
        status: 'error',
      };

    // DELETE FAQ
    case mystoreActionTypes.DELETE_MYSTORE_FAQ_REQUEST:
      return {
        ...state,
      };
    case mystoreActionTypes.DELETE_MYSTORE_FAQ_SUCCESS:
      return {
        ...state,
        faqs: state.faqs.filter((faq) => faq.id !== action.payload),
      };
    case mystoreActionTypes.DELETE_MYSTORE_SECTION_FAILURE:
      return {
        ...state,
        status: 'error',
      };

    // UPDATE FAQ
    case mystoreActionTypes.UPDATE_FAQ_REQUEST:
      return {
        ...state,
      };
    case mystoreActionTypes.UPDATE_FAQ_SUCCESS:
      return {
        ...state,
        faqs: state.faqs.map((item) => {
          if (item.id !== action.id) {
            return item;
          }
          return {
            id: action.id,
            question: action.question,
            excerpt: action.excerpt,
          };
        }),
      };
    case mystoreActionTypes.UPDATE_FAQ_FAILURE:
      return {
        ...state,
        error: action.error,
      };

    // GET CATEGORIES
    case mystoreActionTypes.GET_MYSTORE_CATEGORIES_REQUEST:
      return {
        ...state,
      };
    case mystoreActionTypes.GET_MYSTORE_CATEGORIES_SUCCESS:
      return {
        ...state,
        categories: action.payload.map((cat) => ({ ...cat, value: cat.slug })),
      };
    case mystoreActionTypes.GET_MYSTORE_CATEGORIES_FAILURE:
      return {
        ...state,
        status: 'error',
      };

    // STORE GALLERY
    case mystoreActionTypes.ADD_STORE_IMAGE_REQUEST:
      return {
        ...state,
      };
    case mystoreActionTypes.ADD_STORE_IMAGE_SUCCESS:
      return {
        ...state,
        data: {
          ...state.data,
          gallery: [action.payload, ...state.data.gallery],
        },
      };
    case mystoreActionTypes.ADD_STORE_IMAGE_FAILURE:
      return {
        ...state,
        status: 'error',
      };
    case mystoreActionTypes.DELETE_STORE_IMAGE_REQUEST:
      return {
        ...state,
      };
    case mystoreActionTypes.DELETE_STORE_IMAGE_SUCCESS:
      return {
        ...state,
        data: {
          ...state.data,
          gallery: state.data.gallery.filter(
            (image) => image.photo !== action.payload,
          ),
        },
      };
    case mystoreActionTypes.DELETE_STORE_IMAGE_FAILURE:
      return {
        ...state,
        status: 'error',
      };
    case mystoreActionTypes.UPDATE_STORE_IMAGE_REQUEST:
      return {
        ...state,
        ...loadingStatus.loading,
      };
    case mystoreActionTypes.UPDATE_STORE_IMAGE_SUCCESS: {
      const galleryFoundIndex = state?.data?.gallery?.findIndex(
        (i) => i.id === action.payload.id,
      );
      const storeGallery =
        galleryFoundIndex < 0
          ? state.data.gallery
          : [
              ...state.data.gallery.slice(0, galleryFoundIndex),
              action.payload,
              ...state.data.gallery.slice(galleryFoundIndex + 1),
            ];
      return {
        ...state,
        ...loadingStatus.idle,
        data: {
          ...state.data,
          gallery: storeGallery.sort((a, b) =>
            a.order < b.order ? -1 : a.order > b.order ? 1 : 0,
          ),
        },
      };
    }
    case mystoreActionTypes.UPDATE_STORE_IMAGE_FAILURE:
      return {
        ...state,
        ...loadingStatus.error,
        error: action.error,
      };

    // PROFILE/COVER IMAGE
    case mystoreActionTypes.UPDATE_PROFILE_PHOTO_REQUEST:
      return {
        ...state,
        data: loadingStatus.loading,
      };
    case mystoreActionTypes.UPDATE_PROFILE_PHOTO_SUCCESS:
      return {
        ...state,
        data: {
          ...action.payload,
          ...loadingStatus.idle,
        },
      };
    case mystoreActionTypes.UPDATE_PROFILE_PHOTO_FAILURE:
      return {
        ...state,
        data: {
          ...state.data,
          ...loadingStatus.error,
          error: action.error,
        },
      };

    // FETCH PRODUCT DETAIL
    case mystoreActionTypes.FETCH_PRODUCT_REQUEST:
      return {
        ...state,
        active_product: {
          ...action.payload,
          ...loadingStatus.loading,
        },
      };
    case mystoreActionTypes.FETCH_PRODUCT_SUCCESS:
      return {
        ...state,
        active_product: {
          ...action.payload,
          photos: action.payload.photos.slice(0, 12).map((item, index) => ({
            ...item,
            order: index,
          })),
          attributes_menu: [],
          ...loadingStatus.idle,
        },
      };
    case mystoreActionTypes.FETCH_PRODUCT_FAILURE:
      return {
        ...state,
        active_product: {
          error: action.error,
          ...loadingStatus.error,
        },
      };

    // DELETE PRODUCT
    case mystoreActionTypes.DELETE_PRODUCT_REQUEST:
      return {
        ...state,
      };
    case mystoreActionTypes.DELETE_PRODUCT_SUCCESS:
      const newResults = state.products;
      newResults.results = newResults.results.filter(
        (product) => product.slug !== action.payload,
      );
      return {
        ...state,
        newResults,
      };
    case mystoreActionTypes.DELETE_PRODUCT_FAILURE:
      return {
        ...state,
        status: 'error',
      };

    // ADD PRODUCT
    case mystoreActionTypes.ADD_PRODUCT_REQUEST:
      return {
        ...state,
        active_product: {
          ...state.active_product,
          ...loadingStatus.loading,
        },
      };
    case mystoreActionTypes.ADD_PRODUCT_SUCCESS:
      return {
        ...state,
        products: [action.payload, ...state.products.results],
        active_product: {
          ...action.payload,
          ...loadingStatus.idle,
        },
      };
    case mystoreActionTypes.ADD_PRODUCT_FAILURE:
      return {
        ...state,
        active_product: {
          ...state.active_product,
          ...loadingStatus.error,
        },
      };

    // EDIT PRODUCT
    case mystoreActionTypes.EDIT_PRODUCT_REQUEST:
      return {
        ...state,
        active_product: {
          ...loadingStatus.updating,
        },
      };
    case mystoreActionTypes.EDIT_PRODUCT_SUCCESS:
      return {
        ...state,
        active_product: {
          ...action.payload,
          ...loadingStatus.idle,
        },
      };
    case mystoreActionTypes.EDIT_PRODUCT_FAILURE:
      return {
        ...state,
        active_product: {
          error: action.error,
          ...loadingStatus.error,
        },
      };

    // PRODUCT ATTRIBUTES
    case mystoreActionTypes.UPDATE_ATTRIBUTES_REQUEST:
      return {
        ...state,
      };
    case mystoreActionTypes.UPDATE_ATTRIBUTES_SUCCESS:
      return {
        ...state,
        active_product: {
          ...state.active_product,
          attributes: action.payload,
        },
      };
    case mystoreActionTypes.UPDATE_ATTRIBUTES_FAILURE:
      return {
        ...state,
        status: 'error',
      };
    case mystoreActionTypes.FETCH_ATTRIBUTE_TYPES_REQUEST:
      return {
        ...state,
      };
    case mystoreActionTypes.FETCH_ATTRIBUTE_TYPES_SUCCESS:
      return {
        ...state,
        attribute_types: action.payload,
      };
    case mystoreActionTypes.FETCH_ATTRIBUTE_TYPES_FAILURE:
      return {
        ...state,
        status: 'error',
      };
    case mystoreActionTypes.FETCH_PRODUCT_ATTRIBUTES_REQUEST:
      return {
        ...state,
      };
    case mystoreActionTypes.FETCH_PRODUCT_ATTRIBUTES_SUCCESS:
      return {
        ...state,
        active_product: {
          ...state.active_product,
          attributes_menu: action.payload,
        },
      };
    case mystoreActionTypes.FETCH_PRODUCT_ATTRIBUTES_FAILURE:
      return {
        ...state,
        status: 'error',
      };

    // PRODUCT GALLERY
    case mystoreActionTypes.ADD_PRODUCT_IMAGE_REQUEST:
      return {
        ...state,
        active_product: {
          ...state.active_product,
          ...loadingStatus.loading,
        },
      };
    case mystoreActionTypes.ADD_PRODUCT_IMAGE_SUCCESS:
      return {
        ...state,
        active_product: {
          ...state.active_product,
          ...loadingStatus.idle,
          photos: [...state.active_product.photos.slice(0, 11), action.payload],
        },
      };
    case mystoreActionTypes.ADD_PRODUCT_IMAGE_FAILURE:
      return {
        ...state,
        active_product: {
          ...state.active_product,
          ...loadingStatus.error,
          error: action.error,
        },
      };
    case mystoreActionTypes.UPDATE_PRODUCT_IMAGE_REQUEST:
      return {
        ...state,
        active_product: {
          ...state.active_product,
          ...loadingStatus.loading,
        },
      };
    case mystoreActionTypes.UPDATE_PRODUCT_IMAGE_SUCCESS: {
      const foundIndex = state.active_product.photos.findIndex(
        (i) => i.id === action.payload.id,
      );
      const gallery = state.active_product.photos;
      return {
        ...state,
        // active_product: {
        //   ...state.active_product,
        //   ...loadingStatus.idle,
        //   // photos: [
        //   //   // ...gallery.slice(0, foundIndex),
        //   //   action.payload,
        //   //   // ...gallery.slice(foundIndex + 1),
        //   // ],
        // },
        photoUpdate: action.payload,
      };
    }
    case mystoreActionTypes.UPDATE_PRODUCT_IMAGE_FAILURE:
      return {
        ...state,
        active_product: {
          ...state.active_product,
          ...loadingStatus.error,
          error: action.error,
        },
      };
    case mystoreActionTypes.DELETE_PRODUCT_IMAGE_REQUEST:
      return {
        ...state,
        active_product: {
          ...state.active_product,
          ...loadingStatus.loading,
        },
      };
    case mystoreActionTypes.DELETE_PRODUCT_IMAGE_SUCCESS:
      return {
        ...state,
        active_product: {
          ...state.active_product,
          ...loadingStatus.idle,
          photos: state.active_product.photos.filter(
            (image) => image.photo !== action.payload,
          ),
        },
      };
    case mystoreActionTypes.DELETE_PRODUCT_IMAGE_FAILURE:
      return {
        ...state,
        active_product: {
          ...state.active_product,
          ...loadingStatus.error,
          error: action.error,
        },
      };

    // GET PRODUCT LIST
    case mystoreActionTypes.GET_PRODUCTS_REQUEST:
      return {
        ...state,
        status: 'loading',
      };
    case mystoreActionTypes.GET_PRODUCTS_SUCCESS:
      return {
        ...state,
        status: 'success',
        products: action.payload,
      };
    case mystoreActionTypes.GET_PRODUCTS_FAILURE:
      return {
        ...state,
        status: 'error',
      };

    // ADD FISCAL DATA
    case mystoreActionTypes.GET_FISCAL_REGISTRIES_REQUEST:
      return {
        ...state,
      };
    case mystoreActionTypes.GET_FISCAL_REGISTRIES_SUCCESS:
      return {
        ...state,
        fiscal_registries: action.payload,
      };
    case mystoreActionTypes.GET_FISCAL_REGISTRIES_FAILURE:
      return {
        ...state,
        status: 'error',
      };

    // ADD FISCAL DATA
    case mystoreActionTypes.ADD_FISCAL_DATA_REQUEST:
      return {
        ...state,
      };
    case mystoreActionTypes.ADD_FISCAL_DATA_SUCCESS:
      return {
        ...state,
        data: {
          ...state.data,
          fiscal_data: action.payload,
        },
      };
    case mystoreActionTypes.ADD_FISCAL_DATA_FAILURE:
      return {
        ...state,
        status: 'error',
      };

    // UPDATE FISCAL DATA
    case mystoreActionTypes.UPDATE_FISCAL_DATA_REQUEST:
      return {
        ...state,
      };
    case mystoreActionTypes.UPDATE_FISCAL_DATA_SUCCESS:
      if (action.payload.data) {
        return {
          ...state,
          data: {
            ...state.data,
            fiscal_data: action.payload.data,
          },
        };
      }
      //If its a 400 error
      return {
        ...state,
        data: {
          ...state.data,
          fiscal_data: {
            ...state.data.fiscal_data,
          },
        },
      };

    case mystoreActionTypes.UPDATE_FISCAL_DATA_FAILURE:
      return {
        ...state,
        status: 'error',
      };

    // GET FISCAL DATA
    case mystoreActionTypes.FETCH_FISCAL_DATA_REQUEST:
      return {
        ...state,
      };
    case mystoreActionTypes.FETCH_FISCAL_DATA_SUCCESS:
      return {
        ...state,
        fiscal_data: action.payload,
      };
    case mystoreActionTypes.FETCH_FISCAL_DATA_FAILURE:
      return {
        ...state,
        status: 'error',
      };

    // GET INVOICE OPTIONS
    case mystoreActionTypes.GET_INVOICE_OPTIONS_REQUEST:
      return {
        ...state,
      };
    case mystoreActionTypes.GET_INVOICE_OPTIONS_SUCCESS:
      return {
        ...state,
        invoice_options: { ...action.payload },
      };
    case mystoreActionTypes.GET_INVOICE_OPTIONS_FAILURE:
      return {
        ...state,
        status: 'error',
      };

    // ADD BANK ACCOUNT
    case mystoreActionTypes.ADD_BANK_ACCOUNT_REQUEST:
      return {
        ...state,
      };
    case mystoreActionTypes.ADD_BANK_ACCOUNT_SUCCESS:
      return {
        ...state,
        bank_account: action.payload,
      };
    case mystoreActionTypes.ADD_BANK_ACCOUNT_FAILURE:
      return {
        ...state,
        status: 'error',
      };

    // GET BANK ACCOUNT
    case mystoreActionTypes.FETCH_BANK_ACCOUNT_REQUEST:
      return {
        ...state,
      };
    case mystoreActionTypes.FETCH_BANK_ACCOUNT_SUCCESS:
      return {
        ...state,
        bank_account: action.payload,
      };
    case mystoreActionTypes.FETCH_BANK_ACCOUNT_FAILURE:
      return {
        ...state,
        status: 'error',
      };

    // GET BANK ACCOUNT OPTIONS
    case mystoreActionTypes.GET_BANK_ACCOUNT_OPTIONS_REQUEST:
      return {
        ...state,
      };
    case mystoreActionTypes.GET_BANK_ACCOUNT_OPTIONS_SUCCESS:
      return {
        ...state,
        bank_account_options: { ...action.payload },
      };
    case mystoreActionTypes.GET_BANK_ACCOUNT_OPTIONS_FAILURE:
      return {
        ...state,
        status: 'error',
      };

    // GET MOVEMENTS
    case mystoreActionTypes.GET_MYSTORE_MOVEMENTS_REQUEST:
      return {
        ...state,
        movements: {
          ...state.movements,
          ...loadingStatus.loading,
        },
      };
    case mystoreActionTypes.GET_MYSTORE_MOVEMENTS_SUCCESS:
      return {
        ...state,
        movements: {
          ...action.payload,
          ...loadingStatus.idle,
        },
      };
    case mystoreActionTypes.GET_MYSTORE_MOVEMENTS_FAILURE:
      return {
        ...state,
        movements: {
          ...state.movements,
          ...loadingStatus.error,
        },
      };

    //ADD VACATIONS
    case mystoreActionTypes.ADD_MYSTORE_VACATIONS_REQUEST:
      return {
        ...state,
      };
    case mystoreActionTypes.ADD_MYSTORE_VACATIONS_SUCCESS:
      return {
        ...state,
        vacations: [...state.vacations, action.payload],
      };

    case mystoreActionTypes.ADD_MYSTORE_VACATIONS_FAILURE:
      return {
        ...state,
        status: 'error',
      };

    //GET VACATIONS
    case mystoreActionTypes.GET_MYSTORE_VACATIONS_REQUEST:
      return {
        ...state,
      };
    case mystoreActionTypes.GET_MYSTORE_VACATIONS_SUCCESS:
      return {
        ...state,
        vacations: action.payload,
      };

    case mystoreActionTypes.GET_MYSTORE_VACATIONS_FAILURE:
      return {
        ...state,
        status: 'error',
      };

    //DELETE VACATIONS
    case mystoreActionTypes.DELETE_MYSTORE_VACATIONS_REQUEST:
      return {
        ...state,
      };

    case mystoreActionTypes.DELETE_MYSTORE_VACATIONS_SUCCESS:
      return {
        ...state,
        vacations: state.vacations.filter(
          (vacation) => vacation.id !== action.payload,
        ),
      };

    case mystoreActionTypes.DELETE_MYSTORE_VACATIONS_FAILURE:
      return {
        ...state,
        status: 'error',
      };

    //UPDATE VACATIONS
    case mystoreActionTypes.UPDATE_MYSTORE_VACATIONS_REQUEST:
      return {
        ...state,
      };

    case mystoreActionTypes.UPDATE_MYSTORE_VACATIONS_SUCCESS:
      return {
        ...state,
        vacations: state.vacations.map((vacation) => {
          if (vacation.id === action.payload.id) {
            return action.payload;
          }
          return vacation;
        }),
      };

    case mystoreActionTypes.UPDATE_MYSTORE_VACATIONS_FAILURE:
      return {
        ...state,
        status: 'error',
      };

    // ADD REFERRER CODE
    case mystoreActionTypes.ADD_REFERRER_CODE_REQUEST:
      return {
        ...state,
        data: {
          ...state.data,
          ...loadingStatus.loading,
        },
      };
    case mystoreActionTypes.ADD_REFERRER_COOKIE_CODE_SUCCESS:
      return {
        ...state,
        data: {
          ...state.data,
          entered_codes: action.payload,
        },
      };

    case mystoreActionTypes.ADD_REFERRER_CODE_SUCCESS:
      return {
        ...state,
        data: {
          ...state.data,
          entered_codes: [...state.data.entered_codes, action.payload],
          ...loadingStatus.idle,
        },
      };
    case mystoreActionTypes.ADD_REFERRER_CODE_FAILURE:
      return {
        ...state,
        status: 'error',
        data: {
          ...state.data,
          ...loadingStatus.error,
        },
      };

    // GET MY ENTERED REFERRER CODE
    case mystoreActionTypes.GET_ENTERED_REFERRER_CODES_REQUEST:
      return {
        ...state,
        data: {
          ...state.data,
          ...loadingStatus.loading,
        },
      };
    case mystoreActionTypes.GET_ENTERED_REFERRER_CODES_SUCCESS:
      const newData = {
        ...state,
        data: {
          ...state.data,
          entered_codes: action.payload,
          ...loadingStatus.idle,
        },
      };

      // console.log("----------------------------",newData)

      return newData;
    case mystoreActionTypes.GET_ENTERED_REFERRER_CODES_FAILURE:
      return {
        ...state,
        status: 'error',
        data: {
          ...loadingStatus.error,
        },
      };

    //GET IVA LIST
    case mystoreActionTypes.GET_LIST_IVA_REQUEST:
      return {
        ...state,
      };
    case mystoreActionTypes.GET_LIST_IVA_SUCCESS:
      return {
        ...state,
        data: {
          ...state.data,
          listIVA: action.payload,
        },
      };
    case mystoreActionTypes.GET_LIST_IVA_FAILURE:
      return {
        ...state,
        status: 'error',
      };

    /**PLANS */
    //GET STORES PLANS
    case mystoreActionTypes.GET_MYSTORE_PLANS_REQUEST:
      return {
        ...state,
      };
    case mystoreActionTypes.GET_MYSTORE_PLANS_SUCCESS:
      return {
        ...state,
        store_plans: {
          ...state.store_plans,
          plans: action.payload,
        },
      };
    case mystoreActionTypes.GET_MYSTORE_PLANS_FAILURE:
      return {
        ...state,
        status: 'error',
        store_plans: {
          ...loadingStatus.error,
        },
      };

    //START FREE-TRIAL PRO PLAN
    case mystoreActionTypes.ADD_MYSTORE_FREE_PLAN_REQUEST:
      return {
        ...state,
      };
    case mystoreActionTypes.ADD_MYSTORE_FREE_PLAN_SUCCESS:
      return {
        ...state,
        store_plans: { ...state.store_plans, free_trial: action.payload },
      };
    case mystoreActionTypes.ADD_MYSTORE_FREE_PLAN_FAILURE:
      return {
        ...state,
        status: 'error',
      };

    //CANCEL PRO PLAN
    case mystoreActionTypes.CANCEL_MYSTORE_PLAN_REQUEST:
      return {
        ...state,
      };
    case mystoreActionTypes.CANCEL_MYSTORE_PLAN_SUCCESS:
      return {
        ...state,
        store_plans: { ...state.store_plans, cancel_plan: action.payload },
      };
    case mystoreActionTypes.CANCEL_MYSTORE_PLAN_FAILURE:
      return {
        ...state,
        status: 'error',
      };
    //PAYPAL SUBSCRIPTION
    case mystoreActionTypes.GET_MYSTORE_PAYPAL_SUBSCRIPTION_REQUEST:
      return {
        ...state,
      };
    case mystoreActionTypes.GET_MYSTORE_PAYPAL_SUBSCRIPTION_SUCCESS:
      return {
        ...state,
        store_plans: { ...state.store_plans, paypal: action.payload },
      };
    case mystoreActionTypes.GET_MYSTORE_PAYPAL_SUBSCRIPTION_FAILURE:
      return {
        ...state,
        status: 'error',
      };
    //GET STRIPE PLANS
    case mystoreActionTypes.GET_STRIPE_PLANS_REQUEST:
      return {
        ...state,
      };
    case mystoreActionTypes.GET_STRIPE_PLANS_SUCCESS:
      return {
        ...state,
        store_plans: { ...state.store_plans, stripePlans: action.payload },
      };
    case mystoreActionTypes.GET_STRIPE_PLANS_FAILURE:
      return {
        ...state,
        status: 'error',
        store_plans: {
          ...loadingStatus.error,
        },
      };
    //GET MY SAVED CARDS (STRIPE)
    case mystoreActionTypes.GET_SAVED_CARDS_REQUEST:
      return {
        ...state,
      };
    case mystoreActionTypes.GET_SAVED_CARDS_SUCCESS:
      return {
        ...state,
        store_plans: { ...state.store_plans, listSavedCards: action.payload },
      };
    case mystoreActionTypes.GET_SAVED_CARDS_FAILURE:
      return {
        ...state,
        status: 'error',
        store_plans: {
          ...loadingStatus.error,
        },
      };
    //DELETE SAVED CARD (STRIPE)
    case mystoreActionTypes.DELETE_SAVED_CARD_REQUEST:
      return {
        ...state,
      };
    case mystoreActionTypes.DELETE_SAVED_CARD_SUCCESS:
      return {
        ...state,
        store_plans: {
          ...state.store_plans,
          deleteCard: action.payload,
        },
      };
    case mystoreActionTypes.DELETE_SAVED_CARD_FAILURE:
      return {
        ...state,
        status: 'error',
        store_plans: {
          ...loadingStatus.error,
        },
      };
    //CREATE SUBSCRIPTION STRIPE
    case mystoreActionTypes.CREATE_STRIPE_SUBSCRIPTION_REQUEST:
      return {
        ...state,
      };
    case mystoreActionTypes.CREATE_STRIPE_SUBSCRIPTION_SUCCESS:
      return {
        ...state,
        store_plans: {
          ...state.store_plans,
          createStripeSubscription: action.payload,
        },
      };
    case mystoreActionTypes.CREATE_STRIPE_SUBSCRIPTION_FAILURE:
      return {
        ...state,
        status: 'error',
      };
    //SAVE CARD STRIPE
    case mystoreActionTypes.SAVE_CARD_REQUEST:
      return {
        ...state,
      };
    case mystoreActionTypes.SAVE_CARD_SUCCESS:
      return {
        ...state,
        store_plans: {
          ...state.store_plans,
          saveACard: action.payload,
        },
      };
    case mystoreActionTypes.SAVE_CARD_FAILURE:
      return {
        ...state,
        status: 'error',
      };
    //SET DEAFULT SAVED CARD STRIPE
    case mystoreActionTypes.SET_DEFAULT_SAVED_CARD_REQUEST:
      return {
        ...state,
      };
    case mystoreActionTypes.SET_DEFAULT_SAVED_CARD_SUCCESS:
      return {
        ...state,
        store_plans: {
          ...state.store_plans,
          setDefault: action.payload,
        },
      };
    case mystoreActionTypes.SET_DEFAULT_SAVED_CARD_FAILURE:
      return {
        ...state,
        status: 'error',
      };
    //GET USER PROFILE INFO
    case mystoreActionTypes.GET_USER_PROFILE_REQUEST:
      return {
        ...state,
      };
    case mystoreActionTypes.GET_USER_PROFILE_SUCCESS:
      return {
        ...state,
        store_plans: {
          ...state.store_plans,
          userProfile: action.payload,
        },
      };
    case mystoreActionTypes.GET_USER_PROFILE_FAILURE:
      return {
        ...state,
        status: 'error',
      };
    //UPDATE STORE STATUS
    case mystoreActionTypes.UPDATE_STORE_STATUS_REQUEST:
      return {
        ...state,
        ...loadingStatus.loading,
      };
    case mystoreActionTypes.UPDATE_STORE_STATUS_SUCCESS:
      return {
        ...state,
        store_status: {
          ...state.store_status,
          data: action.payload,
          ...loadingStatus.idle,
        },
      };
    case mystoreActionTypes.UPDATE_STORE_STATUS_FAILURE:
      return {
        ...state,
        ...loadingStatus.error,
      };
    // GET INTERESTS
    case mystoreActionTypes.GET_MYSTORE_INTERESTS_REQUEST:
      return {
        ...state,
      };
    case mystoreActionTypes.GET_MYSTORE_INTERESTS_SUCCESS:
      return {
        ...state,
        interests: action.payload,
      };
    case mystoreActionTypes.GET_MYSTORE_INTERESTS_FAILURE:
      return {
        ...state,
        status: 'error',
      };
    // DEFAULT
    default:
      return state;
  }
}

/*---------------------------------------------------
    SELECTORS
---------------------------------------------------*/
export const getMyStoreDetail = ({ myStore }) => {
  const { data: myStoreDetail } = myStore;
  const { loading = true } = myStoreDetail;

  if (loading) {
    return {
      loading,
    };
  }

  const cover = Object.keys(myStoreDetail.cover).reduce(
    (flag, imageSize) => (!flag ? myStoreDetail.cover[imageSize] !== '' : true),
    false,
  )
    ? myStoreDetail.cover
    : null;
  const photo = Object.keys(myStoreDetail.photo).reduce(
    (flag, imageSize) => (!flag ? myStoreDetail.photo[imageSize] !== '' : true),
    false,
  )
    ? myStoreDetail.photo
    : null;

  return {
    loading,
    ...myStoreDetail,
    cover,
    photo,
    shipping_schedules: formatShippingSchedules(myStoreDetail.shipping_schedules),
    work_schedules: formatWorkSchedules(myStoreDetail.work_schedules),
  };
};
export const getProductDetailForm = (state) => {
  // Get properties
  const storeDetail = getMyStoreDetail(state);
  const { active_product: productDetail, categories } = state.myStore;
  const { loading } = productDetail;

  // // Default values
  // const shipping_schedules = productDetail.physical_properties
  //    && productDetail.physical_properties.shipping_schedules.length
  // ? productDetail.physical_properties.shipping_schedules
  // : storeDetail.shipping_schedules;

  // const work_schedules = productDetail.work_schedules && productDetail.work_schedules.length
  // ? formatWorkSchedules(productDetail.work_schedules)
  // : storeDetail.work_schedules;

  //const category = productDetail.category;
  // const category = productDetail.category
  //   ? categories.filter(
  //       (c) =>
  //         productDetail.category.find((cat) => cat.slug === c.slug) !== undefined,
  //     )
  //   : [];

  const photos = productDetail.photos
    ? productDetail.photos.sort((a, b) =>
        a.order > b.order ? 1 : a.order < b.order ? -1 : 0,
      )
    : [];

  // Make sure at least Express Car is set as default shipping method value (NO need update 09 September 20)
  // const shippingMethods = productDetail.physical_properties
  //   ? productDetail.physical_properties.shipping.filter(
  //       (sm) => sm.slug !== SHIPPING_METHODS.EXPRESS_CAR.slug
  //     )
  //   : [];
  // shippingMethods.push(SHIPPING_METHODS.EXPRESS_CAR);

  // Return object
  return {
    loading,
    ...productDetail,
    photos,
    new_category: productDetail?.new_category,
    interest: productDetail?.interest,
    section: productDetail.section
      ? [state.myStore.sections.find((s) => s.slug === productDetail.section)]
      : [],
    physical_properties: {
      ...productDetail.physical_properties,
      // shipping: shippingMethods,
      // shipping_schedules: shipping_schedules.map(s => s.id)
    },
    price: productDetail.price || '1.00',
  };
};
export const getFiscalDataOptions = ({ myStore }) => {
  const { invoice_options } = myStore;
  return {
    entities: invoice_options.entities ? invoice_options.entities : [],
    regimes: invoice_options.regimes ? invoice_options.regimes : [],
    states: invoice_options.states ? invoice_options.states : [],
  };
};
export const getFiscalData = ({ myStore }) => {
  const { fiscal_data } = myStore;
  return {
    ...fiscal_data,
    state: fiscal_data.state ? fiscal_data.state.value : '',
    regime: fiscal_data.regime ? fiscal_data.regime.value : '',
    entity_type: fiscal_data.entity_type ? fiscal_data.entity_type : '',
  };
};
export const getBankData = ({ myStore }) => {
  const { bank_account } = myStore;
  return {
    ...bank_account,
    bank: bank_account.bank ? bank_account.bank.value : '',
    account_number_type: bank_account.account_number_type
      ? bank_account.account_number_type.value
      : '',
  };
};
export const getBankDataOptions = ({ myStore }) => {
  const { bank_account_options } = myStore;
  return {
    banks: bank_account_options.banks ? bank_account_options.banks : [],
    type_account: bank_account_options.type_account
      ? bank_account_options.type_account
      : [],
  };
};
