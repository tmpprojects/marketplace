import { modalBoxConstants } from '../Actions/modalBox.actions';

const windowTypes = {
  ERROR: 'alert-danger',
  SUCCESS: 'alert-success',
  NONE: null,
};
const initialState = {
  visible: false,
  window: windowTypes.NONE,
  message: '',
};

export function modalBox(state = initialState, action) {
  switch (action.type) {
    case modalBoxConstants.SUCCESS:
      return {
        visible: true,
        window: windowTypes.SUCCESS,
        message: action.message,
      };
    case modalBoxConstants.ERROR:
      return {
        visible: true,
        window: windowTypes.ERROR,
        message: action.message,
      };

    case modalBoxConstants.CLOSE:
      return {
        ...initialState,
      };
    case modalBoxConstants.OPEN:
      return {
        visible: true,
        window: action.window,
        message: '',
      };

    default:
      return state;
  }
}
