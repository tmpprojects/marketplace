import { getFormValues } from 'redux-form';

import { SHOPPING_CART_FORM_CONFIG } from '../Utils/shoppingCart/shoppingCartFormConfig';
import { userActionTypes, mystoreActionTypes } from '../Constants';

/*---------------------------------------------------
    STATE
---------------------------------------------------*/
const loadingStatusIdle = {
  error: false,
  loading: false,
};
const loadingStatus = {
  idle: {
    error: false,
    loading: false,
  },
  loading: {
    error: false,
    loading: true,
  },
  error: {
    error: true,
    loading: false,
  },
};
const defaultState = {
  isLogged: false,
  isGuest: false,
  message: '',
  addresses: {
    ...loadingStatus.loading,
    addresses: [],
  },
  reviews: {
    pending: {
      ...loadingStatus.loading,
      count: 0,
      npages: 0,
      pages: [],
      next: null,
      previous: null,
      results: [],
    },
    completed: {
      ...loadingStatus.loading,
      count: 0,
      npages: 0,
      pages: [],
      next: null,
      previous: null,
      results: [],
    },
  },
  creditCards: {
    creditCards: [],
  },
  profile: {
    has_store: false,
    first_name: '',
    last_name: '',
    mobile: '',
    ...loadingStatus.loading,
  },
};

/*---------------------------------------------------
    REDUCER
---------------------------------------------------*/
export default function (state = defaultState, action) {
  switch (action.type) {
    // GET MYSTORE
    case mystoreActionTypes.GET_MYSTORE_SUCCESS:
      return {
        ...state,
        //isLogged: true,
        profile: {
          ...state.profile,
          has_store: true,
        },
      };

    // USER ADDRESS
    case userActionTypes.ADDRESS_ADD_REQUEST:
      return {
        ...state,
        addresses: {
          ...state.addresses,
          loading: true,
        },
      };
    case userActionTypes.ADDRESS_ADD_SUCCESS:
      // Check for duplicates before adding the new address.
      const addressExists = state.addresses.addresses.find(
        (a) => a.uuid === action.payload.uuid,
      );
      const addressList = !addressExists
        ? [action.payload, ...state.addresses.addresses]
        : state.addresses.addresses;

      //
      return {
        ...state,
        addresses: {
          ...state.addresses,
          ...loadingStatusIdle,
          addresses: addressList,
        },
      };
    case userActionTypes.ADDRESS_ADD_FAILURE:
      return {
        ...state,
        addresses: {
          ...state.addresses,
          loading: false,
          error: action.payload,
        },
      };
    case userActionTypes.ADDRESS_LIST_SUCCESS:
      return {
        ...state,
        addresses: {
          ...loadingStatusIdle,
          previouslyLoaded: true,
          addresses: action.payload,
        },
      };
    case userActionTypes.ADDRESS_LIST_FAILURE:
      return {
        ...state,
        addresses: {
          loading: false,
          error: action.payload,
          addresses: [],
        },
      };
    case userActionTypes.ADDRESS_REMOVE_SUCCESS:
      return {
        ...state,
        addresses: {
          ...loadingStatusIdle,
          addresses: state.addresses.addresses.filter(
            (address) => address.uuid !== action.payload,
          ),
        },
      };
    case userActionTypes.ADDRESS_REMOVE_FAILURE:
      return {
        ...state,
        addresses: {
          ...state.addresses,
          loading: false,
          error: action.payload,
        },
      };

    case userActionTypes.ADDRESS_GUEST_REMOVE_SUCCESS:
      const newArray = state.addresses.addresses.slice(0, 0);
      return {
        ...state,
        addresses: {
          ...loadingStatusIdle,
          addresses: newArray,
        },
      };
    case userActionTypes.ADDRESS_UPDATE_SUCCESS:
      return {
        ...state,
        addresses: {
          ...state.addresses,
          addresses: state.addresses.addresses.map((address) =>
            address.uuid === action.payload.uuid ? action.payload : address,
          ),
        },
      };

    // GET USER REVIEWS
    case userActionTypes.GET_USER_REVIEWS_REQUEST:
      return {
        ...state,
        reviews: {
          ...state.reviews,
          ...loadingStatus.loading,
        },
      };
    case userActionTypes.GET_USER_REVIEWS_SUCCESS:
      if (action.reviewType === 'pending') {
        let results = [...action.payload.results];

        //In case results is not empty
        if (state.reviews.pending.results !== []) {
          //Add more reviews (not repeated) to the current reviews array
          const newProductReviews = results.filter(
            (r) =>
              state.reviews.pending.results.findIndex((pr) => pr.id === r.id) === -1,
          );
          results = [...state.reviews.pending.results, ...newProductReviews];
        }
        return {
          ...state,
          reviews: {
            pending: {
              ...loadingStatus.idle,
              previouslyLoaded: true,
              ...action.payload,
              results: results.filter((r) => r.product),
            },
            completed: {
              ...state.reviews.completed,
            },
          },
        };
      }

      //In case of 'completed' reviews
      return {
        ...state,
        reviews: {
          // ...loadingStatus.loading,
          pending: {
            ...state.reviews.pending,
          },
          completed: {
            ...loadingStatus.idle,
            previouslyLoaded: true,
            ...action.payload,
            results: action.payload.results.filter((r) => r.product),
          },
        },
      };

    case userActionTypes.GET_USER_REVIEWS_FAILURE:
      return {
        ...state,
        reviews: {
          ...state.reviews,
          ...loadingStatus.error,
          error: action.error,
        },
      };

    case userActionTypes.REVIEWS_ADD_REQUEST:
      return {
        ...state,
        reviews: {
          ...state.reviews,
          ...loadingStatus.loading,
        },
      };
    case userActionTypes.REVIEWS_ADD_SUCCESS:
      const foundProductReview = state.reviews.pending.results.findIndex(
        (pr) => pr.id === action.currentReview.id,
      );

      return {
        ...state,
        reviews: {
          ...state.reviews,
          ...loadingStatus.idle,
          pending: {
            ...state.reviews.pending,
            results: [
              ...state.reviews.pending.results.slice(0, foundProductReview),
              ...state.reviews.pending.results.slice(foundProductReview + 1),
            ],
          },
          completed: {
            ...state.reviews.completed,
          },
        },
      };
    case userActionTypes.REVIEWS_ADD_FAILURE:
      return {
        ...state,
        reviews: {
          ...state.reviews,
          ...loadingStatus.error,
          error: action.error,
        },
      };

    //ADD CREDIT CARD
    case userActionTypes.CREDITCARD_ADD_SUCCESS:
      return {
        ...state,
        creditCards: {
          creditCards: [action.payload, ...state.creditCards.creditCards],
          ...loadingStatus.idle,
        },
      };
    case userActionTypes.CREDITCARD_REMOVE_SUCCESS:
      return {
        ...state,
        creditCards: state.creditCards.filter(
          (creditCard) => creditCard.number !== action.number,
        ),
      };

    // GET USER CREDIT CARDS
    case userActionTypes.GET_USER_CREDIT_CARDS_REQUEST:
      return {
        ...state,
        creditCards: {
          ...state.creditCards,
          ...loadingStatus.loading,
        },
      };
    case userActionTypes.GET_USER_CREDIT_CARDS_SUCCESS:
      return {
        ...state,
        creditCards: {
          ...state.creditCards,
          ...loadingStatus.idle,
          creditCards: action.payload.reverse(),
        },
      };

    case userActionTypes.GET_USER_CREDIT_CARDS_FAILURE:
      return {
        ...state,
        creditCards: {
          ...state.creditCards,
          ...loadingStatus.error,
          error: action.payload,
        },
      };

    // PASSWORD RECOVER
    case userActionTypes.PASSWORD_RECOVER_REQUEST:
      return {
        ...state,
      };
    case userActionTypes.PASSWORD_RECOVER_SUCCESS:
      return {
        ...state,
        message: action.payload,
      };
    case userActionTypes.PASSWORD_RECOVER_FAILURE:
      return {
        ...state,
        message: action.payload,
      };

    // PASSWORD UPDATE
    case userActionTypes.PASSWORD_UPDATE_REQUEST:
      return {
        ...state,
      };
    case userActionTypes.PASSWORD_UPDATE_SUCCESS:
      return {
        ...state,
        message: action.payload,
      };
    case userActionTypes.PASSWORD_UPDATE_FAILURE:
      return {
        ...state,
        message: action.payload,
      };

    // SET USER AS GUEST
    case userActionTypes.SET_USER_AS_GUEST_REQUEST:
      return {
        ...state,
      };
    case userActionTypes.SET_USER_AS_GUEST_SUCCESS:
      return {
        ...state,
        isGuest: true,
        isLogged: false,
      };
    case userActionTypes.SET_USER_AS_GUEST_FAILURE:
      return {
        ...state,
        isGuest: false,
      };

    // GET CURRENT USER
    case userActionTypes.GET_CURRENT_USER_REQUEST:
      return {
        ...state,
        profile: {
          ...loadingStatus.loading,
        },
      };
    case userActionTypes.GET_CURRENT_USER_SUCCESS:
      return {
        ...state,
        isLogged: true,
        isGuest: false,
        profile: {
          ...loadingStatus.idle,
          ...action.payload,
        },
      };
    case userActionTypes.GET_CURRENT_USER_FAILURE:
      return {
        ...state,
        isLogged: false,
        profile: {
          ...loadingStatus.error,
          error: action.error,
        },
      };

    // USER PROFILE PHOTO
    case userActionTypes.UPLOAD_USER_PHOTO_REQUEST:
      return {
        ...state,
        profile: {
          ...state.profile,
          ...loadingStatus.loading,
        },
      };
    case userActionTypes.UPLOAD_USER_PHOTO_SUCCESS:
      return {
        ...state,
        profile: {
          ...state.profile,
          ...loadingStatus.idle,
          profile_photo: action.payload.profile_photo,
        },
      };
    case userActionTypes.UPLOAD_USER_PHOTO_FAILURE:
      return {
        ...state,
        profile: {
          ...state.profile,
          ...loadingStatus.error,
          error: action.error,
        },
      };

    // UPDATE USER PROFILE
    case userActionTypes.UPDATE_INFO_REQUEST:
      return {
        ...state,
        profile: {
          ...loadingStatus.loading,
          ...state.profile,
        },
      };
    case userActionTypes.UPDATE_INFO_SUCCESS:
      return {
        ...state,
        profile: {
          //...state.profile,
          ...loadingStatus.idle,
          ...action.payload,
        },
      };
    case userActionTypes.UPDATE_INFO_ERROR:
      return {
        ...state,
        profile: {
          ...state.profile,
          ...loadingStatus.error,
          error: action.error,
        },
      };

    default:
      return state;
  }
}

/*---------------------------------------------------
    SELECTORS
---------------------------------------------------*/
export const getAddress = (address = {}) => {
  // Here we can make any validations,
  // data transformation, formatting, etc...
  return {
    ...address,
    //default_address: address.default_address || false,
    //pickup_address: address.pickup_address || false
  };
};

export const getAddressesList = (state) =>
  state.users.addresses.addresses.map((a) => getAddress(a));

export const getUserProfile = (state) => {
  const { profile } = state.users;

  // Format / Default Values
  let birthday;
  if (profile.birthday) {
    birthday = profile.birthday.split('-');
  } else {
    birthday = ['', '', ''];
  }

  return {
    loading: profile.loading,
    ...profile,
    bd_year: birthday[0],
    bd_month: birthday[1],
    bd_day: birthday[2],
  };
};

/**
 * getUserZipCode()
 * @param {obj} state | Redux state.
 * @returns {int} Zip Code.
 */
export const getUserZipCode = (state) => {
  // Get redux state and shopping form values.
  const shoppingFormCartValues = getFormValues(SHOPPING_CART_FORM_CONFIG.formName)(
    state,
  );

  // Format user´s zip code to look up for products availability.
  let zipCode = null;
  const addressesList = state.users.addresses.addresses;

  // Try to get zip code only if the current users has stored addresses.
  if (addressesList.length) {
    // If 'shopping form' already has a selected shipping address,
    // choose this address to keep 'persistance'.
    let shippingAddress = addressesList.find((address) => {
      // Set selected address.
      if (
        shoppingFormCartValues &&
        shoppingFormCartValues.selectedShippingAddress === address.uuid
      ) {
        return address;
      }
    });

    if (!shippingAddress) {
      shippingAddress = addressesList.reduce((found, a, i, l) => {
        // If the address has not been found yet,
        // search whithin different options.
        if (!found) {
          let targetAddress = false;

          // If the address´s not been found,
          // search if this is the default address.
          if (a.default_address && !targetAddress) {
            targetAddress = a;
          }

          // If the above options failed to set an address,
          // choose the first address on the list.
          if (i === l.length - 1 && !targetAddress) {
            targetAddress = l[0];
          }

          // Return result.
          return targetAddress;
        }
        return found;
      }, false);
    }

    // Set zipCode.
    zipCode = shippingAddress ? shippingAddress.zip_code : zipCode;
  }

  //
  return zipCode;
};
