import React, { Component, Fragment } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import queryString from 'query-string';

import { appActions } from '../Actions';
import '../../styles/_storesPage.scss';
import SEO from '../../statics/SEO.json';
import PageHead from '../Utils/PageHead';
import StoresList from '../components/Search/StoresList';
import withPagination from '../components/hocs/withPagination';
import { insertKeyToQueryParam } from './../Utils/queryParamsUtils';
import HoraRosa from '../components/HoraRosa/HoraRosa.js';

const MAX_ITEMS_PER_PAGE = 12;
const PaginatedList = withPagination(StoresList);
export class StoresListPage extends Component {
  constructor(props) {
    super(props);
    this.currentPage = this.getResultsPageFromQueryString(
      this.props.location.search,
    );
  }

  componentDidMount() {
    // shippingAddressObserver
    globalThis.shippingAddressObserver.subscribe(this.addZipCodeFilter);

    this.performSearch(this.currentPage);
  }

  componentWillReceiveProps(nextProps) {
    // If page param (Ej. URL ?page={pageNumber}}) has changed
    if (this.props.location.search !== nextProps.location.search) {
      this.performSearch(
        this.getResultsPageFromQueryString(nextProps.location.search),
      );
    }
  }

  componentWillUnmount() {
    // shippingAddressObserver
    globalThis.shippingAddressObserver.unsubscribe(this.addZipCodeFilter);
  }

  /**
   * addZipCodeFilter()
   * Adds zip code filter to current queryparams string
   * @param {string} zipCode | Query string to send to API
   */
  addZipCodeFilter = (zipCode) => {
    this.performSearch(
      1,
      insertKeyToQueryParam('zipcode', zipCode, document.location.search),
    );
  };

  getResultsPageFromQueryString = (query) => {
    if (query) {
      const resultsPage = parseInt(queryString.parse(query).p, 10);
      return !isNaN(resultsPage) ? resultsPage : 1;
    }
    return 1;
  };

  performSearch = (page = 1, filters = '') => {
    // Scroll to top of page
    // TODO: Scroll to top of results list instead of top of document.
    window.scrollTo(0, 0);

    // Construct search query parameters
    const storesQuery = `?page=${page}&page_size=${MAX_ITEMS_PER_PAGE}&${filters}`;

    this.currentPage = parseInt(page, 10);

    //Send request to API
    this.props.getStoresList(storesQuery);
  };

  render() {
    const { storesList, location } = this.props;
    const currentPage = this.currentPage;
    if (storesList.loading) {
      return null;
    }
    return (
      <Fragment>
        <HoraRosa location={this.props.location} little={true}></HoraRosa>
        <section className="storesPage">
          <PageHead attributes={SEO.StoresListPage} />
          <h1 className="cr__text--subtitle cr__stores-title">Nuestras tiendas</h1>
          <div className="storesPage-list">
            {/* PAGINATED LIST */}
            <PaginatedList
              items={storesList.results}
              baseLocation="/stores?"
              page={currentPage}
              action={this.testingFunction}
              maxItems={MAX_ITEMS_PER_PAGE}
              totalPages={storesList.npages}
              location={location.pathname}
            />
            {/* END: /PAGINATED LIST */}
          </div>
        </section>
      </Fragment>
    );
  }
}

// Load Data for Server Side Rendering TO DO
StoresListPage.loadData = (reduxStore, routePath) => {
  const { match } = routePath;

  const resultsPage = match.params.p;
  const storesQuery = `?page=${resultsPage}&page_size=${MAX_ITEMS_PER_PAGE}`;

  const promises = [reduxStore.dispatch(appActions.getStoresList(storesQuery))];
  return Promise.all(promises.map((p) => (p.catch ? p.catch((e) => e) : p)));
};

// Map Redux Props and Actions to component
function mapStateToProps({ app: { storesList } }) {
  return {
    storesList,
  };
}
function mapDispatchToProps(dispatch) {
  const { getStoresList } = appActions;

  return bindActionCreators(
    {
      getStoresList,
    },
    dispatch,
  );
}

// Export Component
export default connect(mapStateToProps, mapDispatchToProps)(StoresListPage);
