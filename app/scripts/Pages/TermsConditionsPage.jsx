import React from 'react';
import '../../styles/_terms.scss';
import PageHead from '../Utils/PageHead';
import { ORDERS_EMAIL } from '../Constants/config.constants';

export default function TermsConditions() {
  return (
    <React.Fragment>
      <PageHead
        attributes={{
          title: 'Términos y Condiciones de Uso | Canasta Rosa',
        }}
      />

      <section className="wrapper--center terms">
        <h1 className="title">Términos y Condiciones</h1>

        <h4>USO DEL SERVICIO PARA COMPRADORES</h4>
        <p>
          El presente Contrato establece los términos y condiciones de CANASTA ROSA,
          S.A.P.I. de C.V., identificado en lo sucesivo bajo el nombre comercial de
          CANASTA ROSA que regulan el acceso y uso del sitio web www.canastarosa.com
          (en adelante, “EL SITIO”) y todos los sitios web adicionales de CANASTA
          ROSA (los “Sitios web de CANASTA ROSA”). Cualquier persona que haga uso de
          EL SITIO o de los servicios que presta CANASTA ROSA deberá sujetarse a los
          presentes términos y condiciones junto con las demás políticas establecidas
          por CANASTA ROSA.
        </p>
        <p>
          Cualquier persona que no acepte estos términos y condiciones, mismos que
          tienen un carácter de obligatorio deberá de abstenerse de utilizar El SITIO
          y/o los servicios prestados por CANASTA ROSA.
        </p>
        <p>
          LLos servicios prestados por CANASTA ROSA sólo podrán ser utilizados por
          personas que cuenten con la capacidad legal para contratar.
        </p>
        <h4>POLÍTICAS DE ENTREGA, REEMBOLSO Y CANCELACIÓN DE PEDIDOS:</h4>
        <h5>ENTREGAS</h5>
        <p>
          El domicilio donde se entregarán los productos será el ingresado al sistema
          al momento de realizar la compra. De haber un error en el domicilio en el
          sistema, no se puede garantizar que este se pueda corregir. En caso de
          existir un error en el domicilio señalado, o que este sea inaccesible y la
          orden no pueda ser entregada, se considerará como una “Entrega Fallida” y
          el equipo de atención al cliente hará llegar un correo notificando al
          destinatario. Debido a que alguno de los productos que se exhiben a través
          de la plataforma son productos perecederos, los productos deberán ser
          destruidos y EL COMPRADOR no podrá ejercer su derecho de retracto o ningún
          tipo de derecho similar que podría corresponderle. En caso de que EL
          OPERADOR LOGÍSTICO pueda resguardar el producto, EL COMPRADOR tendrá 24
          horas para recogerlo en el punto de resguardo o podrá reprogramar una nueva
          entrega con un costo adicional. En caso contrario, el producto deberá ser
          destruido.
        </p>
        <p>
          El repartidor puede permanecer sólo 10 minutos en el lugar de entrega. Si
          el destinatario no se encuentra en el domicilio, se tratará de contactar al
          mismo para esperar instrucciones. Como alternativa, se hará el intento de
          entregar con algún conocido dentro del domicilio que tenga facultades para
          recibir el producto: familiar, compañero de trabajo, recepcionista,
          vigilante, etc. En caso de que el producto contenga alcohol, tabaco, o
          cualquier otra substancia exclusiva para mayores de edad, sólo EL COMPRADOR
          podrá recibir el paquete, y deberá presentar una identificación oficial que
          verifique su mayoría de edad.
        </p>
        <p>
          En caso de que ninguna de estas opciones sea exitosa, el repartidor
          continuará su ruta y el equipo de atención a clientes hará llegar un correo
          notificando una “Entrega Fallida” al destinatario. Debido a que alguno de
          los productos que se exhiben a través de la plataforma son productos
          perecederos, los productos deberán ser destruidos y EL COMPRADOR no podrá
          ejercer su derecho de retracto o ningún tipo de derecho similar que podría
          corresponderle. En caso de que EL OPERADOR LOGÍSTICO pueda resguardar el
          producto, EL COMPRADOR tendrá 24 horas para recogerlo en el punto de
          resguardo o podrá reprogramar una nueva entrega con un costo adicional.
        </p>
        <p>
          Todos los tiempos de entrega son estimados debido a los tiempos de
          elaboración que pueda tener EL VENDEDOR y a la operación de EL OPERADOR
          LOGÍSTICO, por lo que CANASTA ROSA hará sus mayores esfuerzos porque la
          entrega se realice en tiempo, pero no se hace responsable por la desviación
          con respecto a la fecha y hora estimada de entrega.
        </p>
        <h5>DIRECCIONES DE ENTREGA CON ACCESOS LIMITADOS</h5>
        <p>
          Cuando por políticas internas de una empresa o localidad (como hospitales y
          algunos corporativos), no se permita concretar la entrega como indicó EL
          COMPRADOR, se considerará como una “Entrega Fallida”. Debido a que alguno
          de los productos que se exhiben a través de la plataforma son productos
          perecederos, los productos deberán ser destruidos y EL COMPRADOR no podrá
          ejercer su derecho de retracto o ningún tipo de derecho similar que podría
          corresponderle. En caso de que EL OPERADOR LOGÍSTICO pueda resguardar el
          producto, EL COMPRADOR tendrá 24 horas para recogerlo en el punto de
          resguardo o podrá reprogramar una nueva entrega con un costo adicional.
        </p>
        <h5>ENTREGA DE REGALOS RECHAZADOS</h5>
        <p>
          En caso de que el destinatario no quiera recibir el regalo se le notificará
          al COMPRADOR. Si desea, se puede programar una nueva dirección de envío por
          un costo adicional; de lo contrario se considerará como una “Entrega
          Fallida”. Debido a que alguno de los productos que se exhiben a través de
          la plataforma son productos perecederos, los productos deberán ser
          destruidos y EL COMPRADOR no podrá ejercer su derecho de retracto o ningún
          tipo de derecho similar que podría corresponderle. En caso de que EL
          OPERADOR LOGÍSTICO pueda resguardar el producto, EL COMPRADOR tendrá 24
          horas para recogerlo en el punto de resguardo o podrá reprogramar una nueva
          entrega con un costo adicional.
        </p>
        <h5>CANCELACIONES POR PARTE DEL COMPRADOR</h5>
        <p>
          En caso de que el comprador desee cancelar el pedido de compra deberá de:
        </p>
        <ul>
          <li>
            <p>
              Enviar un correo electrónico a{' '}
              <a href="mailto:contacto@canastarosa.com">contacto@canastarosa.com</a>,
              El estatus del pedido deberá de ser Nuevo o Por Confirmar.
            </p>
          </li>
          <li>
            <p>
              En caso de que EL COMPRADOR haya solicitado la cancelación del pedido
              de compra respectivo y el producto haya sido entregado a EL COMPRADOR,
              la cancelación no procederá.
            </p>
          </li>
        </ul>
        <h4>CAMBIOS Y DEVOLUCIONES</h4>
        <p>
          En caso de que el producto no haya llegado en las condiciones acordadas, se
          puede solicitar un código de compra por la cantidad pagada, o puede
          solicitar un cambio por otro producto dentro de la plataforma con un valor
          total del mismo precio pagado o menor. Si EL COMPRADOR no recibe el
          producto, podrá solicitar una devolución por el monto pagado, y ésta se
          procederá dentro de los siguientes 20 días hábiles.
        </p>
        <p>
          Si EL COMPRADOR considera que EL VENDEDOR violó una de las políticas de
          CANASTA ROSA, y desea la devolución de su dinero podrá hacer una solicitud
          al equipo de atención a clientes de CANASTA ROSA dentro de las primeras 24
          horas después de recibir su producto. CANASTA ROSA llevará a cabo una
          investigación de las acusaciones. Los resultados de dicha investigación se
          compartirán con EL COMPRADOR y en caso de concordar con el reclamo de EL
          COMPRADOR se procederá a generar una devolución dentro de los siguientes 20
          días hábiles.
        </p>
        <h5>INCONFORMIDAD CON EL PRODUCTO</h5>
        <p>
          Los productos vendidos a través de CANASTA ROSA pueden ser artesanales y
          pueden existir variaciones entre las fotografías y los productos
          entregados. En caso de estar inconforme con un producto o una tienda, podrá
          dar retroalimentación del producto o de la tienda a través de las encuestas
          de satisfacción o de las reseñas de los productos. En caso de requerir un
          cambio o devolución deberá seguir los pasos de cambios o devoluciones.
        </p>
        <h4>CONTRACARGO</h4>
        <p>
          En caso de que se presente una reclamación por cargo a tarjeta de crédito o
          débito no reconocida por EL COMPRADOR (denominado “CONTRACARGO”), CANASTA
          ROSA se reserva el derecho de iniciar la acción legal, coadyuvando con el
          Ministerio Público y la Institución Financiera para iniciar la Averiguación
          Previa por la comisión de un delito. Así mismo, CANASTA ROSA podrá tomar
          las medidas que considere necesarias dentro del marco de la Ley para
          recuperar los productos si estos fueron entregados a EL COMPRADOR.
        </p>
        <p>
          La baja de EL COMPRADOR del portal de CANASTA ROSA, no exime de su
          responsabilidad de pago por operaciones realizadas durante la vigencia de
          la misma en el portal de CANASTA ROSA.
        </p>
        <h4>INSCRIPCIÓN</h4>
        <p>
          Para hacer uso de los Sitios web de CANASTA ROSA o de los servicios
          prestados por CANASTA ROSA, EL COMPRADOR podrá optar por cualquiera de las
          siguientes opciones:
        </p>
        <ul>
          <li>
            <p>
              El COMPRADOR podrá crear una cuenta registrándose en la página web de
              CANASTA ROSA debiendo ingresar la información correspondiente en todos
              los campos con datos válidos de manera exacta, precisa y verdadera. En
              este caso EL COMPRADOR asume el compromiso de actualizar su información
              cuando exista algún cambio en la información previamente ingresada.
              CANASTA ROSA podrá utilizar diversos medios para identificar a los
              COMPRADORES, sin que esto implique que CANASTA ROSA se responsabilice
              por la certeza de la información provista por la persona que se ostente
              como EL COMPRADOR. Asimismo, EL COMPRADOR garantiza y responde, en
              cualquier caso, de la veracidad, exactitud, vigencia y autenticidad de
              la información ingresada para su registro.
            </p>
            <p>
              CANASTA ROSA se reserva el derecho de solicitar algún comprobante y/o
              dato adicional con la finalidad de validar la información ingresada por
              EL COMPRADOR, así como de suspender temporal o definitivamente a
              aquellos COMPRADORES cuya información no haya podido ser comprobada. En
              este caso, se dará de baja la cuenta de EL COMPRADOR, sin que ello
              genere algún derecho a resarcimiento.
            </p>
            <p>
              CANASTA ROSA se reserva el derecho de rechazar cualquier solicitud de
              registro o bien a cancelar un registro previamente realizado con causa
              justificada, sin que esté obligado a notificar a EL COMPRADOR los
              motivos que dieron lugar a rechazar o cancelar el registro y sin que
              este hecho genere algún derecho a indemnización o resarcimiento.
            </p>
          </li>
          <li>
            <p>
              EL COMPRADOR podrá ingresar a EL SITIO como invitado. En este supuesto,
              EL COMPRADOR podrá navegar sin limitación alguna por EL SITIO para
              ingresar a cualquiera de las Tiendas Afiliadas a CANASTA ROSA sin que
              requiera la creación de una cuenta para navegar y comprar.
            </p>
          </li>
        </ul>
        <h4>PRIVACIDAD</h4>
        <p>
          De conformidad a la Ley Federal de Protección de Datos Personales en
          Posesión de los Particulares, CANASTA ROSA es el responsable de su
          información personal. En este sentido, le sugerimos leer el{' '}
          <a href="https://canastarosa.com/legales/privacidad">
            Aviso de Privacidad
          </a>
          , al cual se sujeta el tratamiento de la información personal de EL
          COMPRADOR en la página web de CANASTA ROSA, sus filiales y/o subsidiarias.
        </p>
        <p>
          CANASTA ROSA en todo momento dará cumplimiento a las leyes de protección de
          datos aplicables y a los términos de su{' '}
          <a href="https://canastarosa.com/legales/privacidad">
            Aviso de Privacidad
          </a>{' '}
          con respecto a los datos e información proporcionados por EL COMPRADOR.
          Derivado de lo anterior CANASTA ROSA hace saber a EL COMPRADOR que podrá
          usar los datos e información que publique para fines estadísticos, de
          mercadotecnia, promocionales, entre otros.
        </p>
        <p>
          El aviso de privacidad podrá ser consultado en la siguiente dirección web:{' '}
          <a href="https://www.canastarosa.com/legales/privacidad">
            https://www.canastarosa.com/legales/privacidad
          </a>
        </p>
        <p>
          El COMPRADOR autoriza a CANASTA ROSA a utilizar su información personal
          para fines propios de la operación en EL SITIO o en los sitios web de
          CANASTA ROSA. CANASTA ROSA por su parte, se obliga a no transmitir, ceder o
          enajenar de forma alguna sus datos a favor de un tercero, salvo que hubiere
          recabado previamente el consentimiento de EL COMPRADOR para dichos efectos,
          a excepción de nombre y correo electrónico para fines informativos,
          promocionales o publicitarios. Si EL COMPRADOR lo desea, en cualquier
          momento y a través de los medios dispuestos para tal efecto, podrá
          solicitar que lo excluyan de las listas para el envío de información
          promocional o publicitaria.
        </p>
        <p>
          CANASTA ROSA utiliza tecnología de cookies y de rastreo de IP cuando un
          COMPRADOR navega por El SITIO o Los Sitios web de CANASTA ROSA, lo anterior
          le permite reconocer a los COMPRADORES registrados sin que tengan que
          ingresar su correo electrónico cada vez que acceden a los Sitios web de
          CANASTA ROSA y recopilar cierta información de sus coincidencias,
          preferencias y operaciones. Recuerda que puedes configurar tu navegador
          para aceptar, gestionar o rechazar todas o algunas cookies, así como
          eliminar las descargadas o instaladas en tu dispositivo. Sin embargo, es
          posible que el rechazo por parte de EL COMPRADOR para que CANASTA ROSA
          mantenga un registro de sus cookies podrá afectar el mejor funcionamiento
          de EL SITIO o de Los Sitios web de CANASTA ROSA con respecto a dicho
          COMPRADOR. En caso de permitir la utilización de esta tecnología, EL
          COMPRADOR consiente que la información personal que así se obtenga, podrá
          ser utilizada para analizar sus preferencias, para definir y ejecutar
          políticas de marketing personalizadas y para otras aplicaciones vinculadas
          en EL SITIO o Los Sitios web de CANASTA ROSA y/o de sus filiales,
          subsidiarias o asociadas.
        </p>
        <h4>CONDICIONES DE COMPRA</h4>
        <p>
          El COMPRADOR podrá comprar en línea entrando al sitio web de CANASTA ROSA
          en <a href="www.canastarosa.com">www.canastarosa.com</a>. En dicho sitio,
          será desplegado un catálogo de tiendas en la sección nominada MARKET.
        </p>
        <p>
          Una vez dentro de la tienda, podrá revisar el desplegado de productos y, en
          caso de querer manifestar su intención de compra, deberá dar clic sobre la
          imagen del producto que se trate; la imagen será ampliada y será desplegada
          la información y disponibilidad de dicho producto, en caso de que la
          información proporcionada se ajuste a las necesidades de EL COMPRADOR éste
          deberá dar clic en el botón COMPRAR.
        </p>
        <p>
          Hecho lo anterior, se desplegará una nueva ventana dividida en los
          siguientes apartados:
        </p>
        <ul>
          <li>
            <p>
              El apartado “Datos de contacto” pedirá al Comprador iniciar sesión para
              comenzar la compra o registrar sus datos de manera manual.
            </p>
          </li>
          <li>
            <p>
              El apartado “Datos de envío” solicitará información acerca del tipo de
              envío que éste requiere, así como calle con número (tanto interior como
              exterior), colonia, alcaldía, código postal, ciudad y estado, donde
              deberá realizarse la entrega.
            </p>
          </li>
          <li>
            <p>
              El apartado “Forma de pago” pide seleccionar como medio de pago
              cualquiera de las opciones que se ofrecen: Tarjeta de crédito o débito;
              PayPal; Depósito Bancario; Depósito en Tiendas de conveniencia.
            </p>
          </li>
        </ul>
        <p>
          Para el caso de realizar la compra con Tarjeta de crédito o débito, misma
          que es procesada por un Tercero designado por CANASTA ROSA (EL AGREGADOR),
          se solicitará información como puede ser pero sin limitarse a: datos de la
          tarjeta (Nombre del titular, Número de tarjeta, Fecha de expiración así
          como el código de seguridad); por políticas de seguridad esta opción de
          pago puede requerir un proceso adicional de confirmación (envío de
          documentos vía electrónica) y/o la opción de realizar un cargo de prueba.
        </p>
        <p>
          Si se selecciona Depósito en Tiendas de conveniencia, se enviará la
          información de la orden y el código de pago al correo electrónico de EL
          COMPRADOR, así como los establecimientos donde podrá realizar el pago
          correspondiente.
        </p>
        <p>
          En la parte derecha de la pantalla se mostrará el nombre de la tienda,
          nombre del producto, costo, número de piezas, descuentos y costos de envío,
          así como el costo total de su compra. Al llenar este formulario, y dar clic
          en el botón 'pagar orden' se enviará automáticamente una notificación vía
          correo electrónico al cliente para informar el estatus de su orden y
          compra. Conforme el proceso de compra actualice el estatus de las órdenes
          generadas, se enviarán notificaciones para el cliente directamente al
          correo electrónico registrado.
        </p>
        <p>
          CANASTA ROSA cuenta con medidas de seguridad disponibles para el manejo de
          la información de compra, así mismo, CANASTA ROSA manifiesta que no tiene
          acceso ni almacena datos sensibles relativos al medio de pago utilizado por
          EL COMPRADOR. Únicamente EL AGREGADOR quien funge como entidad procesadora
          de pago tiene acceso a estos datos a efecto de poder operar las
          transacciones. Es importante resaltar que, EL AGREGADOR es un tercero
          totalmente ajeno a CANASTA ROSA, por lo que CANASTA ROSA no puede responder
          por el uso que dé a dichos datos.
        </p>
        <p>
          Por su parte, CANASTA ROSA, preocupado por la seguridad de sus compradores
          y de las transacciones que éstos realicen a través del Sitio Web de CANASTA
          ROSA, ha implementado un programa de detección de posibles operaciones
          fraudulentas, por lo que CANASTA ROSA se reserva el derecho a solicitar
          información, confirmaciones y/o documentos adicionales a EL COMPRADOR, aún
          después de haber realizado una o más compras a través de EL SITIO, mediante
          llamada telefónica o correo electrónico dirigidos al número o la cuenta
          proporcionada por EL COMPRADOR, con la finalidad de informar a EL AGREGADOR
          que EL COMPRADOR efectivamente realizó y autorizó dicha(s) compra(s) y que
          éste pueda permitir la(s) transacción(es) respectiva(s).
        </p>
        <p>
          Reconoce EL COMPRADOR que CANASTA ROSA no es productor, proveedor,
          expendedor, ni agente de los productos que exhibe, ya que opera sólo como
          una plataforma tecnológica que permite el encuentro de COMPRADORES y
          VENDEDORES para la solicitud de encargos.
        </p>
        <h4>ENVÍO DE PRODUCTOS</h4>
        <p>
          El tiempo de entrega estimado del producto inicia a partir de la
          confirmación de la recepción del pago de la orden por parte de EL
          AGREGADOR. Para realizar la entrega del producto en el domicilio registrado
          por EL COMPRADOR, será necesario que la persona firme de recibido, y se
          identifique con un documento oficial (credencial para votar, pasaporte o
          forma migratoria aplicable en su caso), en cuyo caso el personal de
          mensajería podrá solicitar una copia fotostática del mismo o tomar una
          fotografía del documento de identificación mostrado únicamente para generar
          el expediente correspondiente. En caso de que el producto contenga alcohol,
          tabaco, o cualquier otra substancia exclusiva para mayores de edad, sólo EL
          COMPRADOR podrá recibir el paquete, y deberá presentar una identificación
          oficial que verifique su mayoría de edad.
        </p>
        <p>
          Los productos y/o servicios publicados por las tiendas en el portal de
          CANASTA ROSA no incluyen servicios de instalación, ajustes, configuración
          ni adaptaciones de ningún tipo, salvo que el producto o servicio contratado
          explícitamente lo indique. Para el caso de mensajería, esta se entrega a la
          entrada del domicilio indicado.
        </p>
        <h4>PROTECCIÓN DE PROPIEDAD INTELECTUAL DE CANASTA ROSA</h4>
        <p>
          Los Sitios web de CANASTA ROSA son propiedad de y operados por CANASTA ROSA
          y/o sus empresas afiliadas o subsidiarias y están protegidos por las leyes
          de derecho de autor y propiedad industrial aplicables en los Estados Unidos
          Mexicanos, así como por los tratados internacionales de la materia
          celebrados por los Estados Unidos Mexicanos. Los derechos de autor sobre el
          contenido y el software proporcionado serán en todo momento propiedad de
          CANASTA ROSA. Asimismo, todas las marcas, avisos comerciales, diseño,
          logos, textos, gráficos o cualquier objeto sujeto a la propiedad industrial
          o intelectual, son propiedad de CANASTA ROSA, o bien han sido
          proporcionados con la autorización correspondiente del titular.
        </p>
        <p>
          Toda aquella persona que haga uso de las marcas, avisos comerciales,
          diseños o cualesquier otro derecho de CANASTA ROSA con fines comerciales y
          sin la autorización de CANASTA ROSA, será sujeto a las acciones legales que
          correspondan por derecho a CANASTA ROSA, incluyendo acciones civiles,
          penales o administrativas. Por lo anterior, los Usuarios de este sitio web
          se comprometen a no reproducir ni utilizar la materia objeto de propiedad
          intelectual de este sitio sin la debida autorización.
        </p>
        <p>
          Quedan reservados todos los derechos no otorgados expresamente en éstos
          Términos y Condiciones. El material de los Sitios web de CANASTA ROSA no
          podrá reproducirse, distribuirse o transmitirse de forma alguna sin
          autorización previa y por escrito de CANASTA ROSA. Ningún vínculo a
          cualquiera de los Sitios web de CANASTA ROSA podrá crearse sin autorización
          expresa y por escrito de CANASTA ROSA. EL COMPRADOR de cualquiera de los
          Sitios web de CANASTA ROSA podrá descargar una copia de todo o parte del
          material publicado en dichos Sitios web para su uso personal y sin fines
          comerciales, siempre que no se modifique o altere dicho material de forma
          alguna, ni se elimine o modifique cualquier aviso o leyenda en materia de
          derechos de autor o propiedad industrial. En este acto CANASTA ROSA de
          manera expresa manifiesta que NO otorga ni pretende de manera implícita
          autorizar la cesión o el otorgamiento de cualquier tipo de derecho o
          licencia que recaiga sobre dicho material. Todo el material contenido en
          los Sitios web de CANASTA ROSA se publicará únicamente para fines lícitos.
        </p>
        <p>
          Toda receta que los Usuarios de CANASTA ROSA o la Administración de CANASTA
          ROSA compartan a través del sitio de internet canastarosa.com que no sea
          creación de dichas personas, debe ir acompañada de la fuente de la cual se
          tomó y/o modificó. Lo anterior de conformidad con la legislación de
          derechos de autor y propiedad intelectual vigente en México.
        </p>
        <p>
          Cualquier información que los Usuarios introduzcan en los espacios públicos
          disponibles del sitio CANASTA ROSA será del dominio público, salvo todo
          aquello protegido por la Ley Federal del Derecho de Autor o la Ley de la
          Propiedad Industrial vigente en México.
        </p>
        <p>
          CANASTA ROSA se reserva el derecho de usar, mostrar, modificar, compartir,
          publicar, distribuir, involucrar en trabajos futuros, y usar comercialmente
          fotografías, imágenes, videos, listas, comentarios, ideas, notas, conceptos
          o cualquier otra información, contenido o material, u otro término que los
          Usuarios realicen o carguen al sitio CANASTA ROSA; es decir, el Usuario
          cede y otorga a favor de CANASTA ROSA el derecho sobre, y/o el derecho a
          utilizar de manera indefinida, todos los derechos de autor y propiedad
          intelectual de la información y contenidos que comparta a través del sitio
          canastarosa.com, sin contraprestación de ningún tipo al Usuario ni a
          tercero alguno, con excepción de lo protegido por la legislación de
          propiedad intelectual y/o industrial vigente en México.
        </p>
        <h4>COMPORTAMIENTO ILEGAL</h4>
        <p>
          El COMPRADOR acepta no: (I) Utilizar los Sitios web de CANASTA ROSA para
          fines ilícitos, en violación a las leyes y regulaciones aplicables,
          cualquier disposición de cualquier mercado de valores nacional o de
          cualquier otro tipo, y cualquier regulación que tenga fuerza de ley; (II)
          Publicar páginas web que contengan vínculos que inicien descargas de
          material en franca infracción de propiedad intelectual o que sea ilícito;
          (III) Acosar u hostigar de manera electrónica a otra persona; (IV)
          Participar en cualquier actividad de internet que viole los derechos de
          privacidad de otras personas, incluyendo sin limitación, recabar y
          distribuir información sobre compradores de Internet sin su autorización,
          con excepción de lo que permita la ley; (V) Realizar rifas, sorteos o
          concursos por internet que violen las leyes aplicables.
        </p>
        <h4>COMPORTAMIENTO PERJUDICIAL</h4>
        <p>
          Como condición adicional para el uso y acceso a los Sitios web de CANASTA
          ROSA, EL COMPRADOR acepta no: (I) publicar, distribuir o transmitir
          cualquier gusano, virus, archivo u otro código, archivo o programa
          perjudicial por medio de los sitios web de CANASTA ROSA; (II) ingresar o
          introducir información de carácter obsceno o mecanismos para captar o
          distorsionar información, ni hipervínculos“ links” o publicidad no
          autorizada; (III) ejecutar cualquier programa que ofrezca un servicio o
          recurso a otras personas, incluyendo sin limitación, redireccionamiento de
          puertos o servidores proxy; (IV) interferir, afectar o perjudicar en
          cualquier modo los sitios web de CANASTA ROSA o los servidores o redes
          conectados a dichos sitios; (V) ejecutar programas o configurar
          especialmente equipos de tal manera que mantengan una conexión de marcado
          activa aunque no se use o de cualquier otra forma eludir la desconexión
          automática por inactividad, a menos que se provea una cuenta de acceso
          dedicado; (VI) utilizar los sitios web de CANASTA ROSA en violación de las
          disposiciones de cualquier otro proveedor de sitios web, en dichos sitios
          web, salas de chat o elementos similares; (VII) utilizar los sitios web de
          CANASTA ROSA para acceder a las cuentas de otras personas sin su
          autorización; (VIII) intentar vulnerar las medidas de seguridad de CANASTA
          ROSA o de otra entidad, obtener o eludir las contraseñas de otras personas;
          (IX) participar en ataques de denegación de servicio, esto es, en acciones
          diseñadas para afectar el acceso a la red bombardeando un sitio o cualquier
          red de Internet con tráfico inútil.
        </p>
        <p>
          El equipo de Servicio al Cliente y las tiendas de CANASTA ROSA son parte de
          una comunidad amistosa y de respeto. En el caso de recibir comentarios
          ofensivos, con groserías, o tonos de agresión EL COMPRADOR podría perder su
          derecho de utilizar la plataforma.
        </p>
        <h4>RESTRICCIONES DE EDAD</h4>
        <p>
          Los Sitios web de CANASTA ROSA están diseñados para ser usados por
          individuos que tengan por lo menos 18 años de edad cumplidos en adelante.
          Los compradores menores a 18 años de edad deben obtener la ayuda de sus
          padres o tutores para utilizar el Sitio web.
        </p>
        <h4>EXCLUSIÓN DE RESPONSABILIDAD DE CANASTA ROSA</h4>
        <p>
          Al utilizar los sitios web de CANASTA ROSA sus filiales y/o subsidiarias,
          usted acepta expresamente que el uso de los sitios web de CANASTA ROSA es
          bajo propio riesgo. Los sitios web de CANASTA ROSA se ofrecen tal como
          están y según estén disponibles. CANASTA ROSA no garantiza que el uso de
          los sitios web de CANASTA ROSA estará libre de interrupciones o de errores
          involuntarios.
        </p>
        <p>
          CANASTA ROSA expresamente rechaza garantizar, ya sea expresa o
          implícitamente, incluyendo sin limitar por medio del otorgamiento de
          cualquier garantía sobre la propiedad, comerciabilidad o aptitud que se
          requiera para un fin específico al máximo grado posible bajo las leyes
          aplicables. Ninguna información verbal ofrecida por CANASTA ROSA
          constituirá garantía alguna.
        </p>
        <p>
          CANASTA ROSA no será responsable bajo circunstancia alguna de los daños y
          perjuicios directos, indirectos, incidentales, especiales o
          consecuenciales, causados por el mal uso o inhabilidad de EL COMPRADOR de
          los sitios web de CANASTA ROSA, incluyendo sin limitación, los daños o
          perjuicios que puedan originarse en caso que EL COMPRADOR se apoye de
          información publicada a través de los sitios de CANASTA ROSA, que resulte
          ser producto de un error, omisión, interrupción, supresión y/o, corrupción
          del VENDEDOR o de algún archivo proporcionado por éste. En cuyo caso, El
          VENDEDOR, como el proveedor del servicio de venta y comercialización de
          productos y/o alimentos, será responsable de resarcir cualquier daño o
          perjuicio que le resulte imputable de dicha venta y comercialización. Este
          párrafo aplicará a todo contenido, producto y servicio ofrecido por medio
          de cualquiera de los sitios web de CANASTA ROSA y que no sea atribuible,
          que no quede bajo el control o que no hubiere sido posible prevenir o
          evitar por parte de CANASTA ROSA.
        </p>
        <p>
          CANASTA ROSA en ningún caso garantiza la veracidad y calidad de los
          productos, servicios, información o cualquier otro tipo de material que sea
          adquirido u obtenido por medio de los sitios web de CANASTA ROSA, de igual
          manera CANASTA ROSA no garantiza que los productos adquiridos cumplirán o
          deban cumplir con las expectativas de EL COMPRADOR. El VENDEDOR, como el
          proveedor del servicio de venta y comercialización de productos y/o
          alimentos, será responsable de resarcir cualquier daño o perjuicio que le
          resulte imputable de dicha venta y comercialización.
        </p>
        <h4>RESPONSABILIDAD DEL USUARIO</h4>
        <p>CANASTA ROSA se excluye de responsabilidad sobre lo siguiente:</p>
        <ul>
          <li>
            <p>
              El Usuario es el único responsable sobre el uso que le dé a dicha
              página web. Declara que entra a esta dirección electrónica bajo su
              propia cuenta y riesgo, y que el simple hecho de navegar en este sitio
              implica el consentimiento tácito y expreso del Usuario sobre la
              aceptación y respeto de los presentes términos y condiciones y la
              política de privacidad publicada en la página de CANASTA ROSA.
            </p>
          </li>
          <li>
            <p>
              El uso que se haga de esta página por los Usuarios, se compromete a ser
              únicamente para las propias necesidades de quienes tengan acceso a este
              sitio web, y no a ser utilizado directa o indirectamente o de cualquier
              forma por éstos, para la explotación comercial de los servicios y/o
              productos que CANASTA ROSA pueda ofrecer. Los Usuarios podrán
              electrónicamente copiar e imprimir porciones del sitio solo para
              materiales de uso personal y no comercial. El uso que no cuente con el
              consentimiento por escrito de canastarosa.com se encuentra
              estrictamente prohibido.
            </p>
          </li>
          <li>
            <p>
              CANASTA ROSA no asume la responsabilidad por las comunicaciones hechas
              entre los Usuarios de{' '}
              <a href="www.canastarosa.com">www.canastarosa.com</a>, ni ningún otro
              tipo de contenido o archivos electrónicos compartidos entre estos.
            </p>
          </li>
          <li>
            <p>
              CANASTA ROSA no se hace responsable por el contenido de cualquier otra
              página web que esté ligada a Canastarosa.com; estos sitios pueden no
              están relacionados y/o alineados con los presentes términos, las
              políticas de privacidad o confidencialidad en la información personal.
              CANASTA ROSA no se hace responsable por los datos que el usuario
              comparta o proporcione con dichos sitios web.
            </p>
          </li>
          <li>
            <p>
              Las cuentas y contraseñas que los Usuarios establezcan para
              Canastarosa.com, son responsabilidad única de los Usuarios, y CANASTA
              ROSA no se hace responsable de ninguna forma por las mismas.
            </p>
          </li>
          <li>
            <p>
              CANASTA ROSA se reserva el derecho de cambiar, modificar o suspender el
              contenido de éste sitio web (Canastarosa.com) en cualquier momento y
              sin previo aviso. CANASTA ROSA no será responsable por las
              implicaciones que esto pueda causar en los Usuarios. Además de poder
              restringir en su totalidad el acceso a esta página web y eliminar todo
              tipo de contenido que obre por parte de CANASTA ROSA.
            </p>
          </li>
        </ul>
        <h4>POLÍTICA ANTI-SPAMMING DE CANASTAROSA.COM</h4>
        <p>El Usuario se abstendrá de:</p>
        <ul>
          <li>
            <p>
              Recabar datos con finalidad publicitaria y de remitir publicidad de
              cualquier clase y comunicaciones con fines de venta u otras de
              naturaleza comercial sin que medie su previa solicitud y
              consentimiento;
            </p>
          </li>
          <li>
            <p>
              Remitir cualesquiera otros mensajes no solicitados ni consentidos
              previamente a una pluralidad de personas;
            </p>
          </li>
          <li>
            <p>
              Enviar cadenas de mensajes electrónicos no solicitados ni previamente
              consentidos
            </p>
          </li>
          <li>
            <p>
              Utilizar listas de distribución a las que pueda accederse para la
              realización de las actividades señaladas en los apartados (i) a (iii)
              anteriores;
            </p>
          </li>
          <li>
            <p>
              Poner a disposición de terceros, con cualquier finalidad, datos
              recabados a partir de listas de distribución.
            </p>
          </li>
        </ul>
        <p>
          Los Usuarios o terceros perjudicados por la recepción de mensajes no
          solicitados dirigidos a una pluralidad de personas podrán comunicarlo a
          Canastarosa.com remitiendo un mensaje a la siguiente dirección de correo
          electrónico:{' '}
          <a href="informacion@canastarosa.com">informacion@canastarosa.com</a>.
        </p>
        <h4>NOTIFICACIONES</h4>
        <p>
          CANASTA ROSA y sus afiliadas ejercen sus derechos de propiedad intelectual
          al máximo grado de la ley. En caso de preguntas sobre los avisos legales
          previamente expresados, usted puede comunicarse al Departamento Legal de
          CANASTA ROSA.
        </p>
        <p>
          El domicilio de CANASTA ROSA está ubicado en Bosques de Radiatas 26 Piso 8,
          Bosques de Las Lomas, Cuajimalpa, Cd de México, México, CP. 05120.
        </p>
        <p>
          Las notificaciones serán válidas en las direcciones establecidas por
          CANASTA ROSA y La Tienda, y se presumirán recibidas a más tardar a los
          cinco días hábiles de la fecha de recepción en el acuse de notificación.
        </p>
        <h4>OBSERVANCIA DE LOS TÉRMINOS DE USO</h4>
        <p>
          Estos Términos y Condiciones se rigen e interpretan de acuerdo con las
          leyes de los Estados Unidos Mexicanos, sin referencia a sus disposiciones
          en materia de conflictos de ley. Cualquier y toda controversia sobre la
          validez o interpretación de estos Términos de Uso o su cumplimiento, será
          resuelta exclusivamente por un tribunal competente ubicado en la Ciudad de
          México, el cual constituirá la única sede para cualquier controversia. Si
          cualquier parte de estos Términos de Uso deviene ilegal, inválida o no
          aplicable, dicha parte será separada y no afectará la validez y
          cumplimiento de las disposiciones restantes. CANASTA ROSA podrá modificar
          estos Términos de Uso en cualquier momento mediante la actualización,
          notificación y aceptación de EL COMPRADOR de los nuevos términos y
          condiciones. EL COMPRADOR deberá aceptar dichas modificaciones para
          continuar utilizando los servicios de CANASTA ROSA.
        </p>

        <h5>CANASTA ROSA, SAPI DE CV</h5>
      </section>
      <section className="wrapper--center terms">
        <h4>USO DEL SERVICIO PARA VENDEDORES</h4>
        <p>
          El presente Contrato establece los términos y condiciones de CANASTA ROSA
          S.A.P.I. de C.V., identificado en lo sucesivo bajo el nombre comercial de
          CANASTA ROSA que regulan el acceso y uso del sitio web www.canastarosa.com
          (en adelante, “EL SITIO”) y todos los sitios web adicionales de CANASTA
          ROSA (los sitios web de CANASTA ROSA). Cualquier persona que haga uso de EL
          SITIO, venda a través de EL SITIO, o que haga uso de los servicios que
          presta CANASTA ROSA deberá sujetarse a los presentes términos y condiciones
          junto con las demás políticas establecidas por CANASTA ROSA.
        </p>
        <p>
          Cualquier persona que no acepte estos términos y condiciones, mismos que
          tienen un carácter de obligatorio deberá de abstenerse de utilizar El SITIO
          y/o los servicios prestados por CANASTA ROSA.
        </p>
        <p>
          Los servicios prestados por CANASTA ROSA sólo podrán ser utilizados por
          personas que cuenten con la capacidad legal para contratar.
        </p>
        <h4>POLÍTICAS DE ENTREGA, REEMBOLSO Y CANCELACIÓN DE PEDIDOS:</h4>
        <h5>RECOLECCIONES</h5>
        <p>
          La dirección donde se recolectarán los productos será la ingresada al
          sistema al momento de que una tienda reciba una orden de compra. En caso de
          existir un error en la dirección señalada y la orden no pueda ser
          recolectada, se considerará como una “Recolección Fallida”, y el equipo de
          atención a vendedores se comunicará al remitente.
        </p>
        <p>
          El repartidor puede permanecer un tiempo máximo de 10 minutos. En caso de
          que EL VENDEDOR quisiera programar una nueva recolección, se podrá hacer
          con un costo adicional, mismo que EL VENDEDOR deberá cubrir en su
          totalidad. De lo contrario, se procederá con una cancelación de la orden en
          el lugar de recolección. Si el remitente pide una extensión al tiempo de
          espera permitido, y EL OPERADOR LOGÍSTICO acuerda, se puede cobrar un monto
          adicional a EL VENDEDOR. Si el remitente no se encuentra en el domicilio,
          se tratará de contactar al mismo para esperar instrucciones. En caso de que
          esta opción no sea exitosa, el repartidor continuará su ruta y el equipo de
          atención a vendedores se comunicará para avisar que hubo una “Recolección
          Fallida” al remitente. El embalaje y la entrega completa de las
          especificaciones del producto es responsabilidad de EL VENDEDOR, y como
          consecuencia, es responsable de cualquier gasto que surja a partir de un
          embalaje o envío no apropiado o incompleto.
        </p>
        <h5>DIRECCIONES DE RECOLECCIÓN CON ACCESOS LIMITADOS</h5>
        <p>
          Cuando por políticas internas de una empresa o localidad (como hospitales y
          algunos corporativos), no se permita concretar la recolección como indicó
          EL VENDEDOR, se considerará como una “Recolección Fallida”. En caso de que
          EL VENDEDOR quisiera programar una nueva recolección, se podrá hacer con un
          costo adicional; de lo contrario, se procederá con una cancelación de la
          orden.
        </p>
        <h5>CANCELACIONES POR PARTE DEL VENDEDOR</h5>
        <p>
          En caso de que EL VENDEDOR desee cancelar el pedido de compra deberá de
          comunicarse con el equipo de atención a vendedores antes de que se asigne a
          un OPERADOR LOGÍSTICO para recoger su producto. En caso de que ya se haya
          asignado un OPERADOR LOGÍSTICO, la cancelación puede llevar un costo
          asociado. Cancelaciones de pedidos por parte de EL VENDEDOR puede ser razón
          para suspender la tienda de EL VENDEDOR en la plataforma de CANASTA ROSA.
        </p>
        <h4>CAMBIOS Y DEVOLUCIONES</h4>
        <p>
          El COMPRADOR podrá solicitar el cambio de un producto directamente con EL
          VENDEDOR. Queda a discreción de EL VENDEDOR otorgar este cambio.
        </p>
        <p>
          Si EL COMPRADOR considera que EL VENDEDOR violó una de las políticas de
          CANASTA ROSA, y desea la devolución de su dinero podrá hacer una solicitud
          al equipo de atención a clientes de CANASTA ROSA dentro de las primeras 24
          horas después de recibir su producto. CANASTA ROSA llevará a cabo una
          investigación de las acusaciones. Los resultados de dicha investigación se
          compartirán con EL VENDEDOR y en caso de concordar con el reclamo de EL
          COMPRADOR se procederá a cobrar a EL VENDEDOR el monto para la devolución
          dentro de los siguientes 5 días hábiles.
        </p>
        <h5>PAGOS</h5>
        <p>
          EL VENDEDOR recibirá un pago de todas las órdenes acreedoras a pagos los
          días 15 y 30 de cada mes. En caso de que estos días sean fin de semana o
          festivos, se adelantará un día el pago. Las órdenes acreedoras a pagos son
          aquellas órdenes entregadas en el periodo entre pagos. Cualquier
          penalización o cobro puede ser deducido del monto total a pagar. CANASTA
          ROSA se reserva el derecho de escoger a EL AGREGADOR por el cual se va a
          distribuir este pago.
        </p>
        <h4>INSCRIPCIÓN</h4>
        <p>
          Para hacer uso de los sitios web de CANASTA ROSA o de los servicios
          prestados por CANASTA ROSA, EL VENDEDOR deberá crear una cuenta
          registrándose en la página web de CANASTA ROSA debiendo ingresar la
          información correspondiente en todos los campos con datos válidos de manera
          exacta, precisa y verdadera. En este caso, EL VENDEDOR asume el compromiso
          de actualizar su información cuando exista algún cambio en la información
          previamente ingresada. CANASTA ROSA podrá utilizar diversos medios para
          identificar la información de EL VENDEDOR, sin que esto implique que
          CANASTA ROSA se responsabiliza por la certeza de la información provista
          por la persona que se ostente como EL VENDEDOR. Asimismo, EL VENDEDOR
          garantiza y responde, en cualquier caso, de la veracidad, exactitud,
          vigencia y autenticidad de la información ingresada para su registro.
        </p>
        <p>
          CANASTA ROSA se reserva el derecho de solicitar algún comprobante y/o dato
          adicional con la finalidad de validar la información ingresada por EL
          VENDEDOR, así como de suspender temporal o definitivamente a aquellos
          vendedores cuya información no haya podido ser comprobada. En este caso, se
          dará de baja la cuenta de EL VENDEDOR, sin que ello genere algún derecho a
          resarcimiento.
        </p>
        <p>
          CANASTA ROSA se reserva el derecho de rechazar cualquier solicitud de
          registro o bien a cancelar un registro previamente realizado, con causa
          justificada, sin que esté obligado a notificar a EL VENDEDOR los motivos
          que dieron lugar a rechazar o cancelar el registro y sin que este hecho
          genere algún derecho a indemnización o resarcimiento.
        </p>
        <h4>PRIVACIDAD</h4>
        <p>
          De conformidad a la Ley Federal de Protección de Datos Personales en
          Posesión de los Particulares, CANASTA ROSA es el responsable de su
          información personal. En este sentido, le sugerimos leer el{' '}
          <a href="https://canastarosa.com/legales/privacidad">
            Aviso de Privacidad
          </a>
          , al cual se sujeta el tratamiento de la información personal de EL
          COMPRADOR en la página web de CANASTA ROSA, sus filiales y/o subsidiarias.
        </p>
        <p>
          CANASTA ROSA en todo momento dará cumplimiento a las leyes de protección de
          datos aplicables y a los términos de su{' '}
          <a href="https://canastarosa.com/legales/privacidad">
            https://canastarosa.com/legales/privacidad
          </a>
          con respecto a los datos e información proporcionados por EL VENDEDOR.
          Derivado de lo anterior, CANASTA ROSA hace saber a EL VENDEDOR que podrá
          usar los datos e información que publique para fines estadísticos, de
          mercadotecnia, promocionales entre otros. El aviso de privacidad podrá ser
          consultado en la siguiente dirección web
          <a href="https://canastarosa.com/legales/privacidad">
            https://canastarosa.com/legales/privacidad
          </a>
        </p>
        <p>
          El VENDEDOR autoriza a CANASTA ROSA utilizar su información personal para
          fines propios de la operación en EL SITIO o en los sitios web de CANASTA
          ROSA. CANASTA ROSA por su parte, se obliga a no transmitir, ceder o
          enajenar de forma alguna sus datos a favor de un tercero, salvo que hubiere
          recabado previamente el consentimiento de EL VENDEDOR para dichos efectos,
          a excepción de nombre y correo electrónico para fines informativos,
          promocionales o publicitarios. Si EL VENDEDOR lo desea, en cualquier
          momento y a través de los medios dispuestos para tal efecto, podrá
          solicitar que lo excluyan de las listas para el envío de información
          promocional o publicitaria.
        </p>
        <p>
          CANASTA ROSA utiliza tecnología cookie y de rastreo de IP cuando un
          VENDEDOR navega por El SITIO o los sitios web de CANASTA ROSA, lo anterior
          le permite reconocer a los COMPRADORES registrados sin que tengan que
          ingresar su correo electrónico cada vez que acceden a los Sitios web de
          CANASTA ROSA y recopilar cierta información de sus coincidencias,
          preferencias y operaciones. El rechazo por parte de EL VENDEDOR para que
          CANASTA ROSA mantenga un registro de sus cookies podrá afectar el mejor
          funcionamiento de EL SITIO o de los sitios web de CANASTA ROSA con respecto
          a EL VENDEDOR. En caso de permitir la utilización de esta tecnología, EL
          VENDEDOR consiente que la información personal que así se obtenga, podrá
          ser utilizada para analizar sus preferencias, para definir y ejecutar
          políticas de marketing personalizadas y para otras aplicaciones vinculadas
          en EL SITIO o los sitios web de CANASTA ROSA y/o de sus filiales,
          subsidiarias o asociadas.
        </p>
        <p>
          Conforme a la legislación aplicable en materia de protección de datos
          personales, el apartado “Privacidad” no será aplicable a la información de
          personas físicas en su calidad de comerciantes y profesionistas, así como
          de personas físicas que presten sus servicios para alguna persona moral o
          persona física con actividades empresariales y/o prestación de servicios
          (ej. representante legal, empleados, etc.), consistente únicamente en su
          nombre y apellidos, las funciones o puestos desempeñados, así como algunos
          de los siguientes datos laborales: domicilio físico, dirección electrónica,
          teléfono y número de fax; siempre que esta información sea tratada para
          fines de representación del empleador o contratista.
        </p>
        <h4>CONDICIONES DE VENTA</h4>
        <p>
          EL VENDEDOR reconoce ser el productor, proveedor, expendedor, agente,
          distribuidor y en general cualquier tipo de comercializador de los
          productos que exhibe y vende, tomando toda la responsabilidad sobre el
          contenido que incluye en la plataforma y los productos entregados a EL
          COMPRADOR. EL VENDEDOR confirma tener los derechos y/o autorización
          necesaria para la publicación de las marcas, fotografías y productos que
          suban a la plataforma. CANASTA ROSA se reserva el derecho de eliminar
          cualquier tienda y cualquier producto a la venta en su plataforma con causa
          justificada. EL VENDEDOR no podrá promocionar otro canal de venta distinto
          a CANASTA ROSA dentro de la plataforma de CANASTA ROSA y dentro de las
          órdenes creadas en CANASTA ROSA.
        </p>
        <p>
          Si EL VENDEDOR llega a ser ofensivo, grosero o agresivo con cualquier
          integrante del equipo de CANASTA ROSA, EL VENDEDOR será dado de baja de la
          plataforma.
        </p>
        <h4>PROTECCIÓN DE PROPIEDAD INTELECTUAL DE CANASTA ROSA</h4>
        <p>
          Los Sitios web de CANASTA ROSA son propiedad de y operados por CANASTA ROSA
          y/o sus empresas afiliadas o subsidiarias y están protegidos por las leyes
          de derecho de autor y propiedad industrial aplicables en los Estados Unidos
          Mexicanos, así como por los tratados internacionales de la materia
          celebrados por los Estados Unidos Mexicanos. Los derechos de autor sobre el
          contenido y el software proporcionado serán en todo momento propiedad de
          CANASTA ROSA. Asimismo, todas las marcas, avisos comerciales, diseño,
          logos, textos, gráficos o cualquier objeto sujeto a la propiedad industrial
          o intelectual, son propiedad de CANASTA ROSA, o bien han sido
          proporcionados con la autorización correspondiente del titular. EL VENDEDOR
          otorga a CANASTA ROSA el derecho a utilizar sus marcas, avisos comerciales,
          diseño, logos, textos, gráficos, fotografías o cualquier otro derecho de
          propiedad intelectual sobre su negocio y productos para poder dar
          cumplimiento a los presentes términos y condiciones y mostrar su negocio y
          productos en los Sitios web de CANASTA ROSA.
        </p>
        <p>
          Toda aquella persona que haga uso de las marcas, avisos comerciales,
          diseños o cualesquier otro de los derechos de CANASTA ROSA con fines
          comerciales y sin la autorización de CANASTA ROSA, será sujeto a las
          acciones legales que correspondan por derecho a CANASTA ROSA, incluyendo
          acciones civiles, penales o administrativas. Por lo anterior, los Usuarios
          de este sitio web se comprometen a no reproducir ni utilizar la materia
          objeto de propiedad intelectual de este sitio sin la debida autorización.
        </p>
        <p>
          Quedan reservados todos los derechos no otorgados expresamente en éstos
          Términos y Condiciones. El material de los Sitios web de CANASTA ROSA no
          podrá reproducirse, distribuirse o transmitirse de forma alguna sin
          autorización previa y por escrito de CANASTA ROSA. Ningún vínculo a
          cualquiera de los Sitios web de CANASTA ROSA podrá crearse sin autorización
          expresa y por escrito de CANASTA ROSA. En este acto CANASTA ROSA de manera
          expresa manifiesta que NO otorga ni pretende de manera implícita autorizar
          la cesión o el otorgamiento de cualquier tipo de derecho o licencia que
          recaiga sobre dicho material. Todo el material contenido en los Sitios web
          de CANASTA ROSA se publicará únicamente para fines lícitos.
        </p>
        <p>
          Toda receta que los Usuarios de CANASTA ROSA o la Administración de CANASTA
          ROSA compartan a través del sitio de internet canastarosa.com que no sea
          creación de dichas personas, debe ir acompañada de la fuente de la cual se
          tomó y/o modificó. Lo anterior de conformidad con la legislación de
          derechos de autor y propiedad intelectual vigente en México.
        </p>
        <p>
          Cualquier información que los Usuarios introduzcan en los espacios públicos
          disponibles del sitio CANASTA ROSA será del dominio público, salvo todo
          aquello protegido por la Ley Federal del Derecho de Autor o la Ley de la
          Propiedad Industrial vigente en México.
        </p>
        <p>
          CANASTA ROSA se reserva el derecho de usar, mostrar, modificar, compartir,
          publicar, distribuir, involucrar en trabajos futuros, y usar comercialmente
          fotografías, imágenes, videos, listas, comentarios, ideas, notas, conceptos
          o cualquier otra información, contenido o material, u otro término que los
          Usuarios realicen o carguen al sitio CANASTA ROSA; es decir, el Usuario
          cede y otorga a favor de CANASTA ROSA el derecho sobre, y/o el derecho a
          utilizar de manera indefinida, todos los derechos de autor y propiedad
          intelectual de la información y contenidos que comparta a través del sitio
          canastarosa.com, sin contraprestación de ningún tipo al Usuario ni a
          tercero alguno, con excepción de lo protegido por la legislación de
          propiedad intelectual y/o industrial vigente en México.
        </p>
        <h4>PROPIEDAD INTELECTUAL DE VENDEDORES Y/O TERCEROS</h4>
        <p>
          Usted podrá subir Contenidos al Sitio. Si Usted o cualquier otra persona
          autorizada por Usted sube Contenido al Sitio, Usted será enteramente
          responsable por cualquier reclamación relacionada con su Contenido; lo
          anterior, con independencia de que Usted haya puesto a disposición del
          público su Contenido a través del Sitio, y sin perjuicio de que su
          Contenido se encuentre objetivado en textos, gráficos o audio, a través de
          cualquier mecanismo conocido o por conocerse.
        </p>
        <p>
          Usted mantendrá los derechos de propiedad intelectual que Usted detente
          sobre su Contenido y al poner a disposición su Contenido a través del
          Sitio, usted declara y reconoce que:{' '}
        </p>
        <p>
          No infringe derechos de propiedad intelectual de terceros, incluyendo
          derechos de autor, signos distintivos, invenciones, secretos industriales,
          etc.{' '}
        </p>
        <p>
          Si Usted ha generado su Contenido en su calidad de empleado, o por comisión
          expresa de algún tercero, Usted cuenta con autorización para poner a
          disposición el Contenido a través del Sitio.
        </p>
        <p>
          Su Contenido no es spam, no ha sido generado por una máquina, no está
          sujeto a restricciones legales en alguna materia específica; ni está
          diseñado para conducir el tráfico de usuarios a sitios de terceros, o a
          través de actos ilegales o que puedan confundir a los usuarios sobre la
          fuente de sus Contenidos.
        </p>
        <p>
          Su Contenido no es pornográfico, contrario a la moral o a las buenas
          costumbres, ni incita a violencia en contra de individuos o entidades, así
          como tampoco viola el derecho a la libertad de expresión o derechos de
          privacidad de personas identificadas o identificables.
        </p>
        <p>
          Su Contenido no ha sido preparado o diseñado de forma tal que conduzca a
          que Usted sea confundido por alguna otra persona física o moral, según
          corresponda.{' '}
        </p>
        <p>
          Cuando Usted suba Contenidos al Sitio Usted le confiere al Sitio y se
          obliga a documentar en la forma que el Sitio lo requiera, una licencia
          mundial para usar, almacenar, reproducir, modificar, crear obras derivadas
          (incluyendo aquellas que resulten de la traducción, adaptación o cualquier
          otro cambio que debamos hacer a su Contenido para poder incluirlo en el
          Sitio, para la prestación de nuestros Servicios), divulgar, publicar,
          comunicar o transmitir públicamente y distribuir su Contenido.
        </p>
        <p>
          En caso de que se haga del conocimiento del Sitio que los Contenidos
          dispuestos, habilitados, alojados y/o transmitidos en el Sitio infringen
          Derechos de Terceros; contravienen alguna política interna del Sitio o han
          sido restringidos por resolución emitida por autoridad competente (el
          “Contenido Infractor”), el Sitio podrá, a su sola discreción y sin que ello
          constituya un incumplimiento de su parte a los presentes términos y
          condiciones: (i) remover, retirar, eliminar o inhabilitar el acceso al
          Contenido Infractor; (ii) realizar las acciones que estime necesarias para
          evitar que el Contenido Infractor se vuelva a subir al Sitio, y/o (ii)
          terminar la relación comercial con el responsable del Contenido Infractor,
          sin responsabilidad atribuible al Sitio.{' '}
        </p>
        <p>
          Previo a implementar cualquiera de las acciones señaladas en los apartados
          (ii) y (iii) del párrafo anterior, el Sitio deberá tomar las medidas
          razonables para notificar al responsable del Contenido Infractor,
          otorgándole un plazo de tres días naturales para presentar un contra-aviso,
          en caso de contar con elementos para demostrar la titularidad/autorización
          con la que cuenta para el uso del Contenido Infractor, o justificar su uso
          de acuerdo con la Legislación Aplicable y solicitar que se restaure su
          contenido. Transcurrido el plazo anterior sin que el Sitio haya recibido
          contra-aviso alguno y/o evidencia que lo soporte, procederá a implementar
          cualquiera de las acciones previstas anteriormente bajo los incisos (ii) a
          (iii) según correspondan.{' '}
        </p>
        <p>
          En caso de que el Sitio reciba un contra-aviso dentro del plazo señalado
          anteriormente, deberá en su caso, informar a la persona que haya presentado
          el aviso por el que originalmente se identificó al Contenido Infractor,
          quien tendrá un periodo de 15 días para evidenciar ante el Sitio haber
          iniciado un procedimiento judicial o administrativo, una denuncia penal o
          un mecanismo alterno de solución de controversias. En caso de que
          transcurra en exceso el plazo para que el Sitio reciba evidencia sobre las
          acciones emprendidas en contra del responsable del Contenido Infractor, el
          Sitio habilitará el Contenido, lo cual no le generará responsabilidad
          alguna por daños y perjuicios por violaciones a derechos de propiedad
          intelectual alguno.{' '}
        </p>
        <p>
          Usted reconoce que tratándose de Contenido Infractor, el retiro,
          inhabilitación o suspensión unilateral del mismo, implementada por el Sitio
          de buena fé, con el propósito de impedir la violación de la Legislación
          Aplicable o para cumplir con un mandato de autoridad competente, por lo que
          Usted, no se reserva acción o reclamación alguna en contra del Sitio en
          caso de que se le considere responsable de Contenido Infractor y por ello,
          se le someta al mecanismo previsto en párrafos anteriores.{' '}
        </p>
        <p>
          Para mayor información respecto a posibles disputas en materia de propiedad
          intelectual o Contenido Infractor (atención a avisos y contra-avisos), por
          favor contáctenos a{' '}
          <a href="contacto@canastarosa.com">contacto@canastarosa.com.</a>
        </p>
        <p>
          En caso de detectar algún Contenido Infractor en esta aplicación que
          pudiera violentar sus derechos de autor, podrá hacerlo de nuestro
          conocimiento a través de un aviso que deberá incluir, por lo menos, la
          siguiente información:
        </p>
        <p>
          <ol>
            <li>
              1. Nombre y correo electrónico del titular (o representante legal) del
              Contenido que presuntamente se infringe en el Sitio;
            </li>
            <li>
              2. Identificación del Contenido Infractor y especificar la ubicación
              electrónica del mismo;
            </li>
            <li>
              3. Manifestar el sustento del derecho que le asiste, adjuntando el
              certificado de inscripción de su derecho de autor ante autoridades
              competentes.{' '}
            </li>
          </ol>
        </p>
        <p>
          Al respecto, le comentamos que en términos de la Ley Federal del Derecho de
          Autor, el Sitio, en su calidad de Proveedor de Servicios en Línea, no está
          obligado a supervisar, o monitorear sus redes para buscar activamente
          posibles violaciones a Derechos de Terceros, por lo que a falta de aviso
          con el que se haga de su conocimiento cierto la existencia de una presunta
          infracción, el Sitio se verá imposibilitado a tomar acción alguna en contra
          de dicho contenido, lo cual no le generará responsabilidad alguna por daños
          y perjuicios por violaciones en materia de derechos de autor.{' '}
        </p>
        <h4>COMPORTAMIENTO ILEGAL</h4>
        <p>
          El VENDEDOR acepta no: (I) utilizar los Sitios web de CANASTA ROSA para
          fines ilícitos, en violación a las leyes y regulaciones aplicables,
          cualquier disposición de cualquier mercado de valores nacional o de
          cualquier otro tipo, y cualquier regulación que tenga fuerza de ley; (II)
          publicar páginas web que contengan vínculos que inicien descargas de
          material en franca infracción de propiedad intelectual o que sea ilícito;
          (III) acosar u hostigar de manera electrónica a otra persona; (IV)
          participar en cualquier actividad de internet que viole los derechos de
          privacidad de otras personas, incluyendo sin limitación, recabar y
          distribuir información sobre EL COMPRADOR de internet sin su autorización,
          con excepción de lo que permita la ley, y (V) realizar rifas, sorteos o
          concursos por internet que violen las leyes aplicables.
        </p>
        <h4>COMPORTAMIENTO PERJUDICIAL</h4>
        <p>
          Como condición adicional para el uso y acceso a los sitios web de CANASTA
          ROSA, EL VENDEDOR acepta no: (I) publicar, distribuir o transmitir
          cualquier gusano, virus, archivo u otro código, archivo o programa
          perjudicial por medio de los sitios web de CANASTA ROSA; (II) ingresar o
          introducir información de carácter obsceno o mecanismos para captar o
          distorsionar información, ni hipervínculos “links” o publicidad no
          autorizada; (III) ejecutar cualquier programa que ofrezca un servicio o
          recurso a otras personas, incluyendo sin limitación, redireccionamiento de
          puertos, servidores proxy; (IV) interferir, afectar o perjudicar en
          cualquier modo los sitios web de CANASTA ROSA o los servidores o redes
          conectados a dichos sitios; (V) ejecutar programas o configurar
          especialmente equipos de tal manera que mantengan una conexión de marcado
          activa aunque no se use o de cualquier otra forma eludir la desconexión
          automática por inactividad, a menos que se provea una cuenta de acceso
          dedicado; (VI) utilizar los Sitios web de CANASTA ROSA en violación de las
          disposiciones de cualquier otro proveedor de sitios web, en dichos sitios
          web, salas de chat o elementos similares; (VII) utilizar los sitios web de
          CANASTA ROSA para acceder a las cuentas de otras personas sin su
          autorización; (VIII) intentar vulnerar las medidas de seguridad de CANASTA
          ROSA o de otra entidad, u obtener o eludir las contraseñas de otras
          personas, y (IX) participar en ataques de denegación de servicio, esto es,
          en acciones diseñadas para afectar el acceso a la red bombardeando un sitio
          o cualquier red de Internet con tráfico inútil.
        </p>
        <h4>RESTRICCIONES DE EDAD</h4>
        <p>
          Los sitios web de CANASTA ROSA están diseñados para ser usados por
          individuos que tengan por lo menos 18 años de edad cumplidos en adelante.
        </p>
        <h4>EXCLUSIÓN DE RESPONSABILIDAD DE CANASTA ROSA</h4>
        <p>
          Al utilizar los sitios web de CANASTA ROSA sus filiales y/o subsidiarias,
          usted acepta expresamente que el uso de los sitios web de CANASTA ROSA es
          bajo propio riesgo. Los sitios web de CANASTA ROSA se ofrecen tal como
          están y según estén disponibles. CANASTA ROSA no garantiza que el uso de
          los Sitios web de CANASTA ROSA estará libre de interrupciones o de errores
          involuntarios.
        </p>
        <p>
          CANASTA ROSA expresamente rechaza garantizar, ya sea expresa o
          implícitamente, incluyendo sin limitar por medio del otorgamiento de
          cualquier garantía sobre la propiedad, comerciabilidad o aptitud que se
          requiera para un fin específico al máximo grado posible bajo las leyes
          aplicables. Ninguna información verbal ofrecida por CANASTA ROSA
          constituirá garantía alguna.
        </p>
        <p>
          CANASTA ROSA no será responsable bajo circunstancia alguna de los daños y
          perjuicios directos, indirectos, incidentales, especiales o
          consecuenciales, causados por el mal uso o inhabilidad de EL VENDEDOR de
          los Sitios web de CANASTA ROSA, incluyendo sin limitación, los daños o
          perjuicios que puedan originarse en caso que EL VENDEDOR se apoye de
          información publicada a través de los sitios de CANASTA ROSA, que resulte
          ser producto de un error, omisión, interrupción, supresión y/o, corrupción
          de algún archivo no atribuible a CANASTA ROSA. Este párrafo aplicará a todo
          contenido, producto y servicio ofrecido por medio de cualquiera de los
          sitios web de CANASTA ROSA y que no sea atribuible, no quede bajo el
          control o que no hubiere sido posible prevenir o evitar por parte de
          CANASTA ROSA.
        </p>
        <p>
          El VENDEDOR, como el proveedor del servicio de venta y comercialización de
          productos y/o alimentos, se hace responsable en su totalidad por el
          etiquetado, publicidad y calidad de sus productos en el entendido de que
          éste deberá cumplir con toda la legislación sanitaria aplicable al producto
          que venda y CANASTA ROSA únicamente actuará como intermediario para la
          venta. EL VENDEDOR se compromete y se hará responsable de resarcir
          cualquier daño o perjuicio y de sacar en paz y a salvo a CANASTA ROSA por
          cualquier incumplimiento con lo anterior y/o daño que le resulte imputable
          de dicha venta y comercialización, incluyendo cualquier error, omisión,
          interrupción, supresión y/o, corrupción de la información contenida en
          algún archivo o cualquier publicación que éste le proporcione a CANASTA
          ROSA para su publicación en cualquiera de los sitios web de CANASTA ROSA,
          ya sea ante el Usuario de CANASTA ROSA o ante la Comisión Federal para la
          Protección contra Riesgos Sanitarios, la Procuraduría Federal de Protección
          al Consumidor y cualesquier autoridad que corresponda.
        </p>
        <h4>RESPONSABILIDAD DEL USUARIO</h4>
        <p>CANASTA ROSA se excluye de responsabilidad sobre lo siguiente:</p>
        <ul>
          <li>
            <p>
              El Usuario es el único responsable sobre el uso que le dé a dicha
              página web. Declara que entra a esta dirección electrónica bajo su
              propia cuenta y riesgo, y que el simple hecho de navegar en este sitio
              implica el consentimiento tácito y expreso del Usuario sobre la
              aceptación y respeto de los presentes términos y condiciones y la
              política de privacidad publicada en la página de CANASTA ROSA.
            </p>
          </li>
          <li>
            <p>
              CANASTA ROSA no asume la responsabilidad por las comunicaciones hechas
              entre los Usuarios de Canastarosa.com, ni ningún otro tipo de contenido
              o archivos electrónicos compartidos entre estos.
            </p>
          </li>
          <li>
            <p>
              CANASTA ROSA no se hace responsable por el contenido de cualquier otra
              página web que esté ligada a Canastarosa.com; estos sitios pueden no
              están relacionados y/o alineados con los presentes términos, las
              políticas de privacidad o confidencialidad en la información personal.
              CANASTA ROSA no se hace responsable por los datos que el usuario
              comparta o proporcione con dichos sitios web.
            </p>
          </li>
          <li>
            <p>
              Las cuentas y contraseñas que los Usuarios establezcan para
              Canastarosa.com, son responsabilidad única de los Usuarios, y CANASTA
              ROSA no se hace responsable de ninguna forma por las mismas.
            </p>
          </li>
        </ul>
        <h4>POLÍTICA ANTI-SPAMMING DE CANASTAROSA.COM</h4>
        <p>El Usuario se abstendrá de:</p>
        <ul>
          <li>
            <p>
              Recabar datos con finalidad publicitaria y de remitir publicidad de
              cualquier clase y comunicaciones con fines de venta u otras de
              naturaleza comercial sin que medie su previa solicitud y
              consentimiento;
            </p>
          </li>
          <li>
            <p>
              Remitir cualesquiera otros mensajes no solicitados ni consentidos
              previamente a una pluralidad de personas;
            </p>
          </li>
          <li>
            <p>
              Enviar cadenas de mensajes electrónicos no solicitados ni previamente
              consentidos
            </p>
          </li>
          <li>
            <p>
              Utilizar listas de distribución a las que pueda accederse para la
              realización de las actividades señaladas en los apartados (i) a (iii)
              anteriores;
            </p>
          </li>
          <li>
            <p>
              Poner a disposición de terceros, con cualquier finalidad, datos
              recabados a partir de listas de distribución.
            </p>
          </li>
        </ul>
        <p>
          Los Usuarios o terceros perjudicados por la recepción de mensajes no
          solicitados dirigidos a una pluralidad de personas podrán comunicarlo a
          Canastarosa.com remitiendo un mensaje a la siguiente dirección de correo
          electrónico:{' '}
          <a href="mailto:informacion@canastarosa.com">
            informacion@canastarosa.com
          </a>
          .
        </p>
        <h4>NOTIFICACIONES</h4>
        <p>
          CANASTA ROSA y sus afiliadas ejercen sus derechos de propiedad intelectual
          al máximo grado de la ley. En caso de preguntas sobre los avisos legales
          previamente expresados, usted puede comunicarse al Departamento Legal de
          CANASTA ROSA.
        </p>
        <p>
          La dirección de CANASTA ROSA está ubicada en Avenida Cordillera de los
          Andes 315, Lomas de Chapultepec, Miguel Hidalgo, CDMX, CP 11000.
        </p>
        <p>
          Las notificaciones serán válidas en las direcciones establecidas por
          CANASTA ROSA y La Tienda, y se presumirán recibidas a más tardar a los
          cinco días hábiles de la fecha de recepción en el acuse de notificación.
        </p>
        <h4>OBSERVANCIA DE LOS TÉRMINOS DE USO</h4>
        <p>
          Estos Términos y Condiciones se rigen e interpretan de acuerdo con las
          leyes de los Estados Unidos Mexicanos, sin referencia a sus disposiciones
          en materia de conflictos de ley. Cualquier y toda controversia sobre la
          validez o interpretación de estos Términos de Uso o su cumplimiento, será
          resuelta exclusivamente por un tribunal competente ubicado en la Ciudad de
          México, el cual constituirá la única sede para cualquier controversia. Si
          cualquier parte de estos Términos de Uso deviene ilegal, inválida o no
          aplicable, dicha parte será separada y no afectará la validez y
          cumplimiento de las disposiciones restantes. CANASTA ROSA podrá modificar
          estos Términos de Uso en cualquier momento mediante la actualización,
          notificación y aceptación de EL VENDEDOR de los nuevos términos y
          condiciones. EL VENDEDOR deberá aceptar dichas modificaciones para
          continuar utilizando los servicios de CANASTA ROSA.
        </p>
        <h5>CANASTA ROSA, SAPI DE CV</h5>
      </section>
    </React.Fragment>
  );
}
