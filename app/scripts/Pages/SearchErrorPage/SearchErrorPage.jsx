import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Link } from 'react-router-dom';
import { appActions } from '../../Actions';
import NewStores from '../../components/Home/NewStores';
import CategoriesPopular from '../../Utils/CategoriesPopular';

import '../../../styles/_404-ProductPage.scss';
import './_searchErrorPage.scss';

function SearchErrorPage(props) {
  const { stores = [] } = props;

  return (
    <div className="cr__searchError">
      <div className="cr__searchError--content">
        <div className="cr__searchError--message">
          <h6 className="cr__textColor--colorDark300 cr__text--subtitle">
            Búsqueda inválida.
          </h6>
          <ul>
            <li className="cr__textColor--colorDark300 cr__text--paragraph">
              • Intenta con un término de búsqueda diferente.
            </li>
            <li className="cr__textColor--colorDark300 cr__text--paragraph">
              • Evita utilizar caracteres especiales.
            </li>
          </ul>
          <Link className="cr__textColor--colorMain300 cr__text--paragraph" to="/">
            Regresar al inicio.
          </Link>
        </div>
        <div className="cr__searchError--img">
          <img
            src={require('../../../images/illustration/doll_search.svg')}
            alt="Búsqueda Inálida"
          />
        </div>
      </div>
      <div className="notFoundPage newStores">
        <div className="home">
          <NewStores
            stores={stores
              .filter((store) => store.cover.thumbnail !== '')
              .slice(0, 3)}
          />
        </div>
      </div>

      <CategoriesPopular />
    </div>
  );
}

function mapStateToProps({ app }) {
  const { storesList } = app;
  return {
    stores: storesList.results,
  };
}
function mapDispatchToProps(dispatch) {
  return bindActionCreators({}, dispatch);
}
SearchErrorPage.loadData = (store) =>
  Promise.all([store.dispatch(appActions.getStoresList())]);

// Export Component
export default connect(mapStateToProps, mapDispatchToProps)(SearchErrorPage);
