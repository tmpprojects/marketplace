import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import './Improvements.scss';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Route, Redirect } from 'react-router';
import { SubmissionError } from 'redux-form'; // ES6

import StoreFiscalDataForm from '../components/StoreApp/Store/StoreFiscalDataForm';
import { myStoreActions, statusWindowActions } from './../Actions';
import {
  getBankDataOptions,
  getFiscalDataOptions,
} from './../Reducers/mystore.reducer';

const ValidateImprovements = (props) => {
  const checkFiscal = props.myStoreFiscal.fiscal_registry;
  if (props.improvements.url === '') {
    props.improvements.url = '/my-store/dashboard';
  }
  if (checkFiscal !== undefined && checkFiscal !== null && checkFiscal !== '') {
    return <Redirect to={props.improvements.url} />;
  } else {
    return null;
  }
};

class Improvements extends Component {
  componentDidMount = () => {
    //this.props.fetchFiscalData();
  };

  onChangeFiscalRegistry = (registryType) => {
    this.props
      .updateMyStore({ fiscal_registry: registryType })
      .then((response) => this.props.fetchMyStore());
  };

  componentWillReceiveProps(nextProps) {
    // You don't have to do this check first, but it can help prevent an unneeded render
  }

  addFiscalData = (formValues) => {
    if (
      formValues.hasOwnProperty('fiscalData') !== null &&
      formValues['fiscalData'] !== ''
    ) {
      this.props
        .addFiscalData(formValues)
        .then((response) => {
          this.props.openStatusWindow({
            type: 'success',
            message: 'Información actualizada.',
          });
          this.onChangeFiscalRegistry('with-rfc');
        })
        .catch((error) => {
          // Display UI error messages

          const uiMessage = 'Revisa que todos los campos sean correctos.';
          // const uiMessage =
          //   error.response.data.non_field_errors ||
          //   getUIErrorMessage(error.response, myStoreErrorTypes);
          this.props.openStatusWindow({
            type: 'error',
            message: uiMessage,
          });
          throw new SubmissionError({
            _error: uiMessage,
          });
        });
    } else {
      return false;
    }
  };

  render() {
    const {
      myStore: { ...myStore },
    } = this.props;
    const {
      invoiceOptions,
      bankAccountOptions,
      fiscalData,
      improvements,
    } = this.props;
    const { handleSubmit, options } = this.props;
    const { ...props } = this.props;

    return (
      <div className="improvements-root-container">
        <ValidateImprovements {...props} />

        <div className="improvements-container">
          <div className="improvements-title">
            <div className="logo improvements-logo">
              <a className="logo__button" href="/">
                <img src="/images/header_logo_full.svg" className="imgLogo" />
              </a>
            </div>
            <div className="improvements-text-title">
              Revisa y actualiza tu información.{' '}
            </div>
          </div>

          <div className="improvements-info-title">
            <img src="/images/crPro/icons/icon_school.svg" alt="Cursos" />
            Han ocurrido algunos cambios desde tu última visita, porfavor revísalos y
            actualiza tu información para poder continuar.
          </div>

          <div className="improvements-components">
            <div className="improvements-component">
              <div className="improvements-component-title">Facturación</div>
              <div className="improvements-component-include">
                <StoreFiscalDataForm
                  initialValues={{
                    ...fiscalData,
                    state: fiscalData ? fiscalData.state.value : null,
                    regime: fiscalData ? fiscalData.regime.value : null,
                    //fiscal_registry: !myStore.fiscal_registry ? 'without-rfc' : myStore.fiscal_registry
                    fiscal_registry: !myStore.fiscal_registry
                      ? null
                      : myStore.fiscal_registry,
                  }}
                  onChangeFiscalRegistry={this.onChangeFiscalRegistry}
                  onSubmit={this.addFiscalData}
                  options={invoiceOptions}
                />
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

// Add Redux state and actions to component´s props
function mapStateToProps(state) {
  const { myStore } = state;
  return {
    invoiceOptions: getFiscalDataOptions(state),
    bankAccountOptions: getBankDataOptions(state),
    fiscalData: myStore.data.fiscal_data,
    myStoreFiscal: myStore.data,
    improvements: myStore.improvements,
  };
}

function mapDispatchToProps(dispatch) {
  const {
    fetchMyStore,
    updateMyStore,
    addFiscalData,
    setMyStoreImprovements,
  } = myStoreActions;
  return bindActionCreators(
    {
      fetchMyStore,
      updateMyStore,
      addFiscalData,
      setMyStoreImprovements,
      openStatusWindow: statusWindowActions.open,
    },
    dispatch,
  );
}

// Attach Router Props
Improvements = withRouter(Improvements);

// Export Component
export default connect(mapStateToProps, mapDispatchToProps)(Improvements);

//export default Improvements;
