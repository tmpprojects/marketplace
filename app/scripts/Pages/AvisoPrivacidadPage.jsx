import React from 'react';
import '../../styles/_terms.scss';
import PageHead from '../Utils/PageHead';

const AvisoPrivacidadPage = () => {
  return (
    <section className="wrapper--center terms">
      <PageHead
        attributes={{
          title: 'Aviso de Privacidad | Canasta Rosa',
        }}
      />

      <h1 className="title">Aviso Legal</h1>
      <p className="mt-medium">
        "Canasta Rosa", S.A.P.I. de C.V. ("Canasta Rosa") es una sociedad constituida
        conforme a la legislación de los Estados Unidos Mexicanos ("México") con
        domicilio en la ciudad de México, Distrito Federal. 
      </p>
      <p>
        Canasta Rosa está atento a sus comentarios. Para cualquier duda, pregunta y/o
        comentarios, por favor comuníquese a 
        <a href="mailto:info@canastarosa.com">info@canastarosa.com</a>
      </p>
      <p>
        Canasta Rosa hace del conocimiento de sus usuarios y el público en general
        las siguientes:
      </p>

      <h4>Políticas de uso del sitio web Canastarosa.com</h4>
      <p>
        Las presentes políticas de uso (las "Políticas") del sitio web, origen el uso
        que Usted le dé a dicho sitio y a cualquiera de los contenidos disponibles
        por o a través de este sitio web.
      </p>
      <p>
        Canasta Rosa fomenta el uso y comunicación de datos, publicaciones multimedia
        y contenidos generados tanto por los usuarios de esta página web
        (los "Usuarios"), como todo contenido creado por la administración de Canasta
        Rosa.
      </p>
      <p>
        Canasta Rosa no comparte la base de datos de este sitio referente a los datos
        personales que los Usuarios de este sitio web proporcionen, y efectuará las
        medidas tendientes a la protección de dichos datos. Canastarosa.com contiene
        hipervínculos o hipertextos, links, banners, botones y herramientas de
        búsqueda en la World Wide Web (páginas, promociones, micrositios, tiendas
        virtuales, y otros servicios compartidos con terceros) que al ser oprimidos o
        utilizados por los Usuarios conllevan a otros portales o sitios de Internet
        propiedad de terceros. No obstante que en algunos casos estos sitios o
        portales de terceros se encuentran enmarcados con la barra de navegación o
        el Look & Feel de www.canastarosa.com, la Información que el Usuario llegase
        a proporcionar a través de esos sitios o portales no se encuentra sujeta,
        prevista o alineada a las presentes Políticas y su manejo o utilización no es
        responsabilidad de Canasta Rosa, por lo que se recomienda a los Usuarios
        verificar las políticas de privacidad desplegadas o aplicables a dichos
        sitios o portales de terceros.
      </p>
      <p>
        Cualquier información que los Usuarios introduzcan en los espacios públicos
        disponibles del sitio Canasta Rosa será del dominio público, salvo todo
        aquello protegido por la Ley Federal del Derecho de Autor o la Ley de la
        Propiedad Industrial vigente en México.
      </p>

      <p>
        Canasta Rosa se reserva el derecho de usar, mostrar, modificar, compartir,
        publicar, distribuir, involucrar en trabajos futuros, y usar comercialmente
        fotografías, imagenes, videos, listas, comentarios, ideas, notas, conceptos o
        cualquier otra información, contenido o material, u otro término que los
        Usuarios realicen o carguen al sitio Canasta Rosa; es decir, el Usuario cede
        y otorga a favor de Canasta Rosa todos los derechos de autor y propiedad
        intelectual de la información y contenidos que comparta a través del
        sitio canastarosa.com, sin contraprestación de ningún tipo al Usuario ni a
        tercero alguno, con excepción de lo protegido por la legislación de propiedad
        intelectual y/o industrial vigente en México.
      </p>

      <p>
        Canasta Rosa podrá recolectar información sobre los gustos, preferencias y en
        general de la utilización que hacen los Usuarios del sitio web. Dicha
        información podrá ser utilizada para diversos objetivos comerciales, como:
        proporcionar datos a anunciantes potenciales, enviar publicidad a los
        Usuarios de acuerdo a sus intereses específicos, conducir investigaciones de
        mercadeo, así como las actividades o promociones que Canasta Rosa considere
        apropiadas. Asimismo, Canasta Rosa podrá revelar la información cuando por
        mandato de Ley y/o de autoridad competente le fuere requerido o por
        considerar, de buena fe, que dicha revelación es necesaria para: (i) cumplir
        con procesos legales; (ii) responder reclamaciones que involucren cualquier
        contenido que menoscabe derechos de terceros; o (iii) proteger los derechos,
        la propiedad, o la seguridad de Canasta Rosa, sus Usuarios y el público en
        general.
      </p>

      <p>
        Canasta Rosa se reserva el derecho de evitar y/o eliminar cualquier
        publicación de fotos, contenido escrito o video que vaya en contra de los
        principios de Canasta Rosa o que sean considerados como ilegales, contra el
        orden público o la moral. Por lo que el Usuario de Canasta Rosa se compromete
        a no ingresar cualquier contenido al sitio web considerado como obsceno,
        vulgar, pornográfico, evasivo, abusivo, racial, apologista o constitutivo de
        un delito, de acuerdo con la legislación mexicana, suplantar la identidad de
        otra persona en el sitio web, ofrecer servicios, promociones o publicidad sin
        el consentimiento de Canasta Rosa, entre otras. Asimismo, el Usuario se
        compromete a utilizar los contenidos de Canasta Rosa de conformidad con las
        presentes Políticas del sitio web, y demás avisos puestos en conocimiento del
        público a través de Canastarosa.com
      </p>

      <p>
        Canastarosa.com no recolecta Información proveniente de Usuarios menores de
        trece años residentes en México ni del extranjero.
      </p>

      <h4>Propiedad Intelectual</h4>
      <p>
        Todas las marcas, avisos comerciales, diseño, logos, textos, gráficos o
        cualquier objeto sujeto a la propiedad industrial o intelectual, son
        propiedad de CANASTA ROSA, S.A.P.I. de C.V., o bien han sido proporcionados
        con la autorización correspondiente del titular.
      </p>
      <p>
        Toda aquella persona que haga uso de las marcas, avisos comerciales, diseño o
        cualesquier otro de lo ya mencionado sin la autorización de Canasta Rosa,
        será sujeto de las accionas legales que correspondan por derecho a Canasta
        Rosa, incluyendo acciones civiles, penales o administrativas. Por lo
        anterior, los Usuarios de este sitio web se comprometen a no reproducir ni
        utilizar la materia objeto de propiedad intelectual de este sitio sin la
        debida autorización.
      </p>
      <p>
        Toda receta que los Usuarios de Canasta Rosa o la Administración de Canasta
        Rosa compartan a través del sitio de internet canastarosa.com que no sea
        creación de dichas personas, debe ir acompañada de la fuente de la cual se
        tomó y/o modificó. Lo anterior de conformidad con la legislación de derechos
        de autor y propiedad intelectual vigente en México.
      </p>

      <h4>Responsabilidad</h4>
      <p>Canasta Rosa se excluye de responsabilidad sobre lo siguiente:</p>
      <ul>
        <li>
          <p>
            El Usuario es el único responsable sobre el uso que le dé a dicha página
            web. Declara que entra a esta dirección electrónica bajo su propia cuenta
            y riesgo, y que el simple hecho de navegar en este sitio implica el
            consentimiento tácito y expreso del Usuario sobre la aceptación y respeto
            de las presentes Políticas.
          </p>
        </li>
        <li>
          <p>
            El uso que se haga de esta página por los Usuarios, se compromete a ser
            únicamente para las propias necesidades de quienes tengan acceso a este
            sitio web, y no a ser utilizado directa o indirectamente o de cualquier
            forma por éstos, para la explotación comercial de los servicios y/o
            productos que Canasta Rosa pueda ofrecer. Los Usuarios podrán
            electrónicamente copiar e imprimir porciones del sitio solo para
            materiales de uso personal y no comercial. El uso que no cuente con el
            consentimiento por escrito de canastarosa.com se encuentra estrictamente
            prohibido.
          </p>
        </li>
        <li>
          <p>
            Canasta Rosa no garantiza que el servicio que proporciona a través de la
            página web Canastarosa.com se brindará de manera continua e
            ininterrumpida, así como que estará libre de fallas del servidor y/o
            errores dentro de éste.
          </p>
        </li>
        <li>
          <p>
            Canasta Rosa no asume la responsabilidad por las comunicaciones hechas
            entre los Usuarios de Canastarosa.com, ni ningún otro tipo de contenido o
            archivos electrónicos compartidos entre estos.
          </p>
        </li>
        <li>
          <p>
            Canasta Rosa no se hace responsable por el contenido de cualquier otra
            página web que esté ligada a Canastarosa.com; estos sitios pueden no
            estar relacionados y/o alineados con las presentes Políticas, las
            políticas de privacidad o confidencialidad en la información personal.
            Canasta Rosa no se hace responsable por los datos que el usuario comparta
            o proporcione con dichos sitios web. Canastarosa.com únicamente será
            responsable sobre el manejo de Información proporcionada en sitios
            ligados o de terceros al sitio web, en caso de que expresamente se
            especifique esta responsabilidad y hasta el alcance que se determine en
            las políticas de privacidad que se desplieguen en dichos sitios.
          </p>
        </li>
        <li>
          <p>
            Las cuentas y contraseñas que los Usuarios establezcan para
            Canastarosa.com, son responsabilidad única de los Usuarios, y Canasta
            Rosa no se hace responsable de ninguna forma por las mismas.
          </p>
        </li>
        <li>
          <p>
            Canasta Rosa se reserva el derecho de cambiar, modificar o suspender el
            contenido de éste sitio web (Canastarosa.com) en cualquier momento y sin
            previo aviso. Canasta Rosa no será responsable por las implicaciones que
            esto pueda causar en los Usuarios. Además de poder restringir en su
            totalidad el acceso a esta página web y eliminar todo tipo de contenido
            que obre por parte de Canasta Rosa.
          </p>
        </li>
        <li>
          <p>
            Canasta Rosa se hace responsable por la información financiera ingresada
            en este éste sitio web (Canastarosa.com).
          </p>
        </li>
      </ul>

      <h4>Política Anti-Spamming de Canastarosa.com</h4>
      <p>El Usuario se abstendrá de:</p>
      <ul>
        <li>
          <p>
            Recabar datos con finalidad publicitaria y de remitir publicidad de
            cualquier clase y comunicaciones con fines de venta u otras de naturaleza
            comercial sin que medie su previa solicitud y consentimiento;
          </p>
        </li>
        <li>
          <p>
            Remitir cualesquiera otros mensajes no solicitados ni consentidos
            previamente a una pluralidad de personas;
          </p>
        </li>
        <li>
          <p>
            Enviar cadenas de mensajes electrónicos no solicitados ni previamente
            consentidos
          </p>
        </li>
        <li>
          <p>
            Utilizar listas de distribución a las que pueda accederse para la
            realización de las actividades señaladas en los apartados (i) a (iii)
            anteriores;
          </p>
        </li>
        <li>
          <p>
            Poner a disposición de terceros, con cualquier finalidad, datos recabados
            a partir de listas de distribución.
          </p>
        </li>
      </ul>

      <p>
        Los Usuarios o terceros perjudicados por la recepción de mensajes no
        solicitados dirigidos a una pluralidad de personas podrán comunicarlo
        a Canastarosa.com remitiendo un mensaje a la siguiente dirección de correo
        electrónico: 
        <a href="mailto:info@canastarosa.com">informacion@canastarosa.com</a>.
      </p>
    </section>
  );
};
export default AvisoPrivacidadPage;
