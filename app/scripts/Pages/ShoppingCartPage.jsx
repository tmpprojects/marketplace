import moment from 'dayjs';
import Sentry from '../Utils/Sentry';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { renderRoutes } from 'react-router-config';
import { Link, Redirect } from 'react-router-dom';
import { SubmissionError } from 'redux-form';
import hash from 'object-hash';
import Placeholder from '@canastarosa/ds-theme/assets/images/placeholder/placeholder--productPage.jpg';

import { formValueSelector, getFormSyncErrors, getFormValues } from 'redux-form';
import { default as reduxActions } from 'redux-form/lib/actions';

import withWarning from '../components/hocs/withWarning';
import { getCartProductsCount, getCartProductsTotal } from '../Reducers';
import {
  getCartShippingPrice,
  getUnavailableProducts,
  getCartGrandTotal,
} from '../Reducers/shoppingCart.reducer';
import { trackWithGTM } from '..//Utils/trackingUtils';
import '../../styles/_shoppingCart.scss';
import SEO from '../../statics/SEO.json';
import PageHead from '../Utils/PageHead';
import Register from '../Utils/Register';
import Login from '../Utils/Login';
import FailBuy from '../Utils/modalbox/FailBuy';
import FixedBarCheckout from '../components/ShoppingCart/FixedBarCheckout';
import { PAYMENT_METHODS, SHIPPING_METHODS } from '../Constants/config.constants';
import { shoppingCartErrorTypes } from '../Constants/shoppingCart.constants';
import { userRegisterErrorTypes } from '../Constants/';
import { getUIErrorMessage, getErrorInPayment } from '../Utils/errorUtils';
import { SHOPPING_CART_SECTIONS } from '../Constants';
import SideBar from '../components/ShoppingCart/SideBar';
import AddressManager from '../Utils/AddressManager.jsx';
import {
  shoppingCartActions,
  modalBoxActions,
  userActions,
  statusWindowActions,
  appActions,
} from '../Actions';
import { getAddress } from '../Reducers/users.reducer';
import PromoCode from '../components/ShoppingCart/PromoCode';
import { SHOPPING_CART_FORM_CONFIG } from '../Utils/shoppingCart/shoppingCartFormConfig';
import iconFail from './../../images/icons/icon_fail.svg';
import Covid19 from '../../scripts/Utils/modalbox/Covid/Covid19';
import ImportantMessage from '../../scripts/Utils/modalbox/ImportantMessage/ImportantMessage';
import { list } from '../../scripts/components/ShoppingCart/listStores';
import './../components/ShoppingCart/CheckoutMethods.scss';
import './ShoppingCartPage.scss';
import ErrorBoundery from '../components/StoreApp/Plans/ErrorBoundary/ErrorBoundary';
import { checkOnlineStatus } from '../Utils/genericUtils';

const { change, submit } = reduxActions;

// Assets
const headerFullLogo = require('../../images/logo/header_logo_full--white.svg');
const headerLogo = require('../../images/logo/header_logo.svg');

//
class PaymentError extends Error {
  constructor(message, paymentMethod = '', data = {}) {
    super(message);
    this.name = 'PaymentError';
    this.type = paymentMethod;
    this.data = data;
  }
}

////////////////////////////////////////
////////////////////////////////////////
const validateShippingAddress = (address) => {
  if (!address || !address.zip_code) return false;

  let isValid = true;
  if (!address.num_ext || !address.num_ext.length) {
    isValid = false;
  }
  if (!address.zip_code || address.zip_code.length !== 5) {
    isValid = false;
  }
  if (address.street_address.length < 2) {
    isValid = false;
  }
  if (address.neighborhood.length < 2) {
    isValid = false;
  }
  if (address.city.length <= 2) {
    isValid = false;
  }
  if (address.state.length < 2) {
    isValid = false;
  }

  return isValid;
};

/*-----------------------------------------------
            RequireAccountModal
-----------------------------------------------*/
const RequireAccountModal = (props) => {
  const formSubmit = () => {
    window.location = '/checkout/shipping-payment';
  };

  return (
    <div className="modalWindow requireAccount">
      <div className="message">
        Ingresa a tu cuenta o continua como invitado para terminar tu compra de
        manera segura.
      </div>
      <div className="modalLogin modalLogin-guest">
        <div className="title">
          <h4 style={{ marginBottom: 0 }}>Invitado</h4>
        </div>
        {/*<p>Si quieres continuar tu compra sin crear una cuenta haz click en:{' '}</p>*/}
        <Link
          to="/checkout/shipping-payment"
          className="c2a_square"
          style={{ color: 'white' }}
          onClick={(e) => {
            props.loginAsGuest();
            props.closeModalBox();
          }}
        >
          Continuar como invitado
        </Link>
      </div>

      <Login />
      {/* <h4 className="title">Login</h4>
            <FormLogin onFormSubmit={formSubmit} />

            <div className="requireAccount__register">
                <p className="subtitle">¿A&uacute;n no tienes una cuenta?</p>
                <button
                    className="create-account"
                    onClick={() => openModalBox(Register)} 
                >Reg&iacute;strate gratis aqu&iacute;</button>
            </div> */}
    </div>
  );
};
const errorImage = (e) => {
  e.target.src = Placeholder;
};
/**
 * RenderUnavailableProducts()
 * Renders a list of unavailable products
 * @param {obj} props | Object Properies
 * @returns UI component
 */
const RenderUnavailableProducts = (props) => {
  return (
    <div className="product-unavailableProducts">
      <div className="photo-unavailableProducts">
        {typeof props?.data?.product?.photos[0]?.photo?.small !== undefined ? (
          <img
            src={props?.data?.product?.photos[0]?.photo?.small}
            alt={props.data.product.name}
            onError={errorImage}
          />
        ) : (
            <img src={Placeholder} alt="Error" />
          )}
      </div>
      <div className="details-unavailableProducts">
        <span className="details-unavailableProducts__name">
          {props.data.product.name}
        </span>
        <span className="details-unavailableProducts__store">
          {props.data.product.store.name}
        </span>
        <span className="details-unavailableProducts__price">
          ${props.data.product.price}
        </span>
      </div>
      <div className="button-unavailableProducts">
        <button
          className="button-simple remove"
          onClick={() => {
            const product = props.data;
            props.removeFromCart({
              ...product,
              attribute: props.data.attribute ? props.data.attribute.uuid : null,
            });
          }}
        >
          Eliminar
        </button>
      </div>
    </div>
  );
};

const ShowUnavailable = (props) => {
  //trackWithGTM('eec.checkout.option', { step: 1, option: 'unavailableOrder' });

  if (props.unavailableProducts.length) {
    return (
      <div className="container-unavailableProducts">
        <div className="msg-unavailableProducts">
          <h5 className="module__title">
            <img src={iconFail} className="icon-fail"></img>NO DISPONIBLES
          </h5>
          <div>
            Los siguientes productos marcados con <span className="red">rojo</span>{' '}
            no están disponibles para la dirección seleccionada y no pueden ser
            comprados.
          </div>
        </div>
        <div className="list-unavailableProducts">
          {props.unavailableProducts.map((p, i) => (
            <div key={i}>
              <RenderUnavailableProducts
                data={p}
                removeFromCart={props.removeFromCart}
              ></RenderUnavailableProducts>
            </div>
          ))}
        </div>
      </div>
    );
  } else {
    return null;
  }
};

/*-----------------------------------------------
                ShoppingCartPage
-----------------------------------------------*/
class ShoppingCartPage extends Component {
  // PropTypes / DefaultProps
  static propTypes = {
    cartProductsCount: PropTypes.number,
    cartTotal: PropTypes.number,
  };

  /*
   * Constructor Function
   * @param {object} props React Component Properties
   */
  constructor(props) {
    super(props);
    this.state = {
      section: 'cart',
      isMobile: false,
      isCouponButtonDisabled: !this.props.discountCodeIsValid,
      promoCodeStatusMessage:
        this.props.discountCodeIsValid === true ? 'Código Válido' : '',
    };

    // Class Properties
    this.lastState = undefined;
    this.lastScrollPosition = 0;
    this.lastScrollOrientation = true;
  }
  componentWillMount() {
    this.setCartSection(this.props.location);
  }
  componentDidMount() {
    this.setCartSection(this.props.location);
    this.onResizeHandler();
    // Update Cart
    this.props.fetchShoppingCart();
    this.props.fetchPaymentErrorMessages();
    // For bazar
    const BAZAR_NAME = 'americano';
    this.props.getBazarStoresList('?page_size=99', BAZAR_NAME);

    //
    window.addEventListener('resize', this.onResizeHandler);

    //this.onScroll();
    //window.addEventListener('scroll', this.onScroll);
    if (this.props.isLogged) this.props.getUserCreditCards();
  }
  componentWillReceiveProps(nextProps) {
    const { props: currentProps } = this;
    // Set Cart Section
    if (nextProps.location !== currentProps.location) {
      this.setCartSection(nextProps.location);
      // Update Cart
      currentProps.fetchShoppingCart();
    }

    // Listen to some shopping form values to update cart
    if (currentProps.shoppingCartFormValues) {
      // If user has changed the shipping address...
      if (
        currentProps.shoppingCartFormValues.selectedShippingAddress !==
        nextProps.shoppingCartFormValues.selectedShippingAddress
      ) {
        // Update Cart
        currentProps.fetchShoppingCart();
      }
    }

    // Set 'Promo Code' Status
    if (currentProps.discountCodeIsValid !== nextProps.discountCodeIsValid) {
      this.setState({
        isCouponButtonDisabled: !nextProps.discountCodeIsValid,
        promoCodeStatusMessage:
          nextProps.discountCodeIsValid === true ? 'Código Válido' : '',
      });
    }
  }
  componentWillUnmount() {
    //window.removeEventListener('scroll', this.onScroll);
    window.removeEventListener('resize', this.onResizeHandler);
  }

  /**
   * onResizeHandler()
   * Window onResize Handler
   */
  onResizeHandler = () => {
    let isMobile = false;
    const w =
      window.innerWidth ||
      document.documentElement.clientWidth ||
      document.body.clientWidth;

    if (w < 727) {
      isMobile = true;
    }

    this.setState({
      isMobile,
    });
  };

  /**
   * onScroll()
   * Window onScroll handler
   */
  onScroll = () => {
    const navbar = document.querySelector('.fixed-bar');
    const scrollPosition = window.pageYOffset || document.documentElement.scrollTop;
    let scrollOrientation = this.lastScrollOrientation;

    if (navbar) {
      let currentState = null;
      const scrollOffset = scrollPosition - this.lastScrollPosition;
      const delta = 10;
      const stickyContainer = navbar.querySelector('.fixed-bar__container');

      // Detect delta
      if (Math.abs(scrollOffset) < delta) return;

      navbar.style.height = stickyContainer.style.height;
      const rect = navbar.getBoundingClientRect();
      const bottomPos = rect.top + rect.height;

      // Define corresponding 'stiky bar' layout
      if (
        bottomPos > (window.innerHeight || document.documentElement.clientHeight)
      ) {
        currentState = 'sticky';
      } else {
        currentState = null;
      }

      // Define scroll 'direction'
      scrollOrientation = scrollOffset <= 0;

      // Scroll upwards
      if (scrollPosition > this.lastScrollPosition) {
        // Detect if block needs refresh
        if (
          currentState !== this.lastState ||
          scrollOrientation !== this.lastScrollOrientation
        ) {
          if (currentState === 'sticky') {
            stickyContainer.classList.add(currentState);
            stickyContainer.style.bottom = '0px';
          } else {
            stickyContainer.classList.remove('sticky');
            stickyContainer.style.bottom = '0px';
          }
        }

        // Scroll bacwards
      } else {
        // Detect if block needs refresh
        if (
          currentState !== this.lastState ||
          scrollOrientation !== this.lastScrollOrientation
        ) {
          if (currentState === 'sticky') {
            stickyContainer.classList.add(currentState);
            stickyContainer.style.bottom = `-${stickyContainer.getBoundingClientRect().height
              }px`;
          } else {
            stickyContainer.classList.remove('sticky');
            stickyContainer.style.bottom = '0px';
          }
        }
      }

      this.lastState = currentState;
    }

    // Update Scroll Position
    this.lastScrollOrientation = scrollOrientation;
    this.lastScrollPosition = scrollPosition <= 0 ? 0 : scrollPosition;
  };

  /*
   * On Remove Product
   * Updates the shopping cart after removing a product
   * @param {object} product : Product Object
   */
  onRemoveProduct = (product) => {
    this.props.removeFromCart({
      ...product,
      attribute: product.attribute ? product.attribute.uuid : null,
    });
  };

  /*
   * On Update Product
   * Updates the shopping cart information after updating a product within the cart
   * @param {object} product : Updated Product
   */
  onUpdateProduct = (product) => {
    this.props.updateCartProduct({
      ...product,
      product: product.slug,
      attribute: product.attribute ? product.attribute.uuid : null,
      physical_properties: {
        ...product.physical_properties,
        shipping_date: moment(product.physical_properties.shipping_date).format(
          'YYYY-MM-DD',
        ),
        shipping_schedule: product.physical_properties.shipping_schedule.id,
      },
    });
  };

  /*
   * On Payment Type Selected
   * @param {object} paymentType : Payment Type Details
   */
  onPaymentMethodChange = (paymentType) => {
    this.props.fetchShoppingCart(paymentType);
  };

  dispatchDataLayer() {
    try {

      let dataLayerContent = [];

      this.props.orders.map((order, index) => {

        order.products.map((item, indexProduct) => {
          let addItem = {};

          addItem.name = item?.product?.name
          addItem.id = item?.product?.id
          addItem.price = item?.product?.price
          addItem.brand = item?.product?.store?.name
          addItem.category = null
          addItem.variant = null
          addItem.quantity = item?.quantity

         // console.log("SAVE ITEM", addItem)


          dataLayerContent.push(addItem);
        })
      })
      globalThis.googleAnalytics.checkoutStep1(dataLayerContent)
      globalThis.googleAnalytics.checkoutStep2(this.props.paymentMethod.name)

    } catch (e) {
      console.log("invoke checkoutStep1 [ShoppingCartPage.jsx]", e)
    }
  }

  /*
   * On Complete Order
   * Sends payment to payment providers
   */


  onCompleteOrder = async () => {
    const {
      clientData,
      giftReceiver,
      discountCode,
      paymentMethod,
      shippingMethods,
      addressShipping,
      requiresInvoice,
      orders,
      shoppingCartFormValues,
    } = this.props;

    // console.log(this.props)

    // return false;
    // console.log("this.props clientData", clientData)
    // console.log("this.props paymentMethod", paymentMethod)
    // console.log("this.props all", this.props)
    // return false;

    // Gift information
    let receiverName = null;
    let receiverNote = null;
    let receiverTelephone = null;
    if (shoppingCartFormValues.gift) {
      receiverName = giftReceiver.name;
      receiverNote = giftReceiver.note;
      receiverTelephone = giftReceiver.phone;
    }

    const online = await checkOnlineStatus();
    

    if (online) {

      this.dispatchDataLayer();

      // Construct outter order object for Backend
      const finalOrderData = {
        email: clientData.email,
        orders: orders.reduce((o, order) => {
          if (!order.products.length) return [...o];

          //
          return [
            ...o,
            {
              ...order,
              store: order.store.slug,
              products: order.products.map((product) => {
                const p = {
                  ...product,
                  product: product.slug,
                };
                if (product.attribute) {
                  p.attribute = product.attribute.uuid;
                }
                return p;
              }),
              physical_properties: {
                //...order.physical_properties,
                shipping_date: moment(
                  order.physical_properties.shipping_date,
                ).format('YYYY-MM-DD'),
                shipping_method:
                  order.physical_properties.selected_shipping_method.slug,
                shipping_schedule: order.physical_properties.shipping_schedule.id,
              },
            },
          ];
        }, []),
        telephone: clientData.phone,
        name: `${clientData.client_name} ${clientData.client_lastName}`,
        receiver_name: receiverName,
        receiver_telephone: receiverTelephone,
        receiver_note: receiverNote,
        payment_provider: paymentMethod.type,
        required_invoice: requiresInvoice,
        coupon: discountCode || null,
      };

      // Verify if shipping method requires an address.
      const requiresAddress = shippingMethods.find((sm) => {
        if (
          sm.slug === SHIPPING_METHODS.EXPRESS.slug ||
          sm.slug === SHIPPING_METHODS.EXPRESS_BIKE.slug ||
          sm.slug === SHIPPING_METHODS.EXPRESS_CAR.slug ||
          sm.slug === SHIPPING_METHODS.STANDARD.slug
        ) {
          return true;
        }
        return false;
      });

      // Get Address 'by field' if shipping method is express.
      if (requiresAddress) {
        return this.verifySippingAddress(addressShipping)
          .then((response) => {
            finalOrderData.shipping_address = {
              street: response.street_address,
              neighborhood: response.neighborhood,
              city: response.city,
              state: response.state,
              zip_code: response.zip_code,
              num_ext: response.num_ext,
              num_int: response.num_int,
              latitude: response.latitude,
              longitude: response.longitude,
            };
            finalOrderData.billing_address = finalOrderData.shipping_address;

            return this.makePayment(finalOrderData)
              .then((response) => {
                if (
                  response.payment_status &&
                  (response.payment_status.value === 'awaiting_payment' ||
                    response.payment_status.value === 'in_process' ||
                    response.payment_status.value === 'approved')
                ) {
                  this.props.history.push('/order-success');
                  return;
                }

                if (response && response.type === 'paypal-payment-id') {
                  return response.paymentID;
                }

                // console.log(response);

                //throw response
              })
              .catch((error) => {
                if (error instanceof PaymentError) {
                  let details = '';
                  let title = '';
                  if (error.data) {
                    switch (error.type) {
                      case 'mercado-pago':
                        // details = error.data.payment_response.response.status_detail;

                        switch (error.data.payment_response.response.status_detail) {
                          case 'cc_rejected_insufficient_amount':
                            details = this.props.paymentErrorMessages[
                              'payment_rejected'
                            ].details;

                            title = this.props.paymentErrorMessages[
                              'payment_rejected'
                            ].title;
                            break;
                          case 'cc_rejected_bad_filled_other':
                            details = this.props.paymentErrorMessages['bad_filled']
                              .details;
                            title =
                              his.props.paymentErrorMessages['bad_filled'].title;
                            break;
                        }

                        break;
                      case 'paypal':
                        details = error.data.payment_status;
                        break;
                      default:
                        details = '';
                    }
                  }

                  // Show UI Notification
                  this.showPaymentErrorMessage({
                    title,
                    details,
                  });
                } else if (this.props.orderConfirmationError) {
                  const errorContent = getErrorInPayment(
                    this.props.orderConfirmation_errorInfo,
                    this.props.cartProducts,
                    this.props.paymentErrorMessages,
                  );
                  this.showPaymentErrorMessage(errorContent);
                } else {
                  this.showPaymentErrorMessage();
                }

                //
                Sentry.captureException(error);
                this.props.history.push('/checkout/shipping-payment');
              });
          })
          .catch((error) => {
            // console.log('error', error);
            // alert('Por favor, verifica la dirección de entrega.');
            this.showPaymentErrorMessage({
              title: 'No pudimos completar la compra',
              details:
                'Lo sentimos hubo un problema en el proceso de compra, intenta más tarde.',
            });
            Sentry.captureException(error);
          });
      }

      //
      return this.makePayment(finalOrderData)
        .then((response) => {
          if (
            response.payment_status &&
            (response.payment_status.value === 'awaiting_payment' ||
              response.payment_status.value === 'in_process' ||
              response.payment_status.value === 'approved')
          ) {
            this.props.history.push('/order-success');
            return;
          }

          if (response && response.type === 'paypal-payment-id') {
            return response.paymentID;
          }
        })
        .catch((error) => {
          if (error instanceof PaymentError) {
            let details = '';
            if (error.data) {
              switch (error.type) {
                case 'mercado-pago':
                  details = error.data.payment_response.response.status_detail;
                  break;
                case 'paypal':
                  details = error.data.payment_status;
                  break;
                default:
                  details = '';
              }
            }

            // Show UI Notification
            this.showPaymentErrorMessage({
              title: error.message,
              details,
            });
          } else {
            this.showPaymentErrorMessage();
          }

          //
          Sentry.captureException(error);
          this.props.history.push('/checkout/shipping-payment');
        });
    } else {
      this.showPaymentErrorMessage(
        this.props.paymentErrorMessages['internet_connection'],
      );
    }
  };

  /**
   * makePayment()
   * Select the corresponding payment method and proccess transaction.
   * @params order: <object> | An order group object.
   * @returns Promise | Indicates success or failure of transaction.
   */
  makePayment = (order) => {
    // We must 'switch' logic behaviour according to user preferences.
    // Some payment methods have different implementation 'flows'.
    // Each case depends on API implementations, technical specs, UX flows, etc...
    switch (order.payment_provider) {
      // CREDIT/DEBIT CARDS
      // Currently managed by MERCADO PAGO.
      // This provider could change in the future.
      case PAYMENT_METHODS.MERCADO_PAGO.slug:
        const {
          paymentMethod: { secureForm },
        } = this.props;

        if (secureForm) {
          // 1. Request Mercado Pago API for a 'card token'.
          // We must send an HTML form containing the card details (in this case the 'secureForm' object)
          // The API sends use a time-expiring token in exchange.
          // 2. Handle API´s response either to create an order group, or to notify the user about any problem.
          return new Promise((resolve, reject) => {
            Mercadopago.createToken(secureForm, (status, response) =>
              this.makeMercadoPagoPayment(status, response, order)
                .then((response) => {
                  // Redirect User to Payment Success Page
                  this.props.history.push('/order-success/');
                  resolve(response);
                })
                .catch((error) => {
                  reject(error);
                }),
            );
          }).catch((error) => {
            throw error;
          });
        }
        break;

      // PAYPAL
      case PAYMENT_METHODS.PAYPAL.slug: {
        order.payment_method_id = 'paypal';

        // Send Order to backend API to request a payment.
        // Getting a valid payment request from paypal let the users continue
        // with the PayPal payment flow (popup window)
        return this.props
          .sendPayPalOrder(order)
          .then((response) => ({
            type: 'paypal-payment-id',
            paymentID: response.data.payment_response.id,
          }))
          .catch((error) => {
            throw error;
          });
      }

      // BANK TRANSFER & OXXO
      // This payment methods are UX-seamless for the user.
      // The frontend just sends the order creation request and
      // each case is handled by the backend logic.
      case PAYMENT_METHODS.BANK_TRANSFER.slug:
      case PAYMENT_METHODS.OXXO.slug:
        return this.props
          .sendOrder(order)
          .then((response) => response.data)
          .catch((error) => {
            throw error;
          });

      // Payment Method not selected.
      default:
    }

    // No Payment Method selected
    return Promise.reject(
      new PaymentError('No haz elegido ningún método de pago válido.', 'none'),
    );
  };

  /**
   * onCouponChange()
   * Handler for the promoCode input
   * @param {object} e | Event Object
   */
  onCouponChange = (e) => {
    const { value } = e.target;

    // If user has typed something.,
    // enable the UI validation button.
    if (value && value !== '') {
      this.setState({
        isCouponButtonDisabled: false,
        promoCodeStatusMessage: '',
      });
    } else {
      this.setState({
        isCouponButtonDisabled: true,
        promoCodeStatusMessage: '',
      });
    }
  };

  /**
   * onShippingAddressChange()
   * @param {object} address || Address object.
   */
  onShippingAddressChange = (address, isGuest = false) => {
    const formattedAddress = getAddress(address);

    // Validate shipping address
    return this.verifySippingAddress(formattedAddress)
      .then((response) => {
        this.props.fetchShoppingCart();

        if (!isGuest) {
          this.props
            .updateAddress(formattedAddress)
            .then((response) => {
              this.props.listAddresses();
              this.props.openStatusWindow({
                type: 'success',
                message: 'La direccion se actualizó correctamente.',
              });
            })
            .catch((error) => {
              this.props.openStatusWindow({
                type: 'error',
                message: 'Error. Por favor, intenta de nuevo.',
              });
            });
          this.props.listAddresses();
        }
      })
      .catch((error) => console.log(error));
  };

  verifySippingAddress = (address) => {
    // TODO: Refactor this code
    const prom = new Promise((resolve, reject) => {
      const reason = new Error('Address has an invalid format.');

      // If shipping address has no location coordinates
      // or contains no 'exterior number', open Address Manager Window.
      if (!validateShippingAddress(address)) {
        //
        const ModalWithWarning = withWarning(AddressManager);

        this.props.openModalBox(() => (
          <ModalWithWarning
            initialValues={address}
            onSubmit={(a) => {
              this.props.closeModalBox();
              this.props.updateAddress(a);
              resolve(a);
            }}
            onCancel={() => reject(reason)}
          />
        ));

        // ...Else, continue to next step.
      } else {
        // Close modalbox and save updated address on DB
        this.props.closeModalBox();
        resolve(address);
      }
    });
    return prom;
  };

  /**
   * verifyCoupon()
   * Calls and endpoint to verify if promo code is valid.
   */
  onVerifyCoupon = () => {
    let formattedCoupon = this.props.discountCode;
    if (formattedCoupon === '') {
      this.props.cleanCouponData();
      return;
    }
    if (formattedCoupon) {
      formattedCoupon = formattedCoupon.toUpperCase(); // To uppercase

      //Request validation of promo coupon
      this.props
        .verifyPromoCoupon(formattedCoupon)
        .then((response) => {
          // Update status message
          this.setState({
            promoCodeStatusMessage: 'Código Válido',
          });
        })
        .catch((error) => {
          const defaultMessage = 'Hubo un problema al procesar tu cupón.';
          let uiMessage = '';
          if (error.response.data.detail) {
            uiMessage = error.response.data.detail
              ? error.response.data.detail
              : 'Error desconocido.';
          } else {
            uiMessage = 'Hubo un problema al procesar tu cupón. ';
          }

          const finalUiMessage = uiMessage !== '' ? uiMessage : defaultMessage;

          // Update status message
          this.setState({
            promoCodeStatusMessage: finalUiMessage,
          });

          //
          Sentry.captureException(error);
        });
    }
  };

  /*
   * cleanCouponStateFromShoppingCart()
   * Set Coupon related state to initial values
   */
  cleanCouponStateFromShoppingCart = () => {
    this.setState({
      isCouponButtonDisabled: true,
      promoCodeStatusMessage: '',
    });
  };

  /*
   * Set Cart Selection
   * Define the current 'section' of the checkout process
   * @param {object} location : URL path location
   */
  setCartSection = (location) => {
    let sectionPath = location.pathname.split('/').filter((item) => item !== '');
    sectionPath = sectionPath.slice(-1)[0];
    switch (sectionPath) {
      case SHOPPING_CART_SECTIONS.SHIPPING:
      case SHOPPING_CART_SECTIONS.PAYMENT:
      case SHOPPING_CART_SECTIONS.REVIEW:
      case SHOPPING_CART_SECTIONS.CART:
      case SHOPPING_CART_SECTIONS.SHIPPING_PAYMENT:
        break;
      default:
        sectionPath = 'invalid_section';
    }
    // Update State
    this.setState({
      section: sectionPath,
    });
  };

  /**
   * getOffsetTop()
   * Get the target DOM element offset from the top of the page.
   * @param {object} el | DOM object
   */
  getOffsetTop = (el) => {
    let element = el;
    let top = 0;
    let left = 0;
    do {
      top += element.offsetTop || 0;
      left += element.offsetLeft || 0;
      element = element.offsetParent;
    } while (element);
    return {
      top,
      left,
    };
  };

  /*
   * makeMercadoPagoPayment()
   * Callback that receives a secure Credit Card Token (MercadoPago)
   * @param {int} status : Http Status Code
   * @param {object} response : SDK Response Object
   */
  makeMercadoPagoPayment = (status, response, order) => {
    return new Promise((resolve, reject) => {
      // Clear MercadoPago session.
      // This allows to create another card token if the transaction fails
      // or the card is denied. Without this, MercadoPago WONT! generate a new token.
      Mercadopago.clearSession();

      // If we dont get an OK response, show payment error.
      if (status !== 200 && status !== 201) {
        reject(
          new PaymentError(
            'Por favor, verifica los datos de tu tarjeta.',
            'mercado-pago',
            response,
          ),
        );
      }

      // Add Card Token Hidden Field to Form
      const {
        paymentMethod: { secureForm: form },
      } = this.props;
      form.querySelector('input[name=token]').value = response.id;
      order.payment_card_token = response.id;
      order.payment_method_id = this.props.paymentMethod.cardProvider
        ? this.props.paymentMethod.cardProvider
        : null;
      if (this.props.paymentMethod.isStoredCard) {
        order.customer_id = this.props.paymentMethod.customerId;
      }

      // Send Order to Backend
      this.props
        .sendOrder(order)
        .then((orderResponse) => {
          const paymentResponse = orderResponse.data.payment_response;

          // Handle Payment Responses
          if (paymentResponse.status === 201) {
            switch (paymentResponse.response.status) {
              // Approved
              case 'approved':
              case 'in_process':
                // If the user used a new credit/debit card and
                // choosed to store it on the database via the UI
                // send card details to the backend.
                if (this.props.paymentMethod.saveCreditCard) {
                  this.props.addCreditCard({
                    token: order.payment_card_token,
                  });
                }

                //
                resolve(orderResponse.data);
                break;

              case 'rejected':
              default:
                reject(
                  new PaymentError(
                    'Lo sentimos, pero tu tarjeta fue rechazada. Por favor intenta con otro método de pago.',
                    'mercado-pago',
                    orderResponse.data,
                  ),
                );
                break;
            }
          } else {
            reject(
              new PaymentError(
                'Lo sentimos, pero no pudimos generar tu pago. Por favor, revisa tus datos e intenta de nuevo.',
                'mercado-pago',
                orderResponse.data,
              ),
            );
          }
        })
        .catch((error) => {
          reject(error);
        });
    });
  };

  /**
   * makePayPalPayment()
   * Send an order and payment attempt to PayPal through the backend API.
   * @param {object} paymentData : Order and Payment information
   */
  makePayPalPayment = (paymentData) => {
    return this.props
      .payPalAuthorize(paymentData)
      .then((response) => {
        // Handle Payment Responses
        // Check if http response is OK and order has been paid
        if (response.status === 200 && response.data.paid) {
          // Verify Payment Status.
          // (Payment Status: 3 - Paid, 2 - Refused, 1 - Awaiting)
          if (
            response.data.payment_status.value !== 'approved' &&
            response.data.payment_status.value !== 'in_process'
          ) {
            throw new PaymentError(
              'Lo sentimos, pero tu pago fue rechazada. Por favor intenta con otro método de pago.',
              'paypal',
              response.data,
            );
          }

          // Redirect to success page
          this.props.history.push('/order-success');

          // Other http responses different form 201 (created) are invalid.
        } else {
          throw new PaymentError(
            'Lo sentimos, pero no pudimos generar tu pago. Por favor, revisa tus datos e intenta de nuevo.',
            'paypal',
            response.data,
          );
        }
      })
      .catch((error) => {
        throw error;
      });
  };

  /**
   * paymentErrorMessage()
   * Notify users of an error on their attempt to make a payment.
   */
  showPaymentErrorMessage = (content) => {
    let title = 'Tu compra no pudo completarse';
    let message =
      '<p>Lo sentimos. Por algún motivo ajeno a nosotros, no pudimos procesar tu pago.</p>';

    //
    if (content && content.title !== '') {
      title = content.title;
    }
    if (content && content.details !== '') {
      message = `<br/>${content.details}`;
    }

    //
    this.props.openModalBox(() => (
      <FailBuy
        link="/checkout/shipping-payment"
        linkTitle="Regresar"
        onClick={this.props.closeModalBox}
        title={title}
        content={`${message}`}
      />
    ));
  };

  /*
   * ON ADD ADDRESS
   * Callback when a user creates a new address.
   * @param {object} formValues : New Address Object
   */
  onAddAddress = (formValues) => {
    if (this.props.isLogged) {
      // Create Address
      return this.props
        .addAddress(formValues)
        .then((response) => {
          // Update selected address reference
          this.props.dispatch(change('selectedShippingAddress', response.data.uuid));
          this.onShippingAddressChange(response.data);

          // close modal window
          this.props.closeModalBox();
        })
        .catch((error) => {
          const uiMessage = getUIErrorMessage(
            error.response,
            userRegisterErrorTypes,
          );
          throw new SubmissionError({
            _error: uiMessage,
          });
          Sentry.captureException(error);
        });
    } else {
      const address = { ...formValues, uuid: hash(formValues) };
      this.props.deleteGuestAddress();
      return this.props
        .addGuestAddress(address)
        .then((response) => {
          // TODO: Refactor this
          this.props.dispatch(change('selectedShippingAddress', response.uuid));
          this.onShippingAddressChange(response, !this.props.isLogged);

          // close modal window
          this.props.closeModalBox();
        })
        .catch((e) => {
          Sentry.captureException(error);
        });
    }
  };

  navigateToCheckoutStep = (step: number = 1) => {
    let promiseHandler = new Promise();

    //
    switch (step) {
      case 2:
        if (!this.props.isLogged && !this.props.isGuest) {
          window.location = '/cart';
          return promiseHandler.reject('User cant access to this page');
        }
        window.location = '/checkout/shipping-payment';
        return promiseHandler.resolve();
        break;

      case 1:
        window.location = '/cart';
        return promiseHandler.resolve();
      default:
    }

    return promiseHandler.reject('Checkout step not found');
  };

  openAccountManagerWindow = () => {
    this.props.openModalBox(() => (
      <RequireAccountModal
        history={this.props.history}
        openModalBox={this.props.openModalBox}
        closeModalBox={this.props.closeModalBox}
        loginAsGuest={this.props.loginAsGuest}
      />
    ));
  };

  /**
   * render()
   * Render component
   */
  render() {
    const {
      section,
      isMobile,
      isCouponButtonDisabled,
      promoCodeStatusMessage,
    } = this.state;
    const {
      orders,
      products,
      cartProductsCount,
      addressShipping,
      referenceShippingAddress,
      cartTotal = 0.0,
      isLogged,
      user,
      isGuest,
      unavailableProducts,
      grandTotal,
      ...props
    } = this.props;

    // To proceed to checkout, user must be logged-in or pass as a guest user
    if (section !== 'cart' && !isLogged && !isGuest) {
      this.openAccountManagerWindow();
      return <Redirect to="/cart" />;
    }

    // Detect in what checkout step we are currently in
    // and calculate if we need to redirect the users
    switch (section) {
      case SHOPPING_CART_SECTIONS.REVIEW:
        if (
          !props.shippingMethod ||
          (props.shippingMethod === SHIPPING_METHODS.EXPRESS.slug &&
            !props.addressShipping) ||
          !props.paymentMethod ||
          !props.paymentMethod.isValid ||
          !orders.length
        ) {
          return <Redirect to="/cart" />;
        }
        break;
      case SHOPPING_CART_SECTIONS.SHIPPING_PAYMENT:
        if (!orders || !orders.length) {
          return <Redirect to="/cart/" />;
        }
        break;
      case SHOPPING_CART_SECTIONS.SHIPPING:
        if (!orders || !orders.length) {
          return <Redirect to="/cart/" />;
        }
        break;
      case SHOPPING_CART_SECTIONS.PAYMENT:
        if (
          !props.shippingMethod ||
          (props.shippingMethod === SHIPPING_METHODS.EXPRESS.slug &&
            !props.addressShipping.uuid)
        ) {
          return <Redirect to="/cart" />;
        }
        break;
      default:
      case SHOPPING_CART_SECTIONS.CART:
        break;
    }

    // Show a messgae if there are no products on cart
    if (cartProductsCount < 1) {
      return (
        <div style={{ margin: '12rem auto', textAlign: 'center' }}>
          No has agregado ningún producto a tu carrito.
        </div>
      );
    }

    /**
     * When a item on the cart matches a slug of
     * stores on promotion turns true an validates the coupon
     */
    /**
     * Validate different coupons from multiples stores.
     */
    // const ordersSlugs = orders.map((order) => order.store.slug);
    // const found = list.some(store => ordersSlugs.includes(store.slug));
    /**
     * Validate one coupon for different stores
     */
    const ordersSlugs = orders.map((order) => order.store.slug);
    //console.log('orderSlugs', ordersSlugs);
    const found = list.some((slug) => ordersSlugs.includes(slug));
    //console.log('found', found);

    /**
     * Validate one coupon for different stores using List of stores from backend
     */
    // const ordersSlugs = orders.map((order) => order.store.slug);
    // const found = this.props.storesBazarList?.some(({ slug }) =>
    //   ordersSlugs.includes(slug),
    // );
    //

    // Promo Code Component (for mobile and desktop)
    const promoCodeButton = (
      <PromoCode
        onCouponChange={this.onCouponChange}
        onVerifyCoupon={this.onVerifyCoupon}
        promoCodeStatusMessage={promoCodeStatusMessage}
        isCouponButtonDisabled={isCouponButtonDisabled}
        discountCodeState={this.props.discountCodeState}
        cleanCouponData={this.props.cleanCouponData}
        cleanCouponStateFromShoppingCart={this.cleanCouponStateFromShoppingCart}
        /**AUTO COUPON VALIDATION ONLY */
        foundSlug={found}
        cartTotal={cartTotal}
      />
    );
    return (
      <div className="cart_container shoppingCart">
        <PageHead attributes={SEO.CheckoutPage} />
        {/* CHECKOUT HEADER (without navigation) */}
        {section !== SHOPPING_CART_SECTIONS.CART && (
          <header className="shoppingCart__header">
            <div className="navigation">
              <div className="logo">
                <Link to="/">
                  <picture>
                    <source media="(min-width: 36em)" srcSet={headerFullLogo} />
                    <img src={headerLogo} alt="Home" />
                  </picture>
                </Link>
              </div>

              <div className="secure_checkout">Compra Segura</div>
            </div>
          </header>
        )}
        {/* END: CHECKOUT HEADER */}

        {/* COVID BANNER */}
        {!isMobile &&
          // && section !== 'cart'
          section !== 'review' && (
            <div style={{ marginTop: '0.5em' }}>
              {' '}
              {/* <Covid19 />{' '} */}
              {/* <ImportantMessage /> */}
            </div>
          )}

        {/* UNAVAILABLE PRODUCTS WARNING */}
        {unavailableProducts.length > 0 && (
          <div className="statusNotificationsWindow--warning">
            <div className="statusNotificationsWindow__content">
              Algunos de los productos en tu carrito&nbsp;
              <strong>no están disponibles</strong>&nbsp;
              {referenceShippingAddress && (
                <React.Fragment>
                  para entregar en&nbsp;
                  <strong>
                    {referenceShippingAddress.street_address}&nbsp;
                    {referenceShippingAddress.num_ext}...&nbsp; CP.{' '}
                    {referenceShippingAddress.zip_code}
                  </strong>
                  .<br />
                  Cambiar de dirección:&nbsp;
                  <select
                    name=""
                    defaultValue={
                      user.addresses.addresses.find(
                        (a) => a.uuid === referenceShippingAddress.uuid,
                      ).uuid
                    }
                    onChange={(e) => {
                      e.preventDefault();
                      if (e.target.value === 'new_address') {
                        new Promise((resolve, reject) => {
                          /* */
                          this.props.openModalBox(() => (
                            <AddressManager
                              onSubmit={this.onAddAddress}
                              title="Nueva Dirección"
                            />
                          ));
                        });
                      } else {
                        const test = user.addresses.addresses.find(
                          (a) => a.uuid === e.target.value,
                        );
                        this.onShippingAddressChange(test);
                        // console.log('test', test);
                        // this.props
                        //   .updateAddress(test)

                        // .then((response) => {
                        //   this.props.openStatusWindow({
                        //     type: 'success',
                        //     message: 'La dirección se actualizó correctamente.',
                        //   });
                        // })

                        // .catch((error) => {
                        //   this.props.openStatusWindow({
                        //     type: 'error',
                        //     message: 'Error. Por favor, intenta de nuevo.',
                        //   });
                        // });
                      }
                    }}
                  >
                    <option value={'new_address'} key={0}>
                      Nueva Dirección
                    </option>
                    {user.addresses.addresses.map((a) => (
                      <option value={a.uuid} key={a.uuid}>
                        {a.street_address}&nbsp;
                        {a.num_ext}...&nbsp;
                      </option>
                    ))}
                  </select>
                </React.Fragment>
              )}
              {/*
              <br />
              {unavailableProducts.map((p, i) => (
                <div key={i}>
                  <strong key={p.id}>
                    {i + 1}- {p.product.name} de {p.product.store.name}.
                  </strong>

                  <RenderUnavailableProducts
                    product={p}
                  ></RenderUnavailableProducts>
                </div>
              ))}
              */}
            </div>
          </div>
        )}
        {/* END: UNAVAILABLE PRODUCTS WARNING */}

        <main className="cart wrapper--center">
          {/* UNAVAILABLE PRODUCTS LIST */}
          <ErrorBoundery>
            <ShowUnavailable {...this.props}></ShowUnavailable>
          </ErrorBoundery>
          {/* UNAVAILABLE PRODUCTS LIST */}

          {/* <p className="cart__counter">
            <span>{cartProductsCount}</span> 
            artículo{cartProductsCount !== 1 ? 's' : ''} en tu carrito
          </p> */}

          {/* CHECKOUT SECTIONS */}
          <div className="order_container cart_container">
            {renderRoutes(this.props.route.routes, {
              dispatch: this.props.dispatch,
              onRemoveProduct: this.onRemoveProduct,
              onUpdateProduct: this.onUpdateProduct,
              onCreditCardSubmit: this.onCreditCardSubmit,
              onShippingAddressChange: this.onShippingAddressChange,
              onPaymentMethodChange: this.onPaymentMethodChange,
              onSecurityCodeValidate: this.onSecurityCodeValidate,
              setShoppingCartUpdateStatus: this.props.setShoppingCartUpdateStatus,
            })}
          </div>
          {/* END: CHECKOUT SECTIONS */}

          {/* SIDEBAR */}
          {orders.length > 0 && (
            <React.Fragment>
              <SideBar
                className={`
                  cart-resume
                  ${section === SHOPPING_CART_SECTIONS.CART
                    ? ''
                    : 'cart-resume--pushTop'
                  }`}
                section={section}
                isMobile={isMobile}
                products={products}
                couponComponent={promoCodeButton}
                discountCodeState={props.discountCodeState}
                confirmationSubmit={this.onCompleteOrder}
                verifyCoupon={this.onVerifyCoupon}
                onPayPalAuthorize={this.makePayPalPayment}
                onPaymentSuccess={props.paymentSuccess}
                onPaymentError={props.paymentError}
                onPaymentCancel={props.paymentCancel}
                cartProductsCount={cartProductsCount}
                cart_price={cartTotal}
                shipping_price={props.shippingPrice}
                grand_total={grandTotal}
                paymentMethod={this.props.paymentMethod}
                openAccountManagerWindow={this.openAccountManagerWindow}
                // PROP for validate coupon Buena vibra bazar for(message)
                found={found}
              />

              {isMobile && section === SHOPPING_CART_SECTIONS.SHIPPING_PAYMENT && (
                <div className="form promo-code">{promoCodeButton}</div>
              )}

              {isMobile && (
                <FixedBarCheckout
                  cartTotal={cartTotal}
                  grandTotal={grandTotal}
                  productsCount={cartProductsCount}
                  shippingPrice={props.shippingPrice}
                  section={section}
                  discountCodeState={props.discountCodeState}
                  confirmationSubmit={this.onCompleteOrder}
                  onPayPalAuthorize={this.makePayPalPayment}
                  onPaymentSuccess={props.paymentSuccess}
                  onPaymentError={props.paymentError}
                  onPaymentCancel={props.paymentCancel}
                  paymentMethod={this.props.paymentMethod}
                  syncErrors={this.props.syncErrors}
                  openAccountManagerWindow={this.openAccountManagerWindow}
                  isLogged={this.props.isLogged}
                  isGuest={this.props.isGuest}
                />
              )}
            </React.Fragment>
          )}
          {/* END: SIDEBAR */}
        </main>
      </div>
    );
  }
}

// Load Data for Server Side Rendering
ShoppingCartPage.loadData = (reduxStore, routePath) => Promise.all([]);

// Pass Redux state and actions to component
const selector = formValueSelector(SHOPPING_CART_FORM_CONFIG.formName);
function mapStateToProps(state) {
  const { users, cart, app } = state;
  const orders = selector(state, 'orders') || [];

  // Discount Code
  const couponIsValid = cart.couponDetails.valid;
  const discountCode =
    couponIsValid === true ? state.cart.coupon : selector(state, 'discount_code');
  let discountCodeState = 'idle';
  if (discountCode && couponIsValid) {
    discountCodeState = 'valid';
  } else if (discountCode && !couponIsValid) {
    discountCodeState = 'invalid';
  }
  const discountCodeIsValid = couponIsValid;
  let referenceShippingAddress = selector(state, 'addressShipping');
  if (!referenceShippingAddress) {
    referenceShippingAddress = users.addresses.addresses
      ? users.addresses.addresses.find((s) => s.default_address)
      : null;
  }

  // Pass Properties
  return {
    isLogged: users.isLogged,
    isGuest: users.isGuest,
    shoppingCartFormValues:
      getFormValues(SHOPPING_CART_FORM_CONFIG.formName)(state) || {},
    cartTotal: getCartProductsTotal(state),
    grandTotal: getCartGrandTotal(state),
    clientData: selector(state, 'clientData'),
    giftReceiver: selector(state, 'giftReceiver'),
    secureSavedCardForm: selector(state, 'secureSavedCardForm'),
    customer_id: selector(state, 'customer_id'),
    orders,
    cartProductsCount: getCartProductsCount(state),
    paymentMethod: selector(state, 'paymentMethodDetails'),
    discountCode,
    discountCodeState,
    discountCodeIsValid,
    user: users,
    unavailableProducts: getUnavailableProducts(state),
    shippingMethods:
      orders.reduce((order, o) => {
        if (!o.products.length) return order;
        return [...order, o.physical_properties.selected_shipping_method];
      }, []) || [], //selector(state, 'shippingMethod'),
    addressBilling: selector(state, 'addressBilling'),
    addressShipping: selector(state, 'addressShipping'),
    referenceShippingAddress,
    requiresInvoice: selector(state, 'requiresInvoice'),
    shippingPrice: getCartShippingPrice(state),
    syncErrors: getFormSyncErrors(SHOPPING_CART_FORM_CONFIG.formName)(state),
    storesBazarList: state.app.storesBazarList.results,
    orderConfirmationError: state.cart.order_confirmation.error,
    orderConfirmation_errorInfo: state.cart.order_confirmation.error_info,
    cartProducts: state.cart.products,
    paymentErrorMessages: state.cart.paymentErrorMessages,
    //ordersByStoreAndShipping: getCartProductsByStoreAndShipping(state),
  };
}

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
    ...bindActionCreators(
      {
        removeFromCart: shoppingCartActions.removeFromCart,
        updateCartProduct: shoppingCartActions.updateCartProduct,
        fetchShoppingCart: shoppingCartActions.fetchShoppingCart,
        setShoppingCartUpdateStatus: shoppingCartActions.setShoppingCartUpdateStatus,
        fetchPaymentErrorMessages: shoppingCartActions.fetchPaymentErrorMessages,
        addAddress: userActions.addAddress,
        addGuestAddress: userActions.addGuestAddress,
        deleteGuestAddress: userActions.deleteGuestAddress,
        updateAddress: userActions.updateAddress,
        listAddresses: userActions.listAddresses,
        addCreditCard: userActions.addCreditCard,

        loginAsGuest: userActions.loginAsGuest,
        getUserCreditCards: userActions.getUserCreditCards,

        openModalBox: modalBoxActions.open,
        closeModalBox: modalBoxActions.close,
        sendOrder: shoppingCartActions.sendOrder,
        verifyPromoCoupon: shoppingCartActions.verifyPromoCoupon,
        cleanCouponData: shoppingCartActions.cleanCouponData,
        submit,

        sendPayPalOrder: shoppingCartActions.sendPayPalOrder,
        payPalAuthorize: shoppingCartActions.payPalAuthorize,
        paymentSuccess: shoppingCartActions.paymentSuccess,
        paymentError: shoppingCartActions.paymentError,
        paymentCancel: shoppingCartActions.paymentCancel,

        openStatusWindow: statusWindowActions.open,

        getBazarStoresList: appActions.getBazarStoresList,
      },
      dispatch,
    ),
  };
}

// Export Component
export default connect(mapStateToProps, mapDispatchToProps)(ShoppingCartPage);
