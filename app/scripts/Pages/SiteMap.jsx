import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

export const SiteMap = (props) => <div>SiteMap</div>;
SiteMap.loadData = (store) => Promise.all([]);
// Map Redux Props and Actions to component
function mapStateToProps(state) {
  return {};
}
function mapDispatchToProps(dispatch) {
  return bindActionCreators({}, dispatch);
}

// Export Component
export default connect(mapStateToProps, mapDispatchToProps)(SiteMap);
