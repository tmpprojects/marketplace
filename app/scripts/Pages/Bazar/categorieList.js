export const list = [
  {
    name: 'Comida',
    slug: 'comida-v2',
    img: require('../../../images/assets_bazar/categorias/comida.jpg'),
    key: 1,
  },
  // {
  //   name: 'Diseño',
  //   slug: 'disenio-1',
  //   img: require('./assets/categoryImg/Libreta.jpg'),
  //   key: 2,
  // },
  // {
  //   name: 'Flores y Regalos',
  //   slug: 'Flores-y-Regalos',
  //   img: require('./assets/categoryImg/Flores.jpg'),
  //   key: 3,
  // },
  {
    name: 'Ropa y Calzado',
    slug: 'ropa-calzado-y-accesorios-v2',
    img: require('../../../images/assets_bazar/categorias/ropaYCalzado.jpg'),
    key: 4,
  },
  {
    name: 'Joyería',
    slug: 'materiales-joyeria-v2',
    img: require('../../../images/assets_bazar/categorias/accesorios.jpg'),
    key: 5,
  },
  {
    name: 'Infantil',
    slug: 'infantil-v2',
    img: require('../../../images/assets_bazar/categorias/infantil.jpg'),
    key: 6,
  },
  {
    name: 'Salud y Belleza',
    slug: 'salud-y-belleza-v2',
    img: require('../../../images/assets_bazar/categorias/belleza.jpg'),
    key: 7,
  },
  // {
  //   name: 'Mascotas',
  //   slug: 'mascotas-1',
  //   img: require('./assets/categoryImg/Mascotas.jpg'),
  //   key: 8,
  // },
  {
    name: 'Decoración del Hogar',
    slug: 'casa-y-decoracion-decoracion-del-hogar-v2',
    img: require('../../../images/assets_bazar/categorias/decoracionDelHogar.jpg'),
    key: 9,
  },
];
