import React from 'react';
import '../../styles/_terms.scss';
import PageHead from '../Utils/PageHead';
import { ORDERS_EMAIL } from '../Constants/config.constants';

export default function ShippingPolicies() {
  return (
    <React.Fragment>
      <PageHead
        attributes={{
          title: 'Políticas de Envíos | Canasta Rosa',
        }}
      />

      <section className="wrapper--center terms">
        <h1 className="title">Políticas de envío en Canasta Rosa</h1>

        <h4>ENVÍOS EXPRESS</h4>
        <h5>Horarios</h5>
        <p>
          Para poder administrar nuestras rutas de manera eficiente, manejamos
          bloques de horario de recolección y entrega. Recolectamos de lunes a sábado
          en un turno matutino (de 9:00 a 13:00) y uno vespertino (de 15:00 a 19:00).
        </p>
        <h5>Empaque de producto</h5>
        <p>
          Todos los productos vendidos a través de Canasta Rosa, deben de ser
          empacados adecuadamente para salvaguardar el producto durante el trayecto.
          No se permiten productos perecederos sin un empaque cerrado. Todos los
          productos deben de poder durar hasta 4 hrs. de trayecto en el estado que se
          entregan. Si los productos requieren empaques especiales de refrigeración,
          estos deben ser entregados así al mensajero.
        </p>
        <h5>Productos refrigerados</h5>
        <p>
          Las unidades de Canasta Rosa o proveedores subcontratados para servicio
          express, no cuentan con unidades, ni servicio de refrigeración. Canasta
          Rosa se reserva del uso y manejo de estos productos a criterio y
          responsabilidad total de la tienda, para el correcto embalaje y asegure que
          el producto se mantenga en el estado idóneo durante 4 hrs.
        </p>
        <h5>Dimensiones y peso máximo de producto</h5>
        <p>
          Todo producto que exceda las siguientes dimensiones o peso, deberá
          contactar a Canasta Rosa para validar la viabilidad de la entrega.
          <ul>
            <li>Productos unitarios mayores a 15 kg </li>
            <li>Productos con algun lado mayor a 120 cm</li>
          </ul>
        </p>
        <h5>Productos prohibidos</h5>
        <p>
          La distribución express de Canasta Rosa, restringe los siguientes
          productos:
          <ul>
            <li>Productos caducados</li>
            <li>Productos con plagas u hongos.</li>
            <li>Productos puntiagudos sin empaque de seguridad.</li>
            <li>Productos dañados o con contenido derramado.</li>
            <li>Productos que hayan sido reproducidos o fabricados sin licencia.</li>
            Nota: Nos reservamos el derecho de denegar las solicitudes de retirada de
            todo aquel inventario identificado como falsificación y eliminarlo.
            <li>Productos que determinamos no aptos por otro motivo</li>
          </ul>
        </p>

        <h5>Medidas preventivas de COVID-19</h5>
        <p>
          Todos los conductores están obligados a utilizar Mascarilla y sanitizantes
          adecuados para conservar la higiene durante una entrega. Tienda no se
          encuentra en domicilio:{' '}
          <ul>
            <li>
              Canasta Rosa tiene la obligación de llamar a la tienda máximo 2
              ocasiones una vez estando afuera del domicilio de recolección
              registrado.
            </li>
            <li>
              La tienda tiene 2 intentos para poder entregar recolección de la o las
              órdenes comprometidas.
            </li>
            <li>
              En caso de no entregar la o las órdenes, Canasta Rosa contactará al
              cliente quién podrá decidir si se cancela* la orden o solicita una
              reprogramación (desde el primer intento).
            </li>
            <li>
              Para poder programar un 3er intento deberá de ser pagado por la tienda
              con un costo de $99.00 IVA incluido.{' '}
            </li>
            <li>
              El tiempo máximo que podrá permanecer el repartidor será de 15 min,
              excedido este tiempo se tomará como recolección fallida.
            </li>
            * La cancelación no podrá ser pagada a tienda.
          </ul>
        </p>
        <h5>Cliente no se encuentra en domicilio</h5>
        <p>
          <ul>
            <li>
              Canasta Rosa tiene la obligación de llamar al cliente 2 veces una vez
              estando fuera del domicilio de entrega registrado, seguido de mails
              notificando la visita.
            </li>
            <li>
              En caso de no ser exitoso el contacto, se deberá enviar a resguardo el
              producto por un tiempo máximo de 24 hrs.
            </li>
            <li>
              Para poder ejecutar el reenvío, una vez estando el producto en
              resguardo, se deberá pagar $99.00 IVA incluido.
            </li>
            <li>
              El tiempo máximo que podrá permanecer el repartidor será de 20 min,
              excedido este tiempo se tomará como entrega fallida.
            </li>
            * Cambio de domicilio, es gratuito si se ejecuta 24 hrs. previas al día
            de entrega. En caso de que sea el cambio de domicilio en el momento, el
            cliente deberá pagar $99.00 IVA incluido.
          </ul>
        </p>
        <h5>Conducta hacia Canasta Rosa o conductores independientes</h5>
        <p>
          No se tolera el uso de lenguaje abusivo así como actitudes abusivas hacia
          el personal de Canasta Rosa o los conductores independientes.
        </p>
        <h4>ENVÍOS NACIONALES</h4>
        <h5>Recolecciones</h5>
        <p>
          Toda tienda es responsable de descargar, imprimir y pegar en el empaque la
          guía de recolección. Recolecciones se programan para el día siguiente hábil
          que se genera la guía.
        </p>
        <h5>Horarios</h5>
        <p>
          Los horarios de recolección son de 09:00 a 19:00, solo en días hábiles.
        </p>
        <h5>Productos prohibidos</h5>
        <p>
          La distribución nacional de Canasta Rosa, restringe los siguientes
          productos:
          <ul>
            <li>Productos unitarios mayores a 10kg</li>
            <li>Productos con algun lado mayor a 120cm </li>
            <li>Perecederos no sellados al vacío</li>
            <li>Baterías o electrónicos que incluyan batería integrada.</li>
            <li>Productos caducados</li>
            <li>
              Artículos que requieren control de temperatura para su conservación.
            </li>
            <li>
              Artículos químicos, inflamables o explosivos ej. (aerosoles o
              combustibles){' '}
            </li>
            <li>Productos con plagas u hongos. </li>
            <li>Productos puntiagudos sin empaque de seguridad.</li>
            <li>Productos dañados o con contenido derramado. </li>
            <li>Productos que hayan sido reproducidos o fabricados sin licencia.</li>
            Nota: Nos reservamos el derecho de denegar las solicitudes de retirada de
            todo aquel inventario identificado como falsificación y eliminarlo.
            <li>
              <a href="http://www.fedex.com/lacpdf/mx/shipping-services/Lista_Articulos_No_Permitidos.pdf">
                Artículos prohibidos por Fedex.
              </a>
            </li>
            <li> Productos que determinamos no aptos por otro motivo</li>
          </ul>
        </p>
        <p>Canasta Rosa puede eliminar o limitar la cobertura de distribución.</p>

        <h5>CANASTA ROSA, SAPI DE CV</h5>
      </section>
    </React.Fragment>
  );
}
