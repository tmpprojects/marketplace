import React, { Component } from 'react';
import { renderRoutes } from 'react-router-config';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Link, NavLink } from 'react-router-dom';
import axios from 'axios';
import config from '../../config';

import '../../styles/_crPro.scss';
import SEO from '../../statics/SEO.json';
import CollapseBox from '../Utils/CollapseBox';
import Services from '../components/CRPro/Services';
import School from '../components/CRPro/School';
import Workshops from '../components/CRPro/Workshops';
import Store from '../components/CRPro/Store';
import PageHead from '../Utils/PageHead';
import ServicesModalBox from '../Utils/modalbox/ServicesModalBox';
import EventsModalBox from '../Utils/modalbox/EventsModalBox';
import CoursesModalBox from '../Utils/modalbox/CoursesModalBox';
import { modalBoxActions } from '../Actions';
import FormLogin from '../Utils/FormLogin';
import Register from '../Utils/Register';
import { CANASTAROSA_PRO_SLUG, PRODUCT_TYPES } from '../Constants';

const logoMobile = require('../../images/logo/header_logo.svg');

const RequireAccountModal = ({ openModalBox }) => {
  const formSubmit = () => {
    window.location = '/stores/create';
  };

  return (
    <div className="modalWindow requireAccount">
      <div className="message">
        Para crear una tienda gratis, es necesario que ingreses a tu cuenta.
      </div>
      <h4 className="title">Login</h4>
      <FormLogin onFormSubmit={formSubmit} />

      <div className="requireAccount__register">
        <p className="subtitle">¿A&uacute;n no tienes una cuenta?</p>
        <button className="create-account" onClick={() => openModalBox(Register)}>
          Reg&iacute;strate gratis aqu&iacute;
        </button>
      </div>
    </div>
  );
};
class CRProPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isActive: true,
      sections: [],
      products: [],
      events: [],
      courses: [],
    };

    // DOM Refs
    this.contentWrapRefMobile = React.createRef();
    this.contentWrapRefDesktop = React.createRef();

    // Content Position
    this.contentWrapDesktopYPos = 0;
    this.contentWrapMobileYPos = 0;
  }

  /**
   * componentDidMount()
   */
  componentDidMount() {
    // Fetch Products from API
    axios
      .get(
        `${config.frontend_host}/services/api/v1/market/stores/${CANASTAROSA_PRO_SLUG}/products/?page_size=100`,
      )
      .then((response) => {
        this.setState({
          products: response.data.results.filter(
            (p) => p.product_type.value === PRODUCT_TYPES.SERVICE,
          ),
          events: response.data.results.filter(
            (p) => p.product_type.value === PRODUCT_TYPES.EVENT,
          ),
        });
      })
      .catch((error) => {
        console.log(error);
      });

    // Fetch Canasta Pro sections.
    axios
      .get(
        `${config.frontend_host}/services/api/v1/market/stores/${CANASTAROSA_PRO_SLUG}/sections/`,
      )
      .then((response) => {
        this.setState({
          sections: response.data.filter((s) => s.slug !== 'eventos'),
        });
      })
      .catch((error) => {
        console.log(error);
      });

    // Content Position
    this.updateContentPosition();
    window.addEventListener('resize', this.onResize);

    // According to the URL, try to find if we need to open
    // the product detail window
    let urlPath = [];
    if (this.props.location.pathname) {
      urlPath = this.props.location.pathname.split('/');

      // Fake product mockup
      // TODO: This is a quick fix.
      // We should get this information with a better aproach.
      const product = {
        product_type: {
          value: PRODUCT_TYPES.PHYSICAL,
        },
      };
      switch (urlPath[2]) {
        case 'servicios':
          product.product_type.value = PRODUCT_TYPES.SERVICE;
          break;
        case 'cursos':
          product._embedded = {};
          product.product_type.value = PRODUCT_TYPES.COURSE;
          break;
        case 'eventos':
          product.product_type.value = PRODUCT_TYPES.EVENT;
          break;
        default:
          product.product_type.value = PRODUCT_TYPES.PHYSICAL;
      }

      // Open modalbox component only if URL path has a product slug.
      if (urlPath[3]) {
        this.openDetailsWindow(product);
      }
    }
  }

  /**
   * componentWillUnmount()
   */
  componentWillUnmount() {
    window.removeEventListener('resize', this.onResize);
  }

  /**
   * onResize()
   */
  onResize = () => {
    this.updateContentPosition();
  };

  /**
   * updateContentPosition()
   * Update variables that define position of 'CR PRO services' DOM wrapper
   * relative to the window.
   */
  updateContentPosition = () => {
    this.contentWrapDesktopYPos =
      this.contentWrapRefDesktop.current.getBoundingClientRect().top +
      window.scrollY;
    this.contentWrapMobileYPos =
      this.contentWrapRefMobile.current.getBoundingClientRect().top + window.scrollY;
  };

  /**
   * openDetailsWindow()
   * Opens a modalbox with product description depending on product_type.
   * @param {object} product | Product Object
   */
  openDetailsWindow = (product) => {
    let productType;
    if (product._embedded) {
      // TODO: Fix this
      productType = PRODUCT_TYPES.COURSE;
    } else {
      productType = product.product_type.value;
    }

    //
    switch (productType) {
      case PRODUCT_TYPES.EVENT:
        this.props.openModalBox(() => <EventsModalBox />);
        break;
      case PRODUCT_TYPES.SERVICE:
        this.props.openModalBox(() => <ServicesModalBox />);
        break;
      case PRODUCT_TYPES.COURSE:
        this.props.openModalBox(() => <CoursesModalBox product={product} />);
        break;
      case PRODUCT_TYPES.PHYSICAL:
      default:
        break;
    }
  };

  /**
   * isUserLogged()
   * Check if user is logged in
   * @param {object} e | Button event
   */
  isUserLogged = (e) => {
    const { isUserLogged } = this.props;
    if (!isUserLogged) {
      e.preventDefault();
      this.props.openModalBox(() => (
        <RequireAccountModal
          history={this.props.history}
          openModalBox={this.props.openModalBox}
        />
      ));
    }
  };

  /**
   * render()
   */
  render() {
    return (
      <div className="crpro-page">
        <PageHead attributes={SEO.CRProPage} />

        <div className="crpro-cover">
          <div className="details">
            <div itemProp="logo" className="details__logo">
              <img src={logoMobile} alt="Canasta Rosa Pro" />
            </div>
            <h1 className="details__name">Canasta Rosa Pro</h1>
            <p className="details__description">Haz lo que amas hacer.</p>
          </div>
        </div>

        <div className="crpro-intro-box">
          <h3 className="title">
            CanastaRosa Pro, un espacio para que puedas crear,
            <br />
            lanzar y hacer crecer tu negocio personal.
          </h3>
          {/*<div className="crpro-intro">
                        <p>Igual que tú, somos un equipo de emprendedores que quieren ver
                        crecer sus ideas y materializar sus sueños. Por eso, entendemos que saber 
                        concebir y desarrollar un negocio, puede parecer una barrera 
                        o un paso complicado.</p>
                        
                        <p>Canasta Rosa pone a tu alcance toda la experiencia 
                        que hemos adquirido lanzando marcas pequeñas y viendo despegar 
                        ideas increíbles.<br />
                        A través de nuestras asesorías, talleres y cursos 
                        diseñados especialmente para ti, buscamos contar la historia 
                        de cientos de emprendedoras a través de sus productos y creaciones.</p>
                    </div>*/}
          <p className="content">
            Impulsa tu marca con los servicios de Canasta Rosa Pro.
          </p>
        </div>

        {/* DESKTOP MENU */}
        <div className="crpro-menu-lg" ref={this.contentWrapRefDesktop}>
          <div className="crpro-menu-lg__nav">
            <NavLink
              exact={false}
              to={{
                pathname: '/pro/servicios',
                state: { scrollToPosition: this.contentWrapDesktopYPos },
              }}
              className="section-tab"
              activeClassName="active"
              isActive={(match, location) => {
                if (location.pathname === '/pro') return true;
                if (match) return true;
                return false;
              }}
            >
              <div className="crpro-menu-lg__section" section="services">
                <div className="crpro-menu-lg__icon">
                  {this.props.match.params.section === 'servicios' ||
                  this.props.location.pathname === '/pro' ? (
                    <img
                      src={require('../../images/crPro/icons/icon_services_active.svg')}
                      alt="Servicios"
                    />
                  ) : (
                    <img
                      src={require('../../images/crPro/icons/icon_services.svg')}
                      alt="Servicios"
                    />
                  )}
                </div>
                <div className="crpro-menu-lg__title">
                  <p>
                    SERVICIOS
                    {this.props.match.params.section !== 'servicios' && (
                      <span>Ver Servicios</span>
                    )}
                  </p>
                </div>
              </div>
            </NavLink>
            {/* <NavLink 
                            to={{
                                pathname: '/pro/cursos',
                                state: { scrollToPosition: this.contentWrapDesktopYPos }
                            }}
                            className="section-tab"
                            activeClassName="active"
                        >
                            <div className="crpro-menu-lg__section" section="school" >
                                    <div className="crpro-menu-lg__icon">
                                    {
                                        (this.props.match.params.section === 'cursos') ?
                                        <img src={require('../../images/crPro/icons/icon_school_active.svg')} alt='Cursos' /> :
                                        <img src={require('../../images/crPro/icons/icon_school.svg')} alt='Cursos' />
                                    }
                                    </div>
                                    <div className="crpro-menu-lg__title">
                                        <p>
                                            CURSOS 
                                            {this.props.match.params.section !== 'cursos' &&
                                                <span>Ver Cursos</span>
                                            }
                                        </p>
                                    </div>
                            </div>
                        </NavLink> */}
            <NavLink
              to={{
                pathname: '/pro/eventos',
                state: { scrollToPosition: this.contentWrapDesktopYPos },
              }}
              className="section-tab"
              activeClassName="active"
            >
              <div className="crpro-menu-lg__section" section="workshops">
                <div className="crpro-menu-lg__icon">
                  {this.props.match.params.section === 'eventos' ? (
                    <img
                      src={require('../../images/crPro/icons/icon_workshops_active.svg')}
                      alt="Eventos"
                    />
                  ) : (
                    <img
                      src={require('../../images/crPro/icons/icon_workshops.svg')}
                      alt="Eventos"
                    />
                  )}
                </div>
                <div className="crpro-menu-lg__title">
                  <p>
                    EVENTOS
                    {this.props.match.params.section !== 'eventos' && (
                      <span>Ver Eventos</span>
                    )}
                  </p>
                </div>
              </div>
            </NavLink>
            <Link to="/stores/create" onClick={this.isUserLogged}>
              <div className="crpro-menu-lg__section" section="sell">
                <div className="crpro-menu-lg__icon">
                  {this.props.match.params.section === 'sell' ? (
                    <img
                      src={require('../../images/crPro/icons/icon_sell_active.svg')}
                      alt="Vende"
                    />
                  ) : (
                    <img
                      src={require('../../images/crPro/icons/icon_sell.svg')}
                      alt="Vende"
                    />
                  )}
                </div>
                <div className="crpro-menu-lg__title">
                  <p>
                    VENDE EN <span>Canasta Rosa</span>
                  </p>
                </div>
              </div>
            </Link>
            <Link to="/stores/canastarosa-productos">
              <div className="crpro-menu-lg__section" section="store">
                <div className="crpro-menu-lg__icon">
                  {this.props.match.params.section === 'store' ? (
                    <img
                      src={require('../../images/crPro/icons/icon_store_active.svg')}
                      alt="Store"
                    />
                  ) : (
                    <img
                      src={require('../../images/crPro/icons/icon_store.svg')}
                      alt="Store"
                    />
                  )}
                </div>
                <div className="crpro-menu-lg__title">
                  <p>
                    NUESTRA TIENDA <span>Canasta Rosa</span>
                  </p>
                </div>
              </div>
            </Link>
          </div>

          <div className="section">
            {renderRoutes(this.props.route.routes, {
              openDetailsWindow: this.openDetailsWindow,
              sections: this.state.sections,
              products: this.state.products,
              courses: this.state.courses,
              events: this.state.events,
            })}
          </div>
        </div>

        {/* MOBILE MENU */}
        <div className="crpro-menu" ref={this.contentWrapRefMobile}>
          <div className="">
            <CollapseBox
              open={this.props.match.params.section === 'servicios'}
              content={
                <Services
                  openDetailsWindow={this.openDetailsWindow}
                  sections={this.state.sections}
                  products={this.state.products}
                />
              }
              header={
                <NavLink
                  to={{
                    pathname: '/pro/servicios',
                    state: { scrollToPosition: this.contentWrapMobileYPos },
                  }}
                  activeClassName="active"
                  className="crpro-menu__link"
                >
                  <div className="crpro-menu__section">
                    <div className="crpro-menu__icon">
                      {this.props.match.params.section === 'servicios' ||
                      this.props.location.pathname === '/pro' ? (
                        <img
                          src={require('../../images/crPro/icons/icon_services_active_pink.svg')}
                          alt="Services"
                        />
                      ) : (
                        <img
                          src={require('../../images/crPro/icons/icon_services.svg')}
                          alt="Services"
                        />
                      )}
                    </div>
                    <div className="crpro-menu__title">
                      <p>
                        Servicios <span>Canasta Rosa</span>
                      </p>
                    </div>
                  </div>
                </NavLink>
              }
            />
          </div>

          {/* <div className="">
                        <CollapseBox
                            open={this.props.match.params.section === 'cursos'}
                            content={
                                <School 
                                    openDetailsWindow={this.openDetailsWindow} 
                                    courses={this.state.courses}
                                />
                            }
                            header={
                                <NavLink 
                                    to={{
                                            pathname: '/pro/cursos',
                                            state: { scrollToPosition: this.contentWrapMobileYPos }
                                        }}
                                    activeClassName="active" 
                                    className="crpro-menu__link"
                                >
                                    <div className="crpro-menu__section">
                                        <div className="crpro-menu__icon">
                                            {(this.props.match.params.section === 'cursos') ?
                                                <img src={require('../../images/crPro/icons/icon_school_active_pink.svg')} alt='School' /> :
                                                <img src={require('../../images/crPro/icons/icon_school.svg')} alt='School' />
                                            }
                                        </div>
                                        <div className="crpro-menu__title">
                                            <p>Cursos <span>Canasta Rosa</span></p>
                                        </div>
                                    </div>
                                </NavLink>
                            }
                        />
                    </div> */}

          <div className="">
            <CollapseBox
              open={this.props.match.params.section === 'eventos'}
              content={
                <Workshops
                  events={this.state.events}
                  openDetailsWindow={this.openDetailsWindow}
                />
              }
              header={
                <NavLink
                  to={{
                    pathname: '/pro/eventos',
                    state: { scrollToPosition: this.contentWrapMobileYPos },
                  }}
                  activeClassName="active"
                  className="crpro-menu__link"
                >
                  <div className="crpro-menu__section">
                    <div className="crpro-menu__icon">
                      {this.props.match.params.section === 'eventos' ? (
                        <img
                          src={require('../../images/crPro/icons/icon_workshops_active_pink.svg')}
                          alt="Workshops"
                        />
                      ) : (
                        <img
                          src={require('../../images/crPro/icons/icon_workshops.svg')}
                          alt="Workshops"
                        />
                      )}
                    </div>
                    <div className="crpro-menu__title">
                      <p>
                        Eventos <span>Canasta Rosa</span>
                      </p>
                    </div>
                  </div>
                </NavLink>
              }
            />
          </div>

          <div className="">
            <CollapseBox
              open={this.props.match.params.section === 'sell'}
              content={<Store openDetailsWindow={this.openDetailsWindow} />}
              header={
                <Link to="/stores/create" onClick={this.isUserLogged}>
                  <div className="crpro-menu__section">
                    <div className="crpro-menu__icon">
                      {this.props.match.params.section === 'sell' ? (
                        <img
                          src={require('../../images/crPro/icons/icon_sell_active_pink.svg')}
                          alt="Sell"
                        />
                      ) : (
                        <img
                          src={require('../../images/crPro/icons/icon_sell.svg')}
                          alt="Sell"
                        />
                      )}
                    </div>
                    <div className="crpro-menu__title">
                      <p>
                        Vende en <span>Canasta Rosa</span>
                      </p>
                    </div>
                  </div>
                </Link>
              }
            />
          </div>

          <div className="">
            <CollapseBox
              open={this.props.match.params.section === 'store'}
              content={<Store openDetailsWindow={this.openDetailsWindow} />}
              header={
                <Link to="/stores/canastarosa-productos">
                  <div className="crpro-menu__section">
                    <div className="crpro-menu__icon">
                      {this.props.match.params.section === 'store' ? (
                        <img
                          src={require('../../images/crPro/icons/icon_store_active.svg')}
                          alt="Store"
                        />
                      ) : (
                        <img
                          src={require('../../images/crPro/icons/icon_store.svg')}
                          alt="Store"
                        />
                      )}
                    </div>
                    <div className="crpro-menu__title">
                      <p>
                        NUESTRA TIENDA <span>Canasta Rosa</span>
                      </p>
                    </div>
                  </div>
                </Link>
              }
            />
          </div>
        </div>
      </div>
    );
  }
}

// Load Data for Server Side Rendering
CRProPage.loadData = (reduxStore, routePath) => {};

// Map Redux Props and Actions to component
function mapStateToProps(state) {
  return {
    isUserLogged: state.users.isLogged,
  };
}
function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      openModalBox: modalBoxActions.open,
      closeModalBox: modalBoxActions.close,
    },
    dispatch,
  );
}

// Export Component
export default connect(mapStateToProps, mapDispatchToProps)(CRProPage);
