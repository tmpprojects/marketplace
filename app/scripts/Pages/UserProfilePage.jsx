import React, { Component } from 'react';
import { Link, NavLink } from 'react-router-dom';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { renderRoutes } from 'react-router-config';
import { SubmissionError } from 'redux-form';
import PropTypes from 'prop-types';
import requireAuth from '../components/hocs/requireAuth';

import '../../styles/_users.scss';
import SEO from '../../statics/SEO.json';
import PageHead from '../Utils/PageHead';
import { userTypes } from '../Constants';
import { getUIErrorMessage } from '../Utils/errorUtils';
import { userProfileErrorTypes } from '../Constants/user.constants';
import { getUserProfile } from '../Reducers/users.reducer';
import { getCustomerOrdersList } from '../Reducers/orders.reducer';
import { userActions, statusWindowActions, ordersActions } from '../Actions';

/*
 * Form Validation Function
 * @param {object} values : Form values
 */
const validate = (values) => {
  const errors = {};

  // Nombre
  if (!values.first_name || values.first_name.length < 3) {
    errors.first_name = 'Escribe tu nombre completo.';
  }
  if (!values.last_name || values.last_name.length < 3) {
    errors.last_name = 'Escribe tus apellidos.';
  }

  // Return errors
  return errors;
};

/*
 * Render Custom Form Fields
 * @param {object} props : Field Properties
 */
const renderField = ({
  input,
  label,
  autoFocus,
  placeholder,
  className: classSuffix = '',
  meta: { touched, error },
}) => (
  <div className={`form__data form__data-${classSuffix}`}>
    <label className="title" htmlFor={input.name}>
      {label}
    </label>
    <input
      {...input}
      id={input.name}
      placeholder={placeholder}
      autoFocus={autoFocus}
    />
    {touched && error && <div className="form_status danger">{error}</div>}
  </div>
);

/*
 * React UserProfilePage Component
 */
class UserProfilePage extends Component {
  static propTypes = {
    userType: PropTypes.string.isRequired,
  };
  static defaultProps = {
    userType: userTypes.CUSTOMER,
  };

  /**
   * constructor()
   * @param {object} props | Component Props
   */
  constructor(props) {
    super(props);
    this.state = {
      isAddressMenuOpen: false,
      isPasswordMenuOpen: false,
      error: null,
      errorInfo: null,
    };
  }

  /*
   * REACT COMPONENT LIFECYCLE
   */
  componentDidMount() {
    // Load User orders
    //this.props.getOrders(this.props.userType);
  }
  componentWillReceiveProps(nextProps) {
    if (this.props.user !== nextProps.user) {
      let birthday;
      if (nextProps.user.birthday) {
        birthday = nextProps.user.birthday.split('-');
      } else {
        birthday = ['0000', '00', '00'];
      }

      this.setState({
        bd_day: birthday[2],
        bd_month: birthday[1],
        bd_year: birthday[0],
      });
    }
  }

  /*
   * FORM FUNCTIONS
   */
  onPersonalInfoSubmit = (formValues) => {
    const formData = {
      first_name: formValues.first_name,
      last_name: formValues.last_name,
      gender: formValues.gender.value,
      location: formValues.location,
      birthday: formValues.birthday,
    };

    // Update user settings
    return this.props
      .updateUserInfo(formData)
      .then((response) => {
        this.props.openStatusWindow({
          type: 'success',
          message: 'Los datos se guardaron con éxito.',
        });
      })
      .catch((error) => {
        this.props.openStatusWindow({
          type: 'error',
          message: 'Error. Por favor, intenta de nuevo.',
        });
        throw new SubmissionError({
          _error: `Ocurrió un problema al actualizar tu información.
                Por favor, intenta de nuevo.`,
        });
      });
  };

  /**
   * onBioSubmit()
   * Sets user profile bio.
   * @param {object} userForm
   */
  onBioSubmit = ({ presentation }) =>
    // Update user bio
    this.props
      .updateUserInfo({ presentation })
      .then((response) => {
        this.props.openStatusWindow({
          type: 'success',
          message: 'Información actualizada.',
        });
      })
      .catch((error) => {
        this.props.openStatusWindow({
          type: 'error',
          message: 'Error. Por favor, intenta de nuevo.',
        });
        throw new SubmissionError({
          _error: `Ocurrió un problema al actualizar tu información.
                Por favor, intenta de nuevo.`,
        });
      });

  /**
   * onChangeProfilePhoto()
   * Sets a new profile picture
   */
  onChangeProfilePhoto = (formData) =>
    this.props
      .updateUserInfo(formData)
      .then((response) => {
        this.props.openStatusWindow({
          type: 'success',
          message: 'Imagen actualizada.',
        });
      })
      .catch((error) => {
        const uiError = getUIErrorMessage(error.response, userProfileErrorTypes);
        this.props.openStatusWindow({ type: 'error', message: uiError });
        throw new SubmissionError({
          _error: uiError,
        });
      });

  /**
   * onEmailSubmit()
   * Sets a new email
   */
  onEmailSubmit = (e) => {
    // Prevent Form Submit
    e.preventDefault();

    // Get form data
    const formData = { email: this.state.userData.emailNew };

    // Update user profile
    this.props.updateUserInfo(formData);

    // Reset Input Fields
    this.setState({
      userData: {
        emailNew: '',
        emailConf: '',
      },
    });
  };

  /**
   * onPasswordSubmit()
   * @param {object} passwordObject
   */
  onPasswordSubmit = ({
    passwordNew: password_new,
    passwordCurrent: password_old,
  }) =>
    // Update user profile
    this.props
      .updatePassword({ password_new, password_old })
      .then((response) => {
        this.props.openStatusWindow({
          type: 'success',
          message: 'Información actualizada.',
        });
      })
      .catch((error) => {
        this.props.openStatusWindow({
          type: 'error',
          message: 'Error. Por favor, intenta de nuevo.',
        });
        throw new SubmissionError({
          _error: `Ocurrió un problema al actualizar tu información. 
                Por favor, intenta de nuevo.`,
        });
      });

  /**
   * isMenuOpen()
   * @param {string} target
   */
  isMenuOpen = (target) => {
    const targetMenu =
      target === 'address_menu' ? 'isAddressMenuOpen' : 'isPasswordMenuOpen';
    this.setState({
      [targetMenu]: !this.state[targetMenu],
    });
  };

  /**
   * componentDidCatch()
   * Catch errors in any child components and
   * re-renders with an error message
   * @param {object} error
   * @param {object} errorInfo
   */
  componentDidCatch(error, errorInfo) {
    this.setState({
      error,
      errorInfo,
    });
  }

  /**
   * render()
   */
  render() {
    const { isAddressMenuOpen, isPasswordMenuOpen } = this.state;
    const { route, user, orders, reviews, testNew } = this.props;
    const {
      onChangeProfilePhoto,
      onPersonalInfoSubmit,
      onBioSubmit,
      onPasswordSubmit,
      onEmailSubmit,
      isMenuOpen,
    } = this;
    const { profile_photo, has_store } = this.props.user;
    let photo = 'require(utils/user_profile.png)';
    let user_photo = profile_photo !== '' ? profile_photo : photo;
    // UI Fallback if an error occurs
    if (this.state.error) {
      return (
        <div className="message-error wrapper--center">
          <h3>Oh-no! Algo salió mal.</h3>
          <p>
            Encontramos un problema al consultar tu información.
            <br />
            Hemos recibido la notificación y estamos trabajando para solucionarlo.
          </p>
          <p>Por favor, regresa más tarde.</p>
          {/*<p className="red">
                        {this.state.error && this.state.error.toString()}
                    </p>
                    <div>{"Component Stack Error Details: "}</div>
                    <p className="red">{this.state.errorInfo.componentStack}</p>*/}
        </div>
      );
    }

    // Return markup
    return (
      <div className="profile_container wrapper--center">
        <PageHead attributes={SEO.UserProfilePage} />

        <div className="menu_category">
          <h4 className="title">Tu perfil</h4>

          <ul className="categories">
            <li className="category">
              <NavLink
                to="/users/profile/"
                className="dropdown"
                activeClassName="active"
              >
                Mi Informaci&oacute;n
              </NavLink>
            </li>
            <li className="category">
              {!has_store ? (
                <Link to="/stores/create" className="dropdown">
                  Abre una Tienda
                </Link>
              ) : (
                <Link to="/my-store/dashboard" className="dropdown">
                  Mi tienda
                </Link>
              )}
            </li>
            <li className="category">
              <NavLink to="/users/cards" href="#addresses" activeClassName="active">
                Mis formas de pago
              </NavLink>
            </li>
            <li className="category">
              <NavLink to="/users/orders" activeClassName="active">
                Mis Compras
              </NavLink>
            </li>
            <li className="category">
              <NavLink to="/users/reviews" activeClassName="active">
                Mis reseñas
              </NavLink>
            </li>
            <li className="category">
              <a
                href="https://ayuda.canastarosa.com/"
                target="_blank"
                activeClassName="active"
              >
                Ayuda
              </a>
            </li>
          </ul>
        </div>

        {renderRoutes(route.routes, {
          userType: userTypes.CUSTOMER,
          user,
          orders,
          reviews,
          isMenuOpen,
          onBioSubmit,
          onEmailSubmit,
          onPasswordSubmit,
          isAddressMenuOpen,
          isPasswordMenuOpen,
          onChangeProfilePhoto,
          onPersonalInfoSubmit,
        })}
      </div>
    );
  }
}

// Redux Map Functions
function mapStateToProps(state) {
  const customer = userTypes.CUSTOMER.toLowerCase();
  const customerOrders = getCustomerOrdersList(state);
  /*const reviews = []//state.users.reviews.data.results || [];
    const productReviews = reviews.reduce((productsList, currentItem) => {
        let foundProductIndex = -1;
        if(productsList.length){
            foundProductIndex = productsList.findIndex(p => 
                p[0].product.slug === currentItem.product.slug
            );
        }

        if( foundProductIndex === -1 ){
            return [...productsList, [currentItem]];
        } else {
            const updatedProduct = [
                ...productsList[foundProductIndex],
                currentItem
            ]
            return [
                ...productsList.slice(0, foundProductIndex),
                updatedProduct,
                ...productsList.slice(foundProductIndex+1),
            ];
        }
    }, []);*/

  return {
    user: getUserProfile(state),
    orders: customerOrders, //state.orders[customer],
  };
}
const mapDispatchToProps = (dispatch) =>
  bindActionCreators(
    {
      getOrders: ordersActions.getOrders,
      updateUserInfo: userActions.updateUserInfo,
      openStatusWindow: statusWindowActions.open,
      updatePassword: userActions.updatePassword,
      uploadUserPhoto: userActions.uploadUserPhoto,
    },
    dispatch,
  );

// Connect Component to Redux Store
UserProfilePage = connect(mapStateToProps, mapDispatchToProps)(UserProfilePage);

// Require Authentication
UserProfilePage = requireAuth(UserProfilePage);

// Load Data for Server Side Rendering
UserProfilePage.loadData = (store) =>
  Promise.all([store.dispatch(ordersActions.getOrders(userTypes.CUSTOMER))]);

// Export Component
export default UserProfilePage;
