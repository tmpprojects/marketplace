import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import '../../styles/_inspireHomePage.scss';
import '../../styles/_errorPage.scss';
import SEO from '../../statics/SEO.json';
import PageHead from '../Utils/PageHead';
import { inspireActions } from '../Actions';
import Search from '../components/Inspire/Search';
import Listing from '../components/Inspire/Listing';
import { AboutUs } from '../components/Inspire/AboutUs';
import MainBanner from '../components/Inspire/MainBanner';
import CategoriesPopular from '../Utils/CategoriesPopular';
import HoraRosa from '../components/HoraRosa/HoraRosa.js';
class InspireHomePage extends Component {
  constructor(props) {
    super(props);
    this.currentPage = 1;
    this.loadPage = this.loadPage.bind(this);
  }
  componentDidMount() {
    this.props.resetArticles();
    this.props.fetchArticles();
    this.props.fetchBanners();
    this.props.fetchTags();
  }
  loadPage(filters = { page: 1 }) {
    this.currentPage = filters.page;
    this.props.fetchArticles(filters);
  }
  render() {
    const articlesObject = this.props.articles || {};
    return (
      <div>
        <HoraRosa location={this.props.location} little={true}></HoraRosa>
        <PageHead attributes={SEO.InspireHomePage} />
        <Search />
        <MainBanner banners={this.props.banners} />
        <Listing
          title="Artículos Recientes"
          articles={articlesObject}
          fetchArticles={this.loadPage}
          page={this.currentPage}
        />
        <CategoriesPopular />
        <AboutUs />
      </div>
    );
  }
}

// Preload Data for Server side rendering
InspireHomePage.loadData = (store) => {
  const promises = [
    store.dispatch(inspireActions.resetArticles()),
    store.dispatch(inspireActions.fetchArticles()),
    store.dispatch(inspireActions.fetchBanners()),
    store.dispatch(inspireActions.fetchTags()),
  ];
  return Promise.all(promises.map((p) => (p.catch ? p.catch((e) => e) : p)));
};

// Redux Map Functions
function mapStateToProps({ inspire }) {
  const { articles, banners } = inspire;
  return { articles, banners };
}
function mapDispatchToProps(dispatch) {
  const { fetchArticles, resetArticles, fetchBanners, fetchTags } = inspireActions;
  return bindActionCreators(
    {
      fetchBanners,
      fetchArticles,
      resetArticles,
      fetchTags,
    },
    dispatch,
  );
}

// Export Connected Component
export default connect(mapStateToProps, mapDispatchToProps)(InspireHomePage);
