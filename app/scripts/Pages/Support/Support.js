import React from 'react';
import './../../../styles/_crPro.scss';
import './Support.scss';

export default class Support extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount() {
    if (process.env.CLIENT) {
      //window.openSupport();
    }
  }

  render() {
    return (
      <div>
        <div className="crpro-page cr-support">
          <div className="crpro-cover">
            <div className="details">
              <div itemProp="logo" className="details__logo"></div>
              <h1 className="details__name">Soporte / Ayuda</h1>
              <p className="details__description">Estamos para apoyarte.</p>
            </div>
          </div>

          <div className="crpro-intro-box">
            <h3 className="title">
              ¿Tienes alguna duda o problema para completar tu compra?
            </h3>

            {/*
            <p className='content'>
              No dudes en contactarnos a los teléfonos 55-7583-8134 y/o al
              WhatsApp 
              <a target="_blank" rel="noopener noreferrer" href='https://api.whatsapp.com/send?phone=5215579393795'>
                55-7939-3795
              </a>
              o si prefieres puedes escribirnos en 
              <a href='#' onClick={() => window.openSupport()}>
                nuestro chat
              </a>
            </p>

            */}

            <p className="content">
              No dudes en contactarnos en{' '}
              <a href="#" onClick={() => window.openSupport()}>
                nuestro chat
              </a>
            </p>
          </div>
        </div>
      </div>
    );
  }
}
