import React from 'react';

import '../components/Awards/awards.scss';
import categoriesList from '../components/Awards/categories_awards.json';
import CategoryDetail from '../components/Awards/CategoryDetail';

export default class AwardsPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      categoryWindow: false,
      category: {},
    };
  }
  closeWindow = () => {
    this.setState({
      categoryWindow: false,
    });
  };
  openWindow = (category) => {
    this.setState({
      categoryWindow: true,
      category,
    });
  };

  render() {
    const { categoryWindow, category } = this.state;
    return (
      <section className="awards">
        <div className="awards__cover" />
        <div className="awards__presentation container">
          <h1 className="title color">
            El reconocimiento a las mejores marcas de emprendedores Mexicanos.
          </h1>
          <p className="wrapper--center info">
            <span className="bold">CANASTA ROSA AWARDS</span> es la votación
            realizada por los usuarios de <br />
            Canasta Rosa para reconocer e impulsar las{' '}
            <span className="bold">mejores marcas de emprendedores</span> a lo largo
            de todo el país. <br />
            El evento culmina en una gran ceremonia donde se otorgará un
            reconocimiento y{' '}
            <span className="bold">
              se promoverá el talento y esfuerzo a las marcas ganadoras.
            </span>{' '}
            <br />
            Es un espacio que{' '}
            <span className="bold">
              impulsa a los emprendedores a romper las barreras
            </span>{' '}
            y mostrar a sus clientes lo que es posible.
          </p>
        </div>

        <div className="awards__button">
          <a href="#ganadores" className="button__square" rel="noopener noreferrer">
            Conoce a los nominados aquí
          </a>
        </div>

        <div className="awards__steps"></div>

        <div className="awards__categories container">
          <div className="wrapper--center">
            <h4 className="title" id="ganadores">
              Categorías
            </h4>
            <p className="info">
              En su primera edición, Canasta Rosa Awards otorgará reconocimientos a
              las marcas más sobresalientes en{' '}
              <span className="bold">8 categorías</span>, además de{' '}
              <span className="bold">3 menciones honoríficas</span>. <br />
              Se premiarán a los <span className="bold">
                tres primeros lugares
              </span>{' '}
              de cada categoría.
            </p>
            {categoryWindow ? (
              <CategoryDetail category={category} closeWindow={this.closeWindow} />
            ) : (
              <React.Fragment>
                <ul className="categories__list special">
                  {categoriesList.special.map((category, i) => (
                    <li key={i} className="category">
                      <button
                        className={`link ${category.slug}`}
                        onClick={(e) => this.openWindow(category)}
                      >
                        {category.name}
                      </button>
                    </li>
                  ))}
                </ul>
                <ul className="categories__list general">
                  {categoriesList.general.map((category, i) => (
                    <li key={i} className="category">
                      <button
                        className={`link ${category.slug}`}
                        onClick={(e) => this.openWindow(category)}
                      >
                        {category.name}
                      </button>
                    </li>
                  ))}
                </ul>
              </React.Fragment>
            )}
          </div>
        </div>

        <div className="awards__event">
          <div className="wrapper--center">
            <h4 className="title color">Ceremonia de premiación</h4>
            <p className="info">
              El <span className="bold">26 de Septiembre</span> se llevará acabo el
              evento de cierre donde serán{' '}
              <span className="bold">invitados todos los finalistas</span> e
              impulsaremos sus increíbles productos con{' '}
              <span className="bold">
                importantes influencers y medios de comunicacón
              </span>
              .
            </p>
          </div>
        </div>

        <footer className="awards__footer">
          <div className="wrapper--center">
            <ul className="socialMedia__list">
              <li className="socialMedia__list__item facebook">
                <a
                  id="CR Facebook_Footer"
                  className="gtm_link_click"
                  href="https://www.facebook.com/lacanastarosa/"
                  target="_blank"
                  rel="noopener noreferrer"
                >
                  Facebook
                </a>
              </li>
              <li className="socialMedia__list__item instagram">
                <a
                  id="CR Instagram_Footer"
                  className="gtm_link_click"
                  href="https://www.instagram.com/canastarosa/"
                  target="_blank"
                  rel="noopener noreferrer"
                >
                  Instagram
                </a>
              </li>
            </ul>
            <p className="copy">Canasta Rosa @ 2019</p>
          </div>
        </footer>
      </section>
    );
  }
}
