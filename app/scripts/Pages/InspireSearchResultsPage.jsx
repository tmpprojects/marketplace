import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import '../../styles/_inspireSearchResultsPage.scss';
import config from '../../config';
import SEO from '../../statics/SEO.json';
import PageHead from '../Utils/PageHead';
import { inspireActions } from '../Actions';
import Search from '../components/Inspire/Search';
import Listing from '../components/Inspire/Listing';
import CategoriesPopular from '../Utils/CategoriesPopular';

class InspireSearchResultsPage extends Component {
  static propTypes = {
    articles: PropTypes.object.isRequired,
  };
  static defaultProps = {
    articles: {},
  };

  /**
   * constructor()
   * @param {object} props : Component Props
   */
  constructor(props) {
    super(props);
    this.currentPage = 1;
    this.search = this.search.bind(this);
    this.resetSearch = this.resetSearch.bind(this);
    this.loadPage = this.loadPage.bind(this);
  }
  componentDidMount() {
    // Reset Search and fetch new Results
    this.resetSearch();
    this.search(this.props.match.params.term);
  }
  componentWillReceiveProps(nextProps) {
    // If search term changes, Reset Search and fetch new Results
    if (this.props.match.url !== nextProps.match.url) {
      this.resetSearch();
      this.search(nextProps.match.params.term);
    }
  }

  /**
   * resetSearch()
   * Resets Page Counter and Empty Search Results List on Redux Store
   */
  resetSearch() {
    this.currentPage = 1;
    this.props.resetSearch();
  }

  /**
   * search()
   * @param {string} term : Search Term
   * @param {object} filtersObject : Search Fitlers (page, category, etc...)
   */
  search(term, filtersObject = {}) {
    // Default query
    const defaultQuery = {
      page: 1,
      tag: '',
      search: '',
      category: '',
    };

    // Determine where should we look for results
    const path = this.props.match.path.split('/');
    const searchType = path.includes('tag') ? { tag: term } : { search: term };

    // Fetch Items
    this.props.getSearchResults({
      ...defaultQuery,
      ...filtersObject,
      ...searchType,
    });
  }

  /**
   * loadPage()
   * @param {object} filters : Object with search filters
   */
  loadPage(filters = { page: 1 }) {
    // Set current page and execute search
    this.currentPage = filters.page;
    this.search(this.props.match.params.term, { ...filters });
  }

  // Render Component
  render() {
    const { articles, match } = this.props;
    const searchTerm = match.params.term;
    return (
      <div>
        <PageHead
          attributes={{
            ...SEO.InspireHomePage,
            title: `${searchTerm} - Inspire | Canasta Rosa`,
            meta: [
              {
                property: 'og:url',
                content: `${config.frontend_host}${this.props.location.pathname}`,
              },
              {
                property: 'og:title',
                content: `${searchTerm} - Inspire | Canasta Rosa`,
              },
            ],
          }}
        />
        <Search style={{ marginBottom: '1em' }} />
        <Listing
          title={`Resultados de ${searchTerm}`}
          articles={articles}
          fetchArticles={this.loadPage}
          page={this.currentPage}
        />
        <CategoriesPopular />
      </div>
    );
  }
}

// Load Data for Server Side Rendering
InspireSearchResultsPage.loadData = (reduxStore, routePath) => {
  const term = routePath.match.params.term;

  // Default query
  const defaultQuery = {
    page: 1,
    tag: '',
    search: '',
    category: '',
  };

  // Determine where should we look for results
  const path = routePath.location.split('/');
  const searchType = path.includes('tag')
    ? {
        tag: term,
      }
    : {
        search: term,
      };

  return Promise.all([
    reduxStore.dispatch(inspireActions.resetSearch()),
    reduxStore.dispatch(
      inspireActions.getSearchResults({
        ...defaultQuery,
        ...searchType,
      }),
    ),
  ]);
};

// Map Redux Props and Actions to component
function mapStateToProps({ inspire }) {
  const { search } = inspire;
  return {
    articles: search,
  };
}
function mapDispatchToProps(dispatch) {
  const { getSearchResults, resetSearch } = inspireActions;
  return bindActionCreators(
    {
      resetSearch,
      getSearchResults,
    },
    dispatch,
  );
}

// Export Connected Component
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(InspireSearchResultsPage);
