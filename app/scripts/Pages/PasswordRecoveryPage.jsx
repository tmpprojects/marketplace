import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Field, reduxForm, SubmissionError } from 'redux-form';

import '../../styles/_recoverpass.scss';
import { userActions, modalBoxActions } from '../Actions';
import Password from '../Utils/Password';
import Login from '../Utils/Login.jsx';
import { PasswordField } from '../Utils/forms/formComponents';
import { password, required } from '../Utils/forms/formValidators';

const password_confirmation = (value, allValues) =>
  value && value !== allValues.password_new
    ? 'Las contraseñas nos coinciden.'
    : undefined;

class PasswordRecoveryPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loaded: false,
      formError: false,
      responseMessage: '',
    };
    this.submitForm = this.submitForm.bind(this);
    this.openModalBox = this.openModalBox.bind(this);
  }

  /**
   * openModalBox()
   * @param {element} target : React Component to be rendered on modalbox
   */
  openModalBox(target) {
    this.props.dispatch(modalBoxActions.open(target));
  }

  /**
   * submitForm()
   * @param {object} formData : Form Data
   */
  submitForm(formData) {
    this.props
      .resetPassword(formData, this.props.match.params.key)
      .then((response) => {
        // Update State
        this.setState({
          loaded: true,
          formError: false,
          responseMessage: 'Tu contraseña se ha reestablecido correctamente.',
        });
      })
      .catch((error) => {
        let responseMessage = error.response.status;
        switch (error.response.status) {
          case 410:
            responseMessage = 'Esta clave ya ha expirado o ha sido utilizada.';
            break;
        }
        // Update State
        this.setState({
          loaded: true,
          formError: error,
          responseMessage,
        });
      });
  }

  /**
   * render()
   */
  render() {
    const { loaded, formError, responseMessage } = this.state;
    const { handleSubmit, pristine, submitting, error } = this.props;
    return (
      <section className="recoverPass">
        {!loaded ? (
          <div>
            <div className="title">
              <h4>Nueva contraseña</h4>
            </div>
            <div className="recoverPass_form">
              <p>
                Ingresa tu nueva contraseña y confirma. Recuerda que está debe
                contener mínimo 8 caracteres.
              </p>
              <form className="form" onSubmit={handleSubmit(this.submitForm)}>
                <fieldset>
                  <Field
                    name="password_new"
                    component={PasswordField}
                    label="Password"
                    validate={[required, password]}
                    placeholder="Password"
                  />

                  <Field
                    name="password_conf"
                    component={PasswordField}
                    label="Confirmación"
                    validate={[required, password, password_confirmation]}
                    placeholder="Confirmación"
                  />

                  <input
                    type="submit"
                    name="passwordRecovery_submit"
                    value="Restablecer Contraseña"
                    disabled={pristine || submitting}
                  />
                </fieldset>
              </form>
            </div>
          </div>
        ) : (
          <div style={{ textAlign: 'center' }}>
            {!formError ? (
              <div>
                <h4>Operación Exitosa</h4>
                <p>{responseMessage}</p>
                <a
                  href="#"
                  className="c2a_square"
                  onClick={(e) => {
                    e.preventDefault();
                    this.openModalBox(Login);
                  }}
                >
                  Ingresa a tu Cuenta
                </a>
              </div>
            ) : (
              <div>
                <h4>Lo sentimos</h4>
                <p>{responseMessage}</p>
                <a
                  href="#"
                  className="c2a_square"
                  onClick={(e) => {
                    e.preventDefault();
                    this.openModalBox(Password);
                  }}
                >
                  Intentar de Nuevo
                </a>
              </div>
            )}
          </div>
        )}
      </section>
    );
  }
}

// Load Data for Server Side Rendering
PasswordRecoveryPage.loadData = (store) => Promise.all([]);

// Wrap component within reduxForm
PasswordRecoveryPage = reduxForm({
  form: 'login_form',
})(PasswordRecoveryPage);

// Map Redux Props and Actions to component
function mapStateToProps(state) {
  return {};
}
function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      resetPassword: userActions.resetPassword,
    },
    dispatch,
  );
}

// Export Connected Component
export default connect(mapStateToProps, mapDispatchToProps)(PasswordRecoveryPage);
