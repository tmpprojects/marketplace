import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import queryString from 'query-string';

import '../../../styles/_search.scss';
import config from '../../../config';
import { appActions, analyticsActions } from '../../Actions';
import PageHead from '../../Utils/PageHead';
import { SEARCH_SECTIONS } from '../../Constants';
import { IconPreloader } from '../../Utils/Preloaders';
import Stores from '../../components/Search/StoresList';
import ProductsList from '../../components/Search/ProductsList';
import Articles from '../../components/Search/ArticlesList';
import CategoriesPopular from '../../Utils/CategoriesPopular';
import withPagination from '../../components/hocs/withPagination';
import SideBarFilters from '../../components/Categories/SideBarFilters';
import { trackWithGTM } from './../../Utils/trackingUtils';

const MAX_ITEMS_PER_PAGE = 15;
const DEFAULT_SORT = '';

/**
 * SORTING LIST COMPONENT
 * Renders a SelectInput with different sorting parameters
 */
const SortingList = (props) => {
  const onChange = function (e) {
    e.preventDefault();
    props.onSortChange(e.target.value);
  };

  return null;
  return (
    <div className="filters">
      <p>Ordenar por:</p>
      <div className="filters__list">
        <select value={props.sort} name="filters" onChange={onChange}>
          <option value="-created">Fecha</option>
          <option value="price">Precio</option>
          <option value="name">Nombre</option>
        </select>
      </div>
    </div>
  );
};

export class SearchCategoryResults extends Component {
  constructor(props) {
    super(props);

    //
    this.currentPage = this.getResultsPageFromQueryString(
      this.props.location.search,
    );
    this.urlValues = this.getURLVals(this.props.location.search);
    this.searchThumbnailComponent = null;
    this.searchQuery = '';

    const initValues = this.prepareComponentForSearch(
      this.props.match.params.term,
      this.props.match.params.section,
    );
    this.state = {
      searchTerm: initValues.searchTerm || undefined,
      searchSection: initValues.searchSection || undefined,
      isMobile: false,
      // Filters
      sortCriteria:
        this.getSortFromQueryString(this.props.location.search) || DEFAULT_SORT,
      isOpen: false,
    };
  }

  componentDidMount() {
    const { searchTerm } = this.state;
    // Make the actual search
    this.performSearch(
      searchTerm,
      this.currentPage,
      this.getSortFromQueryString(this.props.location.search),
      this.getFiltersFromQueryString(this.props.location.search),
    );

    //For Responsive Design
    this.onResizeHandler();
    window.addEventListener('resize', this.onResizeHandler);
  }

  componentWillReceiveProps(nextProps) {
    // If the pathname (URL) has changed, perform a search.
    //si cambia termino de busqueda
    if (this.props.location.pathname !== nextProps.location.pathname) {
      this.performSearch(
        nextProps.match.params.term,
        this.getResultsPageFromQueryString(nextProps.location.search),
        this.getSortFromQueryString(nextProps.location.search),
        this.getFiltersFromQueryString(this.props.location.search),
      );
    }

    //si cambia filtro o pagina
    if (this.props.location.search !== nextProps.location.search) {
      this.currentPage = this.getResultsPageFromQueryString(
        nextProps.location.search,
      );

      this.performTabSearch(
        nextProps.match.params.term,
        this.getResultsPageFromQueryString(nextProps.location.search),
        this.getSortFromQueryString(nextProps.location.search),
        this.getFiltersFromQueryString(nextProps.location.search),
      );
    }

    // If we have new search results,
    // prepare the component to render equivalent thumbnails, etc...
    //si cambia info de componente
    if (this.props.searchResults !== nextProps.searchResults) {
      this.setState(
        this.prepareComponentForSearch(
          nextProps.match.params.term,
          nextProps.match.params.section,
        ),
      );
    }

    //Criteo tracking
    if (this.props.searchResults.products !== nextProps.searchResults.products) {
      const products = nextProps.searchResults.products.results;
      if (products !== undefined) {
        const idProducts = products
          .slice(0, 3)
          .map((product) => product.id.toString());
        trackWithGTM('ListingPage_Criteo', idProducts);

        //Track for endpoint
        const formattedProducts = products.map((prod, index) => ({
          product: prod.slug,
          index,
        }));
        //Send to API
        this.props.trackListingImpressions('search-results-page', formattedProducts);
      }
    }
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.onResizeHandler);
  }

  getURLVals = (query) => {
    const queryParams = this.getQueryParamsAsObject(query.replace('?', ''));

    return Object.keys(queryParams).reduce((a, key) => {
      const ifv = a;

      if (key === 'delivery_date') {
        ifv.delivery_day = 'picked';
        //Only for the option where user selects an specific date, for calendar
        ifv.delivery_day_picked = queryParams[key];
      }

      if (key === 'zone') {
        ifv.shipping = queryParams[key];
      }

      if (key === 'max_price') {
        ifv.price = `max_price_${queryParams[key]}`;
      }

      if (key === 'stores') {
        //Turn stores param into array of Slug Stores
        const storesArray = queryParams[key].replace('+', ' ').split(' ');
        //Array of Selected Stores
        const storesSelected = storesArray.map((s) => {
          const store = this.props.storesList.results.find((st) => st.slug === s);
          //Add 'selected' key so the store appears checked
          return {
            ...store,
            selected: true,
          };
        });

        //Array of Not Selected Stores
        let storesNotSelected = this.props.storesList.results;
        storesSelected.forEach((sl) => {
          storesNotSelected = storesNotSelected.filter((s) => s !== sl);
        });

        ifv.stores = storesSelected.concat(storesNotSelected);
      }
      return ifv;
    }, {});
  };

  /**
   * openFiltersWindow()
   * Update component state to open filters container.
   */
  openFiltersWindow = (toggle) => {
    this.setState({
      isOpen: toggle !== undefined ? toggle : !this.state.isOpen,
    });
  };
  /**
   * onResizeHandler()
   * Listen for device resize and updates component state.
   */

  onResizeHandler = () => {
    let isMobile = false;
    const w =
      window.innerWidth ||
      document.documentElement.clientWidth ||
      document.body.clientWidth;
    if (w < 727) {
      isMobile = true;
    }
    this.setState({
      isMobile,
    });
  };

  /**
   * prepareComponentForSearch()
   * @param {string} searchTerm : Search Keyword
   * @param {string} searchSection : Where to look for the keyword
   */
  prepareComponentForSearch = (searchTerm, searchSection) => {
    // Determine Search Section
    // and define the thumbnail compontent we sall render depending on the section
    switch (searchSection) {
      case SEARCH_SECTIONS.STORES:
        searchSection = SEARCH_SECTIONS.STORES;
        this.searchThumbnailComponent = Stores; // Componente de tiendas
        break;
      case SEARCH_SECTIONS.ARTICLES:
        searchSection = SEARCH_SECTIONS.ARTICLES;
        this.searchThumbnailComponent = Articles; // Componente de inspire
        break;
      case SEARCH_SECTIONS.PRODUCTS:
      default:
        searchSection = SEARCH_SECTIONS.PRODUCTS;
        this.searchThumbnailComponent = ProductsList;
        break;
    }

    // Update state
    return {
      searchTerm,
      searchSection,
    };
  };

  /**
   * onFilterSubmit()
   * @param {string} query | Filters querystring.
   */
  onFilterSubmit = (query) => {
    this.props.history.push(`${this.props.location.pathname}?${query}`);
  };

  /**
   * onSearchTermChange()
   * Controls/Update the search bar input
   * @param {object} e : Form Event
   */
  onSearchTermChange = (e) => {
    e.preventDefault();
    this.setState({
      searchTerm: e.target.value,
    });
  };

  /**
   * updateSearchQuery()
   * @param {string} section : Section where we should look for the search keyword
   */
  updateSearchQuery = (searchTerm = '', section = SEARCH_SECTIONS.PRODUCTS) => {
    // Encode search term.
    // Double encoding is made on porpouse to force encoding the '%' sign.
    if (!searchTerm == '') {
      const encodedTerm = encodeURIComponent(encodeURIComponent(searchTerm));
      this.props.history.push(`/search/${section}/${encodedTerm}`);
    }
  };

  /*
   * performSearch()
   * Constructs a search query string and perfor the search action (API call)
   */
  performSearch = (term = '', page = 1, sortType = DEFAULT_SORT, filters = '') => {
    // Scroll to top of page
    // TODO: Scroll to top of results list instead of top of document.
    window.scrollTo(0, 0);

    // Construct the search query string
    const searchQuery = encodeURI(
      `?sort=${sortType}&${filters}&q=${term}&page=${page}&page_size=${MAX_ITEMS_PER_PAGE}`,
    );

    // Make the call to the API to get results
    // Modificar para que solo llame a uno , sino viene nada si a todos y sino solo a ese
    Promise.all([
      this.props.getSearchStores(searchQuery),
      this.props.getSearchProducts(searchQuery),
      this.props.getSearchArticles(searchQuery),
    ]);
  };

  performTabSearch = (
    term = '',
    page = 1,
    sortType = DEFAULT_SORT,
    filters = '',
  ) => {
    // Scroll to top of page
    // TODO: Scroll to top of results list instead of top of document.
    window.scrollTo(0, 0);

    // Construct the search query string
    const searchQuery = encodeURI(
      `?sort=${sortType}&${filters}&q=${term}&page=${page}&page_size=${MAX_ITEMS_PER_PAGE}`,
    );

    // Make the call to the API to get results
    // Modificar para que solo llame a uno , sino viene nada si a todos y sino solo a ese
    //switch (this.state.searchSection) {
    //    case 'stores':
    this.props.getSearchStores(searchQuery);
    //        break;
    //    case 'articles':
    this.props.getSearchArticles(searchQuery);
    //        break;
    //    case 'products':
    //    default:
    this.props.getSearchProducts(searchQuery);
    //       break;
    //}
  };

  /**
   * onSortChange()
   * @param {string} sortBy | Sorting criteria.
   */
  onSortChange = (sortBy) => {
    this.props.history.push(`${this.props.location.pathname}?sort=${sortBy}`);

    this.setState({ sortCriteria: sortBy });
  };

  /**
   * getFiltersFromQueryString()
   * Extracts filters portion from a query string.
   */
  getFiltersFromQueryString = (query = '') => {
    const queryParams = this.getQueryParamsAsObject(query.replace('?', ''));
    return Object.keys(queryParams).reduce((acc, key) => {
      let qs = acc;
      if (key !== 'sort' && key !== 'p' && key !== 'store') {
        qs += `&${key}=${queryParams[key]}`;
      }
      return qs;
    }, '');
  };

  getResultsPageFromQueryString = (query) => {
    if (query) {
      const resultsPage = parseInt(queryString.parse(query).p, 10);
      return !isNaN(resultsPage) ? resultsPage : 1;
    }
    return 1;
  };

  /**
   * getSortFromQueryString()
   * @param {string} query
   * Extracts and return sort criteria from a query string.
   */
  getSortFromQueryString = (query) => {
    if (query && queryString.parse(query).sort) {
      return queryString.parse(query).sort;
    }
    return '';
  };

  getQueryParamsAsObject = (query = '') => {
    const queryArray = query.split('&');

    return queryArray.reduce((a, b) => {
      const tempAttribute = a;
      const keyValueArr = b.split('=');
      if (keyValueArr.length === 2) {
        tempAttribute[keyValueArr[0]] = keyValueArr[1];
      }
      return tempAttribute;
    }, {});
  };

  render() {
    //return (<div>Hola Mundo</div>);
    const { searchSection, sortCriteria, isMobile, isOpen } = this.state;
    const { searchResults = {}, location } = this.props;
    const currentPage = this.currentPage;
    let searchTerm;
    try {
      searchTerm = decodeURIComponent(this.state.searchTerm);
    } catch (e) {
      searchTerm = this.state.searchTerm;
    }

    //If search Results are empty, show a 'loading' spinner
    let loadingContent = false;
    if (
      !searchResults.articles.results ||
      !searchResults.products.results ||
      !searchResults.stores.results
    ) {
      loadingContent = true;
    }

    // Filters sidebar component.
    const renderSideBar = (
      <SideBarFilters
        stores={{}}
        //selectedStore={}
        isMobile={isMobile}
        close={this.openFiltersWindow}
        location={location}
        onFilterSubmit={this.onFilterSubmit}
        urlValues={urlValues}
      />
    );

    const totalResultsCount = !loadingContent
      ? searchResults.stores.count +
        searchResults.articles.count +
        searchResults.products.count
      : 0;

    //Esto si Get the results and thumbnail component that will be rendered
    const sectionInfo = searchResults ? searchResults[searchSection] : {};
    const sectionResults = sectionInfo ? sectionInfo.results : [];
    const SectionComponent = this.searchThumbnailComponent;
    const PaginatedList = withPagination(SectionComponent);
    const urlValues = this.urlValues;

    // Render Results
    return (
      <section className="searchResults">
        <PageHead
          attributes={{
            title: `Encuentra ${searchTerm} - Market | Canasta Rosa`,
            meta: [
              {
                property: 'og:url',
                content: `${config.frontend_host}${this.props.location.pathname}`,
              },
              {
                property: 'og:title',
                content: `Encuentra ${searchTerm} - Market | Canasta Rosa`,
              },
            ],
          }}
        />
        <div>
          <form
            className="search_form"
            action={'/search'}
            onSubmit={(e) => {
              e.preventDefault();
              this.updateSearchQuery(searchTerm, searchSection);
            }}
          >
            <input
              type="search"
              className="search_field"
              placeholder="Buscar"
              name="searchTerm"
              autoComplete="off"
              value={searchTerm}
              onChange={this.onSearchTermChange}
            />
            <input type="submit" value="Buscar" className="cr__inputSearchBtn" />
          </form>
          <div className="results-details">
            {totalResultsCount}&nbsp; resultado
            {totalResultsCount === 0 || totalResultsCount > 1 ? 's' : ''} para{' '}
            <span className="pink">{searchTerm}</span>
          </div>

          <ul className="menu_searchType">
            <li
              className={`${
                searchSection === SEARCH_SECTIONS.STORES ? 'active' : ''
              } searchType`}
            >
              <Link
                to={`/search/${SEARCH_SECTIONS.STORES}/${encodeURIComponent(
                  searchTerm,
                )}`}
              >
                Tiendas
                <span>
                  ({searchResults.stores.count && searchResults.stores.count})
                </span>
              </Link>
            </li>
            <li
              className={`${
                searchSection === SEARCH_SECTIONS.PRODUCTS ? 'active' : ''
              } searchType`}
            >
              <Link
                to={`/search/${SEARCH_SECTIONS.PRODUCTS}/${encodeURIComponent(
                  searchTerm,
                )}`}
              >
                Productos
                <span>
                  ({searchResults.products.count && searchResults.products.count})
                </span>
              </Link>
            </li>
            <li
              className={`${
                searchSection === SEARCH_SECTIONS.ARTICLES ? 'active' : ''
              } searchType`}
            >
              <Link
                to={`/search/${SEARCH_SECTIONS.ARTICLES}/${encodeURIComponent(
                  searchTerm,
                )}`}
              >
                Posts
                <span>
                  ({searchResults.articles.count && searchResults.articles.count})
                </span>
              </Link>
            </li>
          </ul>
        </div>
        <div className="results-list--container wrapper--center">
          <div className="results-list">
            {!isMobile &&
              searchSection === SEARCH_SECTIONS.PRODUCTS &&
              renderSideBar}

            {loadingContent ? (
              <IconPreloader />
            ) : sectionResults.length > 0 ? (
              <ul className="typeList">
                <div className="filters-bar">
                  {/* Show SortingList only if you are in Products Tab */}
                  {searchSection === SEARCH_SECTIONS.PRODUCTS && (
                    <SortingList
                      sort={sortCriteria}
                      onSortChange={this.onSortChange}
                    />
                  )}

                  {/* Mobile SideBarFilters  */}
                  {isOpen && (
                    <div className={`addToCart ${isOpen ? 'open' : 'close'}`}>
                      {renderSideBar}
                    </div>
                  )}
                  {isMobile && (
                    <a className="icon_filter" onClick={this.openFiltersWindow}>
                      Filtros
                    </a>
                  )}
                </div>
                <PaginatedList
                  items={sectionResults}
                  baseLocation={`/search/${searchSection}/${encodeURIComponent(
                    searchTerm,
                  )}?${this.getFiltersFromQueryString(
                    location.search,
                  )}&sort=${this.getSortFromQueryString(location.search)}`}
                  page={currentPage}
                  maxItems={9}
                  maxItems={MAX_ITEMS_PER_PAGE}
                  totalPages={sectionInfo.npages}
                  location={'search-results-page'}
                />
              </ul>
            ) : (
              <div className="typeList typeList--empty">
                <p className="typeList__message">
                  No se encontraron resultados con esta búsqueda.
                </p>
              </div>
            )}
          </div>
        </div>
        <CategoriesPopular />
      </section>
    );
  }
}

// Load Data for Server Side Rendering
SearchCategoryResults.loadData = (reduxStore, routePath) => {
  const searchTerm = decodeURIComponent(
    encodeURIComponent(routePath.match.params.term),
  );
  //const searchSection = routePath.match.params.section;

  // Construct the search query string
  const searchQuery = `?q=${searchTerm}`;
  return Promise.all([
    reduxStore.dispatch(appActions.getSearchStores(searchQuery)),
    reduxStore.dispatch(appActions.getSearchProducts(searchQuery)),
    reduxStore.dispatch(appActions.getSearchArticles(searchQuery)),
  ]);
};

// Map Redux Props and Actions to component
function mapStateToProps({ app }) {
  return {
    searchResults: app.searchResults,
    marketCategories: app.marketCategories.categories,
  };
}

function mapDispatchToProps(dispatch) {
  const { getSearchStores, getSearchProducts, getSearchArticles } = appActions;

  const { trackListingImpressions } = analyticsActions;

  return bindActionCreators(
    {
      getSearchStores,
      getSearchProducts,
      getSearchArticles,
      trackListingImpressions,
    },
    dispatch,
  );
}

// Export Component
export default connect(mapStateToProps, mapDispatchToProps)(SearchCategoryResults);
