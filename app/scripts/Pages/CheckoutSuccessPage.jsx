import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Moment from 'dayjs';
import { trackWithFacebookPixel } from '../Utils/trackingUtils';

import {
  SHIPPING_METHODS,
  PRODUCT_TYPES,
  PAYMENT_METHODS,
  ORDERS_EMAIL,
  BANK_TRANSFER,
  COUPON_TYPES,
} from '../Constants';
import SEO from '../../statics/SEO.json';
import PageHead from '../Utils/PageHead';
import { shoppingCartActions } from '../Actions';
import { CardsList } from '../components/Home/CardsList';
import { getUserProfile } from '../Reducers/users.reducer';
import { CreateStore } from '../components/Home/CreateStore';
import { formatNumberToPrice } from '../Utils/normalizePrice';
import CardReview from '../components/ShoppingCart/CardReview';
import { Covid19 } from '../../scripts/Utils/modalbox/Covid/Covid19';
import { ImportantMessage } from '../../scripts/Utils/modalbox/ImportantMessage/ImportantMessage';
import { trackWithGTM } from '../Utils/trackingUtils';
import { SHOPPING_CART_FORM_CONFIG } from '../Utils/shoppingCart/shoppingCartFormConfig';
import { Position } from 'acorn';

const newTransferEmail = 'pagos-transferencia@canastarosa.com';

class CheckoutSuccessPage extends Component {
  // PropTypes / DefaultProps
  static propTypes = {
    //products: PropTypes.array.isRequired,
  };

  componentWillMount() {
    // Redirect to Home Page if there´s no order confirmation
    if (!this.props.orderConfirmed) {
      this.props.history.push('/');
    }
  }
  componentDidMount() {
    const { order, cart_price } = this.props;
    // Prevent function from continuing if there´s no confirmation order
    if (!order) {
      return false;
    }

    const items = order.orders.reduce(
      (a, b) => [
        ...a,
        ...b.products.map((p, i) => ({
          id: p.product.id.toString(),
          name: p.product.name,
          // category: 'guides/google-tag-manager/enhanced-ecommerce',
          //variant: p.product.physical_properties.size.display_name,
          brand: b.store.name,
          quantity: p.quantity,
          price: p.unit_price,
          dimension1: p.product.slug,
          dimension2: b.store.slug,
        })),
      ],
      [],
    );

    let shippingAddress = {};
    if (order.shipping_address) {
      shippingAddress = {
        dimension2: order.shipping_address.state,
        dimension3: order.shipping_address.city,
        dimension4: order.shipping_address.neighborhood,
      };
    }

    //Enhanced Ecommerce Purchase
    trackWithGTM('eec.purchase', {
      actionField: {
        id: order.uid,
        affiliation: 'Canasta Rosa',
        revenue: cart_price.toString(),
        dimension1: order.payment_provider_static_name, //shopping method
        ...shippingAddress,
        //shipping,
        //coupon
      },
      products: items,
    });

    //Facebook Pixel
    const itemIds = items.map((item) => item.id);
    trackWithFacebookPixel('track', 'Purchase', {
      value: cart_price.toString(),
      currency: 'MXN',
      content_type: 'product',
      content_ids: itemIds,
      content_name: order.uid,
    });


    try {
      let actionField = {
        "id": `${this.props.order.uid}`,
        "affiliation": "Canasta Rosa",
        "revenue": this.props?.total_price,
        "Tax": null,
        "shipping": this.props?.shipping_price,
        "coupon": this.props?.couponDetails?.type ? this.props?.couponDetails?.type : null
      }

      let dataLayerContent = [];

      this.props.order.orders.map((order, index) => {
        order.products.map((item, indexProduct) => {
          let addItem = {};
          addItem.name = item?.product?.name
          addItem.id = item?.product?.id
          addItem.price = item?.product?.price
          addItem.brand = item?.product?.store?.name
          addItem.category = null
          addItem.variant = null
          addItem.quantity = item?.quantity ? item?.quantity : null
          // console.log("SAVE ITEM", addItem)
          dataLayerContent.push(addItem);
        })
      })

      console.log("this.props->",this.props)
      console.log("dataLayerContent purchase->",dataLayerContent)
      console.log("dataLayerContent purchase->", actionField)

      globalThis.googleAnalytics.purchase(dataLayerContent, actionField)

    } catch (e) {
      console.log("SuccessPage --->", e)
    }

  }

  /**
   * React Lifecycle Methods
   */
  render() {
    // Handle returns and redirects
    if (this.props.loading) return null;

    // Get Order Properties
    const {
      user,
      order,
      cartProductsCount,
      cart_price,
      total_price,
      shipping_price,
      randomProducts = [],
      couponDetails,
    } = this.props;

    const hasPickupMethod = order.orders.reduce(
      (a, o) =>
        !a
          ? o.physical_properties.shipping_method_name ===
          SHIPPING_METHODS.PICKUP_POINT.name
          : false,
      false,
    );
    const paymentMethod = order.payment_provider_static_name;
    const yourSelections = randomProducts.slice(0, 6);

    // Coupon Information
    let couponRow = null;
    if (couponDetails.valid) {
      switch (couponDetails.type) {
        case COUPON_TYPES.QUANTITY:
          couponRow = (
            <p className="cart-resume__breakdown coupon">
              Cupón
              <span className="quantity">-{couponDetails.discount}MX</span>
            </p>
          );
          break;
        case COUPON_TYPES.PERCENTUAL:
        case COUPON_TYPES.FREE_SHIPPING_AND_PERCENTUAL:
          couponRow = (
            <p className="cart-resume__breakdown coupon">
              Cupón
              <span className="quantity">-{couponDetails.discount * 100}%</span>
            </p>
          );
          break;
        default:
          couponRow = null;
      }
    }

    //Decide what text show in thanks message
    let orderConfirmationStatusText = '';
    if (
      paymentMethod === PAYMENT_METHODS.BANK_TRANSFER.slug ||
      paymentMethod === PAYMENT_METHODS.OXXO.slug
    )
      orderConfirmationStatusText = 'esperando pago';
    if (paymentMethod === PAYMENT_METHODS.MERCADO_PAGO.slug)
      orderConfirmationStatusText = 'confirmada';

    // Return Markup
    return (
      <section className="confirmation">
        <PageHead attributes={SEO.CheckoutPage} />
        <React.Fragment>
          {/* <div style={{ marginTop: '0.5em' }}> */} {/* <Covid19 />{' '} */}
          {/* <ImportantMessage /> */}
          {/* </div> */}
        </React.Fragment>
        <div className="msn_confirmation wrapper--center">
          <div className="header">
            <div className="img_container">
              <img src={require('../../images/illustration/loyalty.svg')} alt="" />
            </div>
            <div className="title_container">
              <h3 className="title">
                ¡Gracias por tu compra
                {!user.first_name ? '!' : ` ${user.first_name}!`}
              </h3>
              <p className="order">
                Tu orden&nbsp;
                <Link to={`/users/orders/${order.uid}`}>{order.uid}</Link>&nbsp;
                {`está ${orderConfirmationStatusText}.`}
              </p>
            </div>
          </div>

          <div className="methods_info">
            {/* ORDER 'ON-SITE' PICKUP DETAILS */}
            {/*hasPickupMethod && (
              <div className='pickup-point'>
                <hr />
                <h5 className='title-method'>
                  Método de envío: <span>Recoger en Mercado Rosa</span>
                </h5>
                <p className='address'>
                  Mercado Rosa en Antara Fashion Hall. Av Ejército Nacional 843,
                  Granada, C.P. 11520, CDMX
                  <br />
                  <span className='note--strong'></span>
                </p>
              </div>
            )*/}

            {/* BANK TRANSFER ORDER AND PAYMENT DETAILS  */}
            {paymentMethod === PAYMENT_METHODS.BANK_TRANSFER.slug && (
              <div className="payment-method">
                <hr />
                <h5 className="title-method">
                  Método de pago: <span>Transferencia Bancaria</span>
                </h5>
                <div className="bankTransfer-info">
                  <h5>Realiza tu transferencia a:</h5>
                  <h5>{BANK_TRANSFER.NAME}</h5>
                  <p>
                    <span>Banco:</span> {BANK_TRANSFER.BANK}
                  </p>
                  <p>
                    <span>Cuenta:</span> {BANK_TRANSFER.ACCOUNT}
                  </p>
                  <p>
                    <span>Clabe:</span> {BANK_TRANSFER.CLABE}
                  </p>
                  <p>
                    <span>RFC:</span> {BANK_TRANSFER.RFC}
                  </p>

                  <p className="note">
                    `` Una vez que hayas efectuado tu pago, recuerda enviarnos tu
                    comprobante a &nbsp;
                    <a href={`mailto:${newTransferEmail}`}>{newTransferEmail}</a>
                  </p>
                  <p className="note--small">
                    * Las fechas de entrega pueden variar dependiendo &nbsp; del
                    momento en que confirmemos tu pago.
                  </p>
                </div>
              </div>
            )}

            {/* OXXO PAYMENT DETAILS  */}
            {paymentMethod === PAYMENT_METHODS.OXXO.slug && (
              <div className="payment-method">
                <hr />
                <h5 className="title-method">
                  Método de pago <span>OXXO</span>
                </h5>

                <div className="oxxo-info">
                  <p className="note">
                    Realiza tu pago en cualquier tienda OXXO&nbsp; dentro del{' '}
                    <span className="bold">transcurso máximo de 24 horas</span>.
                    <br />
                    Presenta al personal&nbsp;
                    <a
                      target="_blank"
                      rel="noopener noreferrer"
                      className="link"
                      href={
                        order.payment_response.response.transaction_details
                          .external_resource_url
                      }
                    >
                      tu código
                    </a>
                    &nbsp; para realizar el pago de tu orden.
                  </p>
                  <p className="note">
                    Una vez completado tu pago, se verá validado y confirmado de 24 a
                    84 horas.
                    <br />
                    Recuerda que el tiempo de entrega empieza a contar desde que se
                    confirma el pago.
                    <br />
                    Te recomendamos realizar el depósito antes de las 4:00pm.
                  </p>

                  <a
                    target="_blank"
                    rel="noopener noreferrer"
                    className="payment-document-link button-square--pink"
                    href={
                      order.payment_response.response.transaction_details
                        .external_resource_url
                    }
                  >
                    Ver código para realizar tu pago.
                  </a>

                  <p className="note--small">
                    * Las fechas de entrega pueden variar dependiendo&nbsp; del
                    momento en que confirmemos tu pago.
                  </p>
                </div>
              </div>
            )}
          </div>
        </div>

        <div className="cart_container">
          <div className="cart wrapper--center">
            <p className="confirmation-text">
              Pronto recibirás un correo a la dirección
              <span className="mail"> {user.email} </span>
              con todos los detalles de tu compra.
            </p>
            {/* <p className="subtitle">Imprimir Recibo</p> */}
            <div className="promo-container">
              <h5>¡OBTÉN UN ENVÍO GRATIS!</h5>
              <p className="copy">
                Sube una foto de tu producto o de tu celebración a
                <span className="bold instagram">Instagram </span> mencionando a
                <span className="bold"> @canastarosa</span> con el
                <span className="bold">#compracanastarosa</span>  y recibe un cupón
                de ENVÍO GRATIS para tu siguiente compra.
              </p>
            </div>
            <div className="cart wrapper--center order-resume">
              <div className="order_container">
                <div className="order_container--detail">
                  <p>
                    Número de orden:&nbsp;
                    <Link to={`/users/orders/${order.uid}`}>{order.uid}</Link>
                  </p>
                  <p>
                    Realizada el:&nbsp;
                    {Moment(order.created).format('D MMMM YYYY')}
                  </p>
                </div>
                <CardReview productsByStore={order.orders} />
              </div>

              <div className="cart-resume">
                <p className="cart-resume__breakdown">
                  Productos
                  <span className="products-count">
                    ({cartProductsCount}&nbsp; producto
                    {cartProductsCount !== 1 ? 's' : ''})
                  </span>
                  <span className="quantity">{formatNumberToPrice(cart_price)}</span>
                </p>
                <p className="cart-resume__breakdown">
                  Envío{' '}
                  <span className="quantity">
                    {formatNumberToPrice(shipping_price)}MX
                  </span>
                </p>

                {couponRow}
                <div className="cart-resume__breakdown cart-resume__total">
                  <div>
                    Total
                    <span className="cr__text--caption cr__textColor--colorGray300 taxIncluded">
                      IVA incluido
                    </span>
                  </div>
                  <span className="quantity">
                    {formatNumberToPrice(total_price)}MX
                  </span>
                </div>
              </div>
            </div>
          </div>
        </div>
        <CardsList products={yourSelections} title="Artículos similares" />
        <CreateStore />
      </section>
    );
  }
}

// Map Redux Props and Actions and export component
function mapStateToProps(state) {
  const { cart, app } = state;
  const profile = getUserProfile(state);
  const orderConfirmation = cart.order_confirmation;
  if (orderConfirmation.loading) {
    return {
      loading: true,
    };
  }

  const coupon = orderConfirmation.couponDetails;
  const hasFreeShippingCoupon =
    coupon.valid &&
    (coupon.type === COUPON_TYPES.FREE_SHIPPING ||
      coupon.type === COUPON_TYPES.FREE_SHIPPING_ALL ||
      coupon.type === COUPON_TYPES.FREE_SHIPPING_UNIQUE ||
      coupon.type === COUPON_TYPES.FREE_SHIPPING_STORE ||
      coupon.type === COUPON_TYPES.FREE_SHIPPING_GENERAL_STORE ||
      coupon.type === COUPON_TYPES.FREE_SHIPPING_AND_PERCENTUAL);

  //
  return {
    loading: false,
    couponDetails: coupon,
    platformShippinMethods: app.shippingMethods,
    randomProducts: app.randomProducts.products.filter(
      (p) => p.product_type.value === PRODUCT_TYPES.PHYSICAL,
    ),
    order: orderConfirmation,
    orderConfirmed: cart.order_confirmed,
    total_price: parseInt(orderConfirmation.price, 10) || 0,
    cart_price: orderConfirmation.orders.reduce(
      (count, order) => count + parseInt(order.products_price, 10),
      0,
    ),
    cartProductsCount: orderConfirmation.orders.reduce(
      (count, order) =>
        count +
        order.products.reduce((q, product) => q + parseInt(product.quantity, 10), 0),
      0,
    ),
    shipping_price:
      orderConfirmation.orders.reduce(
        (a, order) => a + parseInt(order.physical_properties.shipping_price, 10),
        0,
      ) * (hasFreeShippingCoupon ? 0 : 1),
    user: profile,
  };
}
function mapDispatchToProps(dispatch) {
  const { emptyShoppingCart } = shoppingCartActions;
  return bindActionCreators(
    {
      emptyShoppingCart,
    },
    dispatch,
  );
}

// Export Component
export default connect(mapStateToProps, mapDispatchToProps)(CheckoutSuccessPage);
