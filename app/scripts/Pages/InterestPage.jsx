import React, { Component } from 'react';
import { Link, NavLink } from 'react-router-dom';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import queryString from 'query-string';

import '../../styles/_categorySearchResults.scss';
import { appActions, analyticsActions } from '../Actions';
import SEO from '../../statics/SEO.json';
import PageHead from '../Utils/PageHead';
import { IconPreloader } from '../Utils/Preloaders';
import { ResponsiveImage, ResponsiveImageFromURL } from '../Utils/ImageComponents';
import CategoriesPopular from '../Utils/CategoriesPopular';
import ProductsList from '../components/Search/ProductsList';
import withPagination from '../components/hocs/withPagination';
import { Assistant } from '../Utils/Assistant';
import SeasonBanner from '../Utils/SeasonBanner';
import SideBarFilters from '../components/Categories/SideBarFilters';
import { SIDEBAR_FILTERS_FORM_CONFIG } from '../Utils/SideBarFilters/SideBarFiltersConfig';
import { trackWithGTM } from './../Utils/trackingUtils';
import AwardsBanner from '../components/Awards/AwardsBanner';
import { insertKeyToQueryParam } from './../Utils/queryParamsUtils';
import HoraRosa from '../components/HoraRosa/HoraRosa.js';

const MAX_ITEMS_PER_PAGE = 30;
const ENVIO_NACIONAL_SLUG = 'envio-nacional';

let breadcrumbsPath;
let dataLayerInit;
let dataLayerPage;

/**
 * SORTING LIST COMPONENT
 * Renders a SelectInput with different sorting parameters
 */
const SortingList = (props) => {
  const onChange = function (e) {
    props.onSortChange(e.target.value);
  };

  //return null;
  return (
    <div className="filters">
      <p>Ordenar por:</p>
      <div className="filters__list">
        <select name="filters" onChange={onChange} value={props.sort}>
          <option value="">Relevancia</option>
          <option value="price">Precio menor a mayor</option>
          <option value="-price">Precio mayor a menor</option>
        </select>
      </div>
    </div>
  );
};

/**
 * ProductsList Component (with pagination)
 * Wraps a list of products within 'withPagination' HOC,
 * and displays a paginated component.
 */
const PaginatedList = withPagination(ProductsList);

/**
 * InterestPage Class Component
 */
export class InterestPage extends Component {
  constructor(props) {
    dataLayerInit = '';
    super(props);
    this.defaultSort = '';
    this.queryParams = {
      p: this.props.location.search
        ? queryString.parse(this.props.location.search).p
        : 1,
      sort: '',
      ...this.mapQueryParamsToComponentParams(this.props.location.search),
    };

    // Component state
    this.state = {
      isOpen: false,
      isMobile: false,
      firstTimeCharged: true,
    };
  }

  /**
   * React Life Cycle Methods
   */
  componentDidMount() {
    // shippingAddressObserver
    globalThis.shippingAddressObserver.subscribe(this.updateResults);

    // Fetch category listing
    if (
      !this.props.categoryResults.previouslyLoaded ||
      this.state.firstTimeCharged
    ) {
      // Perform the search
      this.performSeach(
        this.props.match.params.interestName,
        this.queryParams.p,
        this.getFiltersFromQueryParams(this.queryParams),
        this.queryParams.sort,
      );
    }

    //
    this.onResizeHandler();
    window.addEventListener('resize', this.onResizeHandler);
    this.setState({ firstTimeCharged: false });
  }

  componentWillReceiveProps(nextProps) {

    //DATALAYER - HOOOKS
    if ((JSON.stringify(this.props.categoryResults.results) != JSON.stringify(nextProps?.categoryResults?.results)) && nextProps?.categoryResults?.results.length > 0 && nextProps?.categoryResults?.loading != true) {
      if (dataLayerInit != nextProps.match.params.interestName || dataLayerPage != this.queryParams.p) {
        console.log("dispatchDataLayer");
        this.dispatchDataLayer(nextProps?.categoryResults?.results)
        dataLayerInit = nextProps.match.params.interestName;
        dataLayerPage = this.queryParams.p;
      }
    }

    // If search query string (Ej. URL ?p={pageNumber}&store={storeSlug}) has changed
    if (this.props.location.search !== nextProps.location.search) {
      this.queryParams = {
        //p: 1,
        ...this.mapQueryParamsToComponentParams(nextProps.location.search),
      };

      this.performSeach(
        nextProps.match.params.interestName,
        this.queryParams.p,
        this.getFiltersFromQueryParams(this.queryParams),
        this.queryParams.sort,
      );
    }

    // If pathname (category name) has changed, perform a new search
    if (this.props.location.pathname !== nextProps.location.pathname) {
      // console.log('another Pathname!!');
      this.performSeach(nextProps.match.params.interestName);
    }


  }

  componentWillUnmount() {
    dataLayerInit="";
    // shippingAddressObserver
    globalThis.shippingAddressObserver.unsubscribe(this.updateResults);
    window.removeEventListener('resize', this.onResizeHandler);
  }

  mapQueryParamsToComponentParams = (query = '') => {
    const queryParams = queryString.parse(queryString.extract(query));

    //
    return Object.keys(queryParams).reduce(
      (a, key) => {
        const ifv = a;

        // Page
        if (key === 'p') {
          const resultsPage = parseInt(queryParams.p, 10);
          ifv.p = isNaN(resultsPage) ? 1 : resultsPage;
        }

        if (key === 'delivery_date') {
          ifv.delivery_day = 'picked';
          //Only for the option where user selects an specific date, for calendar
          ifv.delivery_day_picked = queryParams[key];
        }

        if (key === 'zone') {
          ifv.shipping = queryParams[key];
        }

        // if (key === 'max_price') {
        //   ifv.price = `max_price_${queryParams[key]}`;
        // }

        // if (key === 'min_price') {
        //   ifv.min_price = `min_price_${queryParams[key]}`;
        // }

        if (key === 'stores') {
          //create a special key in queryParams property of the class, this is used in getFilterParams to build the correct url when filter is activated
          const storesString = queryParams[key].split(' ').join('+');
          //Turn stores param into array of Slug Stores
          const storesArray = queryParams[key].replace('+', ' ').split(' ');
          //Array of Selected Stores
          const storesSelected = storesArray.map((s) => {
            const store = this.props.storesList.results.find((st) => st.slug === s);
            //Add 'selected' key so the store appears checked
            return {
              ...store,
              selected: true,
            };
          });

          //Array of Not Selected Stores
          let storesNotSelected = this.props.storesList.results;
          storesSelected.forEach((sl) => {
            storesNotSelected = storesNotSelected.filter((s) => s.slug !== sl.slug);
          });

          ifv.stores = storesSelected.concat(storesNotSelected);
          ifv.storesString = storesString;
        }
        return ifv;
      },
      { ...queryParams },
    );
  };

  /**
   * onResizeHandler()
   * Listen for device resize and updates component state.
   */
  onResizeHandler = () => {
    let isMobile = false;
    const w =
      window.innerWidth ||
      document.documentElement.clientWidth ||
      document.body.clientWidth;
    if (w < 727) {
      isMobile = true;
    }
    this.setState({
      isMobile,
    });
  };

  /**
   * onSortChange()
   * @param {string} sortBy | Sorting criteria.
   */
  onSortChange = (sortBy) => {
    this.queryParams.sort = sortBy;

    // Update URL to refresh content
    this.props.history.push(
      `${this.props.location.pathname}?${queryString.stringify(this.queryParams)}`,
    );
  };

  /**
   * onFilterSubmit()
   * @param {string} query | Filters querystring.
   */
  onFilterSubmit = (query) => {
    this.props.history.push(`${this.props.location.pathname}?${query}`);
  };

  /**
   * getFiltersFromQueryParams()
   * Extracts filters portion from a query string.
   */
  getFiltersFromQueryParams = (queryParams = {}) => {
    return Object.keys(queryParams).reduce((acc, key) => {
      let qs = acc;
      if (
        key === 'delivery_date' ||
        key === 'min_price' ||
        key === 'max_price' ||
        key === 'order_zipcode' ||
        key === 'zipcode'
      ) {
        qs += `${key}=${queryParams[key]}&`;
      }

      if (key === 'storesString') {
        qs += `stores=${queryParams[key]}&`;
      }

      // console.log('getFiltersFromQueryParams qs', qs);
      return qs;
    }, '');
  };

  /**
   * openFiltersWindow()
   * Update component state to open filters container.
   */
  openFiltersWindow = (toggle) => {
    this.setState({
      isOpen: toggle !== undefined ? toggle : !this.state.isOpen,
    });
  };

  /**
   * fetchContent()
   * Retrieves results from the API. Call redux actions.
   * @param {string} query | Query string to send to API
   */
  fetchContent = (query, slug) =>
    Promise.all([
      this.props.getCategoryResults(query.categoryQuery, slug),
      this.props.getStoresList(query.storesQuery, slug),
    ]);

  /**
   * updateResults()
   * Update category results
   */
  updateResults = () => {
    this.props.history.push(`${this.props.location.pathname}`);
    this.performSeach(
      this.props.match.params.interestName,
      //this.queryParams.p,
      //this.getFiltersFromQueryParams(this.queryParams),
      //this.queryParams.sort
    );
  };

  /**
   * performSeach()
   * Constructs a search query string to perform the search.
   * @param {string} interestName | Category Slug
   * @param {int} page | Page index
   * @param {string} filters | Filters querystring portion
   * @param {string} sort | Sorting 'slug'
   * @param {int} itemsPerPage | Maximum number of items per page
   */
  performSeach = (
    interestName,
    page = 1,
    filters = '',
    sortType = this.defaultSort,
    itemsPerPage = MAX_ITEMS_PER_PAGE,
  ) => {
    // Scroll to top of page
    // TODO: Scroll to top of results list instead of top of document.
    window.scrollTo(0, 0);

    // Construct search query parameters
    const searchQueries = buildQuery(
      {
        filters,
        page,
        slug: interestName,
        pageSize: itemsPerPage,
        sort: sortType,
      },
      {
        filters,
        slug: interestName,
        pageSize: 21,
      },
    );

    // Make the call to the API to get results
    //const store = this.getStoreFromQueryString(filters);
    this.fetchContent(searchQueries, interestName);
  };

  /**
   * findCategoryDetails()
   * @param {array} categories | List of categories.
   * @param {string} categorySlug | Category slug to match.
   * @returns {object} Category Object
   */
  findCategoryDetails = (categories, categorySlug) =>
    categories.reduce((a, b) => {
      // If we´ve found category details, bubble it up.
      if (a) {
        return a;
      }

      // Return currentItem if it matches categorySlug
      if (b.slug === categorySlug) {
        return b;
      }

      // Else, use this function recursevly with category children.
      return this.findCategoryDetails(b.children, categorySlug);
    }, null);

  /**
   * createBreadcrumbsTree()
   * Returns an array with the marching categories path.
   * @param {array} categories | Array of nested categories to search from.
   * @param {string} targetCategorySlug | Category String to search.
   * @param {bool} found | Indicates if category has been already found.
   * @param {array} parentPath | Array with the parent categories.
   * @returns {array} An array of categories (Model example: [{name, slug}, ...]).
   */
  createBreadcrumbsTree = (
    categories,
    targetCategorySlug,
    found = false,
    parentPath = [],
  ) => {
    const tester = categories.reduce(
      (a, b) => {
        let result = a;

        // If category is not found, proceed to recursive loop.
        if (!result.found) {
          // If we've found the matching category, return current result
          if (b.slug === targetCategorySlug) {
            result = {
              path: [...result.path, { name: b.name, slug: b.slug }],
              found: true,
            };

            // else, search whithin current category children.
          } else if (b.children.length) {
            const innerCategoryPath = this.createBreadcrumbsTree(
              b.children,
              targetCategorySlug,
              false,
              [...result.path, { name: b.name, slug: b.slug }],
            );

            if (innerCategoryPath.found) {
              result = innerCategoryPath;
            }
          }
        }

        // Return current path.
        return result;
      },
      { found, path: parentPath },
    );

    return tester;
  };

  dispatchDataLayer(dataLayerResults) {


    try {

      //breadcrumbs
      let dataLayerDimensions = '';
      const { marketInterests = [], categoryResults, match, location } = this.props;
      const interestName = match.params.interestName;
      breadcrumbsPath = this.createBreadcrumbsTree(marketInterests, interestName)
      .path;
      let latestName = '';
      breadcrumbsPath.map((item, index) => {
        //if (index !== 0) {
          dataLayerDimensions += `"cd${(index + 1)}": "${item.name}",\n\r`;
        //}
        latestName = item.name;
      });
      //breadcrumbs

      //products
      let dataLayerContent = '[';
      dataLayerResults.map((item, index) => {
        dataLayerContent += `
       {
        "name": "${globalThis.googleAnalytics.utils.clearLayerText(item?.name)}",
        "id": "${item?.id}",
        "price": "${item?.price}",
        "brand": "${globalThis.googleAnalytics.utils.clearLayerText(item?.store?.name)}",
        "category": "${globalThis.googleAnalytics.utils.clearLayerText(latestName)}",
        ${dataLayerDimensions}
        "variant": null,
        "list": "Intereses",
        "position": ${index}
       }${globalThis.googleAnalytics.utils.closeArray(index, dataLayerResults)} 
        `;
      });
      //products

      const dataLayerBreadcrumbs = globalThis.googleAnalytics.utils.checkBreadcrumbs(breadcrumbsPath);
      //console.log("dataLayerContent ", dataLayerContent)
      dataLayerContent = JSON.parse(dataLayerContent);

      //Call productImpression  - event
      globalThis.googleAnalytics.productImpression(dataLayerContent, dataLayerBreadcrumbs);

    } catch (e) {
      console.log("invoke productImpression [InterestPage.jsx]", e)
    }
  }

  /**
   * render()
   */
  render() {
    const { marketInterests = [], categoryResults, match, location } = this.props;
    let { storesList = {} } = this.props;
    const { p: currPage, ...urlValues } = this.queryParams;
    const {
      p,
      stores,
      storesString,
      ...urlValuesToInjectInBaseLocation
    } = this.queryParams;
    urlValuesToInjectInBaseLocation.stores = storesString;
    const interestName = match.params.interestName;
    const sortCriteria = urlValues.sort;
    const { isMobile, isOpen } = this.state;

    // Sort stores list alphabetically
    storesList = {
      ...storesList,
      stores: storesList.results.sort((a, b) => {
        if (a.name < b.name) {
          return -1;
        }
        if (a.name > b.name) {
          return 1;
        }
        return 0;
      }),
    };

    // Find Category tree for breadcrumbs.
    breadcrumbsPath = this.createBreadcrumbsTree(marketInterests, interestName)
      .path;



    // Filters sidebar component.
    const renderSideBar = (
      <SideBarFilters
        stores={storesList}
        searchResults={categoryResults.results}
        isMobile={isMobile}
        close={this.openFiltersWindow}
        location={location}
        onFilterSubmit={this.onFilterSubmit}
        urlValues={urlValues}
        formName={SIDEBAR_FILTERS_FORM_CONFIG.formName}
      />
    );

    // Show preloader if categoryResults are still loading.
    let loadingContent = false;
    if (categoryResults.loading || !categoryResults.results) {
      loadingContent = true;
    }

    // Get Category Details.
    let category = { name: '', description: '' };
    if (marketInterests.length) {
      category = {
        ...category,
        ...this.findCategoryDetails(marketInterests, interestName),
      };
    }

    // This is a PATCH.
    // This hardcodes category values if
    // the querystring passed on the URL is 'envíos nacionales'.
    // This category is not dynamic.
    if (interestName === ENVIO_NACIONAL_SLUG) {
      category = {
        name: 'Envío Nacional',
        description: '',
        slug: ENVIO_NACIONAL_SLUG,
        photo: {},
        text: 'Envío Nacional',
        banner: {
          title: 'Envío Nacional',
          seo_tags: '',
        },
      };
    }

    // Render Results
    return (
      <section className="categoryResults">
        <PageHead
          attributes={{
            title: `${category.name} - Market | Canasta Rosa`,
          }}
        />

        <HoraRosa location={this.props.location} little={true}></HoraRosa>

        <div className="results_list">
          <div className="wrapper--center">
            {/* CATEGORY COVER */}
            <div className="category-cover">
              <div className="category-cover__image" />

              <div className="details">
                <h1 className="details__name">{category.name}</h1>
                <p className="details__description">{category.description}</p>
              </div>
            </div>
            {/* END: /CATEGORY COVER */}
            <Assistant />
            {/* <SeasonBanner /> */}
            {/* BREADCRUMBS */}
            {breadcrumbsPath.length > 0 && (
              <nav className="breadcrumbs">
                <ul className="breadcrumbs__list">
                  <li className="category">
                    <NavLink to="/" className="category__link">
                      Home
                    </NavLink>
                  </li>
                  {breadcrumbsPath.map((c) => (
                    <li className="category" key={c.slug}>
                      <NavLink
                        to={`/interest/${c.slug}`}
                        className="category__link"
                        activeClassName="category__link--active"
                      >
                        {c.name}
                      </NavLink>
                    </li>
                  ))}
                </ul>
                &nbsp;
                <span className="breadcrumbs__counter">
                  ({categoryResults.count} producto
                  {categoryResults.count !== 1 ? 's' : ''})
                </span>
              </nav>
            )}
            {/* END: BREADCRUMBS */}

            <div className="results_list-container">
              {!isMobile && renderSideBar}

              {loadingContent ? (
                <IconPreloader />
              ) : (
                  <div className="productsList">
                    {/* TITLE & SORTING CONTAINER */}
                    <div className="title_container">
                      <h5 className="title">
                        <span>{categoryResults.count}&nbsp;</span>
                      resultado{categoryResults.results.length !== 1 ? 's' : ''}
                      </h5>
                      {isOpen && (
                        <div className={`addToCart ${isOpen ? 'open' : 'close'}`}>
                          {renderSideBar}
                        </div>
                      )}

                      <SortingList
                        sort={sortCriteria}
                        onSortChange={this.onSortChange}
                      />
                      {isMobile && (
                        <a
                          className="icon_filter"
                          onClick={() => this.openFiltersWindow(true)}
                        >
                          Filtros
                        </a>
                      )}
                    </div>
                    {/* END: /TITLE & SORTING CONTAINER */}

                    {/* PAGINATED LIST */}
                    <PaginatedList
                      items={categoryResults.results}
                      baseLocation={`?${queryString.stringify(
                        {
                          ...urlValuesToInjectInBaseLocation,
                        },
                        { encode: false },
                      )}`}
                      page={currPage}
                      maxItems={MAX_ITEMS_PER_PAGE}
                      totalPages={categoryResults.npages}
                      location={'category-page'}
                    />
                    {/* END: /PAGINATED LIST */}
                  </div>
                )}
            </div>
          </div>
        </div>

        <CategoriesPopular />
      </section>
    );
  }
}

const buildQuery = (categoryQuery, storesQuery) => {
  //
  return {
    categoryQuery: `?interest__slug=${categoryQuery.slug}&ordering=${categoryQuery.sort}&page=${categoryQuery.page}&page_size=${categoryQuery.pageSize}&${categoryQuery.filters}`,
    storesQuery: `?products__interest__slug=${storesQuery.slug}&page_size=${storesQuery.pageSize}&${categoryQuery.filters}`,
  };
};

// Load Data for Server Side Rendering
InterestPage.loadData = (reduxStore, routePath) => {
  const { match, params } = routePath;

  //
  const resultsPage = params?.p ? params.p : 1;
  const searchQueries = buildQuery(
    {
      slug: match.params.interestName,
      page: resultsPage,
      pageSize: MAX_ITEMS_PER_PAGE,
      filters: '',
      sort: '',
    },
    {
      slug: match.params.interestName,
      pageSize: 21,
    },
  );

  return Promise.all([
    reduxStore.dispatch(appActions.getCategoryResults(searchQueries.categoryQuery)),
    reduxStore.dispatch(appActions.getStoresList(searchQueries.storesQuery)),
  ]).catch((e) => {
    return false;
  });
};

// Map Redux Props and Actions to component
function mapStateToProps({ app }) {
  return {
    storesList: app.storesList,
    categoryResults: app.categoryResults,
    marketInterests: app.marketInterests.interests,
  };
}
function mapDispatchToProps(dispatch) {
  const { getCategoryResults, getStoresList } = appActions;
  const { trackListingImpressions } = analyticsActions;
  return bindActionCreators(
    {
      getStoresList,
      getCategoryResults,
      trackListingImpressions,
    },
    dispatch,
  );
}

// Export Component
export default connect(mapStateToProps, mapDispatchToProps)(InterestPage);
