import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Parser from 'html-react-parser';
import image302 from './../../images/illustration/403.svg';
import CategoriesPopular from '../Utils/CategoriesPopular';
import '../../styles/_inspireSinglePage.scss';
import '../../styles/_errorPage.scss';
import config from '../../config';
import SEO from '../../statics/SEO.json';
import PageHead from '../Utils/PageHead';
import { inspireActions } from '../Actions';
import Search from '../components/Inspire/Search';
import { IconPreloader } from '../Utils/Preloaders';
import Sidebar from '../components/Inspire/Sidebar';
import { StandardImage } from '../Utils/ImageComponents';
import { MarketRelated } from '../components/Inspire/MarketRelated';
import { socialNetworksShareMessages } from '../Constants/config.constants';
import { shareOnFacebook, shareOnTwitter } from '../Utils/socialMediaUtils';
import Moment from 'dayjs';
import HoraRosa from '../components/HoraRosa/HoraRosa.js';
/*----------------------------------------------------
    MATERIALES COMPONENT
----------------------------------------------------*/
const Materials = (props) => {
  const materials = props.materials.map((item, i) => (
    <li className="materials__list__item" key={i}>
      {item}
    </li>
  ));
  return <ul className="materials__list">{materials}</ul>;
};
Materials.propTypes = {
  materials: PropTypes.array.isRequired,
};
Materials.defaultProps = {
  materials: [],
};

/*----------------------------------------------------
    INSPIRE SINGLE PAGE COMPONENT
----------------------------------------------------*/
export class InspireSinglePage extends Component {
  constructor(props) {
    super(props);
    const { staticContext } = props;
    this.setCode = process.env.CLIENT
      ? window.INIT_NODE.context.status
      : staticContext.status;
    // Define initial state.
    this.state = {
      status: this.setCode,
    };
  }

  componentDidMount() {
    // Reset Articles List and Fetch New results
    this.props.resetArticles();
    this.props.fetchArticles();
    this.props.fetchDetail(this.props.match.params.articleSlug);
    this.restoreStatusCode();
  }
  componentWillReceiveProps(nextProps) {
    // If pathname changes, reset and load new results
    if (this.props.match.params.articleSlug !== nextProps.match.params.articleSlug) {
      this.props.fetchDetail(nextProps.match.params.articleSlug);
      this.state.status = 200;
      this.forceUpdate();
    }
  }
  sanitizeUrl(slug) {
    const extractSlug = slug.split('_');
    let newSlug = '';
    let num = 0;
    for (let obj of extractSlug) {
      newSlug += `${obj} `;
      num++;
    }
    return newSlug;
  }
  restoreStatusCode() {
    if (process.env.CLIENT) {
      window.INIT_NODE.context.status = 200;
    }
  }
  componentDidCatch(error, info) {
    console.log('Error InspireSinglePage');
  }
  render() {
    if (this.state.status === 404) {
      return (
        <div className="ProductError">
          <img src={image302} />
          <h3 className="title--main">
            [4] No logramos encontrar el artículo:
            <br />
            <strong className="ProductErrorName">
              {this.sanitizeUrl(this.props.match.params.articleSlug)}
            </strong>
          </h3>

          <p>Posiblemente se movió a otra sección o se ha eliminado.</p>
          <p>
            <a
              href={
                '/search/articles/' +
                this.sanitizeUrl(this.props.match.params.articleSlug)
              }
            >
              Ver otros resultados
            </a>
          </p>
          <div className="ProductSpace" />
          <CategoriesPopular />
        </div>
      );
    }
    if (this.props.single.loading) {
      return <IconPreloader />;
    }

    const {
      cover_photo,
      title,
      article_author_first_name,
      article_author_last_name,
      author,
      article_url_source,
      marketplace_URL,
      published_date,
      text,
      excerpt,
      materials: materials = [],
    } = this.props.single;
    const articlesObject = this.props.articles || {};

    // Render Component
    return (
      <div className="single">
        <HoraRosa location={this.props.location} little={true}></HoraRosa>
        <PageHead
          attributes={{
            title: `${title.substring(0, 40)}... | Canasta Rosa`, //For SEO
            meta: [
              {
                name: 'description',
                content: excerpt,
              },
              {
                property: 'og:url',
                content: `${config.frontend_host}${this.props.location.pathname}`,
              },
              {
                property: 'og:title',
                content: `${title} - Inspire | Canasta Rosa`,
              },
              {
                property: 'og:image',
                content: cover_photo,
              },
              {
                property: 'og:image:width',
                content: '500',
              },
              {
                property: 'og:image:height',
                content: '200',
              },
              {
                property: 'og:description',
                content: excerpt,
              },
            ],
          }}
        />
        <Search />

        <div className="single__wrapper">
          <section className="article-container">
            <div className="article-container__share">
              {/* <a 
                                    href="#" 
                                    className="add"
                                ><span>Agregar a Colecci&oacute;n</span></a>
                            */}
              <a href={marketplace_URL} className="market">
                <span>Compra en marketplace</span>
              </a>
              <div className="social-share">
                <a
                  href="#"
                  onClick={(e) => {
                    e.preventDefault();
                    shareOnFacebook(window.location.href);
                  }}
                  className="icon icon--facebook"
                >
                  <span>Compartir</span>
                </a>
                <a
                  href="#"
                  onClick={(e) => {
                    e.preventDefault();
                    shareOnTwitter(
                      window.location.href,
                      socialNetworksShareMessages.TWITTER_INSPIRE,
                    );
                  }}
                  className="icon icon--twitter"
                >
                  <span>Tweet</span>
                </a>
                {/* <a 
                                    href="" 
                                    className="icon icon--pinterest"
                                ><span>Pin</span></a> */}
              </div>
            </div>

            <div className="article-container__cover">
              <a href={article_url_source} target="_blank" className="link">
                <StandardImage src={cover_photo} alt={title} className="image" />
              </a>
            </div>

            <div className="author">
              <div className="author__info">
                {/*<img src={'/images/header_profile@2x.png'} alt={title} />*/}
                <div>
                  <h5>
                    Creado por:&nbsp;
                    <span>{`${article_author_first_name} ${article_author_last_name}`}</span>
                  </h5>
                  {/*<a href="#">@{author}</a>*/}
                </div>
              </div>
              <div className="author__links">
                <div className="author__links--wrapper">
                  {/* <a
                    href={article_url_source}
                    className="author__links--source"
                    target="_blank"
                  >
                    Ver art&iacute;culo completo
                  </a> */}
                  <a
                    href={marketplace_URL}
                    className="author__links--market c2a_square"
                  >
                    Compra en marketplace
                  </a>
                </div>
              </div>
            </div>

            <div className="meta">
              <div className="meta__date">
                Publicado el <span>{Moment(published_date).format('LL')}</span>
              </div>

              <h1 className="title">{title}</h1>

              <div className="meta__excerpt">{Parser(String(excerpt))}</div>
            </div>

            <div className="content">{Parser(String(text))}</div>

            {materials.length > 0 && (
              <div className="materials">
                <h3>Materiales & ingredientes</h3>
                <Materials materials={materials} />
              </div>
            )}
          </section>

          {/*
                    <section className="related-marketplace">
                        <MarketRelated />
                    </section>
                    */}

          <Sidebar articles={articlesObject} tags={this.props.single.tags} />
        </div>
      </div>
    );
  }
}

// Load Data for Server Side Rendering
InspireSinglePage.loadData = (reduxStore, routePath) => {
  const articleSlug = routePath.match.params.articleSlug;

  return Promise.all([
    reduxStore.dispatch(inspireActions.resetArticles()),
    reduxStore.dispatch(inspireActions.fetchArticles()),
    reduxStore.dispatch(inspireActions.fetchDetail(articleSlug)),
  ]).catch((e) => {
    return false;
  });
};

// Map Redux Props and Actions to component
function mapStateToProps({ inspire }) {
  const { single, articles } = inspire;
  return { single, articles };
}
function mapDispatchToProps(dispatch) {
  const { fetchDetail, resetArticles, fetchArticles } = inspireActions;
  return bindActionCreators(
    {
      fetchDetail,
      fetchArticles,
      resetArticles,
    },
    dispatch,
  );
}

// Export Connected Component
export default connect(mapStateToProps, mapDispatchToProps)(InspireSinglePage);
