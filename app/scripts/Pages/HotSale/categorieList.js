export const list = [
  {
    name: 'Salud y Belleza',
    slug: 'salud-y-belleza-v2',
    img:
      'https://canastarosa.s3.us-east-2.amazonaws.com/media/buenFin/img_categories/belleza.jpg',
    key: 1,
  },
  {
    name: 'Diseño',
    slug: 'arte-y-diseno-v2',
    img:
      'https://canastarosa.s3.us-east-2.amazonaws.com/media/buenFin/img_categories/diseno.jpg',
    key: 2,
  },
  // {
  //   name: 'Fitness',
  //   slug: 'ropa-calzado-y-accesorios-ropa-mujer-ropa-deportiva-v2',
  //   img:
  //     'https://canastarosa.s3.us-east-2.amazonaws.com/media/buenFin/img_categories/fitness.jpg',
  //   key: 3,
  // },

  {
    name: 'Flores y Regalos',
    slug: 'flores-y-arreglos-v2',
    img:
      'https://canastarosa.s3.us-east-2.amazonaws.com/media/buenFin/img_categories/flores.jpg',
    key: 4,
  },
  {
    name: 'Gourmet',
    slug: 'comida-cocina-gourmet-v2',
    img:
      'https://canastarosa.s3.us-east-2.amazonaws.com/media/buenFin/img_categories/gourmet.jpg',
    key: 5,
  },

  {
    name: 'Juegos y Entretenimiento',
    slug: 'juegos-y-entretenimiento-v2',
    img:
      'https://canastarosa.s3.us-east-2.amazonaws.com/media/buenFin/img_categories/infantil.jpg',
    key: 6,
  },

  {
    name: 'Relojes y Joyería',
    slug: 'relojes-y-joyeria-v2',
    img:
      'https://canastarosa.s3.us-east-2.amazonaws.com/media/buenFin/img_categories/joyeria.jpg',
    key: 7,
  },
  {
    name: 'Mascotas',
    slug: 'mascotas-v2',
    img:
      'https://canastarosa.s3.us-east-2.amazonaws.com/media/buenFin/img_categories/mascotas.jpg',
    key: 8,
  },
  {
    name: 'Postres',
    slug: 'comida-postres-y-reposteria-v2',
    img:
      'https://canastarosa.s3.us-east-2.amazonaws.com/media/buenFin/img_categories/postres.jpg',
    key: 9,
  },
  {
    name: 'Ropa y Calzado',
    slug: 'ropa-calzado-y-accesorios-v2',
    img:
      'https://canastarosa.s3.us-east-2.amazonaws.com/media/buenFin/img_categories/ropa-y-calzado.jpg',
    key: 10,
  },
  {
    name: 'Comida Saludable',
    slug: 'cat-temp-v2',
    img:
      'https://canastarosa.s3.us-east-2.amazonaws.com/media/buenFin/img_categories/saludable.jpg',
    key: 11,
  },
  {
    name: 'Casa y Decoración',
    slug: 'casa-y-decoracion-v2',
    img:
      'https://canastarosa.s3.us-east-2.amazonaws.com/media/buenFin/img_categories/casa-decoracion.png',
    key: 12,
  },
  // {
  //   name: 'Bebidas',
  //   slug: 'comida-bebidas-2',
  //   img: require('./assets/categoryImg/Bebidas.png'),
  //   key: 4,
  // },
];
