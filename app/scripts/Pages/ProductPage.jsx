import React, { Fragment } from 'react';
import Calendar from 'react-calendar';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { Field, reduxForm } from 'redux-form';
import { bindActionCreators } from 'redux';
import Moment from 'dayjs';

import { trackWithGTM, trackWithFacebookPixel } from '../Utils/trackingUtils';
import '../../styles/_productPage.scss';
import '../../styles/_errorPage.scss';
import config from '../../config';
import PageHead from '../Utils/PageHead';
import Rating from '../components/Rating/Rating';
import ReviewsList from '../components/Store/ReviewsList';
import { IconPreloader } from '../Utils/Preloaders';
import { ActionButton } from '../Utils/ActionButton';
import QuantityButton from '../Utils/QuantityButton';
import CategoriesPopular from '../Utils/CategoriesPopular';
import { ResponsiveImageFromURL } from '../Utils/ImageComponents';
import { formatNumberToPrice } from '../Utils/normalizePrice';
import { getProductDetail, getStoreDetail, getUserProfile } from '../Reducers';
import { SimilarItem } from '../components/Product/SimilarItem';
import ProductGallery from '../components/Product/ProductGallery';
import { SiderbarItem } from '../components/Product/SiderbarItem';
import { shareOnFacebook, shareOnTwitter } from '../Utils/socialMediaUtils';
import {
  SHIPPING_FEE,
  PRODUCT_TYPES,
  ORDERS_EMAIL,
  SHIPPING_METHODS,
} from '../Constants/config.constants';
import {
  storeActions,
  modalBoxActions,
  shoppingCartActions,
  userActions,
  analyticsActions,
} from '../Actions';
import ProductNotAvailableOnline from '../Utils/modalbox/ProductNotAvailableOnline';
import Assistant from '../Utils/Assistant';
import SeasonBanner from '../Utils/SeasonBanner';
import {
  formatDate,
  formatDateYYMMDD,
  addHourPeriodSuffix,
  isValidWorkingDay,
} from '../Utils/dateUtils';
import image302 from './../../images/illustration/403.svg';
import { symbol } from 'prop-types';
import { SHOPPING_CART_FORM_CONFIG } from '../Utils/shoppingCart/shoppingCartFormConfig';
import AwardsBanner from '../components/Awards/AwardsBanner';
import ModalRating from '../Utils/ModalRating';
import HoraRosa from '../components/HoraRosa/HoraRosa.js';
const defaultVariationIndex = 0;
const MAX_ITEMS_PER_PAGE = 5;
const MAX_STOCK_QUANTITY = 50;

let breadcrumbsPath;
let dataLayerInit = false;
let dataLayerContent = '';

export class ProductPage extends React.Component {
  constructor(props) {
    super(props);
    const {
      product: { physical_properties },
      staticContext,
    } = props;
    this.currentPage = 1;
    this.storeSlug = this.props.match.params.store;
    this.productSlug = this.props.match.params.slug;
    this.setCode = process.env.CLIENT
      ? window.INIT_NODE.context.status
      : staticContext.status;
    this.selectedQuantity = 1;
    (this.facebookShare = ''),
      // Define initial state.
      (this.state = {
        productVariations: [],
        attributesTypesList: [],
        selectedProduct: {},
        selectedProductMap: [],
        // selectedQuantity: 1,
        shipping_date:
          physical_properties && physical_properties.minimum_shipping_date,
        shipping_schedule: physical_properties
          ? physical_properties.shipping_schedules[0]
          : null,
        shouldRenderCalendar: false,
        status: this.setCode,
        ...(this.props.product.loaded ? this.setupProduct(this.props.product) : {}),
      });
  }

  componentWillMount() {
    // Empty redux store for this product
    // to prevent mixing data.
    this.props.resetProductDetail();
    // Setup loaded product
    if (this.props.product.loaded) {
      this.setupProduct(this.props.product);
    }
  }

  componentDidMount() {
    // Update details when shipping address change.
    globalThis.shippingAddressObserver.subscribe(this.fetchDetails);

    // Fetch Product & Store data
    this.fetchDetails();

    //
    this.facebookShare = `https://canastarosa.com${this.props.match.url}`;

    //facebook_share;
    this.restoreStatusCode();
  }

  clearLayerText(text) {
    return text.replace(/\"/g, '');
  }

  componentWillReceiveProps(nextProps) {
    // If product details have finished loading and
    // the product is not the same as the one already loaded, setup the product again

    if (!nextProps.product.loading && !dataLayerInit) {
      console.log('CARGA PRODUCTO', nextProps.product);
      console.log('CARGA PRODUCTO', nextProps);

      this.dispatchDataLayer(nextProps.product);
      dataLayerInit = true;
    }

    if (
      nextProps.product.loaded &&
      this.props.product.slug !== nextProps.product.slug
    ) {
      this.updateSelectedProduct(this.setupProduct(nextProps.product));
      const p = nextProps.product;
      //FOR GTM tracking
      const productModel = {
        id: p.id.toString(),
        name: p.name,
        variant: p.physical_properties.size.display_name,
        brand: p.store.name,
        price: p.price,
        // category: this.props.product.category.length ?
        // this.props.product.category[0] : '',
      };
      this.facebookShare = `https://canastarosa.com${this.props.match.url}`;
      //this.forceUpdate();

      trackWithGTM('eec.detail', {
        list: 'Product Detail',
        products: [productModel],
      });

      trackWithGTM('ProductPage_Criteo', productModel.id);
      //Send analytics to endpoint

      this.props.trackProductImpression(p.slug, this.props.location.search);

      this.state.status = 200;
      //this.forceUpdate();
    }

    // If product url has changed, fetch new product details
    if (this.props.match.params.slug !== nextProps.match.params.slug) {
      this.props.fetchProductDetail(
        nextProps.match.params.store,
        nextProps.match.params.slug,
      );
    }
  }

  checkVariant(p, variant) {
    let result = null;
    if (variant) {
      p?.attributes.map((variantItem) => {
        if (variantItem.attribute_uuid.uuid == variant) {
          result = variantItem.value;
        }
      });
      return result;
    } else {
      return result;
    }
  }

  dispatchDataLayerAdd(p, product) {
    try {
      console.log("dispatchDataLayerAdd-->0")
      if (process.env.CLIENT) {
        console.log("dispatchDataLayerAdd-->1")
        let prevProduct = '';
        if (globalThis.googleAnalytics?.currentProductClick?.dataLayerContent) {
          //console.log("HAY DATALAYER PREVIO");
          prevProduct =
            globalThis.googleAnalytics?.currentProductClick?.dataLayerContent;
          prevProduct.quantity = product?.quantity;
          prevProduct.variant = this.checkVariant(p, product?.attribute);
        } else {
          prevProduct = {
            name: p?.name,
            id: p?.id,
            price: p?.price,
            brand: p?.store?.name,
            category: dataLayerContent.category,
            variant: this.checkVariant(p, product?.attribute),
            quantity: product?.quantity,
          };
        }
          window.dataLayer.push({
            event: 'AddToCart',
            eventCategory: 'Ecommerce',
            eventAction: 'Añadir al carrito',
            eventLabel: p?.name,
            // Variable con nombre visible del producto
            ecommerce: {
              currencyCode: 'MXN',
              add: {
                products: [prevProduct],
              },
            },
          });
       
      }
    } catch (e) {
      console.log("dispatchDataLayerAdd--> error")
      console.log(e);
    }
  }

  dispatchDataLayer(item) {
   
    try {
      let eventLabel = 'null';

      if (globalThis.googleAnalytics?.currentProductClick?.dataLayerContent) {
        //console.log("HAY DATALAYER PREVIO");
        dataLayerContent =
          globalThis.googleAnalytics?.currentProductClick?.dataLayerContent;
        eventLabel = `${globalThis.googleAnalytics?.currentProductClick?.dataLayerContent.category}`;
      } else {
        //console.log("NO HAY DATALAYER PREVIO");
        dataLayerContent = `{
          "name": "${this.clearLayerText(item?.name)}",
          "id": "${item?.id}",
          "price": "${item?.price}",
          "brand": "${this.clearLayerText(item?.store?.name)}",
          "category": null,
          "variant": null
         }`;
        dataLayerContent = JSON.parse(dataLayerContent);
        eventLabel = null;
        // globalThis.datalayer = dataLayerContent;
      }

      if (process.env.CLIENT) {
        let tmpDataLayerBreadcrumbs = dataLayerContent?.category
          ? dataLayerContent.category
          : null;
        let tmpDataLayerContent = dataLayerContent?.name ? dataLayerContent : null;

        globalThis.googleAnalytics.ProductDetail(
          tmpDataLayerContent,
          tmpDataLayerBreadcrumbs,
        );
      }
    } catch (e) {
      console.log(e.message);
      console.log(e);
    }
  }

  componentWillUnmount() {
    // shippingAddressObserver
    dataLayerInit = false;
    globalThis.shippingAddressObserver.unsubscribe(this.fetchDetails);
  }

  fetchDetails = () => {
    this.props.fetchStore(this.storeSlug).catch((e) => console.log(e));
    this.props.fetchProducts(this.storeSlug).catch((e) => console.log(e));
    this.props
      .fetchProductDetail(this.storeSlug, this.productSlug)
      .then((res) => {
        this.getReviews(this.storeSlug, this.productSlug, this.currentPage);
        this.props.getPendingProductReviews(this.storeSlug, this.productSlug);
      })
      .catch((e) => console.log(e));
  };

  /**
   * onAddProductToCart()
   * Adds a product to the shopping cart into redux store
   * and sends it to backend through API
   */
  onAddProductToCart = () => {
    const product = {
      note: '',
      product: this.props.product.slug,
      // quantity: parseInt(this.state.selectedQuantity, 10),
      quantity: parseInt(this.selectedQuantity, 10),
      attribute: this.state.selectedProduct.uuid
        ? this.state.selectedProduct.uuid.uuid
        : null,
      physical_properties: {
        shipping_schedule: this.state.shipping_schedule,
        shipping_date: formatDateYYMMDD(this.state.shipping_date),
      },
    };

    //
    const { product: p } = this.props;
    const productModel = {
      id: p.id.toString(),
      name: p.name,
      variant: p.physical_properties.size.display_name,
      brand: p.store.name,
      quantity: parseInt(this.selectedQuantity, 10),
      dimension1: p.store.slug,
      price: this.state.selectedProduct.price,
      // category: this.props.product.category.length ?
      // this.props.product.category[0] : '',
    };

    trackWithGTM('eec.add', {
      products: [productModel],
    });

    // Send Event to Facebook pixel.
    trackWithFacebookPixel('track', 'AddToCart', {
      ...productModel,
      value: p.price,
      currency: 'MXN',
      content_type: 'product',
      content_ids: [p.id],
    });

    console.log("AÑADIR A CARRITO");
    this.dispatchDataLayerAdd(p, product);

    // Add Product to ShoppingCart
    return this.props.updateCartProduct(product);
  };

  /**
   * onAttributeChange()
   * Attribute change handler
   * @param {object} e | Form Event
   */
  onAttributeChange = (e) => {
    const { attributesTypesList, productVariations } = this.state;
    const selectedProductMap = attributesTypesList.map(
      (item) => this[item.ref].selectedIndex,
    );

    // Get matching product & update component
    const selectedProduct = this.selectProduct(
      selectedProductMap,
      productVariations,
      this.props.product,
    );
    this.updateSelectedProduct({
      selectedProduct,
      selectedProductMap,
    });
  };

  /**
   * onQuantityChange()
   * Quantity change handler
   * @param {object} e | Form Event
   */
  onQuantityChange = (q) => {
    this.selectedQuantity = q;
  };

  /**
   * onCalendarDateChange()
   * @param {string} selectedDate | Selected Date from calendar
   */
  onCalendarDateChange = (selectedDate) => {
    const { product } = this.props;
    const { shipping_date } = this.state;

    // Update selected delivery date.
    this.updateSelectedProduct(
      {
        shipping_date: new Date(selectedDate),
        shouldRenderCalendar: false,
      },
      () => {
        this.updateSelectedProduct({
          shipping_schedule: product.physical_properties.shipping_schedules.find(
            (schedule) =>
              this.isValidShippingScheduleTime(
                schedule.schedules.limit_to_order,
                shipping_date,
              ),
          ).value,
        });
      },
    );
  };

  /**
   * onDeliveryScheduleChange()
   * @param {int} deliveryShecule | Delivery schedule ID
   */
  onDeliveryScheduleChange = (deliveryShecule) => {
    // Change button styles
    const shippingButtons = document.querySelectorAll('.shipping-schedule-button');
    [].slice.call(shippingButtons).forEach((e) => {
      const parent = e.parentNode;
      if (parseInt(e.dataset.shippingId, 10) === deliveryShecule) {
        parent.classList.toggle('active', true);
      } else {
        parent.classList.toggle('active', false);
      }
    });

    // Update selected product properties
    this.updateSelectedProduct({
      shipping_schedule: deliveryShecule,
    });
  };

  getReviews = (store, product, page) => {
    this.props.getProductReviews(
      {
        store_slug: store,
        product_slug: product,
      },
      `?page=${page}&page_size=${MAX_ITEMS_PER_PAGE}`,
    );

    this.currentPage = page;
  };

  /**
   * setupProduct()
   * Setup a new project view.
   * Parse attributes into selectboxes and select a default product.
   * @param {object} product | Product object
   */
  setupProduct(product) {
    const productVariations = product.attributes;
    const attributesTypesList = this.parseAttributeTypesIntoSelectOptions(
      productVariations,
    );
    const selectedProductMap = attributesTypesList.map(
      (item) => defaultVariationIndex,
    );

    // Select default product
    // If product has attributes, get the first choice within product variations
    let selectedProduct = {
      uuid: null,
    };
    if (productVariations.length) {
      selectedProduct = this.selectProduct(
        selectedProductMap,
        productVariations,
        product,
      );

      // ...else get the default listing properties
    } else {
      selectedProduct.slug = product.slug;
      selectedProduct.price = product.price;
      selectedProduct.quantity = product.quantity;
      selectedProduct.price_without_discount = product.price_without_discount;
    }

    // Update Component State
    let shippingSchedule = product.physical_properties.shipping_schedules.find(
      (schedule) =>
        this.isValidShippingScheduleTime(
          schedule.schedules.limit_to_order,
          product.physical_properties.minimum_shipping_date,
        ),
    );
    shippingSchedule = shippingSchedule ? shippingSchedule.value : null;

    return {
      selectedProduct,
      selectedProductMap,
      productVariations,
      attributesTypesList,
      shipping_date: product.physical_properties.minimum_shipping_date,
      shipping_schedule: shippingSchedule,
    };
  }

  /**
   * updateSelectedProduct()
   * Updates the component state with an updated version of the selected product
   * @param {object} targetProduct : Product object
   */
  updateSelectedProduct = (targetProduct, callback = () => {}) => {
    // Update selected Product Properties
    this.setState(
      {
        ...targetProduct,
      },
      callback,
    );
  };

  /**
   * parseAttributeTypesIntoSelectOptions()
   * This function gets a list or product attributes and parses it
   * to return a list of selectboxes with options based on product attributes.
   * @param {array} productAttributes : List of product attributes
   * @param {array} listOfAttributes : Accumulative (recursive) list of select/option elements
   * @return {array} Returns an list of selectboxes with options, based on product attributes
   */
  parseAttributeTypesIntoSelectOptions = (
    productAttributes = [],
    listOfAttributes = [],
  ) => {
    if (!productAttributes.length) return [];

    // Create Options for the 'Product Variations Selectbox'
    const childAttributes = [];
    const optionsList = productAttributes.reduce(
      (optionsAccumulator, currentOption, index) => {
        // Avoid Duplicates.
        if (!optionsAccumulator.values.includes(currentOption.value)) {
          // Add new atribute to the final list.
          optionsAccumulator.options.push(
            <option key={index} value={index}>
              {currentOption.value}
            </option>,
          );
          optionsAccumulator.values.push(currentOption.value);

          // If this attribute has children, add them to a list of
          // subchild attributes to use them whithin this function recursively.
          if (currentOption.hasOwnProperty('children')) {
            childAttributes.push(...currentOption.children);
          }
        }

        // Return options.
        return optionsAccumulator;
      },
      { options: [], values: [] },
    );

    // Construct Attributes Object
    const attributeType = productAttributes[0].attribute_type;
    const selectboxName = `${attributeType}_sbx`;
    const attribute = {
      type: attributeType,
      selectbox: (
        <select
          onChange={this.onAttributeChange}
          name={selectboxName}
          ref={(input) => {
            this[selectboxName] = input;
          }}
        >
          {optionsList.options}
        </select>
      ),
      ref: selectboxName,
    };
    // Push Object to attributes list
    listOfAttributes.push(attribute);

    // Iterate through children attributes
    if (childAttributes.length) {
      this.parseAttributeTypesIntoSelectOptions(childAttributes, listOfAttributes);
    }

    // Return List of Attributes
    return listOfAttributes;
  };

  /**
   * selectProduct()
   * @param {array} map : A list of product attributes/variations combination (by position index)
   * @param {array} variations : A list of product attributes/variations
   * @param {object} product : A Product
   * @return {object} Returns an object with the selected product attributes and information
   */
  selectProduct(map, variations, product) {
    const selectedProduct = { uuid: null };

    // Iterate through all product variables tree
    // with the 'map' of selected values
    map.reduce(
      (variationAccumulator, currentVariation) => {
        //------------------------------------------------------
        // TODO: This is bad code. Just to fix a bug.
        if (!variationAccumulator.variations) return variationAccumulator;
        //------------------------------------------------------

        // Get product variations.
        const element = variationAccumulator.variations[currentVariation];
        if (element.attribute_stock !== null) {
          selectedProduct.quantity = element.attribute_stock.quantity;
          selectedProduct.price = element.attribute_stock.price;
          selectedProduct.price_without_discount =
            element.attribute_stock.price_without_discount;
        }
        if (element.hasOwnProperty('attribute_uuid')) {
          selectedProduct.uuid = element.attribute_uuid;
        }
        //
        return { variations: element.children };
      },
      { variations },
    );

    // If selected product doesn´t contain price or quantity
    // Take the values from the main product
    if (!selectedProduct.price && !selectedProduct.quantity) {
      selectedProduct.price = product.price;
      selectedProduct.quantity = product.quantity;
      selectedProduct.price_without_discount = product.price_without_discount;
    }

    // If selected product is 'made on demand'
    // it must use the 'main product quantity'.
    if (product.productMadeOnDemand) {
      selectedProduct.quantity = product.quantity;
    }

    // Return Information to populate Selected Product
    return selectedProduct;
  }

  /**
   * isValidShippingScheduleTime()
   * Si la fecha de entrega es 'same day' o el producto tiene 'stock'
   * verificamos que el horario que enviamos como parámetro sea un horario válido.
   * @param {string} time | Horario en formato 'hh:mm:ss'
   * @returns bool
   */
  isValidShippingScheduleTime = (schedule, targetDate) => {
    if (!schedule || !targetDate) return false;
    const NOW = new Date();

    // Si la fecha de entrega es para hoy,
    // verificamos que el horario de entrega no haya pasado aún.
    if (targetDate.toDateString() === NOW.toDateString()) {
      // Convertimos el horario (string 'hh:mm:ss') en un objeto Date
      // para poder compararlo con otras fechas.
      const limitTimeParts = schedule.split(':');
      const scheduleLimitTime = new Date(NOW);
      scheduleLimitTime.setHours(limitTimeParts[0]);
      scheduleLimitTime.setMinutes(limitTimeParts[1]);
      scheduleLimitTime.setSeconds(limitTimeParts[2]);

      // ¿Ya pasó el horario que estamos buscando?
      return scheduleLimitTime > NOW;
    }
    return true;
  };

  /**
   * renderTimeTable()
   * Renders a list of weekdays and schedules
   * @returns {array}
   */
  renderTimeTable = (schedules) =>
    schedules.map((schedule) => (
      <li className="schedules-list__item" key={schedule.value}>
        <div className="weekday">{schedule.name}</div>
        <div className="schedule">
          {`${addHourPeriodSuffix(schedule.open.value)} 
                - 
                ${addHourPeriodSuffix(schedule.close.value)}`}
        </div>
      </li>
    ));

  /**
   * renderDeliverySchedules()
   * Render list of available delivery schedules.
   */
  renderDeliverySchedules() {
    const { physical_properties } = this.props.product;
    const { shipping_date, shipping_schedule } = this.state;
    return physical_properties.shipping_schedules.map((schedule) => {
      // verificamos que el horario de entrega no haya pasado aún.
      const isDisabled = !this.isValidShippingScheduleTime(
        schedule.schedules.limit_to_order,
        shipping_date,
      );

      // Return markup
      return (
        <li
          key={schedule.id}
          className={`
                        button-option 
                        ${isDisabled ? 'button-option--disabled' : ''}
                        ${
                          shipping_schedule === schedule.id && !isDisabled
                            ? 'active'
                            : ''
                        }
                    `}
        >
          <button
            type="button"
            disabled={isDisabled}
            data-shipping-id={schedule.id}
            className={`shipping-schedule-button button-tag ${schedule.className}`}
            onClick={(e) => this.onDeliveryScheduleChange(schedule.id)}
          >
            {schedule.schedules.delivery}
          </button>
        </li>
      );
    });
  }

  sanitizeUrl(slug) {
    const extractSlug = slug.split('-');
    let newSlug = '';
    let num = 0;
    for (let obj of extractSlug) {
      if (num < extractSlug.length - 1) {
        newSlug += `${obj} `;
      }
      num++;
    }
    return newSlug;
  }

  restoreStatusCode() {
    if (process.env.CLIENT) {
      window.INIT_NODE.context.status = 200;
    }
  }

  /*
   * openRatingWindow()
   * Opens modalbox to rating product
   * @param {object} productPurchase | Product Purchase
   * @param {int} value | Initial Rating Value to display on Rating component
   */
  openRatingWindow = (productPurchase, value = 0) => {
    this.props.openModalBox(() => (
      <ModalRating
        productPurchase={productPurchase}
        productScore={value}
        updateReviews={this.updateReviews}
      />
    ));
  };

  updateReviews = () => {
    this.getReviews(this.storeSlug, this.productSlug, this.currentPage);
    this.props.getPendingProductReviews(this.storeSlug, this.productSlug);
  };

  /**
   * createBreadcrumbsTree()
   * Returns an array with the marching categories path.
   * @param {array} categories | Array of nested categories to search from.
   * @param {string} targetCategorySlug | Category String to search.
   * @param {bool} found | Indicates if category has been already found.
   * @param {array} parentPath | Array with the parent categories.
   * @returns {array} An array of categories (Model example: [{name, slug}, ...]).
   */
  createBreadcrumbsTree = (
    categories,
    targetCategorySlug,
    found = false,
    parentPath = [],
  ) => {
    const tester = categories.reduce(
      (a, b) => {
        let result = a;

        // If category is not found, proceed to recursive loop.
        if (!result.found) {
          // If we've found the matching category, return current result
          if (b.slug === targetCategorySlug) {
            result = {
              path: [...result.path, { name: b.name, slug: b.slug }],
              found: true,
            };

            // else, search whithin current category children.
          } else if (b.children.length) {
            const innerCategoryPath = this.createBreadcrumbsTree(
              b.children,
              targetCategorySlug,
              false,
              [...result.path, { name: b.name, slug: b.slug }],
            );

            if (innerCategoryPath.found) {
              result = innerCategoryPath;
            }
          }
        }

        // Return current path.
        return result;
      },
      { found, path: parentPath },
    );

    return tester;
  };

  render() {
    // Product not Found
    if (this.state.status === 404) {
      return (
        <div className="ProductError">
          <div className="img_container">
            <img src={image302} />
          </div>
          <h3 className="title--main">
            [2] No logramos encontrar el artículo:
            <br />
            <strong className="ProductErrorName">
              {this.sanitizeUrl(this.productSlug)}
            </strong>
          </h3>

          <p>Posiblemente se movió a otra sección o se ha eliminado.</p>
          <p>
            <a
              href={
                '/search/products/' + this.sanitizeUrl(this.props.match.params.slug)
              }
            >
              Ver otros resultados con este producto
            </a>
          </p>
          <div className="ProductSpace" />
          <section
            itemScope=""
            itemType="http://schema.org/Product"
            className="productItem_container"
          >
            <SimilarItem
              productsList={this.props.storeProducts.slice(0, 4)}
              store={this.props.store}
            />
          </section>
          <CategoriesPopular />
        </div>
      );
    }

    // Render preloader icon
    if (
      this.props.store.loading ||
      !this.props.storeProducts.length ||
      this.props.product.loading
    ) {
      return <IconPreloader />;
    }
    if (
      Object.keys(this.state.selectedProduct).length === 0 &&
      this.state.selectedProduct.constructor === Object
    ) {
      return <IconPreloader />;
    }

    // Extract properties and state
    const {
      price,
      price_without_discount,
      discount,
      slug,
      name,
      description,
      photos = [],
      category,
      physical_properties,
      work_schedules,
      productMadeOnDemand,
      reviews: { count: reviewsCount, rating },
    } = this.props.product;

    const {
      store,
      store: {
        user: { first_name: fullName = '', profile_photo },
        is_on_vacation,
      },
      reviews,
      pendingReviews,
      userReviews: {
        pending: { results: currentUserReviews },
      },
    } = this.props;

    const {
      attributesTypesList,
      selectedProduct,
      shouldRenderCalendar,
    } = this.state;

    const currenPage = this.currentPage;
    //Does the current user has a pending review for this product?
    const hasCurrentUserPendingReview =
      currentUserReviews.find((r) => r.product.slug === this.props.product.slug) ||
      false;

    const currentUserPendingReview = currentUserReviews.find(
      (us) => us.product.slug === this.props.product.slug,
    );

    // const hasCurrentUserReview = hasCurrentUserPurchasedProduct
    //     ? hasCurrentUserPurchasedProduct.review : false;

    // Page Location
    const pageLocation = `${config.frontend_host}${this.props.location.pathname}`;

    // Shipping Date format calculations
    let shipping_date = this.state.shipping_date;
    let minimum_shipping_date = this.props.product.physical_properties
      .minimum_shipping_date;
    let maximum_shipping_date = this.props.product.physical_properties
      .maximum_shipping_date;

    const hasExpressShipping = this.props?.product?.physical_properties?.shipping?.reduce(
      (found, availableShippingMethod) => {
        if (!found) {
          return (
            SHIPPING_METHODS.EXPRESS.slug === availableShippingMethod ||
            SHIPPING_METHODS.EXPRESS_BIKE.slug === availableShippingMethod ||
            SHIPPING_METHODS.EXPRESS_CAR.slug === availableShippingMethod
          );
        }
        return found;
      },
      false,
    );
    const hasNationalShipping = this.props?.product?.physical_properties?.shipping?.find(
      (item) => item === 'standard',
    );
    // Product Description
    const productDescription = (
      <div className="description_container">
        <div className="details">
          <div className="details__category">
            {/*
            <div className="details__like">
              <FacebookProvider appId="130383784310446">
                <Like href={this.facebookShare} colorScheme="dark" large showFaces />
              </FacebookProvider>
            </div>
            */}
            <h4 className="module-title">
              <a className="dropdown">Detalles del producto</a>
            </h4>
          </div>
          <div className="details__txt">
            <p>{description}</p>
          </div>
        </div>

        <div className="author">
          <div className="container">
            <div className="container__img">
              <ResponsiveImageFromURL src={profile_photo.small} alt={fullName} />
            </div>
            <div className="container__info">
              <span>Creado por:</span> <br />
              <span className="name">@{fullName}</span>
            </div>
          </div>
        </div>

        <div className="details reviews">
          {hasCurrentUserPendingReview && reviews.results.length < 1 && (
            <div className="review_wrapper message-container">
              <h4 className="title">¡Sé el primero en escribir una reseña!</h4>
              <p className="message ">
                ¿Compraste este producto? ¡Haznos saber lo que piensas!
                <br />
                Con tus comentarios ayudas a otros compradores a conseguir los
                mejores artículos y a las tiendas a mejorar sus productos.
              </p>
              <button
                className="c2a_square"
                onClick={() => this.openRatingWindow(currentUserPendingReview)}
              >
                Escribir una reseña
              </button>
            </div>
          )}

          {(reviews.results.length > 0 || pendingReviews.results.length > 0) && (
            <ReviewsList
              showProductDetails={false}
              //showAddButton={hasCurrentUserPendingReview}
              purchase={{}}
              reviews={reviews.results}
              reviewsCount={reviewsCount}
              pendingReviews={pendingReviews.results}
              rating={rating}
              pageLocation={'productPage'}
            />
          )}

          {
            // Show button only if next page exists
            reviews.next !== null && (
              <div className="review_btn">
                <button
                  type="button"
                  className="button-square--gray-deg"
                  onClick={() =>
                    this.getReviews(this.storeSlug, this.productSlug, currenPage + 1)
                  }
                >
                  Ver más Reseñas
                </button>
              </div>
            )
          }
        </div>
      </div>
    );
    // Product Categories and Clasification
    const isProductEdible = category.find((cat) => cat.slug === 'delicioso');

    // Quantity calculations
    let quantityProduct = [];
    if (selectedProduct.quantity) {
      const maximumQuantity = Math.min(selectedProduct.quantity, MAX_STOCK_QUANTITY);
      for (let i = 1; i < maximumQuantity + 1; i++) {
        quantityProduct.push(
          <option key={i} value={i}>
            {i}
          </option>,
        );
      }
    } else {
      quantityProduct = <option value="No Disponible">No Disponible</option>;
    }

    // Purchase / Add to Cart Button
    let purchaseButton;

    if (
      (selectedProduct.quantity || productMadeOnDemand) &&
      physical_properties.shipping.length !== 0
    ) {
      purchaseButton = (
        <ActionButton
          style={{ width: '100%' }}
          icon="check"
          className="c2a_square addToCart"
          onClick={this.onAddProductToCart}
          disabled={selectedProduct.quantity || productMadeOnDemand ? false : true}
        >
          Agregar a Carrito
        </ActionButton>
      );
    } else {
      purchaseButton = (
        <button
          type="button"
          className="c2a_square disabled "
          style={{ width: '100%' }}
        >
          {physical_properties.shipping.length === 0
            ? 'No Disponible'
            : 'Producto agotado'}
        </button>
      );
    }

    // Return component markup
    return (
      <Fragment>
        <HoraRosa location={this.props.location} little={true}></HoraRosa>
        <section
          itemScope
          itemType="http://schema.org/Product"
          className="productItem_container"
        >
          <PageHead
            attributes={{
              title: `${name.substring(0, 40)}... | Canasta Rosa`, //For SEO
              meta: [
                {
                  name: 'description',
                  content: description.substring(0, 300), //For SEO
                },
                {
                  property: 'og:url',
                  content: `${pageLocation}`,
                },
                {
                  property: 'og:title',
                  content: `${name} por ${store.name} - Market | Canasta Rosa`,
                },
                {
                  property: 'og:image:width',
                  content: '500',
                },
                {
                  property: 'og:image:height',
                  content: '200',
                },
                {
                  property: 'og:description',
                  content: description,
                },
                photos.length
                  ? {
                      property: 'og:image',
                      content: photos[0].photo.big,
                    }
                  : {},
                {
                  property: 'product:brand',
                  content: store.name,
                },
                {
                  property: 'product:availability',
                  content: 'in stock',
                },
                {
                  property: 'product:condition',
                  content: 'new',
                },
                {
                  property: 'product:price:amount',
                  content: price,
                },
                {
                  property: 'product:price:currency',
                  content: 'MXN',
                },
                {
                  property: 'product:retailer_item_id',
                  content: slug,
                },
                {
                  itemprop: 'name',
                  content: name,
                },
                {
                  itemprop: 'brand',
                  content: store.name,
                },
                {
                  itemprop: 'description',
                  content: description,
                },
                {
                  itemprop: 'productID',
                  content: slug,
                },
                {
                  itemprop: 'url',
                  content: pageLocation,
                },
                photos.length
                  ? {
                      itemprop: 'image',
                      content: photos[0].photo.big,
                    }
                  : {},
              ],
              tags: [store.name],
              jsonLD: {
                '@context': 'http://schema.org',
                '@type': 'Product',
                name,
                brand: store.name,
                description,
                productID: slug,
                url: pageLocation,
                offers: {
                  price,
                  priceCurrency: 'MXN',
                  availability: 'http://schema.org/InStock',
                },
                image: photos.length ? photos[0].photo.big : '',
                itemCondition: 'NewCondition',
              },
            }}
          />

          <div
            itemProp="brand"
            itemScope
            itemType="http://schema.org/Brand"
            className="productItem_container__store-details wrapper--center"
          >
            <Link
              itemProp="url"
              to={`/stores/${this.props.store.slug}`}
              className="store_brand"
            >
              <ResponsiveImageFromURL
                src={this.props.store.photo.small}
                alt={this.props.store.name}
              />
            </Link>
            <Link
              itemProp="url"
              to={`/stores/${this.props.store.slug}`}
              className="store_name"
            >
              {this.props.store.name}
              {/* <p>@{this.props.store.slug}</p> */}
            </Link>
          </div>

          <div className="productItem_container__product-details wrapper--center">
            <div className="col-l">
              <div className="photos_container">
                <div className="socialMedia__share">
                  {/* <afterAllsName="wishlist">
                                        <span>Agregar a Colecci&oacute;n</span>
                                </a>*/}
                  <div className="socialMedia__share--buttons">
                    <a
                      href="#"
                      onClick={(e) => {
                        e.preventDefault();
                        shareOnFacebook(pageLocation);
                      }}
                      className="icon icon--facebook"
                    >
                      <span>Compartir</span>
                    </a>
                    <a
                      href="#"
                      onClick={(e) => {
                        e.preventDefault();
                        shareOnTwitter(pageLocation, name);
                      }}
                      className="icon icon--twitter"
                    >
                      <span>Tweet</span>
                    </a>
                    <a
                      href={`https://api.whatsapp.com/send?text=${pageLocation}`}
                      //data-action="share/whatsapp/share"
                      className="icon icon--whatsapp"
                    >
                      <span>Whatsapp</span>
                    </a>
                  </div>
                </div>

                <div
                  className={`${
                    !productMadeOnDemand && !selectedProduct.quantity
                      ? 'soldout'
                      : ''
                  }`}
                >
                  <ProductGallery gallery={photos.slice(0, 6)} />
                </div>

                <div itemProp="description">{productDescription}</div>
              </div>
            </div>

            <div className="col-s">
              <div className="detail-info">
                <h1 className="title" itemProp="name">
                  {name}
                </h1>

                <div className="rating">
                  {/*<span className="score">{`${rating.toFixed(1)} `}</span>*/}
                  {/* <Rating rating={rating} />
                <span>({reviewsCount} calificaciones)</span> */}
                </div>

                {/* <p>@{this.props.store.slug}</p> */}
                <React.Fragment>
                  {hasExpressShipping && (
                    <div className="detail-info--shippingNote shipping-schedules">
                      <p className="title name icon_shipping_note">Envío express</p>
                    </div>
                  )}
                </React.Fragment>
                <React.Fragment>
                  {physical_properties.shipping.length === 0 && (
                    <div className="detail-info--shippingNote shipping-schedules">
                      <p className="title warning--title name icon_shipping_warning">
                        Este producto no está disponible para su dirección actual.
                      </p>
                    </div>
                  )}
                </React.Fragment>

                <React.Fragment>
                  {!hasExpressShipping && hasNationalShipping && (
                    <div>
                      <div className="detail-info--shippingNote cdmx shipping-schedules">
                        <p className="title name icon_shipping_cdmx">
                          Envío nacional
                        </p>
                      </div>
                    </div>
                  )}
                </React.Fragment>
                <div
                  itemProp="offers"
                  itemScope
                  itemType="http://schema.org/Offer"
                  className="detail-info--price"
                >
                  <div>
                    {parseFloat(discount, 10) > 0 ? (
                      <React.Fragment>
                        <div
                          className="cr__text--subtitle3"
                          style={{ color: '#D87041' }}
                        >
                          <span>Antes</span>{' '}
                          <span className="line-through">
                            {selectedProduct.price_without_discount}
                            MX
                          </span>
                        </div>

                        <h3 itemProp="price " className="cr__text--title">
                          {/* DISCOUNT TAG */}
                          {parseFloat(discount, 10) > 0 && (
                            <span
                              style={{
                                color: '#1eb592',
                              }}
                            >
                              -{Math.round(discount)}%{' '}
                            </span>
                          )}
                          {/* /DISCOUNT TAG */}
                          {parseFloat(discount, 10) > 0
                            ? formatNumberToPrice(selectedProduct.price)
                            : formatNumberToPrice(
                                selectedProduct.price_without_discount,
                              )}
                          MX
                          <br />
                        </h3>

                        <p className="cr__text--subtitle3 cr__textColor--colorGray100">
                          IVA incluido
                        </p>
                      </React.Fragment>
                    ) : (
                      <React.Fragment>
                        <h3 itemProp="price">{selectedProduct.price}MX</h3>
                        <p className="cr__text--subtitle3 cr__textColor--colorGray200">
                          IVA incluido
                        </p>
                      </React.Fragment>
                    )}

                    {productMadeOnDemand ? (
                      <p className="type_product">Producto elaborado bajo pedido.</p>
                    ) : selectedProduct.quantity ? (
                      <p className="type_product">Producto en existencia.</p>
                    ) : selectedProduct.quantity ? (
                      <p className="type_product">Producto en existencia.</p>
                    ) : (
                      <p className="type_product">Producto agotado.</p>
                    )}
                  </div>

                  <form className="form">
                    <label htmlFor="quantity">Cantidad</label>
                    <QuantityButton
                      name="quantity"
                      quantity={this.selectedQuantity}
                      onQuantityChange={this.onQuantityChange}
                      maxQuantity={selectedProduct.quantity}
                    />
                  </form>
                </div>
              </div>

              <div className="detail-info--attributes">
                {attributesTypesList &&
                  attributesTypesList.map((item) => (
                    <form className="form" key={item.type}>
                      <div className="title">{item.type}</div>
                      {item.selectbox}
                    </form>
                  ))}
              </div>
              <div className="detail-info--addCar">
                {purchaseButton}
                {/* () => this.props.openModalBox(() => <ProductNotAvailableOnline product={name} store={this.props.store.name} />)  */}
                {/*<img src={require('images/store/ranking.png')} alt="" />*/}
              </div>

              <div className="detail-info__shipping shipping-schedules">
                <div className="delivery_info container">
                  {is_on_vacation ? (
                    <div className="is_holidays">
                      <p>
                        Esta tienda se encuentra de vacaciones.
                        <br />
                        La fecha más próxima de entrega es:
                      </p>
                    </div>
                  ) : null}

                  <div className="schedule_container shipping-schedules">
                    <strong className="name icon_schedule">
                      Recíbelo el:
                      <span>&nbsp;{formatDate(shipping_date)}</span>
                    </strong>
                  </div>
                  <div className="shipping-schedules delivery">
                    <strong>Elige el día y hora de entrega</strong>
                    <div className="delivery_details">
                      <div className="delivery_details--day">
                        <p>Día:</p>
                        <div className="ui-calendar-selector">
                          <div
                            className="ui-calendar-selector__day"
                            onClick={(e) =>
                              this.setState({ shouldRenderCalendar: true })
                            }
                          >
                            {formatDate(shipping_date)}
                          </div>

                          {shouldRenderCalendar && (
                            <Calendar
                              view="month"
                              locale="es"
                              value={shipping_date}
                              className="calendar-component"
                              maxDate={maximum_shipping_date}
                              minDate={minimum_shipping_date}
                              onChange={this.onCalendarDateChange}
                              tileDisabled={({ date }) =>
                                !isValidWorkingDay(
                                  date,
                                  this.props.product,
                                  'PRODUCT_PAGE',
                                )
                              }
                              tileClassName={({ date }) => {
                                const className = 'calendar-component__tile';
                                let tileType = !isValidWorkingDay(
                                  date,
                                  this.props.product,
                                  'PRODUCT_PAGE',
                                )
                                  ? 'calendar-component__tile--disabled '
                                  : '';
                                tileType =
                                  date.getTime() === shipping_date.getTime()
                                    ? `${tileType} calendar-component__tile--active`
                                    : tileType;
                                tileType =
                                  date.getDay() === 6 || date.getDay() === 0
                                    ? `${tileType} calendar-component__tile--weekend`
                                    : tileType;
                                return `${className} ${tileType}`;
                              }}
                            />
                          )}
                        </div>
                      </div>

                      <div className="delivery_details--schedule">
                        <p>Hora:</p>
                        <ul className="buttons_list">
                          {this.renderDeliverySchedules()}
                        </ul>
                      </div>
                    </div>
                    {/* <div className="custom">
                    <h4 className="module-title">Orden personalizada</h4>
                    <p>
                      ¿Necesitas hacer un pedido especial o la fecha de entrega
                      no es la que necesitas?&nbsp;
                      <a className="contact" href={`mailto:${ORDERS_EMAIL}`}>
                        Contáctanos
                      </a>
                    </p>
                  </div> */}
                  </div>
                </div>

                <Assistant />
                {/* <SeasonBanner /> */}

                <div className="container shipping-schedules">
                  {productMadeOnDemand && (
                    <div>
                      <h4 className="module-title">
                        Detalles de elaboración y pedidos
                      </h4>
                      <strong className="name icon_time-prepare">
                        Tiempo de elaboración
                      </strong>
                      <p className="txt">
                        {physical_properties.fabrication_time} días
                      </p>
                    </div>
                  )}

                  <div className="store-schedules">
                    <strong className="name icon_store-schedules">
                      Horarios para realizar pedidos en esta tienda
                    </strong>
                    <ol className="schedules-list">
                      {this.renderTimeTable(work_schedules)}
                    </ol>
                  </div>
                </div>

                {isProductEdible && (
                  <div className="container">
                    <h4 className="module-title">Detalles de envío</h4>
                    <p>
                      Los productos se elaboran frescos y se envían directamente
                      desde la tienda.
                      <br />
                      ¿Tienes un evento especial? Te sugerimos realizar tu pedido con
                      anticipación para poder garantizar su sabor y frescura para tu
                      evento.
                      <br />
                    </p>
                  </div>
                )}
              </div>
              <div>{productDescription}</div>
              <SiderbarItem
                productsList={this.props.storeProducts.slice(0, 4)}
                store={this.props.store}
              />
            </div>
          </div>

          {/* <SimilarItem
          productsList={this.props.storeProducts.slice(0, 4)}
          store={this.props.store}
        /> */}

          <CategoriesPopular />
        </section>
      </Fragment>
    );
  }
}

// Load Data for Server Side Rendering
ProductPage.loadData = (reduxStore, routePath) => {
  const storeSlug = routePath.match.params.store;
  const productSlug = routePath.match.params.slug;

  return Promise.all([
    new Promise((resolve) => {
      reduxStore
        .dispatch(storeActions.fetchStore(storeSlug))
        .then(resolve)
        .catch(resolve);
    }),
    new Promise((resolve) => {
      reduxStore
        .dispatch(storeActions.fetchProducts(storeSlug))
        .then(resolve)
        .catch(resolve);
    }),
    reduxStore.dispatch(storeActions.fetchProductDetail(storeSlug, productSlug)),
  ]);
};

// Map Redux Props and Actions to component
function mapStateToProps(state) {
  return {
    currentUser: state.users,
    userReviews: state.users.reviews,
    reviews: state.store.reviews.product,
    pendingReviews: state.store.pending_reviews.product,
    store: getStoreDetail(state),
    product: getProductDetail(state),
    marketCategories: state.app.marketCategories.categories,
    storeProducts:
      state.store.products.results && state.store.products.results.length
        ? state.store.products.results.filter(
            (p) => p.product_type.value === PRODUCT_TYPES.PHYSICAL,
          )
        : [],
  };
}

function mapDispatchToProps(dispatch) {
  const {
    fetchStore,
    fetchProducts,
    fetchProductDetail,
    resetProductDetail,
    getProductReviews,
    getPendingProductReviews,
  } = storeActions;
  const { updateCartProduct } = shoppingCartActions;
  const { addReview } = userActions;
  const { trackProductImpression } = analyticsActions;
  return bindActionCreators(
    {
      fetchStore,
      fetchProducts,
      fetchProductDetail,
      updateCartProduct,
      openModalBox: modalBoxActions.open,
      closeModalBox: modalBoxActions.close,
      resetProductDetail,
      getProductReviews,
      getPendingProductReviews,
      addReview,
      trackProductImpression,
    },
    dispatch,
  );
}

// Export Component
export default connect(mapStateToProps, mapDispatchToProps)(ProductPage);
