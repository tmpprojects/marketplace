import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import axios from 'axios';

import { landingActions } from '../../Actions/landing.actions';
import PageHead from '../../Utils/PageHead';
import Banner from '../../components/Landing/Banner/Banner';
import Products from '../../components/Landing/Products/Products';
import Inspire from '../../components/Landing/Inspire/Inspire';
import { IconPreloader } from '../../Utils/Preloaders';
import Stores from '../../components/Landing/Stores/Stores';
import LandingCountDown from '../../components/Landing/Countdown/LandingCountDown';
import ProductsPage from '../../components/Landing/Products/ProductswithPagination';
import StoresList from '../../components/Landing/StoresCatalog/StoresList';
import HeaderAndCountDown from '../../components/Landing/HeaderAndCountdown/HeaderAndCountdown';

import image404 from './../../../images/illustration/404.svg';
import 'slick-carousel/slick/slick.scss';
import 'slick-carousel/slick/slick-theme.scss';
import './_landing.scss';
import '../../../styles/_search.scss';
import HoraRosa from '../../components/HoraRosa/HoraRosa.js';
let dataLayerInit = null;

const storesJaliscoList: string =
  'https://canastarosa.s3.us-east-2.amazonaws.com/temp/static-listings/stores_ja.json';
const storesNuevoLeonList: string =
  'https://canastarosa.s3.us-east-2.amazonaws.com/temp/static-listings/stores_nl.json';

//
type CollectionPageProps = {
  match: object,
  fetchData: function,
  landing: object,
};
type CollectionPageState = {
  from: number,
  to: number,
  activePage: number,
  staticList: array,
};
class Landing extends Component<CollectionPageProps, CollectionPageState> {
  state = {
    from: 0,
    to: 11,
    activePage: 1,
    staticList: [],
  };


  constructor(props) {
    super(props);
  }

  componentWillUnmount() {
    dataLayerInit=null;

  
  }

  /**
   * Render
   */
  componentDidMount() {
    const { params } = this.props.match;
    this.fetchData(params.store);

    //
    if (params.store.includes('activacion-mty')) {
      axios
        .get(storesNuevoLeonList)
        .then((response) => {
          this.setState({ staticList: response.data });
        })
        .catch();
    } else if (params.store.includes('stores-jalisco')) {
      axios
        .get(storesJaliscoList)
        .then((response) => {
          this.setState({ staticList: response.data });
        })
        .catch();
    }
  }
  componentWillReceiveProps(nextProps) {
    if (this.props.match.params.store !== nextProps.match.params.store) {
      this.fetchData(nextProps.match.params.store);
    }

    //console.log(this.props.landing.allData.listings)
    if (JSON.stringify(this.props.landing.allData.listings) != JSON.stringify(nextProps.landing.allData.listings) && this.props.landing.loading == false) {
      dataLayerInit = [];
    }
  }

  /**
   * fetchData
   */
  fetchData = (slug): void => {
    this.props.fetchData(slug);
  };

  /**
   * changeSlice
   */
  changeSlice = (from: number = 0, to: number = 0, activePage: number = 0): void => {
    this.setState({ from, to, activePage });
    window.scrollTo({ top: 0, behavior: 'smooth' });
  };

  dispatchDataLayer(dataLayerResults, category, all) {

    try {

      //breadcrumbs
      let dataLayerDimensions = '';
      let breadcrumbsPath = null;
      //breadcrumbs

      //products
      let dataLayerContent = '[';
      dataLayerResults.map((preItem, index) => {
        const item = preItem;
        dataLayerContent += `
       {
        "name": "${globalThis.googleAnalytics.utils.clearLayerText(item?.name)}",
        "id": "${item?.id}",
        "price": "${item?.price}",
        "brand": "${globalThis.googleAnalytics.utils.clearLayerText(item?.store?.name)}",
        "category": null,
        ${dataLayerDimensions}
        "variant": null,
        "list": "Colecciones",
        "position": ${(index + 1)}
       }${globalThis.googleAnalytics.utils.closeArray(index, dataLayerResults)} 
        `;
      });
      //products

      const dataLayerBreadcrumbs = globalThis.googleAnalytics.utils.checkBreadcrumbs(breadcrumbsPath);
      dataLayerContent = JSON.parse(dataLayerContent);

      //Call productImpression  - event
      globalThis.googleAnalytics.productImpression(dataLayerContent, dataLayerBreadcrumbs);


      globalThis.googleAnalytics.currentImpression = {
        event: "ProductImpression",
        dataLayerBreadcrumbs,
        dataLayerContent: all
      }

    } catch (e) {
      console.log("invoke productImpression [Storepage.jsx]", e)
    }
  }

  /**
   * Render
   */
  render() {
    // Destructuring the object key that contains all the data.
    const { landing } = this.props;
    const { params } = this.props.match;
    const { staticList } = this.state;

    // Redirect in case the fetch data returns an error.
    if (landing.error) {
      return (
        <div className="landing-error">
          <img src={image404} alt="error404" />
          <p>
            El landing <strong>{params.store}</strong> no existe.
          </p>
        </div>
      );
    }
    // Loader while the data its loading.
    if (landing.loading) return <IconPreloader />;
    // Rename object key
    const { allData: dataObject } = landing;
    // Defining initial values of the variables that will contain the data of each section.
    const stores = [];
    const products = [];
    const articles = [];
    // Check if listings contain information
    if (dataObject.listings?.length) {
      // Iterate object
      dataObject.listings.forEach((a) => {
        // Depending of the type of listings, it will assign in the correct variable.
        switch (a.type) {
          case 'store':
            stores.push(a);
            break;
          case 'product':
            // Saves only the first item of the type of listings.
            products.push(a);
            break;
          case 'article':
            // Saves only the first item of the type of listings.
            articles.push(a);
            break;
          default:
        }
      });

      if (dataLayerInit !== null && (JSON.stringify(dataLayerInit) != JSON.stringify(products))) {
        //console.log("PRODUCTS", products)
        let multipleProducts = [];
        products.map((i, index) => {
          let productsCategory = []
          i.items.map((item, index2) => {
            item.position = index2;
            multipleProducts.push(item.product)
            productsCategory.push(item);
            //console.log("--->product", item.product)
          })
         // console.log(i);
          //this.dispatchDataLayer(productsCategory, i.name, multipleProducts)
        });
        this.dispatchDataLayer(multipleProducts, null, multipleProducts)

        //console.log("TODOS", multipleProducts)
        //console.log("SI")
        dataLayerInit = products;
      }else{
        //console.log("NO")
      }

    }

    // calc for fake paginator
    const top = Math.ceil((products.length + 1) / 10) * 10;
    let pages = [];
    //console.log('top', top);

    for (let i = 0; i < top / 10; i++) {
      pages.push(
        <li
          key={i}
          className={this.state.activePage === i + 1 ? 'active' : undefined}
          onClick={() => this.changeSlice(i * 10, i * 10 + 10, i + 1)}
        >
          {i + 1}
        </li>,
      );
    }
    ///

    const { from = 0, to = 0, activePage = 0 } = this.state;

    return (
      <div className="landing-container">
        {/* Browser Tab */}
        <PageHead
          attributes={{
            title: `${dataObject.name} - Market | Canasta Rosa`,
          }}
        />

        {/* Seccions of the Page */}
        <HoraRosa location={this.props.location} little={true}></HoraRosa>
        <Banner data={dataObject} id="top" />

        {/* Show only the countdown on landing according to params*/}
        {/* <HeaderAndCountDown
          paramOfStore={params.store}
          triggerRenderParam={'comadre_bazar'}
          date={'2020-11-27T23:59:00.000'}
          text={
            'Envío gratis del 28 de Noviembre  al 02 de Diciembre en compras mayores a $299 MX'
          }
        />
        <HeaderAndCountDown
          paramOfStore={params.store}
          triggerRenderParam={'momsters_bazar'}
          date={'2020-12-02T23:59:00.000'}
          text={
            'Envío gratis del 03 al 06 de Diciembre en compras mayores a $450 MX'
          }
        />
        <HeaderAndCountDown
          paramOfStore={params.store}
          triggerRenderParam={'bazar_solidario'}
          date={'2020-12-02T23:59:00.000'}
          text={'Envío gratis usando el código de cada tienda.'}
        /> */}

        {stores?.length >= 1 && <Stores storesData={stores} params={params.store} />}

        {/* Stores Catalog */}
        {params.store.includes('activacion-mty') && (
          <div className="cr__storesCatalog">
            <StoresList location={'landing-mty'} items={staticList} />
          </div>
        )}
        {params.store.includes('stores-jalisco') && (
          <div className="cr__storesCatalog">
            <StoresList location={'landing-gdl'} items={staticList} />
          </div>
        )}

        {products.length >= 1 && (
          <React.Fragment>
            <ProductsPage
              productsData={products}
              from={from}
              to={to}
              params={params.store}
            />
            <div className="pagination">
              <ul>{pages.length > 1 && pages.map((page) => page)}</ul>
            </div>
          </React.Fragment>
        )}

        <Inspire articlesData={articles} params={params.store} />
      </div>
    );
  }
}
// Preload Data for Server side rendering
Landing.loadData = (store, routePath) => {
  //const { params } = this.props.match;
  const { match } = routePath;
  const params = match.params;

  const promises = [store.dispatch(landingActions.fetchData(params.store))];
  return Promise.all(promises.map((p) => (p.catch ? p.catch((e) => e) : p)));
};
// Redux Map Functions
function mapStateToProps({ landing }) {
  return { landing };
}

function mapDispatchToprops(dispatch) {
  const { fetchData } = landingActions;
  return bindActionCreators(
    {
      fetchData,
    },
    dispatch,
  );
}
// Export Connected Component
export default connect(mapStateToProps, mapDispatchToprops)(Landing);
