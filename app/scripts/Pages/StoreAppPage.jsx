import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  Field,
  reduxForm,
  formValueSelector,
  getFormValues,
  getFormSyncErrors,
  getFormSubmitErrors,
  SubmissionError,
} from 'redux-form';
import { bindActionCreators } from 'redux';
import { renderRoutes } from 'react-router-config';
import { Link } from 'react-router-dom';
import * as typeformEmbed from '@typeform/embed';

import '../../styles/_storeApp.scss';
import SEO from '../../statics/SEO.json';
import PageHead from '../Utils/PageHead';
import { userTypes } from '../Constants';
import { IconPreloader } from '../Utils/Preloaders';
import requireAuth from '../components/hocs/requireAuth';
import {
  modalBoxActions,
  myStoreActions,
  ordersActions,
  statusWindowActions,
  typeformActions,
} from '../Actions';
import { InputField } from '../Utils/forms/formComponents';
import requireStore from '../components/hocs/requireStore';
import StoreSideBar from '../components/StoreApp/StoreSideBar';
import { getMyStoreDetail } from '../Reducers/mystore.reducer';
import { getVendorOrdersList } from '../Reducers/orders.reducer';
import StoreOff from '../components/StoreApp/Store/StoreStatus/StoreOffBanner/StoreOff';
import StorePausedBanner from '../components/StoreApp/Store/StoreStatus/StorePausedBanner/StorePausedBanner';
import StoreInProgressBanner from '../components/StoreApp/Store/StoreStatus/StoreInProgressBanner/StoreInProgressBanner';
import StoreWelcomeBanner from '../components/StoreApp/Store/StoreStatus/StoreWelcomeBanner/StoreWelcomeBanner';
import Cookies from 'universal-cookie';
const cookies = new Cookies();
import './StoreAppPage.scss';

const fakeObject = [
  {
    invited_by_user: {
      full_name: 'Cookie',
      store: 'Cookie',
    },
    invited_with_role: 'store',
    invited_to: 'create_store',
  },
];

class StoreAppPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
      referrerCode: '',
    };

    // Properties and Method bindings
    this.canUpdateNotificationsWindow = false;
  }

  onSubmit = (formValues) => {
    this.props
      .addReferrerCode({
        code: formValues.referrer,
      })
      .then((response) => {
        this.props.openStatusWindow({
          type: 'success',
          message: 'Tu código se aplicó correctamente.',
        });
      })
      .catch((error) => {
        // throw new SubmissionError({
        //   username: 'User does not exist',
        //   _error: 'Login failed!'
        // });
        this.props.openStatusWindow({
          type: 'error',
          message: error.response.data.code,
        });
      });
  };

  getReferredCode = (e) => {
    let { value } = e.target;
    value = value.replace(/[^A-Za-z0-9-]/gi, '');
    this.setState({ referrerCode: value });
  };

  /**
   * isMenuOpen()
   * Open mobile menu
   */
  isMenuOpen = () => {
    this.setState({
      open: !this.state.open,
    });
  };

  /**
   * isMenuClose()
   * Closes mobile menu
   */
  isMenuClose = () => {
    this.setState({
      open: this.state.open,
    });
  };

  /*
   * getNotificationMessage
   * Displays a personalized link based on the pending field name
   */
  getNotificationMessage = (codeField) => {
    let pendingField = codeField.charAt(0).toUpperCase() + codeField.slice(1);
    switch (pendingField) {
      case 'Telefono':
        pendingField = <Link to="/my-store/settings/info">Teléfono</Link>;
        break;
      case 'Address':
        pendingField = <Link to="/my-store/settings/addresses">Dirección</Link>;
        break;
      case 'Pago':
        pendingField = <Link to="/my-store/settings/accounts">Formas de Pago</Link>;
        break;
      case 'Products':
        pendingField = <Link to="/my-store/products">Publicar un producto</Link>;
        break;
      default:
    }
    return pendingField;
  };

  setCookie = () => {
    this.props.addReferrerCodeCookie(fakeObject);
    cookies.set('referrerCode', JSON.stringify(fakeObject), { path: '/' });
  };

  /*
   * renderNotifications
   * Displays a UI Notifications Window to alert users of Store 'status'.
   */
  renderNotifications = () => {
    // Update window state
    this.canUpdateNotificationsWindow = true;

    const { myStore } = this.props;

    //Avoid rendering unwanted code
    const list = myStore.is_active.pending.filter(
      (item) => item.code !== 'store_deactivated',
    );

    const content = (
      <React.Fragment>
        <div>
          Tu tienda aún no es visible para el público. Por favor completa los
          siguientes datos para continuar:
        </div>

        <ul className="list">
          {list.map((p, i) => (
            <li key={i}>
              <Link to="/my-store/products">
                {this.getNotificationMessage(p.code)}
              </Link>
            </li>
          ))}
        </ul>
      </React.Fragment>
    );

    return (
      <div className="statusNotificationsWindow statusNotificationsWindow--warning">
        <div className="statusNotificationsWindow__content">{content}</div>
        demo
      </div>
    );
  };

  /**
   * updateNotificationsWindowSize()
   * Calculate and update Notifications window size/position
   */
  updateNotificationsWindowSize = () => {
    const notificationsWindow = document.querySelector('.statusNotificationsWindow');

    // Update only after notifications window has mounted
    if (this.canUpdateNotificationsWindow && notificationsWindow) {
      //this.canUpdateNotificationsWindow = false;
      const content = notificationsWindow.querySelector(
        '.statusNotificationsWindow__content',
      );
      content.style.width = `${notificationsWindow.clientWidth}px`;
      notificationsWindow.style.height = `${content.clientHeight}px`;
    }
  };

  /*
   * React Component Life Cycle Functions
   */
  componentDidUpdate(prevProps, prevState) {
    this.updateNotificationsWindowSize();

    const { myStore = {}, showTypeform = {} } = this.props;

    if (
      (showTypeform?.isShown &&
        myStore?.store_status?.slug === 'deactivated_by_seller') ||
      (showTypeform?.isShown &&
        myStore?.store_status?.slug === 'in_process_to_deactivated_by_seller')
    ) {
      this.formCloseStore.open();
      this.props.closeTypeform();
    }

    if (
      (showTypeform?.isShown &&
        myStore?.store_status?.slug === 'paused_by_seller') ||
      (showTypeform?.isShown &&
        myStore?.store_status?.slug === 'in_process_to_paused_by_seller')
    ) {
      this.formPausedStore.open();
      this.props.closeTypeform();
    }
  }

  componentWillUnmount() {
    // Remove notification window events
    window.removeEventListener('resize', this.updateNotificationsWindowSize);
  }
  componentDidMount() {
    // Fetch Store Info
    this.props.fetchMyStore().then((response) => {
      let cookiesGet = cookies.get('referrerCode');
      if (cookiesGet) {
        this.props.addReferrerCodeCookie(cookiesGet);
      } else {
        this.props.getEnteredReferrerCodes();
      }
    });

    const isStoreClosed = this.props?.myStore?.is_active?.pending?.some(
      (item) => item.code === 'store_deactivated',
    );

    if (!isStoreClosed) {
      this.props.fetchProductList();
      this.props.fetchMyStoreSections();
      this.props.getFiscalRegistries();
      this.props.getInvoiceOptions();
      this.props.getBankAccountOptions();

      // Load Vendor Orders
      this.props.getOrders(userTypes.VENDOR);
    }

    // Update notification window dimmensions on window resize
    window.addEventListener('resize', this.updateNotificationsWindowSize);

    //Typeform

    const urlCloseStore = 'https://mycanastarosa.typeform.com/to/TglvwBIL';

    const urlPausedStore = 'https://mycanastarosa.typeform.com/to/lVzZLXTJ';

    const options = {
      mode: 'popup',
      opacity: 10,
      hideFooter: true,
      hideHeaders: true,
    };

    // Form for Close Store
    this.formCloseStore = typeformEmbed.makePopup(urlCloseStore, options);

    // Form for Paused Store
    this.formPausedStore = typeformEmbed.makePopup(urlPausedStore, options);

    // Typeform end
  }

  render() {
    const { route, orders, myStore, products, deletePaymentMethod } = this.props;

    const isStoreActive = myStore?.is_active?.bool;

    const isStoreClosed =
      !isStoreActive && myStore?.store_status?.slug === 'deactivated_by_seller';

    const isStorePaused =
      !isStoreActive && myStore?.store_status?.slug === 'paused_by_seller';

    const isStoreInProgressToClose =
      !isStoreActive &&
      myStore?.store_status?.slug === 'in_process_to_deactivated_by_seller';

    const isStoreInProgressToPause =
      !isStoreActive &&
      myStore?.store_status?.slug === 'in_process_to_paused_by_seller';

    const isInProgressTocloseOrToPaused =
      isStoreInProgressToClose || isStoreInProgressToPause;

    const storeName = myStore?.name;

    // Render preloader icon if store data is still loading
    if (myStore.loading) {
      //|| !myStore.entered_codes) {
      return <IconPreloader />;
    }

    // Look for server side notifications
    let notifications = null;
    if (myStore.hasOwnProperty('is_active') && !myStore.is_active.bool) {
      //Avoid rendering unwanted code
      notifications = myStore.is_active.pending.filter(
        (item) => item.code !== 'store_deactivated',
      );
    }

    // Render Component
    return (
      <div>
        <PageHead attributes={SEO.StoreAppPage} />
        {/* <StoreAppHeader /> */}

        <div className="storeApp">
          <StoreSideBar
            open={this.state.open}
            myStore={myStore}
            toogleMenu={this.isMenuOpen}
            isStoreClosed={isStoreClosed}
            isStorePaused={isStorePaused}
          />

          <div className="contentWrapper">
            {isStoreClosed ? (
              <StoreOff
                storeName={storeName}
                updateStoreStatus={this.props.updateStoreStatus}
                fetchMyStore={this.props.fetchMyStore}
                openStatusWindow={this.props.openStatusWindow}
                openModalBox={this.props.openModalBox}
                closeModalBox={this.props.closeModalBox}
              />
            ) : (
              <>
                {isStorePaused && (
                  <StorePausedBanner
                    storeName={storeName}
                    updateStoreStatus={this.props.updateStoreStatus}
                    fetchMyStore={this.props.fetchMyStore}
                    openStatusWindow={this.props.openStatusWindow}
                    openModalBox={this.props.openModalBox}
                    closeModalBox={this.props.closeModalBox}
                  />
                )}

                {isInProgressTocloseOrToPaused && (
                  <StoreInProgressBanner
                    updateStoreStatus={this.props.updateStoreStatus}
                    fetchMyStore={this.props.fetchMyStore}
                    openStatusWindow={this.props.openStatusWindow}
                    openModalBox={this.props.openModalBox}
                    closeModalBox={this.props.closeModalBox}
                  />
                )}

                <StoreWelcomeBanner isStoreActive={isStoreActive} />

                {notifications?.length > 0 && this.renderNotifications()}

                {renderRoutes(route.routes, {
                  deletePaymentMethod,
                  myStore,
                  products,
                  orders,
                })}
              </>
            )}
          </div>
        </div>
      </div>
    );
  }
}

// Wrap component within reduxForm
const formName = 'myStoreReferrerCode_Form';
StoreAppPage = reduxForm({
  form: formName,
  // enableReinitialize: true,
  // keepDirtyOnReinitialize: true // <---- Prevent 'dirty' fields from updating
})(StoreAppPage);

// Add Redux state and actions to component´s props
const selector = formValueSelector(formName);

// Map Redux Props and Actions to component
function mapStateToProps(state) {
  const { myStore, typeform } = state;
  const vendorOrders = getVendorOrdersList(state); //orders[userTypes.VENDOR.toLowerCase()];
  const myStoreDetail = getMyStoreDetail(state);

  return {
    myStore: myStoreDetail,
    products: myStore.products,
    orders: vendorOrders,
    myStoreFiscal: myStore.data,
    improvements: myStore.improvements,
    formValues: getFormValues(formName)(state) || {},
    formErrors: getFormSyncErrors(formName)(state),
    submitErrors: getFormSubmitErrors(formName)(state),
    showTypeform: typeform,
  };
}
function mapDispatchToProps(dispatch) {
  const {
    fetchMyStore,
    fetchProductList,
    deletePaymentMethod,
    fetchMyStoreSections,
    fetchFiscalData,
    getInvoiceOptions,
    getFiscalRegistries,
    setMyStoreImprovements,
    addReferrerCode,
    addReferrerCodeCookie,
    getEnteredReferrerCodes,
    getBankAccountOptions,
    updateStoreStatus,
  } = myStoreActions;

  return bindActionCreators(
    {
      fetchMyStore,
      fetchProductList,
      deletePaymentMethod,
      fetchMyStoreSections,
      fetchFiscalData,
      getFiscalRegistries,
      setMyStoreImprovements,
      getInvoiceOptions,
      addReferrerCodeCookie,
      getOrders: ordersActions.getOrders,
      addReferrerCode,
      getEnteredReferrerCodes,
      getBankAccountOptions,
      openStatusWindow: statusWindowActions.open,
      updateStoreStatus,
      openModalBox: modalBoxActions.open,
      closeModalBox: modalBoxActions.close,
      closeTypeform: typeformActions.close,
    },
    dispatch,
  );
}

// Decorate Component
StoreAppPage = requireAuth(StoreAppPage);
StoreAppPage = requireStore(StoreAppPage);
StoreAppPage = connect(mapStateToProps, mapDispatchToProps)(StoreAppPage);

// Load Data for Server Side Rendering
StoreAppPage.loadData = (store) =>
  Promise.all([
    store.dispatch(myStoreActions.fetchMyStore()),
    store.dispatch(myStoreActions.fetchProductList()),
    store.dispatch(myStoreActions.fetchMyStoreSections()),
    store.dispatch(myStoreActions.getFiscalRegistries()),
    store.dispatch(myStoreActions.getInvoiceOptions()),
    store.dispatch(ordersActions.getOrders(userTypes.VENDOR)),
    store.dispatch(myStoreActions.getBankAccountOptions()),
  ]).catch((e) => {
    return false;
  });

// Export Component
export default StoreAppPage;
