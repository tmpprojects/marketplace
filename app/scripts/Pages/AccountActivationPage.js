import React from 'react';
import '../../styles/_accountActivation.scss';

const AccountActivationPage = (props) => {
  // Render markup
  return (
    <section className="accountActivation">
      <div className="welcome__illustration"></div>
      {/* <h3 style={{ margin: '1em auto .75em', textAlign: 'center' }}>Te damos la bienvenida.</h3> */}
      <h5>Ya formas parte de nuestra comunidad de creadores y artesanos.</h5>
      <p>
        Haz login con tu email/contraseña y descubre originales artículos, tutoriales
        y mucho más.
      </p>
    </section>
  );
};
export default AccountActivationPage;
