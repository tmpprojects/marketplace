import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Link } from 'react-router-dom';

import '../../styles/_404-ProductPage.scss';
import { appActions } from '../Actions';
import NewStores from '../components/Home/NewStores';
import CategoriesPopular from '../Utils/CategoriesPopular';

export class NotFoundPage extends Component {
  //const NotFoundPage = ({ staticContext = {} }) => {
  constructor(props) {
    super(props);
    let { staticContext = {} } = props;
    // staticContext.notFound = true;
  }

  componentDidMount() {
    this.restoreStatusCode();
  }
  restoreStatusCode() {
    if (process.env.CLIENT) {
      window.INIT_NODE.context.status = 200;
    }
  }
  componentDidCatch(error, info) {
    console.log('Error NotFoundPage');
  }
  render() {
    const { stores = [] } = this.props;

    return (
      <section className="notFoundPage">
        <div className="img_container">
          <img src={require('./../../images/illustration/404.svg')} />
        </div>
        <h3>[3] No logramos encontrar esta página.</h3>
        <p>
          <Link className="button-simple button-simple--pink link" to={'/'}>
            Conoce nuestros últimos productos
          </Link>
        </p>
        <div className="home">
          <NewStores
            stores={stores
              .filter((store) => store.cover.thumbnail !== '')
              .slice(0, 3)}
          />
        </div>
        <div className="ProductSpace" />
        <CategoriesPopular />
      </section>
    );
  }
}

function mapStateToProps({ app }) {
  const { storesList } = app;
  return {
    stores: storesList.results,
  };
}
function mapDispatchToProps(dispatch) {
  return bindActionCreators({}, dispatch);
}

NotFoundPage.loadData = (store) =>
  Promise.all([
    store.dispatch(appActions.fetchBanners()),
    store.dispatch(appActions.getStoresList()),
    store.dispatch(appActions.getFeaturedStores()),
    store.dispatch(appActions.getFeaturedProducts()),
  ]);

// Export Component
export default connect(mapStateToProps, mapDispatchToProps)(NotFoundPage);
