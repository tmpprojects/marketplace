import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Link } from 'react-router-dom';
import { appActions } from '../Actions';
import NewStores from '../components/Home/NewStores';
import CategoriesPopular from '../Utils/CategoriesPopular';
import '../../styles/_404-ProductPage.scss';

export class NotFoundPage extends Component {
  //const NotFoundPage = ({ staticContext = {} }) => {
  constructor(props) {
    super(props);
    let { staticContext = {} } = props;
    //staticContext.notFound = true;
  }
  render() {
    const { stores = [] } = this.props;

    return (
      <section className="notFoundPage">
        <div className="img_container">
          <img src={require('./../../images/illustration/404.svg')}></img>
        </div>
        <h3>No pudimos encontrar la página que buscas.</h3>
        <p>
          <Link className="button-simple button-simple--pink link" to={'/'}>
            Conoce nuestros últimos products
          </Link>
        </p>
        <div className="home">
          <NewStores
            stores={stores
              .filter((store) => store.cover.thumbnail !== '')
              .slice(0, 3)}
          />
        </div>
        <div className="ProductSpace">
          <CategoriesPopular />
        </div>
      </section>
    );
  }
}

function mapStateToProps({ app }) {
  const { storesList } = app;

  return {
    stores: storesList.results,
  };
}
function mapDispatchToProps(dispatch) {
  const {} = appActions;

  return bindActionCreators({}, dispatch);
}

NotFoundPage.loadData = (store) =>
  Promise.all([
    store.dispatch(appActions.fetchBanners()),
    store.dispatch(appActions.getStoresList()),
    store.dispatch(appActions.getFeaturedStores()),
    store.dispatch(appActions.getFeaturedProducts()),
  ]).catch((e) => {
    //Send a redirection in case something goes wrong
    return false;
  });

// Export Component
export default connect(mapStateToProps, mapDispatchToProps)(NotFoundPage);
