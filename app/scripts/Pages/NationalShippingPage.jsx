import React, { Component } from 'react';
import { Link, NavLink } from 'react-router-dom';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import queryString from 'query-string';

import '../../styles/_categorySearchResults.scss';
import { appActions } from '../Actions';
import SEO from '../../statics/SEO.json';
import PageHead from '../Utils/PageHead';
import { IconPreloader } from '../Utils/Preloaders';
import { ResponsiveImage, ResponsiveImageFromURL } from '../Utils/ImageComponents';
import CategoriesPopular from '../Utils/CategoriesPopular';
import ProductsList from '../components/Search/ProductsList';
import withPagination from '../components/hocs/withPagination';
import { Assistant } from '../Utils/Assistant';

const MAX_ITEMS_PER_PAGE = 21;

/**
 * FILTER LIST COMPONENT
 * Renders a SelectInput with different sorting parameters
 */
const FiltersList = (props) => {
  const onChange = function (e) {
    props.onSortChange(e.target.value);
  };

  return null;
  return (
    <div className="filters">
      <p>Ordenar por:</p>
      <div className="filters__list">
        <select name="filters" onChange={onChange}>
          <option value="-created" selected={props.sort === '-created'}>
            Fecha
          </option>
          <option value="price" selected={props.sort === 'price'}>
            Precio
          </option>
          <option value="name" selected={props.sort === 'name'}>
            Nombre
          </option>
        </select>
      </div>
    </div>
  );
};

/**
 * ProductsList Component (with pagination)
 * Wraps a list of products within 'withPagination' HOC,
 * and displays a paginated component.
 */
const PaginatedList = withPagination(ProductsList);

/**
 * NationalShippingPage Class Component
 */
export class NationalShippingPage extends Component {
  constructor(props) {
    super(props);
    const resultsPage = this.getResultsPageFromQueryString(
      this.props.location.search,
    );
    this.defaultSort = '';
    this.currentPage = resultsPage;
    this.selectedStore = null;
    this.state = {
      sortCriteria: this.defaultSort,
    };
  }

  /**
   * React Life Cycle Methods
   */
  componentWillMount() {}
  componentDidMount() {
    // Make the search
    this.performSeach(
      this.props.match.params.categoryName,
      this.currentPage,
      this.getStoreFromQueryString(this.props.location.search),
      this.getSortFromQueryString(this.props.location.search),
    );
  }
  componentWillReceiveProps(nextProps) {
    // If search query string (URL ?p={pageNumber}&store={storeSlug}) has changed
    if (this.props.location.search !== nextProps.location.search) {
      this.performSeach(
        nextProps.match.params.categoryName,
        this.getResultsPageFromQueryString(nextProps.location.search),
        this.getStoreFromQueryString(nextProps.location.search),
        this.getSortFromQueryString(nextProps.location.search),
      );
    }

    // If pathname (category name) has changed, perform a new search
    if (this.props.location.pathname !== nextProps.location.pathname) {
      this.performSeach(nextProps.match.params.categoryName);
    }
  }

  onSortChange = (sortBy) =>
    this.props.history.push(
      `${
        this.props.location.pathname
      }?sort=${sortBy}&store=${this.getStoreFromQueryString(
        this.props.location.search,
      )}`,
    );

  getResultsPageFromQueryString = (searchParams) => {
    if (searchParams) {
      const resultsPage = parseInt(queryString.parse(searchParams).p, 10);
      return !isNaN(resultsPage) ? resultsPage : 1;
    }
    return 1;
  };

  getStoreFromQueryString = (searchParams) => {
    if (searchParams && queryString.parse(searchParams).store) {
      return queryString.parse(searchParams).store;
    }
    return '';
  };

  getSortFromQueryString = (searchParams) => {
    if (searchParams && queryString.parse(searchParams).sort) {
      return queryString.parse(searchParams).sort;
    }
    return '';
  };

  /**
   * fetchContent()
   * Retrieves results from the API
   * @param {string} query | Query string to send to API
   */
  fetchContent = (query) =>
    Promise.all([
      this.props.getCategoryResults(query.categoryQuery),
      this.props.getStoresList(query.storesQuery),
      // conditionally, load stores list.
      // ...(this.props.match.categoryName !== query.categoryQuery.categoryName
      //     ? this.props.getStoresList(query.storesQuery) : []
      // )
    ]);

  /**
   * performSeach()
   * Constructs a search query string and perform the search action(API call)
   * @param {string} categoryName | Category Slug
   * @param {int} page | Page index
   * @param {string} store | Store slug
   * @param {string} sort | Sorting 'slug'
   * @param {int} itemsPerPage | Maximum number of items per page
   */
  performSeach = (
    categoryName,
    page = 1,
    store = '',
    sortType = this.defaultSort,
    itemsPerPage = MAX_ITEMS_PER_PAGE,
  ) => {
    // Scroll to top of page
    // TODO: Scroll to top of results list instead of top of document.
    window.scrollTo(0, 0);

    // Construct search query parameters
    const searchQueries = buildQuery(
      {
        store,
        page,
        slug: categoryName,
        pageSize: itemsPerPage,
        sort: sortType,
      },
      {
        slug: categoryName,
        pageSize: 21,
      },
    );

    // Make the call to the API to get results
    this.sortCriteria = sortType;
    this.selectedStore =
      store !== ''
        ? this.props.storesList.results.find((s) => s.slug === store)
        : null;
    this.currentPage = parseInt(page, 10);
    this.fetchContent(searchQueries);
  };

  /**
   * render()
   */
  render() {
    const {
      marketCategories = [],
      searchResults,
      storesList,
      match,
      location,
    } = this.props;
    const currentPage = this.currentPage;
    const categoryName = match.params.categoryName;
    const { selectedStore } = this;
    const { sortCriteria } = this.state;

    // Show preloader if searchResults are still loading.
    if (searchResults.loading) return <IconPreloader />;

    // Get Page Title based on Category
    let category = { name: '', description: '' };
    if (marketCategories.length) {
      category = {
        ...category,
        ...marketCategories.find((item) => item.slug === categoryName),
      };
    }

    // Render Results
    return (
      <section className="categoryResults">
        <PageHead
          attributes={{
            title: `${category.name} - Market | Canasta Rosa`,
          }}
        />

        {/* CATEGORY COVER */}
        <div className="category-cover">
          <div
            className="category-cover__image"
            style={
              category.photo
                ? { backgroundImage: `url(${category.photo.medium})` }
                : {}
            }
          />

          <div className="details">
            <h1 className="details__name">{category.name}</h1>
            <p className="details__description">{category.description}</p>
          </div>
        </div>
        {/* END: /CATEGORY COVER */}

        <div className="results_list">
          <div className="wrapper--center">
            <Assistant />

            <div className="results_list-container">
              <div className="storesMenu">
                <h5 className="title">
                  Tiendas en esta categoría{' '}
                  <span>({storesList.results.length || 0})</span>
                </h5>

                {/* STORES LIST */}
                <ul>
                  <li className={`store ${!selectedStore ? 'active' : ''}`}>
                    <NavLink to={`${category.slug}`} className="name">
                      Todos los productos
                    </NavLink>
                  </li>

                  {storesList.loading ? (
                    <li className="store">Cargando Tiendas</li>
                  ) : (
                    storesList.results.map((store) => (
                      <li
                        key={store.slug}
                        className={`store ${
                          selectedStore && selectedStore.slug === store.slug
                            ? 'active'
                            : ''
                        }`}
                      >
                        <NavLink
                          to={`${location.pathname}?store=${store.slug}`}
                          className="logo"
                        >
                          <ResponsiveImageFromURL
                            src={store.photo.small}
                            alt={store.name}
                          />
                        </NavLink>
                        <NavLink
                          to={`${location.pathname}?store=${store.slug}`}
                          className="name"
                        >
                          {store.name}
                          {/*<span className="count">({store.count})</span>*/}
                        </NavLink>
                      </li>
                    ))
                  )}
                </ul>
                {/* END: /STORES LIST */}
              </div>

              <div className="productsList">
                {
                  /* STORE COVER */
                  selectedStore && (
                    <div className="store-summary">
                      <div
                        className="store-summary__cover"
                        style={{
                          backgroundImage: `url(${selectedStore.cover.medium})`,
                        }}
                      />
                      <div className="store-summary__info">
                        <div className="image">
                          <ResponsiveImageFromURL
                            src={selectedStore.photo.small}
                            alt={selectedStore.name}
                          />
                        </div>
                        <div className="name-wrapper">
                          <h3 className="title">{selectedStore.name}</h3>
                          <h4 className="slogan">{selectedStore.slogan}</h4>
                        </div>
                        <Link
                          className="visit button-square"
                          to={`/stores/${selectedStore.slug}`}
                        >
                          Visitar Tienda
                        </Link>
                      </div>
                    </div>
                  )
                  /* END: /STORE COVER */
                }

                {/* TITLE & SORTING CONTAINER */}
                <div className="title_container">
                  <h5 className="title">
                    Producto{searchResults.results.length !== 1 ? 's' : ''}
                    <span>({searchResults.count})</span>
                  </h5>
                  <FiltersList
                    sort={sortCriteria}
                    store={selectedStore}
                    onSortChange={this.onSortChange}
                  />
                </div>
                {/* END: /TITLE & SORTING CONTAINER */}

                {/* PAGINATED LIST */}
                <PaginatedList
                  items={searchResults.results}
                  baseLocation={`/category/${
                    category.slug
                  }?sort=${this.getSortFromQueryString(
                    location.search,
                  )}&store=${this.getStoreFromQueryString(location.search)}`}
                  page={currentPage}
                  action={this.testingFunction}
                  maxItems={MAX_ITEMS_PER_PAGE}
                  totalPages={searchResults.npages}
                />
                {/* END: /PAGINATED LIST */}
              </div>
            </div>
          </div>
        </div>

        <CategoriesPopular />
      </section>
    );
  }
}

const buildQuery = (categoryQuery, storesQuery) => {
  return {
    categoryQuery: `?ordering=${categoryQuery.sort}&store__slug=${categoryQuery.store}&category__slug=${categoryQuery.slug}&page=${categoryQuery.page}&page_size=${categoryQuery.pageSize}`,
    storesQuery: `?products__category__slug=${storesQuery.slug}&page_size=${storesQuery.pageSize}`,
  };
};

// Load Data for Server Side Rendering
NationalShippingPage.loadData = (reduxStore, routePath) => {
  const { match, location } = routePath;

  // TODO: How to get URL params with react-router on node side?
  const resultsPage = 1; //location.search ? queryString(location.search).p : 1;
  const searchQueries = buildQuery(
    {
      slug: match.params.categoryName,
      page: resultsPage,
      pageSize: MAX_ITEMS_PER_PAGE,
    },
    {
      slug: match.params.categoryName,
      pageSize: 21,
    },
  );

  return Promise.all([
    reduxStore.dispatch(appActions.getCategoryResults(searchQueries.categoryQuery)),
    reduxStore.dispatch(appActions.getStoresList(searchQueries.storesQuery)),
  ]);
};

// Map Redux Props and Actions to component
function mapStateToProps({ app }) {
  return {
    storesList: app.storesList,
    searchResults: app.categoryResults.loading
      ? { loading: true }
      : app.categoryResults.results,
    marketCategories: app.marketCategories.categories,
  };
}
function mapDispatchToProps(dispatch) {
  const { getCategoryResults, getStoresList } = appActions;
  return bindActionCreators(
    {
      getStoresList,
      getCategoryResults,
    },
    dispatch,
  );
}

// Export Component
export default connect(mapStateToProps, mapDispatchToProps)(NationalShippingPage);
