import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import '../../styles/_store.scss';
import '../../styles/_errorPage.scss';
import config from '../../config';
import SEO from '../../statics/SEO.json';
import PageHead from '../Utils/PageHead';
import { storeActions, modalBoxActions } from '../Actions';
import Stock from '../components/Store/Stock';
import About from '../components/Store/About';
import { Faqs } from '../components/Store/Faqs';
import { Menu } from '../components/Store/Menu';
import { Terms } from '../components/Store/Terms';
import { IconPreloader } from '../Utils/Preloaders';
import Reviews from '../components/Store/Reviews';
import { getStoreDetail } from '../Reducers/store.reducer';
import { SimilarItem } from '../components/Product/SimilarItem';
import CategoriesPopular from '../Utils/CategoriesPopular';
import { ResponsiveImage, ResponsiveImageFromURL } from '../Utils/ImageComponents';
import SubmenuStock from '../components/Store/SubmenuStock';
import { socialNetworksURL, PRODUCT_TYPES } from '../Constants/config.constants';
import Rating from '../components/Rating/Rating';
import { formatDate, formatDateToUTC } from '../Utils/dateUtils';
import ServicesModalBox from '../Utils/modalbox/ServicesModalBox';
import EventsModalBox from '../Utils/modalbox/EventsModalBox';
import CoursesModalBox from '../Utils/modalbox/CoursesModalBox';
import { shareOnFacebook, shareOnTwitter } from '../Utils/socialMediaUtils';
import image302 from './../../images/illustration/403.svg';
import ReviewsList from '../components/Store/ReviewsList';
import { insertKeyToQueryParam } from './../Utils/queryParamsUtils';
import withPagination from '../components/hocs/withPagination';
import queryString from 'query-string';

const MAX_ITEMS_PER_PAGE = 5;
let dataLayerInit;
let dataLayerPage;

const PaginatedList = withPagination(Stock);
export class StorePage extends Component {
  constructor(props) {
    super(props);
    dataLayerInit ="";

    const { staticContext } = props;
    this.currentPage = 1;
    this.storeSlug = this.props.match.params.slug;
    this.queryParams = {
      p: this.props.location.search
        ? queryString.parse(this.props.location.search).p !== undefined
          ? queryString.parse(this.props.location.search).p
          : 1
        : 1,
    };

    this.searchProducts = this.searchProducts.bind(this);
    this.setCode = process.env.CLIENT
      ? window.INIT_NODE.context.status
      : staticContext.status;
    this.state = {
      status: this.setCode,
    };
  }

  componentDidMount() {
    globalThis.shippingAddressObserver.subscribe(this.addZipCodeFilter);

    // Load Store Details, Products and Sections
    this.props
      .fetchStore(this.storeSlug)
      .then((r) => {
        this.props.fetchProducts(this.storeSlug, `&page=${this.queryParams.p}`);
        this.props.fetchStoreSections(this.storeSlug);
        this.getReviews(this.storeSlug, this.currentPage);
        if (this.props.currentUser.isLogged) {
          this.props.getPendingStoreReviews(this.storeSlug);
        }
      })
      .catch((e) => console.log(e));
    this.restoreStatusCode();
  }

  componentWillUnmount() {
    dataLayerInit="";
    globalThis.shippingAddressObserver.unsubscribe(this.addZipCodeFilter);
  }

  dispatchDataLayer(dataLayerResults) {


    try {

      //breadcrumbs
      let dataLayerDimensions = '';
      let breadcrumbsPath = null;
      //breadcrumbs

      //products
      let dataLayerContent = '[';
      dataLayerResults.map((item, index) => {
        dataLayerContent += `
       {
        "name": "${globalThis.googleAnalytics.utils.clearLayerText(item?.name)}",
        "id": "${item?.id}",
        "price": "${item?.price}",
        "brand": "${globalThis.googleAnalytics.utils.clearLayerText(item?.store?.name)}",
        "category": ${globalThis.googleAnalytics.utils.checkBreadcrumbs(breadcrumbsPath)},
        ${dataLayerDimensions}
        "variant": null,
        "list": "Tiendas",
        "position": ${(index + 1) }
       }${globalThis.googleAnalytics.utils.closeArray(index, dataLayerResults)} 
        `;
      });
      //products

      const dataLayerBreadcrumbs = globalThis.googleAnalytics.utils.checkBreadcrumbs(breadcrumbsPath);
      dataLayerContent = JSON.parse(dataLayerContent);

      //Call productImpression  - event
      globalThis.googleAnalytics.productImpression(dataLayerContent, dataLayerBreadcrumbs);

    } catch (e) {
      console.log("invoke productImpression [Storepage.jsx]", e)
    }
  }

  componentWillReceiveProps(nextProps) {

   //DATALAYER - HOOOKS
   if ((JSON.stringify(this.props.products) != JSON.stringify(nextProps?.products)) && nextProps?.products?.length > 0 && nextProps?.productsAreLoading != true) {
    if (dataLayerInit !== nextProps.match.params.slug && dataLayerPage != this.queryParams.p) {
      console.log("dispatchDataLayer");
      this.dispatchDataLayer(nextProps?.products)
      dataLayerInit = nextProps.match.params.slug;
      dataLayerPage = this.queryParams.p;
    }
  }

    if (this.props.match.params.slug !== nextProps.match.params.slug) {
      this.state.status = 200;
      //this.forceUpdate();
    }

    if (this.props.location.search !== nextProps.location.search) {
      this.queryParams = {
        p: nextProps.location.search
          ? queryString.parse(nextProps.location.search).p
          : 1,
      };

      this.props.fetchProducts(this.storeSlug, `&page=${this.queryParams.p}`);
    }

    // const test = nextProps.storeProducts.find(p => p.store.slug === nextProps.match.params.slug);

    // if ((this.props.storeProducts.length !== nextProps.storeProducts.length) && test) {
    //   console.log(" avers storeProducts", nextProps.storeProducts);
    // }
    // if (this.props.store.loading !== nextProps.store.loading) {
    //   console.log("cambio algo");
    // }
  }

  /**
   * openDetailsWindow()
   * Opens a modalbox with product description depending on product_type.
   * @param {object} product | Product Object
   */
  openDetailsWindow = (product) => {
    const productType = product.product_type.value;
    switch (productType) {
      case PRODUCT_TYPES.EVENT:
        this.props.openModalBox(() => <EventsModalBox />);
        break;
      case PRODUCT_TYPES.SERVICE:
        this.props.openModalBox(() => <ServicesModalBox />);
        break;
      case PRODUCT_TYPES.COURSE:
        this.props.openModalBox(() => <CoursesModalBox product={product} />);
        break;
      default:
        break;
    }
  };

  //   /**
  //  * onFilterSubmit()
  //  * @param {string} query | Filters querystring.
  //  */
  onFilterSubmit = (query) => {
    this.props.history.push(`${this.props.location.pathname}${query}`);
  };

  // /**
  //  * addZipCodeFilter()
  //  * Adds zip code filter to current queryparams string
  //  * @param {string} zipCode | Query string to send to API
  //  */
  addZipCodeFilter = (zipCode) => {
    // this.onFilterSubmit(
    //   insertKeyToQueryParam('zipcode', zipCode, document.location.search)
    // );

    // Load Store Details, Products and Sections
    this.props.fetchProducts(this.storeSlug, `&page=1`);
  };

  /**
   * searchProducts()
   * Search Products (from API) by filter
   * @param {*} filter
   */
  searchProducts(filter) {
    // Fetch filtered products
    this.props.fetchProducts(this.props.store.slug, filter);
  }

  /**
   * render()
   */
  sanitizeUrl(slug) {
    const extractSlug = slug.split('-');
    let newSlug = '';
    for (let obj of extractSlug) {
      newSlug += `${obj} `;
    }
    return newSlug;
  }
  restoreStatusCode() {
    if (process.env.CLIENT) {
      window.INIT_NODE.context.status = 200;
    }
  }

  //Get approved reviews from store (of all their products)
  getReviews(store, page) {
    this.props.getStoreReviews(
      store,
      `?page=${page}&page_size=${MAX_ITEMS_PER_PAGE}`,
    );
    this.currentPage = page;
  }

  componentDidCatch(error, info) {
    console.log('Error StorePage');
  }
  render() {
    if (this.state.status === 404) {
      return (
        <div className="ProductError">
          <img src={image302} width="400px" />
          <h3 className="title--main">
            [1] No logramos encontrar la tienda:
            <br />
            <strong className="ProductErrorName">
              {this.sanitizeUrl(this.props.match.params.slug)}
            </strong>
          </h3>

          <p>Posiblemente se movió a otra sección o se ha eliminado.</p>
          <p>
            <a
              href={
                '/search/stores/' + this.sanitizeUrl(this.props.match.params.slug)
              }
            >
              Ver otras tiendas similares
            </a>
          </p>
          <div className="ProductSpace" />

          {/* <section
            itemScope=""
            itemType="http://schema.org/Product"
            className="productItem_container"
          >
            <SimilarItem
              productsList={this.props.storeProducts.slice(0, 4)}
              store={this.props.store}
            />
          </section>
          */}
          <CategoriesPopular />
        </div>
      );
    }

    // Wait for basic info to load
    if (this.props.store.loading || !this.props.products) {
      return (
        <div>
          <IconPreloader />
        </div>
      );
    }

    // Get Store Properties
    const {
      name,
      excerpt,
      photo,
      cover,
      user = { profile_photo: '' },
      social_links = [],
      reviews: { count: reviewsCount, rating },
      is_on_vacation,
      vacation_range,
    } = this.props.store;
    const { sections, products, reviews, pendingReviews } = this.props;

    // Page Location
    const pageLocation = `${config.frontend_host}${this.props.location.pathname}`;

    const store = this.props.match.params.slug;
    const currentPage = this.currentPage;
    // Social Media Links
    const socialMediaLinks_list = social_links.map((item, index) => {
      const networkName = item.name.charAt(0).toUpperCase() + item.name.slice(1);
      const networkPrefix = socialNetworksURL[item.name];
      return !networkPrefix ? null : (
        <div className="socialMedia__list_container" key={index}>
          <li className={`socialMedia__list__item ${item.name}`}>
            <a
              href={`${networkPrefix}${item.link}`}
              rel="noopener noreferrer"
              target="_blank"
            >
              {networkName}
            </a>
          </li>
        </div>
      );
    });

    const socialMediaShare = (
      <div className="socialMedia__share">
        <div className="socialMedia__share--buttons">
          <span className="share">Compartir</span>
          <a
            href="#"
            onClick={(e) => {
              e.preventDefault();
              shareOnFacebook(pageLocation);
            }}
            className="icon icon--facebook"
          >
            <span>Compartir</span>
          </a>
          <a
            href="#"
            onClick={(e) => {
              e.preventDefault();
              shareOnTwitter(pageLocation, name);
            }}
            className="icon icon--twitter"
          >
            <span>Tweet</span>
          </a>
          <a
            href={`https://api.whatsapp.com/send?text=${pageLocation}`}
            //data-action="share/whatsapp/share"
            className="icon icon--whatsapp"
          >
            <span>Whatsapp</span>
          </a>
        </div>
      </div>
    );

    const { p: currPage } = this.queryParams;
    // Return Markup
    return (
      <section className="storePage">
        <PageHead
          attributes={{
            title: `${name} - Market | Canasta Rosa`,
            meta: [
              {
                name: 'description',
                content: `${excerpt}`,
              },
              {
                property: 'og:title',
                content: `${name}`,
              },
              {
                property: 'og:description',
                content: `${excerpt}`,
              },
              {
                property: 'og:url',
                content: `${config.frontend_host}${this.props.location.pathname}`,
              },
              {
                property: 'og:image',
                content: `${cover.big}`,
              },
              {
                property: 'og:image:width',
                content: '500',
              },
              {
                property: 'og:image:height',
                content: '200',
              },
            ],
            tags: [name],
          }}
        />

        <div className="profile">
          <div className="profile__cover">
            <ResponsiveImage src={cover} alt={`${name} Cover Photo`} />
          </div>

          <div className="profile__info wrapper--center">
            <div className="contactInfo">
              <div className="contactInfo__profilePicture">
                <ResponsiveImageFromURL
                  src={photo.small}
                  alt={`${name} Profile Photo`}
                />
              </div>
              <div className="desktop">{socialMediaShare}</div>
              {/* <div className="desktop">
                  <a 
                      href="#" 
                      className=" follow c2a_border">
                          <span>Seguir Tienda</span>
                  </a>

                  {(socialMediaLinks_list.length > 0) &&
                      <nav className="socialMedia">
                          <ul className="socialMedia__list">
                              {socialMediaLinks_list}
                          </ul>
                      </nav>
                  }
              </div> */}
            </div>

            <div className="details">
              <div className="details__name">
                <h1 className="title">{name}</h1>
                <div className="rating-container">
                  <Rating disabled rating={rating} />
                  <span>({reviewsCount})</span>
                </div>
              </div>
              <p className="details__excerpt">{excerpt}</p>
            </div>

            <div className="creator">
              <div className="creator__info">
                <div className="photo">
                  <ResponsiveImageFromURL
                    src={user.profile_photo.small}
                    alt={`${user.first_name} ${user.last_name}`}
                  />
                </div>
                <div>
                  <p>
                    {user.first_name} {user.last_name}
                  </p>
                  <span>Owner</span>
                </div>
              </div>
            </div>
            <div className="mobile">{socialMediaShare}</div>

            {/* {(socialMediaLinks_list.length > 0) &&
                <div className="mobile">
                        <a 
                        href="#" 
                        className="follow c2a_border"
                        ><span>Seguir Tienda</span></a>
                    <nav className="socialMedia">
                        <ul className="socialMedia__list">
                            {socialMediaLinks_list}
                        </ul>
                    </nav>
                </div>
            } */}
          </div>
        </div>

        {is_on_vacation ? (
          <div className="holidays">
            <h4>Estamos de Vacaciones</h4>
            <span className="day">
              - Regresamos el{' '}
              {formatDate(formatDateToUTC(new Date(vacation_range.end)))} -
            </span>
          </div>
        ) : null}

        <Menu />

        <div className="products products-list">
          <SubmenuStock
            searchProducts={this.searchProducts}
            sections={sections}
            store={this.props.store.slug}
          />

          {this.props.productsAreLoading ? (
            <IconPreloader />
          ) : products.length > 0 ? (
            // <Stock
            //   store={this.props.store}
            //   stock={products}
            //   id="stock"
            //   openDetailsWindow={this.openDetailsWindow}
            // />
            <div className="paginator__container">
              <PaginatedList
                items={products}
                baseLocation={`/stores/${this.props.store.slug}?`}
                page={parseInt(currPage, 10)}
                maxItems={99}
                totalPages={this.props.products_npages}
                location={'store-page'}
              />
            </div>
          ) : (
            <div className="products__wrapper message-container">
              <p className="message">No se encontraron productos disponibles.</p>
            </div>
          )}
        </div>

        {(reviews.results.length > 0 || pendingReviews.results.length > 0) && (
          <div className="review" id="reviews">
            <div className="wrapper--center">
              <ReviewsList
                reviews={reviews.results}
                pendingReviews={pendingReviews.results}
                reviewsCount={reviewsCount}
                rating={rating}
                purchase={{}}
                showProductDetails
              />
            </div>

            {
              // Show button for More Reviews only if exists another page
              reviews.next !== null && (
                <div className="review_btn">
                  <button
                    type="button"
                    className="button-square--pink"
                    onClick={() => this.getReviews(store, currentPage + 1)}
                  >
                    Ver más Reseñas
                  </button>
                </div>
              )
            }
          </div>
        )}

        <About id="about" storeData={this.props.store} />

        <div className="info">
          {/* <Faqs id="faqs" faqs={this.props.myStore.faqs}/> */}
          {/* <Terms id="terms"/> */}
        </div>
      </section>
    );
  }
}

// Load Data for Server Side Rendering
StorePage.loadData = (reduxStore, routePath) => {
  const storeSlug = routePath.match.params.slug;

  return Promise.all([
    reduxStore.dispatch(storeActions.fetchStore(storeSlug)).catch((e) => {
      throw e;
    }),
    reduxStore.dispatch(storeActions.fetchProducts(storeSlug)),
    reduxStore.dispatch(storeActions.fetchStoreSections(storeSlug)),
  ]).catch((e) => {
    return false;
  });
};

// Map Redux Props and Actions to component
function mapStateToProps(state) {
  const { store, users, app } = state;

  return {
    isMobile: app.isMobile,
    currentUser: users,
    store: getStoreDetail(state),
    products_npages: store.products.npages,
    productsAreLoading: store.products?.loadingStatus?.loading,
    products:
      store.products.results && store.products.results.length
        ? store.products.results.filter(
            (p) => p.product_type.value === PRODUCT_TYPES.PHYSICAL,
          )
        : [],
    sections: store.sections.sections,
    storeProducts:
      state.store.products.results && state.store.products.results.length
        ? state.store.products.results.filter(
            (p) => p.product_type.value === PRODUCT_TYPES.PHYSICAL,
          )
        : [],
    reviews: state.store.reviews.store,
    pendingReviews: state.store.pending_reviews.store,
  };
}
function mapDispatchToProps(dispatch) {
  const {
    fetchStore,
    fetchProducts,
    fetchStoreSections,
    getStoreReviews,
    getPendingStoreReviews,
  } = storeActions;
  return bindActionCreators(
    {
      fetchStore,
      fetchProducts,
      fetchStoreSections,
      openModalBox: modalBoxActions.open,
      closeModalBox: modalBoxActions.close,
      getStoreReviews,
      getPendingStoreReviews,
    },
    dispatch,
  );
}

// Export Component
export default connect(mapStateToProps, mapDispatchToProps)(StorePage);
