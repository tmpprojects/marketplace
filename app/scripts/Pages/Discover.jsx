import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { appActions } from '../Actions';
import { AboutUs } from '../components/Inspire/AboutUs';
import Tags from './../components/Tags/Tags';
import '../../styles/_discoverPage.scss';

export const Discover = (props) => (
  <section className="home discoverPage">
    <div>
      <Tags />
    </div>
    <AboutUs />
  </section>
);

// Load Data for Server Side Rendering
Discover.loadData = (store) => Promise.all([]);

// Map Redux Props and Actions to component
function mapStateToProps(state) {
  return {};
}
function mapDispatchToProps(dispatch) {
  return bindActionCreators({}, dispatch);
}

// Export Component
export default connect(mapStateToProps, mapDispatchToProps)(Discover);
