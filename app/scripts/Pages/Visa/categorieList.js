export const list = [
  {
    name: 'Comida',
    slug: 'comida-1',
    img: require('./assets/categoryImg/Comida.jpg'),
    key: 1,
  },
  {
    name: 'Diseño',
    slug: 'disenio-1',
    img: require('./assets/categoryImg/Libreta.jpg'),
    key: 2,
  },
  {
    name: 'Flores y Regalos',
    slug: 'Flores-y-Regalos',
    img: require('./assets/categoryImg/Flores.jpg'),
    key: 3,
  },
  {
    name: 'Ropa y Calzado',
    slug: 'ropa-y-calzado-1',
    img: require('./assets/categoryImg/RopaYcalzado.jpg'),
    key: 4,
  },
  {
    name: ' Accesorios y Joyería ',
    slug: 'accesorios-y-joyeria-1',
    img: require('./assets/categoryImg/Joyeria.jpg'),
    key: 5,
  },
  {
    name: 'Infantil',
    slug: 'infantil-1',
    img: require('./assets/categoryImg/Infantil.jpg'),
    key: 6,
  },
  {
    name: 'Belleza',
    slug: 'belleza-1',
    img: require('./assets/categoryImg/Belleza.jpg'),
    key: 7,
  },
  {
    name: 'Mascotas',
    slug: 'mascotas-1',
    img: require('./assets/categoryImg/Mascotas.jpg'),
    key: 8,
  },
];
