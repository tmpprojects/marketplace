import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import queryString from 'query-string';
import Slider from 'react-slick';

import { appActions, analyticsActions } from '../../Actions';
import PageHead from '../../Utils/PageHead';
import { IconPreloader } from '../../Utils/Preloaders';
import { ResponsiveImageFromURL } from '../../Utils/ImageComponents';
import CategoriesPopular from '../../Utils/CategoriesPopular';
import { ProductsList } from '../../components/Visa/ProductList';
import withPagination from '../../components/hocs/withPagination';
import { list } from '../Visa/categorieList';
import { insertKeyToQueryParam } from '../../Utils/queryParamsUtils';
import { stores } from './statisStores';
import { storesList } from './statisStores';
import Placeholder from '@canastarosa/ds-theme/assets/images/placeholder/placeholder--productPage.jpg';

import 'slick-carousel/slick/slick.scss';
import 'slick-carousel/slick/slick-theme.scss';
import '../../../styles/_categorySearchResults.scss';
import './visa_Landing.scss';

/**Slick settings */
const settings = {
  infinite: true,
  swipeToSlide: true,
  arrows: process.env.CLIENT ? true : false,
  speed: 500,
  slidesToShow: 6,
  slidesToScroll: 6,
  autoplay: false,
  //autoplaySpeed: 7000,
  lazyLoad: process.env.CLIENT ? 'progressive' : null,
  pauseOnHover: true,
  cssEase: 'ease',
  responsive: [
    {
      breakpoint: 2000,
      settings: {
        slidesToShow: 5.99,
        slidesToScroll: 6,
      },
    },
    {
      breakpoint: 1200,
      settings: {
        slidesToShow: 4,
        slidesToScroll: 4,
        arrows: false,
      },
    },
  ],
};

const MAX_ITEMS_PER_PAGE = 25;
const ENVIO_NACIONAL_SLUG = 'envio-nacional';
/**
 * ProductsList Component (with pagination)
 * Wraps a list of products within 'withPagination' HOC,
 * and displays a paginated component.
 */
const PaginatedList = withPagination(ProductsList);

/**
 * CategoryPage Class Component
 */
export class CategoryPage extends Component {
  constructor(props) {
    super(props);
    this.defaultSort = '';
    this.currentPage = this.getResultsPageFromQueryString(
      this.props.location.search,
    );
    this.selectedStore = null;
    //To populate the SidebarFilters form if the url contains filters
    this.urlValues = this.getURLVals(this.props.location.search);

    this.state = {
      sortCriteria: this.defaultSort,
      isOpen: false,
      isMobile: false,
    };
  }

  /**
   * React Life Cycle Methods
   */
  componentDidMount() {
    this.props.history.push('/visa/?has_discount=true');
    this.props.getStoresListVisa();
    // Perform the search
    if (!this.props.searchResults.previouslyLoaded) {
      this.performSeach(
        this.props.match.params.categoryName,
        this.currentPage,
        this.getFiltersFromQueryString(this.props.location.search),
        this.getSortFromQueryString(this.props.location.search),
      );
    }

    // Resize handler
    this.onResizeHandler();
    window.addEventListener('resize', this.onResizeHandler);

    // shippingAddressObserver
    // globalThis.shippingAddressObserver.subscribe(this.addZipCodeFilter);
  }

  componentWillReceiveProps(nextProps) {
    // If search query string (Ej. URL ?p={pageNumber}&store={storeSlug}) has changed
    if (this.props.location.search !== nextProps.location.search) {
      this.performSeach(
        nextProps.match.params.categoryName,
        this.getResultsPageFromQueryString(nextProps.location.search),
        this.getFiltersFromQueryString(nextProps.location.search),
        this.getSortFromQueryString(nextProps.location.search),
      );
    }

    // If pathname (category name) has changed, perform a new search
    if (this.props.location.pathname !== nextProps.location.pathname) {
      this.performSeach(nextProps.match.params.categoryName);
    }

    //Track impressions
    if (this.props.searchResults !== nextProps.searchResults) {
      const products = nextProps.searchResults.results;
      if (products !== undefined) {
        const formattedProducts = products.map((prod, index) => ({
          product: prod.slug,
          index,
        }));
        this.props.trackListingImpressions('category-page', formattedProducts);
      }
    }
  }
  componentWillUnmount() {
    window.removeEventListener('resize', this.onResizeHandler);

    // shippingAddressObserver
    // globalThis.shippingAddressObserver.unsubscribe(this.addZipCodeFilter);
  }

  getURLVals = (query) => {
    const queryParams = this.getQueryParamsAsObject(query.replace('?', ''));

    return Object.keys(queryParams).reduce((a, key) => {
      const ifv = a;

      if (key === 'delivery_date') {
        ifv.delivery_day = 'picked';
        //Only for the option where user selects an specific date, for calendar
        ifv.delivery_day_picked = queryParams[key];
      }

      if (key === 'zone') {
        ifv.shipping = queryParams[key];
      }

      if (key === 'max_price') {
        ifv.price = `max_price_${queryParams[key]}`;
      }

      if (key === 'stores') {
        //Turn stores param into array of Slug Stores
        const storesArray = queryParams[key].replace('+', ' ').split(' ');
        //Array of Selected Stores
        const storesSelected = storesArray.map((s) => {
          const store = this.props.storesList.results.find((st) => st.slug === s);
          //Add 'selected' key so the store appears checked
          return {
            ...store,
            selected: true,
          };
        });

        //Array of Not Selected Stores
        let storesNotSelected = this.props.storesList.results;
        storesSelected.forEach((sl) => {
          storesNotSelected = storesNotSelected.filter((s) => s.slug !== sl.slug);
        });

        ifv.stores = storesSelected.concat(storesNotSelected);
      }
      return ifv;
    }, {});
  };

  /**
   * onResizeHandler()
   * Listen for device resize and updates component state.
   */
  onResizeHandler = () => {
    let isMobile = false;
    const w =
      window.innerWidth ||
      document.documentElement.clientWidth ||
      document.body.clientWidth;
    if (w < 727) {
      isMobile = true;
    }
    this.setState({
      isMobile,
    });
  };
  /**
   * onSortChange()
   * @param {string} sortBy | Sorting criteria.
   */
  onSortChange = (sortBy) =>
    this.props.history.push(
      `${
        this.props.location.pathname
      }?sort=${sortBy}&store=${this.getStoreFromQueryString(
        this.props.location.search,
      )}`,
    );

  /**
   * onFilterSubmit()
   * @param {string} query | Filters querystring.
   */
  onFilterSubmit = (query) => {
    this.props.history.push(`${this.props.location.pathname}?${query}`);
  };

  /**
   * addZipCodeFilter()
   * Adds zip code filter to current queryparams string
   * @param {string} zipCode | Query string to send to API
   */
  addZipCodeFilter = (zipCode) => {
    this.onFilterSubmit(
      insertKeyToQueryParam('order_zipcode', zipCode, document.location.search),
    );
  };

  getQueryParamsAsObject = (query = '') => {
    const queryArray = query.split('&');

    return queryArray.reduce((a, b) => {
      const tempAttribute = a;
      const keyValueArr = b.split('=');
      if (keyValueArr.length === 2) {
        tempAttribute[keyValueArr[0]] = keyValueArr[1];
      }
      return tempAttribute;
    }, {});
  };

  /**
   * getResultsPageFromQueryString()
   * @param {string} query
   * Extracts the page number and returns it as an int number.
   */
  getResultsPageFromQueryString = (query) => {
    if (query) {
      const resultsPage = parseInt(queryString.parse(query).p, 10);
      return !isNaN(resultsPage) ? resultsPage : 1;
    }
    return 1;
  };

  /**
   * getResultsPageFromQueryString()
   * @param {string} query
   * Extracts and return the store slug from a query string.
   */
  getStoreFromQueryString = (query) => {
    if (query && queryString.parse(query).store) {
      return queryString.parse(query).store;
    }
    return '';
  };

  /**
   * getSortFromQueryString()
   * @param {string} query
   * Extracts and return sort criteria from a query string.
   */
  getSortFromQueryString = (query) => {
    if (query && queryString.parse(query).sort) {
      return queryString.parse(query).sort;
    }
    return '';
  };

  /**
   * getFiltersFromQueryString()
   * Extracts filters portion from a query string.
   */
  getFiltersFromQueryString = (query = '') => {
    const queryParams = this.getQueryParamsAsObject(query.replace('?', ''));
    return Object.keys(queryParams).reduce((acc, key) => {
      let qs = acc;
      if (key !== 'sort' && key !== 'p' && key !== 'store') {
        qs += `&${key}=${queryParams[key]}`;
      }
      return qs;
    }, '');
  };

  /**
   * openFiltersWindow()
   * Update component state to open filters container.
   */
  openFiltersWindow = (toggle) => {
    this.setState({
      isOpen: toggle !== undefined ? toggle : !this.state.isOpen,
    });
  };

  /**
   * fetchContent()
   * Retrieves results from the API. Call redux actions.
   * @param {string} query | Query string to send to API
   */
  fetchContent = (query, slug) =>
    Promise.all([
      this.props.getCategoryResults(query.categoryQuery, slug),
      this.props.getStoresList(query.storesQuery, slug),
    ]);

  /**
   * performSeach()
   * Constructs a search query string to perform the search.
   * @param {string} categoryName | Category Slug
   * @param {int} page | Page index
   * @param {string} filters | Filters querystring portion
   * @param {string} sort | Sorting 'slug'
   * @param {int} itemsPerPage | Maximum number of items per page
   */
  performSeach = (
    categoryName,
    page = 1,
    filters = '',
    sortType = this.defaultSort,
    itemsPerPage = MAX_ITEMS_PER_PAGE,
  ) => {
    // Scroll to top of page
    // TODO: Scroll to top of results list instead of top of document.
    window.scrollTo(0, 0);

    // Construct search query parameters
    const searchQueries = buildQuery(
      {
        filters,
        page,
        slug: categoryName,
        pageSize: itemsPerPage,
        sort: sortType,
      },
      {
        slug: categoryName,
        pageSize: 21,
      },
    );

    // Make the call to the API to get results
    this.sortCriteria = sortType;
    // const store = this.getStoreFromQueryString(filters);
    // this.selectedStore = store
    //     ? this.props.storesList.stores.find(s => s.slug === store)
    //     : null;
    this.selectedStore = '';
    this.currentPage = parseInt(page, 10);
    this.fetchContent(searchQueries, categoryName);
  };

  /**
   * findCategoryDetails()
   * @param {array} categories | List of categories.
   * @param {string} categorySlug | Category slug to match.
   * @returns {object} Category Object
   */
  findCategoryDetails = (categories, categorySlug) =>
    categories.reduce((a, b) => {
      // If we´ve found category details, bubble it up.
      if (a) {
        return a;
      }

      // Return currentItem if it matches categorySlug
      let cat = null;
      if (b.slug === categorySlug) {
        return b;
      }

      // Else, use this function recursevly with category children.
      return this.findCategoryDetails(b.children, categorySlug);
    }, null);

  /**
   * createBreadcrumbsTree()
   * Returns an array with the marching categories path.
   * @param {array} categories | Array of nested categories to search from.
   * @param {string} targetCategorySlug | Category String to search.
   * @param {bool} found | Indicates if category has been already found.
   * @param {array} parentPath | Array with the parent categories.
   * @returns {array} An array of categories (Model example: [{name, slug}, ...]).
   */
  createBreadcrumbsTree = (
    categories,
    targetCategorySlug,
    found = false,
    parentPath = [],
  ) => {
    const tester = categories.reduce(
      (a, b) => {
        let result = a;

        // If category is not found, proceed to recursive loop.
        if (!result.found) {
          // If we've found the matching category, return current result
          if (b.slug === targetCategorySlug) {
            result = {
              path: [...result.path, { name: b.name, slug: b.slug }],
              found: true,
            };

            // else, search whithin current category children.
          } else if (b.children.length) {
            const innerCategoryPath = this.createBreadcrumbsTree(
              b.children,
              targetCategorySlug,
              false,
              [...result.path, { name: b.name, slug: b.slug }],
            );

            if (innerCategoryPath.found) {
              result = innerCategoryPath;
            }
          }
        }

        // Return current path.
        return result;
      },
      { found, path: parentPath },
    );

    return tester;
  };

  errorImage = (e) => {
    e.target.src = Placeholder;
  };
  /**
   * render()
   */
  render() {
    const { marketCategories = [], searchResults, match, location } = this.props;
    let { storesList = {} } = this.props;
    const currentPage = this.currentPage;
    const urlValues = this.urlValues;
    const categoryName = match.params.categoryName;
    const { selectedStore } = this;
    const { sortCriteria, isMobile, isOpen } = this.state;
    // Sort stores list alphabetically
    storesList = {
      ...storesList,
      stores: storesList.results.sort((a, b) => {
        if (a.name < b.name) {
          return -1;
        }
        if (a.name > b.name) {
          return 1;
        }
        return 0;
      }),
    };

    // Find Category tree for breadcrumbs.
    const breadcrumbsPath = this.createBreadcrumbsTree(
      marketCategories,
      categoryName,
    ).path;

    // Show preloader if searchResults are still loading.
    let loadingContent = false;
    if (searchResults.loading || !searchResults.results) {
      loadingContent = true;
    }

    // Get Category Details.
    let category = { name: '', description: '' };
    if (marketCategories.length) {
      category = {
        ...category,
        ...this.findCategoryDetails(marketCategories, categoryName),
      };
    }

    // This is a PATCH.
    // This hardcodes category values if
    // the querystring passed on the URL is 'envíos nacionales'.
    // This category is not dynamic.
    if (categoryName === ENVIO_NACIONAL_SLUG) {
      category = {
        name: 'Envío Nacional',
        description: '',
        slug: ENVIO_NACIONAL_SLUG,
        photo: {},
        text: 'Envío Nacional',
        banner: {
          title: 'Envío Nacional',
          seo_tags: '',
        },
      };
    }
    console.log(this.props, 'Visa Landing');
    return (
      <section className="categoryResults">
        <PageHead
          attributes={{
            title: 'Visa Sales - Market | Canasta Rosa',
          }}
        />
        {/* CATEGORY COVER */}
        {isMobile ? (
          <>
            <div
              style={{
                padding: '2.5em 0em',
                display: 'flex',
                backgroundColor: '#1a1f71',
              }}
            >
              <div
                style={{
                  width: '50%',
                  display: 'flex',
                  alignItems: 'center',
                  paddingLeft: '0.5em',
                }}
              >
                <p
                  className="cr__text-caption"
                  style={{
                    color: 'white',
                    whiteSpace: 'normal',
                    fontSize: '1em',
                  }}
                >
                  Para un pequeño negocio,{' '}
                  <span style={{ display: 'block' }}>tu compra es grande</span>{' '}
                </p>
              </div>
              <div
                style={{
                  width: '50%',
                  display: 'flex',
                  alignItems: 'center',
                  justifyContent: 'space-around',
                }}
              >
                <img
                  style={{
                    width: '80%',
                  }}
                  src={require('./assets/logoImg/logo compuesto.svg')}
                  alt="logoCR"
                />
              </div>
            </div>
          </>
        ) : (
          <>
            <div style={{ width: '100%', height: '7em', display: 'flex' }}>
              <div
                style={{
                  width: '66.67%',
                  height: '7em',
                  backgroundImage: `url(${require('./assets/logoImg/galletas.jpg')})`,
                  backgroundSize: 'cover',
                  backgroundPositionX: 'center',
                  display: 'flex',
                  alignItems: 'center',
                  justifyContent: 'center',
                }}
              >
                <p
                  className="cr__text--subtitle2"
                  style={{ color: 'white', margin: '0' }}
                >
                  Para un pequeño negocio, tu compra es grande.
                </p>
              </div>
              <div
                style={{
                  backgroundColor: '#1a1f71',
                  width: '33.33%',
                  display: 'flex',
                  alignItems: 'center',
                  justifyContent: 'center',
                }}
              >
                <img
                  src={require('./assets/logoImg/logo compuesto.svg')}
                  alt="logoCR"
                  style={{ width: '50%' }}
                />
              </div>
            </div>
          </>
        )}

        <div className="visa-slider wrapper--center">
          <p className="cr__text--subtitle3 cr__textColor--colorBlack300">
            Increíbles descuentos por categorías
          </p>
          {!isMobile ? (
            <div className="visa-slider-container">
              <Slider {...settings}>
                {list.map(({ name, slug, img, key }) => (
                  <div className="visa-slider-container-item" key={key}>
                    <Link to={`/visa/${slug}/`}>
                      <img src={img} alt={name} />
                      <h6>{name}</h6>
                    </Link>
                  </div>
                ))}
              </Slider>
            </div>
          ) : (
            <div className="visa-slider-container slider-mobile">
              {list.map(({ name, slug, img }) => (
                <div className="visa-slider-container-item">
                  <Link to={`/visa/${slug}/`}>
                    <img src={img} alt={name} />
                    <h6>{name}</h6>
                  </Link>
                </div>
              ))}
            </div>
          )}
        </div>

        <div className="visa-discover">
          <div className="visa-discover-trianglelvisa" />
          <div className="visa-discover-trianglervisa" />
          <div className="visa-discover-offers">
            <ul className="visa-discover-offers-list">
              <li className="visa-discover-offers-list-item">
                <Link to="/visa/category/?has_discount=true&percentage_discount_from=20&percentage_discount_to=24">
                  <img
                    src={require('./assets/discountsImg/20.jpg')}
                    alt="hot sale"
                  />
                  {/* <Link
                  className='cr__text--caption'
                  to='/hotsale/category/?has_discount=true&percentage_discount_to=20'
                >
                  HASTA 20%
                </Link> */}
                </Link>
              </li>
              <li className="visa-discover-offers-list-item">
                <Link to="/visa/category/?has_discount=true&percentage_discount_from=25&percentage_discount_to=29">
                  <img
                    src={require('./assets/discountsImg/25.jpg')}
                    alt="hot sale"
                  />
                  {/* <Link
                  className='cr__text--caption'
                  to='/hotsale/category/?has_discount=true&percentage_discount_to=20'
                >
                  HASTA 20%
                </Link> */}
                </Link>
              </li>
              <li className="visa-discover-offers-list-item">
                <Link to="/visa/category/?has_discount=true&percentage_discount_from=30&percentage_discount_to=39">
                  <img
                    src={require('./assets/discountsImg/30.jpg')}
                    alt="hot sale"
                  />
                  {/* <Link
                  className='cr__text--caption'
                  to='/hotsale/category/?has_discount=true&percentage_discount_from=20&percentage_discount_to=30'
                >
                  20% - 30%
                </Link> */}
                </Link>
              </li>
              <li className="visa-discover-offers-list-item">
                <Link to="/visa/category/?has_discount=true&percentage_discount_from=40&percentage_discount_to=49">
                  <img
                    src={require('./assets/discountsImg/40.jpg')}
                    alt="hot sale"
                  />
                  {/* <Link
                    className='cr__text--caption'
                    to='/hotsale/category/?has_discount=true&percentage_discount_from=30&percentage_discount_to=40'
                  >
                    30% - 40%
                  </Link> */}
                </Link>
              </li>
            </ul>
          </div>
        </div>

        <div className="categoriesPopular  wrapper--center cr__visaStores">
          <p className="cr__text--subtitle2 visaTitle">Tiendas con envío gratis</p>
          {this.props?.storesListVisa?.loading ? (
            <IconPreloader />
          ) : (
            <div className="cr__storesCardsWrapper">
              {this.props?.storesListVisa?.list?.results?.map((item) => (
                <div key={item?.slug} className="cr__storesCardWrapper">
                  <Link to={`/stores/${item?.slug}`}>
                    <div className="cr__storesCover">
                      <span className="cr__text--caption cr__textColor--colorWhite">
                        Envío gratis
                      </span>
                      <img src={item?.cover?.medium} onError={this.errorImage} />
                    </div>
                  </Link>

                  <div className="cr__storesInfo">
                    <Link
                      to={`/stores/${item?.slug}`}
                      className="cr__storesInfo-img"
                    >
                      <img src={item?.photo?.small} onError={this.errorImage} />
                    </Link>
                    <Link
                      to={`/stores/${item?.slug}`}
                      className="cr__storesInfo-names"
                    >
                      <div>
                        <p className="cr__storesInfo-names-name cr__textColor--colorDark300 cr__text-paragraph">
                          {item?.name.charAt(0).toUpperCase() + item?.name?.slice(1)}
                        </p>
                      </div>
                      <div>
                        <p className="cr__storesInfo-names-slug cr__textColor--colorDark300 cr__text-paragraph">
                          {item?.slogan?.length >= 48
                            ? item?.slogan?.charAt(0).toUpperCase() +
                              item?.slogan.slice(1, 48) +
                              '...'
                            : item?.slogan?.charAt(0).toUpperCase() +
                              item?.slogan.slice(1)}
                        </p>
                      </div>
                    </Link>
                  </div>
                </div>
              ))}
            </div>
          )}
        </div>

        <div className="results_list">
          <div className="wrapper--center">
            <div className="results_list-container">
              {loadingContent ? (
                <IconPreloader />
              ) : (
                <div className="productsList" style={{ marginTop: '2em' }}>
                  <p className="cr__text--subtitle2 visaTitle">
                    Productos increíbles
                  </p>
                  {selectedStore && (
                    <div className="store-summary">
                      <div
                        className="store-summary__cover"
                        style={{
                          backgroundImage: `url(${selectedStore.cover.medium})`,
                        }}
                      />
                      <div className="store-summary__info">
                        <div className="image">
                          <ResponsiveImageFromURL
                            src={selectedStore.photo.small}
                            alt={selectedStore.name}
                          />
                        </div>
                        <div className="name-wrapper">
                          <h3 className="title">{selectedStore.name}</h3>
                          <h4 className="slogan">{selectedStore.slogan}</h4>
                        </div>
                        <Link
                          className="visit button-square"
                          to={`/stores/${selectedStore.slug}`}
                        >
                          Visitar Tienda
                        </Link>
                      </div>
                    </div>
                  )}

                  <PaginatedList
                    items={searchResults.results}
                    baseLocation={`/visa/?has_discount=true${this.getFiltersFromQueryString(
                      location.search,
                    )}`}
                    page={currentPage}
                    action={this.testingFunction}
                    maxItems={MAX_ITEMS_PER_PAGE}
                    totalPages={searchResults.npages}
                    location={'visa'}
                  />
                </div>
              )}
            </div>
          </div>
        </div>
        <CategoriesPopular />
      </section>
    );
  }
}

const buildQuery = (categoryQuery, storesQuery) => {
  if (categoryQuery.slug === ENVIO_NACIONAL_SLUG) {
    return {
      categoryQuery: `?ordering=${categoryQuery.sort}&page=${categoryQuery.page}&page_size=${categoryQuery.pageSize}${categoryQuery.filters}`,
      storesQuery: `?page_size=${storesQuery.pageSize}&${categoryQuery.filters}`,
    };
  } else {
    return {
      categoryQuery: `?${categoryQuery.filters}&has_discount=true&page=${categoryQuery.page}&page_size=${categoryQuery.pageSize}`,
    };
  }
};

// Load Data for Server Side Rendering
CategoryPage.loadData = (reduxStore, routePath) => {
  const { match, location } = routePath;

  // TODO: How to get URL params with react-router on node side?
  const resultsPage = 1; //location.search ? queryString(location.search).p : 1;
  const searchQueries = buildQuery(
    {
      slug: match.params.categoryName,
      page: resultsPage,
      pageSize: MAX_ITEMS_PER_PAGE,
    },
    {
      slug: match.params.categoryName,
      pageSize: 21,
    },
  );

  return Promise.all([
    reduxStore.dispatch(appActions.getCategoryResults(searchQueries.categoryQuery)),
    reduxStore.dispatch(appActions.getStoresList(searchQueries.storesQuery)),
  ]).catch((e) => {
    return false;
  });
};

// Map Redux Props and Actions to component
function mapStateToProps({ app }) {
  return {
    storesList: app.storesList,
    searchResults: app.categoryResults,
    marketCategories: app.marketCategories.categories,
    storesListVisa: app.storesListVisa,
  };
}
function mapDispatchToProps(dispatch) {
  const { getCategoryResults, getStoresList, getStoresListVisa } = appActions;
  const { trackListingImpressions } = analyticsActions;
  return {
    dispatch,
    ...bindActionCreators(
      {
        getStoresList,
        getCategoryResults,
        trackListingImpressions,
        getStoresListVisa,
      },
      dispatch,
    ),
  };
}

// Export Component
export default connect(mapStateToProps, mapDispatchToProps)(CategoryPage);
