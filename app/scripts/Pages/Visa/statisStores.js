export const stores = [
  {
    name: 'ENVIA UNA TAZA',
    slug: 'envia-una-taza',
    photo: {
      original:
        'https://canastarosa.s3.amazonaws.com/media/market/store/photo/75c84ab086f326a35ac9458bd8516566.jpeg',
      medium:
        'https://canastarosa.s3.amazonaws.com/media/market/store/photo/75c84ab086f326a35ac9458bd8516566.jpeg',
      big:
        'https://canastarosa.s3.amazonaws.com/media/market/store/photo/75c84ab086f326a35ac9458bd8516566.jpeg',
      small:
        'https://canastarosa.s3.amazonaws.com/media/market/store/photo/75c84ab086f326a35ac9458bd8516566.jpeg',
    },
    cover: {
      medium:
        'https://canastarosa.s3.amazonaws.com/media/market/store/cover/0e9f8c7f4f6f8fb318f6dc9e085f89f9.jpeg',
      small:
        'https://canastarosa.s3.amazonaws.com/media/market/store/cover/0e9f8c7f4f6f8fb318f6dc9e085f89f9.jpeg',
      original:
        'https://canastarosa.s3.amazonaws.com/media/market/store/cover/0e9f8c7f4f6f8fb318f6dc9e085f89f9.jpeg',
      big:
        'https://canastarosa.s3.amazonaws.com/media/market/store/cover/0e9f8c7f4f6f8fb318f6dc9e085f89f9.jpeg',
      thumbnail:
        'https://canastarosa.s3.amazonaws.com/media/market/store/cover/0e9f8c7f4f6f8fb318f6dc9e085f89f9.jpeg',
    },
    slogan: 'Enviamos emociones en forma de TAZA',
    mean_store_score: 0.0,
    shipping_methods: [],
  },
  {
    name: 'Entre Aromas & Colores',
    slug: 'entre-aromas-colores',
    photo: {
      original:
        'https://canastarosa.s3.amazonaws.com/media/market/store/photo/476670671ce33a2e5697ede66014e888.jpeg',
      medium:
        'https://canastarosa.s3.amazonaws.com/media/market/store/photo/476670671ce33a2e5697ede66014e888.jpeg',
      big:
        'https://canastarosa.s3.amazonaws.com/media/market/store/photo/476670671ce33a2e5697ede66014e888.jpeg',
      small:
        'https://canastarosa.s3.amazonaws.com/media/market/store/photo/476670671ce33a2e5697ede66014e888.jpeg',
    },
    cover: {
      medium:
        'https://canastarosa.s3.amazonaws.com/media/market/store/cover/615608feb2de8f083b7990b65b0be4ca.jpg',
      small:
        'https://canastarosa.s3.amazonaws.com/media/market/store/cover/615608feb2de8f083b7990b65b0be4ca.jpg',
      original:
        'https://canastarosa.s3.amazonaws.com/media/market/store/cover/615608feb2de8f083b7990b65b0be4ca.jpg',
      big:
        'https://canastarosa.s3.amazonaws.com/media/market/store/cover/615608feb2de8f083b7990b65b0be4ca.jpg',
      thumbnail:
        'https://canastarosa.s3.amazonaws.com/media/market/store/cover/615608feb2de8f083b7990b65b0be4ca.jpg',
    },
    slogan: 'Original y Natural',
    mean_store_score: 0.0,
    shipping_methods: [],
  },
  {
    name: 'Ápice Recuerdos',
    slug: 'apice-recuerdos',
    photo: {
      original:
        'https://canastarosa.s3.amazonaws.com/media/market/store/photo/c42f4c36d0166f2b5755c8bde51ec074.jpg',
      medium:
        'https://canastarosa.s3.amazonaws.com/media/market/store/photo/c42f4c36d0166f2b5755c8bde51ec074.jpg',
      big:
        'https://canastarosa.s3.amazonaws.com/media/market/store/photo/c42f4c36d0166f2b5755c8bde51ec074.jpg',
      small:
        'https://canastarosa.s3.amazonaws.com/media/market/store/photo/c42f4c36d0166f2b5755c8bde51ec074.jpg',
    },
    cover: {
      medium:
        'https://canastarosa.s3.amazonaws.com/media/market/store/cover/1ff579dca91c740944d1ed7e54f1a92b.jpg',
      small:
        'https://canastarosa.s3.amazonaws.com/media/market/store/cover/1ff579dca91c740944d1ed7e54f1a92b.jpg',
      original:
        'https://canastarosa.s3.amazonaws.com/media/market/store/cover/1ff579dca91c740944d1ed7e54f1a92b.jpg',
      big:
        'https://canastarosa.s3.amazonaws.com/media/market/store/cover/1ff579dca91c740944d1ed7e54f1a92b.jpg',
      thumbnail:
        'https://canastarosa.s3.amazonaws.com/media/market/store/cover/1ff579dca91c740944d1ed7e54f1a92b.jpg',
    },
    slogan: 'Creando Recuerdos',
    mean_store_score: 0.0,
    shipping_methods: [],
  },
  {
    name: 'Mágica Hierbería',
    slug: 'magica-hierberia',
    photo: {
      original:
        'https://canastarosa.s3.amazonaws.com/media/market/store/photo/c682c8f1c1e9899591f2eed8992494a5.png',
      medium:
        'https://canastarosa.s3.amazonaws.com/media/market/store/photo/c682c8f1c1e9899591f2eed8992494a5.png',
      big:
        'https://canastarosa.s3.amazonaws.com/media/market/store/photo/c682c8f1c1e9899591f2eed8992494a5.png',
      small:
        'https://canastarosa.s3.amazonaws.com/media/market/store/photo/c682c8f1c1e9899591f2eed8992494a5.png',
    },
    cover: {
      medium:
        'https://canastarosa.s3.amazonaws.com/media/market/store/cover/f58a7d35ef8a97c4d48644c19fb0bab3.jpg',
      small:
        'https://canastarosa.s3.amazonaws.com/media/market/store/cover/f58a7d35ef8a97c4d48644c19fb0bab3.jpg',
      original:
        'https://canastarosa.s3.amazonaws.com/media/market/store/cover/f58a7d35ef8a97c4d48644c19fb0bab3.jpg',
      big:
        'https://canastarosa.s3.amazonaws.com/media/market/store/cover/f58a7d35ef8a97c4d48644c19fb0bab3.jpg',
      thumbnail:
        'https://canastarosa.s3.amazonaws.com/media/market/store/cover/f58a7d35ef8a97c4d48644c19fb0bab3.jpg',
    },
    slogan: 'Medicina Ancestral.',
    mean_store_score: 0.0,
    shipping_methods: [],
  },
  {
    name: 'Kefirëe10',
    slug: 'kefiree10',
    photo: {
      original:
        'https://canastarosa.s3.amazonaws.com/media/market/store/photo/9442c0f420006300481ec43a2be3eb49.jpeg',
      medium:
        'https://canastarosa.s3.amazonaws.com/media/market/store/photo/9442c0f420006300481ec43a2be3eb49.jpeg',
      big:
        'https://canastarosa.s3.amazonaws.com/media/market/store/photo/9442c0f420006300481ec43a2be3eb49.jpeg',
      small:
        'https://canastarosa.s3.amazonaws.com/media/market/store/photo/9442c0f420006300481ec43a2be3eb49.jpeg',
    },
    cover: {
      medium:
        'https://canastarosa.s3.amazonaws.com/media/market/store/cover/6018f69272a1a3e85f0c2513e7382401.jpeg',
      small:
        'https://canastarosa.s3.amazonaws.com/media/market/store/cover/6018f69272a1a3e85f0c2513e7382401.jpeg',
      original:
        'https://canastarosa.s3.amazonaws.com/media/market/store/cover/6018f69272a1a3e85f0c2513e7382401.jpeg',
      big:
        'https://canastarosa.s3.amazonaws.com/media/market/store/cover/6018f69272a1a3e85f0c2513e7382401.jpeg',
      thumbnail:
        'https://canastarosa.s3.amazonaws.com/media/market/store/cover/6018f69272a1a3e85f0c2513e7382401.jpeg',
    },
    slogan: 'Te hace bien',
    mean_store_score: 5.0,
    shipping_methods: [],
  },
  {
    name: 'Jozz Love',
    slug: 'jozz-love',
    photo: {
      original:
        'https://canastarosa.s3.amazonaws.com/media/market/store/photo/a6a2c71fba231bf78b98bb697a7c4b71.jpg',
      medium:
        'https://canastarosa.s3.amazonaws.com/media/market/store/photo/a6a2c71fba231bf78b98bb697a7c4b71.jpg',
      big:
        'https://canastarosa.s3.amazonaws.com/media/market/store/photo/a6a2c71fba231bf78b98bb697a7c4b71.jpg',
      small:
        'https://canastarosa.s3.amazonaws.com/media/market/store/photo/a6a2c71fba231bf78b98bb697a7c4b71.jpg',
    },
    cover: {
      medium:
        'https://canastarosa.s3.amazonaws.com/media/market/store/cover/14f9f297732b232574d3ec6f121ae5a6.png',
      small:
        'https://canastarosa.s3.amazonaws.com/media/market/store/cover/14f9f297732b232574d3ec6f121ae5a6.png',
      original:
        'https://canastarosa.s3.amazonaws.com/media/market/store/cover/14f9f297732b232574d3ec6f121ae5a6.png',
      big:
        'https://canastarosa.s3.amazonaws.com/media/market/store/cover/14f9f297732b232574d3ec6f121ae5a6.png',
      thumbnail:
        'https://canastarosa.s3.amazonaws.com/media/market/store/cover/14f9f297732b232574d3ec6f121ae5a6.png',
    },
    slogan: 'Moda que enamora',
    mean_store_score: 5.0,
    shipping_methods: [],
  },
  {
    name: 'Sivillanerias',
    slug: 'sivillanerias',
    photo: {
      original:
        'https://canastarosa.s3.amazonaws.com/media/market/store/photo/c2ae2a4236a637c81e87755ee4fc8a49.JPG',
      medium:
        'https://canastarosa.s3.amazonaws.com/media/market/store/photo/c2ae2a4236a637c81e87755ee4fc8a49.JPG',
      big:
        'https://canastarosa.s3.amazonaws.com/media/market/store/photo/c2ae2a4236a637c81e87755ee4fc8a49.JPG',
      small:
        'https://canastarosa.s3.amazonaws.com/media/market/store/photo/c2ae2a4236a637c81e87755ee4fc8a49.JPG',
    },
    cover: {
      medium:
        'https://canastarosa.s3.amazonaws.com/media/market/store/cover/5a5a630a023b1fce712940eb413d0cb8.jpg',
      small:
        'https://canastarosa.s3.amazonaws.com/media/market/store/cover/5a5a630a023b1fce712940eb413d0cb8.jpg',
      original:
        'https://canastarosa.s3.amazonaws.com/media/market/store/cover/5a5a630a023b1fce712940eb413d0cb8.jpg',
      big:
        'https://canastarosa.s3.amazonaws.com/media/market/store/cover/5a5a630a023b1fce712940eb413d0cb8.jpg',
      thumbnail:
        'https://canastarosa.s3.amazonaws.com/media/market/store/cover/5a5a630a023b1fce712940eb413d0cb8.jpg',
    },
    slogan: 'Amor a la comida',
    mean_store_score: 5.0,
    shipping_methods: [],
  },
  {
    name: 'El huerto de Pau',
    slug: 'el-huerto-de-pau',
    discount: '30.00',
    slogan: 'Amor a la comida',
    photo: {
      medium:
        'https://canastarosa.s3.amazonaws.com/media/market/store/photo/c09674252fc720d7d1a4b621dfcfff46.jpeg',
      small:
        'https://canastarosa.s3.amazonaws.com/media/market/store/photo/c09674252fc720d7d1a4b621dfcfff46.jpeg',
      big:
        'https://canastarosa.s3.amazonaws.com/media/market/store/photo/c09674252fc720d7d1a4b621dfcfff46.jpeg',
      original:
        'https://canastarosa.s3.amazonaws.com/media/market/store/photo/c09674252fc720d7d1a4b621dfcfff46.jpeg',
    },
    cover: {
      big:
        'https://canastarosa.s3.amazonaws.com/media/market/store/cover/6d471ab7021c1da30f5fa3540f7048fd.jpeg',
      original:
        'https://canastarosa.s3.amazonaws.com/media/market/store/cover/6d471ab7021c1da30f5fa3540f7048fd.jpeg',
      small:
        'https://canastarosa.s3.amazonaws.com/media/market/store/cover/6d471ab7021c1da30f5fa3540f7048fd.jpeg',
      medium:
        'https://canastarosa.s3.amazonaws.com/media/market/store/cover/6d471ab7021c1da30f5fa3540f7048fd.jpeg',
      thumbnail:
        'https://canastarosa.s3.amazonaws.com/media/market/store/cover/6d471ab7021c1da30f5fa3540f7048fd.jpeg',
    },
  },
];
