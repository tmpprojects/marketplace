import React, { Component } from 'react';
import { Link, NavLink } from 'react-router-dom';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import queryString from 'query-string';
import Slider from 'react-slick';

import { insertKeyToQueryParam } from '../../Utils/queryParamsUtils';
import { appActions, analyticsActions } from '../../Actions';
import PageHead from '../../Utils/PageHead';
import { IconPreloader } from '../../Utils/Preloaders';
import { ResponsiveImageFromURL } from '../../Utils/ImageComponents';
import CategoriesPopular from '../../Utils/CategoriesPopular';
import { ProductsList } from '../../components/Visa/ProductListCategory';
import withPagination from '../../components/hocs/withPagination';
import { SideBarFilters } from '../../components/Categories/SideBarFilters';
import { SIDEBAR_FILTERS_FORM_CONFIG } from '../../Utils/SideBarFilters/SideBarFiltersConfig';
import { trackWithGTM } from '../../Utils/trackingUtils';
import { list } from '../Visa/categorieList';

import 'slick-carousel/slick/slick.scss';
import 'slick-carousel/slick/slick-theme.scss';
import '../../../styles/_categorySearchResults.scss';

const MAX_ITEMS_PER_PAGE = 24;
const ENVIO_NACIONAL_SLUG = 'envio-nacional';

/**Slick settings */
const settings = {
  infinite: true,
  swipeToSlide: true,
  arrows: process.env.CLIENT ? true : false,
  speed: 500,
  slidesToShow: 6,
  slidesToScroll: 6,
  autoplay: true,
  autoplaySpeed: 7000,
  lazyLoad: process.env.CLIENT ? 'progressive' : null,
  pauseOnHover: true,
  cssEase: 'ease',
  responsive: [
    {
      breakpoint: 2000,
      settings: {
        slidesToShow: 5.99,
        slidesToScroll: 6,
      },
    },
    {
      breakpoint: 1200,
      settings: {
        slidesToShow: 5,
        slidesToScroll: 4,
        arrows: false,
      },
    },
  ],
};
/**
 * SORTING LIST COMPONENT
 * Renders a SelectInput with different sorting parameters
 */
const SortingList = (props) => {
  const onChange = function (e) {
    props.onSortChange(e.target.value);
  };

  return null;
  return (
    <div className="filters">
      <p>Ordenar por:</p>
      <div className="filters__list">
        <select name="filters" onChange={onChange} value={props.sort}>
          <option value="-created">Fecha</option>
          <option value="price">Precio</option>
          <option value="name">Nombre</option>
        </select>
      </div>
    </div>
  );
};

/**
 * ProductsList Component (with pagination)
 * Wraps a list of products within 'withPagination' HOC,
 * and displays a paginated component.
 */
const PaginatedList = withPagination(ProductsList);

/**
 * CategoryPage Class Component
 */
export class CategoryPage extends Component {
  constructor(props) {
    super(props);
    this.defaultSort = '';
    this.currentPage = this.getResultsPageFromQueryString(
      this.props.location.search,
    );
    this.selectedStore = null;
    //To populate the SidebarFilters form if the url contains filters
    this.urlValues = this.getURLVals(this.props.location.search);

    this.state = {
      sortCriteria: this.defaultSort,
      isOpen: false,
      isMobile: false,
    };
  }

  /**
   * React Life Cycle Methods
   */
  componentDidMount() {
    // Perform the search
    this.performSeach(
      this.props.match.params.categoryName,
      this.currentPage,
      this.getFiltersFromQueryString(this.props.location.search),
      this.getSortFromQueryString(this.props.location.search),
    );

    // Window resize
    this.onResizeHandler();
    window.addEventListener('resize', this.onResizeHandler);

    // shippingAddressObserver
    // globalThis.shippingAddressObserver.subscribe(this.addZipCodeFilter);
  }

  componentWillReceiveProps(nextProps) {
    // If search query string (Ej. URL ?p={pageNumber}&store={storeSlug}) has changed
    if (this.props.location.search !== nextProps.location.search) {
      this.performSeach(
        nextProps.match.params.categoryName,
        this.getResultsPageFromQueryString(nextProps.location.search),
        this.getFiltersFromQueryString(nextProps.location.search),
        this.getSortFromQueryString(nextProps.location.search),
      );
    }

    // If pathname (category name) has changed, perform a new search
    if (this.props.location.pathname !== nextProps.location.pathname) {
      this.performSeach(nextProps.match.params.categoryName);
    }

    //Criteo tracking
    if (this.props.searchResults !== nextProps.searchResults) {
      const products = nextProps.searchResults.results;
      if (products !== undefined) {
        const idProducts = products
          .slice(0, 3)
          .map((product) => product.id.toString());
        trackWithGTM('ListingPage_Criteo', idProducts);

        const formattedProducts = products.map((prod, index) => ({
          product: prod.slug,
          index,
        }));
        this.props.trackListingImpressions('category-page', formattedProducts);
      }
    }
  }
  componentWillUnmount() {
    // shippingAddressObserver
    // globalThis.shippingAddressObserver.unsubscribe(this.addZipCodeFilter);

    // Resize event
    window.removeEventListener('resize', this.onResizeHandler);
  }

  getURLVals = (query) => {
    const queryParams = this.getQueryParamsAsObject(query.replace('?', ''));

    return Object.keys(queryParams).reduce((a, key) => {
      const ifv = a;

      if (key === 'delivery_date') {
        ifv.delivery_day = 'picked';
        //Only for the option where user selects an specific date, for calendar
        ifv.delivery_day_picked = queryParams[key];
      }

      if (key === 'zone') {
        ifv.shipping = queryParams[key];
      }

      if (key === 'max_price') {
        ifv.price = `max_price_${queryParams[key]}`;
      }

      if (key === 'stores') {
        //Turn stores param into array of Slug Stores
        const storesArray = queryParams[key].replace('+', ' ').split(' ');
        //Array of Selected Stores
        const storesSelected = storesArray.map((s) => {
          const store = this.props.storesList.results.find((st) => st.slug === s);
          //Add 'selected' key so the store appears checked
          return {
            ...store,
            selected: true,
          };
        });

        //Array of Not Selected Stores
        let storesNotSelected = this.props.storesList.results;
        storesSelected.forEach((sl) => {
          storesNotSelected = storesNotSelected.filter((s) => s.slug !== sl.slug);
        });

        ifv.stores = storesSelected.concat(storesNotSelected);
      }
      return ifv;
    }, {});
  };

  /**
   * onResizeHandler()
   * Listen for device resize and updates component state.
   */
  onResizeHandler = () => {
    let isMobile = false;
    const w =
      window.innerWidth ||
      document.documentElement.clientWidth ||
      document.body.clientWidth;
    if (w < 727) {
      isMobile = true;
    }
    this.setState({
      isMobile,
    });
  };
  /**
   * onSortChange()
   * @param {string} sortBy | Sorting discount criteria. HOTSALE
   */
  onSortDiscount = (from, to) =>
    this.props.history.push(
      `${
        this.props.location.pathname
      }?has_discount=true&percentage_discount_from=${from}&percentage_discount_to=${to}${this.getStoreFromQueryString(
        this.props.location.search,
      )}`,
    );

  /**
   * onSortChange()
   * @param {string} sortBy | Sorting criteria.
   */
  onSortChange = (sortBy) =>
    this.props.history.push(
      `${
        this.props.location.pathname
      }?sort=${sortBy}&store=${this.getStoreFromQueryString(
        this.props.location.search,
      )}`,
    );

  /**
   * onFilterSubmit()
   * @param {string} query | Filters querystring.
   */
  onFilterSubmit = (query) => {
    this.props.history.push(`${this.props.location.pathname}?${query}`);
  };

  /**
   * addZipCodeFilter()
   * Adds zip code filter to current queryparams string
   * @param {string} zipCode | Query string to send to API
   */
  // addZipCodeFilter = (zipCode) => {
  //   this.onFilterSubmit(
  //     insertKeyToQueryParam('order_zipcode', zipCode, document.location.search)
  //   );
  // };

  getQueryParamsAsObject = (query = '') => {
    const queryArray = query.split('&');

    return queryArray.reduce((a, b) => {
      const tempAttribute = a;
      const keyValueArr = b.split('=');
      if (keyValueArr.length === 2) {
        tempAttribute[keyValueArr[0]] = keyValueArr[1];
      }
      return tempAttribute;
    }, {});
  };

  /**
   * getResultsPageFromQueryString()
   * @param {string} query
   * Extracts the page number and returns it as an int number.
   */
  getResultsPageFromQueryString = (query) => {
    if (query) {
      const resultsPage = parseInt(queryString.parse(query).p, 10);
      return !isNaN(resultsPage) ? resultsPage : 1;
    }
    return 1;
  };

  /**
   * getResultsPageFromQueryString()
   * @param {string} query
   * Extracts and return the store slug from a query string.
   */
  getStoreFromQueryString = (query) => {
    if (query && queryString.parse(query).store) {
      return queryString.parse(query).store;
    }
    return '';
  };

  /**
   * getSortFromQueryString()
   * @param {string} query
   * Extracts and return sort criteria from a query string.
   */
  getSortFromQueryString = (query) => {
    if (query && queryString.parse(query).sort) {
      return queryString.parse(query).sort;
    }
    return '';
  };

  /**
   * getFiltersFromQueryString()
   * Extracts filters portion from a query string.
   */
  getFiltersFromQueryString = (query = '') => {
    const queryParams = this.getQueryParamsAsObject(query.replace('?', ''));
    return Object.keys(queryParams).reduce((acc, key) => {
      let qs = acc;
      if (key !== 'sort' && key !== 'p' && key !== 'store') {
        qs += `&${key}=${queryParams[key]}`;
      }
      return qs;
    }, '');
  };

  /**
   * openFiltersWindow()
   * Update component state to open filters container.
   */
  openFiltersWindow = (toggle) => {
    this.setState({
      isOpen: toggle !== undefined ? toggle : !this.state.isOpen,
    });
  };

  /**
   * fetchContent()
   * Retrieves results from the API. Call redux actions.
   * @param {string} query | Query string to send to API
   */
  fetchContent = (query, slug) =>
    Promise.all([
      this.props.getCategoryResults(query.categoryQuery, slug),
      this.props.getStoresList(query.storesQuery, slug),
    ]);

  /**
   * performSeach()
   * Constructs a search query string to perform the search.
   * @param {string} categoryName | Category Slug
   * @param {int} page | Page index
   * @param {string} filters | Filters querystring portion
   * @param {string} sort | Sorting 'slug'
   * @param {int} itemsPerPage | Maximum number of items per page
   */
  performSeach = (
    categoryName,
    page = 1,
    filters = '',
    sortType = this.defaultSort,
    itemsPerPage = MAX_ITEMS_PER_PAGE,
  ) => {
    // Scroll to top of page
    // TODO: Scroll to top of results list instead of top of document.
    window.scrollTo(0, 0);

    // Construct search query parameters
    const searchQueries = buildQuery(
      {
        filters,
        page,
        slug: categoryName,
        pageSize: itemsPerPage,
        sort: sortType,
      },
      {
        slug: categoryName,
        pageSize: 21,
      },
    );

    // Make the call to the API to get results
    this.sortCriteria = sortType;
    // const store = this.getStoreFromQueryString(filters);
    // this.selectedStore = store
    //     ? this.props.storesList.stores.find(s => s.slug === store)
    //     : null;
    this.selectedStore = '';
    this.currentPage = parseInt(page, 10);
    this.fetchContent(searchQueries, categoryName);
  };

  /**
   * findCategoryDetails()
   * @param {array} categories | List of categories.
   * @param {string} categorySlug | Category slug to match.
   * @returns {object} Category Object
   */
  findCategoryDetails = (categories, categorySlug) =>
    categories.reduce((a, b) => {
      // If we´ve found category details, bubble it up.
      if (a) {
        return a;
      }

      // Return currentItem if it matches categorySlug
      let cat = null;
      if (b.slug === categorySlug) {
        return b;
      }

      // Else, use this function recursevly with category children.
      return this.findCategoryDetails(b.children, categorySlug);
    }, null);

  /**
   * createBreadcrumbsTree()
   * Returns an array with the marching categories path.
   * @param {array} categories | Array of nested categories to search from.
   * @param {string} targetCategorySlug | Category String to search.
   * @param {bool} found | Indicates if category has been already found.
   * @param {array} parentPath | Array with the parent categories.
   * @returns {array} An array of categories (Model example: [{name, slug}, ...]).
   */
  createBreadcrumbsTree = (
    categories,
    targetCategorySlug,
    found = false,
    parentPath = [],
  ) => {
    const tester = categories.reduce(
      (a, b) => {
        let result = a;

        // If category is not found, proceed to recursive loop.
        if (!result.found) {
          // If we've found the matching category, return current result
          if (b.slug === targetCategorySlug) {
            result = {
              path: [...result.path, { name: b.name, slug: b.slug }],
              found: true,
            };

            // else, search whithin current category children.
          } else if (b.children.length) {
            const innerCategoryPath = this.createBreadcrumbsTree(
              b.children,
              targetCategorySlug,
              false,
              [...result.path, { name: b.name, slug: b.slug }],
            );

            if (innerCategoryPath.found) {
              result = innerCategoryPath;
            }
          }
        }

        // Return current path.
        return result;
      },
      { found, path: parentPath },
    );

    return tester;
  };

  /**
   * render()
   */
  render() {
    const { marketCategories = [], searchResults, match, location } = this.props;
    let { storesList = {} } = this.props;
    const currentPage = this.currentPage;
    const urlValues = this.urlValues;
    const categoryName = match.params.categoryName;
    const { selectedStore } = this;
    const { sortCriteria, isMobile, isOpen } = this.state;
    console.log(this.props, 'props cateegory');
    // Sort stores list alphabetically
    storesList = {
      ...storesList,
      stores: storesList.results.sort((a, b) => {
        if (a.name < b.name) {
          return -1;
        }
        if (a.name > b.name) {
          return 1;
        }
        return 0;
      }),
    };

    // Find Category tree for breadcrumbs.
    const breadcrumbsPath = this.createBreadcrumbsTree(
      marketCategories,
      categoryName,
    ).path;

    // Filters sidebar component.
    const renderSideBar = (
      <SideBarFilters
        stores={storesList}
        selectedStore={selectedStore}
        searchResults={searchResults.results}
        isMobile={isMobile}
        close={this.openFiltersWindow}
        location={location}
        onFilterSubmit={this.onFilterSubmit}
        urlValues={urlValues}
        formName={SIDEBAR_FILTERS_FORM_CONFIG.formName}
      />
    );

    // Show preloader if searchResults are still loading.
    let loadingContent = false;
    if (searchResults.loading || !searchResults.results) {
      loadingContent = true;
    }

    // Get Category Details.
    let category = { name: '', description: '' };
    if (marketCategories.length) {
      category = {
        ...category,
        ...this.findCategoryDetails(marketCategories, categoryName),
      };
    }

    // This is a PATCH.
    // This hardcodes category values if
    // the querystring passed on the URL is 'envíos nacionales'.
    // This category is not dynamic.
    if (categoryName === ENVIO_NACIONAL_SLUG) {
      category = {
        name: 'Envío Nacional',
        description: '',
        slug: ENVIO_NACIONAL_SLUG,
        photo: {},
        text: 'Envío Nacional',
        banner: {
          title: 'Envío Nacional',
          seo_tags: '',
        },
      };
    }

    // Render Results
    return (
      <section className="categoryResults">
        <PageHead
          attributes={{
            title: `${
              category.name ? category.name : 'Weekend Sales'
            } - Market | Canasta Rosa`,
          }}
        />

        {/* CATEGORY COVER */}
        <div
          style={{
            width: '100%',
            height: '7em',
            backgroundColor: '#1a1f71',
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
          }}
        >
          <h5 className="cr__text--subtitle2" style={{ color: 'white' }}>
            Descuentos de hasta 40%
          </h5>
        </div>
        {/* END: /CATEGORY COVER */}

        {/**DISCOVER */}
        <div
          className="hotsale-discover category-discovery"
          style={{ paddingTop: '0.5em' }}
        >
          <div className="hotsale-discover-triangle-c-l" />
          <div className="hotsale-discover-triangle-c-r" />

          {!isMobile ? (
            <div
              className="hotsale-slider-container"
              style={{ width: '59em', marginTop: '1em' }}
            >
              <Slider {...settings}>
                {list.map(({ name, slug, img, key }) => (
                  <div className="hotsale-slider-container-item" key={key}>
                    <Link to={`/visa/${slug}/?has_discount=true`}>
                      <img src={img} alt={name} />
                      <h6>{name}</h6>
                    </Link>
                  </div>
                ))}
              </Slider>
            </div>
          ) : (
            <div className="hotsale-slider-container slider-mobile-category">
              {list.map(({ name, slug, img }) => (
                <div className="hotsale-slider-container-item">
                  <Link to={`/visa/${slug}/?has_discount=true`}>
                    <img src={img} alt={name} />
                    <h6>{name}</h6>
                  </Link>
                </div>
              ))}
            </div>
          )}

          <div className="hotsale-discover-offers category-offers">
            <ul className="hotsale-discover-offers-list">
              <li className="hotsale-discover-offers-list-item">
                <a
                  className="cr__text--caption button buttonVisa"
                  onClick={() => this.onSortDiscount(0, 20)}
                >
                  20%
                </a>
              </li>
              <li className="hotsale-discover-offers-list-item">
                <a
                  className="cr__text--caption button buttonVisa"
                  onClick={() => this.onSortDiscount(25, 29)}
                >
                  25%
                </a>
              </li>
              <li className="hotsale-discover-offers-list-item">
                <a
                  className="cr__text--caption button buttonVisa"
                  onClick={() => this.onSortDiscount(30, 34)}
                >
                  30%
                </a>
              </li>
              {/* <li className="hotsale-discover-offers-list-item">
                <a
                  className="cr__text--caption button buttonVisa"
                  onClick={() => this.onSortDiscount(35, 39)}
                >
                  35%
                </a>
              </li> */}
              <li className="hotsale-discover-offers-list-item">
                <a
                  className="cr__text--caption button buttonVisa"
                  onClick={() => this.onSortDiscount(40, 49)}
                >
                  40%
                </a>
              </li>
              {/* <li className="hotsale-discover-offers-list-item">
                <a
                  className="cr__text--caption button buttonVisa"
                  onClick={() => this.onSortDiscount(50, 100)}
                >
                  50%
                </a>
              </li> */}
            </ul>
          </div>
        </div>
        {/**DISCOVER END */}
        <div className="results_list">
          <div className="wrapper--center">
            {/* <SeasonBanner /> */}
            {/* BREADCRUMBS */}
            {breadcrumbsPath.length > 0 && (
              <nav className="breadcrumbs" style={{ marginTop: '1em' }}>
                <ul className="breadcrumbs__list">
                  <li className="category">
                    <NavLink to="/" className="category__link">
                      Home
                    </NavLink>
                  </li>
                  {breadcrumbsPath.map((c) => (
                    <li className="category" key={c.slug}>
                      <NavLink
                        to={`/visa/${c.slug}/`}
                        className="category__link"
                        activeClassName="category__link--active"
                      >
                        {c.name}
                      </NavLink>
                    </li>
                  ))}
                </ul>
                &nbsp;
                <span className="breadcrumbs__counter">
                  ({searchResults.count} producto
                  {searchResults.count !== 1 ? 's' : ''})
                </span>
              </nav>
            )}
            {/* END: BREADCRUMBS */}

            <div className="results_list-container">
              {!isMobile && renderSideBar}

              {loadingContent ? (
                <IconPreloader />
              ) : (
                <div className="productsList">
                  {
                    /* STORE COVER */
                    selectedStore && (
                      <div className="store-summary">
                        <div
                          className="store-summary__cover"
                          style={{
                            backgroundImage: `url(${selectedStore.cover.medium})`,
                          }}
                        />
                        <div className="store-summary__info">
                          <div className="image">
                            <ResponsiveImageFromURL
                              src={selectedStore.photo.small}
                              alt={selectedStore.name}
                            />
                          </div>
                          <div className="name-wrapper">
                            <h3 className="title">{selectedStore.name}</h3>
                            <h4 className="slogan">{selectedStore.slogan}</h4>
                          </div>
                          <Link
                            className="visit button-square"
                            to={`/stores/${selectedStore.slug}`}
                          >
                            Visitar Tienda
                          </Link>
                        </div>
                      </div>
                    )
                    /* END: /STORE COVER */
                  }

                  {/* TITLE & SORTING CONTAINER */}
                  {/* <div className='title_container'>
                    <h5 className='title'>
                      <span>{searchResults.count}&nbsp;</span>
                      resultado{searchResults.results.length !== 1 ? 's' : ''}
                    </h5>
                    {isOpen && (
                      <div className={`addToCart ${isOpen ? 'open' : 'close'}`}>
                        {renderSideBar}
                      </div>
                    )}

                    <SortingList
                      sort={sortCriteria}
                      store={selectedStore}
                      onSortChange={this.onSortChange}
                    />
                    {isMobile && (
                      <a
                        className='icon_filter'
                        onClick={() => this.openFiltersWindow(true)}
                      >
                        Filtros
                      </a>
                    )}
                  </div> */}
                  {/* END: /TITLE & SORTING CONTAINER */}

                  {/* PAGINATED LIST */}
                  <PaginatedList
                    items={searchResults.results}
                    baseLocation={`/visa/${
                      category.slug ? category.slug : 'category'
                    }/?has_discount=true&${this.getFiltersFromQueryString(
                      location.search,
                    )}&sort=${this.getSortFromQueryString(
                      location.search,
                    )}&store=${this.getStoreFromQueryString(location.search)}`}
                    page={currentPage}
                    action={this.testingFunction}
                    maxItems={MAX_ITEMS_PER_PAGE}
                    totalPages={searchResults.npages}
                    location={'visa'}
                  />
                  {/* END: /PAGINATED LIST */}
                </div>
              )}
            </div>
          </div>
        </div>

        <CategoriesPopular />
      </section>
    );
  }
}

const buildQuery = (categoryQuery, storesQuery) => {
  if (categoryQuery.slug === ENVIO_NACIONAL_SLUG) {
    return {
      categoryQuery: `?has_discount=true&ordering=${categoryQuery.sort}&page=${categoryQuery.page}&page_size=${categoryQuery.pageSize}&${categoryQuery.filters}`,
      storesQuery: `?page_size=${storesQuery.pageSize}&${categoryQuery.filters}`,
    };
  } else {
    return {
      categoryQuery: `?has_discount=true&ordering=${categoryQuery.sort}&${categoryQuery.filters}&category__slug=${categoryQuery.slug}&page=${categoryQuery.page}&page_size=${categoryQuery.pageSize}`,
      storesQuery: `?products__category__slug=${storesQuery.slug}&page_size=${storesQuery.pageSize}&${categoryQuery.filters}`,
    };
  }
};

// Load Data for Server Side Rendering
CategoryPage.loadData = (reduxStore, routePath) => {
  const { match, location } = routePath;

  // TODO: How to get URL params with react-router on node side?
  const resultsPage = 1; //location.search ? queryString(location.search).p : 1;
  const searchQueries = buildQuery(
    {
      slug: match.params.categoryName,
      page: resultsPage,
      pageSize: MAX_ITEMS_PER_PAGE,
      filters: '',
      sort: '',
    },
    {
      slug: match.params.categoryName,
      pageSize: 21,
    },
  );

  return Promise.all([
    reduxStore.dispatch(appActions.getCategoryResults(searchQueries.categoryQuery)),
    reduxStore.dispatch(appActions.getStoresList(searchQueries.storesQuery)),
  ]).catch((e) => {
    return false;
  });
};

// Map Redux Props and Actions to component
function mapStateToProps({ app }) {
  return {
    storesList: app.storesList,
    searchResults: app.categoryResults,
    marketCategories: app.marketCategories.categories,
  };
}
function mapDispatchToProps(dispatch) {
  const { getCategoryResults, getStoresList } = appActions;
  const { trackListingImpressions } = analyticsActions;
  return bindActionCreators(
    {
      getStoresList,
      getCategoryResults,
      trackListingImpressions,
    },
    dispatch,
  );
}

// Export Component
export default connect(mapStateToProps, mapDispatchToProps)(CategoryPage);
