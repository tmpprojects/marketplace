import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { renderRoutes } from 'react-router-config';

import Improvements from './Improvements';
import StoreAppPage from './StoreAppPage';
import { Redirect } from 'react-router-dom';

let showImprovements = true;

class ValidateImprovements extends Component {
  render() {
    if (showImprovements) {
      return <Redirect to="/target" />;
    } else {
      return <Improvements />;
    }
  }
}

export default ValidateImprovements;
