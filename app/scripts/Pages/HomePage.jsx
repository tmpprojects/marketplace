import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';

import '../../styles/_homePage.scss';
import { appActions, analyticsActions } from '../Actions';
import SEO from '../../statics/SEO.json';
import PageHead from '../Utils/PageHead';
import FavStores from '../components/Home/FavStores';
import NewStores from '../components/Home/NewStores';
import { AboutUs } from '../components/Inspire/AboutUs';
import { CardsList } from '../components/Home/CardsList';
import { MainBanner } from '../components/Home/MainBanner';
import { ResponsiveImage, ResponsiveImageFromURL } from '../Utils/ImageComponents';
import CardsTrending from '../components/Home/CardsTrending';
import { CreateStore } from '../components/Home/CreateStore';
import { Assistant } from '../Utils/Assistant';
import { trackWithGTM } from '../Utils/trackingUtils';
import AwardsBanner from '../components/Awards/AwardsBanner';
import SeasonBanner from '../Utils/SeasonBanner';
import { Covid19 } from '../../scripts/Utils/modalbox/Covid/Covid19';
import { ImportantMessage } from '../../scripts/Utils/modalbox/ImportantMessage/ImportantMessage';
import { insertKeyToQueryParam } from './../Utils/queryParamsUtils';
import { Banners } from '../components/Home/BannersNLJA';
import HoraRosa from '../components/HoraRosa/HoraRosa.js';

export class HomePage extends Component {
  //hackathon -  remove if necessary
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    // globalThis.shippingAddressObserver.subscribe(this.addZipCodeFilter);
    // Fetch Data on the client side
    if (!this.props.marketBanners.previouslyLoaded) {
      this.props.fetchBanners();
    }
    if (!this.props.storesList.previouslyLoaded) {
      this.props.getStoresList();
    }
    if (!this.props.featuredStores.previouslyLoaded) {
      this.props.getFeaturedStores();
    }
    if (!this.props.featuredProducts.previouslyLoaded) {
      this.props.getFeaturedProducts();
    }
  }
  //  /**
  //  * onFilterSubmit()
  //  * @param {string} query | Filters querystring.
  //  */
  // onFilterSubmit = (query) => {
  //   this.props.history.push(`${this.props.location.pathname}?${query}`);
  // };
  //   /**
  //  * addZipCodeFilter()
  //  * Adds zip code filter to current queryparams string
  //  * @param {string} zipCode | Query string to send to API
  //  */
  // addZipCodeFilter = (zipCode) => {
  //   this.onFilterSubmit(
  //     insertKeyToQueryParam('zipcode', zipCode, document.location.search)
  //   );
  // };

  render() {
    const {
      storesList,
      marketBanners,
      featuredStores,
      featuredProducts,
      trackListingImpressions,
      trackProductClick,
    } = this.props;
    const stores = storesList.results;
    const bannerSlides = marketBanners.banners;
    const featuredStoresArray = featuredStores.stores;
    const trendingItems = featuredProducts.products.slice(0, 4);

    const slides = bannerSlides.map((item, i) => {
      return (
        <li
          className="slide"
          key={i}
          style={{ width: `${100 / bannerSlides.length}%` }}
        >
          <div className="slide__image">
            <Link
              id={`MainBanner_image (${item.title})`}
              className="gtm_link_click"
              to={item.link}
            >
              <ResponsiveImage src={item.photo_desktop} alt={item.title} />
            </Link>
          </div>
          <div className="slide__data">
            <div className="content-wrapper">
              <div className="header">
                <h1 className="title">
                  <Link
                    id={`MainBanner_title (${item.title})`}
                    className="gtm_link_click"
                    to={item.link}
                  >
                    {item.title}
                  </Link>
                </h1>
                <h5>{item.subtitle}</h5>
              </div>
              <p>{item.excerpt}</p>
              {item.link_text && (
                <Link
                  id={`MainBanner_text (${item.title})`}
                  to={item.link}
                  className="shop-button gtm_link_click"
                >
                  {item.link_text}&nbsp;
                </Link>
              )}
            </div>
          </div>
        </li>
      );
    });

    // Display Component
    return (
      <section className="home">
        <HoraRosa location={this.props.location}>
          <MainBanner banners={slides} />
        </HoraRosa>

        {/* <div className="banner-container wrapper--center" > */}
        {/* <Assistant /> */}
        {/* <SeasonBanner /> */}
        {/* </div> */}
        <div className="cr__home">
          {/* <Covid19 /> */}
          {/* <ImportantMessage /> */}

          <Banners />
        </div>
        <CardsTrending
          products={trendingItems}
          store={featuredStoresArray}
          trackListingImpressions={trackListingImpressions}
          trackProductClick={trackProductClick}
        />
        <FavStores stores={featuredStoresArray.slice(0, 3)} />
        <NewStores stores={stores.slice(0, 3)} />
        <CreateStore />
        <AboutUs />
        {/*<CardsList products={yourSelections} title="Selecciones para ti" className="your-selections" />*/}
        {/*<CardsList products={recentlyViewed} title="Vistos recientemente" className="viewed-recently" />*/}
      </section>
    );
  }
}

// Load Data for Server Side Rendering
HomePage.loadData = (store) =>
  Promise.all([
    store.dispatch(appActions.fetchBanners()),
    store.dispatch(appActions.getStoresList()),
    store.dispatch(appActions.getFeaturedStores()),
    store.dispatch(appActions.getFeaturedProducts()),
  ]);

// Map Redux Props and Actions to component
function mapStateToProps({ app }) {
  const { featuredStores, featuredProducts, marketBanners, storesList } = app;

  return {
    storesList,
    marketBanners,
    featuredStores,
    featuredProducts,
  };
}
function mapDispatchToProps(dispatch) {
  const {
    fetchBanners,
    createBitly,
    getStoresList,
    getFeaturedStores,
    getFeaturedProducts,
  } = appActions;

  const { trackListingImpressions, trackProductClick } = analyticsActions;

  return bindActionCreators(
    {
      fetchBanners,
      createBitly,
      getStoresList,
      getFeaturedStores,
      getFeaturedProducts,
      trackListingImpressions,
      trackProductClick,
    },
    dispatch,
  );
}

// Export Component
export default connect(mapStateToProps, mapDispatchToProps)(HomePage);
