import React from 'react';
import { Link, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Field, reduxForm, SubmissionError } from 'redux-form';

import '../../styles/_storeCreate.scss';
import SEO from '../../statics/SEO.json';
import PageHead from '../Utils/PageHead';
import { storeActions } from '../Actions';
import { getUIErrorMessage } from '../Utils/errorUtils';
import requireAuth from '../components/hocs/requireAuth';
import { required } from '../Utils/forms/formValidators';
import { InputField } from '../Utils/forms/formComponents';
import { getUserProfile } from '../Reducers/users.reducer';
import { myStoreErrorTypes } from '../Constants/mystore.constants';
import ErrorFormDisplay from '../../scripts/components/StoreApp/Utils/ErrorFormDisplay/ErrorFormDisplay';

/*
 * Form Validation Function
 * @param {object} values : Form values
 */
const name_validation = (value) =>
  value && value.length < 3 ? 'Escribe un nombre mayor a 3 caracteres.' : undefined;

/*
 * STORE CREATE CLASS
 */
class StoreCreatePage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      name: '',
      terms_accept: false,
      submitted: false,
      myError: '',
      badWords: [],
      errorForm: false,
    };

    /** Bind Scope to class methods */
    this.onInputChange = this.onInputChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
    this.testForm = this.testForm.bind(this);
  }

  onInputChange(event) {
    const { name } = event.target;
    const value =
      event.target.type === 'checkbox' ? event.target.checked : event.target.value;

    /** Update State */

    this.setState({
      [name]: value,
    });
  }

  testForm(formData) {
    this.setState({ errorForm: false });

    const { name } = formData;

    const { list = [] } = this.props?.prohibitedWords;

    // If prohibited words list doesnt have items
    if (list?.length === 0) {
      return this.onSubmit(formData);
    }

    if (list?.length > 0) {
      const dataArray = name.toLowerCase().split(' ');

      const finalList = list?.map((item) => item.toLowerCase());

      const wordsList = [
        ...new Set(
          dataArray.filter((word) => finalList.includes(word.toLowerCase())),
        ),
      ];

      if (wordsList.length > 0) {
        this.setState({
          badWords: wordsList,
          errorForm: true,
        });
      } else {
        this.onSubmit(formData);
      }
    }
  }

  onSubmit(formData) {
    /**  Validate Form */
    if (!formData.terms_accept) {
      /** Assign a new error to local state.
       *  this setState is to fix problems with show error in UI after API call in a redux-form-action
       *  called  "@@redux-form/UPDATE_SYNC_ERRORS"*/
      this.setState({
        myError: 'Debes aceptar nuestros términos y condiciones.',
      });

      /** Throw error according to redux-form */
      throw new SubmissionError({
        _error: 'Debes aceptar nuestros términos y condiciones.',
      });
    }

    /** Fill required fields */
    formData.pickup_address = null;
    formData.work_schedules = [
      { week_day: 0, end: '17:30', start: '08:30' },
      { week_day: 1, end: '17:30', start: '08:30' },
      { week_day: 2, end: '17:30', start: '08:30' },
      { week_day: 3, end: '17:30', start: '08:30' },
      { week_day: 4, end: '17:30', start: '08:30' },
    ];
    formData.shipping_schedules =
      this.props.pickupSchedules.schedules.map((schedule) => schedule.id) || [];

    /** Send data to API */
    return this.props
      .createStore(formData)
      .then((response) => {
        this.props.history.push('/my-store/settings/info');
      })
      .catch((error) => {
        const uiMessage = getUIErrorMessage(error.response, myStoreErrorTypes);

        /** Assign a new error to local state.
         *  this setState is to fix problems with show error in UI after API call in a redux-form-action
         *  called  "@@redux-form/UPDATE_SYNC_ERRORS"*/
        this.setState({ myError: uiMessage });

        /** Throw error according to redux-form */
        throw new SubmissionError({
          _error: uiMessage,
        });
      });
  }

  render() {
    const { pristine, submitting, handleSubmit, error, hasStore } = this.props;

    const { badWords, errorForm } = this.state;

    /** If user already has a store
     * Redirect them to their store dashboard */
    if (hasStore) {
      return <Redirect to="/my-store/dashboard" />;
    }

    /** Render Component */

    return (
      <section className="storeCreate">
        <PageHead attributes={SEO.StoreCreatePage} />
        <h3>Abre una Tienda</h3>

        <div className="storeCreate__wrapper">
          <div className="storeCreate__background" />

          <div className="storeCreate__container">
            {submitting === false ? (
              <form
                autoComplete="off"
                className="storeForm"
                onSubmit={handleSubmit(this.testForm)}
              >
                <fieldset>
                  <Field
                    autoFocus
                    id="storeName"
                    name="name"
                    className="storeName"
                    label="Nombre de tu tienda"
                    type="text"
                    validate={[required, name_validation]}
                    component={InputField}
                  />

                  <ErrorFormDisplay errorForm={errorForm} type={badWords} />

                  <div className="form__data form__data-terms">
                    <Field
                      type="checkbox"
                      id="terms_chk"
                      name="terms_accept"
                      component="input"
                    />

                    <p>
                      Al crear una tienda en Canasta Rosa aceptas nuestros
                      <Link to="/legales/terminos-condiciones">
                        T&eacute;rminos y Condiciones
                      </Link>{' '}
                      y<Link to="/legales/privacidad"> Aviso de Privacidad</Link>.
                    </p>
                  </div>

                  {/* Conditional rendering using error thrown by redux-fom 
                  and also by using local state error "myError" */}
                  {error !== undefined ? (
                    <div className="form_status status--error status--main">
                      {error}
                    </div>
                  ) : null}

                  {error === undefined && this.state.myError !== '' ? (
                    <div className="form_status status--error status--main">
                      {this.state.myError}
                    </div>
                  ) : null}

                  <div className="form__data">
                    <input
                      type="submit"
                      id="submit_btn"
                      name="name"
                      value="Abrir Tienda"
                      disabled={pristine || submitting}
                    />
                  </div>
                </fieldset>
              </form>
            ) : (
              <div>Enviando Solicitud</div>
            )}
          </div>
        </div>
      </section>
    );
  }
}

// Wrap component within reduxForm
StoreCreatePage = reduxForm({
  form: 'myStoreCreate_form',
})(StoreCreatePage);

// Dispatch Redux Props and State to Props
function mapStateToProps(state) {
  const profile = getUserProfile(state);
  const { pickupSchedules, prohibitedWords } = state.app;
  return {
    hasStore: profile.has_store,
    pickupSchedules,
    prohibitedWords,
  };
}
function mapDispatchToProps(dispatch) {
  const { createStore } = storeActions;
  return bindActionCreators(
    {
      createStore,
    },
    dispatch,
  );
}

//Requires User to be logged in
StoreCreatePage = requireAuth(StoreCreatePage);
export default connect(mapStateToProps, mapDispatchToProps)(StoreCreatePage);
