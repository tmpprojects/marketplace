import React from 'react';
import { renderRoutes } from 'react-router-config';

import '../../styles/_aboutUs.scss';
import SEO from '../../statics/SEO.json';
import PageHead from '../Utils/PageHead';

const AboutUsPage = (props) => (
  <div className="about-us-page">
    <PageHead attributes={SEO.AboutUsPage} />
    {renderRoutes(props.route.routes)}
  </div>
);
export default AboutUsPage;
