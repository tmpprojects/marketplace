import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import {
  ResponsiveImage,
  ResponsiveImageFromURL,
} from '../../Utils/ImageComponents';
import { trackWithGTM } from '../../Utils/trackingUtils';
export default class NewStores extends Component {
  // PropTypes / DefaultProps
  static defaultProps = {
    stores: [],
  };
  static propTypes = {
    stores: PropTypes.array.isRequired,
  };

  render() {
    const { stores } = this.props;
    return (
      <section className="new-shops wrapper--center">
        <h3 className="title--main">Nuevas Tiendas</h3>
        <div className="link_container">
          <Link
            id="Ver más (Nuevas Tiendas)"
            className="gtm_link_click"
            to="/stores/"
          >
            Ver M&aacute;s
          </Link>
        </div>

        <div className="shops">
          <ul className="shops__list">
            {stores.map((shop, index) => {
              //For GTM Tracking
              const store = {
                id: shop.slug,
                name: shop.name,
                position: index,
                //category: 'guides/google-tag-manager/java'
              };

              const onClickGTMTracking = () => {
                trackWithGTM('eec.impressionClick', [store], 'Nuevas Tiendas');
              };
              return (
                <li className="shop" key={index}>
                  <div className="shop__brand">
                    <Link
                      onClick={onClickGTMTracking}
                      className="profile-photo"
                      to={`/stores/${shop.slug}/`}
                    >
                      <ResponsiveImageFromURL
                        src={shop.photo.small}
                        alt={shop.name}
                      />
                    </Link>
                    <div className="name">
                      <Link
                        onClick={onClickGTMTracking}
                        className="name__link"
                        to={`/stores/${shop.slug}/`}
                      >
                        {shop.name}
                      </Link>
                      <span className="name__description">{shop.slogan}</span>
                    </div>
                  </div>

                  <div className="shop__image">
                    <Link
                      onClick={onClickGTMTracking}
                      className="image-link"
                      to={`/stores/${shop.slug}/`}
                    >
                      {shop.cover.small && (
                        <ResponsiveImageFromURL
                          src={shop.cover.small}
                          alt={shop.name}
                        />
                      )}
                    </Link>
                  </div>

                  {/* <div className="shop__info">
                                            <p>Invitaciones personalizadas para eventos infantiles</p>
                                        </div> */}
                </li>
              );
            })}
          </ul>
        </div>
      </section>
    );
  }
}
