import React from 'react';
import { Link } from 'react-router-dom';
import { trackWithGTM } from '../../Utils/trackingUtils';

const webNL = require('../../../images/banners/web-mty.jpg');
const webJA = require('../../../images/banners/web-gdl.jpg');
const mobileNL = require('../../../images/banners/mobile-mty.jpg');
const mobileJA = require('../../../images/banners/mobile-gdl.jpg');

function BannersNLJA() {
  const onGTMTracking = (value = '', index = 0) => {
    const data = {
      id: index,
      name: value,
      position: index,
    };
    trackWithGTM('eec.impressionClick', [data], 'BannerHome');
  };

  return (
    <div className="cr__home-banners">
      <Link
        to="/landing/activacion-mty"
        className="cr__home-banners-nl"
        onClick={() => onGTMTracking('Monterrey', 1)}
      >
        <div className="cr__home-banners-nl-img">
          <img src={webNL} alt="Nuevo León" loading="lazy" />
        </div>
        <div className="cr__home-banners-nl-imgMobile">
          <img src={mobileNL} alt="Nuevo León" loading="lazy" />
        </div>
        <div className="cr__home-banners-nl-container">
          <div className="cr__home-banners-nl-text">
            <h5 className="cr__textColor--colorWhite">¡Llegamos a Monterrey!</h5>
            <h6 className="cr__textColor--colorWhite cr__text--caption">
              Ahora los mejores productos en territorio regio.
            </h6>
          </div>
        </div>
      </Link>
      <Link
        to="/landing/stores-jalisco"
        className="cr__home-banners-ja"
        onClick={() => onGTMTracking('Guadalajara', 2)}
      >
        <div className="cr__home-banners-ja-img">
          <img src={webJA} alt="Jalisco" loading="lazy" />
        </div>
        <div className="cr__home-banners-ja-imgMobile">
          <img src={mobileJA} alt="Jalisco" loading="lazy" />
        </div>
        <div className="cr__home-banners-nl-container">
          <div className="cr__home-banners-ja-text">
            <h5>¡Hola Guadalajara!</h5>
            <h6 className="cr__text--caption">
              Descubre increíbles productos en la perla tapatía.
            </h6>
          </div>
        </div>
      </Link>
    </div>
  );
}

export const Banners = React.memo(BannersNLJA);
