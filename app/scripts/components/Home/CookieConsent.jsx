import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import './cookieConsent.scss';

export default function CookieConsent() {
  const [acceptCookie, setAcceptCookie] = useState(null);
  const [show, setShow] = useState(false);
  useEffect(() => {
    const consent = window.localStorage.getItem('CRCookieConsent');
    if (consent) setShow(false);
    else setShow(true);
  }, [acceptCookie]);

  const weUseCookies = () => {
    window.localStorage.setItem('CRCookieConsent', true);
    setAcceptCookie(true);
    setShow(false);
  };

  return (
    <div className={`cr__cookieConsent ${!show ? 'user' : ''}`}>
      {show ? (
        <div className="cr__cookieConsent-content">
          <span className="cr__cookieConsent-content-title">
            <div className="cr__text--paragraph cr__textColor--colorWhite">
              Utilizamos cookies para brindarte la mejor experiencia.{' '}
              <span className="cr__cookieConsent--link">
                <Link
                  to="/legales/terminos-condiciones"
                  className="cr__textColor--colorGray200"
                >
                  Da click aquí para conocer nuestra política de cookies.
                </Link>
              </span>
            </div>

            <div className="button-container">
              <button onClick={() => weUseCookies()}>Aceptar</button>
            </div>
          </span>
        </div>
      ) : null}
    </div>
  );
}
