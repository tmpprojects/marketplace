import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import { ResponsiveImageFromURL } from '../../Utils/ImageComponents';
import { trackWithGTM } from '../../Utils/trackingUtils';

export default class FavStores extends Component {
  // PropTypes / DefaultProps
  static defaultProps = {
    stores: [],
  };
  static propTypes = {
    stores: PropTypes.array.isRequired,
  };

  render() {
    const { stores } = this.props;

    return (
      <section className="fav_stores">
        <div className="title_container">
          <h2 className="title--main">Nuestras tiendas favoritas</h2>
        </div>

        <ul className="fav_stores-list wrapper--center">
          {stores.map((shop, index) => {
            //For GTM Tracking
            const store = {
              id: shop.store.slug,
              name: shop.store.name,
              position: index,
              //category: 'guides/google-tag-manager/java'
            };
            const onClickGTMTracking = () => {
              trackWithGTM(
                'eec.impressionClick',
                [store],
                'Nuestras Tiendas Favoritas',
              );
            };
            return (
              <li className="store" key={index}>
                <div className="store__wrapper">
                  <div className="store__name">
                    <h3>
                      <Link
                        onClick={onClickGTMTracking}
                        to={`/stores/${shop.store.slug}/`}
                      >
                        {shop.store.name}
                      </Link>
                    </h3>
                  </div>
                  <div className="store__thumbnail">
                    <Link
                      onClick={onClickGTMTracking}
                      to={`/stores/${shop.store.slug}/`}
                      className="image"
                    >
                      <ResponsiveImageFromURL
                        src={shop.photo.small}
                        alt={shop.store.name}
                      />
                    </Link>
                  </div>
                  <div className="store__data">
                    <h3 className="title">
                      <Link
                        onClick={onClickGTMTracking}
                        to={`/stores/${shop.store.slug}/`}
                      >
                        {shop.store.name}
                      </Link>
                    </h3>
                    <div className="call-to-action">
                      <p className="slogan">
                        {shop.store.slogan.slice(0, 1).toUpperCase() +
                          shop.store.slogan.slice(1).toLowerCase()}
                      </p>
                      <Link
                        onClick={onClickGTMTracking}
                        className="visit-store"
                        to={`/stores/${shop.store.slug}/`}
                      >
                        Visitar Tienda
                      </Link>
                    </div>
                  </div>
                </div>
              </li>
            );
          })}
        </ul>
      </section>
    );
  }
}
