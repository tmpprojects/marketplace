import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

class MainBanner extends Component {
  // PropTypes / DefaultProps
  static propTypes = {
    banners: PropTypes.array.isRequired,
  };

  constructor(props) {
    super(props);
    this.state = {
      current_slide: 0,
    };

    this.hasBanners = false;
    this.autoplay_interval = 0;
    this.autoplay_delay = 5000;
    this.moveToSlide = this.moveToSlide.bind(this);
    this.nextSlide = this.nextSlide.bind(this);
    this.prevSlide = this.prevSlide.bind(this);
    this.startTransition = this.startTransition.bind(this);
    this.stopTransition = this.stopTransition.bind(this);
  }
  componentDidMount() {
    const Hammer = typeof window !== 'undefined' ? require('hammerjs') : undefined;

    const myElement = this.slideWrapper.current;
    const mc = new Hammer.Manager(myElement);
    const swipe = new Hammer.Swipe({ direction: Hammer.DIRECTION_HORIZONTAL });

    // Custom options
    mc.add([swipe]);
    mc.on('swipeleft swiperight', (ev) => {
      switch (ev.type) {
        case 'swiperight':
          this.prevSlide();
          break;
        case 'swipeleft':
          this.nextSlide();
          break;
        default:
          break;
      }
    });
  }
  componentWillReceiveProps(nextProps) {
    if (this.props.banners !== nextProps.banners) {
      // Go to init Slide
      this.hasBanners = true;
      this.startTransition();
      this.moveToSlide(0);
    }
  }
  componentWillUnmount() {
    this.stopTransition();
  }

  slideWrapper = React.createRef();
  startTransition() {
    this.stopTransition();
    this.autoplay_interval = setInterval(() => {
      this.nextSlide();
    }, this.autoplay_delay);
  }
  stopTransition() {
    clearInterval(this.autoplay_interval);
  }

  moveToSlide(_target) {
    // Update current index
    this.setState({
      current_slide: _target,
    });

    /* [
            'WebkitTransform',
            'MozTransform',
            'MsTransform',
            'OTransform',
            'transform',
            'msTransform'
        ].forEach((prop) => {
            slider.style[prop] = `${_target * 100}%`;
            //CSSTranslate(`${_target * 100}%`, 'horizontal');
        }); */

    // Auto pplay transition
    this.startTransition();
  }
  nextSlide = () => {
    let { current_slide } = this.state;
    current_slide++;
    if (current_slide > this.props.banners.length - 1) {
      current_slide = 0;
    }
    this.moveToSlide(current_slide);
  };
  prevSlide = () => {
    let { current_slide } = this.state;
    current_slide--;
    if (current_slide < 0) {
      current_slide = this.props.banners.length - 1;
    }
    this.moveToSlide(current_slide);
  };

  render() {
    const { current_slide } = this.state;
    const { banners, className = '' } = this.props;
    // if (process.env.CLIENT) {
    //   console.log('SOY CLIENTE', process.env);
    // } else {
    //   console.log('SOY SERVIDOR');
    // }

    return (
      <section className={`mainBanner ${className}`}>
        <div className="slider">
          <div
            className="slider__wrapper"
            onMouseEnter={this.stopTransition}
            onMouseLeave={this.startTransition}
            ref={this.slideWrapper}
          >
            <ul
              className="slider__list"
              style={{
                width: `${banners.length * 100}%`,
                left: `-${current_slide * 100}%`,
              }}
            >
              {banners}
            </ul>
          </div>

          <nav className="slider__navigation">
            {banners.length > 1 && (
              <a className="prev" onClick={this.prevSlide}>
                Anterior
              </a>
            )}
            {banners.length > 1 && (
              <a className="next" onClick={this.nextSlide}>
                Siguiente
              </a>
            )}
          </nav>
        </div>

        <nav className="bulletsNav">
          <ul className="bulletsNav__bullets">
            {this.props.banners.map((item, i) => (
              <li
                key={i}
                id={`MainBanner_bulletsNav${i}`}
                className={`bullet gtm_click ${i === current_slide ? 'active' : ''}`}
                onClick={() => {
                  this.moveToSlide(i);
                }}
              >
                {i}
              </li>
            ))}
          </ul>
        </nav>
      </section>
    );
  }
}

// Redux Map Functions
function mapStateToProps() {
  return {};
}
function mapDispatchToProps(dispatch) {
  return bindActionCreators({}, dispatch);
}

// Export Connected Component
const connectedComponent = connect(mapStateToProps, mapDispatchToProps)(MainBanner);
export { connectedComponent as MainBanner };
