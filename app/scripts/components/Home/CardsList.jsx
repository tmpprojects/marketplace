import React from 'react';
import { Component } from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';

import { formatNumberToPrice } from '../../Utils/normalizePrice';
import { ResponsiveImageFromURL } from '../../Utils/ImageComponents';

export class CardsList extends Component {
  // PropTypes / DefaultProps
  static defaultProps = {
    products: [],
    title: '',
    customClassName: '',
  };
  static propTypes = {
    products: PropTypes.array.isRequired,
    title: PropTypes.string,
    customClassName: PropTypes.string,
  };

  render() {
    const { products = [], title, className: customClassName } = this.props;

    return (
      <section className={`cards-list wrapper--center ${customClassName || ''}`}>
        <h2 className="title--main">{title}</h2>

        <div className="product-list-container">
          <ul className="products-list">
            {this.props.products.map((item, i) => {
              return (
                <li className="product" key={item.slug}>
                  <div className="product__wrapper">
                    <div className="product__image">
                      <Link to={`/stores/${item.store.slug}/products/${item.slug}`}>
                        {item.photo && (
                          <ResponsiveImageFromURL
                            src={item.photo.small}
                            alt={item.name}
                          />
                        )}
                      </Link>

                      <div className="product__marker">
                        <span className="price">
                          {formatNumberToPrice(item.price)}MX
                        </span>
                      </div>
                    </div>

                    <div className="product__info">
                      <Link
                        className="product-name"
                        to={`/stores/${item.store.slug}/products/${item.slug}`}
                      >
                        {item.name}
                      </Link>
                      <Link
                        id={`${item.store.name} (${item.store.slug})`}
                        className="store-name"
                        to={`/stores/${item.store.slug}`}
                      >
                        {item.store.name}
                      </Link>
                    </div>
                  </div>
                </li>
              );
            })}
          </ul>
        </div>
      </section>
    );
  }
}
