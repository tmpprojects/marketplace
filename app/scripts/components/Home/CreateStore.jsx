import React, { Component } from 'react';

export class CreateStore extends Component {
  render() {
    return (
      <section className="marketplace-benefits">
        <h3 className="title--main">Beneficios de vender en Canasta Rosa</h3>

        <div className="marketplace-benefits__wrapper">
          <div className="marketplace-benefits__background" />

          <div className="slider wrapper--center">
            <div className="slider__move">
              <ul className="slider__container">
                <li className="slide">
                  <div className="slide__image">
                    <img
                      className="icon__profit"
                      src={require('images/aboutUs/sell/icon_logistic.svg')}
                      alt=""
                    />
                  </div>
                  <div className="slide__info">
                    <h5 className="title">Envíos rápidos y confiables</h5>
                    <p className="content">
                      Expande tu negocio y haz que tus creaciones lleguen a todo el
                      país*.
                    </p>
                  </div>
                </li>

                <li className="slide">
                  <div className="slide__image">
                    <img
                      className="icon__profit"
                      src={require('images/aboutUs/sell/icon_users.svg')}
                      alt=""
                    />
                  </div>
                  <div className="slide__info">
                    <h5 className="title">Alcanza miles de usuarios</h5>
                    <p className="content">
                      Expón tu negocio a millones de usuarios y deja que descubran
                      tus productos.
                    </p>
                  </div>
                </li>

                <li className="slide">
                  <div className="slide__image">
                    <img
                      className="icon__profit"
                      src={require('images/aboutUs/sell/icon_assistant.svg')}
                      alt=""
                    />
                  </div>
                  <div className="slide__info">
                    <h5 className="title">Atención a Clientes</h5>
                    <p className="content">
                      Ayudamos a tus clientes y damos seguimiento personalizado a tus
                      órdenes de principio a fin.
                    </p>
                  </div>
                </li>

                <li className="slide">
                  <div className="slide__image">
                    <img
                      className="icon__profit"
                      src={require('images/aboutUs/sell/icon_payments.svg')}
                      alt=""
                    />
                  </div>
                  <div className="slide__info">
                    <h5 className="title">Pagos Fáciles y Seguros</h5>
                    <p className="content">
                      Cierra más ventas aceptando todos los métodos de pago de manera
                      rápida y segura.
                    </p>
                  </div>
                </li>
              </ul>
            </div>
          </div>
          {/* <nav className="bulletsList">
                        <ol className="bullets">
                            <li ><a className="active"></a></li>
                        </ol>
                    </nav> */}
        </div>

        {/* <div>
                    <a href="" className="c2a_square">Comienza aquí</a>
                </div> */}
      </section>
    );
  }
}
