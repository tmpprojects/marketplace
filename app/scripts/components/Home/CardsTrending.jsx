import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';

import { ResponsiveImageFromURL } from '../../Utils/ImageComponents';
import { formatNumberToPrice } from '../../Utils/normalizePrice';
import { trackWithGTM } from '../../Utils/trackingUtils';

let dispatchDataLayerInit = false;

export default class CardsTrending extends Component {
  // PropTypes / DefaultProps
  static defaultProps = {
    products: [],
  };
  static propTypes = {
    products: PropTypes.array.isRequired,
  };



  dispatchDataLayer(dataLayerResults = []) {
    if (!dispatchDataLayerInit) {
      // console.log("products-------->", this.props.products)
      try {
        if (dataLayerResults.length > 0) {
          let breadcrumbsPath = null;
          let dataLayerDimensions = ""
          //products
          let dataLayerContent = '[';
          dataLayerResults.map((preItem, index) => {
            let item = preItem.product;
            dataLayerContent += `
       {
        "name": "${globalThis.googleAnalytics.utils.clearLayerText(item?.name)}",
        "id": "${item?.id}",
        "price": "${item?.price}",
        "brand": "${globalThis.googleAnalytics.utils.clearLayerText(item?.store?.name)}",
        "category": ${globalThis.googleAnalytics.utils.checkBreadcrumbs(breadcrumbsPath)},
        ${dataLayerDimensions}
        "variant": null,
        "list": "Home cards",
        "position": ${(index + 1)}
       }${globalThis.googleAnalytics.utils.closeArray(index, dataLayerResults)} 
        `;
          });
          //products

          const dataLayerBreadcrumbs = globalThis.googleAnalytics.utils.checkBreadcrumbs(breadcrumbsPath);
          dataLayerContent = JSON.parse(dataLayerContent);

          //Call productImpression  - event
          globalThis.googleAnalytics.productImpression(dataLayerContent, dataLayerBreadcrumbs);
        }
      } catch (e) {
        console.log("invoke productImpression [CardsTrending.jsx]", e)
      }
      dispatchDataLayerInit = true;
    }
  }
  componentWillUnmount() {
    dispatchDataLayerInit = false;
  }

  componentDidMount() {
    const formattedProducts = this.props.products.map((prod, index) => ({
      product: prod.product.slug,
      index,
    }));
    //Send to API
    this.props.trackListingImpressions('lo-mas-in', formattedProducts);



  }

  render() {
    const { products = [], trackProductClick } = this.props;

    if (products.length) {
      this.dispatchDataLayer(this.props.products);
    }


    return (
      <section className="trending-items">
        <div className=" wrapper--center">
          <h3 className="title--main">Lo m&aacute;s in</h3>
          <ul className="cards_list">
            {products.map((item, i) => {
              //For GTM Tracking
              const product = {
                id: item.product.id.toString(),
                name: item.product.name,
                position: i,
                //category: 'guides/google-tag-manager/java',
                //category: item.category[0]['display'] ? item.category[0]['display'] : ''
              };

              const onClickGTMTracking = () => {
                //trackWithGTM('eec.impressionClick', [product], 'Lo más in');
                //Endpoint tracking
                //trackProductClick('lo-mas-in', item.product.slug);
                globalThis.googleAnalytics.productClick(item.product.id);
              };

              return (
                <li className="card" key={item.product.slug}>
                  <div className="card__image">
                    {item.product.photo && (
                      <Link
                        onClick={onClickGTMTracking}
                        to={`/stores/${item.product.store.slug}/products/${item.product.slug}`}
                      >
                        <ResponsiveImageFromURL
                          src={item.product.photo.small}
                          alt={item.product.name}
                        />
                      </Link>
                    )}
                  </div>

                  <div className="card__info">
                    <div className="wrapper">
                      <Link
                        className="product-link"
                        onClick={onClickGTMTracking}
                        to={`/stores/${item.product.store.slug}/products/${item.product.slug}`}
                      >
                        {item.product.name}
                      </Link>
                      <Link
                        id={`${item.product.store.name} (${item.product.store.slug})`}
                        onClick={onClickGTMTracking}
                        className="store-link"
                        to={`/stores/${item.product.store.slug}`}
                      >
                        {item.product.store.name}
                      </Link>

                      <p className="product__shippingMethod cr__text--caption cr__textColor--colorViolet400">
                        {product?.physical_properties?.shipping_methods?.find(
                          (i) => i === 'express-moto' || i === 'express-car',
                        ) && 'Envío express'}
                        {!product?.physical_properties?.shipping_methods?.find(
                          (i) => i === 'express-moto' || i === 'express-car',
                        ) &&
                          product?.physical_properties?.shipping_methods?.find(
                            (i) => i === 'standard',
                          ) &&
                          'Envío Nacional'}
                      </p>

                      {parseFloat(item.product.discount, 10) > 0 && (
                        <div
                          className="cr__text--paragraph"
                          style={{ color: '#D87041' }}
                        >
                          <span>Antes</span>{' '}
                          <span className="line-through">
                            {formatNumberToPrice(
                              item.product.price_without_discount,
                            )}
                            MX
                          </span>
                        </div>
                      )}
                      <span className="price">
                        {parseFloat(item?.product.discount, 10) > 0
                          ? formatNumberToPrice(item?.product.price)
                          : formatNumberToPrice(
                            item?.product.price_without_discount,
                          )}
                        MX
                      </span>

                      {/* DISCOUNT TAG */}
                      {parseFloat(item.product.discount, 10) > 0 && (
                        <div
                          style={{
                            position: 'absolute',
                            bottom: '8em',
                            right: '0.1em',
                            padding: '0.1em .3em',
                            background: '#1eb592',
                            color: '#ffffff',
                          }}
                        >
                          <span className="cr__text--caption cr__textColor--colorWhite">
                            {`-${parseInt(item.product.discount)}%`}
                          </span>
                        </div>
                      )}
                      {/* DISCOUNT TAG */}
                    </div>
                  </div>
                </li>
              );
            })}
          </ul>
        </div>
      </section>
    );
  }
}
