import React from 'react';
import './LittleLoader.scss';

class LittleLoader extends React.Component {
  render() {
    return (
      <div className="snippet">
        <div className="stage">
          <div className="dot-flashing"></div>
        </div>
      </div>
    );
  }
}
export default LittleLoader;
