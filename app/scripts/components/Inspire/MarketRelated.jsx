import React from 'react';
import { Link } from 'react-router-dom';

export default (props) => (
  <div>
    <h3 className="title--main">Art&iacute;culos Similares en la Tienda</h3>

    <ul className="related-marketplace__list">
      <li className="item">
        <div className="item_wrapper">
          <div className="item__image">
            <a href="#" className="">
              <img
                src={require('images/inspire/slide2.png')}
                alt="Post Thumbnail"
                className="vertical"
              />
            </a>
          </div>

          <div className="item__info">
            <h4>
              <a href="#">10 Originales adornos Navideños de papel</a>
            </h4>
            <a href="#" className="item__info__username">
              @helen_crafts
            </a>
          </div>
        </div>
      </li>
    </ul>
  </div>
);
