import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import { IconPreloader } from '../../Utils/Preloaders';

class Listing extends Component {
  static propTypes = {
    articles: PropTypes.object.isRequired,
    page: PropTypes.number.isRequired,
    title: PropTypes.string.isRequired,
    disablePagination: PropTypes.bool,
  };
  static defaultProps = {
    articles: {},
    page: 1,
    title: 'Artículos Recientes',
    disablePagination: false,
  };

  /**
   * constructor()
   * @param {object} props : Component Props
   */
  constructor(props) {
    super(props);
    this.state = {
      grid_columns: 2,
    };
    this.breakpoints = {
      default: { width: '(min-width: 200px)', columns: 2 },
      mobileSm: { width: '(min-width: 36.25em)', columns: 3 },
      tabletSm: { width: '(min-width: 45.5em)', columns: 4 },
      desktopSm: { width: '(min-width: 60em)', columns: 5 },
    };
    this.mediaQueries = [];

    // Bind scope to methods
    this.loadPage = this.loadPage.bind(this);
    this.updateDimensions = this.updateDimensions.bind(this);
  }
  componentDidMount() {
    // Media queries for grid/masonry layout
    Object.keys(this.breakpoints).forEach((key) => {
      const matchMedia = window.matchMedia(this.breakpoints[key].width);
      matchMedia.name = key;
      matchMedia.meta = this.breakpoints[key];
      this.mediaQueries.push(matchMedia);
      this.updateDimensions(matchMedia);
      matchMedia.addListener(this.updateDimensions);
    });
  }
  componentWillUnmount() {
    // Media queries for grid/masonry layout
    Object.keys(this.breakpoints).forEach((key) => {
      const matchMedia = window.matchMedia(this.breakpoints[key].width);
      matchMedia.removeListener(this.updateDimensions);
    });
  }

  /**
   * updateDimensions()
   * Recalculate the number of masonry grid columns depending on mediaqueries
   * @param {object} e : matchMediaEvent
   */
  updateDimensions(e) {
    const activeQuery = this.mediaQueries.reduce((prev, curr) =>
      curr.matches ? curr : prev && prev.matches ? prev : null,
    );
    const breakpointName = activeQuery ? activeQuery.name : 'default';
    const breakpointSize = activeQuery && activeQuery.meta.width;

    // Update grid columns
    this.setState({ grid_columns: this.breakpoints[breakpointName].columns });
  }

  /**
   * loadPage()
   * Loads a page of search results
   * @param {int} _index : Results Page
   */
  loadPage(_index) {
    this.props.fetchArticles({
      page: _index,
    });
  }

  render() {
    const columns = [];
    const { results: articlesList = [], npages: pages_number } = this.props.articles;
    // Create masonry column grid.
    // The number of columns depend on window´s width.
    for (let i = 0; i < this.state.grid_columns; i++) {
      columns.push(
        <div
          style={{ width: `${100 / this.state.grid_columns}%` }}
          className="post"
          key={i}
          children={[]}
        ></div>,
      );
    }

    // Feed grid columns with articles
    articlesList.forEach((item, i) => {
      // Determine the column where the article will be appended
      const container_index = i % columns.length;
      const container = columns[container_index];

      // Append article
      container.props.children.push(
        <li key={item.slug}>
          <div className="post__wrapper">
            <div className="post__image">
              <Link
                to={`/inspire/article/${item.slug}`}
                className="responsive-container"
              >
                <img src={item.cover_photo.medium} alt={item.title} />
              </Link>
            </div>

            <div className="post__info">
              <h4>
                <Link to={`/inspire/article/${item.slug}`}>{item.title}</Link>
              </h4>
            </div>
          </div>
        </li>,
      );
    });

    // Render Component
    return (
      <section className="posts-grid">
        <h1 className="title title--main">{this.props.title}</h1>
        {!articlesList.length ? (
          <IconPreloader />
        ) : (
          <ul className="posts-grid__list">{columns}</ul>
        )}

        {!this.props.disablePagination &&
          pages_number > 1 &&
          this.props.page < pages_number && (
            <a
              className="posts__loadMore c2a_round"
              onClick={() => this.loadPage(this.props.page + 1)}
            >
              Mostrar M&aacute;s
            </a>
          )}
      </section>
    );
  }
}

// Export Connected Component
export default connect()(Listing);
