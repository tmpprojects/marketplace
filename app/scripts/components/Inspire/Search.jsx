import React from 'react';
import { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Link } from 'react-router-dom';
import { withRouter } from 'react-router-dom';

class Search extends Component {
  constructor(props) {
    super(props);
    this.state = {
      searchTerm: '',
    };
    this.onSearchSubmit = this.onSearchSubmit.bind(this);
    this.onChange = this.onChange.bind(this);
  }
  onChange(e) {
    e.preventDefault();
    const { name, value } = e.target;
    this.setState({
      [name]: value,
    });
  }
  onSearchSubmit(e) {
    e.preventDefault();
    const { searchTerm } = this.state;
    this.props.history.push(`/inspire/search/${searchTerm}`);
  }

  render() {
    const tags_list = this.props.tags.map((item, index) => (
      <li className="category" key={index}>
        <div className="category__wrapper">
          <h3>
            <Link to={`/inspire/tag/${item.name}`}>{item.name}</Link>
          </h3>
          <Link to={`/inspire/tag/${item.name}`} className="category__image">
            <img
              src={require('../../../images/inspire/category_childhood.jpg')}
              alt={item.name}
            />
          </Link>
        </div>
      </li>
    ));

    return (
      <div style={{ marginTop: '1em' }}>
        <section className="search wrapper--center">
          {/* <form onSubmit={this.onSearchSubmit}>
                        <fieldset>
                            <input
                                type="text"
                                name="searchTerm"
                                value={this.state.searchTerm}
                                onChange={this.onChange}
                                placeholder="Busca inspiración, artículos, tutoriales..." />
                        </fieldset>
                    </form> 

                    <div className="categories">
                        <div className="categories__slider">
                            <ul className="categories__list">
                                {tags_list}
                            </ul>
                        </div>
                    </div>*/}
        </section>
      </div>
    );
  }
}

// Redux Map Functions
function mapStateToProps({ inspire }) {
  return {
    tags: inspire.tags.results ? inspire.tags.results.slice(0, 8) : [],
  };
}
function mapDispatchToProps(dispatch) {
  return bindActionCreators({}, dispatch);
}

// Export Component
Search = withRouter(Search);
export default connect(mapStateToProps, mapDispatchToProps)(Search);
