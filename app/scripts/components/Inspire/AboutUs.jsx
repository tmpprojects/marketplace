import React from 'react';
import { Link } from 'react-router-dom';

export const AboutUs = () => {
  return (
    <section className="aboutUs ">
      <div className="aboutUs__illustration" />
      <div className="aboutUs__container">
        <h3 className="title--main">¿Qu&eacute; es Canasta Rosa?</h3>
        <p className="content">
          Canasta Rosa es la plataforma para inspirar, comprar y vender productos
          únicos, locales y hechos a mano por emprendedores.
        </p>
        <Link
          to="/about-us"
          id="Conoce más de nosotros"
          className="button-square--pink link gtm_link_click"
        >
          Conoce más de nosotros
        </Link>
      </div>
    </section>
  );
};
