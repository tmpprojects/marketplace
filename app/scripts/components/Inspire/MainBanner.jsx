import React from 'react';
import { Component } from 'react';
import ReactDOM from 'react-dom';
import { Link } from 'react-router-dom';
import { ResponsiveImage } from '../../Utils/ImageComponents';

class MainBanner extends Component {
  constructor(props) {
    super(props);

    this.current_slide = 0;
    this.moveToSlide = this.moveToSlide.bind(this);
    this.nextSlide = this.nextSlide.bind(this);
    this.prevSlide = this.prevSlide.bind(this);
  }

  componentDidMount() {
    // Go to init Slide
    this.moveToSlide(this.current_slide);
  }

  /**
   * moveToSlide()
   * Moves the slider to a desireed slide position
   * @param {int} _target : Index of desireed slide
   */
  moveToSlide(_target) {
    let slider = ReactDOM.findDOMNode(this.slider);
    let slide = slider.childNodes[_target];

    // Move Slider
    slider.style.left = `-${_target * 100}%`;

    // Resize Slide Container
    // this.sliderContainer.style.height = `${slide.offsetHeight}px`;

    // Update current index
    this.current_slide = _target;

    /* [
            'WebkitTransform',
            'MozTransform',
            'MsTransform',
            'OTransform',
            'transform',
            'msTransform'
        ].forEach((prop) => {
            slider.style[prop] = `${_target * 100}%`;//CSSTranslate(`${_target * 100}%`, 'horizontal');
        }); */
  }

  /**
   * nextSlide()
   * Moves slider to next slide
   */
  nextSlide() {
    this.current_slide++;

    // If current slide is greater than the total of slides
    // return to initial position
    if (this.current_slide > this.props.banners.length - 1) {
      this.current_slide = 0;
    }

    // Move to slide
    this.moveToSlide(this.current_slide);
  }

  /**
   * prevSlide()
   * Moves the slider to a previous position
   */
  prevSlide() {
    this.current_slide--;

    // If current slide is lower than the initial the first slide
    // return to last slide position
    if (this.current_slide < 0) {
      this.current_slide = this.props.banners.length - 1;
    }

    // Move slide
    this.moveToSlide(this.current_slide);
  }

  render() {
    const { banners = [] } = this.props;

    /* const slides = banners.map((item, i) => (
            
            <li className="slide" key={item.id}>
                <div className="slide__data">
                    <h2><Link to={`/inspire/article/${item.slug}`}>{item.title}</Link></h2>
                    <p>{item.text}</p>
                </div>

                <div className="slide__images">
                    <div className="container">
                        <Link to={`/inspire/article/${item.slug}`}><img src={item.cover_photo} alt="{item.title}" className="vertical" /></Link>
                    </div>

                    <div className="container">
                        <div><Link to={`/inspire/article/${item.slug}`}><img src={item.cover_photo} alt="{item.title}" className="vertical" /></Link></div>
                        <div><Link to={`/inspire/article/${item.slug}`}><img src={item.cover_photo} alt="{item.title}" className="vertical" /></Link></div>
                    </div>

                    <div className="container">
                        <Link to={`/inspire/article/${item.slug}`}><img src={item.cover_photo} alt="{item.title}" className="vertical" /></Link>
                    </div>
                </div>
            </li>
        )); */
    const slides = banners.map((item, i) => (
      <li className="slide" key={i}>
        <div className="slide__data">
          <div className="content-wrapper">
            <h1 className="title">
              <Link to={`/inspire/article/${item.slug}`}>{item.title}</Link>
            </h1>
            {/* <p>{item.text}</p> */}
            <p>{item.excerpt}</p>
          </div>
        </div>

        <div className="slide__images">
          <div className="container">
            <Link
              to={`/inspire/article/${item.slug}`}
              className="responsive-container"
            >
              <ResponsiveImage src={item.cover_photo} alt={`${item.title}`} />
            </Link>
            {/*<Link to={`/inspire/article/${item.slug}`} className='responsive-container'>
                            <ResponsiveImage src={item.photo} alt={`${item.title}`} />
                        </Link>*/}
          </div>
        </div>
      </li>
    ));

    return (
      <section className="banner_main wrapper--center">
        <div
          className="slider"
          ref={(sliderContainer) => {
            this.sliderContainer = sliderContainer;
          }}
        >
          <div className="slider__move">
            <ul
              id="inspire-mainBanner"
              className="slider__container"
              style={{ width: `${slides.length * 100}%` }}
              ref={(sliderList) => {
                this.slider = sliderList;
              }}
            >
              {slides}
            </ul>
          </div>

          <nav className="slider_navigation">
            {slides.length > 1 && (
              <a className="prev" onClick={this.prevSlide}>
                Anterior
              </a>
            )}

            {slides.length > 1 && (
              <a className="next" onClick={this.nextSlide}>
                Siguiente
              </a>
            )}
          </nav>
        </div>

        <nav className="bulletsList">
          <ol className="bullets">
            {banners.map((item, i) => (
              <li key={i}>
                <a className="" onClick={() => this.moveToSlide(i)}>
                  {i}
                </a>
              </li>
            ))}
          </ol>
        </nav>
      </section>
    );
  }
}

// Export Connected Component
export default MainBanner;
