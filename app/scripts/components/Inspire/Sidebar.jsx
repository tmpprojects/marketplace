import React from 'react';
import { Component } from 'react';
import { Link } from 'react-router-dom';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import { inspireActions } from '../../Actions';
import Listing from './Listing.jsx';

class Sidebar extends Component {
  // Default Properties
  static propTypes = {
    tags: PropTypes.array.isRequired,
  };
  static defaultProps = {
    tags: [],
  };

  constructor(props) {
    super(props);
  }
  componentDidMount() {
    //this.props.fetchArticles();
  }
  render() {
    // Get Articles Object
    let articlesObject = this.props.articles || {};

    // Randomize Results array
    articlesObject = {
      ...articlesObject,
      results: articlesObject.results
        ? articlesObject.results
            .sort(function () {
              return 0.5 - Math.random();
            })
            .slice(0, 10)
        : [],
    };

    // Return Component
    return (
      <aside className="sidebar">
        {this.props.tags.length > 0 && (
          <div className="sidebar__tags">
            <h3 className="title--main">Tags</h3>

            <ul className="tags">
              {this.props.tags.map((item, i) => (
                <li className="tags__item" key={i}>
                  <Link to={`/inspire/tag/${item}`}>{item}</Link>
                </li>
              ))}
            </ul>
          </div>
        )}

        <Listing
          title="Artículos Relacionados"
          articles={articlesObject}
          disablePagination
        />
      </aside>
    );
  }
}

// Redux Map Functions
function mapStateToProps({ inspire }) {
  return {
    articles: inspire.articles,
  };
}
function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      fetchArticles: inspireActions.fetchArticles,
    },
    dispatch,
  );
}

// Export Connected Component
export default connect(mapStateToProps, mapDispatchToProps)(Sidebar);
