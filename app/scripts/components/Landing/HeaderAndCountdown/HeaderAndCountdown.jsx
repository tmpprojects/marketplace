import React, { Fragment, useState } from 'react';
import LandingCountDown from '../Countdown/LandingCountDown';

const HeaderAndCountDown = ({ paramOfStore, triggerRenderParam, date, text }) => {
  const [CountDown, timer] = LandingCountDown(date);

  return (
    <Fragment>
      {paramOfStore.includes(triggerRenderParam) && timer && (
        <div className="cr__container-countdown">
          <h1
            className="cr__title-countdown cr__textColor--colorViolet300"
            style={{ textTransform: 'none' }}
          >
            {text}
          </h1>
          {/* <h1
                className='cr__title-countdown cr__textColor--colorViolet300'
              >
                usando el código MRVIRTUAL1
              </h1> */}
          {CountDown}
        </div>
      )}
    </Fragment>
  );
};

export default HeaderAndCountDown;
