import React from 'react';

export default function Banner(props) {
  const { banner, name } = props.data;

  return (
    <div className="category-banner">
      <div className="content-category">
        <div className="content-category__background">
          {/* <ResponsiveImage src={banner} alt={`${name}`} /> */}
          <img loading="lazy" src={banner?.big} alt={name} />
        </div>
        <div className="titles">
          <h1 className="titles__main cr__text--subtitle2">{name}</h1>
          {/* <p className='titles__subtitle'>{description}</p>
           Link commented until there is a page to link to.
            <Link className="titles__more" to="/">Ver mas de Navidad> </Link> */}
        </div>
      </div>
    </div>
  );
}
