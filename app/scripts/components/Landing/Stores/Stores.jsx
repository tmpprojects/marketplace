import React from 'react';
import Slider from 'react-slick';
import { Link } from 'react-router-dom';
import { trackWithGTM } from '../../../Utils/trackingUtils';

export default function Stores(props) {
  const { storesData } = props;

  const { params } = props;

  const sortedData = storesData.sort((a, b) => a.order - b.order);

  const settings = {
    infinite: true,
    swipeToSlide: true,
    arrows: process.env.CLIENT ? true : false,
    speed: 500,
    slidesToShow: 7,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 7000,
    pauseOnHover: true,
    // waitForAnimate: false,
    lazyLoad: process.env.CLIENT ? 'progressive' : null,
    cssEase: 'ease',
    responsive: [
      {
        breakpoint: 2000,
        settings: {
          slidesToShow: 6.99,
          slidesToScroll: 1,
        },
      },
      {
        breakpoint: 1200,
        settings: {
          slidesToShow: 5,
          slidesToScroll: 1,
          arrows: false,
        },
      },
      {
        breakpoint: 767,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 1,
          arrows: false,
          swipeToSlide: true,
          touchThreshold: 1000,
        },
      },
    ],
  };
  const settingsB = {
    infinite: true,
    swipeToSlide: true,
    arrows: process.env.CLIENT ? true : false,
    speed: 500,
    slidesToShow: 7,
    slidesToScroll: 1,
    autoplay: false,
    autoplaySpeed: 5000,
    pauseOnHover: true,
    // waitForAnimate: false,
    lazyLoad: process.env.CLIENT ? 'progressive' : null,
    cssEase: 'ease',
    responsive: [
      {
        breakpoint: 2000,
        settings: {
          slidesToShow: 6.99,
          slidesToScroll: 1,
        },
      },
      {
        breakpoint: 1200,
        settings: {
          slidesToShow: 5,
          slidesToScroll: 1,
          arrows: false,
        },
      },
      {
        breakpoint: 767,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 1,
          arrows: false,
          swipeToSlide: true,
          touchThreshold: 1000,
        },
      },
    ],
  };

  return (
    <div className="cr__stores-container">
      <h1 className="cr__stores-container-title">Tiendas</h1>
      {sortedData.map(({ id, items, name, photo }) =>
        id % 2 === 0 ? (
          <div key={id} className="cr__store-container-section">
            <h2 className="cr__section-title">{name}</h2>
            {items.length >= 1 && (
              <Slider {...settings} className="cr__slider__container">
                {items
                  .sort((a, b) => a.order - b.order)
                  .map(({ store: shop }, index) => {
                    const storeData = {
                      id: shop.slug,
                      name: shop.name,
                      position: index,
                    };

                    const onClickGTMTracking = () => {
                      trackWithGTM(
                        'eec.impressionClick',
                        [storeData],
                        `Landing-Tiendas de ${params}`,
                      );
                    };
                    return (
                      <div key={shop.id}>
                        <Link
                          to={`/stores/${shop.slug}/`}
                          onClick={onClickGTMTracking}
                        >
                          <div className="cr__ImageContainer">
                            {photo ? (
                              <img
                                src={photo.small}
                                alt={shop.name}
                                className="cr__ImageContainer-image"
                                loading="lazy"
                              />
                            ) : (
                              <img
                                src={shop.photo.small}
                                alt={shop.name}
                                className="cr__ImageContainer-image"
                                loading="lazy"
                              />
                            )}
                          </div>
                        </Link>
                      </div>
                    );
                  })}
              </Slider>
            )}
          </div>
        ) : (
          <div key={id} className="cr__store-container-section">
            <h2 className="cr__section-title">{name}</h2>
            {items.length >= 1 && (
              <Slider {...settingsB} className="cr__slider__container">
                {items
                  .sort((a, b) => a.order - b.order)
                  .map(({ store: shop }, index) => {
                    const storeData = {
                      id: shop.slug,
                      name: shop.name,
                      position: index,
                    };

                    const onClickGTMTracking = () => {
                      trackWithGTM(
                        'eec.impressionClick',
                        [storeData],
                        `Landing-Tiendas de ${params}`,
                      );
                    };
                    return (
                      <div key={shop.id}>
                        <Link
                          to={`/stores/${shop.slug}/`}
                          onClick={onClickGTMTracking}
                        >
                          <div className="cr__ImageContainer">
                            {photo ? (
                              <img
                                src={photo.small}
                                alt={shop.name}
                                className="cr__ImageContainer-image"
                                loading="lazy"
                              />
                            ) : (
                              <img
                                src={shop.photo.small}
                                alt={shop.name}
                                className="cr__ImageContainer-image"
                                loading="lazy"
                              />
                            )}
                          </div>
                        </Link>
                      </div>
                    );
                  })}
              </Slider>
            )}
          </div>
        ),
      )}
    </div>
  );
}
