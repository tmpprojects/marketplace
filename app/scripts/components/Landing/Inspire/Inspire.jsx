import React from 'react';
import { Link } from 'react-router-dom';
import { trackWithGTM } from '../../../Utils/trackingUtils';

export default function InspireB(props) {
  const { articlesData } = props;

  const { params } = props;

  const sortedData = articlesData.sort((a, b) => a.order - b.order);
  return (
    <div className="cr__articles-container">
      {sortedData.length > 0 && <h1 className="sectionTitle">Inspire</h1>}

      {sortedData.map(({ items, name, tag }, i) => (
        <div key={i} className="cr__product-container-section">
          {items.length > 0 && <h2 className="sectionTitle-list">{name}</h2>}
          {items.length >= 1 && (
            <div className="containerList">
              <ul className="mainList">
                {items
                  .sort((a, b) => a.order - b.order)
                  .slice(0, 1)
                  .map(({ article: inspire }, index) => {
                    const articleData = {
                      id: inspire.slug,
                      name: inspire.title,
                      position: index,
                    };
                    const onClickGTMTracking = () => {
                      trackWithGTM(
                        'eec.impressionClick',
                        [articleData],
                        `Landing-Inspire de ${params}`,
                      );
                    };
                    return (
                      <li key={inspire.slug} className="mainList-first">
                        <div className="cr__ImageContainer">
                          <Link
                            to={`/inspire/article/${inspire.slug}/`}
                            onClick={onClickGTMTracking}
                            className="cr__ImageContainer-link"
                          >
                            <img
                              src={inspire.cover_photo.medium}
                              alt={inspire.title}
                              className="cr__ImageContainer-image"
                              loading="lazy"
                            />
                          </Link>
                        </div>
                        <div className="cr__Article">
                          <h1 className="cr__Article-title">{inspire.title}</h1>
                          <p className="cr__Article-excerpt">
                            {inspire.excerpt.substring(0, 80)}
                            {inspire.excerpt.length >= 80 && <span>...</span>}
                            <Link
                              to={`/inspire/article/${inspire.slug}/`}
                              className="cr__Article-more more-mobile"
                              onClick={onClickGTMTracking}
                            >
                              Leer más
                            </Link>
                          </p>
                          <Link
                            to={`/inspire/article/${inspire.slug}/`}
                            className="cr__Article-more more-web"
                            onClick={onClickGTMTracking}
                          >
                            Leer más
                          </Link>
                        </div>
                      </li>
                    );
                  })}
                <li className="mainList-second">
                  <ul className="secondList">
                    {items
                      .sort((a, b) => a.order - b.order)
                      .slice(1, 4)
                      .map(({ article: inspire }, index) => {
                        const articleData = {
                          id: inspire.slug,
                          name: inspire.title,
                          position: index + 1,
                        };
                        const onClickGTMTracking = () => {
                          trackWithGTM(
                            'eec.impressionClick',
                            [articleData],
                            `Landing-Inspire de ${params}`,
                          );
                        };
                        return (
                          <li key={inspire.slug}>
                            <div className="cr__ImageContainer">
                              <Link
                                to={`/inspire/article/${inspire.slug}/`}
                                onClick={onClickGTMTracking}
                              >
                                <img
                                  src={inspire.cover_photo.small}
                                  alt={inspire.title}
                                  className="cr__ImageContainer-image"
                                  loading="lazy"
                                />
                              </Link>
                            </div>
                            <div className="cr__Article">
                              <h1 className="cr__Article-title">{inspire.title}</h1>
                              <p className="cr__Article-excerpt">
                                {inspire.excerpt.substring(0, 80)}
                                {inspire.excerpt.length >= 80 && <span>...</span>}
                                <Link
                                  to={`/inspire/article/${inspire.slug}/`}
                                  className="cr__Article-more"
                                  onClick={onClickGTMTracking}
                                >
                                  Leer más
                                </Link>
                              </p>
                            </div>
                          </li>
                        );
                      })}
                  </ul>
                </li>
              </ul>
              {tag ? (
                <Link
                  to={`/inspire/tag/${tag}/`}
                  className="cr__Article-moreInspire"
                >
                  Ver más INSPIRE
                </Link>
              ) : (
                <Link to="/inspire/" className="cr__Article-moreInspire">
                  Ver más INSPIRE
                </Link>
              )}
            </div>
          )}
        </div>
      ))}
    </div>
  );
}
