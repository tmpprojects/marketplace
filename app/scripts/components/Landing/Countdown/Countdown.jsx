import React, { useState, useEffect } from 'react';
import './_countdown.scss';

export default ({ date }) => {
  const [CountDown, setCountDown] = useState({
    days: 0,
    hours: 0,
    min: 0,
    sec: 0,
  });
  const [timer, setTimer] = useState(false);

  const calculateCountdown = (endDate) => {
    let diff = (Date.parse(new Date(endDate)) - Date.parse(new Date())) / 1000;

    // clear countdown when date is reached
    if (diff <= 0) return false;

    const timeLeft = {
      years: 0,
      days: 0,
      hours: 0,
      min: 0,
      sec: 0,
      millisec: 0,
    };

    // calculate time difference between now and expected date
    if (diff >= 365.25 * 86400) {
      // 365.25 * 24 * 60 * 60
      timeLeft.years = Math.floor(diff / (365.25 * 86400));
      diff -= timeLeft.years * 365.25 * 86400;
    }
    if (diff >= 86400) {
      // 24 * 60 * 60
      timeLeft.days = Math.floor(diff / 86400);
      diff -= timeLeft.days * 86400;
    }
    if (diff >= 3600) {
      // 60 * 60
      timeLeft.hours = Math.floor(diff / 3600);
      diff -= timeLeft.hours * 3600;
    }
    if (diff >= 60) {
      timeLeft.min = Math.floor(diff / 60);
      diff -= timeLeft.min * 60;
    }
    timeLeft.sec = diff;

    return timeLeft;
  };

  const addLeadingZeros = (value) => {
    value = String(value);
    while (value.length < 2) {
      value = `0${value}`;
    }
    return value;
  };

  useEffect(() => {
    const interval = setInterval(() => {
      const newDate = calculateCountdown(date);
      if (newDate) {
        setCountDown(newDate);
        setTimer(true);
      } else {
        clearInterval(interval);
        setTimer(false);
      }
    }, 1000);

    return () => {
      clearInterval(interval);
    };
  }, {});

  return (
    <React.Fragment>
      {timer && (
        <div className="cr__countdown" style={{ marginRight: '1em' }}>
          {/* <div>
            <span className="cr__countdown-days cr__text--subtitle2 cr__textColor--colorMain300">
              {addLeadingZeros(CountDown.days)}
            </span>
            <p className="cr__text--subtitle3 cr__textColor--colorMain200">Días</p>
          </div> */}
          {CountDown.hours !== 0 && (
            <div style={{ backgroundColor: '#151620', border: '0' }}>
              <span
                className="cr__countdown-hours cr__text--subtitle2 cr__textColor--colorMain300"
                style={{ color: 'white', fontSize: '3em' }}
              >
                {addLeadingZeros(CountDown.hours)}
              </span>
              <p
                className="cr__text--subtitle3 cr__textColor--colorMain200"
                style={{ color: '#B7B7B7', fontSize: '0.9em' }}
              >
                hr.
              </p>
            </div>
          )}

          <div style={{ backgroundColor: '#151620', border: '0' }}>
            <span
              className="cr__countdown-minutes cr__text--subtitle2 cr__textColor--colorMain300"
              style={{ color: 'white', fontSize: '3em' }}
            >
              {addLeadingZeros(CountDown.min)}
            </span>
            <p
              className="cr__text--subtitle3 cr__textColor--colorMain200"
              style={{ color: '#B7B7B7', fontSize: '0.9em' }}
            >
              min.
            </p>
          </div>
          <div style={{ backgroundColor: '#151620', border: '0' }}>
            <span
              className="cr__countdown-seconds cr__text--subtitle2 cr__textColor--colorMain300"
              style={{ color: 'white', fontSize: '3em' }}
            >
              {addLeadingZeros(CountDown.sec)}
            </span>
            <p
              className="cr__text--subtitle3 cr__textColor--colorMain200"
              style={{ color: '#B7B7B7', fontSize: '0.9em' }}
            >
              seg.
            </p>
          </div>
        </div>
      )}
    </React.Fragment>
  );
};
