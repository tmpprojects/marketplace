import React from 'react';
import { Link } from 'react-router-dom';
import { trackWithGTM } from '../../../Utils/trackingUtils';
// import { IconPreloader } from '../../../Utils/Preloaders';
import Placeholder from '../../../../images/placeholder/placeholder--productPage.jpg';

const errorImage = (e) => {
  e.target.src = Placeholder;
};

export default class StoresList extends React.Component {
  state = {
    from: 0,
    to: 30,
    activePage: 1,
  };
  changeSlice = (from = 0, to = 0, activePage = 0) => {
    this.setState({ from, to, activePage });
    window.scrollTo({ top: 0, behavior: 'smooth' });
  };

  render() {
    const { location = '', items = [] } = this.props;

    const { from = 0, to = 0, activePage = 0 } = this.state;
    return (
      <React.Fragment>
        <div className="cr__stores">
          <div className="cr__stores-list">
            {items
              ?.slice(from, to)
              ?.map(
                (
                  { name = '', slug = '', photo = '', cover = '', slogan = '' },
                  index,
                ) => {
                  const store = {
                    id: slug,
                    name,
                    position: index,
                  };
                  const onClickGTMTracking = () => {
                    trackWithGTM(
                      'eec.impressionClick',
                      [store],
                      `Landing-${location}`,
                    );
                  };
                  return (
                    <div className="cr__stores-item" key={index}>
                      <Link to={`/stores/${slug}/`} onClick={onClickGTMTracking}>
                        <div className="cr__stores-item-a">
                          {/* {loaded ? null : (
                              <IconPreloader style={{ marginBottom: '35px' }} />
                            )} */}
                          <img
                            // style={loaded ? {} : { display: 'none' }}
                            src={cover}
                            alt={name}
                            aria-label={name}
                            // onLoad={() => setLoaded(true)}
                            onError={errorImage}
                            loading="lazy"
                          />
                        </div>
                        <div className="cr__stores-item-b">
                          <div className="cr__container-logo">
                            {/* {loaded ? null : (
                                <img src={Placeholder} alt='Cargando...' />
                              )} */}
                            <img
                              // style={loaded ? {} : { display: 'none' }}
                              src={photo}
                              alt={name}
                              aria-label={name}
                              // onLoad={() => setLoaded(true)}
                              onError={errorImage}
                              loading="lazy"
                              // onError={(e) => {
                              //   e.target.onerror = null;
                              //   e.target.src = Placeholder;
                              // }}
                            />
                          </div>
                          <div className="cr__container-name">
                            <h3 className="cr__container-name-title cr__text--subtitle3 cr__textColor--colorDark300">
                              {name.length >= 24
                                ? `${name.substring(0, 24)}...`
                                : name}
                            </h3>
                            <div className="cr__text--paragraph cr__textColor--colorDark100">
                              <p>
                                {slogan.length >= 75
                                  ? `${slogan.substring(0, 75)}...`
                                  : slogan}
                              </p>
                            </div>
                          </div>
                        </div>
                      </Link>
                    </div>
                  );
                },
              )}
          </div>
        </div>
        <div className="pagination">
          <ul>
            <li
              className={activePage === 1 ? 'active' : undefined}
              onClick={() => this.changeSlice(0, 36, 1)}
            >
              1
            </li>
            <li
              className={activePage === 2 ? 'active' : undefined}
              onClick={() => this.changeSlice(36, 72, 2)}
            >
              2
            </li>
            <li
              className={activePage === 3 ? 'active' : undefined}
              onClick={() => this.changeSlice(72, 108, 3)}
            >
              3
            </li>
            <li
              className={activePage === 4 ? 'active' : undefined}
              onClick={() => this.changeSlice(108, 144, 4)}
            >
              4
            </li>
            <li
              className={activePage === 5 ? 'active' : undefined}
              onClick={() => this.changeSlice(144, 180, 5)}
            >
              5
            </li>
          </ul>
        </div>
      </React.Fragment>
    );
  }
}
