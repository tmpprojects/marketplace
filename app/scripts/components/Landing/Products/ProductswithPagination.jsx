import React from 'react';
import Slider from 'react-slick';
import { Link } from 'react-router-dom';
import { trackWithGTM } from '../../../Utils/trackingUtils';

function dispatchDataLayerClick(id) {
  //console.log("dispatchDataLayerClick productsWithPagination", id);
  globalThis.googleAnalytics.productClick(id);

};

export default function ProductsB(props) {
  const { productsData, from, to, params } = props;

  // const { params } = props;

  const sortedData = productsData.sort((a, b) => a.order - b.order);
  const settings = {
    infinite: true,
    swipeToSlide: true,
    arrows: process.env.CLIENT ? true : false,
    speed: 500,
    slidesToShow: 4,
    slidesToScroll: 1,
    autoplay: false,
    autoplaySpeed: 7000,
    lazyLoad: process.env.CLIENT ? 'progressive' : null,
    pauseOnHover: true,
    cssEase: 'ease',
    responsive: [
      {
        breakpoint: 2000,
        settings: {
          slidesToShow: 3.99,
          slidesToScroll: 1,
        },
      },
      {
        breakpoint: 1200,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 1,
          arrows: false,
        },
      },
      {
        breakpoint: 767,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1,
          speed: 100,
          arrows: false,
          swipeToSlide: true,
          touchThreshold: 1000,
        },
      },
    ],
  };
  const settingsB = {
    infinite: true,
    swipeToSlide: true,
    arrows: process.env.CLIENT ? true : false,
    speed: 500,
    slidesToShow: 4,
    slidesToScroll: 1,
    autoplay: false,
    autoplaySpeed: 5000,
    pauseOnHover: true,
    lazyLoad: process.env.CLIENT ? 'progressive' : null,
    cssEase: 'ease',
    responsive: [
      {
        breakpoint: 2000,
        settings: {
          slidesToShow: 3.99,
          slidesToScroll: 1,
        },
      },
      {
        breakpoint: 1200,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 1,
          arrows: false,
        },
      },
      {
        breakpoint: 767,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1,
          speed: 100,
          arrows: false,
          swipeToSlide: true,
          touchThreshold: 1000,
        },
      },
    ],
  };

  return (
    <div className="cr__products-container">
      {sortedData.slice(from, to).map(({ id, items, name, excerpt }) =>
        id % 2 === 0 ? (
          <div key={Math.random()} className="cr__product-container-section">
            <div className="cr__section-titleBVF">
              {items
                .sort((a, b) => a.order - b.order)
                .slice(0, 1)
                .map((item) => (
                  <Link to={`/stores/${item.product.store.slug}`}>
                    <h2 className="cr__section-titleBVF-title">{name}</h2>
                    <span className="cr__section-titleBVF-nextTo cr__textColor--colorMain300">
                      Visitar tienda
                    </span>
                  </Link>
                ))}
            </div>
            <p className="cr__section-description">{excerpt}</p>
            {items.length >= 1 && (
              <Slider {...settings} className="cr__slider__container">
                {items
                  .sort((a, b) => a.order - b.order)
                  .map(({ photo, product: productItems }, index) => {
                    const productData = {
                      id: productItems.slug,
                      name: productItems.name,
                      position: index,
                    };

                    const onClickGTMTracking = () => {
                      trackWithGTM(
                        'eec.impressionClick',
                        [productData],
                        `Landing-Productos de ${params}`,
                      );
                    };
                    return (
                      <div key={productItems.slug}>
                        <Link
                          to={`/stores/${productItems.store.slug}/products/${productItems.slug}/`}
                          onClick={onClickGTMTracking}
                        >
                          <div className="cr__ImageContainer">
                            {
                              <img
                                src={
                                  photo.small
                                    ? photo.small
                                    : productItems.photo.small
                                }
                                alt={productItems.name}
                                className="cr__ImageContainer-image"
                                loading="lazy"
                              />
                            }
                          </div>
                          <div className="cr__Product">
                            <h1 className="cr__Product-name">{productItems.name}</h1>
                            <h5 className="cr__Product-storeName">
                              {productItems.store.name}
                            </h5>
                            <div className="cr__Product-priceContainer">
                              {productItems?.price !==
                                productItems?.price_without_discount && (
                                <h1 className="cr__Product-price strikeThrough cr__text--paragraph">
                                  {productItems?.price_without_discount} MXN
                                </h1>
                              )}
                              <h1 className="cr__Product-price cr__text--subtitle3 cr__textColor--colorWhite">
                                {productItems?.price} MXN
                              </h1>
                            </div>
                          </div>
                        </Link>
                      </div>
                    );
                  })}
              </Slider>
            )}
          </div>
        ) : (
          <div key={Math.random()} className="cr__product-container-section">
            <div className="cr__section-titleBVF">
              {items
                .sort((a, b) => a.order - b.order)
                .slice(0, 1)
                .map((item) => (
                  <Link to={`/stores/${item.product.store.slug}`}>
                    <h2 className="cr__section-titleBVF-title">{name}</h2>
                    <span className="cr__section-titleBVF-nextTo cr__textColor--colorMain300">
                      Visitar tienda
                    </span>
                  </Link>
                ))}
            </div>
            <p className="cr__section-description">{excerpt}</p>
            {items.length >= 1 && (
              <Slider {...settingsB} className="cr__slider__container">
                {items
                  .sort((a, b) => a.order - b.order)
                  .map(({ photo, product: productItems }, index) => {
                    const productData = {
                      id: productItems.slug,
                      name: productItems.name,
                      position: index,
                    };

                    const onClickGTMTracking = () => {
                      trackWithGTM(
                        'eec.impressionClick',
                        [productData],
                        `Landing-Productos de ${params}`,
                      );
                    };
                    return (
                      <div key={productItems.slug}>
                        <Link
                          to={`/stores/${productItems.store.slug}/products/${productItems.slug}/`}
                          onClick={() => {
                            // onClickGTMTracking(productItems.id)
                             dispatchDataLayerClick(productItems.id)
                           }}
                        >
                          <div className="cr__ImageContainer">
                            {
                              <img
                                src={
                                  photo.small
                                    ? photo.small
                                    : productItems.photo.small
                                }
                                alt={productItems.name}
                                className="cr__ImageContainer-image"
                                loading="lazy"
                              />
                            }
                          </div>
                          <div className="cr__Product">
                            <h1 className="cr__Product-name">{productItems.name}</h1>
                            <h5 className="cr__Product-storeName">
                              {productItems.store.name}
                            </h5>
                            <div className="cr__Product-priceContainer">
                              {productItems?.price !==
                                productItems?.price_without_discount && (
                                <h1 className="cr__Product-price strikeThrough cr__text--paragraph">
                                  {productItems?.price_without_discount} MXN
                                </h1>
                              )}
                              <h1 className="cr__Product-price cr__text--subtitle3 cr__textColor--colorWhite">
                                {productItems?.price} MXN
                              </h1>
                            </div>
                          </div>
                        </Link>
                      </div>
                    );
                  })}
              </Slider>
            )}
          </div>
        ),
      )}
    </div>
  );
}
