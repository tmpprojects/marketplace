import React from 'react';
import Slider from 'react-slick';
import { Link } from 'react-router-dom';
import { trackWithGTM } from '../../../Utils/trackingUtils';

export default function ProductsB(props) {
  const { productsData } = props;

  const { params } = props;

  const sortedData = productsData.sort((a, b) => a.order - b.order);
  const settings = {
    infinite: true,
    swipeToSlide: true,
    arrows: process.env.CLIENT ? true : false,
    speed: 500,
    slidesToShow: 4,
    slidesToScroll: 1,
    autoplay: false,
    autoplaySpeed: 7000,
    lazyLoad: process.env.CLIENT ? 'progressive' : null,
    pauseOnHover: true,
    cssEase: 'ease',
    responsive: [
      {
        breakpoint: 2000,
        settings: {
          slidesToShow: 3.99,
          slidesToScroll: 1,
        },
      },
      {
        breakpoint: 1200,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 1,
          arrows: false,
        },
      },
      {
        breakpoint: 767,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1,
          speed: 100,
          arrows: false,
          swipeToSlide: true,
          touchThreshold: 1000,
        },
      },
    ],
  };
  const settingsB = {
    infinite: true,
    swipeToSlide: true,
    arrows: process.env.CLIENT ? true : false,
    speed: 500,
    slidesToShow: 4,
    slidesToScroll: 1,
    autoplay: false,
    autoplaySpeed: 5000,
    pauseOnHover: true,
    lazyLoad: process.env.CLIENT ? 'progressive' : null,
    cssEase: 'ease',
    responsive: [
      {
        breakpoint: 2000,
        settings: {
          slidesToShow: 3.99,
          slidesToScroll: 1,
        },
      },
      {
        breakpoint: 1200,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 1,
          arrows: false,
        },
      },
      {
        breakpoint: 767,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1,
          speed: 100,
          arrows: false,
          swipeToSlide: true,
          touchThreshold: 1000,
        },
      },
    ],
  };

  const dispatchDataLayerClick = (id) => {
    console.log("dispatchDataLayerClick products");
    globalThis.googleAnalytics.productClick(id);

  };

  return (
    <div className="cr__products-container">
      {sortedData.map(({ id, items, name, excerpt }) =>
        id % 2 === 0 ? (
          <div key={Math.random()} className="cr__product-container-section">
            <h2 className="cr__section-title">{name}</h2>
            <p className="cr__section-description">{excerpt}</p>
            {items.length >= 1 && (
              <Slider {...settings} className="cr__slider__container">
                {items
                  .sort((a, b) => a.order - b.order)
                  .map(({ photo, product: productItems }, index) => {
                    const productData = {
                      id: productItems.slug,
                      name: productItems.name,
                      position: index,
                    };

                    const onClickGTMTracking = (id) => {
                      trackWithGTM(
                        'eec.impressionClick',
                        [productData],
                        `Landing-Productos de ${params}`,
                      );
                      console.log("product---------- 1",product)
                      dispatchDataLayerClick(id)
                    };
                    return (
                      <div key={productItems.slug}>
                        <Link
                          to={`/stores/${productItems.store.slug}/products/${productItems.slug}/`}
                          onClick={() => onClickGTMTracking(productItems.id)}
                        >
                          <div className="cr__ImageContainer">
                            {/*!photo ? (
                              <img
                                src={photo.small}
                                alt={productItems.name}
                                className='cr__ImageContainer-image'
                              />
                            ) : (
                              <img
                                src={productItems.photo.small}
                                alt={productItems.name}
                                className='cr__ImageContainer-image'
                              />
                            )*/}
                            {
                              <img
                                src={
                                  photo.small
                                    ? photo.small
                                    : productItems.photo.small
                                }
                                alt={productItems.name}
                                className="cr__ImageContainer-image"
                                loading="lazy"
                              />
                            }
                          </div>
                          <div className="cr__Product">
                            <h1 className="cr__Product-name">{productItems.name}</h1>
                            <h5 className="cr__Product-storeName">
                              {productItems.store.name}  --NAME0
                            </h5>
                            <div className="cr__Product-priceContainer">
                              {productItems?.price !==
                                productItems?.price_without_discount && (
                                  <h1 className="cr__Product-price strikeThrough cr__text--paragraph cr__textColor--colorGray400">
                                    {productItems?.price_without_discount} MXN
                                  </h1>
                                )}
                              <h1 className="cr__Product-price cr__text--subtitle3 cr__textColor--colorWhite">
                                {productItems?.price} MXN
                              </h1>
                            </div>
                          </div>
                        </Link>
                      </div>
                    );
                  })}
              </Slider>
            )}
          </div>
        ) : (
          <div key={Math.random()} className="cr__product-container-section">
            <h2 className="cr__section-title">{name}</h2>
            <p className="cr__section-description">{excerpt}</p>
            {items.length >= 1 && (
              <Slider {...settingsB} className="cr__slider__container">
                {items
                  .sort((a, b) => a.order - b.order)
                  .map(({ photo, product: productItems }, index) => {
                    const productData = {
                      id: productItems.slug,
                      name: productItems.name,
                      position: index,
                    };

                    const onClickGTMTracking = () => {
                      trackWithGTM(
                        'eec.impressionClick',
                        [productData],
                        `Landing-Productos de ${params}`,
                      );
                      dispatchDataLayerClick(id)
                    };
                    return (
                      <div key={productItems.slug}>
                        <Link
                          to={`/stores/${productItems.store.slug}/products/${productItems.slug}/`}
                          onClick={onClickGTMTracking}
                        >
                          <div className="cr__ImageContainer">
                            {/*!photo ? (
                              <img
                                src={photo.small}
                                alt={productItems.name}
                                className='cr__ImageContainer-image'
                              />
                            ) : (
                              <img
                                src={productItems.photo.small}
                                alt={productItems.name}
                                className='cr__ImageContainer-image'
                              />
                            )*/}
                              {
                                <img
                                  src={
                                    photo.small
                                      ? photo.small
                                      : productItems.photo.small
                                  }
                                  alt={productItems.name}
                                  className="cr__ImageContainer-image"
                                  loading="lazy"
                                />
                              }
                            </div>
                            <div className="cr__Product">
                              <h1 className="cr__Product-name">{productItems.name}</h1>
                              <h5 className="cr__Product-storeName">
                                {productItems.store.name} --NAMEE1
                            </h5>
                              <div className="cr__Product-priceContainer">
                                {productItems?.price !==
                                  productItems?.price_without_discount && (
                                    <h1 className="cr__Product-price strikeThrough cr__text--paragraph cr__textColor--colorGray400">
                                      {productItems?.price_without_discount} MXN
                                    </h1>
                                  )}
                                <h1 className="cr__Product-price cr__text--subtitle3 cr__textColor--colorWhite">
                                  {productItems?.price} MXN
                              </h1>
                              </div>
                            </div>
                          </Link>
                        </div>
                      );
                    })}
                </Slider>
              )}
            </div>
          ),
      )}
    </div>
  );
}
