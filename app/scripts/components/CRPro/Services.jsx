import React from 'react';
import { Link } from 'react-router-dom';
import { ResponsiveImage } from '../../Utils/ImageComponents';
import './ServicesStyles.scss';
export default class Services extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      sections: this.props.sections || [],
      products: this.props.products || [],
    };
  }

  /**
   * componentWillReceiveProps()
   */
  componentWillReceiveProps(nextProps) {
    if (this.props.sections !== nextProps.sections) {
      this.setState({
        sections: nextProps.sections,
      });
    }
    if (this.props.products !== nextProps.products) {
      this.setState({
        products: nextProps.products,
      });
    }
  }

  /**
   * renderProductsByCategory()
   * Returns an array of products filtered by the passed category slug
   * @param {string} categorySlug | A category slug
   * @returns {array}
   */
  renderProductsByCategory = (categorySlug) => {
    const { products } = this.state;
    const productsByCategory = products.filter((p) =>
      p.section !== 'membresia-pro'
        ? categorySlug === 'servicio-pro'
        : p.section === categorySlug,
    );

    if (productsByCategory.length < 1) {
      return (
        <p className="alert">
          Estamos preparando cosas sorprendentes, sólo para ti.
        </p>
      );
    }
    return productsByCategory.map((product) => this.renderProduct(product));
  };

  /**
   * renderProduct()
   * Returns a product markup depending on the product_type.
   * @param {object} product | A product
   * @returns {object}
   */
  renderProduct = (product) => {
    const productType = product.product_type.value;

    // Construct Link;
    let linkTarget = `/stores/canastarosa-pro/products/${product.slug}`;
    let clickHandler = null;
    if (productType !== 'physical') {
      linkTarget = {
        pathname: `/pro/servicios/${product.slug}`,
        state: { scrollToTop: false },
      };
      clickHandler = (e) => this.props.openDetailsWindow(product);
    }

    // Return Markup
    return (
      <div className="card" key={product.id}>
        <Link to={linkTarget} onClick={clickHandler} className="card__thumbnail">
          <ResponsiveImage src={product?.photo} alt={product.name} />
        </Link>

        <div className="description">
          <Link to={linkTarget} onClick={clickHandler} className="card__title">
            {product.name}
          </Link>
          <div className="card__content">{product.excerpt}</div>

          <Link to={linkTarget} onClick={clickHandler} className="card__link">
            {product.call_to_action_label.length === 0
              ? 'Ver Detalle'
              : product.call_to_action_label}
          </Link>
        </div>
      </div>
    );
  };

  /**
   * render()
   */
  render() {
    const { sections } = this.state;
    if (sections.length < 1) {
      return (
        <div className="section-services">
          <div className="services wrapper--center">
            <p className="alert alert-cat">
              Aún no has agregado Servicios a tu Tienda
            </p>
          </div>
        </div>
      );
    }

    return (
      <div className="section-services">
        <div className="services wrapper--center pro-services-wrapper">
          {/* sections
            .sort((a, b) => a.id - b.id)
            .map((section) => (
              <React.Fragment key={section.id}>
                <h4>{section.name}</h4>
                <div className="cards-box">
                  {this.renderProductsByCategory(section.slug)}
                </div>
              </React.Fragment>
            )) */}
          <p className="alert">
            <h3 className="pro-services pro-services">
              Marketing Kits y Servicios Pro
            </h3>
            Conoce los servicios de marketing y publicidad diseñados exclusivamente
            para generar conocimiento de tu marca, crecer tus ventas y llevar tu
            negocio al siguiente nivel. ¡No esperes más!
            <ul></ul>
            <h3 className="pro-services">Kits Básicos</h3>
            Kits generales y diseñados para todas las tiendas. Si tu tienda es nueva
            en Canasta Rosa y/o aún no has invertido en publicidad, estos kits son la
            mejor opción.
            <ul>
              <li>Rose Bronce Pro</li>
              <li>Rose Silver Pro</li>
              <li>Rose Gold Pro</li>
            </ul>
            <h3 className="pro-services">Kits por Categoría</h3>
            Kits diseñados para cada una de las categorías, más personalizados y
            enfocados a tu tienda.
            <ul>
              <li>Yummy Pro</li>
              <li>Para Regalar Pro</li>
              <li>BeYOUtiful Pro</li>
              <li>Fashionista Pro</li>
            </ul>
            <h3 className="pro-services">Kits Especiales</h3>
            Kits diseñados para los eventos y distintas temporalidades en Canasta
            Rosa. Si estás participando en cualquier evento, estos kits son para ti.
            <ul>
              <li>Pauta Express Eventos 1</li>
              <li>Pauta Express Eventos 2</li>
            </ul>
            <h3 className="pro-services">A tu Medida</h3>
            ¿Quiéres crear tu propio kit? Contrata cualquiera de nuestros Servicios
            Pro por individual. En compras mayores a $3,000 te hacemos el 45% de
            descuento.
            <ul>
              <li>Pauta Google</li>
              <li>Sesión Fotográfica Profesional</li>
              <li>Shooting de Composición</li>
              <li>Post Facebook</li>
              <li>Post Instagram</li>
              <li>Reel Instagram</li>
              <li>Quizzes Facebook</li>
              <li>Mención en Newsletter</li>
              <li>Artículo Inspire</li>
              <li>Banner en el Home</li>
              <li>Video Stop Motion</li>
              <li>Video "30"</li>
            </ul>
          </p>
        </div>
      </div>
    );
  }
}
