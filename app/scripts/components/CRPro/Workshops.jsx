import React from 'react';
import { Link } from 'react-router-dom';

import {
  formatDate,
  formatDateToUTC,
  addHourPeriodSuffix,
} from '../../Utils/dateUtils';
import { formatNumberToPrice } from '../../Utils/normalizePrice';

export default class Workshop extends React.Component {
  renderEvents = () => {
    const events = this.props.events.sort(
      (a, b) => new Date(a.event_properties.day) - new Date(b.event_properties.day),
    );
    if (events.length < 1) {
      return (
        <p className="alert alert-cat">
          Vuelve pronto para enterarte sobre más eventos increíbles.
        </p>
      );
    }
    const LinkFactory = (children, event, externalLink = null) => {
      // console.log('external linlk: ', this.props);
      return externalLink
        ? React.createElement('a', {
            href: `${externalLink}`,
            target: '_blank',
            className: 'registry',
            children,
          })
        : React.createElement('Link', {
            to: {
              pathname: `/pro/eventos/${event.slug}`,
              state: { scrollToTop: false },
            },
            onClick: () => this.props.openDetailsWindow(event),
            className: 'registry',
            children,
          });
    };

    return events.map((event) => {
      const newFormatStartTime = event.event_properties.start_time
        .split(':', 2)
        .join(':');
      const newFormatEndTime = event.event_properties.end_time
        .split(':', 2)
        .join(':');
      let externalLink = null;
      switch (event.slug) {
        case 'abre-tu-tienda-en-canasta-rosa-08009088':
          externalLink =
            'https://canastarosa.clickmeeting.com/abrde-tu-tienda-en-canasta-rosa/register';
          break;
        case 'vuelvete-un-experto-en-vender-flores-y-regalos-08811118':
          externalLink =
            'https://canastarosa.clickmeeting.com/todo-lo-que-neceesitas-para-volverte-un-experto-al-vender/register';
          break;
        case 'abre-tu-tienda-en-canasta-rosa-90111019':
          externalLink =
            'https://canastarosa.clickmeeting.com/abre-tu-tienda-en-casnasta-rosa/register';
          break;
        case 'impuestos-para-emprendedoras-y-temas-fiscales-98981891':
          externalLink =
            'https://canastarosa.clickmeeting.com/impuestos-para-emprendedoras-y-empresarias/register';
          break;
        case 'todo-sobre-categorizacion-98819111':
          externalLink =
            'https://canastarosa.clickmeeting.com/todo-sobre-categorizacion/register';
          break;
        case 'excel-para-principiantes-18110099':
          externalLink =
            'https://canastarosa.clickmeeting.com/tips-de-excel-para-llevar-el-control-de-tu-stock/register';
          break;
        case 'abre-tu-tienda-99989009':
          externalLink =
            'https://canastarosa.clickmeeting.com/abre-tu-tienda-den-canasta-rosa/register';
          break;
        case 'aprende-a-contar-historias-para-enamorar-a-tus-cli-90888111':
          externalLink =
            'https://canastarosa.clickmeeting.com/aprende-a-contar-historias-y-enamora-a-tu-cliente/register';
          break;
        case 'webinar-todo-sobre-categorizacion-01019189':
          externalLink =
            'https://canastarosa.clickmeeting.com/todo-sobre-catdegorizacion/register';
          break;
        case 'tu-contabilidad-en-la-era-digital-91099880':
          externalLink =
            'https://canastarosa.clickmeeting.com/tu-codntabilidad-en-la-era-digital/register';
          break;
        case 'abre-tu-tienda-en-canasta-rosa-19098189':
          externalLink =
            'https://canastarosa.clickmeeting.com/abre-tu-tiednda-en-canasta-rosa/register';
          break;
        case 'impuestos-para-emprendedoras-y-temas-fiscales-81199919':
          externalLink =
            'https://canastarosa.clickmeeting.com/impudestos-para-emprendedoras-y-empresarias/register';
          break;
        case 'webinar-todo-sobre-categorizacion-98898119':
          externalLink =
            'https://canastarosa.clickmeeting.com/todo-sobree-categorizacion/register';
          break;
        case 'webinar-estrategias-de-retencion-11089010':
          externalLink =
            'https://canastarosa.clickmeeting.com/estrategias-de-retencioon/register';
          break;
        case 'aprende-a-contar-historias-y-enamora-a-tu-cliente-01910918':
          externalLink =
            'https://canastarosa.clickmeeting.com/aprednde-a-contar-historias-y-enamora-a-tu-cliente/register';
          break;
        case 'webinar-todo-sobre-categorizacion-08098001':
          externalLink =
            'https://canastarosa.clickmeeting.com/todoo-sobre-categorizacion/register';
          break;
        default:
          externalLink = null;
      }

      return (
        <li key={event.id} className="workshop">
          <div className="workshop_container wrapper--center">
            {LinkFactory(
              <img
                className="image_container"
                src={event.photo.medium}
                alt={event.name}
              />,
              event,
              externalLink,
            )}

            <div className="info_container">
              {LinkFactory(
                <h5 className="title">{event.name}</h5>,
                event,
                externalLink,
              )}

              {event.event_properties.taught_by ? (
                <p>
                  <span className="bold">Impartido por: </span>
                  {event.event_properties.taught_by}
                </p>
              ) : null}

              {event.event_properties.show_date ? (
                <p>
                  <span className="bold">Fecha: </span>
                  {formatDate(formatDateToUTC(new Date(event.event_properties.day)))}
                  ,{addHourPeriodSuffix(newFormatStartTime)} a{' '}
                  {addHourPeriodSuffix(newFormatEndTime)}
                </p>
              ) : null}

              {/*<p><span className="bold">Lugar: </span>{event.event_properties.place}</p>*/}
              <div className="half">
                {/*<p className="price">
                                    <span className="bold">Costo: </span>
                                    {formatNumberToPrice(event.event_properties.price)}
                                </p>*/}
                <p>
                  <span className="bold">Cupo: </span>
                  {event.quantity}
                </p>
              </div>
              {LinkFactory('Regístrate aquí', event, externalLink)}
            </div>
          </div>
        </li>
      );
    });
  };

  componentDidCatch(error, info) {
    console.log('error ', error, info);
    // Display fallback UI
    //this.setState({ viewState: VIEW_STATE.ERROR });
  }

  render() {
    console.log(this.props, 'Workshops');
    return (
      <div className="workshops">
        <ul className="workshops_list">{this.renderEvents()}</ul>
      </div>
    );
  }
}
