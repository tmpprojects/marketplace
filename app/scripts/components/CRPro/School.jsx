import React from 'react';
import { Link } from 'react-router-dom';

class School extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      coursesByCategory: {},
    };
  }

  componentDidMount() {
    this.filterPostsByCategory(this.props.courses);
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.courses !== nextProps.courses) {
      this.filterPostsByCategory(nextProps.courses);
    }
  }

  filterPostsByCategory = (courses) => {
    const coursesByCategory = {};
    // Iterate through each post to get a category based post list.
    courses.forEach((course) => {
      // Loop through each post category
      const cats = course._embedded['wp:term'][0];
      cats.forEach((cat) => {
        //Case: Category Obj doesnt exist
        if (coursesByCategory[`'${cat.slug}'`] === undefined) {
          //Add category object
          coursesByCategory[`'${cat.slug}'`] = {
            category: cat.name,
            courses: [course],
          };
          //Case: Category Obj exist
        } else {
          coursesByCategory[`'${cat.slug}'`].courses.push(course);
        }
      });
    });

    // Update State
    this.setState({
      coursesByCategory,
    });
  };

  renderCourses = () => {
    const { coursesByCategory } = this.state;
    const cats = Object.keys(coursesByCategory);
    if (cats.length < 1) {
      return (
        <p className="alert alert-cat">Aún no has agregado Cursos a tu Tienda</p>
      );
    }
    const coursesTemplate = [];

    // Iterate through each category.
    cats.forEach((cat) => {
      const catTitle = (
        <h4
          key={coursesByCategory[`${cat}`].category}
          dangerouslySetInnerHTML={{
            __html: coursesByCategory[`${cat}`].category,
          }}
        />
      );

      const catPosts = coursesByCategory[`${cat}`].courses.map((course) => (
        <div className="card" key={course.id}>
          <Link
            to={{
              pathname: `/pro/cursos/${course.slug}`,
              state: { scrollToTop: false },
            }}
            className="card__thumbnail"
            onClick={(e) => this.props.openDetailsWindow(course)}
          >
            <img
              src={
                course._embedded['wp:featuredmedia'] === undefined
                  ? ''
                  : course._embedded['wp:featuredmedia'][0].source_url
              }
              alt={course.title.rendered}
            />
          </Link>

          <div className="description">
            <Link
              to={{
                pathname: `/pro/cursos/${course.slug}`,
                state: { scrollToTop: false },
              }}
              onClick={(e) => this.props.openDetailsWindow(course)}
              className="card__title"
            >
              {course.title.rendered}
            </Link>
            <div
              className="card__content"
              dangerouslySetInnerHTML={{ __html: course.excerpt.rendered }}
            />
            <Link
              to={{
                pathname: `/pro/cursos/${course.slug}`,
                state: { scrollToTop: false },
              }}
              onClick={(e) => this.props.openDetailsWindow(course)}
              className="card__link"
            >
              Ver más...
            </Link>
          </div>
        </div>
      ));
      const catBox = <div className="cards-box">{catPosts}</div>;
      coursesTemplate.push(catTitle, catBox);
    });

    // Return markup
    return coursesTemplate;
  };

  render() {
    return (
      <div className="section-services">
        <div className="services wrapper--center">{this.renderCourses()}</div>
      </div>
    );
  }
}

export default School;
