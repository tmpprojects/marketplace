const baseURL = 'https://s3.us-east-2.amazonaws.com/canastarosa/temp/banners';
const fakeBanner = [
  {
    title: 'Nosotros ya estamos listos para las Fiestas Patrias ¿y tú?.',
    text:
      'Los platillos más deliciosos para disfrutar y celebrar a México están aquí.',
    linkText: '¡Ordena tus favoritos ahora!',
    link: '/category/celebraciones-15-de-septiembre-2',
    img: `${baseURL}/Baner-loteria.jpg`,
    color1: '#004D00',
    color2: '#8EF665',
  },
  {
    title: 'Sorprende a tu persona favorita con regalos originales.',
    text:
      'Increíbles canastas, chocolates, flores y galletas para obsequiar a tu persona favorita.',
    linkText: 'Encuentra el detalle perfecto',
    link: '/category/regalos-1',
    img: `${baseURL}/unnamed-10.jpg`,
    color1: '#5ac5f9',
    color2: '#183e68',
  },
  {
    title: 'Deléitate con deliciosos platillos según tus antojos.',
    text:
      'Desde los mejores ingredientes para cocinar hasta exquisitos postres, botanas y platos fuertes.',
    linkText: 'Déjate complacer',
    link: '/category/comida-1',
    img: `${baseURL}/unnamed-2.jpg`,
    color1: '#883E00',
    color2: '#FF9F44',
  },
  {
    title: 'Los mejores productos para las fiestas.',
    text:
      'Organiza una fiesta con los mejores pasteles, globos y diseños de invitación.',
    linkText: 'Comienza tu festejo aquí',
    link: '/category/fiestas-1',
    img: `${baseURL}/unnamed-7.jpg`,
    color1: '#6D0088',
    color2: '#FF4478',
  },
  {
    title: 'Lo mejor en tendencia de diseño de bolsas y para tu hogar.',
    text: 'Decora tu hogar con ideas creativas y luce genial con bolsas únicas.',
    linkText: 'Elige los mejores diseños',
    link: '/category/disenio-1',
    img: `${baseURL}/unnamed-1.jpg`,
    color1: '#010084',
    color2: '#009DFF',
  },
  {
    title: 'Arreglos florales para ese momento especial.',
    text:
      'Porque una rosa dice más que mil palabras, encuentra arreglos originales.',
    linkText: 'Regala flores',
    link: '/category/flores-1',
    img: `${baseURL}/unnamed-6.jpg`,
    color1: '#500084',
    color2: '#DB52FF',
  },
  {
    title:
      'Para el tesoro más preciado de la casa, los mejores artículos para niños y bebés.',
    text:
      'Encuentra artículos únicos como juguetes, cobijas, ropa infantil, cobijas y acccesorios.',
    linkText: 'Diviértelos ahora',
    link: '/category/infantil-1',
    img: `${baseURL}/unnamed-5.jpg`,
    color1: '#008483',
    color2: '#52FFA1',
  },
  {
    title: 'Productos artesanales para tu cuidado y belleza.',
    text:
      'Cuida de tu belleza con maquillaje, cremas faciales, aceites, etc. Todo hecho en México.',
    linkText: 'Ver productos de belleza.',
    link: '/category/belleza-1',
    img: `${baseURL}/unnamed-4.jpg`,
    color1: '#439F68',
    color2: '#C3FF00',
  },
  {
    title: 'Artículos exclusivos para consentir a tu mascota.',
    text: 'Los mejores postres y los juguetes más increíbles para tu mascota.',
    linkText: 'Empieza a cosentirlo',
    link: '/category/mascotas-1',
    img: `${baseURL}/unnamed-8.jpg`,
    color1: '#883E00',
    color2: '#FF9F44',
  },
  {
    title: 'Encuentra ropa y calzado únicos para toda ocasión.',
    text: 'Diseños únicos de ropa y calzado para dama, caballero, niños y bebés.',
    linkText: 'Descubre tu estilo',
    link: '/category/ropa-y-calzado-1',
    img: `${baseURL}/unnamed-9.jpg`,
    color1: '#010084',
    color2: '#009DFF',
  },
  {
    title: 'Deslumbra con accesorios y joyería para toda ocasión.',
    text: 'Encuentra joyería con originales diseños de aretes, collares y pulseras.',
    linkText: 'Luce espléndida',
    link: '/category/accesorios-y-joyeria-1',
    img: `${baseURL}/unnamed-0.jpg`,
    color1: '#883E00',
    color2: '#FF9F44',
  },
];

export default fakeBanner;
