import React from 'react';
import { NavLink } from 'react-router-dom';
import { trackWithGTM } from '../../Utils/trackingUtils';

import { SEARCH_SECTIONS, APP_SECTIONS } from '../../Constants';
import backArrow from './images/back-arrow.svg';
import pro from './images/pro.svg';
import fakeBanner from './fakeBanner';
let menuElement;

let sizeWindow = 0;
let getDevice = 'desktop';
let changeView = false;
let currentView = 1;
let currentBanner = {};

class NewMenu extends React.Component {
  constructor(props) {
    super(props);
    this.myRef = React.createRef();
    this.state = {
      xPos: 0,
      touch: false,
      subCategory1Selected: false,
      subCategory1MobileVisible: '',
      subCategory1: 0,
      showBack: 'hidden-block',
      subCategory2Selected: false,
      subCategory2MobileVisible: ' hidden-block',
      showSearch: false,
      searchCSS: 'hidden-block',
      searchTerm: '',
      menuCSS: '',
      subCategory2Action: () => {
        if (this.state.subCategory1Selected) {
          return '';
        }
        return ' deactivate-category';
      },
      subCategory3Action: () => {
        if (this.state.subCategory2Selected) {
          return '';
        }
        return ' deactivate-category';
      },
      subCategory2: -1,
      subCategory3Selected: false,
      subCategory3MobileVisible: 'hidden-block',
      subCategory3: -1,
      validateChildren: (e) => {
        return ' disabled-option-menu';

        if (e.hasOwnProperty('children')) {
          if (e.children.length > 0) {
            return '';
          }
        }

        return ' disabled-option-menu';
      },
    };
    this.blockCat1Sensor = false;
    this.blockCat1 = false;
  }

  search = (searchTerm, section) => {
    // Construct search path.
    let searchPath;
    if (!searchTerm == '') {
      if (!section) {
        if (this.props.appSection === APP_SECTIONS.INSPIRE) {
          searchPath = `/search/${SEARCH_SECTIONS.ARTICLES}/${searchTerm}`;
        } else {
          searchPath = `/search/${searchTerm}`;
        }
      } else {
        searchPath = `/search/${section}/${searchTerm}`;
      }
    } else {
      // console.log("No puedes hacer búsquedas vacías!");
    }

    // Push searchPath to browser´s history to navigate.
    this.props.history.push(searchPath);
    //window.location.href =searchPath;
    // Clear searchbox
    this.setState({
      searchTerm: '',
    });

    this.closeMenu();
  };

  changeCat(numCat, number, device, item) {
    changeView = true;
    //console.log('number ',number)
    this.fixLevel3(number);

    if (numCat === 1) {
      this.state.subCategory1 = number;
      this.state.subCategory1Selected = true;
      this.state.subCategory2Selected = false;
      this.state.subCategory3 = -1;
      this.state.subCategory2 = 0;

      this.changeView(2);

      this.forceUpdate();
    } else if (numCat === 2) {
      if (!this.emptyValidate(item)) {
        this.state.subCategory2 = number;
        //this.state.subCategory1Selected = false;
        this.state.subCategory2Selected = true;
        this.state.subCategory3Selected = false;
        this.state.subCategory3 = 0;
        this.changeView(3);
        this.forceUpdate();
      } else {
        return null;
      }
    } else if (numCat === 3) {
      this.state.subCategory3 = number;
      this.state.subCategory3Selected = true;
      //this.changeView(3)
      this.forceUpdate();
    }
    // window.scrollTo(0, 0);
  }

  resize = () => {
    this.validateDevice();
  };

  validateDevice() {
    let getWindow = window.innerWidth;
    sizeWindow = getWindow;
    if (sizeWindow > 768) {
      getDevice = 'desktop';
      this.state.subCategory1MobileVisible = '';
      this.state.subCategory2MobileVisible = '';
      this.state.subCategory3MobileVisible = '';
    } else {
      getDevice = 'mobile';
      this.state.subCategory1MobileVisible = '';
      this.state.subCategory2MobileVisible = ' hidden-block';
      this.state.subCategory3MobileVisible = ' hidden-block';
    }
    this.forceUpdate();
  }

  back() {
    if (currentView > 1) {
      currentView--;
    }
    if (currentView === 1) {
      this.state.subCategory2 = -1;
      this.state.subCategory3 = -1;
    } else if (currentView === 2) {
      this.state.subCategory2 = -1;
      this.state.subCategory3 = -1;
    }

    this.changeView(currentView);
  }
  changeView(num) {
    if (getDevice === 'mobile') {
      if (num === 1) {
        this.state.subCategory1MobileVisible = '';
        this.state.subCategory2MobileVisible = ' hidden-block';
        this.state.subCategory3MobileVisible = ' hidden-block';
        this.state.showBack = 'hidden-block';
      } else if (num === 2) {
        this.state.subCategory1MobileVisible = ' hidden-block';
        this.state.subCategory2MobileVisible = '';
        this.state.subCategory3MobileVisible = ' hidden-block';
        this.state.showBack = '';
      } else if (num === 3) {
        this.state.subCategory1MobileVisible = ' hidden-block';
        this.state.subCategory2MobileVisible = ' hidden-block';
        this.state.subCategory3MobileVisible = '';
        this.state.showBack = '';
      }
    } else {
      this.state.subCategory1MobileVisible = '';
      this.state.subCategory2MobileVisible = '';
      this.state.subCategory3MobileVisible = '';
    }

    changeView = false;
    currentView = num;
    this.forceUpdate();
  }

  componentDidMount() {
    this.validateDevice();
    window.addEventListener('resize', this.resize);
  }
  componentWillUnmount() {
    window.removeEventListener('resize', this.resize);
  }

  // Close window if you leave the active zone
  // Validate that at least below the 300px
  onMouseLeave = () => {
    //return null;
    if (this.state.xPos > 320) {
      if (this.state.touch) {
        this.props.setSubMenu();
        this.state.touch = false;
        this.forceUpdate();
      }
    }
  };
  closeMenu() {
    //return null;
    this.props.setSubMenu();
    this.state.touch = false;
    this.forceUpdate();
  }
  //Validate if the user opened the menu
  mouseOver = () => {
    if (getDevice === 'desktop') {
      this.state.touch = false;
      this.forceUpdate();
    }
  };

  openSearch() {
    // this.status.showSearch = true;
    // this.state.searchCSS = 'hidden-block';
    // this.state.menuCSS = '';
    // this.forceUpdate();
  }

  onMouseMove(e) {
    this.setState({ xPos: e.screenY });
  }

  validateArray(arr) {
    if (arr.hasOwnProperty('children')) {
      return arr['children'];
    }
    return [];
  }

  level1() {
    const { marketCategories } = this.props;

    if (marketCategories) {
      return marketCategories;
    }
    return [];
  }

  level2() {
    const { marketCategories } = this.props;

    if (marketCategories[this.state.subCategory1].hasOwnProperty('children')) {
      if (marketCategories[this.state.subCategory1].children.length > 0) {
        return marketCategories[this.state.subCategory1].children;
      }
    }

    return [];
  }

  level3() {
    const { marketCategories } = this.props;

    if (marketCategories[this.state.subCategory1].hasOwnProperty('children')) {
      if (marketCategories[this.state.subCategory1].children.length > 0) {
        //return marketCategories[this.state.subCategory1].children;
        if (
          marketCategories[this.state.subCategory1].children[this.state.subCategory2]
        ) {
          if (
            marketCategories[this.state.subCategory1].children[
              this.state.subCategory2
            ].hasOwnProperty('children')
          ) {
            if (
              marketCategories[this.state.subCategory1].children[
                this.state.subCategory2
              ].children.length > 0
            ) {
              return marketCategories[this.state.subCategory1].children[
                this.state.subCategory2
              ].children;
            }
          }
        }
      }
    }

    return [];
  }

  fixLevel3 = (number) => {
    const { marketCategories } = this.props;

    if (marketCategories[this.state.subCategory1]) {
      if (marketCategories[this.state.subCategory1].hasOwnProperty('children')) {
        if (marketCategories[this.state.subCategory1].children[number]) {
          if (
            marketCategories[this.state.subCategory1].children[
              number
            ].hasOwnProperty('children')
          ) {
            if (
              marketCategories[this.state.subCategory1].children[number].children
                .length === 0
            ) {
              this.state.subCategory2 = -1;
              this.state.subCategory3 = -1;
              this.forceUpdate();
            }
          }
        }
      }
    }
  };

  setFakeBanners(original) {
    // Import fakeBanner
    for (let x in original) {
      original[x]['banner'] = fakeBanner[0];
    }
    return original;
  }

  getFirstCat() {
    const { marketCategories } = this.props;

    //if (this.state.subCategory1 >= 0) {
    if (this.state.subCategory1 < 0) {
      return { __html: `` };
    } else {
      this.setFakeBanners(marketCategories);
      currentBanner = marketCategories[this.state.subCategory1];
      let link = `/category/${marketCategories[this.state.subCategory1].slug}`;
      if (getDevice === 'mobile') {
        return {
          __html: `<a href='${link}' class='pink-link'>${
            marketCategories[this.state.subCategory1].name
          }</a>
          ${this.getViewAll(link)}
          `,
        };
      } else {
        return {
          __html: `<a href='${link}' class='pink-link'>${
            marketCategories[this.state.subCategory1].name
          }</a> /`,
        };
      }

      //}
      return { __html: `` };
    }
    return { __html: `` };
  }

  getSecondCat() {
    const { marketCategories } = this.props;

    if (this.state.subCategory2 < 0) {
      return { __html: `` };
    } else {
      if (this.state.subCategory2 >= 0) {
        if (marketCategories[this.state.subCategory1].hasOwnProperty('children')) {
          if (this.state.subCategory2Selected) {
            if (marketCategories[this.state.subCategory1].children.length > 0) {
              let link = `/category/${
                marketCategories[this.state.subCategory1].children[
                  this.state.subCategory2
                ].slug
              }`;
              if (getDevice === 'mobile') {
                return {
                  __html: `<a href='${link}' class='pink-link'>${
                    marketCategories[this.state.subCategory1].children[
                      this.state.subCategory2
                    ].name
                  }</a>
                  ${this.getViewAll(link)}
                  `,
                };
              } else {
                return {
                  __html: `<a href='${link}' class='pink-link'>${
                    marketCategories[this.state.subCategory1].children[
                      this.state.subCategory2
                    ].name
                  }</a> /`,
                };
              }
            }
          }
        }
      }
    }
    return { __html: `` };
  }

  getThirdCat() {
    const { marketCategories } = this.props;

    if (this.state.subCategory2 < 0) {
      return { __html: `` };
    } else {
      if (this.state.subCategory3 >= 0) {
        if (this.state.subCategory3Selected) {
          if (
            marketCategories[this.state.subCategory1].children[
              this.state.subCategory2
            ].hasOwnProperty('children')
          ) {
            if (
              marketCategories[this.state.subCategory1].children[
                this.state.subCategory2
              ].children.length > 0
            ) {
              let link = `/category/${
                marketCategories[this.state.subCategory1].children[
                  this.state.subCategory2
                ].children[this.state.subCategory3].name
              }`;
              if (getDevice === 'mobile') {
                return {
                  __html: `<a href='${link}' class='pink-link'>${
                    marketCategories[this.state.subCategory1].children[
                      this.state.subCategory2
                    ].children[this.state.subCategory3].name
                  }</a>
                  ${this.getViewAll(link)}
                  `,
                };
              } else {
                return {
                  __html: `/ <a href='${link}' class='pink-link'>${
                    marketCategories[this.state.subCategory1].children[
                      this.state.subCategory2
                    ].children[this.state.subCategory3].name
                  }</a> /`,
                };
              }
            }
          }
        }
      }
    }
    return { __html: `` };
  }

  getViewAll(link) {
    return `<span class="view-all"><a href='${link}'>(Ver todo)</a></span>`;
  }

  titleMenu() {
    if (getDevice === 'mobile') {
      if (currentView === 1) {
        //return <span dangerouslySetInnerHTML={this.getFirstCat()} />;
      } else if (currentView === 2) {
        return <span dangerouslySetInnerHTML={this.getFirstCat()} />;
      } else if (currentView === 3) {
        return <span dangerouslySetInnerHTML={this.getSecondCat()} />;
      }
    } else {
      return (
        <div>
          <span dangerouslySetInnerHTML={this.getFirstCat()} />
          <span dangerouslySetInnerHTML={this.getSecondCat()} />
          <span dangerouslySetInnerHTML={this.getThirdCat()} />
        </div>
      );
    }
  }

  setLinksDesktop(e, item) {
    if (getDevice === 'mobile') {
      if (!this.emptyValidate(item)) {
        e.preventDefault();
      } else {
        this.closeMenu();
      }
    } else {
      this.closeMenu();
    }
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.typeMenu === 'search') {
      this.state.searchCSS = '';
      this.state.menuCSS = ' hidden-block';
      this.forceUpdate();
    } else {
      this.state.searchCSS = 'hidden-block';
      this.state.menuCSS = ' ';
      this.forceUpdate();
    }
    if (this.props.showMenu) {
      this.changeView(1);
    }
  }

  /**
   * onChangeSearchTerm()
   * This method controls the search input element
   * @param {object} e : Form Event
   */
  onChangeSearchTerm = (e) => {
    e.preventDefault();
    const { value, name } = e.target;
    this.setState({
      [name]: value,
    });
  };

  emptyValidate(item) {
    if (item.hasOwnProperty('children')) {
      if (item.children.length > 0) {
        return false;
      }
    }
    return ' menu-last-category';
  }

  // componentWillReceiveProps(){
  //   console.log(" this.myRef ", this.myRef);
  // }
  getCurrentBanner() {
    if (currentBanner.hasOwnProperty('banner')) {
      return currentBanner.banner;
    } else {
      return {
        img: '',
        link: '',
        text: '',
        linkText: '',
        title: '',
      };
    }
  }

  render() {
    const { appSection } = this.props;

    if (this.props.showMenu) {
      return (
        <div
          onMouseLeave={() => this.onMouseLeave()}
          onMouseOver={() => this.mouseOver()}
          onMouseMove={this.onMouseMove.bind(this)}
          className="upper-menu__search-mobile-container-main"
          id="main-submenu"
          ref={this.myRef}
        >
          <div
            className={'upper-menu__search-mobile-container ' + this.state.searchCSS}
          >
            <div className="upper-menu__search-mobile-input">
              <form
                className="form upper-menu__search-mobile-form"
                onSubmit={(e) => {
                  // e.preventDefault();
                  this.search(this.state.searchTerm);
                }}
              >
                <input
                  autoFocus
                  type="search"
                  placeholder="Encuéntralo..."
                  name="searchTerm"
                  autoComplete="off"
                  value={this.state.searchTerm}
                  onChange={this.onChangeSearchTerm}
                  className="input-search"
                />
                <input type="submit" value="Buscar" className="cr__inputSearchBtn" />
              </form>
            </div>
          </div>

          <div className={'sub-menu ' + this.state.menuCSS}>
            {/* ------------------ Category ------------------ */}
            <div className="sub-menu__space sub-menu__categories">
              <div
                className="upper-menu__controller upper-menu__controller-mobile"
                style={{ overflow: 'auto' }}
              >
                <div
                  className="upper-menu__sections"
                  style={{ justifyContent: 'flex-start' }}
                >
                  <div className="upper-menu__sections-block upper-menu__sections-market cursor ">
                    <div className="hoverMenu">
                      <NavLink
                        itemProp="url"
                        to="/landing/Mercadito-Verde-Healthy-Week"
                        id="Landing Tab"
                        className="link gtm_link_click"
                        activeClassName="link--active"
                        isActive={(match, loc) =>
                          appSection === APP_SECTIONS.LANDING &&
                          RegExp('^/landing/Mercadito-Verde-Healthy-Week').test(
                            loc.pathname,
                          )
                        }
                        onClick={() => {
                          trackWithGTM(
                            'NavInteraction',
                            'Mercadito Verde Healthy week',
                            'MainMenu-Collapse',
                          );
                        }}
                      >
                        MERCADITO VERDE
                      </NavLink>
                    </div>
                  </div>
                  <div className="upper-menu__sections-block upper-menu__sections-market cursor ">
                    <div className="hoverMenu" onClick={this.openMenu}>
                      <NavLink
                        itemProp="url"
                        to="/landing/comprarosa"
                        className="link"
                        activeClassName="link--active"
                        onClick={(e) => {
                          this.props.setSubMenu();
                          trackWithGTM(
                            'NavInteraction',
                            'Compra Rosa',
                            'MainMenu-Top',
                          );
                        }}
                        isActive={(match, loc) =>
                          appSection === APP_SECTIONS.LANDING &&
                          RegExp('^/landing/comprarosa').test(loc.pathname)
                        }
                      >
                        #COMPRAROSA
                      </NavLink>
                    </div>
                  </div>
                  {/* <div className="upper-menu__sections-block upper-menu__sections-market cursor ">
                    <div className="hoverMenu" onClick={this.openMenu}>
                      <NavLink
                        itemProp="url"
                        to="/landing/wellness"
                        className="link"
                        activeClassName="link--active"
                        onClick={(e) => {
                          this.props.setSubMenu();
                          trackWithGTM(
                            'NavInteraction',
                            'WELLNESS',
                            'MainMenu-Top'
                          );
                        }}
                        isActive={(match, loc) =>
                          appSection === APP_SECTIONS.LANDING &&
                          RegExp('^/landing/wellness').test(loc.pathname)
                        }
                      >
                        WELLNESS
                      </NavLink>
                    </div>
                  </div> */}
                  <div className="upper-menu__sections-block upper-menu__sections-market cursor ">
                    <div className="hoverMenu" onClick={this.openMenu}>
                      <NavLink
                        itemProp="url"
                        to="/landing/combate-la-diseminacion-del-virus-covid-19"
                        className="link"
                        activeClassName="link--active"
                        onClick={(e) => {
                          this.props.setSubMenu();
                        }}
                        isActive={(match, loc) =>
                          RegExp(
                            '^/landing/combate-la-diseminacion-del-virus-covid-19',
                          ).test(loc.pathname) && appSection === APP_SECTIONS.LANDING
                        }
                      >
                        CUBREBOCAS
                      </NavLink>
                    </div>
                  </div>
                  <div className="upper-menu__sections-block upper-menu__sections-market cursor ">
                    <div className="hoverMenu" onClick={this.openMenu}>
                      <NavLink
                        itemProp="url"
                        to="/landing/camp"
                        className="link"
                        activeClassName="link--active"
                        onClick={(e) => {
                          this.props.setSubMenu();
                        }}
                        isActive={(match, loc) =>
                          RegExp('^/landing/camp').test(loc.pathname) &&
                          appSection === APP_SECTIONS.LANDING
                        }
                      >
                        #ENCASA
                      </NavLink>
                    </div>
                  </div>
                  <div className="upper-menu__sections-block upper-menu__sections-market cursor ">
                    <div className="hoverMenu" onClick={this.openMenu}>
                      <NavLink
                        itemProp="url"
                        to="/landing/promociones"
                        className="link"
                        activeClassName="link--active"
                        onClick={(e) => {
                          this.props.setSubMenu();
                        }}
                        isActive={(match, loc) =>
                          RegExp('^/landing/promociones').test(loc.pathname) ||
                          appSection === APP_SECTIONS.LANDING
                        }
                      >
                        PROMOS
                      </NavLink>
                    </div>
                  </div>
                  <div className="upper-menu__sections-block upper-menu__sections-inspire cursor">
                    <div className="hoverMenu">
                      <NavLink
                        itemProp="url"
                        to="/inspire"
                        className="link"
                        onClick={(e) => {
                          this.props.setSubMenu();
                        }}
                        activeClassName="link--active"
                      >
                        INSPIRE
                      </NavLink>
                    </div>
                  </div>
                  <div className="upper-menu__sections-block upper-menu__sections-pro cursor">
                    <div className="hoverMenu">
                      <NavLink
                        itemProp="url"
                        to="/pro"
                        className="link"
                        onClick={(e) => {
                          this.props.setSubMenu();
                        }}
                        activeClassName="link--active"
                      >
                        PRO
                      </NavLink>
                    </div>
                  </div>
                </div>
              </div>

              <div className="sub-menu__breadcrump-merge">
                <div
                  className={
                    'sub-menu__breadcrump sub-menu__breadcrump-back ' +
                    this.state.showBack
                  }
                >
                  <div
                    onClick={() => {
                      this.back();
                    }}
                    className="back-arrow-container"
                  >
                    <img src={backArrow} className="back-arrow" /> Atrás
                  </div>
                </div>

                <div className="sub-menu__breadcrump sub-menu__breadcrump-category">
                  {/* Categorías {this.state.subCategory1} {this.state.subCategory2}{" "} */}
                  {/* Categorías: */}

                  {this.titleMenu()}
                </div>
              </div>

              <div className="sub-menu__categories-levels">
                <div
                  className={
                    'sub-menu__categories-level1 ' +
                    this.state.subCategory1MobileVisible
                  }
                >
                  {/* ------------------ First level ------------------ */}
                  {this.level1().map((item, i) => (
                    <div
                      className={`menu-category ${
                        item.children.length ? 'menu-category--arrow' : ''
                      }`}
                      key={i}
                      onMouseOver={() => {
                        if (getDevice === 'desktop') {
                          this.changeCat(1, i, 'desktop', item);
                        }
                      }}
                      onClick={() => {
                        this.changeCat(1, i, 'mobile', item);
                      }}
                    >
                      <div
                        className={`text-category ${this.state.validateChildren(
                          item,
                        )}`}
                      >
                        <NavLink
                          itemProp="url"
                          className="link"
                          activeClassName="link--active"
                          to={
                            item.type && item.type === 'landing'
                              ? `/landing/${item.slug}`
                              : `/category/${item.slug}`
                          }
                          onClick={(e) => this.setLinksDesktop(e, item)}
                        >
                          {item.name}
                        </NavLink>
                      </div>
                    </div>
                  ))}

                  {/* ------------------ Envios nacionales ------------------ */}

                  {/*
                    <div className="menu-category menu-last-category">
                      <div className="text-category ">
                        <NavLink
                          itemProp="url"
                          className="link "
                          activeClassName="link--active"
                          onClick={e => this.closeMenu()}
                          to="/category/envio-nacional"
                        >
                          Envíos Nacionales
                        </NavLink>
                      </div>
                    </div>
                  */}

                  {/* ------------------ /First level ------------------ */}
                </div>

                <div
                  className={`sub-menu__categories-level2 ${this.state.subCategory2MobileVisible}`}
                >
                  {/* ------------------ Envios nacionales ------------------ */}
                  {/* ------------------ Second level ------------------ */}

                  {this.level2().map((item, i) => (
                    <div
                      className={`menu-category 
                        ${this.state.validateChildren(item)} 
                        ${this.emptyValidate(item)} 
                        ${item.children.length ? 'menu-category--arrow' : ''}`}
                      key={i}
                      onMouseOver={() => {
                        if (getDevice === 'desktop') {
                          this.changeCat(2, i, 'desktop', item);
                        }
                      }}
                      onClick={() => {
                        this.changeCat(2, i, 'mobile', item);
                      }}
                    >
                      <div className="text-category">
                        <NavLink
                          itemProp="url"
                          className="link"
                          activeClassName="link--active"
                          to={`/category/${item.slug}`}
                          onClick={(e) => this.setLinksDesktop(e, item)}
                        >
                          {item.name}
                        </NavLink>
                      </div>
                    </div>
                  ))}
                  {/* ------------------ Second level ------------------ */}
                </div>
                <div
                  className={
                    'sub-menu__categories-level3 ' +
                    this.state.subCategory3Action() +
                    this.state.subCategory3MobileVisible
                  }
                >
                  {/* ------------------ Third level ------------------ */}
                  {this.level3().map((item, i) => (
                    <div
                      className={
                        'menu-category menu-last-category' +
                        this.state.validateChildren(item)
                      }
                      key={i}
                      onClick={() => {
                        this.props.setSubMenu();
                      }}
                    >
                      <div className="text-category">
                        <NavLink
                          itemProp="url"
                          className="link"
                          activeClassName="link--active"
                          to={`/category/${item.slug}`}
                          //onClick={e => this.setLinksDesktop(e, item)}
                        >
                          {item.name}
                        </NavLink>
                      </div>
                    </div>
                  ))}
                  {/* ------------------ Third level ------------------ */}
                </div>
              </div>
            </div>

            {/* ------------------ Banner Text ------------------ */}
            <div
              className="sub-menu__space sub-menu__banner gradient-1"
              style={{
                background: `linear-gradient(45deg, ${
                  this.getCurrentBanner().color1
                } 0%, ${this.getCurrentBanner().color2} 100%)`,
              }}
            >
              <div className="sub-menu__banner-container">
                <div className="sub-menu__banner-container-title">
                  {this.getCurrentBanner().title}
                </div>
                <div className="sub-menu__banner-container-description">
                  {this.getCurrentBanner().text}
                </div>
                <div className="sub-menu__bannr-container-action">
                  {this.getCurrentBanner().linkText !== '' && (
                    <div className="sub-menu__banner-container-button cursor">
                      <NavLink
                        to={this.getCurrentBanner().link}
                        onClick={(e) => this.closeMenu()}
                      >
                        {this.getCurrentBanner().linkText}
                      </NavLink>
                    </div>
                  )}
                </div>
              </div>
            </div>

            {/* ------------------ Banner IMG ------------------ */}
            <div
              className="sub-menu__space sub-menu__image"
              style={{
                backgroundImage: `url(" ${this.getCurrentBanner().img}")`,
              }}
            />
          </div>
        </div>
      );
    }

    return null;
  }
}

export default NewMenu;
