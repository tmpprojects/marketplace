import React from 'react';
import { Link, NavLink } from 'react-router-dom';

import shoppingCart from './images/shopping-cart.svg';
import searchPink from './images/search_pink.svg';
import search from '../../../images/icons/search/search_icon.svg';
import logo from './images/logo_horizontal.svg';
import pro from './images/pro.svg';
import loginSVG from './images/login.svg';

import logoBasic from './images/logo_symbol.svg';
import hamburguerMenu from './images/hamburguer-menu.svg';
import { SEARCH_SECTIONS, APP_SECTIONS } from '../../Constants';
import Login from '../../Utils/Login.jsx';
import ProfileMenuDropdown from '../ProfileMenuDropdown.jsx';
import Submenu from './../../../images/icons/arrows/arrow_submenu.svg';
import { trackWithGTM } from './../../Utils/trackingUtils';

let getDevice = 'desktop';
let sizeWindow = 0;

class NewUpperMenu extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      use: true,
      searchTerm: '',
      isProfileMenuOpen: false,
    };
  }

  componentDidMount() {
    this.validateDevice();
    window.addEventListener('resize', this.resize);
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.resize);
  }

  resize = () => {
    this.validateDevice();
  };

  validateDevice() {
    sizeWindow = window.innerWidth;
    if (sizeWindow > 768) {
      getDevice = 'desktop';
    } else {
      getDevice = 'mobile';
    }
  }

  /**
   * onChangeSearchTerm()
   * This method controls the search input element
   * @param {object} e : Form Event
   */
  onChangeSearchTerm = (e) => {
    e.preventDefault();
    const { value, name } = e.target;
    this.setState({
      [name]: value,
    });
  };

  /**
   * search()
   * Determine the search parameters and redirect users to Search Results Page
   * @param {string} searchTerm : String to search
   * @param {string} section : String that indicates where we should make the search
   */
  search = (searchTerm = '', section) => {
    // Encode search term.
    // Double encoding is made on porpouse to force encoding the '%' sign.
    const encodedTerm = encodeURIComponent(encodeURIComponent(searchTerm)); //Dejado provisionalmente

    // Construct search path.
    let searchPath;
    if (!searchTerm == '') {
      if (!section) {
        if (this.props.appSection === APP_SECTIONS.INSPIRE) {
          searchPath = `/search/${SEARCH_SECTIONS.ARTICLES}/${encodedTerm}`;
        } else {
          //searchPath = `/search/products/${encodedTerm}`;
          searchPath = `/search/${encodedTerm}`;
        }
      } else {
        searchPath = `/search/${section}/${encodedTerm}`;
      }
    } else {
      // console.log("No puedes hacer búsquedas vacías!");
    }

    // Push searchPath to browser´s history to navigate.
    this.props.history.push(searchPath);

    // Clear searchbox
    this.setState({
      searchTerm: '',
    });

    //Send event to GTM
    trackWithGTM('Search', searchTerm);
  };

  openMenu = () => {
    this.props.setSubMenu('normal');
  };

  openMenuRollOver = () => {
    if (!this.props.stateMenu) {
      this.props.setSubMenu('normal');
    } else {
      this.props.closeMenu();
    }
  };

  closeSubMenu = () => {
    this.validateDevice();
    if (getDevice === 'desktop') {
      this.props.closeMenu();
    }
  };

  openMenuSearch = () => {
    if (!this.props.stateMenu) {
      this.props.setSubMenu('search');
    } else {
      this.props.closeMenu();
    }
  };

  /*
   * Open Dropdown Menu
   * @param {string} targetMenu : Which menu should we open?
   */
  openDropdownMenu = () => {
    this.setState({
      isProfileMenuOpen: true,
    });
  };

  /*
   * Close All Dropdown Menus
   */
  closeDropDownMenu = () => {
    this.setState({
      isProfileMenuOpen: false,
    });
  };

  render() {
    const {
      cartProductsCount,
      openModalBox,
      isLogged,
      isMobile,
      appSection,
      user,
    } = this.props;
    const { isProfileMenuOpen } = this.state;

    return (
      <React.Fragment>
        <div className="upper-menu" itemScope role="navigation">
          <div
            className="upper-menu__logo"
            itemProp="logo"
            onMouseOver={this.closeSubMenu}
          >
            <Link
              to="/"
              itemProp="url"
              id="CR Logo - Uppermenu"
              className="gtm_link_click"
              // onClick={e => this.props.setSubMenu()}
            >
              <img
                src={logo}
                className="show-desktop cursor"
                alt="Canasta Rosa"
                aria-label="Canastarosa.com.mx"
              />
            </Link>
            <Link
              to="/"
              itemProp="url"
              id="CR Logo Basic- Uppermenu"
              className="gtm_link_click"
              //onClick={e => this.props.setSubMenu()}
            >
              <img
                src={logoBasic}
                className="show-mobile cursor"
                alt="Home Logo Mobile"
                aria-label="Canastarosa.com.mx"
              />
            </Link>
          </div>

          <div id="upper-menu__new-option" onClick={this.openMenuRollOver}>
            <span>CATEGORÍAS</span>
            <img src={Submenu} />
            <div />
          </div>

          <div className="upper-menu__options" onMouseOver={this.closeSubMenu}>
            {/* ---------------MENU---------------- */}
            <div className="upper-menu__controller upper-menu__controller-1">
              <div className="upper-menu__sections">
                <div className="upper-menu__sections-block upper-menu__sections-market cursor ">
                  <div className="hoverMenu">
                    <NavLink
                      itemProp="url"
                      to="/landing/comprarosa"
                      id="Landing Tab"
                      className="link gtm_link_click"
                      activeClassName="link--active"
                      isActive={(match, loc) =>
                        appSection === APP_SECTIONS.LANDING &&
                        RegExp('^/landing/comprarosa').test(loc.pathname)
                      }
                      onClick={() => {
                        trackWithGTM(
                          'NavInteraction',
                          'Compra Rosa',
                          'MainMenu-Collapse',
                        );
                      }}
                    >
                      #COMPRAROSA
                    </NavLink>
                  </div>
                </div>
                {/* <div className="upper-menu__sections-block upper-menu__sections-market cursor ">
                  <div className="hoverMenu">
                    <NavLink
                      itemProp="url"
                      to="/landing/wellness"
                      id="Landing Tab"
                      className="link gtm_link_click"
                      activeClassName="link--active"
                      isActive={(match, loc) =>
                        appSection === APP_SECTIONS.LANDING &&
                        RegExp('^/landing/wellness').test(loc.pathname)
                      }
                      onClick={() => {
                        trackWithGTM(
                          'NavInteraction',
                          'Wellness',
                          'MainMenu-Collapse'
                        );
                      }}
                    >
                      WELLNESS
                    </NavLink>
                  </div>
                </div> */}
                <div className="upper-menu__sections-block upper-menu__sections-market cursor ">
                  <div className="hoverMenu">
                    <NavLink
                      itemProp="url"
                      to="/landing/combate-la-diseminacion-del-virus-covid-19"
                      id="Landing Tab"
                      className="link gtm_link_click"
                      activeClassName="link--active"
                      isActive={(match, loc) =>
                        appSection === APP_SECTIONS.LANDING &&
                        RegExp(
                          '^/landing/combate-la-diseminacion-del-virus-covid-19',
                        ).test(loc.pathname)
                      }
                    >
                      CUBREBOCAS
                    </NavLink>
                  </div>
                </div>
                <div className="upper-menu__sections-block upper-menu__sections-market cursor ">
                  <div className="hoverMenu">
                    <NavLink
                      itemProp="url"
                      to="/landing/camp"
                      id="Landing Tab"
                      className="link gtm_link_click"
                      activeClassName="link--active"
                      isActive={(match, loc) =>
                        appSection === APP_SECTIONS.LANDING &&
                        RegExp('^/landing/camp').test(loc.pathname)
                      }
                    >
                      #ENCASA
                    </NavLink>
                  </div>
                </div>
                <div className="upper-menu__sections-block upper-menu__sections-market cursor ">
                  <div className="hoverMenu">
                    <NavLink
                      itemProp="url"
                      to="/landing/promociones"
                      id="Landing Tab"
                      className="link gtm_link_click"
                      activeClassName="link--active"
                      isActive={(match, loc) =>
                        appSection === APP_SECTIONS.LANDING &&
                        RegExp('^/landing/promociones').test(loc.pathname)
                      }
                    >
                      PROMOS
                    </NavLink>
                  </div>
                </div>
                <div className="upper-menu__sections-block upper-menu__sections-inspire cursor">
                  <div className="hoverMenu">
                    <NavLink
                      itemProp="url"
                      to="/inspire"
                      id="Inspire Tab"
                      className="link gtm_link_click"
                      activeClassName="link--active"
                    >
                      INSPIRE
                    </NavLink>
                  </div>
                </div>
                <div className="upper-menu__sections-block upper-menu__sections-pro cursor">
                  <div className="hoverMenu">
                    <NavLink
                      itemProp="url"
                      to="/pro"
                      id="Pro Tab"
                      className="link gtm_link_click"
                      activeClassName="link--active"
                    >
                      PRO
                    </NavLink>
                  </div>
                </div>
              </div>
            </div>

            <div className="upper-menu__controller upper-menu__controller-2">
              <div className="upper-menu__separator mobile-separator" />

              {/* ---------------SEARCH MOBILE---------------- */}
              <div
                className="upper-menu__search-mobile"
                onClick={this.openMenuSearch}
              >
                {/*<img src={search} /> */}
              </div>
              <div className="upper-menu__separator only-mobile-separator" />

              {/* ---------------SEARCH---------------- */}
              <div className="upper-menu__search">
                <form
                  className="form"
                  onSubmit={(e) => {
                    e.preventDefault();
                    this.search(this.state.searchTerm);
                  }}
                >
                  <input
                    autoFocus
                    type="search"
                    placeholder="Encuéntralo..."
                    name="searchTerm"
                    autoComplete="off"
                    value={this.state.searchTerm}
                    onChange={this.onChangeSearchTerm}
                    className="input-search"
                  />
                  <input
                    type="submit"
                    value="Buscar"
                    className="cr__inputSearchBtn"
                  />
                </form>
              </div>

              {/* ---------------SHOPPING---------------- */}
              <div className="upper-menu__separator mobile-separator" />
              <div className="upper-menu__shopping">
                {cartProductsCount > 0 && (
                  <div className="upper-menu__shopping-counter">
                    {cartProductsCount}
                  </div>
                )}

                <Link
                  to="/cart/"
                  className="upper-menu__shopping-link gtm_link_click"
                  id="Ver Carrito"
                >
                  <img
                    src={shoppingCart}
                    className="upper-menu__shopping-img cursor"
                    alt="Carrito de Compras"
                    aria-label={`${cartProductsCount} artículos en el carrito`}
                  />
                </Link>
              </div>
              <div className="upper-menu__separator" />

              {/* --------------- LOGIN ---------------- */}
              {!isLogged ? (
                <div>
                  <div className="upper-menu__register upper-menu__register-access">
                    <button
                      type="button"
                      id="menu-login"
                      onClick={() => {
                        openModalBox(Login);
                      }}
                      className="pink-button cursor gtm_click"
                    >
                      Acceso
                    </button>
                  </div>
                  <div
                    className="upper-menu__register-svg"
                    onClick={() => {
                      openModalBox(Login);
                    }}
                  >
                    <img src={loginSVG} alt="Login" />
                  </div>
                </div>
              ) : (
                <div className="upper-menu__profile">
                  <button
                    type="button"
                    className="button gtm_click"
                    id="menu-profile"
                    onClick={() => this.openDropdownMenu()}
                    style={
                      user.profile_photo
                        ? {
                            backgroundImage: `url(${user.profile_photo.small})`,
                          }
                        : {}
                    }
                  >
                    Mi Perfil
                  </button>

                  {isProfileMenuOpen && (
                    <ProfileMenuDropdown
                      closeMenu={this.closeDropDownMenu}
                      active={isProfileMenuOpen}
                      dispatch={this.props.dispatch}
                      user={user}
                      isMobile={isMobile}
                      logout={this.props.logout}
                    />
                  )}
                </div>
              )}

              {/* --------------- OPTIONS ---------------- */}
              <div
                className="upper-menu__register cursor hamburguer-menu-container"
                onClick={this.openMenu}
              >
                <img
                  src={hamburguerMenu}
                  className="hamburguer-menu"
                  alt="Hamburger Menu"
                />
              </div>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default NewUpperMenu;
