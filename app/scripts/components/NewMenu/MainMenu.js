import React from 'react';
import NewMenu from './NewMenu';
import NewUpperMenu from './NewUpperMenu';

import './NewUpperMenu.scss';
import './NewMenu.scss';
import './MainMenu.scss';

let tempMenu = '';
let sizeWindow = 0;
let sizeHeightWindow = 0;
let getDevice = 'desktop';
let scrollCache = false;
let getWindow;
let scrollTop;

class MainMenu extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      showMenu: false,
      use: false,
      stateMenu: false,
    };
  }

  validateDevice() {
    getWindow = window.innerWidth;
    sizeWindow = getWindow;
    sizeHeightWindow = window.innerHeight;

    if (sizeWindow > 720) {
      getDevice = 'desktop';
    } else {
      getDevice = 'mobile';
    }
    //this.forceUpdate();
  }

  setSubMenu = (typeMenu) => {
    // Close onMouseLeave

    let element = document.getElementById('app-container');
    let elementArrow = document.getElementById('upper-menu__new-option');

    if (this.state.showMenu === false) {
      scrollTop = window.pageYOffset || document.documentElement.scrollTop;
    } else {
      window.scrollTo(0, scrollTop);
    }

    let newValueState = !this.state.showMenu;
    this.setState({
      showMenu: newValueState,
      typeMenu: typeMenu,
      stateMenu: newValueState,
    });

    this.validateDevice();

    if (getDevice === 'desktop') {
      if (sizeHeightWindow < 600) {
        if (this.state.showMenu) {
          element.classList.remove('app-container-fixed');
          //window.scrollTo(0, scrollCache);
          elementArrow.classList.remove('upper-menu__new-option-up');
        } else {
          // console.log(" getWindow ", getWindow);
          //scrollCache = scrollTop;
          element.classList.add('app-container-fixed');
          elementArrow.classList.add('upper-menu__new-option-up');
        }
      }
    }

    window.scrollTo(0, scrollTop);
  };

  componentDidMount = () => {
    this.validateDevice();
    document.addEventListener('scroll', this.onScroll);
  };
  componentWillUnmount = () => {
    document.removeEventListener('scroll', this.onScroll);
  };

  onScroll = (e) => {
    if (getDevice === 'desktop') {
      const bodyHeight = document.body.scrollHeight;
      if (bodyHeight > 800) {
        scrollTop = window.pageYOffset || document.documentElement.scrollTop;
        this.closeMenuGlobal();
      }
    }
  };

  closeMenuGlobal = () => {
    // return null;
    let element = document.getElementById('app-container');
    let elementArrow = document.getElementById('upper-menu__new-option');
    element.classList.remove('app-container-fixed');
    elementArrow.classList.remove('upper-menu__new-option-up');
    this.setState({
      showMenu: false,
      typeMenu: '',
      stateMenu: false,
    });
    window.scrollTo(0, scrollTop);
  };

  closeMenu = () => {
    this.closeMenuGlobal();
  };

  render() {
    return (
      <div className="new-menu-container">
        <NewUpperMenu
          setSubMenu={this.setSubMenu}
          appSection={this.props.appSection}
          history={this.props.history}
          cartProductsCount={this.props.cartProductsCount}
          openModalBox={this.props.openModalBox}
          user={this.props.user}
          isLogged={this.props.isLogged}
          isMobile={this.props.isMobile}
          logout={this.props.logout}
          dispatch={this.props.dispatch}
          stateMenu={this.state.stateMenu}
          closeMenu={this.closeMenu}
        />
        <NewMenu
          showMenu={this.state.showMenu}
          typeMenu={this.state.typeMenu}
          setSubMenu={this.setSubMenu}
          stateMenu={this.state.stateMenu}
          history={this.props.history}
          appSection={this.props.appSection}
          marketCategories={this.props.marketCategories}
        />
      </div>
    );
  }
}

export default MainMenu;
