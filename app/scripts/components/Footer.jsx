import React, { Component } from 'react';
import { Link, NavLink } from 'react-router-dom';
import { connect } from 'react-redux';

import { modalBoxActions } from '../Actions';
import Register from '../Utils/Register.jsx';
import {
  ORDERS_PHONE,
  BUTLER_PHONE,
  BUTLER_PHONE_PLAIN_FORMAT,
} from '../Constants/config.constants';

const footerLogo = require('../../images/logo/footer_logo.svg');

class Footer extends Component {
  constructor(props) {
    super(props);
    this.openModalBox = this.openModalBox.bind(this);
  }
  openModalBox(_type) {
    this.props.dispatch(modalBoxActions.open(_type));
  }

  //
  render() {
    return (
      <footer className="">
        <div className="footer wrapper--center">
          <div className="footer__module logo socialMedia">
            <Link to="/" id="CR Logo - Footer" className="logo gtm_link_click">
              <img src={footerLogo} alt="Canasta Rosa" />
            </Link>

            <ul className="socialMedia__list">
              <li className="socialMedia__list__item facebook">
                <div className="socialMedia__list_container">
                  <a
                    id="CR Facebook_Footer"
                    className="gtm_link_click"
                    href="https://www.facebook.com/lacanastarosa/"
                    target="_blank"
                    rel="noopener noreferrer"
                  >
                    Facebook
                  </a>
                </div>
              </li>
              {/* <div className="socialMedia__list_container">
                                <li className="socialMedia__list__item pinterest">
                                    <a href="#">Pinterest</a>
                                </li>
                            </div> */}
              <li className="socialMedia__list__item instagram">
                <div className="socialMedia__list_container">
                  <a
                    id="CR Instagram_Footer"
                    className="gtm_link_click"
                    href="https://www.instagram.com/canastarosa/"
                    target="_blank"
                    rel="noopener noreferrer"
                  >
                    Instagram
                  </a>
                </div>
              </li>
            </ul>
          </div>

          {/* <div className="footer__module newsletter">
                        <h4>Newsletter</h4>

                        <p>Reg&iacute;strate gratis para recibir las &uacute;ltimas noticias 
                        sobre tus tiendas favoritas, lanzamientos de tus art&iacute;culos 
                        favoritos y mucho m&aacute;s.</p>


                        <form action="#">
                            <fieldset>
                                <input 
                                    type="text" 
                                    placeholder="Email" 
                                    name="newsletter__input" 
                                    value="" 
                                />
                                <input type="submit" name="newsletter__submit" value="Enviar" />
                            </fieldset>
                        </form>
                    </div> */}

          <div className="footer__module menu">
            <h4>Canasta Rosa</h4>

            <nav className="menu__navigation">
              <ul className="navigation">
                <li className="navigation__item">
                  <NavLink
                    id="Sobre Canasta Rosa"
                    className="link gtm_link_click"
                    activeClassName="active"
                    to="/about-us"
                    exact
                  >
                    Sobre Canasta Rosa
                  </NavLink>
                </li>
                <li className="navigation__item">
                  <NavLink
                    className="link"
                    activeClassName="active"
                    to="/about-us/sell"
                  >
                    ¿Cómo vender en Canasta Rosa?
                  </NavLink>
                </li>
                <li className="navigation__item">
                  <NavLink
                    id="¿Quiénes Somos?"
                    className="link gtm_link_click"
                    activeClassName="active"
                    to="/about-us/team"
                  >
                    ¿Quiénes Somos?
                  </NavLink>
                </li>
                <li className="navigation__item">
                  <NavLink
                    id="Preguntas Frecuentes"
                    className="link gtm_link_click"
                    activeClassName="active"
                    to="/about-us/faqs"
                  >
                    Preguntas Frecuentes
                  </NavLink>
                </li>
                <li className="navigation__item">
                  <NavLink
                    id="Bolsa de Trabajo"
                    className="link gtm_link_click"
                    activeClassName="active"
                    to="/about-us/jobs"
                  >
                    Bolsa de Trabajo
                  </NavLink>
                </li>

                {!this.props.userIsLogged && (
                  <li className="navigation__item">
                    <NavLink
                      to="#"
                      id="Crea una cuenta"
                      className="link gtm_link_click"
                      onClick={(e) => {
                        e.preventDefault();
                        this.openModalBox(Register);
                      }}
                    >
                      Crea una Cuenta
                    </NavLink>
                  </li>
                )}
                <li className="navigation__item">
                  <NavLink
                    to="/pro"
                    id="Canasta Rosa Pro"
                    className="link gtm_link_click"
                    activeClassName="active"
                  >
                    Canasta Rosa Pro
                  </NavLink>
                </li>
                <li className="navigation__item">
                  <NavLink
                    id="Sobre Canasta Rosa"
                    className="link gtm_link_click"
                    activeClassName="active"
                    to="/descubre"
                    exact
                  >
                    Encuéntralo
                  </NavLink>
                </li>
              </ul>
            </nav>
          </div>

          <nav className="footer__module payment-methods">
            <div className="creditCards">
              <ul className="creditCards__list">
                <li className="creditCards__list__item mastercard">
                  <span>Mastercard</span>
                </li>
                <li className="creditCards__list__item visa">
                  <span>Visa</span>
                </li>
                <li className="creditCards__list__item amex">
                  <span>Amex</span>
                </li>
                {/* <li className="creditCards__list__item paypal">
                  <span>PayPal</span>
                </li> */}
              </ul>

              <ul className="creditCards__list">
                <li className="creditCards__list__item oxxo">
                  <span>OXXO</span>
                </li>
                <li className="bankTransfer">
                  Transferencia <br />
                  Bancaria
                </li>
              </ul>

              <div className="brand__ssl">
                {' '}
                <span>SSL</span>{' '}
              </div>
            </div>
          </nav>

          <div className="footer__module contact">
            <span className="contact__title">¿Tienes alguna duda o comentario?</span>
            <a
              className="gtm_contact_link contact__link"
              id="info@canastarosa.com"
              href="mailto:info@canastarosa.com"
            >
              info@canastarosa.com
            </a>
            <a
              className="gtm_contact_link contact__link"
              id="facturacion@canastarosa.com"
              href="mailto:facturacion@canastarosa.com"
            >
              facturacion@canastarosa.com
            </a>
            <a
              href="https://ayuda.canastarosa.com/"
              target="_blank"
              id="orders phone"
              className="gtm_contact_link contact__link"
              rel="noopener"
            >
              Ayuda
            </a>
            <a
              className="gtm_contact_link contact__link phone"
              id="orders phone"
              href={`tel:+52-5575838134`}
            >
              55 7583 8134
            </a>
            {/*
                        Temporarily disable in February  
                        <a 
                            target="_blank"
                            rel="noopener noreferrer"
                            className="gtm_contact_link contact__link whatsapp" 
                            id="butler whatsapp"
                            href={`https://api.whatsapp.com/send?phone=${BUTLER_PHONE_PLAIN_FORMAT}`} 
                        >{BUTLER_PHONE}</a>
                        */}
          </div>
        </div>

        <div className="bottomLine">
          <ul className="bottomLine__navigation wrapper--center">
            <li className="bottomLine__navigation__item">
              Canasta Rosa © {new Date().getFullYear()}
            </li>
            <li className="bottomLine__navigation__item">
              <Link
                to="/legales/terminos-condiciones"
                id="Términos y Condiciones"
                className="gtm_link_click"
              >
                T&eacute;rminos y Condiciones
              </Link>
            </li>
            <li className="bottomLine__navigation__item">
              <Link
                to="/legales/privacidad"
                id="Aviso Legal"
                className="gtm_link_click"
              >
                Aviso Legal
              </Link>
            </li>
            <li className="bottomLine__navigation__item">
              <Link
                to="/legales/politicas-de-envio"
                id="Aviso Legal"
                className="gtm_link_click"
              >
                Políticas de Envío
              </Link>
            </li>
          </ul>
        </div>
      </footer>
    );
  }
}

export default connect((state) => ({
  userIsLogged: state.users.isLogged,
}))(Footer);
