import React from 'react';
import { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { MenuSections } from './MenuSections';
import CollapseBox from '../../Utils/CollapseBox';
import faqsList from '../../../statics/faqs.json';
import { MainBanner } from '../Home/MainBanner';
import { PICKUP_POINT } from '../../Constants/config.constants';
import { appActions } from '../../Actions';

const createSlide = (faq, index) => (
  <li className="slide" key={index}>
    <div className="slide__data">
      <h1 className="title">{faq.question}</h1>
      <p dangerouslySetInnerHTML={{ __html: faq.answer }} />
    </div>
  </li>
);

class Jobs extends Component {
  componentDidMount() {
    this.props.getJobOpenings();
  }

  checkIfNull = (className) => {
    if (!className) {
      return 'hide';
    }
    return '';
  };

  render() {
    const { jobOpenings } = this.props;
    return (
      <section className="about_jobs">
        <div className="jobsBanner"></div>
        <MenuSections />

        <div className="jobs">
          <h3 className="jobs__title">Vacantes</h3>
          <ul className="jobs__list">
            {jobOpenings.results.map((job, i) => {
              const answer = `
                            <div class="job">
                                <div class="${this.checkIfNull(
                                  job.area,
                                )}"><span class="subtitle">Área: </span>${
                job.area
              }</div>
                                <div class="${this.checkIfNull(
                                  job.modality,
                                )}"><span class="subtitle">Modalidad: </span>${
                job.modality
              }</div>
                                <div class="${this.checkIfNull(
                                  job.schedule,
                                )}"><span class="subtitle">Horario: </span>${
                job.schedule
              }</div>
                                <div class="${this.checkIfNull(
                                  job.location,
                                )}"><span class="subtitle">Ubicación: </span>${
                job.location
              }</div>
                                
                                <div class="section ${this.checkIfNull(
                                  job.job_description,
                                )}">
                                    <div>
                                        ${job.job_description}
                                    </div>
                                </div>
                                <div class="section ${this.checkIfNull(
                                  job.responsibilities,
                                )}">
                                    <div class="subtitle">Responsabilidades:</div>
                                    <div class="content">
                                        ${job.responsibilities}
                                    </div>
                                </div>

                                <div class="section ${this.checkIfNull(
                                  job.education,
                                )}">
                                    <div class="subtitle">Educación / Conocimiento / Experiencia / Habilidades Técnicas:</div>
                                    <div class="content">
                                        <p>${job.education}</p>
                                    </div>
                                </div>

                                <div class="section ${this.checkIfNull(
                                  job.interpersonal_skills,
                                )}">
                                    <div class="subtitle">Habilidades Interpersonales:</div>
                                    <div class="content">
                                        ${job.interpersonal_skills}
                                    </div>
                                </div>

                                <div class="section">
                                    <div class="${this.checkIfNull(
                                      job.salary,
                                    )}"><span class="subtitle">Salario: </span>${
                job.salary
              }</div>
                                </div>

                                <div class="section ${this.checkIfNull(
                                  job.about_us,
                                )}">
                                    <div class="subtitle">¿A quién buscamos?</div>
                                    <div>
                                        ${job.about_us}
                                    </div>
                                </div>

                                <div class="section contact">
                                    <div class="mail">
                                    Envía tu CV actualizado a
                                    <a class="mail__link" href="mailto:rh@canastarosa.com?subject=${
                                      job.title
                                    }">rh@canastarosa.com</a>
                                    </div>
                                    <div>
                                        <button class="button-square--pink" onclick="window.location.href='mailto:rh@canastarosa.com?subject=${
                                          job.title
                                        }';">
                                            Aplica ahora
                                        </button>
                                    <div>
                                </div>
                            </div>`;

              // <a href="mailto:gabriela.fuentes@canastarosa.com?subject=${job.title}">Contáctanos</a>`;
              return (
                <li key={i}>
                  <CollapseBox
                    content={<div dangerouslySetInnerHTML={{ __html: answer }} />}
                    header={job.title}
                  />
                </li>
              );
            })}
          </ul>
        </div>
      </section>
    );
  }
}

// Pass Redux state and actions to component
function mapStateToProps(state) {
  const { jobOpenings } = state.app;
  return {
    jobOpenings,
  };
}

function mapDispatchToProps(dispatch) {
  const { getJobOpenings } = appActions;
  return bindActionCreators(
    {
      getJobOpenings,
    },
    dispatch,
  );
}

// Export Component
export default connect(mapStateToProps, mapDispatchToProps)(Jobs);
