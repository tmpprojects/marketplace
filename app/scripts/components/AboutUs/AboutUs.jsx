import React from 'react';
import { MenuSections } from './MenuSections';

export default () => (
  <section className="about-cr">
    <div className="cover">
      <h1 className="title">
        <span>Creces tú,</span> crecemos todos
      </h1>
    </div>

    <MenuSections />

    <div className="sections">
      <div className="about-cr__information">
        <p className="info info--big">
          Canasta Rosa es un lugar para tu trabajo, tus ideas y tu creatividad.
          <br />
          Un lugar para ti, donde puedes escribir tu historia y difundirla con los
          demás.
          <br />
          Donde puedes trazar el camino de dónde estás y dónde quieres estar.
        </p>
        <p className="info">
          Canasta Rosa es la plataforma, construida por y para mentes creativas
          independientes, que ayuda a crecer a las mujeres emprendedoras por medio de
          sus tres vertientes:
        </p>
      </div>

      <div className="inspire sections__module">
        <div className="img">
          <img src={require('images/aboutUs/inspire.png')} alt="" />
          <h5 className="title">INSPIRE</h5>
        </div>
        <div className="txt">
          <h5 className="title">INSPIRE</h5>
          <blockquote className="quote">
            Valoramos la artesanía, la manualidad y la originalidad en todo lo que
            hacemos.
          </blockquote>
          <p>
            ¿Estás planeando una fiesta? Inspírate con las{' '}
            <span>nuevas tendencias</span> en decoración y comida.
            <br />
            ¿Buscas un regalo especial? Ve tutoriales sobre como crear{' '}
            <span>regalos únicos</span>. Encuentra{' '}
            <span>contenido curado e innovador</span> de miles de los mejores sitios
            web y blogs.
          </p>
        </div>
      </div>

      <div className="market sections__module">
        <div className="img">
          <img src={require('images/aboutUs/market.png')} alt="" />
          <h5 className="title">MARKET</h5>
        </div>
        <div className="txt">
          <h5 className="title">MARKET</h5>
          <blockquote className="quote">
            Ayudamos a potenciales clientes a descubrir cientos de increíbles
            productos y extraordinarios regalos, elaborados a mano y confeccionados
            con amor hasta el último detalle.
          </blockquote>
          <p>
            Encuentra miles de <span>productos únicos</span> elaborados{' '}
            <span>artesanalmente</span> por emprendedores.
            <br />
            Tenemos un escaparate ideal para todas tus creaciones. Algunas de
            nuestras categorías más populares incluyen: Comida, Saludable, Diseño y
            muchas más.
          </p>
        </div>
      </div>

      <div className="tribe sections__module">
        <div className="img">
          <img src={require('images/aboutUs/tribe.png')} alt="" />
          <h5 className="title">PRO</h5>
        </div>
        <div className="txt">
          <h5 className="title">PRO</h5>
          <blockquote className="quote">
            En medio de un mundo automatizado y genérico, proporcionamos a las
            vendedoras herramientas y medios para que puedan convertir sus sueños e
            ideas en oportunidades de negocio.
          </blockquote>
          <p>
            Encuentra <span>servicios y productos</span> diseñados especialmente para
            facilitar tu <span>camino como emprendedor</span>. Saca provecho de
            nuestros artículos, videos, herramientas y entérate de nuestros
            exclusivos eventos, clases y actividades.
          </p>
        </div>
      </div>
    </div>
  </section>
);
