import React from 'react';
import { MenuSections } from './MenuSections';

export default () => (
  <section className="about_team">
    <div className="cover">
      <h1 className="title">
        Creemos que la magía existe cuando se junta
        <br /> <span>la pasión y la acción</span>
      </h1>
      <MenuSections />
    </div>

    <div className="info_container wrapper--center">
      <p className="subtitle">¡HOLA!</p>
      <h5 className="title">¿Quiénes Somos?</h5>

      <p>
        Somos emprendedores, por eso sabemos que la pasión y dedicación de otros
        emprendedores dan a luz productos y servicios que generan experiencias con un
        toque único y especial.
      </p>
      <p>
        Nuestro compromiso es con el empoderamiento y desarrollo de las mujeres
        emprendedoras.
        <br />
        Nuestra misión es crear un entorno donde las mujeres puedan cumplir sus
        objetivos personales y profesionales a través de sus ideas, productos y
        sueños.
      </p>
      <p>
        Pensamos que el mundo necesita menos de lo mismo y más de lo original, de lo
        auténtico, de lo libre.
        <br />
        Por eso, queremos poner tus creaciones al alcance de nuestros clientes y
        compartir un poco de tu historia de éxito con el mundo.
      </p>
    </div>

    <div className="team">
      <div className="cover team_cover">
        <h4 className="title">
          <span>Creamos,</span> herramientas con propósito.
        </h4>
      </div>
      <h5 className="title">El equipo</h5>

      <ul className="members-list">
        <li className="member">
          <div className="img_container">
            <img src={require('images/aboutUs/team.png')} alt="" />
          </div>
          <p className="name">
            Deborah Dana <span className="role">Founder & CEO</span>
          </p>
        </li>
        <li className="member">
          <div className="img_container">
            <img src={require('images/aboutUs/team2.png')} alt="" />
          </div>
          <p className="name">
            Israel Díaz <span className="role">Tech and Creative Leader</span>
          </p>
        </li>
        <li className="member">
          <div className="img_container">
            <img src={require('images/aboutUs/team3.png')} alt="" />
          </div>
          <p className="name">
            Isabel Olea <span className="role">MKT & Social Media</span>
          </p>
        </li>
        <li className="member">
          <div className="img_container">
            <img src={require('images/aboutUs/team5.png')} alt="" />
          </div>
          <p className="name">
            Karina Vazquez <span className="role">UX/UI & Frontend Developer</span>
          </p>
        </li>
        <li className="member">
          <div className="img_container">
            <img src={require('images/aboutUs/team6.png')} alt="" />
          </div>
          <p className="name">
            Leonardo Ram&iacute;rez <span className="role">Backend Developer</span>
          </p>
        </li>
      </ul>
    </div>
  </section>
);
