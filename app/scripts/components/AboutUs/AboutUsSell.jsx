import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

import config from '../../../config';
import { modalBoxActions, myStoreActions } from '../../Actions';
import FormLogin from '../../Utils/FormLogin';
import CollapseBox from '../../Utils/CollapseBox';
import faqsList from '../../../statics/faqs.json';
import PageHead from '../../Utils/PageHead';
import Register from '../../Utils/Register';
import ComparisonTableWeb from '../StoreApp/Plans/Plans/Tables/ComparisonTableWeb';
import ComparisonTableMobile from '../StoreApp/Plans/Plans/Tables/ComparisonTableMobile';
import './AboutUsSell.scss';

const RequireAccountModal = ({ openModalBox }) => {
  const formSubmit = () => {
    window.location = '/stores/create';
  };

  return (
    <div className="modalWindow requireAccount">
      <div className="message">
        Para crear una tienda gratis, es necesario que ingreses a tu cuenta.
      </div>
      <h4 className="title">Login</h4>
      <FormLogin onFormSubmit={formSubmit} />

      <div className="requireAccount__register">
        <p className="subtitle">¿A&uacute;n no tienes una cuenta?</p>
        <button className="create-account" onClick={() => openModalBox(Register)}>
          Reg&iacute;strate gratis aqu&iacute;
        </button>
      </div>
    </div>
  );
};
class AboutUsSell extends Component {
  state = {
    width: 1200,
  };

  componentDidMount() {
    this.props.getMyStorePlans();
    this.updateDimensions();
    window.addEventListener('resize', this.updateDimensions);
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.updateDimensions);
  }

  updateDimensions = () => {
    this.setState({ width: window.innerWidth });
  };
  /**
   * isUserLogged()
   * Check if user is logged in
   * @param {object} e | Button event
   */
  isUserLogged = (e) => {
    const { isUserLogged } = this.props;
    if (!isUserLogged) {
      e.preventDefault();
      this.props.openModalBox(() => (
        <RequireAccountModal
          history={this.props.history}
          openModalBox={this.props.openModalBox}
        />
      ));
    }
  };
  render() {
    const { plans = [] } = this.props.storePlans;
    if (!plans.length) return null;

    // Get plan details
    const standardPlan = plans.find((plan) => plan.slug === 'free-plan');
    const proPlan = plans.find((plan) => plan.slug === 'pro-plan');
    const proPlanPrice = parseFloat(proPlan.price);
    const standardPlanComission = standardPlan.variables
      .map((v) => v.value * 100)
      .join('');
    const proPlanComission = proPlan.variables.map((v) => v.value * 100).join('');

    // Return JSX
    return (
      <section className="about__sell">
        <PageHead
          attributes={{
            title: 'Abre tu Tienda - Market | Canasta Rosa',
            meta: [
              {
                name: 'description',
                content: 'Abre tu tienda',
              },
              {
                property: 'og:title',
                content: 'Canasta Rosa',
              },
              {
                property: 'og:description',
                content: 'Abre tu tienda',
              },
              {
                property: 'og:url',
                content: `${config.frontend_host}${this.props.location.pathname}`,
              },
              {
                property: 'og:image',
                content: `url`,
              },
              {
                property: 'og:image:width',
                content: '500',
              },
              {
                property: 'og:image:height',
                content: '200',
              },
            ],
            tags: ['Abre tu tienda'],
          }}
        />
        <div className="cover">
          <div className="cover__header">
            <h1 className="cover__header--title h2__title">
              Vende en Línea y Crece tu Negocio Hoy
            </h1>
            {/* <p className="cover__header--subtitle">Crece tu negocio en Canasta Rosa</p> */}
            <Link
              className="c2a_square"
              to="/stores/create"
              onClick={this.isUserLogged}
            >
              Abre tu tienda gratis
            </Link>
          </div>
        </div>

        <div className="section__profits wrapper--center">
          <h3 className="section__subtitle">Beneficios de vender en Canasta Rosa</h3>
          <ul className="profits__list">
            <li className="item__profit">
              <img
                className="icon__profit"
                src={require('images/aboutUs/sell/icon_logistic.svg')}
                alt=""
              />
              <h5 className="item__profit--title">Envíos rápidos y confiables</h5>
              <p className="item__profit--content">
                {' '}
                Expande tu negocio y haz que tus creaciones lleguen a todo el país*.
              </p>
            </li>
            <li className="item__profit">
              <img
                className="icon__profit"
                src={require('images/aboutUs/sell/icon_users.svg')}
                alt=""
              />
              <h5 className="item__profit--title">Alcanza miles de usuarios</h5>
              <p className="item__profit--content">
                Expón tu negocio a millones de usuarios y deja que descubran tus
                productos.
              </p>
            </li>
            <li className="item__profit">
              <img
                className="icon__profit"
                src={require('images/aboutUs/sell/icon_assistant.svg')}
                alt=""
              />
              <h5 className="item__profit--title">Atención a Clientes</h5>
              <p className="item__profit--content">
                Ayudamos a tus clientes y damos seguimiento personalizado a tus
                órdenes de principio a fin.
              </p>
            </li>
            <li className="item__profit">
              <img
                className="icon__profit"
                src={require('images/aboutUs/sell/icon_payments.svg')}
                alt=""
              />
              <h5 className="item__profit--title">Pagos Fáciles y Seguros</h5>
              <p className="item__profit--content"></p> Cierra más ventas aceptando
              todos los métodos de pago de manera rápida y segura.
            </li>
          </ul>
        </div>
        <div className="section__profits wrapper--center">
          <h3 className="section__subtitle">Nuestros Planes</h3>
          <div className="cr__plans-selection" data-test="selection-plan">
            <ul className="cr__plans-selection-list">
              <li className="cr__plans-selection-list-item">
                <div className="cr__plans-selection-container">
                  <img
                    src={require('images/plans/standardC.svg')}
                    className="cr__plans-selection-container-iconStandard"
                    alt="Icono plan estándar"
                    aria-label="Icono plan estándar"
                  />
                  <div className="text-mobile cr__textColor--colorDark300">
                    <h5 className="cr__plans-selection-container-title cr__text--subtitle2">
                      Standard
                    </h5>
                    <h6 className="cr__plans-selection-container-subtitle cr__text--paragraph">
                      Siempre gratuito
                    </h6>
                    <p className="cr__plans-selection-container-paragraph cr__text--paragraph">
                      Todo lo que necesitas para comenzar a vender en l&iacute;nea
                    </p>
                  </div>
                </div>
              </li>
              <li className="cr__plans-selection-list-item">
                <div className="cr__plans-selection-container">
                  <img
                    src={require('images/plans/proC.svg')}
                    className="cr__plans-selection-container-iconPro"
                    alt="figure"
                  />
                  <div className="text-mobile cr__textColor--colorDark300">
                    <h5 className="cr__plans-selection-container-title cr__text--subtitle2 ">
                      Plus
                    </h5>
                    <h6 className="cr__plans-selection-container-subtitle cr__text--paragraph">
                      ${proPlanPrice}MX mensuales
                    </h6>
                    <p className="cr__plans-selection-container-paragraph cr__text--paragraph">
                      Mejores tarifas, soporte especializado y herramientas para
                      crecer tu negocio
                    </p>
                  </div>
                </div>
              </li>
            </ul>
          </div>
          {this.state.width >= 768 ? (
            <ComparisonTableWeb
              proPlanPrice={proPlanPrice}
              comissionPro={proPlanComission}
              comissionStandard={standardPlanComission}
            />
          ) : (
            <ComparisonTableMobile
              proPlanPrice={proPlanPrice}
              comissionPro={proPlanComission}
              comissionStandard={standardPlanComission}
            />
          )}
        </div>

        <div className="section__tribe">
          <div className="wrapper--center section__tribe--container">
            <div className="tribe__illustration" />
            <div className="tribe__description">
              <h5 className="tribe__description--title">
                {' '}
                Forma parte de: <br /> TRIBE
              </h5>
              <p>
                Cursos y Capacitaciones de los temas más relevantes para
                emprendedoras como tú, Contabilidad, Marketing, Estrategia, Finanzas,
                RH y más.
                <br />
                Recibe apoyo profesional y personal en tus proyectos.
                <br />
                Ponte en contacto con nuestro equipo siempre que lo necesites.
              </p>
              <ul className="contact__list">
                <li className="contact__list--email">
                  <a className="contact__link" href="mailto:info@canastarosa.com">
                    info@canastarosa.com
                  </a>
                </li>
                <li className="contact__list--email">
                  <a
                    href="https://canastarosa.s3.us-east-2.amazonaws.com/download/%C2%BFCo%CC%81mo+vender+en+Canasta+Rosa%3F.pdf"
                    target="_blank"
                    rel="noopener noreferrer"
                    className="contact__link"
                    download
                  >
                    Descarga el tutorial
                  </a>
                </li>
              </ul>
            </div>
          </div>
        </div>

        <div className="section__pro wrapper--center">
          <div className="section__pro--text">
            <p className="text__crpro">
              Haz crecer tu negocio personal. <br />
              Impulsa tu marca con los servicios <br />
              <span className="color">Canasta Rosa</span>
            </p>
          </div>
          <div className="section__pro--c2a">
            <a href="/pro" className="c2a_square">
              Ver más
            </a>
          </div>
        </div>

        <div className="section__sellers wrapper--center">
          <h3 className="section__subtitle">Historias de nuestras vendedoras</h3>
          <div className="sellers__container">
            <div className="seller">
              <img
                className="seller__img"
                src={require('images/aboutUs/sell/story_gabriellas.png')}
                alt=""
              />
              <div className="seller__review">
                <p className="seller__review--name">
                  Gabriela Montañez
                  <br />
                  Creadora de: <span className="bold">Gabriellas Bakery</span>
                </p>
                <h5 className="seller__review--review">
                  "¡No sólo son una plataforma digital de ventas sino una guía y
                  maestra para hacer crecer tu negocio y cumplir tus sueños!"
                </h5>
              </div>
            </div>
            <div className="seller pink">
              <img
                className="seller__img"
                src={require('images/aboutUs/sell/story_franssen.png')}
                alt=""
              />
              <div className="seller__review">
                <p className="seller__review--name">
                  Emilia Franssen <br />
                  Creadora de: <span className="bold">Franssen Baked Goods</span>
                </p>
                <h5 className="seller__review--review">
                  "Que la gente me asocie con Canasta Rosa le ha dado más valor a mi
                  marca. Yo amo ser de @canastarosa."
                </h5>
              </div>
            </div>
            <div className="seller pink">
              <img
                className="seller__img"
                src={require('images/aboutUs/sell/story_mrlukas.png')}
                alt=""
              />
              <div className="seller__review">
                <p className="seller__review--name">
                  Jacqueline Torres <br />
                  Creadora de: <span className="bold">Mister Lukas</span>
                </p>
                <h5 className="seller__review--review">
                  "Canasta Rosa, ha sido una aliada en mi crecimiento, es una ventana
                  hacia otros mercados."
                </h5>
              </div>
            </div>
          </div>
        </div>

        <div className="section__faqs">
          <h3 className="section__subtitle">Preguntas frecuentes</h3>
          <ul className="faqs__list">
            {faqsList.vendor.map((faq, i) => (
              <li key={i} className="faq">
                <CollapseBox
                  content={<div dangerouslySetInnerHTML={{ __html: faq.answer }} />}
                  header={faq.question}
                />
              </li>
            ))}
          </ul>
        </div>

        <div className="section__store wrapper--center">
          <h5 className="section__store--title">Comienza ahora</h5>
          <Link
            className="c2a_square"
            to="/stores/create"
            onClick={this.isUserLogged}
          >
            Abre tu Tienda en Canasta Rosa
          </Link>
          <h5 className="section__store--help">¿Necesitas ayuda?</h5>
          <ul className="contact__list">
            <li className="contact__list--email">
              <a
                href="https://canastarosa.s3.us-east-2.amazonaws.com/download/%C2%BFCo%CC%81mo+vender+en+Canasta+Rosa%3F.pdf"
                target="_blank"
                rel="noopener noreferrer"
                className="contact__link"
                download
              >
                Descarga el tutorial
              </a>
            </li>
            <li className="contact__list--email">
              <a className="contact__link" href="mailto:info@canastarosa.com">
                info@canastarosa.com
              </a>
            </li>
          </ul>
        </div>
      </section>
    );
  }
}

// Map Redux Props and Actions to component
function mapStateToProps(state) {
  const { myStore, users } = state;
  return {
    isUserLogged: users.isLogged,
    storePlans: myStore.store_plans,
  };
}
function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      getMyStorePlans: myStoreActions.getMyStorePlans,
      openModalBox: modalBoxActions.open,
      closeModalBox: modalBoxActions.close,
    },
    dispatch,
  );
}

// Export Component
export default connect(mapStateToProps, mapDispatchToProps)(AboutUsSell);
