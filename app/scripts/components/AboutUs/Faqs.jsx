import React from 'react';
import { Component } from 'react';

import { MenuSections } from './MenuSections';
import CollapseBox from '../../Utils/CollapseBox';
import faqsList from '../../../statics/faqs.json';
import { MainBanner } from '../Home/MainBanner';
import { PICKUP_POINT } from '../../Constants/config.constants';

const createSlide = (faq, index) => (
  <li className="slide" key={index}>
    <div className="slide__data">
      <h1 className="title">{faq.question}</h1>
      <p dangerouslySetInnerHTML={{ __html: faq.answer }} />
    </div>
  </li>
);

export default class Faqs extends Component {
  render() {
    let slides = faqsList.buyer.reduce(
      (list, item, index) =>
        item.banner === true ? [...list, createSlide(item, index)] : list,
      [],
    );
    slides = faqsList.vendor.reduce(
      (list, item) =>
        item.banner === true ? [...list, createSlide(item, index)] : list,
      slides,
    );

    return (
      <section className="about_faqs">
        <MainBanner banners={slides} className="faqsBanner" />
        <MenuSections />

        <div className="faqs">
          <h3 className="faqs__title">Compradores</h3>
          <ul className="faqs__list">
            {faqsList.buyer.map((faq, i) => (
              <li key={i}>
                <CollapseBox
                  content={<div dangerouslySetInnerHTML={{ __html: faq.answer }} />}
                  header={faq.question}
                />
              </li>
            ))}
          </ul>

          <h3 className="faqs__title">Vendedores</h3>
          <ul className="faqs__list">
            {faqsList.vendor.map((faq, i) => (
              <li key={i}>
                <CollapseBox
                  content={<div dangerouslySetInnerHTML={{ __html: faq.answer }} />}
                  header={faq.question}
                />
              </li>
            ))}
          </ul>
        </div>
      </section>
    );
  }
}
