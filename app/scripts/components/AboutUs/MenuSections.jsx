import React from 'react';
import { Component } from 'react';
import { Link, NavLink } from 'react-router-dom';

export class MenuSections extends Component {
  render() {
    return (
      <nav className="menuSections">
        <ul>
          <li>
            <NavLink to="/about-us" exact activeClassName="active">
              ¿Qué es?
            </NavLink>
          </li>

          <li>
            <NavLink to="/about-us/team" activeClassName="active">
              ¿Quiénes Somos?
            </NavLink>
          </li>

          <li>
            <NavLink to="/about-us/faqs" activeClassName="active">
              Preguntas Frecuentes
            </NavLink>
          </li>

          <li>
            <NavLink to="/about-us/jobs" activeClassName="active">
              Bolsa de Trabajo
            </NavLink>
          </li>
        </ul>
      </nav>
    );
  }
}
