import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { userActions } from '../../Actions/user.actions';
import { statusWindowActions } from '../../Actions';
import Sentry from '../../Utils/Sentry';
import sadCard from '@canastarosa/ds-theme/assets/images/illustrations/sadCard.jpg';

class MyPaymentsMethods extends Component {
  componentDidMount() {
    this.props.getUserCreditCards();
  }
  deleteCard = (creditCard) => {
    const confirmation = confirm(
      '¿Estás seguro de querer eliminar esta tarjeta?. La operación no podrá deshacerse.',
    );
    if (confirmation) {
      return this.props
        .removeCreditCard(creditCard)
        .then((response) => {
          this.props.openStatusWindow({
            type: 'success',
            message: 'Eliminamos la tarjeta correctamente.',
          });
          this.props.getUserCreditCards();
        })
        .catch((error) => {
          Sentry.captureException(error);
          this.props.openStatusWindow({
            type: 'error',
            message: 'Error. Por favor, intenta de nuevo.',
          });
        });
    }
  };

  defaultCard = (id) => {
    return this.props
      .editCreditCard(id)
      .then((response) => {
        this.props.openStatusWindow({
          type: 'success',
          message: 'La tarjeta se convirtio en principal correctamente.',
        });
        this.props.getUserCreditCards();
      })
      .catch((error) => {
        Sentry.captureException(error);
        this.props.openStatusWindow({
          type: 'error',
          message: 'Error. Por favor, intenta de nuevo.',
        });
      });
  };

  render() {
    const { creditCards } = this.props.users.creditCards;

    return (
      <div className="cr__methodsPayments wrapper--center">
        <h2
          className={
            creditCards.length < 1 || creditCards === undefined
              ? 'cr__methodsPayments__title center-title'
              : 'cr__methodsPayments__title'
          }
        >
          Tarjetas guardadas
        </h2>

        {!creditCards.length < 1 || creditCards === undefined ? (
          <ul className="cr__methodsPayments__list">
            {creditCards
              .filter((card) => card.is_default)
              .map(
                ({
                  last_four_digits: lastFourDigits = '',
                  id = '',
                  is_default: isDefault = '',
                  payment_method: paymentMethod = '',
                }) => (
                  <li key={id} className="cr__methodsPayments__list-item">
                    <div>
                      <div className="cr__cardProvider">
                        <i
                          className={`cr__icon--${paymentMethod.display_name} cr__cardProvider-icon`}
                        />
                      </div>

                      <div className="cr__cardInformation">
                        <p className="cr__cardInformation-lastFour">
                          •••• <span>{lastFourDigits}</span>
                        </p>
                      </div>
                    </div>
                    <div className="cr__defaultCard">
                      {isDefault && (
                        <p className="cr__defaultCard-true">Principal</p>
                      )}
                    </div>

                    <div className="cr__iconContainer">
                      <i
                        className="cr__icon--trash cr__text--paragraph"
                        onClick={() => this.deleteCard(id)}
                      ></i>
                    </div>
                  </li>
                ),
              )}

            {creditCards
              .filter((card) => !card.is_default)
              .map(
                ({
                  last_four_digits: lastFourDigits = '',
                  id = '',
                  is_default: isDefault = '',
                  payment_method: paymentMethod = '',
                }) => (
                  <li key={id} className="cr__methodsPayments__list-item">
                    <div
                      className="cr__container-card gtm_click"
                      onClick={() => this.defaultCard(id)}
                      id="Hacer tarjeta Principal"
                    >
                      <div>
                        <div className="cr__cardProvider">
                          <i
                            className={`cr__icon--${paymentMethod.display_name} cr__cardProvider-icon`}
                          />
                        </div>

                        <div className="cr__cardInformation">
                          <p className="cr__cardInformation-lastFour">
                            •••• <span>{lastFourDigits}</span>
                          </p>
                        </div>
                      </div>

                      <div className="cr__defaultCard">
                        {isDefault && (
                          <p className="cr__defaultCard-true">Principal</p>
                        )}
                      </div>
                    </div>

                    <div className="cr__container-icon">
                      <i
                        className="cr__icon--trash cr__text--paragraph"
                        onClick={() => this.deleteCard(id)}
                        id="Eliminar tarjeta (mis metodos de pago)"
                      ></i>
                    </div>
                  </li>
                ),
              )}
          </ul>
        ) : (
          <div className="cr__sadCardContainer">
            <p>No tienes ninguna tarjeta guardada.</p>
            <img src={sadCard} alt="There is not a save card" className="sadCard" />
          </div>
        )}
      </div>
    );
  }
}

MyPaymentsMethods.loadData = (store) => {
  const promises = [store.dispatch(userActions.getUserCreditCards())];
  return Promise.all(promises.map((p) => (p.catch ? p.catch((e) => e) : p)));
};

// Redux Map Functions
function mapStateToProps(state) {
  const { users } = state;
  return {
    users,
  };
}

function mapDispatchToProps(dispatch) {
  const { getUserCreditCards, removeCreditCard, editCreditCard } = userActions;
  return bindActionCreators(
    {
      getUserCreditCards,
      removeCreditCard,
      editCreditCard,
      openStatusWindow: statusWindowActions.open,
    },
    dispatch,
  );
}

// Export Connected Component
export default connect(mapStateToProps, mapDispatchToProps)(MyPaymentsMethods);
