import React from 'react';
import { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Field, reduxForm, SubmissionError } from 'redux-form';
import PropTypes from 'prop-types';

import { InputField } from '../../Utils/forms/formComponents';
import { getUserProfile } from '../../Reducers/users.reducer';

/*
 * Form Validation Function
 * @param {object} values : Form values
 */
const validate = (values) => {
  const errors = {};

  // Nombre
  if (!values.first_name || values.first_name.length < 3) {
    errors.first_name = 'Escribe tu nombre completo.';
  }
  if (!values.last_name || values.last_name.length < 3) {
    errors.last_name = 'Escribe tus apellidos.';
  }

  // Return errors
  return errors;
};

/*
 * React BasicUserProfileForm
 */
class BasicUserProfileForm extends Component {
  // PropTypes / DefaultProps
  static propTypes = {
    onSubmit: PropTypes.func.isRequired,
  };

  /*
   * React Component Life Cycle Functions
   */
  constructor(props) {
    super(props);
    this.state = {
      bd_day: null,
      bd_month: null,
      bd_year: null,
    };
    this.onSubmit = this.onSubmit.bind(this);
    this.onBirthdayChange = this.onBirthdayChange.bind(this);
  }
  onBirthdayChange(e) {
    const { name, value } = e.target;
    this.setState({
      [name]: value,
    });
  }
  onSubmit(formValues) {
    let birthday = null;
    const bdayData = [formValues.bd_year, formValues.bd_month, formValues.bd_day];
    const bdayValues = bdayData.reduce(
      (a, b, i) => [...a, b !== '' ? true : false],
      [],
    );
    const birthday_object = bdayValues.reduce(
      (a, b, i) => {
        return {
          hasBirthday: a.valueType === b,
          valueType: b,
        };
      },
      {
        hasBirthday: false,
        valueType: bdayValues[0],
      },
    );

    if (birthday_object.hasBirthday && birthday_object.valueType) {
      birthday = bdayData.join('-');
    }

    // Submit Form
    this.props.onSubmit({
      ...formValues,
      birthday,
    });
  }

  /**
   * render()
   */
  render() {
    //const { success } = this.state;
    const { handleSubmit, pristine, submitting, error, submit } = this.props;

    // Format birthday selectboxes
    let years = [];
    for (let i = 2017; i > 1939; i--) {
      years.push(
        <option key={i} value={i}>
          {i}
        </option>,
      );
    }
    let days = [];
    for (let i = 1; i < 32; i++) {
      const day = i < 10 ? `0${i}` : i;
      days.push(
        <option key={day} value={day}>
          {i}
        </option>,
      );
    }

    // Return markup
    return (
      <form id="info_form" className="form" onSubmit={handleSubmit(this.onSubmit)}>
        <fieldset>
          <Field
            maxLength={40}
            autoFocus
            name="first_name"
            id="first_name"
            component={InputField}
            label="Nombre"
            className="first_name"
            type="text"
          />

          <Field
            maxLength={40}
            name="last_name"
            component={InputField}
            id="last_name"
            label="Apellidos"
            className="last_name"
            type="text"
          />

          <div className="form__data">
            <label className="title">G&eacute;nero</label>

            <Field
              id="gender_female"
              className="dots"
              name="gender.value"
              component="input"
              type="radio"
              value="M"
            />
            <label htmlFor="gender_female" className="dot adjustmentBasicUserForm">
              Femenino
            </label>

            <Field
              id="gender_male"
              className="dots"
              name="gender.value"
              component="input"
              type="radio"
              value="H"
            />
            <label htmlFor="gender_male" className="dot adjustmentBasicUserForm">
              Masculino
            </label>

            <Field
              id="gender_other"
              className="dots"
              name="gender.value"
              component="input"
              type="radio"
              value="N"
            />
            <label htmlFor="gender_other" className="dot adjustmentBasicUserForm">
              Otro
            </label>
          </div>

          {/*<Field
                        name="location"
                        component={InputField}
                        label="Ubicación"
                        className="location"
                        type="text"
                        placeholder="CDMX, Guadalajara, Monterrey, etc..." />*/}

          <div className="form__data">
            <label className="title"> Cumplea&ntilde;os</label>

            <Field
              name="bd_day"
              component="select"
              onChange={this.onBirthdayChange}
              className="adjustmentBasicUserFormBd"
            >
              <option value="">Día</option>
              {days}
            </Field>

            <Field
              name="bd_month"
              component="select"
              onChange={this.onBirthdayChange}
              className="adjustmentBasicUserFormBd"
            >
              <option value="">Mes...</option>
              <option value="01"> Enero </option>
              <option value="02"> Febrero</option>
              <option value="03"> Marzo </option>
              <option value="04"> Abril </option>
              <option value="05"> Mayo </option>
              <option value="06"> Junio </option>
              <option value="07"> Julio </option>
              <option value="08"> Agosto </option>
              <option value="09"> Septiembre </option>
              <option value="10"> Octubre </option>
              <option value="11"> Noviembre </option>
              <option value="12"> Diciembre </option>
            </Field>

            <Field
              name="bd_year"
              component="select"
              onChange={this.onBirthdayChange}
              className="adjustmentBasicUserFormBd"
            >
              <option value="">Año...</option>
              {years}
            </Field>
          </div>

          {/*
                    <div className="field_container" >
                        <label className="title" htmlFor="username">Username</label>
                        <input type="text" id="username"
                            value={userData.username}
                            name="username"
                            ref={(input) => { this.usernameInput = input }}
                            onChange={this.onInputChange} />
                    </div>
                    */}

          <div className="field_container submit">
            <input
              type="submit"
              value="Actualizar Información"
              className="c2a_square"
            />
          </div>
        </fieldset>
      </form>
    );
  }
}

// Wrap component within reduxForm
BasicUserProfileForm = reduxForm({
  form: 'userProfile_form',
  validate,
  enableReinitialize: true,
})(BasicUserProfileForm);

// Redux Map Functions
function mapStateToProps(state) {
  return {
    initialValues: getUserProfile(state),
  };
}

// Export Connected Component
BasicUserProfileForm = connect(mapStateToProps)(BasicUserProfileForm);
export default BasicUserProfileForm;
