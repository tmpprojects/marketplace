import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Field, reduxForm, formValueSelector } from 'redux-form';
import PropTypes from 'prop-types';

import { getAddressesList } from '../../Reducers';
import { statusWindowActions, modalBoxActions, userActions } from '../../Actions';
import { UIIcon } from '../../Utils/UIIcon';
import AddressManager from '../../Utils/AddressManager.jsx';
import AddressesList from '../../Utils/AddressesList.jsx';

class AddressForm extends React.Component {
  /*
   * Constructor Function
   * @param {object} props React Component Properties
   */
  constructor(props) {
    super(props);
    this.state = {};

    // Bind Scope to class methods
    this.openAddressWindow = this.openAddressWindow.bind(this);
    this.closeAddressWindow = this.closeAddressWindow.bind(this);
    this.onUpdateAddress = this.onUpdateAddress.bind(this);
    this.onDeleteAddress = this.onDeleteAddress.bind(this);
    this.onAddAddress = this.onAddAddress.bind(this);
    this.onAddressChange = this.onAddressChange.bind(this);
  }

  /*
   * ON ADD ADDRESS
   * Post a new User Address
   * @param {object} address : Address Info
   */
  onAddAddress(addressId) {
    this.props
      .addAddress(addressId)
      .then((response) => {
        this.props.openStatusWindow({
          type: 'success',
          message: 'La nueva dirección se agregó correctamente.',
        });
      })
      .catch((error) => console.log(error));

    this.props.closeModalBox();
  }

  /*
   * ON DELETE ADDRESS
   * Delete a User Address
   * @param {string} addressId : Address Id
   */
  onDeleteAddress(addressId) {
    this.props
      .deleteAddress(addressId)
      .then((response) => {
        this.props.openStatusWindow({
          type: 'success',
          message: 'La direccion se eliminó correctamente.',
        });
      })
      .catch((error) => {
        this.props.openStatusWindow({
          type: 'error',
          message: 'No puedes eliminar la dirección seleccionada por defecto.',
        });
      });
  }
  onUpdateAddress(address) {
    this.props.closeModalBox();
    this.submitForm(address)
      .then()
      .catch((error) => console.log(error));
  }

  /*
   * ON ADDRESS CHANGE
   * Make selected value the default address
   * @param {object} e : Event Object
   * @param {object} address : Current field Address
   */
  onAddressChange(targetAddress) {
    const address = this.props.addresses.find((a) => a.uuid === targetAddress.uuid);
    // Update Default Pickup Address
    this.onUpdateAddress({
      uuid: address.uuid,
      default_address: true,
    });
  }

  /*
   * CLOSE ADDRESS WINDOW
   */
  closeAddressWindow(id = null) {
    // Close Window
    this.props.closeModalBox();
  }

  /*
   * OPEN SECTIONS WINDOW
   * Opens modalbox to create a new term
   */
  openAddressWindow() {
    this.props.openModalBox(() => (
      <AddressManager onSubmit={this.onAddAddress} title="Nueva Dirección" />
    ));
  }

  submitForm(values) {
    return this.props
      .updateAddress(values)
      .then((response) => {
        this.props.listAddresses();
        this.props.openStatusWindow({
          type: 'success',
          message: 'La direccion se actualizó correctamente.',
        });
      })
      .catch((error) => {
        this.props.openStatusWindow({
          type: 'error',
          message: 'Error. Por favor, intenta de nuevo.',
        });
      });
  }

  /*
   * React Component Cycle Methods
   */
  render() {
    const { addresses = [] } = this.props;
    return (
      <form className="form">
        <fieldset>
          <div className="field_container address_container">
            {addresses.map((address) => (
              <Field
                type="radio"
                id={`address_${address.uuid}`}
                userType="buyer"
                name="default_address"
                address={address}
                key={address.uuid}
                value={address.uuid}
                changeAddress={this.onAddressChange}
                updateAddress={this.onUpdateAddress}
                deleteAddress={this.onDeleteAddress}
                component={AddressesList}
                active={address.uuid === this.props.pickup_address}
                className="shipping__address"
                listAddresses={this.props.listAddresses}
              />
            ))}
          </div>

          <div className="field_container">
            <button type="button" onClick={this.openAddressWindow} className="add">
              Agregar nueva direcci&oacute;n
            </button>
          </div>
        </fieldset>
      </form>
    );
  }
}

// Wrap component within reduxForm
AddressForm = reduxForm({
  form: 'userProfileAddresses_form',
  enableReinitialize: true,
  destroyOnUnmount: false,
})(AddressForm);

// Add Redux state and actions to component´s props
const selector = formValueSelector('userProfileAddresses_form');
function mapStateToProps(state) {
  const { users } = state;
  const defaultAddress = users.addresses.addresses.find((address) =>
    address.default_address ? address.uuid : false,
  );
  const selectedShippingAddress = selector(state, 'default_address');
  return {
    addresses: getAddressesList(state),
    initialValues: {
      default_address: defaultAddress ? defaultAddress.uuid : null,
    },
    pickup_address: selectedShippingAddress,
  };
}
function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      openModalBox: modalBoxActions.open,
      closeModalBox: modalBoxActions.close,
      addAddress: userActions.addAddress,
      deleteAddress: userActions.deleteAddress,
      updateAddress: userActions.updateAddress,
      openStatusWindow: statusWindowActions.open,
      listAddresses: userActions.listAddresses,
    },
    dispatch,
  );
}

// Export Component
AddressForm = connect(mapStateToProps, mapDispatchToProps)(AddressForm);
export default AddressForm;
