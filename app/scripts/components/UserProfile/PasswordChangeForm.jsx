import React from 'react';
import { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Field, reduxForm, SubmissionError } from 'redux-form';
import PropTypes from 'prop-types';

import { password, required } from '../../Utils/forms/formValidators';
import { PasswordField } from '../../Utils/forms/formComponents';

/*
 * Form Validation Function
 * @param {object} values : Form values
 */
const password_confirmation = (value, allValues) =>
  value && value !== allValues.passwordNew
    ? 'Las contraseñas nos coinciden.'
    : undefined;

/*
User PasswordChangeForm
*/
class PasswordChangeForm extends Component {
  // PropTypes / DefaultProps
  static propTypes = {
    onSubmit: PropTypes.func.isRequired,
  };

  /*
   * React Component Life Cycle Functions
   */
  constructor(props) {
    super(props);
  }
  render() {
    const { handleSubmit, pristine, submitting, error, submit, valid } = this.props;

    return (
      <form className="form" onSubmit={handleSubmit}>
        <fieldset>
          <Field
            autoFocus="true"
            name="passwordCurrent"
            component={PasswordField}
            validate={[password, required]}
            placeholder="Contrase&ntilde;a Actual"
            className="current"
          />

          <Field
            name="passwordNew"
            component={PasswordField}
            validate={[password, required]}
            placeholder="Nueva Contrase&ntilde;a"
            className="new"
          />

          <Field
            name="passwordConf"
            component={PasswordField}
            validate={[password, required, password_confirmation]}
            placeholder="Confirma tu Contrase&ntilde;a"
            className="conf"
          />

          {valid && (
            <input
              type="submit"
              value="Cambiar contrase&ntilde;a"
              className="disabled"
            />
          )}
        </fieldset>
      </form>
    );
  }
}

// Wrap component within reduxForm
PasswordChangeForm = reduxForm({
  form: 'passwordChange_form',
})(PasswordChangeForm);

// Export Connected Component
export default PasswordChangeForm;
