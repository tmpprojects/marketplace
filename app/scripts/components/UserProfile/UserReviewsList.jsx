import React from 'react';
import { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';

import { ResponsiveImage } from '../../Utils/ImageComponents';
import { modalBoxActions } from '../../Actions';
import ModalRating from '../../Utils/ModalRating';
import { formatDate } from '../../Utils/dateUtils';
import { formatNumberToPrice } from '../../Utils/normalizePrice';
import Rating from '../Rating/Rating';

/**
 * ProductReview Class
 * Display reviews and rating for a specified product
 */
class ProductReview extends Component {
  // Component PropTypes and Values
  static propTypes = {
    editable: PropTypes.bool,
    purchase: PropTypes.object,
    submitRating: PropTypes.func.isRequired,
  };
  static defaultProps = {
    editable: true,
    purchase: {},
  };

  /**
   * render()
   */
  render() {
    const { submitRating, purchase } = this.props;

    // Return markup
    if (!purchase.review) {
      return (
        <div className="review_product">
          <div
            className="review_product-rating"
            onClick={(value) => {
              submitRating(purchase, 0);
            }}
          >
            <button className="button button-simple" type="button">
              Deja una reseña sobre este producto
            </button>
            <Rating
              interactive
              rating={0}
              onClick={(e, value) => {
                e.stopPropagation();
                submitRating(purchase, value);
              }}
            />
          </div>
        </div>
      );
    }

    return (
      <div className="review_product">
        <div className="review_product-rating">
          <span className="button-simple" type="button" className="button">
            Calificación
          </span>
          <Rating rating={purchase.review.product_score} />
        </div>

        <p className="comment">
          Comentario:
          <br />
          <span>{purchase.review.comment}</span>
        </p>
      </div>
    );
  }
}
/**
 * ProductPurchaseReviews Class
 * Holds a list of product purchases and reviews
 */
class ProductPurchaseReviews extends Component {
  // Component PropTypes
  static propTypes = {
    purchase: PropTypes.object.isRequired,
    submitRating: PropTypes.func.isRequired,
  };

  // Declare component initial state
  state = {
    isOpen: false,
  };

  /**
   * renderPurchaseReview() Render purchase details and product reviews/rating
   * @param {object} purchase | Product Purchase
   */
  renderPurchaseReview = (purchase) => {
    const { submitRating } = this.props;
    let status = '';
    if (purchase.review) {
      switch (purchase.review.is_approved) {
        case true:
          status = 'aprobada';
          break;
        case false:
          status = 'desaprobada';
          break;
        default:
          status = 'pendiente';
          break;
      }
    }

    return (
      <div className="summary__detail" key={purchase.id}>
        <div className="summary__detail-order">
          <p className="day">
            Compra realizada:{' '}
            <span>{formatDate(new Date(purchase.order.created))}</span>
          </p>
          <p className="order">
            Orden: <span>{purchase.order.uid}</span>
          </p>
          <p className="price">
            Precio: <span>{formatNumberToPrice(purchase.product.price)}MX</span>{' '}
          </p>
          {purchase.review && (
            <p className="status">
              Status: <span>{status}</span>
            </p>
          )}
        </div>

        <ProductReview
          submitRating={submitRating}
          openRatingWindow={this.openRatingWindow}
          purchase={purchase}
        />
      </div>
    );
  };

  /**
   * render()
   */
  render() {
    const { purchase } = this.props;
    //const [lastPurchase, ...oldPurchases] = purchase;

    return this.renderPurchaseReview(purchase);

    // Return markup
    return (
      <div>
        {this.renderPurchaseReview(lastPurchase)}

        {oldPurchases.length > 0 && (
          <div>
            {this.state.isOpen !== true ? (
              <button
                type="button"
                className="button-square--gray"
                onClick={(e) => {
                  this.setState({
                    isOpen: true,
                  });
                }}
              >
                Ver compras anteriores
              </button>
            ) : (
              <button
                type="button"
                className="button-square--gray"
                onClick={(e) => {
                  this.setState({
                    isOpen: false,
                  });
                }}
              >
                Ver menos
              </button>
            )}

            {this.state.isOpen === true && (
              <div>
                {oldPurchases.map((purchase) => this.renderPurchaseReview(purchase))}
              </div>
            )}
          </div>
        )}
      </div>
    );
  }
}

class UserReviewsList extends Component {
  /*
   * openRatingWindow()
   * Opens modalbox to rating product
   * @param {object} productPurchase | Product Purchase
   * @param {int} value | Initial Rating Value to display on Rating component
   */
  openRatingWindow = (productPurchase, value = 0) => {
    this.props.openModalBox(() => (
      <ModalRating
        productPurchase={productPurchase}
        productScore={value}
        updateReviews={this.updateReviews}
      />
    ));
  };

  updateReviews = () => {
    const { page, performSearch } = this.props;

    performSearch(page, 'pending');
    performSearch(page, 'completed');
  };

  render() {
    const { items = [] } = this.props;
    return items.map((purchase) => (
      <div className="productReview" key={`${purchase.id}`}>
        <div className="summary">
          <div className="summary__detail">
            <div className="img_container">
              <div className="product_img">
                <ResponsiveImage
                  src={purchase.product.photo}
                  alt={purchase.product.name}
                />
              </div>
            </div>

            <div className="info_container">
              <h5 className="store">{purchase.product.store.name}</h5>
              <p>{purchase.product.name}</p>
            </div>
          </div>

          <ProductPurchaseReviews
            submitRating={this.openRatingWindow}
            purchase={purchase}
          />
        </div>
      </div>
    ));
  }
}

// Pass Redux state and actions to component
function mapStateToProps(state) {
  return {};
}
function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      openModalBox: modalBoxActions.open,
      closeModalBox: modalBoxActions.close,
    },
    dispatch,
  );
}

// Wrap component with router component
UserReviewsList = connect(mapStateToProps, mapDispatchToProps)(UserReviewsList);
export default UserReviewsList;
