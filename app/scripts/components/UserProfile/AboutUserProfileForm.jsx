import React from 'react';
import { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Field, reduxForm } from 'redux-form';

import { TextArea } from '../../Utils/forms/formComponents';
import { getUserProfile } from '../../Reducers/users.reducer';
import withCounter from '../hocs/withCounter';

/*
 * Form Validation Function
 * @param {object} values : Form values
 */
const validate = (values) => {
  const errors = {};
  return errors;
};

const TextAreaWithCounter = withCounter(TextArea);
/*
Aboutct AboutUserProfileForm
*/
let AboutUserProfileForm = (props) => {
  const { handleSubmit, submit } = props;

  return (
    <form className="form field_about" onSubmit={handleSubmit}>
      <fieldset>
        <Field
          maxLength={200}
          name="presentation"
          component={TextAreaWithCounter}
          label="Acerca de m&iacute;"
          placeholder="Escribe una breve descripción acerca de ti"
          className="about"
          rows="3"
          cols="50"
          id="presentation"
          onBlur={submit}
        />
      </fieldset>
    </form>
  );
};

// PropTypes Definition
AboutUserProfileForm.propTypes = {
  onSubmit: PropTypes.func.isRequired,
};

// Wrap component within reduxForm
AboutUserProfileForm = reduxForm({
  form: 'aboutProfile_form',
  validate,
  enableReinitialize: true,
})(AboutUserProfileForm);

// Redux Map Functions
function mapStateToProps(state) {
  return {
    initialValues: getUserProfile(state),
  };
}

// Export Connected Component
AboutUserProfileForm = connect(mapStateToProps)(AboutUserProfileForm);
export default AboutUserProfileForm;
