import React from 'react';
import { Component } from 'react';
import ReactCrop from 'react-image-crop';
import 'react-image-crop/dist/ReactCrop.css';

export class RCrop extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      crop: {
        x: 10,
        y: 10,
        width: 80,
        height: 80,
        aspect: 16 / 9,
      },
    };
  }

  handleChange = (crop) => {
    this.setState({ crop });
  };

  handleComplete = (crop, pixelCrop) => {
    console.log('crop', crop, 'pixelCrop', pixelCrop);
  };

  render() {
    return (
      <ReactCrop
        src={require('./racoon.jpg')}
        crop={this.state.crop}
        onChange={this.handleChange}
        onComplete={this.handleComplete}
        keepSelection={true}
      />
    );
  }
}
