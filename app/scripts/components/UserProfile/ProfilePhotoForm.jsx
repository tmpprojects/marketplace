import React from 'react';
import { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { reduxForm, SubmissionError } from 'redux-form';
import PropTypes from 'prop-types';

import { statusWindowActions } from '../../Actions';
import { ResponsiveImage } from '../../Utils/ImageComponents.jsx';

/*
 * Form Validation Function
 * @param {object} values : Form values
 */
const validate = (values) => {
  const errors = {};
  // Return errors
  return errors;
};

/*
Aboutct ProfilePhotoForm
*/
class ProfilePhotoForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      success: false,
    };
    // Bind Method Scopes
    this.onChange = this.onChange.bind(this);
  }

  /*
   * onChange
   * Fires when users select another file to upload
   * @params e Object : Event Object
   */
  onChange(e) {
    e.preventDefault();

    // Verify File size (8MB)
    if (e.target.files[0].size / 1024 / 1024 > 8) {
      e.target.value = '';
      this.props.openStatusWindow({
        type: 'error',
        message: 'Por favor, elige una imagen menor a 8MB.',
      });
      return false;
    }

    // Upload file
    const profilePhotoForm = new FormData();
    profilePhotoForm.append(e.target.name, e.target.files[0]);
    this.props.onSubmit(profilePhotoForm);
  }

  /*
   * React Component Life Cycle Functions
   */
  render() {
    const { success } = this.state;
    const { handleSubmit, pristine, submitting, error, submit } = this.props;

    return (
      <form className="form">
        <fieldset>
          <div className="field_photo">
            <div className="field_photo--image">
              <ResponsiveImage
                src={this.props.userData.profile_photo}
                alt={this.props.userData.first_name}
              />
            </div>

            <input
              type="file"
              name="profile_photo"
              id="profile_picture"
              style={{ display: 'none' }}
              accept="image/x-png,image/gif,image/jpeg,image/jpg"
              onChange={this.onChange.bind(this)}
            />
            <label htmlFor="profile_picture" className="inputfile">
              Cambia tu foto de perfil
            </label>
          </div>
        </fieldset>
      </form>
    );
  }
}

// PropTypes Definition
ProfilePhotoForm.propTypes = {
  onSubmit: PropTypes.func.isRequired,
};

// Wrap component within reduxForm
ProfilePhotoForm = reduxForm({
  form: 'profilePhoto_form',
  validate,
  enableReinitialize: true,
})(ProfilePhotoForm);

// Redux Map Functions
function mapStateToProps(state) {
  return {
    initialValues: {
      ...state.auth.user,
    },
  };
}
const mapDispatchToProps = (dispatch) => {
  return bindActionCreators(
    {
      openStatusWindow: statusWindowActions.open,
    },
    dispatch,
  );
};

// Export Connected Component
ProfilePhotoForm = connect(mapStateToProps, mapDispatchToProps)(ProfilePhotoForm);
export default ProfilePhotoForm;
