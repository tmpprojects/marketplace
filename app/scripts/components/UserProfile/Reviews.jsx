import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';

import Rating from '../Rating/Rating';
import { modalBoxActions } from '../../Actions';
import ModalRating from '../../Utils/ModalRating';
import { formatDate } from '../../Utils/dateUtils';
import { formatNumberToPrice } from '../../Utils/normalizePrice';
import { ResponsiveImage } from '../../Utils/ImageComponents';

/**
 * ProductReview Class
 * Display reviews and rating for a specified product
 */
class ProductReview extends Component {
  // Component PropTypes and Values
  static propTypes = {
    editable: PropTypes.bool,
    purchase: PropTypes.object,
    submitRating: PropTypes.func.isRequired,
  };
  static defaultProps = {
    editable: true,
    purchase: {},
  };

  /**
   * render()
   */
  render() {
    const { submitRating, purchase } = this.props;

    // Return markup
    if (!purchase.review) {
      return (
        <div className="review_product">
          <div
            className="review_product-rating"
            onClick={(e) => submitRating(purchase)}
          >
            <button className="button-simple" type="button">
              Calificación
            </button>
            {/* <Rating interactive rating={purchase.review.rating} /> */}
          </div>

          {/* <div style={{width: '100%'}}>
                            <label>Comentario: </label>
                            <div style={{width: '100%'}}>
                                <textarea style={{width: '100%'}}></textarea>
                            </div>
                        </div> */}
        </div>
      );
    }

    return (
      <div className="review_product">
        <p className="comment">
          Comentario: <span>{purchase.review.comment}</span>
        </p>
        <div className="review_product-rating">
          <button className="button-simple" type="button">
            Calificación
          </button>
          <Rating interactiv rating={purchase.review.product_score} />
        </div>
      </div>
    );
  }
}

/**
 * ProductPurchaseReviews Class
 * Holds a list of product purchases and reviews
 */
class ProductPurchaseReviews extends Component {
  // Component PropTypes
  static propTypes = {
    purchases: PropTypes.object.isRequired,
    submitRating: PropTypes.func.isRequired,
  };

  // Declare component initial state
  state = {
    isOpen: false,
  };

  /**
   * renderPurchaseReview() Render purchase details and product reviews/rating
   * @param {object} purchase | Product Purchase
   */
  renderPurchaseReview = (purchase) => {
    const { submitRating } = this.props;
    return (
      <div className="summary__detail" key={purchase.id}>
        <div className="summary__detail-order">
          <p className="day">
            Compra realizada:
            <span>{formatDate(new Date(purchase.order.created))} </span>
          </p>
          <p className="order">
            Orden:
            <span>{purchase.order.uid} </span>
          </p>
          <p className="price">
            Precio:
            <span>{formatNumberToPrice(purchase.product.price)}MX </span>
          </p>
        </div>

        <ProductReview
          submitRating={submitRating}
          openRatingWindow={this.openRatingWindow}
          purchase={purchase}
        />
      </div>
    );
  };

  /**
   * render()
   */
  render() {
    const { purchases } = this.props;
    //const [ lastPurchase, ...oldPurchases ] = purchases;

    return this.renderPurchaseReview(purchases);

    // Return markup
    return (
      <div>
        {this.renderPurchaseReview(lastPurchase)}

        {oldPurchases.length > 0 && (
          <div>
            {this.state.isOpen !== true ? (
              <button
                type="button"
                className="button-square--gray"
                onClick={(e) => {
                  this.setState({
                    isOpen: true,
                  });
                }}
              >
                Ver compras anteriores
              </button>
            ) : (
              <button
                type="button"
                className="button-square--gray"
                onClick={(e) => {
                  this.setState({
                    isOpen: false,
                  });
                }}
              >
                Ver menos
              </button>
            )}

            {this.state.isOpen === true && (
              <div>
                {oldPurchases.map((purchase) => this.renderPurchaseReview(purchase))}
              </div>
            )}
          </div>
        )}
      </div>
    );
  }
}

/**
 * Purchased Products Reviews Class
 * Display a list of purchased products and the reviews/ratings
 * for every pruchase of that product
 */
class Reviews extends Component {
  /*
   * openRatingWindow()
   * Opens modalbox to rating product
   * @param {object} productPurchase | Product Purchase
   */
  openRatingWindow = (productPurchase) => {
    this.props.openModalBox(() => <ModalRating productPurchase={productPurchase} />);
  };
  /**
   * render() Renders component
   */
  render() {
    const { productReviews } = this.props;
    const pendingReviewCount = productReviews.reduce((count, purchase) => {
      //const p = purchase.reduce((a, b) => !b.review ? a + 1 : a, 0);
      return purchase.review ? count + 1 : count; //count + p;
    }, 0);

    return (
      <div className="reviewsList">
        <div className="reviewsList_header">
          <p className="title">Reseñas de productos </p>

          <div className="filters">
            <p> Ordenar por: </p>
            <div className="filters__list">
              <select name="filters">
                <option value="Todos">Todos </option>
                <option value="ConReseñas">Productos con reseña </option>
                <option value="SinReseñas">Productos sin reseña </option>
              </select>
            </div>
          </div>
        </div>

        {productReviews.length > 0 ? (
          <div className="productReview-list">
            {productReviews.map((purchase) => (
              <div className="productReview" key={purchase.product.slug}>
                <div className="summary">
                  <div className="summary__detail">
                    <div className="img_container">
                      <div className="product_img">
                        <ResponsiveImage
                          src={purchase.product.photo}
                          alt={purchase.product.name}
                        />
                      </div>
                    </div>
                    <div className="info_container">
                      <h5 className="store">{purchase.product.store.name}</h5>
                      <p>{purchase.product.name} </p>
                    </div>
                  </div>

                  <ProductPurchaseReviews
                    submitRating={this.openRatingWindow}
                    purchases={purchase}
                  />
                </div>
              </div>
            ))}
          </div>
        ) : (
          <div className="review_wrapper">
            <p>
              Todas nuestras reseñas provienen de compras verificadas. <br />
              <span>¡Parece que aún no has comprado nada! </span>
            </p>
          </div>
        )}
      </div>
    );
  }
}

// Pass Redux state and actions to component
function mapStateToProps(state) {
  return {
    productReviews: state.users.reviews.data.results || [],
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      openModalBox: modalBoxActions.open,
      closeModalBox: modalBoxActions.close,
    },
    dispatch,
  );
}

// Wrap component with router component
export default connect(mapStateToProps, mapDispatchToProps)(Reviews);
