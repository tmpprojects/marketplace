import React, { Component } from 'react';

import AddressForm from './AddressForm.jsx';
import ProfilePhotoForm from './ProfilePhotoForm.jsx';
import { IconPreloader } from '../../Utils/Preloaders';
import PasswordChangeForm from './PasswordChangeForm.jsx';
import BasicUserProfileForm from './BasicUserProfileForm.jsx';
import AboutUserProfileForm from './AboutUserProfileForm.jsx';

//import {AvaEditor} from './AvaEditor.jsx';
//import {RCrop} from './ReactCrop.jsx';

export default class BasicUserProfile extends Component {
  render() {
    if (this.props.user.loading) {
      return <IconPreloader />;
    }

    const { first_name, last_name, location, email } = this.props.user;

    return (
      <div className="user_info">
        {/* Prueba */}
        {/* <AvaEditor /> */}
        {/* <RCrop /> */}
        {/* <Ham /> */}

        <div className="info_principal">
          <div className="info_principal_photo">
            <ProfilePhotoForm
              onSubmit={this.props.onChangeProfilePhoto}
              userData={this.props.user}
            />

            <div className="field_name">
              <h4 className="title">{`${first_name} ${last_name}`}</h4>
              <h6 className="title_email">{email}</h6>
              <p>
                {location !== '' && <span className="location">{location}</span>}
                {/*` | @${username}`*/}
              </p>
            </div>

            <AboutUserProfileForm
              onSubmit={this.props.onBioSubmit}
              userData={this.props.users}
            />
          </div>

          <div className="info_principal_general">
            <div className="category">
              <a className="dropdown">Informaci&oacute;n personal</a>
            </div>

            <div className="fields">
              <BasicUserProfileForm
                onSubmit={this.props.onPersonalInfoSubmit}
                userData={this.props.users}
              />
            </div>
          </div>
        </div>

        <div className="categories_container">
          <div className="category">
            <a
              className="dropdown"
              onClick={() => this.props.isMenuOpen('password_menu')}
            >
              Cambiar Contrase&ntilde;a
            </a>
          </div>
          <div className={this.props.isPasswordMenuOpen ? 'open fields' : 'closed'}>
            <PasswordChangeForm onSubmit={this.props.onPasswordSubmit} />
          </div>

          <div id="addresses" className="category">
            <a
              className="dropdown"
              onClick={() => this.props.isMenuOpen('address_menu')}
            >
              Direcciones
            </a>
          </div>
          <div className={this.props.isAddressMenuOpen ? 'open fields' : 'closed'}>
            <AddressForm />
          </div>

          {/* <div className="category" >
                        <a className='dropdown' onClick={() => this.isMenuOpen()}>Email</a>
                    </div>
                    <div className={this.state.open ? "open fields" : "closed"}>
                        <form className="form" onSubmit={this.onEmailSubmit}>
                            <fieldset>
                                <div className="field_container" >
                                    {userData.email}
                                </div>

                                <div className="field_container" >
                                    <input type="text" placeholder="Nuevo Email"
                                        name='emailNew'
                                        value={userData.emailNew}
                                        ref={(input) => { this.emailInput = input }}
                                        onChange={this.onInputChange} />
                                </div>

                                <div className="field_container" >
                                    <input type="text" placeholder="Confirmación"
                                        name='emailConf'
                                        value={userData.emailConf}
                                        ref={(input) => { this.emailConfInput = input }}
                                        onChange={this.onInputChange} />
                                </div>

                                { (userData.emailNew !== undefined) 
                                    && (userData.emailNew !== '') 
                                    && (userData.emailNew === userData.emailConf) 
                                    &&  (
                                        <input 
                                            type="submit" 
                                            value="Actualizar Email" 
                                            className="disabled" 
                                        />
                                    )
                                }
                            </fieldset>
                        </form>
                    </div> */}
          {/*<div className="disabled_account">
                        <h5>¿Qué sucede cuando desactivo mi cuenta?</h5>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
                        Curabitur consectetur turpis quis tempus interdum. 
                        Praesent efficitur, tortor pulvinar hendrerit iaculis, 
                        erat lectus blandit massa, non placerat felis sem id mauris. 
                        Donec nibh urna, sodales tristique purus quis, imperdiet dictum neque
                        </p>
                        <a href="#" className="danger">Desactivar Cuenta</a>
                    </div>*/}
        </div>
      </div>
    );
  }
}
