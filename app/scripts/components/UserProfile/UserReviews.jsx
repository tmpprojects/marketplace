import React from 'react';
import { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import queryString from 'query-string';

import { modalBoxActions } from '../../Actions';
import withPagination from '../hocs/withPagination';
import UserReviewsList from './UserReviewsList';
import { userActions } from '../../Actions';
import ModalRating from '../../Utils/ModalRating';
import { formatDate } from '../../Utils/dateUtils';
import { formatNumberToPrice } from '../../Utils/normalizePrice';
import { ResponsiveImage } from '../../Utils/ImageComponents';
import { IconPreloader } from '../../Utils/Preloaders';

const PaginatedList = withPagination(UserReviewsList);
const MAX_ITEMS_PER_PAGE = 6;

/**
 * Purchased Products Reviews Class
 * Display a list of purchased products and the reviews/ratings
 * for every pruchase of that product
 */
class UserReviews extends Component {
  constructor(props) {
    super(props);
    this.currentPage = this.getResultsPageFromQueryString(
      this.props.location.search,
    );
    this.state = {
      currentView: 'pending', //[pending || completed]
    };
  }

  componentDidMount() {
    this.performSearch(this.currentPage, 'pending');
    this.performSearch(this.currentPage, 'completed');
  }

  componentWillReceiveProps(nextProps) {
    // If page param (Ej. URL ?page={pageNumber}}) has changed
    if (this.props.location.search !== nextProps.location.search) {
      this.performSearch(
        this.getResultsPageFromQueryString(nextProps.location.search),
        this.state.currentView,
      );
    }
  }

  componentDidUpdate(prevProps, prevState) {
    // If user changes of reviews tab (pending OR completed)
    if (this.state.currentView !== prevState.currentView) {
      this.props.history.push('/users/reviews');
      this.performSearch(
        this.getResultsPageFromQueryString(this.props.location.search),
        this.state.currentView,
      );
    }
  }

  getResultsPageFromQueryString = (query) => {
    if (query) {
      const resultsPage = parseInt(queryString.parse(query).p, 10);
      return !isNaN(resultsPage) ? resultsPage : 1;
    }
    return 1;
  };

  performSearch = (page = 1, currentView) => {
    // Scroll to top of page
    //window.scrollTo(0, 0);
    let query = '';

    if (currentView === 'pending') {
      query = `?page=${page}&page_size=${MAX_ITEMS_PER_PAGE}&review__isnull=true`;
    } else {
      query = `?page=${page}&page_size=${MAX_ITEMS_PER_PAGE}&review__isnull=false`;
    }

    this.currentPage = parseInt(page, 10);
    //Send request to API
    this.props.getUserReviews(query, currentView);
  };

  render() {
    const { pendingReviews, completedReviews, reviewsLoading } = this.props;
    const { currentView } = this.state;
    const currentPage = this.currentPage;
    let reviewsList = pendingReviews;

    if (currentView === 'completed') {
      reviewsList = completedReviews;
    }

    if (!reviewsList.results) {
      return <IconPreloader />;
    }

    return (
      <div className="reviewsList">
        <div className="reviewsList_header">
          <button
            className={`tab-button ${
              currentView === 'pending' ? 'tab-button--active' : ''
            }`}
            onClick={() => this.setState({ currentView: 'pending' })}
          >
            Pendientes <span className="note">({pendingReviews.count})</span>
          </button>
          <button
            className={`tab-button ${
              currentView === 'completed' ? 'tab-button--active' : ''
            }`}
            onClick={() => this.setState({ currentView: 'completed' })}
          >
            Anteriores <span className="note">({completedReviews.count})</span>
          </button>

          {/* <div className="filters">
                        <p>Ordenar por:</p>
                        <div className="filters__list">
                            <select name="filters">
                                <option value="Todos">Todos</option>
                                <option value="ConReseñas" >Productos con reseña</option>
                                <option value="SinReseñas">Productos sin reseña</option>
                            </select>
                        </div>
                    </div> */}
        </div>

        {reviewsList.count > 0 ? (
          <React.Fragment>
            <div className="productReview-list">
              {/* Show normal pagination if Completed Reviews Section is selected, 
                            otherwise show button pagination (Pending Reviews Section) */}
              {currentView === 'completed' ? (
                <PaginatedList
                  items={reviewsList.results}
                  baseLocation="/users/reviews?"
                  maxItems={MAX_ITEMS_PER_PAGE}
                  action={this.testingFunction}
                  page={currentPage}
                  totalPages={reviewsList.npages}
                />
              ) : (
                <UserReviewsList
                  items={reviewsList.results}
                  performSearch={this.performSearch}
                  page={currentPage}
                />
              )}
            </div>

            {/* Show button for More Reviews only if exists another page */}
            {currentView === 'pending' && reviewsList.next !== null && (
              <div className="btn-container">
                <button
                  type="button"
                  className="c2a_round"
                  onClick={() => this.performSearch(currentPage + 1, currentView)}
                >
                  Mostrar más
                </button>
              </div>
            )}
          </React.Fragment>
        ) : reviewsLoading && !pendingReviews.length ? (
          <IconPreloader />
        ) : (
          <div className="review_wrapper">
            <p>
              Todas nuestras reseñas provienen de compras verificadas. <br />
              <span>¡Parece que aún no has comprado nada!</span>{' '}
            </p>
          </div>
        )}
      </div>
    );
  }
}

// Pass Redux state and actions to component
function mapStateToProps(state) {
  const pendingReviews = state.users.reviews.pending;
  const completedReviews = state.users.reviews.completed;

  return {
    pendingReviews,
    completedReviews,
  };
}
function mapDispatchToProps(dispatch) {
  const { getUserReviews } = userActions;
  return bindActionCreators(
    {
      getUserReviews,
    },
    dispatch,
  );
}

// Wrap component with router component
UserReviews = connect(mapStateToProps, mapDispatchToProps)(UserReviews);
export default UserReviews;
