import React from 'react';
import { Link } from 'react-router-dom';
import { trackWithGTM } from '../../../../../Utils/trackingUtils';
import './profileOptions.scss';

const onGTMTracking = (value) => {
  const data = {
    id: 'mobile',
    name: `Dropdown-ProfileOptions-${value}`,
    position: 1,
  };
  trackWithGTM('eec.impressionClick', [data], 'Menu-Mobile');
};

export default function ProfileOptions(props) {
  const { logout, toggle } = props;
  //
  const logOut = () => {
    logout().then((response) => (window.location = '/'));
  };

  return (
    <div className="cr__headerMobile-profileOptions">
      <div
        className="cr__headerMobile-profileOptions-link"
        onClick={() => {
          toggle();
          onGTMTracking('MyProfile');
        }}
      >
        <Link
          className="cr__textColor--colorDark300 cr__text--paragraph"
          to="/users/profile/"
        >
          Ir a mi Perfil
        </Link>
      </div>
      <div
        className="cr__headerMobile-profileOptions-link"
        onClick={() => {
          toggle();
          onGTMTracking('MyOrders');
        }}
      >
        <Link
          className="cr__textColor--colorDark300 cr__text--paragraph"
          to="/users/orders"
        >
          Mis pedidos
        </Link>
      </div>
      <div
        className="cr__headerMobile-profileOptions-link"
        onClick={() => {
          toggle();
          onGTMTracking('MyReviews');
        }}
      >
        <Link
          className="cr__textColor--colorDark300 cr__text--paragraph"
          to="/users/reviews"
        >
          Reseñas
        </Link>
      </div>
      <div
        className="cr__headerMobile-profileOptions-link"
        onClick={() => {
          toggle();
          onGTMTracking('MySavedCards');
        }}
      >
        <Link
          className="cr__textColor--colorDark300 cr__text--paragraph"
          to="/users/cards"
        >
          Métodos de Pago
        </Link>
      </div>
      <div className="cr__headerMobile-profileOptions-link" onClick={() => toggle()}>
        <a
          href="https://ayuda.canastarosa.com/"
          target="_blank"
          rel="noopener noreferrer"
          className="cr__textColor--colorDark300 cr__text--paragraph"
          onClick={() => onGTMTracking('Help')}
        >
          Ayuda
        </a>
      </div>
      <div
        className="cr__headerMobile-profileOptions-link"
        onClick={() => {
          logOut();
          toggle();
          onGTMTracking('Logout');
        }}
      >
        <a className="cr__textColor--colorDark300 cr__text--paragraph">
          Cerrar sesión
        </a>
      </div>
    </div>
  );
}
