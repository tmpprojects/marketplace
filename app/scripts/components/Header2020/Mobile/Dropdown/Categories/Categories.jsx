import React, { useState, useRef, useEffect } from 'react';
import { Link } from 'react-router-dom';
import { trackWithGTM } from '../../../../../Utils/trackingUtils';
import './categories.scss';

const onGTMTracking = (value, section) => {
  const data = {
    id: 'mobile',
    name: `Dropdown-${section}-${value}`,
    position: 1,
  };
  trackWithGTM('eec.impressionClick', [data], 'Menu-Mobile');
};

export default function Categories(props) {
  const {
    toggle,
    categories,
    onSetSection,
    activeMenu,
    onSetDefaultMenu,
    section,
  } = props;

  const [categoryLevel, setCategoryLevel] = useState(1);

  const [parentCategoryLevel1, setParentCategoryLevel1] = useState({
    name: null,
    slug: null,
    children: [],
  });

  const [parentCategoryLevel2, setParentCategoryLevel2] = useState({
    name: null,
    slug: null,
    children: [],
  });

  const scrollTop = () => {
    window.scrollTo({ top: 0, behavior: 'smooth' });
  };

  const [componentHeight, setComponentHeight] = useState(null);
  const [scrollY, setScrollY] = useState(null);

  const heightReference = useRef(null);

  useEffect(() => {
    updateSize();
    window.addEventListener('scroll', updateSize);
    setComponentHeight(heightReference.current.offsetHeight);

    return () => window.removeEventListener('scroll', updateSize);
  }, [scrollY]);

  const updateSize = () => {
    if (activeMenu && (section === 'category' || section === 'interest')) {
      setScrollY(window.pageYOffset);
    }
    if (scrollY > componentHeight * 1.2) {
      toggle();
      onGTMTracking('CloseMenuByScroll', section);
    }
  };

  return (
    <React.Fragment>
      <div ref={heightReference} className="cr__headerMobile-dropdown-categories">
        {/** 1st Level */}
        {categoryLevel === 1 && (
          <div className="cr__headerMobile-dropdown-categories-level1">
            <h6
              className="cr__headerMobile-dropdown-categories-title border cr__textColor--colorDark300 cr__text--subtitle3"
              onClick={() => {
                onSetSection('main');
                onGTMTracking('BackMain', section);
              }}
            >
              {section === 'category' ? 'Categorías' : 'Intereses'}
            </h6>
            {categories.map(({ name, slug, children: level2 }) => (
              <div key={slug}>
                {level2?.length >= 1 ? (
                  <div
                    onClick={() => {
                      setParentCategoryLevel1({ name, slug, children: level2 });
                      setCategoryLevel(2);
                      scrollTop();
                      onGTMTracking(slug, section);
                    }}
                    className="cr__headerMobile-dropdown-categories-item
                cr__textColor--colorDark100 cr__text--subtitle3
                "
                  >
                    <span>{name}</span>
                  </div>
                ) : (
                  <Link
                    key={slug}
                    to={`/${section}/${slug}`}
                    onClick={() => {
                      toggle();
                      onGTMTracking(slug, section);
                    }}
                    className="cr__headerMobile-dropdown-categories-link
              cr__textColor--colorDark100 cr__text--subtitle3"
                  >
                    <span>{name}</span>
                  </Link>
                )}
              </div>
            ))}
          </div>
        )}

        {/** 2nd Level */}
        {categoryLevel === 2 && parentCategoryLevel1.name && (
          <div className="cr__headerMobile-dropdown-categories-level2">
            <div className="cr__headerMobile-dropdown-categories-header">
              <h6
                className="cr__headerMobile-dropdown-categories-title cr__textColor--colorDark300 cr__text--subtitle3"
                onClick={() => {
                  setCategoryLevel(1);
                  onGTMTracking('BackLevel1', section);
                }}
              >
                {parentCategoryLevel1?.name}
              </h6>
              <Link
                to={`/${section}/${parentCategoryLevel1?.slug}`}
                className="cr__textColor--colorDark100 cr__text--paragraph"
                onClick={() => {
                  toggle();
                  onGTMTracking(`All-${parentCategoryLevel1?.slug}`, section);
                }}
              >
                Ver todo
              </Link>
            </div>

            {parentCategoryLevel1?.children?.map(
              ({ name, slug, children: level3 }) => (
                <div key={slug}>
                  {level3?.length >= 1 ? (
                    <div
                      onClick={() => {
                        setParentCategoryLevel2({
                          name,
                          slug,
                          children: level3,
                        });
                        setCategoryLevel(3);
                        scrollTop();
                        onGTMTracking(slug, section);
                      }}
                      className="cr__headerMobile-dropdown-categories-item
                cr__textColor--colorDark100 cr__text--subtitle3
                "
                    >
                      <span>{name}</span>
                    </div>
                  ) : (
                    <Link
                      key={slug}
                      to={`/${section}/${slug}`}
                      onClick={() => toggle()}
                      className="cr__headerMobile-dropdown-categories-link
              cr__textColor--colorDark100 cr__text--subtitle3"
                    >
                      <span>{name}</span>
                    </Link>
                  )}
                </div>
              ),
            )}
          </div>
        )}

        {/** 3rd Level */}
        {categoryLevel === 3 && parentCategoryLevel2.name && (
          <div className="cr__headerMobile-dropdown-categories-level3">
            <div className="cr__headerMobile-dropdown-categories-header">
              <h6
                className="cr__headerMobile-dropdown-categories-title cr__textColor--colorDark300 cr__text--subtitle3"
                onClick={() => {
                  setParentCategoryLevel1({
                    name: parentCategoryLevel1?.name,
                    slug: parentCategoryLevel1?.slug,
                    children: parentCategoryLevel1?.children,
                  });
                  setCategoryLevel(2);
                  scrollTop();
                  onGTMTracking('BackLevel2', section);
                }}
              >
                {parentCategoryLevel2.name}
              </h6>
              <Link
                to={`/${section}/${parentCategoryLevel2?.slug}`}
                className="cr__textColor--colorDark100 cr__text--paragraph"
                onClick={() => {
                  toggle();
                  onGTMTracking(`All-${parentCategoryLevel2?.slug}`, section);
                }}
              >
                Ver todo
              </Link>
            </div>
            {parentCategoryLevel2.children.map(({ name, slug }) => (
              <Link
                key={slug}
                to={`/${section}/${slug}`}
                onClick={() => {
                  toggle();
                  onGTMTracking(slug, section);
                }}
                className="cr__headerMobile-dropdown-categories-link
            
              cr__textColor--colorDark100 cr__text--subtitle3"
              >
                <span>{name}</span>
              </Link>
            ))}
          </div>
        )}
      </div>
      {/** Underlay */}
      <div
        className={activeMenu ? 'activeDropdown' : ''}
        onClick={() => {
          onSetDefaultMenu();
          onGTMTracking('CloseMenuByUnderlay', section);
        }}
        style={{ height: componentHeight * 4 }}
      />
    </React.Fragment>
  );
}
