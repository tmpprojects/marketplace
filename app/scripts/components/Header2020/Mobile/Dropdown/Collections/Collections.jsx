import React, { useState, useRef, useEffect } from 'react';
import { Link } from 'react-router-dom';
import ScrollToTop from '../../../../../Utils/ScrollToTop';
import { IconPreloader } from '../../../../../Utils/Preloaders';
import { trackWithGTM } from '../../../../../Utils/trackingUtils';
import './collections.scss';

const onGTMTracking = (value) => {
  const data = {
    id: 'mobile',
    name: `Dropdown-Collections-${value}`,
    position: 1,
  };
  trackWithGTM('eec.impressionClick', [data], 'Menu-Mobile');
};

export default function Collections(props) {
  const {
    collections = {},
    toggle,
    onSetSection,
    activeMenu,
    onSetDefaultMenu,
    section,
  } = props;

  const collectionsArray = collections?.landingsList?.results?.slice(0, 7);

  const [componentHeight, setComponentHeight] = useState(null);
  const [scrollY, setScrollY] = useState(null);

  const heightReference = useRef(null);

  useEffect(() => {
    updateSize();
    window.addEventListener('scroll', updateSize);
    setComponentHeight(heightReference.current.offsetHeight);

    return () => window.removeEventListener('scroll', updateSize);
  }, [scrollY]);

  const updateSize = () => {
    if (activeMenu && section === 'collections') {
      setScrollY(window.pageYOffset);
    }
    if (scrollY > componentHeight * 1.2) {
      toggle();
      onGTMTracking('CloseMenuByScroll');
    }
  };
  return (
    <React.Fragment>
      <div ref={heightReference} className="cr__headerMobile-dropdown-collections">
        {!collections?.loading ? (
          <div className="cr__headerMobile-dropdown-collections-container">
            <h6
              className="cr__headerMobile-dropdown-collections-title cr__textColor--colorDark300 cr__text--subtitle3"
              onClick={() => {
                onSetSection('main');
                onGTMTracking('BackMain');
              }}
            >
              Colecciones
            </h6>
            {/* Fix add extra item in position one */}
            {/* <Link
              to={`/visa`}
              onClick={() => {
                toggle();
                onGTMTracking('visa-sales');
              }}
              className="cr__headerMobile-dropdown-collections-item 
                cr__textColor--colorDark100 cr__text--subtitle3
                "
            >
              <span>Visa Sales</span>
            </Link> */}
            {collectionsArray.map(({ name, slug }) => (
              <Link
                key={slug}
                to={`/landing/${slug}`}
                onClick={() => {
                  toggle();
                  onGTMTracking(slug);
                }}
                className="cr__headerMobile-dropdown-collections-item 
                cr__textColor--colorDark100 cr__text--subtitle3
                "
              >
                <span>
                  {name.length > 30
                    ? `${name.substring(0, 30).toLowerCase()}...`
                    : name.toLowerCase()}
                </span>
              </Link>
            ))}
          </div>
        ) : (
          <IconPreloader />
        )}
      </div>
      {/** Underlay */}
      <div
        className={activeMenu ? 'activeDropdown' : ''}
        onClick={() => {
          onSetDefaultMenu();
          onGTMTracking('CloseMenuByUnderlay');
        }}
        style={{ height: componentHeight * 4 }}
      />
    </React.Fragment>
  );
}
