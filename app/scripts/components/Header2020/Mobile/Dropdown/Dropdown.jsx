import React, { useState, useRef, useEffect } from 'react';
import { Link } from 'react-router-dom';
import Login from '../../../../Utils/Login';
import Register from '../../../../Utils/Register';
import ProfileOptions from '../Dropdown/ProfileOptions/ProfileOptions';
import { trackWithGTM } from '../../../../Utils/trackingUtils';
import './dropdown.scss';

const arrow = require('../../../../../images/icons/arrows/arrow_down.svg');

const onGTMTracking = (value) => {
  const data = {
    id: 'mobile',
    name: `Dropdown-${value}`,
    position: 1,
  };
  trackWithGTM('eec.impressionClick', [data], 'Menu-Mobile');
};

export default function Dropdown(props) {
  const [activeProfileOptions, setActiveProfileOptions] = useState(false);
  const toggleProfileOptions = () => setActiveProfileOptions(!activeProfileOptions);

  const [componentHeight, setComponentHeight] = useState(null);
  const [scrollY, setScrollY] = useState(null);

  const heightReference = useRef(null);

  useEffect(() => {
    updateSize();
    window.addEventListener('scroll', updateSize);
    setComponentHeight(heightReference.current.offsetHeight);

    return () => window.removeEventListener('scroll', updateSize);
  }, [scrollY]);

  const updateSize = () => {
    if (props.activeMenu && props.section === 'main') {
      setScrollY(window.pageYOffset);
    }
    if (scrollY > componentHeight * 1.2) {
      props.toggle();
      onGTMTracking('CloseMenuByScroll');
    }
  };

  const {
    user = {},
    openModalBox,
    toggle,
    logout,
    onSetSection,
    activeMenu,
    onSetDefaultMenu,
  } = props;
  return (
    <React.Fragment>
      <div ref={heightReference} className="cr__headerMobile-dropdown">
        {user?.isLogged ? (
          <React.Fragment>
            <div className="cr__headerMobile-dropdown-information">
              <Link
                to={'/users/profile/'}
                className="cr__headerMobile-dropdown-information-photo"
                onClick={() => {
                  toggle();
                  onGTMTracking('Photo');
                }}
              >
                <img
                  src={user.profile?.profile_photo?.small}
                  alt={`${user?.profile?.first_name} ${user?.profile?.last_name}`}
                  loading="lazy"
                />
                <span className="cr__textColor--colorDark300 cr__text--paragraph">
                  {user?.profile?.first_name} {user?.profile?.last_name}
                </span>
              </Link>
              <div
                className="cr__headerMobile-dropdown-information-arrow"
                onClick={() => {
                  toggleProfileOptions();
                  onGTMTracking(
                    !activeProfileOptions
                      ? 'OpenProfileOptions'
                      : 'CloseProfileOptions',
                  );
                }}
              >
                <img
                  src={arrow}
                  alt="Abrir Menú"
                  className={activeProfileOptions ? 'activeProfileOptions' : ''}
                />
              </div>
            </div>
            <div className="cr__headerMobile-dropdown-store">
              {!user?.profile?.has_store ? (
                <Link
                  className="cr__textColor--colorMain300 cr__text--paragraph"
                  to={'/stores/create'}
                  onClick={() => {
                    toggle();
                    onGTMTracking('CreateStore');
                  }}
                >
                  Abre una Tienda
                </Link>
              ) : (
                <Link
                  className="cr__textColor--colorMain300 cr__text--paragraph"
                  to={'/my-store/dashboard'}
                  onClick={() => {
                    toggle();
                    onGTMTracking('MyStore');
                  }}
                >
                  Ir a mi Tienda
                </Link>
              )}
            </div>
          </React.Fragment>
        ) : (
          <div className="cr__headerMobile-dropdown-guest">
            <div className="cr__headerMobile-dropdown-guest-text">
              <span className="cr__textColor--colorDark300 cr__text--paragraph">
                No has accedido
              </span>
              <p className="cr__textColor--colorDark100 cr__text--caption">
                Saca todo el potencial de Canasta Rosa con tu cuenta.
              </p>
            </div>
            <div className="cr__headerMobile-dropdown-guest-buttons">
              <a
                className="cr__textColor--colorMain300 cr__text--paragraph"
                onClick={() => {
                  openModalBox(Login);
                  toggle();
                  onGTMTracking('Login');
                }}
              >
                Inicia Sesi&oacute;n
              </a>
              <a
                className="cr__textColor--colorDark100 cr__text--paragraph"
                onClick={() => {
                  openModalBox(Register);
                  toggle();
                  onGTMTracking('Register');
                }}
              >
                Reg&iacute;strate
              </a>
            </div>
          </div>
        )}
        {activeProfileOptions && (
          <ProfileOptions
            logout={logout}
            toggle={toggle}
            activeProfileOptions={activeProfileOptions}
          />
        )}
        <div className="cr__headerMobile-dropdown-sections">
          <div
            className="cr__headerMobile-dropdown-sections-title"
            onClick={() => {
              onSetSection('category');
              onGTMTracking('Categories');
            }}
          >
            <span className="cr__textColor--colorDark300 cr__text--paragraph">
              Categor&iacute;as
            </span>
          </div>
          <div
            className="cr__headerMobile-dropdown-sections-title"
            onClick={() => {
              onSetSection('interest');
              onGTMTracking('Interests');
            }}
          >
            <span className="cr__textColor--colorDark300 cr__text--paragraph">
              Intereses
            </span>
          </div>
          <Link
            className="
            cr__headerMobile-dropdown-sections-link 
            cr__textColor--colorDark300 cr__text--paragraph"
            to="/stores"
            onClick={() => {
              toggle();
              onGTMTracking('Stores');
            }}
          >
            Tiendas
          </Link>
          <div
            className="cr__headerMobile-dropdown-sections-title"
            onClick={() => {
              onSetSection('collections');
              onGTMTracking('Collections');
            }}
          >
            <span className="cr__textColor--colorDark300 cr__text--paragraph">
              Colecciones
            </span>
          </div>
          <Link
            className=" cr__headerMobile-dropdown-sections-link cr__textColor--colorDark300 cr__text--paragraph"
            to="/inspire"
            onClick={() => {
              toggle();
              onGTMTracking('Inspire');
            }}
          >
            Inspire
          </Link>
        </div>
      </div>
      {/** Underlay */}
      <div
        className={activeMenu ? 'activeDropdown' : ''}
        onClick={() => {
          onSetDefaultMenu();
          onGTMTracking('CloseMenuByUnderlay');
        }}
        style={{ height: componentHeight * 4 }}
      />
    </React.Fragment>
  );
}
