import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { formValueSelector } from 'redux-form';
import { bindActionCreators } from 'redux';
import { modalBoxActions, userActions, statusWindowActions } from '../../../Actions';
import { landingActions } from '../../../Actions/landing.actions';
import { SHOPPING_CART_FORM_CONFIG } from '../../../Utils/shoppingCart/shoppingCartFormConfig';
import TopHeader from './TopHeader/TopHeader';
import MainHeader from './MainHeader/MainHeader';
import Search from '../Common/Search/Search';
import Dropdown from './Dropdown/Dropdown';
import Collections from './Dropdown/Collections/Collections';
import Categories from './Dropdown/Categories/Categories';
import './headerMobile.scss';

function HeaderMobile(props) {
  //
  const [section, setSection] = useState(null);
  const [activeMenu, setActiveMenu] = useState(false);
  const toggle = () => {
    if (activeMenu) {
      setActiveMenu(false);
      setSection(null);
    } else {
      setActiveMenu(true);
      setSection('main');
    }
  };
  const onSetDefaultMenu = () => {
    setActiveMenu(false);
    setSection('');
  };
  const onSetSection = (value) => {
    setSection(value);
  };
  useEffect(() => {
    return () => {
      console.log('clean up');
    };
  }, []);

  //Props Functions
  const {
    openModalBox,
    closeModalBox,
    addGuestAddress,
    deleteGuestAddress,
    openStatusWindow,
    deleteAddress,
    logout,
  } = props;

  //Props Data
  const {
    currentUser = {},
    appSection = '',
    history = {},
    cart = {},
    marketCategories = {},
    marketInterests = {},
    landing = {},
    user = {},
    shippingAddress = {},
  } = props;

  return (
    <div className="cr__headerMobile">
      <TopHeader
        shippingAddress={shippingAddress}
        openModalBox={openModalBox}
        closeModalBox={closeModalBox}
        currentUser={currentUser}
        addGuestAddress={addGuestAddress}
        deleteGuestAddress={deleteGuestAddress}
        openStatusWindow={openStatusWindow}
        deleteAddress={deleteAddress}
        onSetDefaultMenu={onSetDefaultMenu}
      />

      <MainHeader
        cart={cart}
        toggle={toggle}
        activeMenu={activeMenu}
        onSetDefaultMenu={onSetDefaultMenu}
      />

      <div className="cr__headerMobile-search">
        <Search
          history={history}
          appSection={appSection}
          onSetDefaultMenu={onSetDefaultMenu}
        />
      </div>

      {activeMenu && section === 'main' && (
        <Dropdown
          user={user}
          currentUser={currentUser}
          openModalBox={openModalBox}
          toggle={toggle}
          logout={logout}
          onSetSection={onSetSection}
          activeMenu={activeMenu}
          section={section}
          onSetDefaultMenu={onSetDefaultMenu}
        />
      )}
      {activeMenu && section === 'collections' && (
        <Collections
          collections={landing}
          toggle={toggle}
          onSetSection={onSetSection}
          onSetDefaultMenu={onSetDefaultMenu}
          activeMenu={activeMenu}
          section={section}
        />
      )}
      {activeMenu && section === 'category' && (
        <Categories
          toggle={toggle}
          categories={marketCategories}
          onSetSection={onSetSection}
          activeMenu={activeMenu}
          section={section}
          onSetDefaultMenu={onSetDefaultMenu}
        />
      )}
      {activeMenu && section === 'interest' && (
        // used for Interersts too
        <Categories
          toggle={toggle}
          categories={marketInterests}
          onSetSection={onSetSection}
          activeMenu={activeMenu}
          section={section}
          onSetDefaultMenu={onSetDefaultMenu}
        />
      )}
    </div>
  );
}

const formSelector = formValueSelector(SHOPPING_CART_FORM_CONFIG.formName);

function mapStateToProps(state) {
  return {
    landing: state.landing,
    shippingAddress: formSelector(state, 'addressShipping'),
  };
}
function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      openModalBox: modalBoxActions.open,
      closeModalBox: modalBoxActions.close,
      updateAddress: userActions.updateAddress,
      logout: userActions.logout,
      addGuestAddress: userActions.addGuestAddress,
      deleteGuestAddress: userActions.deleteGuestAddress,
      listAddress: userActions.listAddresses,
      openStatusWindow: statusWindowActions.open,
      deleteAddress: userActions.deleteAddress,
    },
    dispatch,
  );
}

// Export Component
export default connect(mapStateToProps, mapDispatchToProps)(HeaderMobile);
