import React, { useState, useEffect } from 'react';
import ModalBoxZipCode from '../../Common/ModalBox/ModalBoxZipCode';
import { trackWithGTM } from '../../../../Utils/trackingUtils';
import './topHeader.scss';

const mapImage = require('../../images/map--white.svg');

const onGTMTracking = (value) => {
  const data = {
    id: 'mobile',
    name: `TopHeader-${value}`,
    position: 1,
  };
  trackWithGTM('eec.impressionClick', [data], 'Menu-Mobile');
};

const SpecialMessage = React.memo(function SpecialMessage() {
  // const [messageBox, setMessageBox] = useState(false);
  return (
    <div className="cr__headerMobile-top-message">
      {/* <span
        className="cr__textColor--colorYellow200 cr__text--caption"
        onClick={() => {
          // setMessageBox(!messageBox);
          onGTMTracking(`CovidMessage-${!messageBox ? 'Open' : 'Close'}`);
        }}
      > */}
      {/* Covid-19 */}
      {/* Por el momento, nuestras entregas podrían presentar una demora.
       */}
      {/* Aviso
      </span> */}
      {/* {messageBox && (
        <div
          className="cr__headerMobile-top-messageBox"
          onClick={() => {
            // setMessageBox(!messageBox);
            onGTMTracking('CovidMessage-Close');
          }}
        >
          <p className="cr__textColor--colorWhite">
            {/* Por el momento, nuestras entregas podrían presentar una demora. */}
      {/* Debido a la contingencia actual, nuestras entregas podrían presentar una
            demora fuera de lo habitual. */}
      {/* </p>
        </div>
      )} */}
    </div>
  );
});

export default function TopHeader(props) {
  const {
    openModalBox,
    closeModalBox,
    addGuestAddress,
    deleteGuestAddress,
    openStatusWindow,
    deleteAddress,
    updateAddress,
    shippingAddress,
    onSetDefaultMenu,
    currentUser = {},
  } = props;

  const isLogged = currentUser?.isLogged;
  const hasAddress = currentUser?.addresses?.addresses?.length >= 1;
  const addressesList = currentUser?.addresses?.addresses;
  const defaultAddressSearch = !isLogged
    ? shippingAddress
    : addressesList?.find((address) => address?.default_address);
  const defaultAddressByZipcode = defaultAddressSearch?.zip_code;

  const getZipCodeGuest = (zipcode) => {
    setZipCodeGuest(zipcode);
  };

  //
  const [zipCodeGuest, setZipCodeGuest] = useState(defaultAddressByZipcode);
  useEffect(() => {
    getZipCodeGuest(defaultAddressByZipcode);
  }, [defaultAddressByZipcode]);

  //
  const openZipCodeModalBox = () => {
    openModalBox(() => (
      <ModalBoxZipCode
        closeModalBox={closeModalBox}
        openModalBox={openModalBox}
        isLogged={isLogged}
        addGuestAddress={addGuestAddress}
        deleteGuestAddress={deleteGuestAddress}
        getZipCodeGuest={getZipCodeGuest}
        openStatusWindow={openStatusWindow}
        defaultAddressSearch={defaultAddressSearch}
        deleteAddress={deleteAddress}
        updateAddress={updateAddress}
      />
    ));
  };
  //
  const showZipCodebyUser = () => {
    if (isLogged && hasAddress) {
      return `Enviar a: CP. ${defaultAddressByZipcode}`;
    } else if (!isLogged && zipCodeGuest) {
      return `Enviar a: CP. ${zipCodeGuest}`;
    } else {
      return 'Elige tu dirección';
    }
  };

  return (
    <div className="cr__headerMobile-top">
      <SpecialMessage />
      <div
        className="cr__headerMobile-top-zipCode"
        onClick={() => {
          openZipCodeModalBox();
          onSetDefaultMenu();
          onGTMTracking('OpenZipcodeModal');
        }}
        tandindex="2"
        role="button"
        aria-haspopup="true"
        id="zipCodeBox"
      >
        <img
          src={mapImage}
          alt="Dirección de envío"
          className="cr__headerMobile-top-icon"
        />
        <span className="cr__text--caption cr__textColor--colorWhite">
          {showZipCodebyUser()}
        </span>
      </div>
    </div>
  );
}
