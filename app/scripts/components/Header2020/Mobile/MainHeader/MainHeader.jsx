import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import { trackWithGTM } from '../../../../Utils/trackingUtils';
import './mainHeader.scss';

const cartIcon = require('../../../../../images/icons/NEW--cart.svg');

const onGTMTracking = (value) => {
  const data = {
    id: 'mobile',
    name: `MainHeader-${value}`,
    position: 1,
  };
  trackWithGTM('eec.impressionClick', [data], 'Menu-Mobile');
};

const Logo = React.memo(function Logo(props) {
  return (
    <div className="cr__headerMobile-main-logo" tabIndex="6">
      <Link
        to="/"
        aria-label="Ir a pagina principal"
        onClick={() => {
          props.onSetDefaultMenu();
          onGTMTracking('Logo');
        }}
      />
    </div>
  );
});

export default function MainHeader(props) {
  //
  const { cart, toggle, onSetDefaultMenu, activeMenu } = props;
  //
  return (
    <div className="cr__headerMobile-main">
      <Logo onSetDefaultMenu={onSetDefaultMenu} />
      <div className="cr__headerMobile-main-icons">
        <Link
          to="/cart"
          className="cr__headerMobile-main-cart"
          onClick={() => {
            onSetDefaultMenu();
            onGTMTracking('Cart');
          }}
        >
          <h5 className="cr__headerMobile-main-cartCounter cr__textColor--colorWhite cr__text--caption">
            {cart?.products?.length}
          </h5>
          <img
            src={cartIcon}
            alt="Carrito"
            className="cr__headerMobile-main-cartIcon"
          />
        </Link>
        <div className="cr__headerMobile-main-menu">
          <a
            className={`cr__headerMobile-main-hamburger ${
              activeMenu ? 'activeHamburger' : ''
            }`}
            onClick={() => {
              toggle();
              onGTMTracking(!activeMenu ? 'OpenHamburger' : 'CloseHamburger');
            }}
          />
        </div>
      </div>
    </div>
  );
}
