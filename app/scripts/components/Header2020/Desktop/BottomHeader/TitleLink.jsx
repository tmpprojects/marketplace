import React from 'react';
import { NavLink } from 'react-router-dom';
import { trackWithGTM } from '../../../../Utils/trackingUtils';

const onGTMTracking = (value) => {
  const data = {
    id: 'web',
    name: `BottomHeader-${value}`,
    position: 1,
  };
  trackWithGTM('eec.impressionClick', [data], 'Menu-Web');
};

function TitleLink({ name = '', route = '', setSectionDropdown, section }) {
  return (
    <NavLink
      to={route}
      className="cr__header-bottomHeader-titles"
      onClick={() => {
        setSectionDropdown(name);
        onGTMTracking(name);
      }}
      // activeClassName='activeSection'
    >
      <span
        className={`cr__text--caption cr__header-bottomHeader-titles-title ${
          name === section ? 'activeSection' : 'hovering'
        }`}
      >
        {name}
      </span>
    </NavLink>
  );
}
export default TitleLink;
