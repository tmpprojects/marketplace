import React from 'react';
import TitleLink from './TitleLink';
import TitleWithIcon from './TitleWithIcon';
import DropdownCollections from '../Dropdowns/DropdownCollections';
import './bottomHeader.scss';

export default function BottomHeader(props) {
  const {
    setSectionDropdown,
    section = '',
    activeDropdown,
    landing = {},
    setFalseDropdown,
  } = props;
  return (
    <div className="cr__header-bottomHeader">
      <TitleWithIcon
        name="CATEGORÍAS"
        setSectionDropdown={setSectionDropdown}
        section={section}
        tandindex="10"
      />
      <TitleLink
        name="TIENDAS"
        setSectionDropdown={setSectionDropdown}
        route="/stores"
        section={section}
        tandindex="11"
      />
      <TitleWithIcon
        name="INTERESES"
        setSectionDropdown={setSectionDropdown}
        section={section}
        tandindex="12"
      />
      {/* <TitleLink
        name='PROMOCIONES'
        setSectionDropdown={setSectionDropdown}
        route='/promociones'
        section={section}
        tandindex='13'
      /> */}
      <TitleWithIcon
        name="COLECCIONES"
        setSectionDropdown={setSectionDropdown}
        section={section}
        tandindex="14"
      />
      <TitleLink
        name="INSPIRE"
        setSectionDropdown={setSectionDropdown}
        route="/inspire"
        section={section}
        tandindex="15"
      />

      {activeDropdown && section === 'COLECCIONES' && (
        <DropdownCollections
          section={section}
          landing={landing}
          setFalseDropdown={setFalseDropdown}
          setSectionDropdown={setSectionDropdown}
        />
      )}
    </div>
  );
}
