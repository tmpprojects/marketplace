import React from 'react';
import { trackWithGTM } from '../../../../Utils/trackingUtils';

const arrowDown = require('../../../../../images/icons/arrows/arrow_down.svg');

const onGTMTracking = (value) => {
  const data = {
    id: 'web',
    name: `BottomHeader-${value}`,
    position: 1,
  };
  trackWithGTM('eec.impressionClick', [data], 'Menu-Web');
};

export default function TitleWithIcon({ name = '', setSectionDropdown, section }) {
  return (
    <div
      className="cr__header-bottomHeader-titles"
      onClick={() => {
        setSectionDropdown(name);
        onGTMTracking(name);
      }}
    >
      <span
        className={`cr__text--caption cr__header-bottomHeader-titles-title ${
          name === section ? 'activeSection' : 'hovering'
        }`}
      >
        {name}
      </span>
      <img
        src={arrowDown}
        alt="Ir a mi cuenta"
        className={`cr__header-bottomHeader-titles-${
          name === section ? 'activeIcon' : 'icon'
        }`}
        loading="lazy"
      />
    </div>
  );
}
