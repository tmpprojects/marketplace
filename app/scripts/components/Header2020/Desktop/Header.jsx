import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { formValueSelector } from 'redux-form';

import { modalBoxActions, userActions, statusWindowActions } from '../../../Actions';
import { SHOPPING_CART_FORM_CONFIG } from '../../../Utils/shoppingCart/shoppingCartFormConfig';
import TopHeader from './TopHeader/TopHeader';
import MainHeader from './MainHeader/MainHeader';
import BottomHeader from './BottomHeader/BottomHeader';
import DropdownCategories from './Dropdowns/DropdownCategories';
import DropdownInterests from './Dropdowns/DropdownInterests';
import DropdownMyAccount from './Dropdowns/DropdownMyAccount';
import './header.scss';

class Header extends Component {
  state = {
    section: '',
    activeDropdown: false,
    // scrolling: false,
    // height: window.innerHeight,
    // lastScroll: 0,
  };
  componentDidMount() {
    // this.prev = window.scrollY;
    // window.addEventListener('scroll', this.isScrolling);
    // window.addEventListener('scroll', (e) => this.detectScrollDirection(e));
    // window.addEventListener('resize', this.updateHeight);
  }
  // componentWillUnmount() {
  // window.removeEventListener('scroll', this.isScrolling);
  // window.removeEventListener('scroll', (e) => this.detectScrollDirection(e));
  // window.removeEventListener('resize', this.updateHeight);
  // }
  setSectionDropdown = (value) => {
    const sectionValue = value;
    if (
      this.state.section === sectionValue ||
      sectionValue === 'PROMOCIONES' ||
      sectionValue === 'INSPIRE' ||
      sectionValue === 'TIENDAS'
    ) {
      return this.setState({ section: '', activeDropdown: false });
    }
    if (this.state.section !== sectionValue) {
      return this.setState({
        section: sectionValue,
        activeDropdown: true,
      });
    }
  };
  setFalseDropdown = () => {
    this.setState({ activeDropdown: false });
  };
  // updateHeight = () => {
  //   this.setState({ height: window.innerHeight });
  // };
  // isScrolling = () => {
  //   const { height, scrolling, activeDropdown } = this.state;
  //   if (height >= 660) {
  //     this.setState({ scrolling: true });
  //   }
  //   if (scrolling && activeDropdown) {
  //     this.setState({ activeDropdown: false, section: '' });
  //   }
  // };
  // detectScrollDirection = (e) => {
  //   const window = e.currentTarget;
  //   if (this.prev > window.scrollY) {
  //     this.props.onHidingMenu(true);
  //   } else if (this.prev < window.scrollY) {
  //     this.props.onHidingMenu(false);
  //   }
  //   this.prev = window.scrollY;
  // };

  render() {
    const {
      section = '',
      activeDropdown = false,
      // scrolling,
      // height,
    } = this.state;
    //Props Functions
    const {
      openModalBox,
      closeModalBox,
      addGuestAddress,
      deleteGuestAddress,
      openStatusWindow,
      deleteAddress,
      logout,
    } = this.props;
    //Props Data
    const {
      currentUser = {},
      appSection = '',
      history = {},
      cart = {},
      marketCategories = {},
      landing = {},
      user = {},
      shippingAddress = {},
      marketInterests = {},
    } = this.props;

    return (
      <div
        className="cr__header"
        aria-label="Canasta Rosa"
        role="banner"
        style={{ height: '121px' }}
      >
        {/* (Left Side) 
        Extra: Random Information (
          Right Side) 
          1. ZipCode Button 
          2. Link to /about-us/sell 
          3. Link to help: Zendesk link */}
        <TopHeader
          shippingAddress={shippingAddress}
          openModalBox={openModalBox}
          closeModalBox={closeModalBox}
          currentUser={currentUser}
          addGuestAddress={addGuestAddress}
          deleteGuestAddress={deleteGuestAddress}
          openStatusWindow={openStatusWindow}
          deleteAddress={deleteAddress}
        />

        {/* 1. CR Logo 
            2. Input Search 
            3. Dropdown My account 
            4. Cart Count / Link to /cart*/}
        <MainHeader
          appSection={appSection}
          history={history}
          cart={cart}
          setFalseDropdown={this.setFalseDropdown}
          setSectionDropdown={this.setSectionDropdown}
          section={section}
          user={currentUser}
          openModalBox={openModalBox}
          activeDropdown={activeDropdown}
        />
        {/* 1.Categories Dropdown 
            2. Link to /stores 
            3. Link to /promociones 
            4. Collections Dropdown 
            5. Link to /Inspire */}
        <BottomHeader
          section={section}
          activeDropdown={activeDropdown}
          landing={landing}
          setFalseDropdown={this.setFalseDropdown}
          setSectionDropdown={this.setSectionDropdown}
        />

        {activeDropdown && section === 'CATEGORÍAS' && (
          <DropdownCategories
            section={section}
            marketCategories={marketCategories}
            setFalseDropdown={this.setFalseDropdown}
            setSectionDropdown={this.setSectionDropdown}
          />
        )}
        {activeDropdown && section === 'INTERESES' && (
          <DropdownInterests
            section={section}
            marketInterests={marketInterests}
            setFalseDropdown={this.setFalseDropdown}
            setSectionDropdown={this.setSectionDropdown}
          />
        )}

        {activeDropdown && section === 'CUENTA' && (
          <DropdownMyAccount
            setFalseDropdown={this.setFalseDropdown}
            setSectionDropdown={this.setSectionDropdown}
            user={user}
            logout={logout}
          />
        )}
      </div>
    );
  }
}
const formSelector = formValueSelector(SHOPPING_CART_FORM_CONFIG.formName);

function mapStateToProps(state) {
  return {
    landing: state.landing,
    shippingAddress: formSelector(state, 'addressShipping'),
    marketInterests: state.app.marketInterests.interests,
  };
}
function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      openModalBox: modalBoxActions.open,
      closeModalBox: modalBoxActions.close,
      updateAddress: userActions.updateAddress,
      logout: userActions.logout,
      addGuestAddress: userActions.addGuestAddress,
      deleteGuestAddress: userActions.deleteGuestAddress,
      listAddress: userActions.listAddresses,
      openStatusWindow: statusWindowActions.open,
      deleteAddress: userActions.deleteAddress,
    },
    dispatch,
  );
}

// Export Component
export default connect(mapStateToProps, mapDispatchToProps)(Header);
