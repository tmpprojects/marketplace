import { Link } from 'react-router-dom';
import React, { useState, useEffect } from 'react';
import { trackWithGTM } from '../../../../Utils/trackingUtils';
import ModalBoxZipCode from '../../Common/ModalBox/ModalBoxZipCode';
import './topHeader.scss';

const onGTMTracking = (value) => {
  const data = {
    id: 'web',
    name: `TopHeader-${value}`,
    position: 1,
  };
  trackWithGTM('eec.impressionClick', [data], 'Menu-Web');
};

const SellWithUs = React.memo(function SellWithUs() {
  return (
    <div
      className="cr__header-topHeader-item"
      tandindex="4"
      aria-label="Vende con nosotros"
    >
      <Link
        to="/about-us/sell"
        className="cr__text--caption"
        role="link"
        onClick={() => onGTMTracking('SellWithUs')}
      >
        Vende con nosotros
      </Link>
    </div>
  );
});

export default function TopHeader(props) {
  const {
    openModalBox,
    closeModalBox,
    addGuestAddress,
    deleteGuestAddress,
    openStatusWindow,
    deleteAddress,
    updateAddress,
    shippingAddress,
    currentUser = {},
  } = props;

  const isLogged = currentUser?.isLogged;
  const hasAddress = currentUser?.addresses?.addresses?.length >= 1;
  const addressesList = currentUser?.addresses?.addresses;
  const defaultAddressSearch = !isLogged
    ? shippingAddress
    : addressesList?.find((address) => address?.default_address);
  const defaultAddressByZipcode = defaultAddressSearch?.zip_code;

  const getZipCodeGuest = (zipcode) => {
    setZipCodeGuest(zipcode);
  };

  //
  const [zipCodeGuest, setZipCodeGuest] = useState(defaultAddressByZipcode);
  useEffect(() => {
    getZipCodeGuest(defaultAddressByZipcode);
  }, [defaultAddressByZipcode]);

  //
  const openZipCodeModalBox = () => {
    openModalBox(() => (
      <ModalBoxZipCode
        closeModalBox={closeModalBox}
        openModalBox={openModalBox}
        isLogged={isLogged}
        addGuestAddress={addGuestAddress}
        deleteGuestAddress={deleteGuestAddress}
        getZipCodeGuest={getZipCodeGuest}
        openStatusWindow={openStatusWindow}
        defaultAddressSearch={defaultAddressSearch}
        deleteAddress={deleteAddress}
        updateAddress={updateAddress}
      />
    ));
  };

  //
  const showZipCodebyUser = () => {
    if (isLogged && hasAddress) {
      return `Enviar a: CP. ${defaultAddressByZipcode}`;
    } else if (!isLogged && zipCodeGuest) {
      return `Enviar a: CP. ${zipCodeGuest}`;
    } else {
      return 'Elige tu dirección';
    }
  };
  return (
    <div className="cr__header-topHeader">
      <div className="cr__header-topHeader-containerA ">
        <div
          className="cr__header-topHeader-item withoutButton cr__text--caption"
          onClick={() => onGTMTracking('CovidMessage')}
        >
          {/* Debido a la contingencia actual, nuestras entregas podrían presentar una
          demora fuera de lo habitual. */}
        </div>
      </div>
      <div className="cr__header-topHeader-containerB">
        <div
          className="cr__header-topHeader-item withoutButton cr__text--caption cr__textColor--colorYellow200"
          onClick={() => {
            openZipCodeModalBox();
            onGTMTracking('OpenZipcodeModal');
          }}
          tandindex="2"
          role="button"
          aria-haspopup="true"
          id="zipCodeBox"
        >
          {showZipCodebyUser()}
        </div>
        {/* <div className='cr__header-topHeader-item'
          tandindex='3' 
        >
          <Link to='/about-us/sell' className='cr__text--caption'>
            ¿Donde esta mi pedido?
          </Link>
        </div> */}
        <SellWithUs />
        <div className="cr__header-topHeader-item" tandindex="5" aria-label="Ayuda">
          <a
            href="https://ayuda.canastarosa.com/"
            className="cr__text--caption"
            role="link"
            target="_blank"
            rel="noopener"
            onClick={() => onGTMTracking('Help')}
          >
            Ayuda
          </a>
        </div>
      </div>
    </div>
  );
}
