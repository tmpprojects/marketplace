import { Link } from 'react-router-dom';
import React from 'react';
import Search from '../../Common/Search/Search';
import Login from '../../../../Utils/Login';
import { trackWithGTM } from '../../../../Utils/trackingUtils';
import './mainHeader.scss';

const arrowDown = require('../../../../../images/icons/arrows/arrow_down.svg');
const cartIcon = require('../../../../../images/icons/NEW--cart.svg');

const onGTMTracking = (value) => {
  const data = {
    id: 'web',
    name: `MainHeader-${value}`,
    position: 1,
  };
  trackWithGTM('eec.impressionClick', [data], 'Menu-Web');
};

const Logo = React.memo(function Logo() {
  return (
    <div className="cr__header-mainHeader-logo" tabIndex="6">
      <Link
        to="/"
        aria-label="Ir a pagina principal"
        onClick={() => onGTMTracking('Logo')}
      />
    </div>
  );
});

const Cart = React.memo(function Cart() {
  return (
    <span className="cr__header-mainHeader-cart-title cr__textColor--colorWhite cr__text--paragraph">
      Carrito
    </span>
  );
});

export default function MainHeader(props) {
  const {
    cart = {},
    history = {},
    setFalseDropdown,
    setSectionDropdown,
    appSection = '',
    section = '',
    user = {},
    openModalBox,
    activeDropdown = false,
  } = props;

  const mouseOut = () => {
    if (section !== 'CUENTA') {
      setFalseDropdown();
      setSectionDropdown();
    }
  };
  const setCuentaDropdown = () => {
    if (user?.isLogged) {
      setSectionDropdown('CUENTA');
    }
    if (!user?.isLogged) {
      openModalBox(Login);
    }
  };
  const tagging = user?.isLogged ? 'MyAccount' : 'Login';

  return (
    <div
      className="cr__header-mainHeader"
      onMouseOut={() => {
        mouseOut();
      }}
    >
      <Logo />
      <Search history={history} appSection={appSection} />
      <div
        className="cr__header-mainHeader-myAccount"
        aria-label="Abrir menú de mi cuenta"
        tabIndex="8"
        onClick={() => {
          setCuentaDropdown();
          onGTMTracking(tagging);
        }}
      >
        <span className="cr__textColor--colorWhite cr__text--paragraph cr__header-mainHeader-myAccount-title">
          {user?.isLogged ? 'Mi cuenta' : 'Ingresa'}
        </span>
        {user?.isLogged && (
          <img
            src={arrowDown}
            alt="Ir a mi cuenta"
            className={`cr__header-mainHeader-myAccount-icon  ${
              activeDropdown && section === 'CUENTA' ? 'dropdownOpen' : ''
            }`}
            loading="lazy"
          />
        )}
      </div>
      <div className="cr__header-mainHeader-separator" />
      <div
        className="cr__header-mainHeader-cart"
        aria-label={`${cart?.products?.length} artículo${
          cart?.products?.length === 1 ? '' : 's'
        } en tu carrito`}
        tabIndex="9"
      >
        <Link to="/cart" onClick={() => onGTMTracking('Cart')}>
          <div className="cr__header-mainHeader-cart-containerIcon">
            <h5 className="cr__header-mainHeader-cart-cartCounter cr__textColor--colorWhite cr__text--caption">
              {cart?.products?.length}
            </h5>
            <img
              src={cartIcon}
              alt="Carrito"
              className="cr__header-mainHeader-cart-icon"
            />
          </div>
          <Cart />
        </Link>
      </div>
    </div>
  );
}
