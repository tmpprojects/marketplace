import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import { trackWithGTM } from '../../../../Utils/trackingUtils';
import { IconPreloader } from '../../../../Utils/Preloaders';
import './dropdownCategories.scss';

const arrowNext = '../../../../images/icons/arrows/arrow_next.svg';

const onGTMTracking = (value) => {
  const data = {
    id: 'web',
    name: `Dropdown-Categories-${value}`,
    position: 1,
  };
  trackWithGTM('eec.impressionClick', [data], 'Menu-Web');
};

export default function Dropdown({
  section = null,
  marketCategories = {},
  setSectionDropdown,
  setFalseDropdown,
}) {
  const [isTablet, setIsTablet] = useState(false);

  useEffect(() => {
    if (
      /(ipad|tablet|(android(?!.*mobile))|(windows(?!.*phone)(.*touch))|kindle|playbook|silk|(puffin(?!.*(IP|AP|WP))))/.test(
        navigator.userAgent.toLowerCase(),
      )
    ) {
      setIsTablet(true);
    }
  }, []);

  const [selectedCategory, setSelectedCategory] = useState({
    slug: null,
    name: null,
    children: [],
  });

  const foundChildrenLevel2 = marketCategories?.find(
    (category) => category?.slug === selectedCategory?.slug,
  );

  const [defaultCategoryClass, setDefaultCategoryClass] = useState(false);

  const onSetDefaultCategory = () => {
    if (!selectedCategory?.slug) {
      marketCategories?.slice(0, 1)?.map(({ slug, name, children: level1 }) => {
        setSelectedCategory({
          slug,
          name,
          children: level1,
        });
      });
      setDefaultCategoryClass(true);
    }
  };
  return (
    <React.Fragment>
      {marketCategories.length >= 1 ? (
        <div className="cr__category-container">
          <div
            className="cr__header-dropdown"
            onMouseLeave={() => {
              setFalseDropdown();
              setSectionDropdown('');
            }}
          >
            {section === 'CATEGORÍAS' && (
              <ul className="cr__header-dropdown-categoriesLevel1">
                {marketCategories?.map(
                  ({ slug = '', name = '', children: level1 = [] }, i) => (
                    <React.Fragment key={i}>
                      {!isTablet ? (
                        <div
                          onClick={() => {
                            setFalseDropdown();
                            setSectionDropdown('');
                            onGTMTracking(slug);
                          }}
                          onMouseOver={() => {
                            setSelectedCategory({
                              slug,
                              name,
                              children: level1,
                            });
                            setDefaultCategoryClass(false);
                          }}
                          onLoad={() => onSetDefaultCategory()}
                        >
                          <Link to={`/category/${slug}/`}>
                            <li
                              className={`cr__header-dropdown-categoriesLevel1-item cr__text--paragraph ${
                                (defaultCategoryClass && i === 0) ||
                                name === selectedCategory.name
                                  ? 'defaultCategory'
                                  : ''
                              }`}
                            >
                              <span>{name}</span>
                              {level1?.length >= 1 && (
                                <img src={arrowNext} alt="Mas categorias" />
                              )}
                            </li>
                          </Link>
                        </div>
                      ) : (
                        <React.Fragment>
                          {/*LOGIC FOR TOUCHABLE DEVICES ONLY, BEING A WIDER COMPONENT ITS ONLY FOR TABLETS */}
                          {level1?.length ? (
                            <div
                              onClick={() => {
                                setSelectedCategory({
                                  slug,
                                  name,
                                  children: level1,
                                });
                                setDefaultCategoryClass(false);
                              }}
                              onLoad={() => onSetDefaultCategory()}
                            >
                              <li
                                className={`cr__header-dropdown-categoriesLevel1-item cr__text--paragraph ${
                                  defaultCategoryClass && i === 0
                                    ? 'defaultCategory'
                                    : ''
                                }`}
                              >
                                <span>{name}</span>
                                {level1?.length >= 1 && (
                                  <img src={arrowNext} alt="Mas categorias" />
                                )}
                              </li>
                            </div>
                          ) : (
                            <div onLoad={() => onSetDefaultCategory()}>
                              {/*When they dont have child categories */}
                              <Link
                                to={`/category/${slug}/`}
                                onClick={() => {
                                  setFalseDropdown();
                                  setSectionDropdown('');
                                  setDefaultCategoryClass(false);
                                  onGTMTracking(slug);
                                }}
                              >
                                <li
                                  className={`cr__header-dropdown-categoriesLevel1-item cr__text--paragraph ${
                                    defaultCategoryClass && i === 0
                                      ? 'defaultCategory'
                                      : ''
                                  }`}
                                >
                                  <span>{name}</span>
                                </li>
                              </Link>
                            </div>
                          )}
                        </React.Fragment>
                      )}
                    </React.Fragment>
                  ),
                )}
              </ul>
            )}
            {/** TITLE CATEGORY LEVEL 2*/}
            <div className="cr__header-dropdown-categoriesLevel2">
              {selectedCategory?.children?.length >= 1 && (
                <div className="cr__header-dropdown-categoriesLevel2-title">
                  <span className="cr__text--subtitle3 cr__textColor--colorDark300">
                    {selectedCategory?.name}
                  </span>
                  <Link
                    to={`/category/${selectedCategory?.slug}`}
                    onClick={() => {
                      setFalseDropdown();
                      setSectionDropdown('');
                      onGTMTracking(`All-${selectedCategory?.slug}`);
                    }}
                  >
                    Ver todo{' '}
                    <img
                      src={arrowNext}
                      alt="Mas categorias"
                      aria-label="Mas categorias"
                      loading="lazy"
                    />
                  </Link>
                </div>
              )}
              <div>
                {/** CATEGORIES LEVEL 2*/}
                <ul className="cr__header-dropdown-categoriesLevel2-level2">
                  {foundChildrenLevel2 !== undefined &&
                    foundChildrenLevel2?.children?.length >= 1 &&
                    foundChildrenLevel2?.children?.map(
                      ({ slug = '', name = '', children: level3 = [] }, i) => (
                        <li
                          key={i}
                          className="cr__header-dropdown-categoriesLevel2-level2-item"
                        >
                          <Link
                            to={`/category/${slug}`}
                            onClick={() => {
                              setFalseDropdown();
                              setSectionDropdown('');
                              onGTMTracking(slug);
                            }}
                          >
                            <span className="cr__text--paragraph cr__textColor--colorDark300">
                              {name}
                              {level3?.length > 0 && (
                                <img
                                  src={arrowNext}
                                  alt="Mas categorias"
                                  aria-label="Mas categorias"
                                  loading="lazy"
                                />
                              )}
                            </span>
                          </Link>
                          {/**CATEGORIES LEVEL 3 */}
                          <div className="cr__header-dropdown-categoriesLevel2-level3">
                            {level3?.length >= 1 &&
                              level3
                                ?.slice(0, 4)
                                .map(({ name = '', slug = '' }, i) => (
                                  <Link
                                    key={i}
                                    to={`/category/${slug}`}
                                    onClick={() => {
                                      setFalseDropdown();
                                      setSectionDropdown('');
                                      onGTMTracking(slug);
                                    }}
                                  >
                                    <span className="cr__header-dropdown-categoriesLevel2-level3-name cr__text--caption">
                                      {name}
                                    </span>
                                  </Link>
                                ))}
                            {level3?.length >= 4 && (
                              <Link
                                to={`/category/${slug}`}
                                onClick={() => {
                                  setFalseDropdown();
                                  setSectionDropdown('');
                                  onGTMTracking(`All-${slug}`);
                                }}
                                className="cr__header-dropdown-categoriesLevel2-level3-more cr__text--caption cr__textColor--colorGray400"
                              >
                                Ver todo{' '}
                                <img
                                  src={arrowNext}
                                  alt="Mas categorias"
                                  aria-label="Mas categorias"
                                  loading="lazy"
                                />
                              </Link>
                            )}
                          </div>
                        </li>
                      ),
                    )}
                </ul>
              </div>
            </div>
          </div>
        </div>
      ) : (
        <div className="loading">
          <IconPreloader />
        </div>
      )}
    </React.Fragment>
  );
}
