import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import { trackWithGTM } from '../../../../Utils/trackingUtils';
import { IconPreloader } from '../../../../Utils/Preloaders';
import './dropdownInterests.scss';

const arrowNext = '../../../../images/icons/arrows/arrow_next.svg';

const onGTMTracking = (value) => {
  const data = {
    id: 'web',
    name: `Dropdown-Interests-${value}`,
    position: 1,
  };
  trackWithGTM('eec.impressionClick', [data], 'Menu-Web');
};

const DropdownInterests = ({
  section = null,
  marketInterests = {},
  setSectionDropdown,
  setFalseDropdown,
}) => {
  const [isTablet, setIsTablet] = useState(false);
  const [selectedInterest, setSelectedInterest] = useState({
    slug: null,
    name: null,
    children: [],
  });
  const [defaultInterestClass, setDefaultInterestClass] = useState(false);
  const [selectedInterestSecondLevel, setSelectedInterestSecondLevel] = useState({});

  useEffect(() => {
    if (
      /(ipad|tablet|(android(?!.*mobile))|(windows(?!.*phone)(.*touch))|kindle|playbook|silk|(puffin(?!.*(IP|AP|WP))))/.test(
        navigator.userAgent.toLowerCase(),
      )
    ) {
      setIsTablet(true);
    }
  }, []);

  const foundChildrenLevel2 = marketInterests?.find(
    (interest) => interest?.slug === selectedInterest?.slug,
  );

  const onSetDefaultInterest = () => {
    if (!selectedInterest?.slug) {
      marketInterests?.slice(0, 1)?.map(({ slug, name, children: level1 }) => {
        setSelectedInterest({
          slug,
          name,
          children: level1,
        });
      });
      setDefaultInterestClass(true);
    }
  };

  return (
    <React.Fragment>
      {marketInterests.length >= 1 ? (
        <div className="cr__interests-container">
          <div
            className="cr__header-dropdown"
            onMouseLeave={() => {
              setFalseDropdown();
              setSectionDropdown('');
            }}
          >
            {section === 'INTERESES' && (
              <ul className="cr__header-dropdown-interestsLevel1">
                {marketInterests?.map(
                  ({ slug = '', name = '', children: level1 = [] }, i) => (
                    <React.Fragment key={i}>
                      {!isTablet ? (
                        <div
                          onClick={() => {
                            setFalseDropdown();
                            setSectionDropdown('');
                            onGTMTracking(slug);
                          }}
                          onMouseOver={() => {
                            setSelectedInterest({
                              slug,
                              name,
                              children: level1,
                            });
                            setSelectedInterestSecondLevel({});
                            setDefaultInterestClass(false);
                          }}
                          onLoad={() => onSetDefaultInterest()}
                        >
                          <Link to={`/interest/${slug}/`}>
                            <li
                              className={`cr__header-dropdown-interestsLevel1-item cr__text--paragraph ${
                                (defaultInterestClass && i === 0) ||
                                name === selectedInterest.name // to maintain hover like interest
                                  ? 'defaultInterest'
                                  : ''
                              }`}
                            >
                              <span>{name}</span>
                              {level1?.length >= 1 && (
                                <img src={arrowNext} alt="Mas categorias" />
                              )}
                            </li>
                          </Link>
                        </div>
                      ) : (
                        <React.Fragment>
                          {/*LOGIC FOR TOUCHABLE DEVICES ONLY, BEING A WIDER COMPONENT ITS ONLY FOR TABLETS */}
                          {level1?.length ? (
                            <div
                              onClick={() => {
                                setSelectedInterest({
                                  slug,
                                  name,
                                  children: level1,
                                });
                                setDefaultInterestClass(false);
                              }}
                              onLoad={() => onSetDefaultInterest()}
                            >
                              <li
                                className={`cr__header-dropdown-interestsLevel1-item cr__text--paragraph ${
                                  defaultInterestClass && i === 0
                                    ? 'defaultInterest'
                                    : ''
                                }`}
                              >
                                <span>{name}</span>
                                {level1?.length >= 1 && (
                                  <img src={arrowNext} alt="Mas categorias" />
                                )}
                              </li>
                            </div>
                          ) : (
                            <div onLoad={() => onSetDefaultInterest()}>
                              {/*When they dont have child categories */}
                              <Link
                                to={`/interest/${slug}/`}
                                onClick={() => {
                                  setFalseDropdown();
                                  setSectionDropdown('');
                                  setDefaultInterestClass(false);
                                  onGTMTracking(slug);
                                }}
                              >
                                <li
                                  className={`cr__header-dropdown-interestsLevel1-item cr__text--paragraph ${
                                    defaultInterestClass && i === 0
                                      ? 'defaultInterest'
                                      : ''
                                  }`}
                                >
                                  <span>{name}</span>
                                </li>
                              </Link>
                            </div>
                          )}
                        </React.Fragment>
                      )}
                    </React.Fragment>
                  ),
                )}
              </ul>
            )}
            {/** TITLE CATEGORY LEVEL 2*/}
            <div className="cr__header-dropdown-interestsLevel2">
              {selectedInterest?.children?.length >= 1 && (
                <div className="cr__header-dropdown-interestsLevel2-title">
                  <span className="cr__text--subtitle3 cr__textColor--colorDark300">
                    {selectedInterest?.name}
                  </span>
                  <Link
                    to={`/interest/${selectedInterest?.slug}`}
                    onClick={() => {
                      setFalseDropdown();
                      setSectionDropdown('');
                      onGTMTracking(`All-${selectedInterest?.slug}`);
                    }}
                  >
                    Ver todo{' '}
                    <img
                      src={arrowNext}
                      alt="Mas categorias"
                      aria-label="Mas categorias"
                      loading="lazy"
                    />
                  </Link>
                </div>
              )}
              <div className="cr__header-dropdown-interestsLevel2-container">
                {/** CATEGORIES LEVEL 2*/}
                <div className="cr__header-dropdown-interestsLevel2-container-list">
                  <ul className="cr__header-dropdown-interestsLevel2-level2">
                    {foundChildrenLevel2 !== undefined &&
                      foundChildrenLevel2?.children?.length >= 1 &&
                      foundChildrenLevel2?.children?.map(
                        (
                          {
                            slug = '',
                            name = '',
                            children: level3 = [],
                            photo = {},
                          },
                          i,
                        ) => (
                          <li
                            key={i}
                            className="cr__header-dropdown-interestsLevel2-level2-item"
                            onMouseOver={() => {
                              setSelectedInterestSecondLevel({
                                slug,
                                name,
                                children: level3,
                                photo,
                              });
                            }}
                            // onMouseLeave={() => {
                            //   setSelectedInterestSecondLevel({});
                            // }}
                          >
                            <Link
                              to={`/interest/${slug}`}
                              onClick={() => {
                                setFalseDropdown();
                                setSectionDropdown('');
                                onGTMTracking(slug);
                              }}
                            >
                              <span className="cr__text--paragraph cr__textColor--colorDark300">
                                {name}
                                {level3.length > 0 && (
                                  <img
                                    src={arrowNext}
                                    alt="Mas categorias"
                                    aria-label="Mas categorias"
                                    loading="lazy"
                                  />
                                )}
                              </span>
                            </Link>
                            {/**CATEGORIES LEVEL 3 */}
                            <div className="cr__header-dropdown-interestsLevel2-level3">
                              {level3?.length >= 1 &&
                                level3
                                  ?.slice(0, 4)
                                  .map(({ name = '', slug = '' }, i) => (
                                    <Link
                                      key={i}
                                      to={`/interest/${slug}`}
                                      onClick={() => {
                                        setFalseDropdown();
                                        setSectionDropdown('');
                                        onGTMTracking(slug);
                                      }}
                                    >
                                      <span className="cr__header-dropdown-interestsLevel2-level3-name cr__text--caption">
                                        {name}
                                      </span>
                                    </Link>
                                  ))}
                              {level3?.length >= 4 && (
                                <Link
                                  to={`/interest/${slug}`}
                                  onClick={() => {
                                    setFalseDropdown();
                                    setSectionDropdown('');
                                    onGTMTracking(`All-${slug}`);
                                  }}
                                  className="cr__header-dropdown-interestsLevel2-level3-more cr__text--caption cr__textColor--colorGray400"
                                >
                                  Ver todo{' '}
                                  <img
                                    src={arrowNext}
                                    alt="Mas categorias"
                                    aria-label="Mas categorias"
                                    loading="lazy"
                                  />
                                </Link>
                              )}
                            </div>
                          </li>
                        ),
                      )}
                  </ul>
                </div>

                <div className="cr__header-dropdown-interestsLevel2-container-representImage">
                  {Object.keys(selectedInterestSecondLevel).length > 0 && (
                    <img
                      src={
                        selectedInterestSecondLevel.photo?.original !== ''
                          ? selectedInterestSecondLevel.photo.original
                          : require('./../../images/collections-thumbnail.jpg')
                      }
                      alt="dummy"
                    />
                  )}
                </div>
              </div>
            </div>
          </div>
        </div>
      ) : (
        <div className="loading--interests">
          <IconPreloader />
        </div>
      )}
    </React.Fragment>
  );
};

export default DropdownInterests;
