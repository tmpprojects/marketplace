import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import { IconPreloader } from '../../../../Utils/Preloaders';
import { trackWithGTM } from '../../../../Utils/trackingUtils';
import CollentionsThumbnail from '../../images/collections-thumbnail.jpg';
import Visa from './visa.jpg';
import './dropdownCollections.scss';

const onGTMTracking = (value) => {
  const data = {
    id: 'web',
    name: `Dropdown-Collections-${value}`,
    position: 1,
  };
  trackWithGTM('eec.impressionClick', [data], 'Menu-Web');
};

export default function DropdownColections(props) {
  const { landing = {}, setFalseDropdown, setSectionDropdown } = props;

  const collectionsArray = landing?.landingsList?.results?.slice(0, 7);

  /* Fix to add extra item to dropdown (first position)  */
  // const [selectedCollection, setSelectedCollection] = useState('visa');

  const [selectedCollection, setSelectedCollection] = useState(null);

  const onSelectCollection = (slug) => {
    setSelectedCollection(collectionsArray.find((item) => item.slug === slug));
  };
  const [defaultCollectionClass, setDefaultCollectionClass] = useState(false);

  const onSetDefaultCollection = () => {
    if (!selectedCollection) {
      setDefaultCollectionClass(true);

      /* Fix to add extra item to dropdown (first position)  */
      // setSelectedCollection(collectionsArray.find(({ item }, i) => i + 1 === 0));
    }
  };

  return (
    <div className="cr__header-collections">
      {landing?.landingsList?.results?.length >= 1 ? (
        <div
          className="cr__header-collections-container"
          onMouseLeave={() => {
            setFalseDropdown();
            setSectionDropdown('');
          }}
          onLoad={() => onSetDefaultCollection()}
        >
          <div className="cr__header-collections-list">
            {/* Fix to add extra item to dropdown (first position)  */}
            {/* <Link
              to={`/visa`}
              onClick={() => {
                onGTMTracking('visa-sales');
                setFalseDropdown();
                setSectionDropdown('');
              }}
              onMouseOver={() => {
                setSelectedCollection('visa');
              }}
            >
              <span
                className={`cr__header-collections-item cr__text--paragraph  ${
                  selectedCollection === 'visa' ? ' defaultCollection' : ''
                }`}
              >
                Visa Sales
              </span>
            </Link> */}
            {/** END */}
            {collectionsArray.map(({ name, slug }, i) => (
              <Link key={i} to={`/landing/${slug}`}>
                <span
                  className={`cr__header-collections-item cr__text--paragraph  ${
                    defaultCollectionClass && i === 0
                      ? // Fix for add extra item
                        // && i + 1 === 0
                        ' defaultCollection'
                      : ''
                  }`}
                  onMouseOver={() => {
                    onSelectCollection(slug);
                    setDefaultCollectionClass(false);
                  }}
                  onClick={() => {
                    setFalseDropdown();
                    setSectionDropdown('');
                    onGTMTracking(slug);
                  }}
                >
                  {name.length > 23
                    ? name.substring(0, 23).toLowerCase() + '...'
                    : name.toLowerCase()}
                </span>
              </Link>
            ))}
          </div>
          <div className="cr__header-collections-cover">
            {/* {selectedCollection === 'visa' ? (
              <img src={Visa} alt="Colección" loading="lazy" />
            ) : ( */}
            <img
              src={
                selectedCollection?.cover?.small
                  ? selectedCollection?.cover?.small
                  : CollentionsThumbnail
              }
              alt="Colección"
              loading="lazy"
            />
            {/* )} */}
          </div>
        </div>
      ) : (
        <div className="cr__header-collections-container">
          <IconPreloader />
        </div>
      )}
    </div>
  );
}
