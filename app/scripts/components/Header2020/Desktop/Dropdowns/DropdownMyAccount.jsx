import React from 'react';
import { Link } from 'react-router-dom';
import { trackWithGTM } from '../../../../Utils/trackingUtils';
import './dropdownMyAccount.scss';

const onGTMTracking = (value) => {
  const data = {
    id: 'web',
    name: `Dropdown-MyAccount-${value}`,
    position: 1,
  };
  trackWithGTM('eec.impressionClick', [data], 'Menu-Web');
};

export default React.memo(function DropdownMyAccount(props) {
  const { user = {}, setFalseDropdown, setSectionDropdown } = props;

  const logOut = () => {
    props.logout().then((response) => (window.location = '/'));
  };
  return (
    <div
      className="cr__header-MyAccount"
      onMouseLeave={() => {
        setFalseDropdown();
        setSectionDropdown('');
      }}
    >
      <div className="cr__header-MyAccount-information">
        <Link
          to={'/users/profile/'}
          className="photo"
          onClick={() => onGTMTracking('ProfilePhoto')}
        >
          <img
            src={user.profile?.profile_photo?.small}
            alt={`${user?.profile?.first_name} ${user?.profile?.last_name}`}
            loading="lazy"
          />
        </Link>
        <span className="cr__textColor--colorDark300 cr__text--paragraph">
          {user?.profile?.first_name} {user?.profile?.last_name}
        </span>
        {/* <p className='info_email'>{user.profile.email}</p> */}
      </div>
      <div className="cr__header-MyAccount-store" onClick={() => setFalseDropdown()}>
        {!user?.profile?.has_store ? (
          <Link
            className="cr__textColor--colorMain300 cr__text--paragraph"
            to={'/stores/create'}
            onClick={() => onGTMTracking('CreateStore')}
          >
            Abre una Tienda
          </Link>
        ) : (
          <Link
            className="cr__textColor--colorMain300 cr__text--paragraph"
            to={'/my-store/dashboard'}
            onClick={() => onGTMTracking('MyStore')}
          >
            Ir a mi Tienda
          </Link>
        )}
      </div>

      <div
        className="cr__header-MyAccount-link profile"
        onClick={() => {
          setFalseDropdown();
          onGTMTracking('MyProfile');
        }}
      >
        <Link
          className="cr__textColor--colorDark300 cr__text--paragraph"
          to="/users/profile/"
        >
          Ir a mi Perfil
        </Link>
      </div>
      <div
        className="cr__header-MyAccount-link"
        onClick={() => {
          setFalseDropdown();
          onGTMTracking('MyOrders');
        }}
      >
        <Link
          className="cr__textColor--colorDark300 cr__text--paragraph"
          to="/users/orders"
        >
          Mis pedidos
        </Link>
      </div>
      <div
        className="cr__header-MyAccount-link"
        onClick={() => {
          setFalseDropdown();
          onGTMTracking('MyReviews');
        }}
      >
        <Link
          className="cr__textColor--colorDark300 cr__text--paragraph"
          to="/users/reviews"
        >
          Reseñas
        </Link>
      </div>
      <div
        className="cr__header-MyAccount-link"
        onClick={() => {
          setFalseDropdown();
          onGTMTracking('SavedCards');
        }}
      >
        <Link
          className="cr__textColor--colorDark300 cr__text--paragraph"
          to="/users/cards"
        >
          Métodos de Pago
        </Link>
      </div>
      <div
        className="cr__header-MyAccount-link"
        onClick={() => {
          setFalseDropdown();
          onGTMTracking('help');
        }}
      >
        <a
          href="https://ayuda.canastarosa.com/"
          target="_blank"
          rel="noopener noreferrer"
          className="cr__textColor--colorDark300 cr__text--paragraph"
        >
          Ayuda
        </a>
      </div>
      <div
        className="cr__header-MyAccount-link logout"
        onClick={() => {
          logOut();
          setFalseDropdown();
          onGTMTracking('Logout');
        }}
      >
        <a className="cr__textColor--colorDark300 cr__text--paragraph">
          Cerrar sesión
        </a>
      </div>
    </div>
  );
});
