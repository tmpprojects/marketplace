import React, { useState } from 'react';
import hash from 'object-hash';
import AddressForm from '../../../UserProfile/AddressForm';
import Login from '../../../../Utils/Login';
import AddressManager from '../../../../Utils/AddressManager';
import AddressesList from '../../../../Utils/AddressesList';
import './modalBoxZipCode.scss';

export default function ModalBoxZipCode(props) {
  const [isAddingAddress, setIsAddingAddress] = useState(false);
  const {
    openModalBox,
    closeModalBox,
    addGuestAddress,
    deleteGuestAddress,
    getZipCodeGuest,
    openStatusWindow,
    deleteAddress,
    defaultAddressSearch = null,
    isLogged = false,
  } = props;

  const onAddAddress = (formValues) => {
    getZipCodeGuest(formValues.zip_code);
    const address = { ...formValues, uuid: hash(formValues) };
    deleteGuestAddress();
    addGuestAddress(address)
      .then((response) => {
        closeModalBox();
        openStatusWindow({
          type: 'success',
          message: 'La dirección se agrego correctamente.',
        });
      })
      .catch((error) => {
        openStatusWindow({
          type: 'error',
          message: 'Error. Por favor, intenta de nuevo.',
        });
      });
  };

  return (
    <div
      className="cr__header-modalBoxZipCode"
      role="dialog"
      aria-label="Cuadro de diálogo"
      aria-labelledby="zipCodeBox"
    >
      <div className="cr__header-modalBoxZipCode-title">
        <span className="cr__text--subtitle3 cr__textColor--colorDark300">
          Elige tu ubicación
        </span>
        <span
          onClick={() => closeModalBox()}
          className="cr__text--subtitle3 cr__textColor--colorDark300 cr__header-modalBoxZipCode-close"
          role="button"
          aria-label="Cerrar Ventana"
        >
          X
        </span>
      </div>
      {!isAddingAddress && (
        <div className="cr__header-modalBoxZipCode-message">
          <span className=" cr__text--caption cr__textColor--colorDark100">
            Para mostrarte los mejores resultados disponibles en tu localidad,
            <br />
            hacemos uso de tu código postal.
          </span>
        </div>
      )}

      {isAddingAddress && (
        <div>
          <AddressManager onSubmit={onAddAddress} />
        </div>
      )}

      {/*!isLogged && (
        <div className="cr__header-modalBoxZipCode-signIn">
          {!isLogged &&
            !isAddingAddress &&
            !defaultAddressSearch?.street_address && (
              <React.Fragment>
                <span className="cr__text--paragraph cr__textColor--colorDark300">
                  Inicia sesión o agrega una dirección para continuar.
                </span>
                <a
                  className="cr__text--paragraph cr__header-modalBoxZipCode-signIn-button"
                  onClick={() => openModalBox(Login)}
                  role="button"
                  aria-label="Iniciar sesión"
                >
                  Iniciar Sesión
                </a>
              </React.Fragment>
            )}
        </div>
      )*/}

      {!isAddingAddress && (
        <div className="cr__header-modalBoxZipCode-modalBox">
          {isLogged ? (
            <AddressForm />
          ) : (
            <div className="cr__header-modalBoxZipCode-newAddress">
              {defaultAddressSearch?.street_address ? (
                <AddressesList
                  address={defaultAddressSearch}
                  updateAddress={onAddAddress}
                  deleteAddress={deleteAddress}
                  isLogged={isLogged}
                />
              ) : (
                <a
                  className="cr__text--paragraph cr__header-modalBoxZipCode-newAddress-button"
                  onClick={() => setIsAddingAddress(true)}
                  role="button"
                  aria-label="Iniciar sesión"
                >
                  Agregar una nueva dirección
                </a>
              )}
            </div>
          )}
        </div>
      )}
    </div>
  );
}
