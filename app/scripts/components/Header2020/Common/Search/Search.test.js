import React from 'react';
import { Provider } from 'react-redux';
import renderer from 'react-test-renderer';
import configureStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import { render, shallow, mount } from 'enzyme';

import Search from './Search';

const middlewares = [thunk];

const mockStore = configureStore(middlewares);

describe('Search Component', () => {
  const store = mockStore({ myStore: 'someText' });

  const wrapper = render(<Search store={store} />);

  const wrapperShallow = shallow(<Search store={store} />);

  test('Search connected to store and render Component', () => {
    expect(wrapper).toBeDefined();
  });

  let wrapperMount;
  const onGTMTracking = jest.fn();
  const toSearch = jest.fn();
  const onUpdateWidth = jest.fn();
  const testSearch = jest.fn();
  const onChangeSearchTerm = jest.fn();

  beforeEach(() => {
    wrapperMount = mount(<Search store={store} />);
  });

  //   test('Enter', () => {
  //     expect(wrapperShallow.value.state('searchTerm')).toBe('');

  //     wrapperShallow
  //       .find('.cr__textColor--colorDark300')
  //       .simulate('change', { target: { value: 'mock' } });

  //     // expect(onChangeSearchTerm).toHaveBeenCalledTimes(0);

  //     expect(wrapperMount.state('searchTerm').searchTerm).toBe('mock');

  //     expect(wrapperShallow.state('searchTerm')).toEqual('pastel');
  //   });
  test('Should unmount correctly', () => {
    expect(wrapperMount.unmount()).toBeTruthy;
  });
});
