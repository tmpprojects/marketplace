import React, { PureComponent } from 'react';
import { connect } from 'react-redux';

import { trackWithGTM } from '../../../../Utils/trackingUtils';
import { SEARCH_SECTIONS, APP_SECTIONS } from '../../../../Constants';
import './search.scss';

class Search extends PureComponent {
  state = {
    searchTerm: '',
    width: 0,
  };

  componentDidMount() {
    this.onUpdateWidth();
    window.addEventListener('resize', this.onUpdateWidth);
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.onUpdateWidth);
  }

  onChangeSearchTerm = (e) => {
    e.preventDefault();
    const { value, name } = e.target;
    this.setState({
      [name]: value,
    });
    if (this.state.width <= 767) {
      this.props.onSetDefaultMenu();
    }
  };

  onUpdateWidth = () => {
    this.setState({ width: window.innerWidth });
  };

  onGTMTracking = (value) => {
    const data = {
      id: 'Search',
      name: `${value}${this.state.width >= 768 ? '-Web' : '-Mobile'}`,
      position: 1,
    };
    trackWithGTM('eec.impressionClick', [data], 'Search-');
  };

  /**
   * Before Process Search, check if searchTerm is a prohibited word otherwise send search to backend
   * testSearch()
   * @param {*} searchTerm
   * @param {*} section
   */
  testSearch = (searchTerm = '', section) => {
    const { prohibitedWords = {} } = this.props;

    const words = prohibitedWords?.list?.map((item) => item.toLowerCase());

    // if there are not words to compare call search
    if (!words.length >= 1) {
      this.toSearch(searchTerm, section);
    }

    const searchTermArray = searchTerm.toLowerCase().split(' ');

    if (searchTermArray.length === 1) {
      if (searchTermArray.some((word) => words.includes(word.toLowerCase()))) {
        this.props.history.push({ pathname: '/error/search', state: searchTerm });
        // Clear searchbox
        this.setState({
          searchTerm: '',
        });
      } else {
        const finalString = searchTermArray.join(' ');

        this.toSearch(finalString, section);
      }
    } else if (searchTermArray.length > 1) {
      const filteredWords = searchTermArray.filter(
        (word) => !words.includes(word.toLowerCase()),
      );

      const finalString = filteredWords.join(' ');

      this.toSearch(finalString, section);
    }
  };

  toSearch = (searchTerm = '', section) => {
    // Encode search term.
    // Double encoding is made on porpouse to force encoding the '%' sign.
    const encodedTerm = encodeURIComponent(encodeURIComponent(searchTerm)); //Dejado provisionalmente

    let searchPath;

    if (
      /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(searchTerm) ||
      searchTerm.includes('@') ||
      searchTerm.includes('/')
    ) {
      this.props.history.push({ pathname: '/error/search', state: searchTerm });
    } else if (!searchTerm == '') {
      if (!section) {
        if (this.props.appSection === APP_SECTIONS.INSPIRE) {
          searchPath = `/search/${SEARCH_SECTIONS.ARTICLES}/${encodedTerm}`;
        } else {
          //searchPath = `/search/products/${encodedTerm}`;
          searchPath = `/search/${encodedTerm}`;
        }
      } else {
        searchPath = `/search/${section}/${encodedTerm}`;
      }
      //Send event to GTM
      trackWithGTM('Search', searchTerm);
    }
    // Push searchPath to browser´s history to navigate.
    this.props.history.push(searchPath);

    // Clear searchbox
    this.setState({
      searchTerm: '',
    });
  };

  render() {
    return (
      <div className="cr__header-search">
        <form
          className="cr__header-search-form"
          onSubmit={(e) => {
            e.preventDefault();
            this.testSearch(this.state.searchTerm);
            this.onGTMTracking('onSearch');
          }}
          method="GET"
          acept-charset="utf-8"
          role="search"
          action="/search/"
        >
          <input
            className="cr__header-search-input cr__textColor--colorDark300 cr__text--paragraph"
            type="search"
            placeholder="¿Qué estás buscando?"
            value={this.state.searchTerm}
            onChange={this.onChangeSearchTerm}
            autoFocus
            autoComplete="off"
            autoCorrect="off"
            autoCapitalize="off"
            spellCheck="false"
            name="searchTerm"
            autoComplete="off"
            dir="ltr"
            maxLength="160"
            aria-label="¿Qué estás buscando?"
            aria-haspopup="false"
            onClick={() => this.onGTMTracking('Input')}
          />
          <input type="submit" value="Buscar" className="cr__inputSearchBtn" />
        </form>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    prohibitedWords: state.app?.prohibitedWords,
  };
}
export default connect(mapStateToProps, null)(Search);
