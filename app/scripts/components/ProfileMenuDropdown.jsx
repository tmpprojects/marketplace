import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';

import { ResponsiveImage } from '../Utils/ImageComponents.jsx';
import withDropDown from './hocs/withDropDown';

import './ProfileMenuDropdown.scss';

class ProfileMenuDropdown extends Component {
  static propTypes = {
    active: PropTypes.bool.isRequired,
  };
  static defaultProps = {
    active: true,
  };

  /*
   * Log Out
   * Logs out user
   */
  logOut = (e) => {
    e.preventDefault();

    // Logout and Redirect users to homepage
    this.props.logout().then((response) => (window.location = '/'));
  };

  render() {
    const { user } = this.props;
    return (
      <div className="menu-dropdown__container">
        <div className="all_info_name">
          <p className="info_name">
            {user.first_name} {user.last_name}
          </p>
          <p className="info_email">{user.email}</p>
        </div>
        <ul>
          <li className="userProfile" onClick={this.props.closeMenu}>
            <Link
              to={'/users/profile/'}
              className="photo"
              style={{
                width: '30px',
                position: 'relative',
                display: 'inline-block',
              }}
            >
              <ResponsiveImage
                src={user.profile_photo}
                alt={`${user.first_name} ${user.last_name}`}
              />
            </Link>

            <div className="info">
              {!user.has_store ? (
                <Link className="button-square--pink" to={'/stores/create'}>
                  Abre una Tienda
                </Link>
              ) : (
                <Link className="button-square--pink" to={'/my-store/dashboard'}>
                  Ir a mi Tienda
                </Link>
              )}
            </div>
          </li>

          <li onClick={this.props.closeMenu}>
            <Link to={'/users/profile/'}>Ir a mi Perfil</Link>
          </li>
          <li onClick={this.props.closeMenu}>
            <Link to={'/users/orders'}>Mis Compras</Link>
          </li>
          <li onClick={this.props.closeMenu}>
            <Link to={'/users/reviews'}>Reseñas</Link>
          </li>
          <li onClick={this.props.closeMenu}>
            <Link to="/users/cards" activeClassName="active">
              Métodos de Pago
            </Link>
          </li>
          <li onClick={this.props.closeMenu}>
            <a href="https://ayuda.canastarosa.com/" target="_blank">
              Ayuda
            </a>
          </li>
          <li className="logout" onClick={this.props.closeMenu}>
            <a onClick={this.logOut}>Cerrar sesión</a>
          </li>
        </ul>
      </div>
    );
  }
}
export default withDropDown(ProfileMenuDropdown);
