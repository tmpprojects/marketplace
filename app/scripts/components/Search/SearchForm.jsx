import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { withRouter } from 'react-router';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import { appActions } from '../../Actions';
import { SEARCH_SECTIONS, APP_SECTIONS } from '../../Constants';

class SearchForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      searchTerm: '',
    };
    this.onChangeSearchTerm = this.onChangeSearchTerm.bind(this);
    this.closeWindow = this.closeWindow.bind(this);
    this.handleClick = this.handleClick.bind(this);
    this.search = this.search.bind(this);
  }

  /**
   * onChangeSearchTerm()
   * This method controls the search input element
   * @param {object} e : Form Event
   */
  onChangeSearchTerm(e) {
    e.preventDefault();
    const { value, name } = e.target;
    this.setState({
      [name]: value,
    });
  }

  /**
   * handleClick()
   * Handles the click event over all the component,
   * actions are performed based on the clicked 'target'.
   * @param {object} e : ClickEvent Object
   */
  handleClick(e) {
    e.preventDefault();
    const target = e.target;

    // If the user clicked on the overlay, close window
    if (target.classList.contains('search-bar__overlay')) {
      this.closeWindow();
    }

    // If the user clicked on a search section
    if (target.classList.contains(SEARCH_SECTIONS.ARTICLES)) {
      this.search(this.state.searchTerm, SEARCH_SECTIONS.ARTICLES);
    }
    if (target.classList.contains(SEARCH_SECTIONS.PRODUCTS)) {
      this.search(this.state.searchTerm, SEARCH_SECTIONS.PRODUCTS);
    }
    if (target.classList.contains(SEARCH_SECTIONS.STORES)) {
      this.search(this.state.searchTerm, SEARCH_SECTIONS.STORES);
    }
  }

  /**
   * search()
   * Determine the search parameters and redirect users to Search Results Page
   * @param {string} searchTerm : String to search
   * @param {string} section : String that indicates where we should make the search
   */
  search(searchTerm, section) {
    let searchPath;
    if (!section) {
      if (this.props.appSection === APP_SECTIONS.INSPIRE) {
        searchPath = `/search/${SEARCH_SECTIONS.ARTICLES}/${searchTerm}`;
      } else {
        searchPath = `/search/${searchTerm}`;
      }
    } else {
      searchPath = `/search/${section}/${searchTerm}`;
    }

    this.props.history.push(searchPath);
    this.closeWindow();
  }

  /**
   * closeWindow()
   * Closes the search modal window
   */
  closeWindow() {
    this.props.closeSearchBar();
  }

  /**
   * render()
   * Render React Component
   */
  render() {
    const searchTerm =
      this.state.searchTerm !== '' ? `'${this.state.searchTerm}'` : '';
    return (
      <div className="search-bar" onClick={this.handleClick}>
        <div className="search-bar__overlay" />

        <div className="search-bar__form-container">
          <form
            className="form"
            onSubmit={(e) => {
              e.preventDefault();
              this.search(this.state.searchTerm);
            }}
          >
            <input
              autoFocus
              type="text"
              placeholder="Busca en Canasta Rosa"
              className="form__field"
              name="searchTerm"
              value={this.state.searchTerm}
              onChange={this.onChangeSearchTerm}
              autoComplete="off"
            />
            <a href="#" className="form__button">
              Buscar
            </a>
          </form>
        </div>

        <div
          className="dropdown"
          style={{
            display: searchTerm.length > 3 ? 'block' : 'none',
          }}
        >
          <ul className="dropdown__list">
            <li className="dropdown__item">
              <Link className={SEARCH_SECTIONS.STORES} to="#">
                Buscar <span className="search-term">{searchTerm}</span> en tiendas
              </Link>
            </li>
            <li className="dropdown__item">
              <Link className={SEARCH_SECTIONS.PRODUCTS} to="#">
                Buscar&nbsp;
                <span className="search-term">{searchTerm}</span>&nbsp; en productos
              </Link>
            </li>
            <li className="dropdown__item">
              <Link className={SEARCH_SECTIONS.ARTICLES} to="#">
                Buscar&nbsp;
                <span className="search-term">{searchTerm}</span>&nbsp; en artículos
              </Link>
            </li>
          </ul>
        </div>
      </div>
    );
  }
}

// Redux Map Functions
function mapStateToProps({ app }) {
  return {
    appSection: app.section,
  };
}
function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      closeSearchBar: appActions.closeSearchBar,
    },
    dispatch,
  );
}

// Export Connected Component
SearchForm = withRouter(SearchForm);
SearchForm = connect(mapStateToProps, mapDispatchToProps)(SearchForm);
export default SearchForm;
