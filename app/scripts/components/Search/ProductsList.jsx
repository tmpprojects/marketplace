import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { ResponsiveImageFromURL } from '../../Utils/ImageComponents';
import { formatNumberToPrice } from '../../Utils/normalizePrice';
import { analyticsActions } from '../../Actions';

export class ProductsList extends Component {
  dispatchDataLayerClick(id) {
    console.log("dispatchDataLayerClick productsList", id);
    globalThis.googleAnalytics.productClick(id);

  };
  render() {
    const { items } = this.props;


    return (
      <ul className="products__list wrapper--center" >
        {
          items.map((product) => (
            <li className="product" key={product.slug}>
              <div className="product_container">

                <div className="thumbnail">
                  <Link
                    to={`/stores/${product.store.slug}/products/${product.slug}`}
                    className="thumbnail__image"
                    onClick={() => {
                     // console.log("product********** 1",product)
                      this.dispatchDataLayerClick(product.id)
                    }}
                  >
                    {product.photo && (
                      <ResponsiveImageFromURL
                        src={product.photo.small}
                        alt={product.name}
                      />
                    )}
                  </Link>

                  {/* DISCOUNT TAG */}
                  {parseFloat(product.discount, 10) > 0 && (
                    <div
                      style={{
                        position: 'absolute',
                        bottom: '.5em',
                        right: '.5em',
                        padding: '0.1em .3em',
                        background: '#1eb592',
                        color: '#ffffff',
                      }}
                    >
                      <span className="cr__text--caption cr__textColor--colorWhite">
                        {`-${parseInt(product.discount)}%`}
                      </span>
                    </div>
                  )}
                  {/* /DISCOUNT TAG */}
                </div>
                <div className="product__info">
                  <Link
                    className="product__name"
                    to={`/stores/${product.store.slug}/products/${product.slug}`}
                    onClick={() => {
                     // console.log("product********* 2",product)
                      this.dispatchDataLayerClick(product);
                    }}
                  >
                    {product.name}
                  </Link>
                  <div className="product__info-detail">

                    <Link
                      className="store product__store-link"
                      to={`/stores/${product.store.slug}`}
                    >
                      {product.store.name}
                    </Link>

                    {/* STIKE THROUGH PRICE */}
                    {parseFloat(product.discount, 10) > 0 && (
                      <div
                        style={{ color: '#D87041' }}
                        className="product__price line-through"
                      >
                        Antes
                        <span style={{ textDecoration: 'line-through' }}>
                          {' '}
                          {formatNumberToPrice(product.price_without_discount)}MX
                      </span>
                      </div>
                    )}
                    {/* /STIKE THROUGH PRICE */}

                    <p className="product__shippingMethod cr__text--caption cr__textColor--colorViolet400">
                      {product?.physical_properties?.shipping_methods?.find(
                        (i) => i === 'express-moto' || i === 'express-car',
                      ) && 'Envío express'}
                      {!product?.physical_properties?.shipping_methods?.find(
                        (i) => i === 'express-moto' || i === 'express-car',
                      ) &&
                        product?.physical_properties?.shipping_methods?.find(
                          (i) => i === 'standard',
                        ) &&
                        'Envío Nacional'}
                    </p>

                    <span>
                      {parseFloat(product.discount, 10) > 0
                        ? formatNumberToPrice(product.price)
                        : formatNumberToPrice(product.price_without_discount)}
                    MX
                  </span>
                    {/* <span className="shipping_date"> Recíbelo el {Moment(product.physical_properties.minimum_shipping_date, 'YYYY-MM-DD', true).format('D MMMM')}</span> */}
                  </div>
                  <Link to="" className="product__rating">
                    <img
                      src={require('images/store/ranking.png')}
                      className="ranking"
                      alt=""
                    />
                  </Link>
                </div>
              </div>
            </li>
          ))
        }
      </ul>
    );
  }
}

// Map Redux Props and Actions to component
function mapStateToProps() {
  return {};
}
function mapDispatchToProps(dispatch) {
  const { trackProductClick } = analyticsActions;

  return bindActionCreators(
    {
      trackProductClick,
    },
    dispatch,
  );
}

// Export Component
export default connect(mapStateToProps, mapDispatchToProps)(ProductsList);
