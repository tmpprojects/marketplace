import React from 'react';
import { Link } from 'react-router-dom';

import { ResponsiveImage } from '../../Utils/ImageComponents';

export default ({ items }) =>
  items.map((article) => (
    <li className="article" key={article.slug}>
      <div className="article_container">
        <div className="thumbnail">
          <Link to={`/inspire/article/${article.slug}`} className="thumbnail__image">
            <ResponsiveImage src={article.cover_photo} alt="" />
          </Link>
        </div>
        <div className="article__info">
          <Link to={`/inspire/article/${article.slug}`} className="article_name">
            {article.title}
          </Link>
        </div>
      </div>
    </li>
  ));
