import React from 'react';
import { Link } from 'react-router-dom';
import { trackWithGTM } from '../../Utils/trackingUtils';
// import { IconPreloader } from '../../../Utils/Preloaders';
import Placeholder from '@canastarosa/ds-theme/assets/images/placeholder/placeholder--productPage.jpg';

const errorImage = (e) => {
  e.target.src = Placeholder;
};

export default class StoresList extends React.Component {
  render() {
    const { location } = this.props;
    return (
      <div className="cr__stores">
        <div className="cr__stores-list">
          {this.props.items.map(({ name, slug, photo, cover, slogan }, index) => {
            const store = {
              id: slug,
              name,
              position: index,
            };
            const onClickGTMTracking = () => {
              trackWithGTM(
                'eec.impressionClick',
                [store],
                `Directorio de Tiendas-${location}`,
              );
            };
            return (
              <div className="cr__stores-item" key={index}>
                <Link to={`/stores/${slug}/`} onClick={onClickGTMTracking}>
                  <div className="cr__stores-item-a">
                    {/* {loaded ? null : (
                              <IconPreloader style={{ marginBottom: '35px' }} />
                            )} */}
                    <img
                      // style={loaded ? {} : { display: 'none' }}
                      src={cover.small}
                      alt={name}
                      aria-label={name}
                      // onLoad={() => setLoaded(true)}
                      onError={errorImage}
                    />
                  </div>
                  <div className="cr__stores-item-b">
                    <div className="cr__container-logo">
                      {/* {loaded ? null : (
                                <img src={Placeholder} alt='Cargando...' />
                              )} */}
                      <img
                        // style={loaded ? {} : { display: 'none' }}
                        src={photo.small}
                        alt={name}
                        aria-label={name}
                        // onLoad={() => setLoaded(true)}
                        onError={errorImage}
                      />
                    </div>
                    <div className="cr__container-name">
                      <h3 className="cr__container-name-title cr__text--subtitle3 cr__textColor--colorDark300">
                        {name.length >= 24 ? `${name.substring(0, 24)}...` : name}
                      </h3>
                      <div className="cr__text--paragraph cr__textColor--colorDark100">
                        <p>
                          {slogan.length >= 75
                            ? `${slogan.substring(0, 75)}...`
                            : slogan}
                        </p>
                      </div>
                    </div>
                  </div>
                </Link>
              </div>
            );
          })}
        </div>
      </div>
    );
  }
}
