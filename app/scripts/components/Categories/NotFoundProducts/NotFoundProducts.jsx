import React, { Component } from 'react';
import { Link } from 'react-router-dom';

import './notFoundProducts.scss';

export class NotFoundProducts extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <section className="notFoundProducts">
        <div className="notFoundProducts-wrapper">
          <div className="notFoundProducts-generalContainer">
            <div className="notFoundProducts-textContainer">
              <div className="notFoundProducts-textContainer-wrapper">
                <h3 className="cr__text-subtitle cr__textColor--colorDark300">
                  Lo sentimos parece ser que no encontramos productos aquí.
                </h3>
                <div className="notFoundProducts-paragraphContainer">
                  <div>
                    <p className="cr__text--paragraph cr__textColor--colorDark300 ">
                      No pudimos encontrar productos con el criterio elegido.
                    </p>
                  </div>
                  <div>
                    <p className="cr__text--paragraph cr__textColor--colorDark300 ">
                      ¡No te preocupes!
                    </p>
                  </div>

                  <div>
                    <p className="cr__text--paragraph cr__textColor--colorDark300">
                      Explora tus productos mediante el <span>menú principal.</span>
                    </p>
                  </div>
                </div>

                <div className="link-container">
                  <p>
                    <Link
                      className="cr__text--paragraph button-simple button-simple--pink link"
                      to={'/'}
                    >
                      Regresar al inicio
                    </Link>
                  </p>
                </div>
              </div>
            </div>
            <div className="notFoundProducts-imageContainer">
              <div className="img_container horizontal">
                <img
                  src={require('./../../../../images/illustration/out_of_stock.svg')}
                />
              </div>
            </div>
          </div>
        </div>
      </section>
    );
  }
}

// Export Component
export default NotFoundProducts;
