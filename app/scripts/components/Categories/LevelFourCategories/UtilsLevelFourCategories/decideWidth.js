/**
 * decideWidth
 * @param {booelan} isMobile| Is Mobile?
 * @param {booelan} isTablet| Is Tablet?
 * @param {number} categoriesLength| Length of Category
 * @returns {obj} {width: }
 */
export const decideWidth = (
  isMobile = false,
  isTablet = false,
  categoriesLength = 2,
) => {
  //   console.log(
  //     'isMobile',
  //     isMobile,
  //     'isTablet',
  //     isTablet,
  //     'categ Length',
  //     categoriesLength,
  //   );

  if (isTablet && categoriesLength === 3) {
    return { width: '70%' };
  }
  if (isMobile && categoriesLength > 2) {
    return { width: '100%' };
  }
  if (isTablet && categoriesLength < 2) {
    return { width: '40%' };
  }
  if (isTablet && categoriesLength == 2) {
    return { width: '40%' };
  }
  if (isMobile && categoriesLength == 2) {
    return { width: '100%' };
  }

  if (!isMobile && categoriesLength > 4) {
    return { width: '87%' };
  }

  if (!isMobile && categoriesLength === 4) {
    return { width: '60%' };
  }
  if (!isMobile && categoriesLength === 3) {
    return { width: '50%' };
  }
  if (!isMobile && categoriesLength === 2) {
    return { width: '30%' };
  } else {
    return { width: '100%' };
  }
};
