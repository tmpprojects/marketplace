import React from 'react';

/**
 * BackImage
 * @param {obj} | Recieves a category object
 * @returns {jsx} JSX Component
 */
export const BackImage = ({ photo }) => {
  return (
    <div
      style={{
        background: `url(${
          photo.medium
            ? photo.medium
            : require('../../../../../images/placeholder/placeholder--productPage.jpg')
        }) 0% /cover`,
        top: '0',
        left: '0',
        bottom: '0',
        right: '0',
        position: 'absolute',
        zIndex: '-1',
        borderRadius: '5px',
      }}
    ></div>
  );
};

export const BackDarkCourtain = ({ photo }) => {
  return (
    <div
      style={{
        backgroundColor: '#37373E',
        opacity: '0.5',
        top: '0',
        left: '0',
        bottom: '0',
        right: '0',
        position: 'absolute',
        zIndex: '-1',
        borderRadius: '5px',
      }}
    ></div>
  );
};
