import React from 'react';
import { Link } from 'react-router-dom';

import './levelFourCategories.scss';

export const LevelFourCategories = (props) => {
  const { categories } = props;

  return (
    <div className="third-level-categ">
      {categories.length > 0 ? (
        <div className="third-level-categ-container" id="third-level">
          {categories.map(({ name, slug, photo }) => (
            <Link to={`/category/${slug}/`} key={slug}>
              <div className="third-level-categ-item" key={slug}>
                <div className="third-level-categ-item--name">{name}</div>
              </div>
            </Link>
          ))}
        </div>
      ) : null}
    </div>
  );
};
