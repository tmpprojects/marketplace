import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import {
  Field,
  FieldArray,
  reduxForm,
  getFormValues,
  SubmissionError,
} from 'redux-form';
import Moment from 'dayjs';
import Calendar from 'react-calendar';

import { appActions } from '../../Actions';
import '../../../styles/_categorySearchResults.scss';
import {
  SIDEBAR_FILTERS_FORM_CONFIG,
  DEFAULT_FILTERS,
} from '../../Utils/SideBarFilters/SideBarFiltersConfig';
import { isValidWorkingDay } from '../../Utils/dateUtils';

/*------------------------------------------------------
//             SHIPPING CALENDAR COMPONENT
------------------------------------------------------*/
const Calendario = (field) => (
  <Calendar
    locale="es"
    view="month"
    value={field.date}
    minDate={field.minDate}
    maxDate={field.maxDate}
    className="calendar-component"
    onChange={(selectedDate) => {
      //Transform date
      const selectedDateUTC = Moment(selectedDate).format('YYYY-MM-DD');
      //Update value of field
      field.input.onChange(selectedDateUTC);
    }}
    tileDisabled={({ date }) => {
      !isValidWorkingDay(date, {
        work_schedules: field.work_schedules,
        physical_properties: {
          maximum_shipping_date: field.maxDate,
          minimum_shipping_date: field.minDate,
        },
      });
    }}
    tileClassName={({ date }) => {
      const className = 'calendar-component__tile';
      let tileType = !isValidWorkingDay(date, {
        work_schedules: field.work_schedules,
        physical_properties: {
          maximum_shipping_date: field.maxDate,
          minimum_shipping_date: field.minDate,
        },
      })
        ? 'calendar-component__tile--disabled '
        : '';
      tileType =
        date.getTime() === field.date.getTime()
          ? `${tileType} calendar-component__tile--active`
          : tileType;
      tileType =
        date.getDay() === 6 || date.getDay() === 0
          ? `${tileType} calendar-component__tile--weekend`
          : tileType;
      return `${className} ${tileType}`;
    }}
  />
);

/*------------------------------------------------------
//                     STORES LIST
------------------------------------------------------*/
const StoresList = ({ fields }) => (
  <React.Fragment>
    {fields.map((store, index) => (
      <li key={`${store}.slug`}>
        <Field
          id={`${store}.slug`} //works with htmlFor
          className="checkmark"
          name={`${store}.selected`} //key that was added for knowing if its selected by the user
          component="input"
          type="checkbox"
          defaultChecked={fields.get(index).selected}
        />
        <label htmlFor={`${store}.slug`} className="checkmark">
          {fields.get(index).name}
        </label>
      </li>
    ))}
  </React.Fragment>
);

/*------------------------------------------------------
//             SideBarFilters COMPONENT
------------------------------------------------------*/
export class SideBarFilters extends Component {
  constructor(props) {
    super(props);
    this.state = {
      checked: false,
      shouldRenderCalendar: false,
      shouldRenderDateDisplay: props.initialValues.delivery_day === 'picked',
      showAllStores: true,
    };
  }

  componentDidMount() {
    this.props.getShippingZones();
  }
  /**
   * Filter Submit Handler
   */
  onSubmit = () => {
    // Mock delay function
    const sleep = (ms) => new Promise((resolve) => resolve()); //new Promise(resolve => setTimeout(resolve, ms));

    //
    const { formValues } = this.props;
    if (formValues !== undefined) {
      // Validation
      //return sleep(500) // simulate server latency
      //.then(() => {
      if (formValues.delivery_day === 'picked' && !formValues.delivery_day_picked) {
        throw new SubmissionError({
          _error: '*Selecciona una fecha en el calendario*',
        });
      } else {
        let filtersQuery = `${this.filterPrice(formValues)}`;
        filtersQuery += `${this.filterShipping(formValues)}`;
        filtersQuery += `${this.filterDelivery(formValues)}`;
        filtersQuery += `${this.filterStores(formValues)}`;

        this.props.close(false);
        this.props.onFilterSubmit(filtersQuery);
        // This close the SideBarFilters when its mobile
      }
      //});
    }
  };

  /**
   * onDeliveryDateChange()
   * @param {object} date | Date passed from Calendar component
   */
  onDeliveryDateChange = (date) => {
    // Update redux form manually
    this.props.change('delivery_day', 'picked');
    // Close calendar view
    this.toggleCalendar(false, true);
  };

  /**
   * onDeliveryDateOptionChange()
   */
  onDeliveryDateOptionChange = (option) => {
    const { formValues } = this.props;
    switch (option.target.value) {
      case 'tomorrow': {
        this.toggleCalendar(false, true);
        this.props.change(
          'delivery_day_picked',
          Moment().add(1, 'days').format('YYYY-MM-DD'),
        );
        break;
      }
      case 'picked': {
        this.toggleCalendar(true, true);
        const date = formValues.delivery_day_picked || Moment().format('YYYY-MM-DD');
        this.props.change('delivery_day_picked', date);
        break;
      }
      case 'today': {
        this.toggleCalendar(false, true);
        this.props.change('delivery_day_picked', Moment().format('YYYY-MM-DD'));
        break;
      }
      case 'all':
      default: {
        this.toggleCalendar(false, false);
        this.props.change('delivery_day_picked', null);
        break;
      }
    }
  };

  /**
   * resetForm()
   * Update form 'initialValues' with default values.
   * Resubmit query to refresh search results
   */
  resetForm = () => {
    // Re-initialize (assigning default values) filters form
    this.props.initialize({
      ...DEFAULT_FILTERS,
      stores: this.props.stores.stores,
    });

    // Send an empty queryString to reload results
    this.props.onFilterSubmit('');
    // This close the SideBarFilters when its mobile
    this.props.close(false);
  };

  /**
   * filterStores()
   * Constructs a queryString portion based on 'selected' stores.
   * @param {object} formValues || Form values
   * @returns {string} String of stores list
   */
  filterStores = (formValues) => {
    const stores = formValues.stores;
    return stores.reduce((qs, store) => {
      let queryString = qs;

      // Construct query string if store checkbox is 'selected'
      if (store.selected && queryString === '') {
        queryString += `&stores=${store.slug}`;
      } else if (store.selected) {
        queryString += `+${store.slug}`;
      }
      return queryString;
    }, '');
  };

  /**
   * filterDelivery()
   * Constructs a queryString portion based on delivery date availability.
   * @param {object} formValues || Form values
   * @returns {string} Delivery date queryString
   */
  filterDelivery = (formValues) => {
    const deliveryDay = formValues.delivery_day;
    const pickedDay = formValues.delivery_day_picked;

    // Determine delivery date
    switch (deliveryDay) {
      case 'today':
        return `&delivery_date=${Moment().format('YYYY-MM-DD')}`;
      case 'tomorrow':
        return `&delivery_date=${Moment().add(1, 'days').format('YYYY-MM-DD')}`;
      case 'picked':
        if (pickedDay) return `&delivery_date=${pickedDay}`;
        return '';
      default:
        return '';
    }
  };

  /**
   * filterPrice()
   * Constructs a queryString portion based on price range.
   * @param {object} formValues || Form values
   * @returns {string} Price range queryString
   */
  filterPrice = (formValues) => {
    switch (formValues.price) {
      case 'max_price_100':
        return '&max_price=100';
      case 'max_price_200':
        return '&min_price=100&max_price=200';
      case 'max_price_300':
        return '&min_price=200&max_price=300';
      case 'max_price_400':
        return '&min_price=300&max_price=400';
      case 'max_price_500':
        return '&min_price=400&max_price=500';
      case 'max_price_':
        return '&min_price=500&max_price=';
      default:
        return '';
    }
  };

  /**
   * filterShipping()
   * Constructs a queryString portion based on shipping method.
   * @param {object} formValues || Form values
   * @returns {string} Shipping Method queryString
   */
  filterShipping = (formValues) => {
    switch (formValues.shipping) {
      case 'cdmx':
        return '&zone=area-metropolitana';
      case 'monterrey':
        return '&zone=monterrey';
      case 'guadalajara':
        return '&zone=guadalajara';
      case 'national':
        return '&zone=national';
      case 'all':
      default:
        return '';
    }
  };

  /**
   * toggleCalendar()
   * Toogle calendar component visibility (use 'flag' param to enforce view state)
   * @param {bool} showCalendar | Optional view state
   * @param {bool} showDateDisplay | Optional view state
   */
  toggleCalendar = (showCalendar, showDateDisplay) => {
    const shouldRenderCalendar =
      showCalendar !== undefined ? showCalendar : !this.state.shouldRenderCalendar;
    let shouldRenderDateDisplay = shouldRenderCalendar;
    if (showDateDisplay !== undefined) {
      shouldRenderDateDisplay = showDateDisplay;
    }

    this.setState({
      shouldRenderCalendar,
      shouldRenderDateDisplay,
    });
  };

  /**
   * showStores()
   * Update component view to collapse/open stores list.
   * @param {object} e || Mouse Event
   */
  showStores = (e) => {
    e.preventDefault();
    if (e !== undefined) {
      this.setState({
        showAllStores: !this.state.showAllStores,
      });
    }
  };

  /**
   * render()
   */
  render() {
    const {
      stores,
      close,
      isMobile,
      location,
      handleSubmit,
      formValues,
      error,
    } = this.props;

    const {
      shouldRenderCalendar,
      showAllStores,
      shouldRenderDateDisplay,
    } = this.state;

    // Render Markup
    return (
      <div className="storesMenu sidebar_filters form">
        <h5 className="title_header">Filtrar por:</h5>

        <form
          className="sidebar_filters--container"
          onSubmit={handleSubmit(this.onSubmit)}
        >
          <div className="filter form__data">
            <p className="filter-title">Día de entrega</p>
            <Field
              type="radio"
              id="delivery_all"
              name="delivery_day"
              component="input"
              value="all"
              className="dots"
              onChange={this.onDeliveryDateOptionChange}
            />
            <label className="dot" htmlFor="delivery_all">
              Cualquier día
            </label>

            <Field
              type="radio"
              id="today"
              name="delivery_day"
              component="input"
              value="today"
              className="dots"
              onChange={this.onDeliveryDateOptionChange}
            />
            <label className="dot" htmlFor="today">
              Recíbelo Hoy
            </label>

            <Field
              type="radio"
              id="tomorrow"
              name="delivery_day"
              component="input"
              value="tomorrow"
              className="dots"
              onChange={this.onDeliveryDateOptionChange}
            />
            <label className="dot" htmlFor="tomorrow">
              Recíbelo Mañana
            </label>

            <Field
              type="radio"
              id="picked"
              name="delivery_day"
              component="input"
              value="picked"
              className="dots"
              onChange={(value) => {
                this.toggleCalendar(true, true);
                this.onDeliveryDateOptionChange(value);
              }}
            />
            <label className="dot" htmlFor="picked">
              Elige una fecha
            </label>

            <div className="ui-calendar-selector">
              {shouldRenderDateDisplay && (
                <div
                  className="ui-calendar-selector__day"
                  onClick={() => {
                    this.toggleCalendar(!this.state.shouldRenderCalendar, true);
                  }}
                >
                  {' '}
                  {formValues && formValues.delivery_day_picked !== null
                    ? formValues.delivery_day_picked
                    : 'Sin seleccionar'}
                </div>
              )}

              {shouldRenderCalendar && (
                <Field
                  component={Calendario}
                  minDate={Moment(new Date(), 'YYYY-MM-DD', true).toDate()} //Format for Calendar to function
                  maxDate={Moment(new Date(), 'YYYY-MM-DD', true)
                    .add(6, 'y')
                    .toDate()}
                  name="delivery_day_picked"
                  onChange={this.onDeliveryDateChange}
                  date={Moment(
                    formValues.delivery_day_picked || new Date(),
                    'YYYY-MM-DD',
                    true,
                  ).toDate()}
                  work_schedules={[
                    {
                      week_day: { display_name: 'Monday', value: 0 },
                      start: '09:00:00',
                      end: '17:00:00',
                    },
                    {
                      week_day: { display_name: 'Tuesday', value: 1 },
                      start: '09:00:00',
                      end: '17:00:00',
                    },
                    {
                      week_day: { display_name: 'Wednesday', value: 2 },
                      start: '09:00:00',
                      end: '17:00:00',
                    },
                    {
                      week_day: { display_name: 'Thursday', value: 3 },
                      start: '09:00:00',
                      end: '17:00:00',
                    },
                    {
                      week_day: { display_name: 'Friday', value: 4 },
                      start: '09:00:00',
                      end: '17:00:00',
                    },
                    {
                      week_day: { display_name: 'Saturday', value: 5 },
                      start: '09:00:00',
                      end: '17:00:00',
                    },
                    {
                      week_day: { display_name: 'Sunday', value: 6 },
                      start: '09:00:00',
                      end: '17:00:00',
                    },
                  ]}
                />
              )}
            </div>
          </div>

          {/* <div className="filter form__data">
            <p className="filter-title">Lugar de entrega</p>
            <Field
              type="radio"
              id="shipping_all"
              name="shipping"
              component="input"
              value="shipping_all"
              className="dots"
            />
            <label className="dot" htmlFor="shipping_all">
              Todos
            </label>

            <Field
              type="radio"
              id="national"
              name="shipping"
              component="input"
              value="national"
              className="dots"
            />
            <label className="dot" htmlFor="national">
              Envío Nacional
            </label>

            <Field
              type="radio"
              id="cdmx"
              name="shipping"
              component="input"
              value="area-metropolitana"
              className="dots"
            />
            <label className="dot" htmlFor="cdmx">
              CDMX
            </label>

            <Field
              type="radio"
              id="monterrey"
              name="shipping"
              component="input"
              value="monterrey"
              className="dots"
            />
            <label className="dot" htmlFor="monterrey">
              Monterrey
            </label>

            <Field
              type="radio"
              id="guadalajara"
              name="shipping"
              component="input"
              value="guadalajara"
              className="dots"
            />
            <label className="dot" htmlFor="guadalajara">
              Guadalajara
            </label>
          </div> */}

          {/*
                    <div className="filter form__data">
                        <p className="filter-title">Para él/Para ella</p>
                        <Field
                            type="checkbox"
                            id="gender_he"
                            name="gender_he"
                            component="input"
                        />
                        <label className="checkmark" htmlFor="gender_he" >Hombre</label>
                        <Field
                            type="checkbox"
                            id="gender_she"
                            name="gender_she"
                            component="input"
                        />
                        <label className="checkmark" h3tmlFor="gender_she" >Mujer</label>
                    </div>
                    */}

          <div className="filter">
            <label className="filter-title" htmlFor="price">
              Rango de Precio
            </label>
            <Field className="price" name="price" component="select">
              <option value="">Elige una opción</option>
              <option value="max_price_100">Debajo de $100</option>
              <option value="max_price_200">$100 a $200</option>
              <option value="max_price_300">$200 a $300</option>
              <option value="max_price_400">$300 a $400</option>
              <option value="max_price_500">$400 a $500</option>
              <option value="max_price_more">$500 o más</option>
            </Field>
          </div>

          {location.pathname.includes('category') ||
          location.pathname.includes('interest') ? (
            <div
              className={`filter stores-directory ${
                showAllStores ? 'see-less' : 'see-more'
              }`}
            >
              <p className="filter-title">
                Tiendas <span>({stores.stores.length || 0})</span>
                {/*<button onClick={this.showStores} className="btn-show_stores">
                                {showAllStores ? 'Ver menos' : 'Ver más'}
                            </button>*/}
              </p>
              <ul className="store-list">
                {stores.loading ? (
                  <li className="store">Cargando Tiendas</li>
                ) : (
                  <FieldArray name="stores" component={StoresList} />
                )}
              </ul>
            </div>
          ) : null}
          <div className="button-container">
            {isMobile && (
              <button className="icon-close" onClick={() => close(false)}>
                Cerrar
              </button>
            )}
            {/* Error in case delivery date is empty, only if they select "Pick day" */}
            <div className="error">{error}</div>
            <button
              type="button"
              className="c2a_border reset"
              onClick={this.resetForm}
            >
              Borrar Filtros
            </button>
            <button type="submit" className="c2a_border">
              Aplicar Filtros
            </button>
          </div>
        </form>
      </div>
    );
  }
}

SideBarFilters = reduxForm({
  ...SIDEBAR_FILTERS_FORM_CONFIG.config,
})(SideBarFilters);

// Map Redux Props and Actions to component
function mapStateToProps(state, ownProps) {
  const defaultFilters = {
    ...DEFAULT_FILTERS,
    stores: state.app.storesList.results,
  };
  let initialValues = defaultFilters;
  const definedValues = Object.keys(ownProps.urlValues).reduce((b, a) => {
    // overwrite initial values
    if (initialValues[a] !== ownProps.urlValues[a]) {
      if (a === 'max_price') {
        return { ...b, price: `max_price_${ownProps.urlValues[a]}` };
      }
      return { ...b, [a]: ownProps.urlValues[a] };
    }
    return b;
  }, {});

  //
  initialValues = {
    ...initialValues,
    ...definedValues,
  };
  // if (ownProps.urlValues) {
  //     initialValues = {
  //         ...defaultFilters,
  //         ...ownProps.urlValues
  //     };
  // }

  //
  return {
    formValues: getFormValues(SIDEBAR_FILTERS_FORM_CONFIG.formName)(state),
    initialValues,
  };
}

function mapDispatchToProps(dispatch) {
  const { getShippingZones } = appActions;
  return bindActionCreators(
    {
      getShippingZones,
    },
    dispatch,
  );
}

// Export Component
SideBarFilters = connect(mapStateToProps, mapDispatchToProps)(SideBarFilters);

SideBarFilters.propTypes = {
  urlValues: PropTypes.object,
  onFilterSubmit: PropTypes.func.isRequired,
  stores: PropTypes.object,
  isMobile: PropTypes.bool,
  close: PropTypes.func,
  location: PropTypes.object.isRequired,
};
SideBarFilters.defaultProps = {
  initialValues: {},
  urlValues: {},
  stores: {},
  isMobile: false,
  close: () => false,
};

export default SideBarFilters;
