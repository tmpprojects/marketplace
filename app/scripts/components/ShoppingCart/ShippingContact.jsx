import Sentry from '../../Utils/Sentry';
import React, { Component } from 'react';
import { Field } from 'redux-form';
import PropTypes from 'prop-types';

import withCounter from '../hocs/withCounter';
import { InputField, TextArea } from '../../Utils/forms/formComponents';
import { IconPreloader } from '../../Utils/Preloaders';
import { normalizePhone } from '../../Utils/forms/formNormalizers';

let sendStep1 = false;

// FORM FIELDS FORMATTING
const TextAreaWithCounter = withCounter(TextArea);

/*---------------------------------------------
        ShippingContact Component
---------------------------------------------*/
class ShippingContact extends Component {
  static propTypes = {
    renderFormView: PropTypes.bool,
  };
  static defaultProps = {
    renderFormView: false,
  };
  constructor(props) {
    super(props);
    this.viewTrigger = 'props'; // options are 'props' || 'user'
    this.state = {
      shouldRenderFormView: this.props.renderFormView,
    };
  }
  componentWillReceiveProps(nextProps) {
    if (
      this.props.renderFormView !== nextProps.renderFormView &&
      this.viewTrigger === 'props'
    ) {
      this.viewTrigger = 'props';
      this.setState({
        shouldRenderFormView: nextProps.renderFormView,
      });
    }
  }

  /**
   * onSubmit()
   * Submit information to backend API
   */
  onSubmit = () => {
    this.closeFormView();
  };

  /**
   * openFormView()
   * Changes component state to show 'contact form view'
   */
  openFormView = () => {
    this.viewTrigger = 'user';
    this.setState({
      shouldRenderFormView: true,
    });
  };

  /**
   * closeFormView()
   * Changes component state to hide 'contact form view'
   */
  closeFormView = () => {
    if (this.verifyData(this.props.clientData)) {
      this.viewTrigger = 'props';
      this.setState({
        shouldRenderFormView: false,
      });
    } else {
      //console.log('falta info close button');
    }
  };

  /**
   * verifyData()
   * Returns a bool indicating if all required data is filled up
   * @param {obj} clientData | Client data object.
   * @returns {bool}
   */
  verifyData = (clientData) => {
    // Verify that clientData is complete.
    // Otherwise, render form.
    let isValid = true;
    if (
      !clientData ||
      !clientData.client_name ||
      !clientData.client_lastName ||
      !clientData.phone
    ) {
      isValid = false;
    }

    return isValid;
  };

  /**
   * onFieldBlur()
   */
  onFieldBlur = (a) => {
    this.viewTrigger = 'props';
  };

  /**
   * onFieldFocus()
   */
  onFieldFocus = (a) => {
    this.viewTrigger = 'user';
  };

  /**
   * updateContactInfo()
   * Update contact information in DB.
   * @param {object} e : Event
   * @param {mixed} curValue : Current Field Value
   * @param {mixed} prevValue : Previous Field Value
   * @param {string} fieldName : Current Field Name
   */
  updateContactInfo = (e) => {
    const { clientData } = this.props;
    if (this.verifyData(clientData)) {
      this.props
        .onEditContactInfo(clientData)
        .then(() => {
          this.closeFormView();
        })
        .catch((error) => {
          Sentry.captureException(error);
        });
    } else {
      //console.log('Información incompleta.');
    }
  };

  onBlurForm = (e) => {
    console.log('BLUR', e);
    console.log('BLUR', this.props);
    if (this.verifyData(this.props.clientData)) {
      if(!sendStep1){
        console.log('Click datalayer');
        sendStep1 = true;
      }
    }
  };

  /**
   * renderContactForm()
   * Renders a form with buyer details.
   * @returns {obj} | Returns JSX (contact form)
   */
  renderContactForm = (contactDetailsTitle = '', showNavButtons = true) => (
    <div className="form module" onBlur={this.onBlurForm}>
      <h5 className="module__title">{contactDetailsTitle}</h5>
      <div className="module__fields">
        <Field
          component={InputField}
          id="receiverName"
          name="clientData.client_name"
          maxLength={70}
          onChange={this.onFieldFocus}
          onBlur={this.onFieldBlur}
          className="receiverName"
          placeholder="Nombre"
          showErrorsOnTouch={true}
        />
        <Field
          component={InputField}
          id="receiverLastName"
          name="clientData.client_lastName"
          maxLength={70}
          onChange={this.onFieldFocus}
          onBlur={this.onFieldBlur}
          className="receiverName"
          placeholder="Apellido"
          showErrorsOnTouch={true}
        />
        <Field
          id="phone"
          className="phone"
          placeholder="Teléfono"
          name="clientData.phone"
          component={InputField}
          normalize={normalizePhone}
          showErrorsOnTouch={true}
          onChange={this.onFieldFocus}
          onBlur={this.onFieldBlur}
        />
        <Field
          id="email"
          className="email"
          maxLength={70}
          placeholder="Email"
          name="clientData.email"
          component={InputField}
          showErrorsOnTouch={true}
          onChange={this.onFieldFocus}
          onBlur={this.onFieldBlur}
        />
      </div>

      {showNavButtons && (
        <div className="module__buttons">
          <button
            type="button"
            onClick={this.closeFormView}
            className="button-simple"
          >
            Regresar
          </button>
          <button
            type="button"
            className="c2a_border"
            onClick={this.updateContactInfo}
          >
            Aceptar
          </button>
        </div>
      )}
    </div>
  );

  /**
   * renderGiftForm()
   * Renders a form with fields for gift-receiver details.
   * @returns {jsx}
   */
  renderGiftForm = () => (
    <div className="form module">
      <h5 className="module__title">¿Para quién es tu regalo?</h5>
      <div className="module__fields">
        <Field
          component={InputField}
          maxLength={50}
          id="giftReceiverName"
          name="giftReceiver.name"
          className="giftReceiverName"
          placeholder="Nombre completo"
        />
        <Field
          component={InputField}
          id="giftReceiverPhone"
          name="giftReceiver.phone"
          className="giftReceiverPhone"
          placeholder="Teléfono*"
          normalize={normalizePhone}
        />
        <Field
          maxLength={400}
          name="giftReceiver.note"
          className="giftReceiverNote"
          component={TextAreaWithCounter}
          placeholder="Escribe un mensaje o dedicatoria"
        />
      </div>
    </div>
  );

  /**
   * render()
   */
  render() {
    const { shouldRenderFormView } = this.state;
    const { clientData, gift, isGuest } = this.props;
    const contactDetailsTitle = gift ? 'Datos del comprador' : 'Persona que recibe';

    //
    if (!clientData || !Object.keys(clientData).length) {
      return <IconPreloader />;
    }

    //
    return (
      <React.Fragment>
        {!shouldRenderFormView ? (
          <div className="module">
            <div className="container">
              <div className="module__header">
                <h5 className="module__title">{contactDetailsTitle}</h5>
                <button
                  type="button"
                  className="button-simple"
                  onClick={this.openFormView}
                >
                  Otra persona
                </button>
              </div>

              <div className="module__info">
                <div itemProp="logo" className="logo">
                  {/* <ResponsiveImage 
                                        src={user.profile_photo} 
                                        alt={`${user.first_name} ${user.last_name}`}
                                    /> */}
                </div>
                <div className="info">
                  <p className="name">
                    {`${clientData.client_name} ${clientData.client_lastName}`}
                  </p>
                  {/*<p>{clientData.email}</p>*/}
                  <p>{clientData.phone}</p>
                </div>
              </div>
            </div>
          </div>
        ) : (
          this.renderContactForm(contactDetailsTitle, !isGuest)
        )}

        <div className="module">
          <div className="container">
            <div className="gift">
              <Field
                id="gift"
                className="checkmark"
                name="gift"
                component="input"
                type="checkbox"
                value="gift"
              />
              <label htmlFor="gift" className="checkmark note--gift">
                Mi compra es para regalo.
                <span>
                  ¿Quieres sorprender a alguien?
                  <br />
                  Deja un mensaje personalizado y los datos de quien recibirá el
                  regalo.
                </span>
              </label>
            </div>
            {gift && this.renderGiftForm()}
          </div>
        </div>
      </React.Fragment>
    );
  }
}

// Export Component
export default ShippingContact;
