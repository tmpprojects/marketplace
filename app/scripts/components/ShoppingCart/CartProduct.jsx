import React, { Component } from 'react';
import { Field } from 'redux-form';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { ResponsiveImage } from '../../Utils/ImageComponents';
import { formatNumberToPrice } from '../../Utils/normalizePrice';
import { TextArea } from '../../Utils/forms/formComponents';
import withCounter from '../hocs/withCounter';
import QuantityButton from '../../Utils/QuantityButton';

/**
 * Converts path expression string to array
 * @param {string} pathExpr path string like "bindings[0][2].labels[0].title"
 * @return {Array<string|int>} array path like ['bindings', 0, 2, 'labels', 0, 'title']
 */
function breakPath(pathExpr) {
  return pathExpr
    .split(/\]\.|\]\[|\[|\]|\./)
    .filter((x) => x.length > 0)
    .map((x) => (isNaN(parseInt(x, 10)) ? x : parseInt(x, 10)));
}

/**
 * Executes path expression on the object
 * @param {string} pathExpr – path string like "bindings[0][2].labels[0].title"
 * @param {Object|Array} obj - object or array value
 * @return {mixed} a value lying in expression path
 * @example
 * ```
 * execPath('books[0].title', {books: [{title: 'foo'}]})
 * // yields
 * 'foo'
 * ```
 */
function execPath(pathExpr, obj) {
  const path = breakPath(pathExpr);
  if (path.length < 1) {
    return obj;
  }
  return path.reduce((o, pathPart) => o[pathPart], obj);
}

const TextAreaWithCounter = withCounter(TextArea);

/*====================================================
CART PRODUCT COMPONENT 
====================================================*/
class CartProduct extends Component {
  constructor(props) {
    super(props);
    this.state = {
      shouldRenderCalendar: false,
      shipping_date: props.product.shipping_date,
      quantity: props.product.quantity,
      note: props.product.note,
    };
  }

  /**
   * onCalendarDateChange()
   * @param {string} selectedDate | Selected Date from calendar
   */
  onCalendarDateChange = (selectedDate) => {
    // Transform date into UTC timezone
    const selectedDateUTC = new Date(selectedDate);

    // Update State
    setTimeout(
      () =>
        this.setState(
          {
            shipping_date: selectedDateUTC,
            shouldRenderCalendar: false,
          },
          () => {
            // Update Shopping Cart Product
            this.props.updateCartProduct();
          },
        ),
      600,
    );
  };

  /**
   * onDeliverySchedulesChange()
   * @param {int} selectedSchedule | Selected Date ID
   */
  onDeliverySchedulesChange = (selectedSchedule) => {
    // Update Shopping Cart Product
    this.props.updateCartProduct();
  };

  // onQuantityChange = quantity => {
  //     const { product,reduxFormChange, updateCartProduct } = this.props;

  //     reduxFormChange(quantity)
  //     .then(response => console.log(" haz algo mas", quantity))
  //     .catch(error => {});
  // }
  /**
   * render()
   * Render Component
   */
  render() {
    // Extract Properties
    const { shouldRenderCalendar } = this.state;
    const { updateCartProduct, removeProduct, product } = this.props;
    const {
      product: { photos = [] },
      attribute,
    } = product;
    // Get Fields
    const groupData = execPath(this.props.slice, this.props);
    const quantity = groupData.quantity;
    const note = groupData.note;

    return (
      <div className="product-detail">
        <div className="thumbnail">
          {photos.length > 0 && (
            <Link
              to={`/stores/${product.product.store.slug}/products/${product.product.slug}`}
            >
              <ResponsiveImage src={photos[0].photo} alt={product.product.name} />
            </Link>
          )}
        </div>
        <div className="product-detail__box-detail">
          <div className="product-detail__name">{product.product.name}</div>
          {product.attribute && (
            <div className="attributes">
              {product.attribute.tree_string
                .split(',')
                .map(
                  (item) =>
                    `${item.charAt(0).toUpperCase()}${item.slice(1).toLowerCase()}`,
                )
                .join(', ')}
            </div>
          )}
        </div>
        <div className="product-detail__price">
          <div className="form quantity">
            <label htmlFor={quantity.input.name}>Cantidad</label>
            <Field
              component={QuantityButton}
              name={quantity.input.name}
              quantity={quantity.input.value}
              onQuantityChange={(q) => {
                //Change value in  the form
                quantity.input.onChange(q);
                updateCartProduct();
              }}
              maxQuantity={product.remainingStock}
            />
          </div>

          <div className="price">
            <span> Subtotal:</span>{' '}
            {formatNumberToPrice(product.quantity * product.unit_price)}MX
          </div>

          <button onClick={removeProduct} className="button-simple remove">
            Eliminar 
          </button>
        </div>
        <div className="message-container form">
          <Field
            maxLength={1000}
            name={note.input.name}
            component={TextAreaWithCounter}
            placeholder="Envía una nota opcional a la tienda."
            cols="30"
            rows="1"
            onBlur={() => setTimeout(updateCartProduct, 400)}
          />
        </div>
      </div>
    );
  }
}

// Map Redux Props and Actions to component
const mapStateToProps = (state) => ({
  pickupSchedules: state.app.pickupSchedules,
});

const mapDispatchToProps = (dispatch) => ({});

// Export Component
export default connect(mapStateToProps, mapDispatchToProps)(CartProduct);
