import React from 'react';
import Moment from 'dayjs';
import { Link } from 'react-router-dom';

import { ResponsiveImage } from '../../Utils/ImageComponents';
import { formatNumberToPrice } from '../../Utils/normalizePrice';
import { SHIPPING_METHODS } from '../../Constants';

export default ({ productsByStore }) => {
  return (
    <ul className="productList">
      {productsByStore.map((order, i) => {
        //
        const isStandardShipping =
          order.physical_properties.selected_shipping_method &&
          order.physical_properties.selected_shipping_method.slug ===
            SHIPPING_METHODS.STANDARD.slug;

        let shippingDateLabel = `Entrega el ${Moment(
          order.physical_properties.shipping_date,
        ).format('D MMMM')}.`;
        if (isStandardShipping) {
          const fixedDate = Moment(order.physical_properties.shipping_date);
          const estimatedDate = fixedDate.add('2', 'day');
          shippingDateLabel = `Recíbelo entre el ${fixedDate.format(
            'D MMMM',
          )} - ${estimatedDate.format('D MMMM')}.`;
        }

        return (
          <div className="store-card" key={`${order.store.slug}-${i}`}>
            <div className="store-detail">
              <Link className="store" to={`/stores/${order.store.slug}`}>
                {order.store.name}
              </Link>
            </div>
            {order.products.map((product) => (
              <div
                className={`product ${
                  ''
                  // product.product.physical_properties.shipping.find(s => !s.is_available) ? 'unavailable' : ''
                }`}
                key={product.product.id}
              >
                <div className="product_photo">
                  {product.product.photos.length > 0 && (
                    <Link
                      className="name"
                      to={`/stores/${product.product.store.slug}/products/${product.product.slug}`}
                    >
                      <ResponsiveImage
                        src={product.product.photos[0].photo}
                        alt={product.product.name}
                      />
                    </Link>
                  )}
                </div>
                <div className="product_detail">
                  <Link
                    className="name"
                    to={`/stores/${product.product.store.slug}/products/${product.product.slug}`}
                  >
                    {product.product.name}
                    {product.quantity > 1 ? ` (x${product.quantity})` : ''}
                  </Link>
                  {product.attribute && (
                    <p className="attribute">
                      {product.attribute.tree_string
                        .split(',')
                        .map(
                          (item) =>
                            `${item.charAt(0).toUpperCase()}${item
                              .slice(1)
                              .toLowerCase()}`,
                        )
                        .join(', ')}
                    </p>
                  )}

                  {order.physical_properties && (
                    <div className="delivery">{shippingDateLabel}</div>
                  )}
                </div>
                <div className="product_price">
                  {product.quantity > 1
                    ? formatNumberToPrice(product.unit_price * product.quantity)
                    : formatNumberToPrice(product.unit_price)}
                </div>
              </div>
            ))}
          </div>
        );
      })}
    </ul>
  );
};
