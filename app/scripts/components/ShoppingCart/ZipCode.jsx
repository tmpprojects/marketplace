import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Field, reduxForm } from 'redux-form';

import { shoppingCartActions } from '../../Actions';
import { InputField } from '../../Utils/forms/formComponents';
import { SHOPPING_CART_FORM_CONFIG } from '../../Utils/shoppingCart/shoppingCartFormConfig';

let ZipCode = (props) => {
  const { shipping_postal_code } = props;

  const onClickHandler = (e) => {
    //props.updateShippingMethod(`${order}.physical_properties.selected_shipping_method`, );
    props.getPriceByZipCode(shipping_postal_code);
  };

  return (
    <fieldset className="form">
      <Field
        name="shipping_postal_code"
        id="shipping_postal_code"
        component={InputField}
        className="field"
        label="C.P."
      />
      <button type="submit" className="button-simple" onClick={onClickHandler}>
        Calcular
      </button>
    </fieldset>
  );
};

ZipCode = reduxForm({
  form: SHOPPING_CART_FORM_CONFIG.formName,
})(ZipCode);

function mapStateToProps() {
  return {};
}
function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      getPriceByZipCode: shoppingCartActions.getPriceByZipCode,
    },
    dispatch,
  );
}

// Export Connected Component
export default connect(mapStateToProps, mapDispatchToProps)(ZipCode);
