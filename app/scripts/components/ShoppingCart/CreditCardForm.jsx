import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Field, reduxForm, getFormValues } from 'redux-form';
import Payment from 'payment'; //'../../Utils/forms/PaymentCustom';

import { getUserProfile } from '../../Reducers/users.reducer';
import { validateCreditCardForm } from '../../Utils/forms/formValidators';

import { shoppingCartActions } from '../../Actions';
import { bindActionCreators } from 'redux';

// CONSTANTS
const CREDIT_CARD_DEFAULT_MODEL = {
  cardProvider: null,
  data: null,
  secureForm: null,
  isValid: false,
  isStoredCard: false,
  saveCreditCard: false,
  type: 'mercado-pago',
};

/*
 * Render Custom Input Field
 * @param {object} props : Field Properties
 */
const CardField = ({
  input: { onBlur, ...input },
  meta,
  className: classSuffix = '',
  normalizeOnBlur,
  showErrorsOnTouch = true,
  ...props
}) => {
  const getErrorClass = () => {
    if (!showErrorsOnTouch) {
      return 'error';
    } else if (meta.touched) {
      return 'error';
    }
    return '';
  };

  const renderErrors = () => (
    <div>
      {(meta.error && <div className="form_status danger">{meta.error}</div>) ||
        (meta.warning && <div className="form_status warning">{meta.warning}</div>)}
    </div>
  );
  return (
    <div className={`form__data ${classSuffix}`}>
      <label className="title" htmlFor={props.id}>
        {props.label}
      </label>
      <input
        className={!meta.valid ? getErrorClass() : ''}
        {...input}
        onBlur={(event) => {
          const value =
            event && event.target && event.target.hasOwnProperty('value')
              ? event.target.value
              : event;
          const newValue = normalizeOnBlur ? normalizeOnBlur(value) : value;
          onBlur(newValue);
        }}
        {...props}
      />
      {!showErrorsOnTouch ? renderErrors() : meta.touched ? renderErrors() : null}
    </div>
  );
};

const CardIssuers = ({ input, meta, showErrorsOnTouch = true, ...props }) => {
  const getErrorClass = () => {
    if (!showErrorsOnTouch) {
      return 'error';
    } else if (meta.touched) {
      return 'error';
    }
    return '';
  };
  const renderErrors = () => (
    <div>
      {(meta.error && <div className="form_status danger">{meta.error}</div>) ||
        (meta.warning && <div className="form_status warning">{meta.warning}</div>)}
    </div>
  );
  return (
    <div className="form__data cc-issuer">
      <label className="title">Emisor de la tarjeta</label>
      <select {...input} className={!meta.valid ? getErrorClass() : ''} {...props} />
      {!showErrorsOnTouch ? renderErrors() : meta.touched ? renderErrors() : null}
    </div>
  );
};

/*
 * Render Custom Input Field
 * @param {object} props : Field Properties
 */
const trimValues = (value) => value.trim();

/*
 * Credit Card Form
 */
class CreditCardForm extends Component {
  constructor(props) {
    super(props);
    this.guessingPaymentMethod = this.guessingPaymentMethod.bind(this);
    this.setPaymentMethodInfo = this.setPaymentMethodInfo.bind(this);
    this.fillSecureCardForm = this.fillSecureCardForm.bind(this);
    this.fetchCardIssuers = this.fetchCardIssuers.bind(this);
  }
  componentDidMount() {
    // Get Secure (hidden) form
    this.secureForm = document.querySelector('#pay');

    // get credit card info DOM input fields
    this.name = document.querySelector('.cc-name input');
    this.number = document.querySelector('.cc-number input');
    this.exp = document.querySelector('.cc-exp input');
    this.cvc = document.querySelector('.cvc input');
    this.validation = document.querySelector('.validation');
    this.issuerList = document.querySelector('.cc-issuer select');
    this.issuerList.parentNode.style.display = 'none';
    this.requireIssuer = false;
    this.issuersFetchStatus = 'idle';

    // Clear Form
    this.clearSecureCardForm();

    // Format credit card input fields
    Payment.formatCardNumber(this.number, 16);
    Payment.formatCardExpiry(this.exp);
    Payment.formatCardCVC(this.cvc);
  }
  componentWillReceiveProps(nextProps) {
    if (nextProps.refresh) {
      this.validateFormFields();
    }
  }

  /*
   * setPaymentMethodInfo()
   * Set Payment Method Details
   * @param {object} e : Event Object
   */
  setPaymentMethodInfo(status, response) {
    if (status === 200 && response) {
      // Add Payment Provider Logo to form
      const thumbContainer = document.querySelector('.new-card');

      thumbContainer.className = `new-card  card-provider ${response[0].id}`;

      // Add Provider ID to form
      const paymentMethod = this.secureForm.querySelector(
        'input[name=paymentMethodId]',
      );
      paymentMethod.setAttribute('value', response[0].id);

      // If needed, show users the card issuer.
      // Since this function is called several times,
      // ensure that we fetch the issuers list only once.
      this.requireIssuer = response[0].additional_info_needed.includes('issuer_id');
      if (this.issuersFetchStatus === 'idle' && this.requireIssuer) {
        this.fetchCardIssuers(response[0].id);
      }
    } else {
      // TODO: Log Error.
    }
  }

  /*
   * Guess Payment Method
   * Get Payment Method/Card Type Based on Card BIN (first 6 digits)
   * @param {object} e : Event Object
   */
  guessingPaymentMethod(event) {
    // Catch first card digits to determine card type
    const bin = event.target.value;
    if (event.type === 'keyup') {
      if (bin.length >= 6) {
        Mercadopago.getPaymentMethod(
          {
            bin,
          },
          this.setPaymentMethodInfo,
        );
      }
    } else {
      setTimeout(() => {
        if (bin.length >= 6) {
          Mercadopago.getPaymentMethod(
            {
              bin,
            },
            this.setPaymentMethodInfo,
          );
        }
      }, 100);
    }
  }

  /**
   * validateFormFields()
   * Validate Card information and updates payment method information
   */
  validateFormFields = () => {
    //Erase data from other form
    //this.props.dispatch(reset('CardId_form'));
    let valid = true;

    // Basic Credit Card validation
    const validName = this.name.value.length;
    const validPayment = Payment.fns.validateCardNumber(this.number.value);
    const validExp = Payment.fns.validateCardExpiry(this.exp.value);
    const cardType = Payment.fns.cardType(this.number.value);
    //const validCVC = Payment.fns.validateCardCVC(this.cvc.value, cardType);

    //  ******* Temporal fix to let go of the shopping
    const validCVC = true;

    // Look for errors in form
    // and update the 'valid' flag accordingly.
    if (
      !validName ||
      !validPayment ||
      !validExp ||
      !validCVC ||
      (this.requireIssuer && !this.issuerList.value)
    ) {
      valid = false;
    }

    // Manage Form
    // Delay actions wit a setTimeout because redux-form sometimes takes
    // a while to update form values. Otherwise we may have outdated values.
    setTimeout(() => {
      if (!valid) {
        // Reset parent paymentMethod
        this.props.onCreditCardUpdate({
          ...CREDIT_CARD_DEFAULT_MODEL,
          saveCreditCard: this.props.formValues.saveCreditCard,
        });

        // if form is OK...
        // Proceed to fill up the form that will be sent to MercadoPago API
      } else {
        // Fill Secure Credit Card Form.
        this.fillSecureCardForm();

        // Send PaymentMethod to parent
        this.props.onCreditCardUpdate({
          // Use default model...
          ...CREDIT_CARD_DEFAULT_MODEL,
          // and overwrite properties
          cardProvider: this.secureForm.querySelector('input[name=paymentMethodId]')
            .value,
          secureForm: this.secureForm,
          isValid: true,
          saveCreditCard: this.props.formValues.saveCreditCard,
        });
      }
    }, 600);
  };

  /**
   * fetchCardIssuers()
   * Retrives a list of bank/card issuers based on a debit/credit card.
   * Create options based on returned elements and populate 'card issuers list' selectbox field.
   * @param {object} cardType | CardType Object
   */
  fetchCardIssuers(cardType) {
    // Update Fetch Status
    this.issuersFetchStatus = 'loading';

    // Show Issuers List
    this.issuerList.parentNode.style.display = 'block';

    // Get Card Issuers List from Mercado Pago
    Mercadopago.getIssuers(cardType, (status, issuers) => {
      // Update loading state
      this.issuersFetchStatus = 'loaded';

      // Sort Alphabetically
      issuers.sort((a, b) => {
        const textA = a.name.toUpperCase();
        const textB = b.name.toUpperCase();
        return textA < textB ? -1 : textA > textB ? 1 : 0;
      });

      // Delete current options from the selectbox.
      for (let i = 0; i < this.issuerList.options.length; i++) {
        this.issuerList.options[i] = null;
      }

      // Create and append the options
      let option = document.createElement('option');
      option.value = '';
      option.text = 'Elige una opción';
      this.issuerList.appendChild(option);
      this.issuerList.value = '';
      issuers.forEach((issuer) => {
        option = document.createElement('option');
        option.value = issuer.id;
        option.text = issuer.name;
        this.issuerList.appendChild(option);
      });
    });
  }

  /*
   * Set Payment Method Details
   * Fillup an HTML form with redux-form value.
   * (This is the one that´ll be sent to MercadoPago!)
   * @param {object} e : Event Object
   */
  fillSecureCardForm() {
    const expiry = Payment.fns.cardExpiryVal(this.exp.value);
    this.secureForm.querySelector(
      "input[data-checkout='cardholderName']",
    ).value = this.name.value;
    this.secureForm.querySelector(
      "input[data-checkout='cardNumber']",
    ).value = this.number.value;
    this.secureForm.querySelector(
      "input[data-checkout='securityCode']",
    ).value = this.cvc.value;
    this.secureForm.querySelector(
      "select[data-checkout='issuerId']",
    ).value = this.issuerList.value;
    this.secureForm.querySelector(
      "input[data-checkout='cardExpirationYear']",
    ).value = expiry.year;
    this.secureForm.querySelector(
      "input[data-checkout='cardExpirationMonth']",
    ).value = expiry.month;

    return false;
  }

  /*
   * Set Payment Method Details
   * Clear HTML form values.
   */
  clearSecureCardForm() {
    this.secureForm.querySelector("input[data-checkout='cardholderName']").value =
      '';
    this.secureForm.querySelector("input[data-checkout='cardNumber']").value = '';
    this.secureForm.querySelector("input[data-checkout='securityCode']").value = '';
    this.secureForm.querySelector("select[data-checkout='issuerId']").value = '';
    this.secureForm.querySelector(
      "input[data-checkout='cardExpirationYear']",
    ).value = '';
    this.secureForm.querySelector(
      "input[data-checkout='cardExpirationMonth']",
    ).value = '';
  }

  /**
   * render()
   * Render Component
   */
  render() {
    return (
      <div className="payment-choice__details">
        <div className="form creditCard__form">
          <fieldset>
            <Field
              label="Nombre del Titular de la Tarjeta"
              className="cc-name"
              name="cc-name"
              type="text"
              placeholder="Nombre del Titular de la Tarjeta"
              autoComplete="card-name"
              autoCorrect="off"
              spellCheck="off"
              component={CardField}
              showErrorsOnTouch={true}
              normalizeOnBlur={trimValues}
              onKeyUp={this.validateFormFields}
              onBlur={this.validateFormFields}
            />

            <div className="card_data">
              <div className="card_data--number" style={{ position: 'relative' }}>
                <Field
                  name="cc-number"
                  className="cc-number"
                  pattern="\d*"
                  autoComplete="card-number"
                  autoCorrect="off"
                  spellCheck="off"
                  label="Número de Tarjeta"
                  placeholder="&bull;&bull;&bull;&bull; &bull;&bull;&bull;&bull; &bull;&bull;&bull;&bull; 1234"
                  normalizeOnBlur={trimValues}
                  component={CardField}
                  showErrorsOnTouch={true}
                  onKeyUp={(e) => {
                    this.guessingPaymentMethod(e);
                    this.validateFormFields();
                  }}
                  onBlur={this.validateFormFields}
                />
                <div className="new-card card-provider" />
              </div>

              <Field
                name="issuerList"
                component={CardIssuers}
                onChange={this.validateFormFields}
                onBlur={this.validateFormFields}
                showErrorsOnTouch={true}
              />

              <Field
                name="cc-exp"
                className="cc-exp"
                x-autocompletetype="cc-exp"
                autoComplete="card-exp"
                autoCorrect="off"
                spellCheck="off"
                pattern="\d*"
                label="Fecha de Expiración"
                placeholder="MM/YYYY"
                maxLength="9"
                component={CardField}
                showErrorsOnTouch={true}
                onKeyUp={this.validateFormFields}
                onBlur={this.validateFormFields}
              />

              <Field
                name="cc-cvc"
                className="cvc tablet-sm--half"
                x-autocompletetype="cc-cvc"
                autoComplete="card-cvc"
                autoCorrect="off"
                spellCheck="off"
                pattern="\d*"
                label="Código de Seguridad"
                placeholder="CVV"
                autoComplete="off"
                maxLength="4"
                component={CardField}
                showErrorsOnTouch={true}
                normalize={(value) => value.replace(/\D/g, '')}
                onKeyUp={this.validateFormFields}
                onBlur={this.validateFormFields}
              />
            </div>
            <div className="saveCreditCard">
              <Field
                id="saveCreditCard"
                name="saveCreditCard"
                className="checkmark"
                component="input"
                type="checkbox"
                onChange={this.validateFormFields}
                onBlur={this.validateFormFields}
              />
              <label htmlFor="saveCreditCard" className="checkmark">
                Guardar tarjeta
              </label>
            </div>
            <div className="validation" />
          </fieldset>
        </div>

        <div style={{ display: 'none' }} action="" method="post" id="pay" name="pay">
          <fieldset>
            <input
              data-checkout="cardNumber"
              type="text"
              autoComplete="off"
              onPaste={() => false}
              onCopy={() => false}
              onCut={() => false}
              onDrag={() => false}
              onDrop={() => false}
            />
            <input
              data-checkout="securityCode"
              type="text"
              autoComplete="off"
              onPaste={() => false}
              onCopy={() => false}
              onCut={() => false}
              onDrag={() => false}
              onDrop={() => false}
            />
            <input
              data-checkout="cardExpirationMonth"
              type="text"
              autoComplete="off"
              onPaste={() => false}
              onCopy={() => false}
              onCut={() => false}
              onDrag={() => false}
              onDrop={() => false}
            />
            <input
              data-checkout="cardExpirationYear"
              type="text"
              autoComplete="off"
              onPaste={() => false}
              onCopy={() => false}
              onCut={() => false}
              onDrag={() => false}
              onDrop={() => false}
            />
            <input data-checkout="cardholderName" type="text" />
            <select id="docType" data-checkout="docType" />
            <select id="issuersList" data-checkout="issuerId" />
            <input data-checkout="docNumber" type="text" />
            <input data-checkout="cardId" type="text" name="cardId" />
            <input type="hidden" name="paymentMethodId" />
            <input type="hidden" name="token" />
          </fieldset>
        </div>
      </div>
    );
  }
}

// Wrap component within reduxForm
CreditCardForm = reduxForm({
  form: 'CreditCard_form',
  validate: validateCreditCardForm,
})(CreditCardForm);

function mapDispatchToProps(dispatch) {
  return bindActionCreators({}, dispatch);
}

// Wrap Form within Redux
CreditCardForm = connect((state) => {
  const user = getUserProfile(state);
  return {
    formValues: getFormValues('CreditCard_form')(state),
    initialValues: {
      'cc-name':
        !user.first_name && !user.last_name
          ? null
          : `${user.first_name} ${user.last_name}`,
      'cc-cvc': null,
      saveCreditCard: false,
    },
  };
}, mapDispatchToProps)(CreditCardForm);

// Export Component
export default CreditCardForm;
