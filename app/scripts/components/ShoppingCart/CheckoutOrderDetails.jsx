import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { formValueSelector, getFormSyncErrors } from 'redux-form';
import { bindActionCreators } from 'redux';

import ShippingDetailsManager from './ShippingDetailsManager';
import PaymentMethodSelector from './PaymentMethodSelector';
import CollapseBox from '../../Utils/CollapseBox';
import { SHIPPING_METHODS } from '../../Constants';
import { SHOPPING_CART_FORM_CONFIG } from '../../Utils/shoppingCart/shoppingCartFormConfig';
import { shoppingCartActions } from './../../Actions';
import { trackWithGTM } from '../../Utils/trackingUtils';

// Styles
import './CheckoutMethods.scss';

// CONSTANTS
const NAV_SECTION = {
  SHIPPING: 'SHIPPING',
  PAYMENT: 'PAYMENT',
};

/**
 * CheckoutOrderDetails
 * Manages Shopping Order Shipping and Payment Details.
 * These include PaymentMethod, Receiver Personal Details,
 * Shipping Address, Billing Address, etc...
 */
export class CheckoutOrderDetails extends Component {
  /**
   * Component Props Definition
   */
  static propTypes = {
    syncErrors: PropTypes.object.isRequired,
    clientData: PropTypes.object.isRequired,
    selectedPaymentMethod: PropTypes.object.isRequired,
    selectedShippingAddress: PropTypes.string.isRequired,
    onShippingAddressChange: PropTypes.func.isRequired,
    onPaymentMethodChange: PropTypes.func.isRequired,
  };
  static defaultProps = {};

  /**
   * Component State Definition
   */
  state = {
    sectionStatus: {
      [NAV_SECTION.SHIPPING]: {
        section: NAV_SECTION.SHIPPING,
        isOpen: true,
      },
      [NAV_SECTION.PAYMENT]: {
        section: NAV_SECTION.PAYMENT,
        isOpen: true,
      },
    },
  };

  componentDidMount() {
    const items = this.props.productsByStore.reduce(
      (a, b) => [
        ...a,
        ...b.products.map((p, i) => ({
          id: p.product.id,
          name: p.product.name,
          //variant: p.product.physical_properties.size.display_name, falta que Ara boy agregue key
          brand: b.store.name,
          quantity: p.quantity,
          price: p.price,
          dimension1: p.product.slug,
          dimension2: b.store.slug,
          // category: 'guides/google-tag-manager/enhanced-ecommerce',
        })),
      ],
      [],
    );
    //Send to Google Tag Manager
    trackWithGTM('eec.checkout', {
      actionField: {
        step: 2,
      },
      products: items,
    });
  }

  /**
   * onTabChanged()
   * This event is called when a 'Order Detail Tab' has changed.
   * These tabs currently are 'Envío' and 'Pago'.
   * @param {obj} sectionInfo || An object with info of the selected tab.
   */
  onTabChanged = (sectionInfo) => {
    this.setState({
      sectionStatus: {
        ...this.state.sectionStatus,
        [sectionInfo.section]: sectionInfo,
      },
    });
  };

  /**
   * render()
   */
  render() {
    const { sectionStatus } = this.state;
    const {
      syncErrors,
      clientData,
      selectedPaymentMethod,
      selectedShippingAddress,
      onShippingAddressChange,
      onPaymentMethodChange,
    } = this.props;

    // Form Validations
    const shippingAddressCompleted =
      selectedShippingAddress && selectedShippingAddress !== '';
    const clientInfoCompleted =
      Boolean(!syncErrors.clientData) && Boolean(clientData && clientData.phone);
    const paymentCompleted = selectedPaymentMethod && selectedPaymentMethod.isValid;
    const changeShippingPostalCode = changeShippingPostalCode;

    // Tabs status
    const isShippingTabOpen = sectionStatus[NAV_SECTION.SHIPPING].isOpen;
    const isPaymentTabOpen = sectionStatus[NAV_SECTION.PAYMENT].isOpen;

    // Return Markup
    return (
      <div className="checkout__methods">
        {/* SHIPPING DETAILS TAB */}
        <CollapseBox
          onSelect={(status) =>
            this.onTabChanged({ section: NAV_SECTION.SHIPPING, isOpen: status })
          }
          open={isShippingTabOpen}
          disabled={!shippingAddressCompleted || !clientInfoCompleted}
          activeClass={
            shippingAddressCompleted && clientInfoCompleted ? 'active' : ''
          }
          extraClass={shippingAddressCompleted && clientInfoCompleted ? 'valid' : ''}
          content={
            <ShippingDetailsManager
              clientInfoCompleted={clientInfoCompleted}
              shippingAddressCompleted={shippingAddressCompleted}
              onShippingAddressChange={onShippingAddressChange}
            />
          }
          header={
            <h5 className="title shipping">
              Envío {!isShippingTabOpen && <span>Cambiar</span>}
            </h5>
          }
        />

        {/* PAYMENT DETAILS TAB */}
        <CollapseBox
          onSelect={(status) =>
            this.onTabChanged({ section: NAV_SECTION.PAYMENT, isOpen: status })
          }
          open={isPaymentTabOpen}
          disabled={!paymentCompleted}
          activeClass={paymentCompleted ? 'active' : ''}
          extraClass={paymentCompleted ? 'valid' : ''}
          content={
            <PaymentMethodSelector onPaymentMethodChange={onPaymentMethodChange} />
          }
          header={
            <h5 className="title payment">
              Pago&nbsp;
              {!isPaymentTabOpen && <span>Cambiar</span>}
            </h5>
          }
        />
      </div>
    );
  }
}

// Pass Redux state and actions to component
const formName = SHOPPING_CART_FORM_CONFIG.formName;
const selector = formValueSelector(formName);

function mapDispatchToProps(dispatch) {
  return {
    ...bindActionCreators(
      {
        removeFromCart: shoppingCartActions.removeFromCart,
      },
      dispatch,
    ),
  };
}
function mapStateToProps(state) {
  const shippingMethod =
    selector(state, 'shippingMethod') || SHIPPING_METHODS.EXPRESS.slug;
  const paymentMethodDetails = selector(state, 'paymentMethodDetails');
  const shippingPostalCode = selector(state, 'shipping_postal_code');
  return {
    clientData: selector(state, 'clientData'),
    coupon: selector(state, 'discount_code') || '',
    syncErrors: getFormSyncErrors(formName)(state),
    addressShipping: selector(state, 'addressShipping'),
    shippingMethods: state.app.shippingMethods,
    selectedPaymentMethod: paymentMethodDetails,
    selectedShippingMethod: shippingMethod,
    productsByStore: selector(state, 'orders'),
    selectedShippingAddress: selector(state, 'selectedShippingAddress'),
    selectShippingPostalCode: shippingPostalCode,
  };
}

// Export Component
export default connect(mapStateToProps, mapDispatchToProps)(CheckoutOrderDetails);
