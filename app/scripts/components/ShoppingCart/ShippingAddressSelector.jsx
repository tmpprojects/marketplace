import React, { Component } from 'react';
import { Field } from 'redux-form';

import AddressesList from '../../Utils/AddressesList';
import { googleMapsID } from '../../../config';
import { IconPreloader } from '../../Utils/Preloaders';

class ShippingAddressSelector extends Component {
  constructor(props) {
    super(props);
    this.state = {
      shouldRenderAddresses: Boolean(!this.props.defaultAddress) || false,
    };
  }
  componentWillReceiveProps(nextProps) {
    if (this.props.defaultAddress !== nextProps.defaultAddress) {
      this.setState({
        shouldRenderAddresses: Boolean(!nextProps.defaultAddress) || false,
      });
    }
  }
  onAddressChange = (e, address) => {
    this.props.onAddressChange(address);
  };
  closeAddressList = () => {
    this.setState({
      shouldRenderAddresses: false,
    });
  };
  openWindowForm = () => {
    this.setState({
      shouldRenderAddresses: true,
    });
  };

  /*
   * renderShippingAddresses()
   * Render Available Addresses
   * @param {array} addresses | Array of addresses
   * @return {jsx}
   */
  renderShippingAddresses = (addresses) => {
    const {
      defaultAddress: selectedShippingAddress,
      openAddressWindow,
    } = this.props;

    return (
      <React.Fragment>
        {this.props.isLogged && (
          <button
            className="address-selector__button button-simple"
            onClick={openAddressWindow}
          >
            + Agregar
          </button>
        )}

        <div className="addresses_container">
          {addresses.map((address) => (
            <Field
              type="radio"
              key={address.uuid}
              id={`address_${address.uuid}`}
              name="selectedShippingAddress"
              value={address.uuid}
              userType="buyer"
              address={address}
              isLogged={this.props.isLogged}
              onChange={this.onAddressChange}
              onClick={(e) => setTimeout(this.closeAddressList, 400)}
              updateAddress={this.props.onUpdateAddress}
              deleteAddress={this.props.onDeleteAddress}
              component={AddressesList}
              active={address.uuid === selectedShippingAddress}
            />
          ))}
        </div>
      </React.Fragment>
    );
  };

  renderAddressList = () => {
    const { openAddressWindow, addresses = [] } = this.props;
    const shippingAddresses = this.renderShippingAddresses(addresses);
    return (
      <div className="module shipping__address">
        <h5 className="module__title">Elige una dirección</h5>
        <div className="addresses_container">
          {addresses.length > 0 && shippingAddresses}

          {!addresses.length && (
            <div
              className={`add_container ${
                !addresses.length ? 'add_container--no-address' : ''
              }`}
            >
              <button
                type="button"
                onClick={openAddressWindow}
                className={`${
                  !addresses.length ? 'c2a_round' : 'button-square--gray button--add'
                }`}
              >
                + Agregar
              </button>
            </div>
          )}
        </div>
      </div>
    );
  };

  render() {
    const { shouldRenderAddresses } = this.state;
    const { addresses, defaultAddress, openAddressWindow } = this.props;

    if (!shouldRenderAddresses && !addresses.length) {
      return <IconPreloader />;
    }

    return (
      <React.Fragment>
        {!shouldRenderAddresses ? (
          <div className="module">
            {addresses.length > 0 ? (
              <React.Fragment>
                <div className="module__header">
                  <h5 className="module__title">Dirección de Entrega</h5>
                  <button
                    type="button"
                    className="button-simple"
                    onClick={this.openWindowForm}
                  >
                    Cambiar
                  </button>
                </div>
                <div className="module__info address">
                  <div className="map_container">
                    {defaultAddress.latitude && defaultAddress.longitude ? (
                      <img
                        alt=""
                        src={`https://maps.googleapis.com/maps/api/staticmap?center=${defaultAddress.latitude},${defaultAddress.longitude}&zoom=16&scale=2&size=112x112&markers=size:small%7Ccolor:0xff0000%7Clabel:1%7C${defaultAddress.latitude},${defaultAddress.longitude}&key=${googleMapsID}`}
                      />
                    ) : (
                      <img
                        alt=""
                        src={require('../../../images/utils/map-location-placeholder.svg')}
                      />
                    )}
                  </div>
                  <div className="info">
                    <strong style={{ fontWeight: 'bold' }}>
                      {defaultAddress.address_name}
                    </strong>
                    <br />
                    <span>
                      {defaultAddress.street_address} {defaultAddress.num_ext},
                      {defaultAddress.num_int ? `${defaultAddress.num_int},` : ''}
                      {defaultAddress.neighborhood}, {defaultAddress.city},{' '}
                      {defaultAddress.zip_code}.
                    </span>
                  </div>
                </div>
              </React.Fragment>
            ) : (
              <React.Fragment>
                <div className="module__header">
                  <h5 className="module__title">Dirección de Entrega</h5>
                </div>
                <div className="module__info address">
                  <button
                    type="button"
                    onClick={openAddressWindow}
                    className="c2a_square"
                  >
                    Agregar nueva dirección
                  </button>
                </div>
              </React.Fragment>
            )}
          </div>
        ) : (
          this.renderAddressList()
        )}
      </React.Fragment>
    );
  }
}

// Export Component
export default ShippingAddressSelector;
