import { SHIPPING_METHODS } from '../../Constants/config.constants';
import { containsEmoji } from '../../Utils/genericUtils';

export const validateShoppingForm = (values) => {
  //debugger;
  const errors = {};
  if (!values) {
    errors.emptyForm = 'Form must not be empty';
    return errors;
  }

  // if (!values.products || !values.products.length) {
  //     errors.products = 'No Products on Cart'
  // }
  let requiresAddress = true;
  if (values.hasOwnProperty('orders')) {
    requiresAddress = values.orders.find((o) =>
      o.physical_properties.selected_shipping_method
        ? o.physical_properties.selected_shipping_method.slug !==
          SHIPPING_METHODS.PICKUP_POINT.slug
        : false,
    );
  }

  // if (requiresAddress) {
  //     errors.shippingMethod = 'Shipping Method Required';
  // }
  if (requiresAddress && !values.selectedShippingAddress) {
    errors.shippingMethod = 'Selecciona una dirección';
  }

  if (!values.addressShipping && requiresAddress) {
    errors.addressShipping = 'Ingresa una dirección valida.';
  }

  if (!values.addressBilling && requiresAddress) {
    errors.addressBilling = 'Billing Address Required';
  }

  if (!values.paymentMethodDetails || !values.paymentMethodDetails.isValid) {
    errors.paymentMethodDetails = 'Ingresa un método de pago valido';
  }

  //ERRORS FOR cc-cvv when is Stored Card
  if ('paymentMethodDetails' in values) {
    if ('isStoredCard' in values.paymentMethodDetails) {
      if (values.paymentMethodDetails.isStoredCard === true) {
        if (!values['cc-cvv']) {
          errors['cc-cvv'] = 'Introduce un código de tarjeta';
        } else if (values['cc-cvv'].length < 3) {
          errors['cc-cvv'] = 'Introduce un código de tarjeta válido. ';
        }
      }
    }
  }

  //

  errors.giftReceiver = {}; //create an empty object to don't have undefined related error following
  if (values.gift) {
    // if checkbox for gift is checked throw errors

    //ERRORS FOR name OF GIFT RECEIVER
    if (!values?.giftReceiver?.name) {
      errors.giftReceiver['name'] = 'Introduce el nombre del destinatario.';
    } else if (containsEmoji(values.giftReceiver.name)) {
      errors.giftReceiver['name'] =
        'Por favor introduce caracteres válidos en este campo.';
    }

    //ERRORS FOR phone OF GIFT RECEIVER
    if (!values?.giftReceiver?.phone) {
      errors.giftReceiver.phone = 'Introduce el teléfono del destinatario.';
    } else if (values.giftReceiver.phone.length < 12) {
      errors.giftReceiver['phone'] =
        'Debes introducir un número telefónico de por lo menos 10 dígitos.';
    }
  }

  //

  errors.clientData = {}; //create an empty object to don't have undefined related error following
  if (!values?.clientData) {
    errors.clientData = 'Información del cliente obligatoria.';
  } else {
    //ERRORS FOR client_name FIELD
    if (!values?.clientData?.client_name) {
      errors.clientData['client_name'] = 'Introduce tu nombre.';
    } else if (containsEmoji(values.clientData.client_name)) {
      errors.clientData['client_name'] =
        'Por favor introduce caracteres válidos en este campo.';
    } else if (values.clientData.client_name.length < 3) {
      errors.clientData['client_name'] =
        'Debes introducir más de 2 caracteres en este campo.';
    }
    //ERRORS FOR client_lastName FIELD
    if (!values?.clientData?.client_lastName) {
      errors.clientData['client_lastName'] = 'Introduce tu(s) apellido(s).';
    } else if (containsEmoji(values.clientData.client_name)) {
      errors.clientData['client_lastName'] =
        'Por favor introduce caracteres válidos en este campo.';
    } else if (values?.clientData?.client_lastName.length < 3) {
      errors.clientData['client_lastName'] =
        'Debes introducir más de 2 caracteres en este campo.';
    }

    //ERRORS FOR phone FIELD
    if (!values?.clientData.phone) {
      errors.clientData['phone'] = 'Introduce tu teléfono.';
    } else if (values?.clientData?.phone?.length < 12) {
      errors.clientData['phone'] =
        'Debes introducir un número telefónico de por lo menos 10 dígitos.';
    }
    //ERRORS FOR email FIELD
    if (!values?.clientData?.email) {
      errors.clientData['email'] = 'Introduce tu email.';
    } else if (
      !/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(
        values?.clientData?.email,
      )
    ) {
      errors.clientData['email'] = 'Introduce un email válido';
    }
  }

  //delete key giftReceiver if errors.giftReceiver={}
  if (Object.keys(errors.giftReceiver).length === 0) {
    delete errors['giftReceiver'];
  }

  //delete key clientData if errors.clientData={}
  if (Object.keys(errors.clientData).length === 0) {
    delete errors['clientData'];
  }

  return errors;
};

export const warnShoppingForm = (values) => {
  const warnings = {};

  warnings.giftReceiver = {};

  if (!values?.giftReceiver?.note) {
    warnings.giftReceiver.note =
      '¿Seguro que no quieres dejar un mensaje o dedicatoria?';
  }

  return warnings;
};

export const shippingSubmit = () => {
  console.log('submit 1');
  //return false;
  //history.push("/checkout/payment")
};
export const paymentSubmit = () => {
  console.log('submit 2');
  //return false;
  //history.push("/checkout/payment")
};
export const confirmationSubmit = () => {
  console.log('submit 3');
  //window.location = "/checkout/review"
  //return false;
  //history.push("/checkout/payment")
};
