import React from 'react';
import Moment from 'dayjs';
import { Link } from 'react-router-dom';

import { ResponsiveImage } from '../../Utils/ImageComponents';
import { formatNumberToPrice } from '../../Utils/normalizePrice';
import { SHIPPING_METHODS } from '../../Constants';

/*------------------------------------------------
//   CART COMPONENT
------------------------------------------------*/
const CardReview = (props) => {
  const { productsByStore } = props;

  /**
   * React LifeCycle Methods
   */
  return (
    <div className="products_list">
      {productsByStore.map((order) => {
        const physicalProperties = order.physical_properties;
        const isStandardShipping =
          physicalProperties.shipping_method_name === SHIPPING_METHODS.STANDARD.name;

        // Construct human-readable shipping date details depending on shipping method.
        let shippingDateLabel = `${Moment(physicalProperties.shipping_date).format(
          'D MMMM',
        )} ${physicalProperties.shipping_schedule.delivery_start} - ${
          physicalProperties.shipping_schedule.delivery_end
        }.`;
        if (isStandardShipping) {
          const fixedDate = Moment(physicalProperties.shipping_date);
          const estimatedDate = fixedDate.add('2', 'day');
          shippingDateLabel = `Entre el ${fixedDate.format(
            'D MMMM',
          )} - ${estimatedDate.format('D MMMM')}.`;
        }

        // Return Markup
        return (
          <div className="store-card" key={order.id}>
            <div className="store-detail">
              <div className="thumbnail">
                <Link className="link" to={`/stores/${order.store.slug}`}>
                  <img src={order.store.photo.small} alt="" />
                </Link>
              </div>
              <div className="store-detail__name">
                <Link className="link" to={`/stores/${order.store.slug}`}>
                  {order.store.name}
                </Link>
              </div>
              <div className="store-detail__quantity">
                Productos: {order.products.reduce((a, b) => b.quantity + a, 0)}
              </div>
            </div>

            {order.products.map((product) => (
              <div className="product-detail" key={product.product.slug}>
                <div className="thumbnail">
                  {product.product.photos.length > 0 && (
                    <Link
                      to={`/stores/${order.store.slug}/products/${product.product.slug}`}
                    >
                      <ResponsiveImage
                        src={product.product.photos[0].photo}
                        alt={product.product.name}
                      />
                    </Link>
                  )}
                </div>
                <div className="product-detail__box-detail">
                  <div className="product-detail__name">{product.product.name}</div>
                  {product.attribute && (
                    <div className="attributes">
                      {product.attribute_names &&
                        product.attribute_names
                          .split(',')
                          .map(
                            (item) =>
                              `${item.charAt(0).toUpperCase()}${item
                                .slice(1)
                                .toLowerCase()}`,
                          )
                          .join(', ')}
                    </div>
                  )}
                </div>
                <div className="product-detail__price">
                  <div className="price">
                    {formatNumberToPrice(
                      parseInt(product.quantity, 10) *
                        parseInt(product.unit_price, 10),
                    )}
                    MX
                  </div>
                  <div className="price__quantity">Cantidad: {product.quantity}</div>
                </div>
                {product.note ? (
                  <div className="note">
                    <span>Nota: </span>
                    {product.note}
                  </div>
                ) : null}
              </div>
            ))}

            <div className="delivery_details-container">
              <div className="shipping-schedules detail">
                <strong className="name icon_store-shipping">Entrega :</strong>
                <p className="shipping_date">{shippingDateLabel}</p>
              </div>
              <div className="shipping-schedules detail">
                <strong className="name icon_store-shipping">
                  Método de Envío:
                </strong>
                <p className="shipping_date">
                  {physicalProperties.shipping_method_name}
                </p>
              </div>
            </div>
          </div>
        );
      })}
    </div>
  );
};
export default CardReview;
