import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Field, reduxForm, formValueSelector } from 'redux-form';

import CreditCardForm from './CreditCardForm';
import { SHOPPING_CART_FORM_CONFIG } from '../../Utils/shoppingCart/shoppingCartFormConfig';
import {
  BANK_TRANSFER,
  PAYMENT_METHODS,
  ORDERS_EMAIL,
  BUTLER_PHONE,
  BUTLER_PHONE_PLAIN_FORMAT,
} from '../../Constants/config.constants';
import CreditCardSelector from '../ShoppingCart/CreditCardSelector';
import { getAddress } from '../../Reducers/users.reducer';

import { shoppingCartActions } from '../../Actions/shoppingCart.actions';
import { bindActionCreators } from 'redux';

// ICONS
const iconMastercard = require('../../../images/icons/payment/icon_mastercard.svg');
const iconVisa = require('../../../images/icons/payment/icon_visa.svg');
const iconAmex = require('../../../images/icons/payment/icon_amex.svg');
const iconOxxo = require('../../../images/icons/payment/icon_oxxo.svg');
const iconPaypal = require('../../../images/icons/payment/icon_paypal-checkout.svg');

// CONSTANTS
const PAYMENT_METHOD_MODEL = {
  type: null,
  name: null,
  isValid: false,
  data: null,
};

/**
 * MethodNotification : UI Component
 * Renders a UI notification when a payment method
 * changes the order delivery date.
 */
const MethodNotification = () => {
  return (
    <div className="method-notification">
      <div className="method-notification-container">
        <strong>La fecha de entrega ha sido modificada en tu(s) producto(s) </strong>
        al seleccionar la opción de pago con <strong>OXXO</strong>. Si no deseas que
        se modifique la fecha de entrega, selecciona otro método de pago.
      </div>
    </div>
  );
};

/**
 * PaymentMethodInput : UI (Form Input) Component
 * This is a custom component for redux-form.
 * Renders a 'native HTML input' component.
 */
const PaymentMethodInput = ({
  input: { onChange, ...input },
  paymentMethodDetails,
  onChangePaymentMethod,
  ...props
}) => (
  <input
    {...input}
    {...props}
    onChange={(e) => {
      onChange(e.target.value);
      onChangePaymentMethod(paymentMethodDetails);
    }}
  />
);

/*----------------------------------------------
   PaymentMethodSelector : UI component
----------------------------------------------*/
class PaymentMethodSelector extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showNotification: false,
    };
  }

  componentDidMount() {
    //check what is the paymentMethod according to redux state
    if (this.props.paymentMethod) {
      let paymentMethodDetails = {};
      switch (this.props.paymentMethod) {
        case PAYMENT_METHODS.OXXO.slug:
          paymentMethodDetails = {
            data: null,
            isValid: true,
            type: PAYMENT_METHODS.OXXO.slug,
            name: PAYMENT_METHODS.OXXO.name,
          };
          break;
        case PAYMENT_METHODS.BANK_TRANSFER.slug:
          paymentMethodDetails = {
            data: null,
            isValid: true,
            type: PAYMENT_METHODS.BANK_TRANSFER.slug,
            name: PAYMENT_METHODS.BANK_TRANSFER.name,
          };
          break;
        default:
          paymentMethodDetails = { ...PAYMENT_METHOD_MODEL };
      }
      this.onChangePaymentMethod(paymentMethodDetails);
    }
  }

  /*
   * On Payment Method Change
   * Set new payment method and update component state
   * @param {object} e : Event Object
   */
  onChangePaymentMethod = (paymentMethod) => {
    // Update Payment Method
    this.updatePaymentMethod(paymentMethod);
    this.props.onPaymentMethodChange(paymentMethod.type);

    // If payment method is 'oxxo',
    // Notify user of a change on her shopping cart details.
    if (paymentMethod.type === 'oxxo') {
      this.setState({
        showNotification: true,
      });

      window.setTimeout(() => {
        this.setState({
          showNotification: false,
        });
      }, 7000);
    } else {
      this.setState({
        showNotification: false,
      });
    }
  };

  /*
   * Update Payment Method
   * Updates payment method details on state & redux form
   * @param {object} paymentMethod : PaymenMethod Details
   */
  updatePaymentMethod = (paymentMethod) => {
    // Update Shopping Order Form
    this.props.change('paymentMethodDetails', {
      //...this.props.paymentMethodDetails,
      ...paymentMethod,
    });
  };

  /**
   * render()
   */
  render() {
    const { paymentMethod, handleSubmit, creditCards } = this.props;

    // Render Component
    return (
      <div className="payment">
        <form onSubmit={handleSubmit}>
          <div className="form">
            {/* CREDIT/DEBIT CARDS */}
            <div
              className={`
                                ${
                                  paymentMethod &&
                                  paymentMethod === PAYMENT_METHODS.MERCADO_PAGO.slug
                                    ? 'payment-choice--selected'
                                    : ''
                                }
                                payment-choice
                                creditCard`}
            >
              <div className="payment-choice__label">
                <Field
                  type="radio"
                  tabIndex="-1"
                  component={PaymentMethodInput}
                  className="radio dots"
                  name="paymentMethod"
                  value={PAYMENT_METHODS.MERCADO_PAGO.slug}
                  paymentMethodDetails={{
                    ...PAYMENT_METHOD_MODEL,
                    type: PAYMENT_METHODS.MERCADO_PAGO.slug,
                    name: PAYMENT_METHODS.MERCADO_PAGO.name,
                    cardProvider: null,
                  }}
                  onChangePaymentMethod={this.onChangePaymentMethod}
                  id="paymentMethod_card"
                />
                <label htmlFor="paymentMethod_card" className="dot logo">
                  Tarjeta de Cr&eacute;dito/D&eacute;bito
                </label>

                <div className="creditCard__logos">
                  <img
                    className="logo master-card"
                    src={iconMastercard}
                    alt="Master Card"
                  />
                  <img className="logo visa" src={iconVisa} alt="Visa" />
                  <img className="logo amex" src={iconAmex} alt="American Express" />
                </div>
              </div>

              {paymentMethod &&
                paymentMethod === PAYMENT_METHODS.MERCADO_PAGO.slug &&
                (creditCards.length ? (
                  <div className="form cardId__form creditCard-info">
                    <fieldset>
                      <CreditCardSelector
                        open={false}
                        creditCards={creditCards}
                        // onCreditCardChange={(e) => {
                        //   this.updatePaymentMethod({
                        //     cardProvider: null,
                        //     data: null,
                        //     secureForm: null,
                        //     isValid: false,
                        //     saveCreditCard: false,
                        //     customerId: null,
                        //     saveCreditCard: false,
                        //     isStoredCard: true,
                        //   });
                        // }}
                        onCreditCardChange={this.updatePaymentMethod}
                        onSecureNumberChange={this.updatePaymentMethod}
                        onInvalidCreditCard={this.onInvalidCreditCard}
                        onCreditCardUpdate={this.updatePaymentMethod}
                      />
                    </fieldset>
                  </div>
                ) : (
                  <CreditCardForm
                    onCreditCardUpdate={this.updatePaymentMethod}
                    onInvalidCreditCard={this.onInvalidCreditCard}
                  />
                ))}
            </div>

            {/* OXXO */}
            <div
              className={`
                                ${
                                  paymentMethod &&
                                  paymentMethod === PAYMENT_METHODS.OXXO.slug
                                    ? 'payment-choice--selected'
                                    : ''
                                }
                                payment-choice
                                oxxo`}
            >
              <div className="payment-choice__label">
                <Field
                  type="radio"
                  tabIndex="-1"
                  component={PaymentMethodInput}
                  className="radio dots"
                  name="paymentMethod"
                  value={PAYMENT_METHODS.OXXO.slug}
                  paymentMethodDetails={{
                    ...PAYMENT_METHOD_MODEL,
                    isValid: true,
                    type: PAYMENT_METHODS.OXXO.slug,
                    name: PAYMENT_METHODS.OXXO.name,
                  }}
                  onChangePaymentMethod={this.onChangePaymentMethod}
                  id="paymentMethod_oxxo"
                />
                <label htmlFor="paymentMethod_oxxo" className="dot logo">
                  OXXO
                </label>

                <div className="logo-container">
                  <img className="logo oxxo" src={iconOxxo} alt="OXXO" />
                </div>
              </div>

              {paymentMethod && paymentMethod === PAYMENT_METHODS.OXXO.slug && (
                <div className="bankTransfer-info">
                  <p className="note">
                    Al finalizar la compra, recibirás un código que debes presentar
                    al personal de OXXO.
                    <br />
                    Realiza tu pago en cualquier tienda OXXO&nbsp; dentro del{' '}
                    <span className="bold">transcurso máximo de 24 horas</span>
                    <br />
                    Recuerda enviarnos tu comprobante a
                    <a href="mailto:`${ORDERS_EMAIL}`"> {ORDERS_EMAIL}</a>.
                  </p>
                  <p className="note--small">
                    * Las fechas de entrega pueden variar
                    <br />
                    dependiendo del momento en que confirmemos tu pago.
                  </p>
                </div>
              )}
            </div>

            {/* BANK TRANSFER */}
            {/* <div
              className={`
                                ${
                                  paymentMethod &&
                                  paymentMethod ===
                                    PAYMENT_METHODS.BANK_TRANSFER.slug
                                    ? 'payment-choice--selected'
                                    : ''
                                }
                                payment-choice
                                bankTransfer`}
            >
              <div className="payment-choice__label">
                <Field
                  type="radio"
                  tabIndex="-1"
                  component={PaymentMethodInput}
                  className="radio dots"
                  name="paymentMethod"
                  value={PAYMENT_METHODS.BANK_TRANSFER.slug}
                  paymentMethodDetails={{
                    ...PAYMENT_METHOD_MODEL,
                    isValid: true,
                    type: PAYMENT_METHODS.BANK_TRANSFER.slug,
                    name: PAYMENT_METHODS.BANK_TRANSFER.name,
                  }}
                  onChangePaymentMethod={this.onChangePaymentMethod}
                  id="paymentMethod_bankTransfer"
                />
                <label htmlFor="paymentMethod_bankTransfer" className="dot">
                  Transferencia Bancaria
                </label>
              </div>
            </div> */}

            {/* PAYPAL */}
            <div
              className={`
                                ${
                                  paymentMethod &&
                                  paymentMethod === PAYMENT_METHODS.PAYPAL.slug
                                    ? 'payment-choice--selected'
                                    : ''
                                }
                                payment-choice
                                payPal`}
            >
              <div className="payment-choice__label">
                <Field
                  type="radio"
                  tabIndex="-1"
                  component={PaymentMethodInput}
                  className="radio dots"
                  name="paymentMethod"
                  value={PAYMENT_METHODS.PAYPAL.slug}
                  paymentMethodDetails={{
                    ...PAYMENT_METHOD_MODEL,
                    isValid: true,
                    type: PAYMENT_METHODS.PAYPAL.slug,
                    name: PAYMENT_METHODS.PAYPAL.name,
                  }}
                  onChangePaymentMethod={this.onChangePaymentMethod}
                  id="paymentMethod_payPal"
                />
                <label htmlFor="paymentMethod_payPal" className="dot logo">
                  PayPal
                </label>

                <div className="creditCard__logos">
                  <img
                    className="logo pay-pal"
                    src={iconPaypal}
                    alt={PAYMENT_METHODS.PAYPAL.name}
                  />
                </div>
              </div>
            </div>

            {/* OTROS */}
            {/*
                        <div className="payment-choice payment-choice--disabled">
                            <div className="payment-choice__label">
                                <Field
                                    disabled
                                    type="radio"
                                    tabIndex="-1"
                                    component={PaymentMethodInput}
                                    className="radio dots"
                                    name="paymentMethod"
                                    value={PAYMENT_METHODS.OTROS.slug}
                                    paymentMethodDetails={{
                                        ...PAYMENT_METHOD_MODEL,
                                        type: PAYMENT_METHODS.OTROS.slug,
                                        name: PAYMENT_METHODS.OTROS.name
                                    }}
                                    onChangePaymentMethod={this.onChangePaymentMethod}
                                    id="paymentMethod_online"
                                />
                                <label htmlFor="paymentMethod_online" className="dot">
                                    Otros Medios de Pago (Pr&oacute;ximamente)
                                </label>
                            </div>
                        </div>
                        */}
          </div>

          {/*
                    <div className='invoice_note'>
                        <Field
                            id='invoice'
                            className='checkmark'
                            name='requiresInvoice'
                            component='input'
                            type='checkbox'
                            value='invoice'
                        />
                        <label htmlFor='invoice' className='checkmark'>
                            Requiero Factura
                            <span className='note'>
                                Una vez finalizado el proceso de compra, envía tus datos
                                fiscales junto con tu número de orden al correo{' '}
                                <a href='mailto:facturacion@canastarosa.com'>
                                facturacion@canastarosa.com
                                </a>
                            </span>
                        </label>
                    </div> */}
        </form>

        {this.state.showNotification && <MethodNotification></MethodNotification>}
      </div>
    );
  }
}

// Wrap component within reduxForm
const formName = SHOPPING_CART_FORM_CONFIG.formName;
PaymentMethodSelector = reduxForm({
  ...SHOPPING_CART_FORM_CONFIG.config,
  //onSubmit: paymentSubmit
})(PaymentMethodSelector);

// Pass Redux state and actions to component
const selector = formValueSelector(formName);
function mapStateToProps(state) {
  // Define or retrieve default form values
  const userCreditCards = state.users.creditCards.creditCards;
  const defaultCreditCard = userCreditCards[0];
  const paymentMethod = selector(state, 'paymentMethod') || undefined;
  const paymentMethodDetails = selector(state, 'paymentMethodDetails') || undefined;
  const requiresInvoice = selector(state, 'requiresInvoice') || undefined;
  const selectedCreditCard = selector(state, 'selectedCreditCard') || undefined;
  const initialValues = {
    paymentMethod: null,
    paymentMethodDetails: null,
    requiresInvoice: false,
    selectedCreditCard: defaultCreditCard ? defaultCreditCard.card_id : null,
  };

  // Return Props
  return {
    creditCards: userCreditCards,
    selectedCreditCard,
    addressBilling: selector(state, 'addressBilling'),
    shippingMethod: selector(state, 'shippingMethod'),
    paymentMethod,
    paymentMethodDetails,
    customer_id: defaultCreditCard ? defaultCreditCard.customer.customer_id : null,
    initialValues: {
      ...(!paymentMethod ? { paymentMethod: initialValues.paymentMethod } : {}),
      ...(!requiresInvoice
        ? { requiresInvoice: initialValues.requiresInvoice }
        : {}),
      ...(!selectedCreditCard
        ? { selectedCreditCard: initialValues.selectedCreditCard }
        : {}),
    },
    addresses: state.users.addresses.addresses.map((address) => getAddress(address)),
  };
}

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
    ...bindActionCreators({}, dispatch),
  };
}

// Export Component
export default connect(mapStateToProps, mapDispatchToProps)(PaymentMethodSelector);
