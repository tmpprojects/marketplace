import React from 'react';
import { Field } from 'redux-form';
import { ItemCreditCardList } from './CreditCardSelector';

export default function CardList(props) {
  const { CreditCards, onCreditCardChange } = props;

  return (
    <div className="credit-cards-list-container" styles={{ width: '100%' }}>
      <label className="cvv-confirmation__label" htmlFor="selectedCreditCard">
        Elige una tarjeta
      </label>

      <div className="credit-cards-list">
        {CreditCards.map((creditCard) => (
          <Field
            type="radio"
            id={`creditCard_${creditCard.card_id}`}
            name="selectedCreditCard"
            creditCard={creditCard}
            key={creditCard.card_id}
            value={creditCard.card_id}
            onChange={onCreditCardChange}
            component={ItemCreditCardList}
          />
        ))}
      </div>
    </div>
  );
}
