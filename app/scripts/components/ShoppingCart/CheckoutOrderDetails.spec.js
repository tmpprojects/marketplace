import React from 'react';
import { shallow } from 'enzyme';

import { CheckoutOrderDetails } from './CheckoutOrderDetails';
import CollapseBox from '../../Utils/CollapseBox';

// CONSTANTS
const NAV_SECTION = {
  SHIPPING: 'SHIPPING',
  PAYMENT: 'PAYMENT',
};

//
describe('Checkout Order Details', () => {
  let wrapper;

  // Setup
  beforeAll(() => {
    const props = {
      syncErrors: {},
      clientData: {},
      selectedPaymentMethod: '',
      selectedShippingAddress: '',
      productsByStore: [],
      onShippingAddressChange: (e) => {},
      onPaymentMethodChange: (e) => {},
      // store: {
      //     getState: jest.fn(),
      //     subscribe: jest.fn(),
      //     dispatch: jest.fn()
      // }
    };
    wrapper = shallow(<CheckoutOrderDetails {...props} />);
    wrapper.setState({
      sectionStatus: {
        [NAV_SECTION.SHIPPING]: {
          section: NAV_SECTION.SHIPPING,
          isOpen: true,
        },
        [NAV_SECTION.PAYMENT]: {
          section: NAV_SECTION.PAYMENT,
          isOpen: true,
        },
      },
    });
  });

  // Component Creation
  it('should be defined correctly', () => {
    expect(wrapper).toBeDefined();
    //expect(component).toMatchSnapshot();
    // expect(container.firstChild.classList.contains('foo')).toBe(true)
  });
  it('should snapshot correctly', () => {
    expect(wrapper).toMatchSnapshot();
  });

  it('should change tab navigation to payment', () => {
    const sectionTemplate = {
      section: NAV_SECTION.PAYMENT,
      isOpen: true,
    };
    const instance = wrapper.instance();
    instance.onTabChanged(sectionTemplate);
    expect(wrapper.state('sectionStatus')[NAV_SECTION.PAYMENT]).toEqual(
      sectionTemplate,
    );
  });
  it('should change tab navigation to shipping', () => {
    const sectionTemplate = {
      section: NAV_SECTION.SHIPPING,
      isOpen: true,
    };

    //const testing = wrapper.find(CollapseBox).at(0).dive();
    //testing.find('.header').first().simulate('click');
    const instance = wrapper.instance();
    instance.onTabChanged(sectionTemplate);
    expect(wrapper.state('sectionStatus')[NAV_SECTION.SHIPPING]).toEqual(
      sectionTemplate,
    );
  });

  it('should unmount correctly', () => {
    expect(wrapper.unmount()).toBeTruthy;
  });
});
