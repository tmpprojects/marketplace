import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Field, reduxForm, formValueSelector } from 'redux-form';
import { allProductsInCartHaveWithDiscount } from '../../Reducers';

import { ActionButton } from '../../Utils/ActionButton';
import { SHOPPING_CART_FORM_CONFIG } from '../../Utils/shoppingCart/shoppingCartFormConfig';
import { list } from './listStores';
import { default as reduxActions } from 'redux-form/lib/actions';
const { change } = reduxActions;
import { shoppingCartActions } from '../../Actions';
import { chooseBuenFinCoupon } from '../../Utils/dateUtils';
class PromoCode extends Component {
  constructor(props) {
    super(props);
    this.state = {
      localReset: false,
      // //autofill: this.props.coupon === null ? true : false,
      // autofill: true,
    };
  }
  /**
   * When a slug found on the cart, it will call
   * the function to validate the coupon.
   */

  componentDidMount() {
    if (this.props.isValid === true || this.props.isValid === false) {
      this.setState({ localReset: true });
      this.executeOnVerifyCoupon();
    } else {
      this.setState({ localReset: false });
    }

    let couponName = chooseBuenFinCoupon().discountToApply;

    if (
      this.props.automaticCouponEnable &&
      this.props.cartTotal >= 299
      //COMMENTED FOR BUEN FIN
      // &&
      //this.props.foundSlug &&
      //when there is Total Restriction in the cart.
      //this.props.cartTotal >= 450
    ) {
      if (
        couponName === 'HORAROSABF' &&
        this.props.allProductsInCartHaveWithDiscount
      ) {
        console.log('didmount HoraRosa');
        //IF ADDED FOR BUEN FIN
        this.props.change('discount_code', `${couponName}`);
        this.executeOnVerifyCoupon();
      } else if (
        ((couponName === 'FSBFCM' || couponName === 'COMADRE2') &&
          this.props.cartTotal >= 299) ||
        (couponName === 'MOMSTER2' &&
          this.props.cartTotal >= 450 &&
          this.props.foundSlug)
      ) {
        this.props.change('discount_code', `${couponName}`);
        this.executeOnVerifyCoupon();
      } else {
        //console.log('clean!');
        setTimeout(() => {
          this.cleanField();
        }, 400);
      }
    }
  }
  componentWillReceiveProps(nextProps) {
    if (nextProps.isValid === true || nextProps.isValid === false) {
      this.setState({ localReset: true });
    } else {
      this.setState({ localReset: false });
    }
    //COMMENTED FOR BUEN FIN
    if (this.props !== nextProps) {
      if (
        //   this.props.foundSlug !== nextProps.foundSlug ||
        this.props.cartTotal !== nextProps.cartTotal
      ) {
        let couponName = chooseBuenFinCoupon().discountToApply;
        if (
          ((couponName === 'FSBFCM' || couponName === 'COMADRE2') &&
            this.props.automaticCouponEnable &&
            nextProps.cartTotal >= 299) ||
          (couponName === 'MOMSTER2' &&
            this.props.automaticCouponEnable &&
            nextProps.cartTotal >= 450 &&
            nextProps.foundSlug !== true)
          //COMMENTED FOR BUEN FIN
          //&&
          // nextProps.foundSlug &&
          //when there is Total Restriction in the cart.
          //nextProps.cartTotal >= 450
        ) {
          console.log('there are conditions to apply cpupon in will receive');
          if (
            couponName === 'HORAROSABF' &&
            nextProps.allProductsInCartHaveWithDiscount
          ) {
            //IF ADDED FOR BUEN FIN
            this.props.change('discount_code', `${couponName}`);
            this.executeOnVerifyCoupon();
          } else if (
            ((couponName === 'FSBFCM' || couponName === 'COMADRE2') &&
              nextProps.cartTotal >= 299) ||
            (couponName === 'MOMSTER2' &&
              nextProps.cartTotal >= 450 &&
              nextProps.foundSlug !== true)
          ) {
            console.log('there are conditons to appply inside if');
            this.props.change('discount_code', `${couponName}`);
            this.executeOnVerifyCoupon();
          } else {
           // console.log('clean!');
            setTimeout(() => {
              this.cleanField();
            }, 400);
          }
        }
        //COMMENTED FOR BUEN FIN
        else {
          this.cleanField();
        }
        //COMMENTED FOR BUEN FIN
        //}
      }
    }
  }

  executeOnVerifyCoupon = () => {
    setTimeout(() => {
      this.props.onVerifyCoupon();
    }, 200);
  };

  /*
   * cleanField()
   * Set Coupon related local state  and ShoppingCartaPage coupon related state to initial values
   */
  cleanField = () => {
    this.setState({ localReset: false });
    //envolve in set time for sincronicity problems
    setTimeout(() => {
      this.props.change('discount_code', '');
      this.props.cleanCouponData();
      this.props.cleanCouponStateFromShoppingCart();
    }, 200);
  };

  render() {
    const calculateClassName = (cuponValid) => {
      if (cuponValid === true) {
        return 'valid';
      } else if (cuponValid === false) {
        return 'invalid';
      } else {
        return 'idle';
      }
    };
   // console.log('automaticCouponEnable', this.props.automaticCouponEnable);
    return (
      <React.Fragment>
        <div className="promo-code__container">
          <Field
            className={`promo-code__input ${calculateClassName(this.props.isValid)}`}
            id={'promo-code__input'}
            type="text"
            component="input"
            disabled={
              (this.props.discountCodeState === 'valid' ||
                this.props.discountCodeState === 'invalid') &&
              this.state.localReset
            }
            name="discount_code"
            onChange={this.props.onCouponChange}
            placeholder="¿Tienes un cupón?"
            onFocus={(e) => {
              e.target.classList.add('focus');
              e.target.classList.remove('blur');
            }}
            onBlur={(e) => {
              e.target.classList.add('blur');
              e.target.classList.remove('focus');
            }}
          />

          {!this.state.localReset && (
            <ActionButton
              disabled={this.props.isCouponButtonDisabled}
              onClick={() => {
                this.props.onVerifyCoupon();
                this.setState({ localReset: true });
              }}
              className="promo-code__button"
            >
              Validar
            </ActionButton>
          )}

          {this.state.localReset && (
            <button
              className={'promo-code__button--clean'}
              onClick={() => {
                this.cleanField();
                this.props.setAutofillCouponToFalse();
              }}
            >
              X
            </button>
          )}
        </div>
        {this.props.promoCodeStatusMessage && (
          <div
            className={`promo-code__status promo-code__status--${this.props.discountCodeState}`}
          >
            {this.props.promoCodeStatusMessage}
          </div>
        )}
      </React.Fragment>
    );
  }
}
// Wrap component within reduxForm
PromoCode = reduxForm({
  ...SHOPPING_CART_FORM_CONFIG.config,
})(PromoCode);

function mapStateToProps(state, ownProps) {
  const selector = formValueSelector(SHOPPING_CART_FORM_CONFIG.formName);
  const discountCode = state.cart.couponDetails.valid
    ? state.cart.coupon
    : selector(state, 'discount_code');

  return {
    initialValues: {
      discount_code: discountCode,
    },
    isValid: state.cart.couponDetails.valid,
    coupon: state.cart.coupon,
    automaticCouponEnable: state.cart.automaticCouponEnable,
    allProductsInCartHaveWithDiscount: allProductsInCartHaveWithDiscount(state),
  };
}

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators(
    {
      change,
      cleanCouponData: shoppingCartActions.cleanCouponData,
      setAutofillCouponToFalse: shoppingCartActions.setAutofillCouponToFalse,
    },
    dispatch,
  );
};

export default connect(mapStateToProps, mapDispatchToProps)(PromoCode);
