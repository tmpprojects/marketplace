import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { reduxForm, formValueSelector, SubmissionError } from 'redux-form';
import hash from 'object-hash';

import { userRegisterErrorTypes } from '../../Constants/user.constants';
import { getUIErrorMessage } from '../../Utils/errorUtils';
import AddressManager from '../../Utils/AddressManager.jsx';
import { SHIPPING_METHODS } from '../../Constants/config.constants';
import { getAddress, getUserProfile } from '../../Reducers/users.reducer';
import {
  getUnavailableProducts,
  getCartProductsByStoreAndShipping,
} from '../../Reducers/shoppingCart.reducer';
import { modalBoxActions, userActions, statusWindowActions } from '../../Actions';
import ShippingContact from './ShippingContact';
import ShippingAddressSelector from './ShippingAddressSelector';
import SideBarProducts from './SideBarProducts';
import { SHOPPING_CART_FORM_CONFIG } from '../../Utils/shoppingCart/shoppingCartFormConfig';

/*
 * Form Validation Function
 * @param {object} values : Form values
 */
const streetValidation = (value) =>
  value && value.length < 5 ? 'Escribe tu dirección completa.' : undefined;
const neighborhoodValidation = (value) =>
  value && value.length < 5
    ? 'Escribe el nombre completo de tu colonia.'
    : undefined;
const cityValidation = (value) =>
  value && value.length < 5
    ? 'Escribe el nombre de tu delegación/municipio.'
    : undefined;
const postalValidation = (value) =>
  value && value.length !== 5 ? 'Escribe tu código postal.' : undefined;

const addressClientNameValidation = (value, allValues) => {
  //
  if (allValues.shippingMethod === SHIPPING_METHODS.PICKUP_POINT.slug) {
    return undefined;
  }
  return !value || value === '' || value.length < 2
    ? 'Este campo es obligatorio'
    : undefined;
};
const addressPhoneValidation = (value, allValues) => {
  //
  if (allValues.shippingMethod === SHIPPING_METHODS.PICKUP_POINT.slug) {
    return undefined;
  }
  return !value || value === '' || value.length < 2
    ? 'Este campo es obligatorio'
    : undefined;
};
const addressStreetValidation = (value, allValues) => {
  //
  if (allValues.shippingMethod === SHIPPING_METHODS.PICKUP_POINT.slug) {
    return undefined;
  }
  return streetValidation(value);
};
const addressNeighborhoodValidation = (value, allValues) => {
  //
  if (allValues.shippingMethod === SHIPPING_METHODS.PICKUP_POINT.slug) {
    return undefined;
  }
  return neighborhoodValidation(value);
};
const addressCityValidation = (value, allValues) => {
  //
  if (allValues.shippingMethod === SHIPPING_METHODS.PICKUP_POINT.slug) {
    return undefined;
  }
  return cityValidation(value);
};
const addressZipValidation = (value, allValues) => {
  //
  if (allValues.shippingMethod === SHIPPING_METHODS.PICKUP_POINT.slug) {
    return undefined;
  }
  return postalValidation(value);
};

/*----------------------------------------------
    ShippingDetailsManager : UI Component
----------------------------------------------*/
class ShippingDetailsManager extends Component {
  /*
   * ON ADD ADDRESS
   * Callback when a user creates a new address.
   * @param {object} formValues : New Address Object
   */
  onAddAddress = (formValues) => {
    if (!this.props.isGuest) {
      // Create Address
      return this.props
        .addAddress(formValues)
        .then((response) => {
          // Update selected address reference
          this.props.change('selectedShippingAddress', response.data.uuid);
          this.props.onShippingAddressChange(response.data);

          // close modal window
          this.props.closeModalBox();
        })
        .catch((error) => {
          const uiMessage = getUIErrorMessage(
            error.response,
            userRegisterErrorTypes,
          );
          throw new SubmissionError({
            _error: uiMessage,
          });
        });
    } else {
      const address = { ...formValues, uuid: hash(formValues) };
      return this.props.addGuestAddress(address).then((response) => {
        // TODO: Refactor this
        this.props.change('selectedShippingAddress', response.uuid);
        this.props
          .onShippingAddressChange(response, this.props.isGuest)
          .then(() => {
            this.props.openStatusWindow({
              type: 'success',
              message: 'La direccion se actualizó correctamente.',
            });
          })
          .catch(() => {
            this.props.openStatusWindow({
              type: 'error',
              message: 'Error. Por favor, intenta de nuevo.',
            });
          });

        // close modal window
        this.props.closeModalBox();
      });
    }
  };

  /*
   * ON EDIT CONTACT INFO
   * Update User Information on DB.
   * @param {object} clientInfo : redux form clientData object
   */
  onEditContactInfo = (contactInfo) => {
    return this.props.updateUser({
      mobile: contactInfo.phone,
    });
  };

  /*
   * ON UPDATE ADDRESS
   * Callback when a user updates an address.
   * @param {object} address : Form Values
   */
  onUpdateAddress = (address) => {
    // Close Modalbox
    this.props.closeModalBox();
    // Submit Form
    this.submitForm(address, this.props.isGuest);
  };

  /*
   * ON DELETE ADDRESS
   * Delete a User Address
   * @param {string} addressId : Address Id
   */
  onDeleteAddress = (addressId) => {
    this.props
      .deleteAddress(addressId)
      .then((response) => {
        this.props.openStatusWindow({
          type: 'success',
          message: 'La direccion se eliminó correctamente.',
        });
        //
      })
      .catch((error) => {
        this.props.openStatusWindow({
          type: 'error',
          message: 'No puedes eliminar la dirección seleccionada por defecto.',
        });
      });
  };

  /*
   * ON ADDRESS CHANGE
   * Make selected value the default address
   * @param {object} address : Current field Address
   */
  onAddressChange = (address) => {
    // Target Address
    this.props.onShippingAddressChange(address);
  };

  /*
   * OPEN CREATE ADDRESS WINDOW
   * Opens a modal box with a 'new address' form.
   * @param {object} formValues : New Address Object
   */
  openAddressWindow = () => {
    this.props.openModalBox(() => (
      <AddressManager onSubmit={this.onAddAddress} title="Nueva Dirección" />
    ));
  };

  /*
   * SUBMIT FORM
   * Submit Addresses form
   * @param {object} values : Form Values
   */
  submitForm = (values, isGuest = false) => {
    if (!isGuest) {
      return this.props
        .updateAddress(values)
        .then((response) => {
          this.props.openStatusWindow({
            type: 'success',
            message: 'La direccion se actualizó correctamente.',
          });
          this.onAddressChange(values);
          //
        })
        .catch((error) => {
          this.props.openStatusWindow({
            type: 'error',
            message: 'Error. Por favor, intenta de nuevo.',
          });
        });
    } else {
      // console.log('isguest from submitForm in ShippingDetailsManger');
      this.props.deleteGuestAddress();
      const address = { ...values, uuid: hash(values) };
      this.props
        .addGuestAddress(address)
        .then((response) => {
          this.props.openStatusWindow({
            type: 'success',
            message: 'La dirección se actualizó correctamente.',
          });
        })
        .catch((error) => {
          console.log('error adding adress', error);
          this.props.openStatusWindow({
            type: 'error',
            message: 'Error. Por favor, intenta de nuevo.',
          });
        });
    }
  };

  /*
   * React Component Cycle Methods
   */
  render() {
    const { addresses, addressShipping, clientData, gift } = this.props;
    let { unavailableProducts } = this.props;
    unavailableProducts = unavailableProducts.reduce((a, b) => {
      let returnValue = a;
      const storeIndex = returnValue.findIndex((s) => s.id);
      if (storeIndex !== -1) {
        returnValue = [
          ...a.slice(0, storeIndex),
          {
            ...a[storeIndex],
            products: [...a[storeIndex].products, b],
          },
          ...a.slice(storeIndex + 1),
        ];
      } else {
        returnValue = [
          ...a,
          {
            store: b.product.store,
            products: [b],
          },
        ];
      }
      return returnValue;
    }, []);

    const hasPickupPoint = this.props.selectedShippingMethods.find(
      (s) => s.slug === SHIPPING_METHODS.PICKUP_POINT.slug,
    );

    return (
      <div className="shipping">
        <ShippingContact
          clientData={clientData}
          isGuest={this.props.isGuest}
          gift={gift}
          onEditContactInfo={this.onEditContactInfo}
          renderFormView={!this.props.clientInfoCompleted}
        />

        {!hasPickupPoint && (
          <ShippingAddressSelector
            addresses={addresses}
            //requiresAddress={false}
            defaultAddress={addressShipping}
            openAddressWindow={this.openAddressWindow}
            onAddressChange={this.onAddressChange}
            onDeleteAddress={this.onDeleteAddress}
            onUpdateAddress={this.onUpdateAddress}
            isLogged={this.props.isLogged}
          />
        )}
        {/*
                {unavailableProducts.length > 0 && (
                    <div className="module">
                        <div className="module__header">
                            <h5 className="module__title">Productos no disponibles</h5>
                        </div>
                        <div className="module__info product_unavailable">
                            <SideBarProducts productsByStore={unavailableProducts} />
                        </div>  
                    </div>
                )}
                */}
      </div>
    );
  }
}

// Wrap component within reduxForm
ShippingDetailsManager = reduxForm({
  ...SHOPPING_CART_FORM_CONFIG.config,
})(ShippingDetailsManager);

// Pass Redux state and actions to component
const selector = formValueSelector(SHOPPING_CART_FORM_CONFIG.formName);
function mapStateToProps(state) {
  const { users, app } = state;
  const shippingMethods = selector(state, 'orders').reduce((a, o) => {
    const selectedShippingMethod = o.physical_properties.selected_shipping_method;
    if (selectedShippingMethod) {
      return [...a, selectedShippingMethod];
    }
    return a;
  }, []);
  const defaultAddress = users.addresses.addresses.find((address, index, list) => {
    // If the user has choosen an address from the UI address list
    if (address.default_address) {
      return address.default_address;

      // If the address is neither, the 'selected' or the 'default' one,
      // choose the last address of the list.
    } else if (index === list.length - 1 && !address.default_address) {
      return true;
    }

    //
    return false;
  });

  // Define or retrieve default form values// Define or retrieve default form values
  let orders = selector(state, 'orders');
  const selectedShippingAddress =
    selector(state, 'selectedShippingAddress') || undefined;
  const clientData = selector(state, 'clientData') || undefined;
  const gift = selector(state, 'gift') || undefined;
  const addressShipping = selector(state, 'addressShipping') || undefined;
  const addressBilling = selector(state, 'addressBilling') || undefined;
  const initialValues = {
    clientData: {
      phone: getUserProfile(state).mobile,
      email: getUserProfile(state).email,
      client_name: getUserProfile(state).first_name,
      client_lastName: getUserProfile(state).last_name,
    },
    gift: false,
    giftReceiver: {
      phone: null,
      name: null,
      note: null,
    },
    selectedShippingAddress: defaultAddress ? defaultAddress.uuid : null,
    addressShipping: defaultAddress ? getAddress(defaultAddress) : null,
    addressBilling: defaultAddress ? getAddress(defaultAddress) : null,
  };

  // Return Props
  return {
    products: selector(state, 'products') || [],
    isGuest: users.isGuest,
    isLogged: users.isLogged,
    addresses: users.addresses.addresses.map((address) => getAddress(address)),
    shippingMethods: app.shippingMethods,
    selectedShippingMethods: shippingMethods,
    unavailableProducts: getUnavailableProducts(state),
    selectedShippingAddress,
    addressShipping,
    clientData,
    gift,
    initialValues: {
      ...(orders ? { orders: orders } : {}),
      ...(!clientData ? { clientData: initialValues.clientData } : {}),
      ...(!gift ? { gift: initialValues.gift } : {}),
      ...(!gift ? { giftReceiver: initialValues.giftReceiver } : {}),
      ...(!selectedShippingAddress
        ? {
            selectedShippingAddress: initialValues.selectedShippingAddress,
          }
        : {}),
      ...(!addressShipping
        ? { addressShipping: initialValues.addressShipping }
        : {}),
      ...(!addressBilling ? { addressBilling: initialValues.addressBilling } : {}),
    },
  };
}
function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      addAddress: userActions.addAddress,
      addGuestAddress: userActions.addGuestAddress,
      deleteGuestAddress: userActions.deleteGuestAddress,
      openModalBox: modalBoxActions.open,
      closeModalBox: modalBoxActions.close,
      deleteAddress: userActions.deleteAddress,
      updateAddress: userActions.updateAddress,
      updateUser: userActions.updateUserInfo,
      openStatusWindow: statusWindowActions.open,
    },
    dispatch,
  );
}

// Export Component
export default connect(mapStateToProps, mapDispatchToProps)(ShippingDetailsManager);
