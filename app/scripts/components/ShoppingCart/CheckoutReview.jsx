import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Fields, reduxForm, formValueSelector } from 'redux-form';
import PropTypes from 'prop-types';

import CartProduct from './CartProduct';
import { googleMapsID } from '../../../config';
import { getAddress } from '../../Reducers/users.reducer';
import { confirmationSubmit } from './validateShoppingForm';
import { PAYMENT_METHODS, SHIPPING_METHODS } from '../../Constants/config.constants';
import { SHOPPING_CART_FORM_CONFIG } from '../../Utils/shoppingCart/shoppingCartFormConfig';

import CardReview from './CardReview';

/*------------------------------------------------
//    CART PRODUCTS LIST COMPONENT
------------------------------------------------*/
const CartProductsList = ({
  fields,
  meta: { error, submitFailed, form },
  ...props
}) => (
  <ul>
    {fields.map((product, index) => (
      <Fields
        component={CartProduct}
        names={[
          `${product}.note`,
          `${product}.price`,
          `${product}.quantity`,
          `${product}.physical_properties.shipping_date`,
          `${product}.physical_properties.shipping_schedule`,
        ]}
        key={`${product}.product.slug`}
        product={fields.get(index)}
        slice={`${product}`}
        removeProduct={(e) => {
          props.removeProduct(fields.get(index));
          fields.remove(index);
        }}
        updateCartProduct={(e) => props.updateCartProduct(fields.get(index))}
      />
    ))}
  </ul>
);

////////////////////////////////////////////
let CheckoutReview = (props) => {
  const {
    clientData,
    paymentMethod,
    shippingMethod,
    addressShipping,
    onRemoveProduct,
    onUpdateProduct,
    requiresInvoice,
  } = props;

  return (
    <div className="review">
      <h4 className="module__title">Confirma tu orden</h4>
      <div className="cart_container review_container">
        <div className="shipping">
          <div className="locations_container">
            <div className="personal-information">
              <p className="title">Información Personal:</p>
              {clientData.client_name} {clientData.client_lastName}
              <br />
              {clientData.email} <br />
              {clientData.phone}
            </div>

            {shippingMethod !== SHIPPING_METHODS.EXPRESS.slug ||
              (shippingMethod !== SHIPPING_METHODS.STANDARD.slug && (
                <div className="location">
                  <p className="title">Dirección de Envío</p>
                  <div className="address">
                    <div className="map_container">
                      {addressShipping.latitude && addressShipping.longitude ? (
                        <img
                          alt=""
                          src={`https://maps.googleapis.com/maps/api/staticmap?center=${addressShipping.latitude},${addressShipping.longitude}&zoom=16&scale=2&size=112x112&markers=size:small%7Ccolor:0xff0000%7Clabel:1%7C${addressShipping.latitude},${addressShipping.longitude}&key=${googleMapsID}`}
                        />
                      ) : (
                        <img
                          alt=""
                          src={require('../../../images/utils/map-location-placeholder.svg')}
                        />
                      )}
                    </div>
                    <div className="info">
                      <strong style={{ fontWeight: 'bold' }}>
                        {addressShipping.address_name}
                      </strong>{' '}
                      <br />
                      <span>
                        {addressShipping.street_address} {addressShipping.num_ext},
                        {addressShipping.num_int
                          ? `${addressShipping.num_int},`
                          : ''}{' '}
                        {addressShipping.neighborhood},{addressShipping.city},{' '}
                        {addressShipping.zip_code}.
                      </span>
                    </div>
                  </div>

                  {/* <div className="address">
                                    <p className="title">Dirección de Facturación</p>
                                    <strong style={{fontWeight: 'bold'}}>{addressBilling.address_name}</strong><br/>
                                    {clientData.client_name}<br/>
                                    {addressBilling.street_address}, {addressBilling.neighborhood},<br/>
                                    {addressBilling.city}, {addressBilling.zip_code}, {addressBilling.state_name}
                                </div> */}
                </div>
              ))}

            <div className="delivery">
              <div className="type">
                <p className="title">Tipo de Entrega: </p>
                {shippingMethod === SHIPPING_METHODS.PICKUP_POINT.slug ? (
                  <div>
                    <strong>Recoger en Canasta Rosa</strong>
                    <p className="address_canastaRosa">
                      Montes Urales 455, Piso 1, Col. Lomas de Chapultepec III
                      Sección, C.P. 11000, Delg. Miguel Hidalgo, CDMX.
                      <strong className="strong">
                        {' '}
                        Horario de Lunes a Viernes de 9:00 am a 6:00 pm.
                      </strong>
                    </p>
                  </div>
                ) : (
                  <strong className="provider_name">Express</strong>
                )}
              </div>
              <div className="pay">
                <p className="title">Forma de pago: </p>
                {paymentMethod.type !== PAYMENT_METHODS.BANK_TRANSFER.slug ? (
                  <div
                    className={`
                                            card-provider
                                            ${
                                              paymentMethod.cardProvider ||
                                              paymentMethod.name
                                            }
                                        `}
                  >
                    {paymentMethod.cardProvider || paymentMethod.name}
                  </div>
                ) : (
                  <div className={'bank-transfer'}>Transferencia Bancaria</div>
                )}
              </div>
            </div>
            {requiresInvoice === true ? (
              <div className="invoice">
                <p className="title">Si requiero Factura</p>
              </div>
            ) : null}
          </div>
          <h4 className="module__title">Resumen</h4>
          <CardReview productsByStore={this.props.order.orders} />
        </div>
      </div>
    </div>
  );
};

// Define PropTypes
CheckoutReview.propTypes = {
  //products: PropTypes.array.isRequired,
  onRemoveProduct: PropTypes.func.isRequired,
  onUpdateProduct: PropTypes.func.isRequired,
};

// Wrap component within reduxForm
CheckoutReview = reduxForm({
  ...SHOPPING_CART_FORM_CONFIG.config,
  //onSubmit: confirmationSubmit,
  //keepDirtyOnReinitialize: true
})(CheckoutReview);

// Pass Redux state and actions to component
const selector = formValueSelector(SHOPPING_CART_FORM_CONFIG.formName);
function mapStateToProps(state) {
  return {
    clientData: selector(state, 'clientData'),
    paymentMethod: selector(state, 'paymentMethod'),
    shippingMethod: selector(state, 'shippingMethod'),
    requiresInvoice: selector(state, 'requiresInvoice'),
    addressShipping: selector(state, 'addressShipping')
      ? getAddress(selector(state, 'addressShipping'))
      : null,
    addressBilling: selector(state, 'addressBilling')
      ? getAddress(selector(state, 'addressBilling'))
      : null,

    productsByStore: selector(state, 'orders'), //getCartProductsByStoreAndShipping(state),
    // initialValues: {
    //     products: getCartProducts(state),
    // }
  };
}
function mapDispatchToProps(dispatch) {
  return bindActionCreators({}, dispatch);
}

// Export Component
export default connect(mapStateToProps, mapDispatchToProps)(CheckoutReview);
