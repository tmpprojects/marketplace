import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { Field, reduxForm, formValueSelector, getFormSyncErrors } from 'redux-form';

import config from '../../../config';
import {
  getAvailableProducts,
  getCartNumberOfShippings,
  getUnavailableProducts,
} from '../../Reducers/shoppingCart.reducer';
import SideBarProducts from './SideBarProducts';
import CollapseBox from '../../Utils/CollapseBox';
import PayPalButton from '../../Utils/PayPalButton';
import { ActionButton } from '../../Utils/ActionButton';
import { InputField } from '../../Utils/forms/formComponents';
import { formatNumberToPrice } from '../../Utils/normalizePrice';
import { modalBoxActions, paymentActions, shoppingCartActions } from '../../Actions';
import {
  PAYMENT_METHODS,
  SHOPPING_CART_SECTIONS,
  COUPON_TYPES,
} from '../../Constants/config.constants';
import { Covid19 } from '../../Utils/modalbox/Covid/Covid19';
import { ImportantMessage } from '../../Utils/modalbox/ImportantMessage/ImportantMessage';
import { SHOPPING_CART_FORM_CONFIG } from '../../Utils/shoppingCart/shoppingCartFormConfig';

/*====================================================
    CHECKOUT SIDEBAR COMPONENT
====================================================*/
class SideBar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isMobile: false,
    };
    this.onResizeHandler = this.onResizeHandler.bind(this);
  }

  componentDidMount() {
    this.onResizeHandler();
    window.addEventListener('resize', this.onResizeHandler);
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.onResizeHandler);
  }

  onResizeHandler() {
    let isMobile = false;
    const w =
      window.innerWidth ||
      document.documentElement.clientWidth ||
      document.body.clientWidth;

    if (w < 727) {
      isMobile = true;
    }

    this.setState({
      isMobile,
    });
  }

  /**
   * verifyCoupon()
   * Calls an endpoint to verify if promo code is valid
   * and receive more details about the coupon type.
   * @param {object} e | Form field event
   */
  couponInput = React.createRef();
  verifyCoupon = () => {
    const coupon = this.couponInput.current.value;
    this.props.verifyCoupon(coupon);
  };

  notAvailable = (props) => {
    const { cartProductsCount, unavailableProducts } = props;
    return cartProductsCount - unavailableProducts.length;
  };
  notAvailableShipment = (props) => {
    const { shippingsCount, cartProductsCount, unavailableProducts } = props;
    const shippingsCountCalc = shippingsCount - unavailableProducts.length;
    return shippingsCountCalc > 1 ? `(${shippingsCountCalc} envíos)` : '';

    // //cartProductsCount -
    // return cartProductsCount - unavailableProducts.length;
    //this.unavailableProducts.length;
  };

  /**
   * renderShippingData()
   * Render billing portions
   */
  renderShippingData = () => {
    // Extract properties
    const { shipping_price } = this.props;
    return (
      <p className="cart-resume__breakdown">
        Envío
        <span className="products-count">
          {this.notAvailableShipment(this.props)}
        </span>
        <span className="quantity">{formatNumberToPrice(shipping_price)}MX</span>
      </p>
    );
  };

  renderSubtotalData = () => {
    // Extract properties
    const { cartProductsCount, cart_price } = this.props;
    return (
      <p className="cart-resume__breakdown subtotal">
        Subtotal&nbsp;
        <span className="products-count">
          ({this.notAvailable(this.props)} producto
          {cartProductsCount !== 1 ? 's' : ''})
        </span>
        <span className="quantity">{formatNumberToPrice(cart_price)}MX</span>
      </p>
    );
  };

  renderTotal = (amount) => (
    <div className=" cart-resume__total" style={{ marginBottom: '1em' }}>
      <div className="cart-resume__breakdown">
        <div>Total</div>
        <span className="quantity">{amount}MX</span>
      </div>
      <div>
        <span className="cr__text--caption cr__textColor--colorGray300 taxIncluded">
          IVA incluido
        </span>
      </div>
    </div>
  );

  // Render sidebar sections
  renderCart = () => {
    const { grand_total } = this.props;
    const { isMobile } = this.state;

    // Coupon Information
    const couponRow = this.getCouponDiscount();

    return (
      <div className="section-checkout">
        {!isMobile && (
          <div className="form promo-code">{this.props.couponComponent}</div>
        )}

        {this.renderSubtotalData()}
        {this.renderShippingData()}
        {couponRow}
        {this.renderTotal(formatNumberToPrice(grand_total))}

        <div className="c2a_container">
          <Link
            to="/checkout/shipping-payment"
            className="c2a_square"
            onClick={(e) => {
              if (!this.props.isGuest && !this.props.isLogged) {
                e.preventDefault();
                this.props.openAccountManagerWindow();
              }
            }}
          >
            Continuar
          </Link>
        </div>

        <div className="cart-resume__module notes">
          <p className="note note--padlock">
            Los vendedores nunca tendr&aacute;n acceso a la informaci&oacute;n de tu
            tarjeta.
          </p>
          <p className="note note--deliveryTime">
            Los tiempos de entrega están sujetos a producci&oacute;n y disponibilidad
            de los art&iacute;culos.
          </p>
        </div>
      </div>
    );
  };
  factoryErrors = () => {
    const {
      clientData = {},
      paymentMethodDetails,
      addressShipping,
      giftReceiver,
    } = this.props.formErrors;

    const arrayofErrors = [];
    if (typeof clientData.client_name !== 'undefined') {
      arrayofErrors.push(clientData.client_name);
    }
    if (typeof clientData.client_lastName !== 'undefined') {
      arrayofErrors.push(clientData.client_lastName);
    }
    if (typeof clientData.email !== 'undefined') {
      arrayofErrors.push(clientData.email);
    }
    if (typeof clientData.phone !== 'undefined') {
      arrayofErrors.push(clientData.phone);
    }
    if (typeof paymentMethodDetails !== 'undefined') {
      arrayofErrors.push(paymentMethodDetails);
    }
    if (typeof addressShipping !== 'undefined') {
      arrayofErrors.push(addressShipping);
    }
    if (typeof giftReceiver !== 'undefined') {
      if (typeof giftReceiver.name !== 'undefined') {
        arrayofErrors.push(giftReceiver.name);
      }
      if (typeof giftReceiver.phone !== 'undefined') {
        arrayofErrors.push(giftReceiver.phone);
      }
    }
    return (
      <div className="factoryErrors">
        <ul>
          {arrayofErrors.slice(0, 1).map((error) => (
            <li>{error}</li>
          ))}
        </ul>
      </div>
    );
  };

  getCouponDiscount = () => {
    const { couponDetails } = this.props;

    // Coupon Information
    let couponRow = null;
    if (couponDetails.valid) {
      switch (couponDetails.type) {
        case COUPON_TYPES.QUANTITY:
          couponRow = (
            <p className="cart-resume__breakdown coupon">
              Cupón
              <span className="quantity">-{couponDetails.discount}MX</span>
            </p>
          );
          break;
        case COUPON_TYPES.PERCENTUAL:
        case COUPON_TYPES.FREE_SHIPPING_AND_PERCENTUAL:
          couponRow = (
            <p className="cart-resume__breakdown coupon">
              Cupón
              <span className="quantity">-{couponDetails.discount * 100}%</span>
            </p>
          );
          break;
        default:
          couponRow = null;
      }
    }

    return couponRow;
  };

  renderReview = () => {
    const {
      formErrors,
      grand_total,
      confirmationSubmit,
      paymentMethod,
    } = this.props;
    const { isMobile } = this.state;
    // PayPal settings!
    const ENV = process.env.NODE_ENV === 'production' ? 'production' : 'sandbox';

    // Look for errors
    let stepHasErrors = formErrors ? Object.keys(formErrors).length : false;
    if (!stepHasErrors) {
      stepHasErrors = !paymentMethod || !paymentMethod.isValid;
    }

    // Coupon Information
    const couponRow = this.getCouponDiscount();

    // If shopping form has errors
    // show an alternative disabled call to action button.
    if (stepHasErrors) {
      return (
        <div>
          {!isMobile && (
            <div className="form promo-code">{this.props.couponComponent}</div>
          )}

          {this.renderSubtotalData()}
          {this.renderShippingData()}
          {couponRow}
          {this.renderTotal(formatNumberToPrice(grand_total))}

          <ActionButton
            disabled
            icon="loading"
            className="c2a_square addToCart"
            style={{ backgroundColor: '#e43271' }}
          >
            Pagar Ahora
          </ActionButton>
          {/* Error factory */}

          <div className="factoryweb c2a_container--return">
            {this.factoryErrors()}

            {/* <Link to="/cart" className="button-simple factorybutton">
              Regresar
            </Link> */}
          </div>
        </div>
      );
    }

    // Return markup
    return (
      <div>
        {!isMobile && (
          <div className="form promo-code">{this.props.couponComponent}</div>
        )}

        {this.renderSubtotalData()}
        {this.renderShippingData()}
        {couponRow}
        {this.renderTotal(formatNumberToPrice(grand_total))}

        {paymentMethod.type === PAYMENT_METHODS.MERCADO_PAGO.slug && (
          <ActionButton
            disabled={!paymentMethod}
            icon="loading"
            className="c2a_square addToCart"
            onClick={confirmationSubmit}
            style={{ backgroundColor: '#e43271' }}
          >
            Pagar Ahora
          </ActionButton>
        )}

        {(paymentMethod.type === PAYMENT_METHODS.BANK_TRANSFER.slug ||
          paymentMethod.type === PAYMENT_METHODS.OXXO.slug) && (
          <ActionButton
            icon="loading"
            className="c2a_square addToCart"
            onClick={confirmationSubmit}
            style={{ backgroundColor: '#e43271' }}
          >
            Confirmar Orden
          </ActionButton>
        )}

        {paymentMethod.type === PAYMENT_METHODS.PAYPAL.slug && (
          <div className="paypal-payment">
            <span className="paypal-payment__title">Finalizar Compra</span>

            <PayPalButton
              commit
              env={ENV}
              currency={'MXN'}
              className="paypal-payment__button"
              client={{
                sandbox: config.paypal.sandbox,
                production: config.paypal.production,
              }}
              style={{
                label: 'generic', // checkout | credit | pay | buynow | generic
                shape: 'rect', // pill | rect
                size: 'responsive', // small | medium | large | responsive
                color: 'gold', // gold | blue | silver | black
              }}
              payment={confirmationSubmit}
              onAuthorize={this.props.onPayPalAuthorize}
              onSuccess={this.props.onPaymentSuccess}
              onError={this.props.onPaymentError}
              onCancel={this.props.onPaymentCancel}
            />
          </div>
        )}
        {/* <div className="c2a_container--return">
          <Link to="/cart" className="button-simple">
            Regresar
          </Link>
        </div> */}
      </div>
    );
  };

  render() {
    const { section, className, productsByStore, grand_total } = this.props;
    const { isMobile } = this.state;

    // Switch sidebar section to render
    let sectionToRender;
    switch (section) {
      case SHOPPING_CART_SECTIONS.CART:
        sectionToRender = this.renderCart();
        break;
      case SHOPPING_CART_SECTIONS.SHIPPING_PAYMENT:
        sectionToRender = this.renderReview();
        break;
      default:
    }
    const header = (
      <div className="header-resume">
        <h5 className="summary">Ver resúmen</h5>
        <p className="summary-sum" style={{ fontWeight: 'bold' }}>
          {formatNumberToPrice(grand_total)}
        </p>
      </div>
    );
    const content = (
      <div>
        {section !== 'cart' && section !== 'review' && (
          <SideBarProducts productsByStore={productsByStore} />
        )}
        {sectionToRender}
        {/* {this.renderShipping()} */}
        {section !== 'payment' && section !== 'review' && (
          <div className="creditCards">
            <ul className="creditCards__list">
              <li className="creditCards__list__item mastercard">
                <span>Mastercard</span>
              </li>
              <li className="creditCards__list__item visa">
                <span>Visa</span>
              </li>
              <li className="creditCards__list__item amex">
                <span>Amex</span>
              </li>
              {/* <li className="creditCards__list__item paypal">
                <span>PayPal</span>
              </li> */}
            </ul>
            <ul className="creditCards__list">
              <li className="creditCards__list__item oxxo">
                <span>OXXO</span>
              </li>
              <li className="bankTransfer">
                Transferencia <br />
                Bancaria
              </li>
            </ul>
          </div>
        )}
      </div>
    );

    // Return markup
    return (
      <div className={className}>
        {!isMobile && (
          <React.Fragment>
            {section !== 'cart' && section !== 'review' && (
              <SideBarProducts productsByStore={productsByStore} />
            )}
            {sectionToRender}

            <div className="cart-resume__module notes">
              <p className="note terms-conditions">
                Al finalizar mi compra, acepto los&nbsp;
                <Link className="link" to="/legales/terminos-condiciones">
                  Términos y Condiciones
                </Link>
                &nbsp; de Canasta Rosa.
              </p>
            </div>
            {/*
            {section !== 'payment' && section !== 'review' && (
              <div className='creditCards'>
                <ul className='creditCards__list'>
                  <li className='creditCards__list__item mastercard'>
                    <span>Mastercard</span>
                  </li>
                  <li className='creditCards__list__item visa'>
                    <span>Visa</span>
                  </li>
                  <li className='creditCards__list__item amex'>
                    <span>Amex</span>
                  </li>
                  <li className='creditCards__list__item paypal'>
                    <span>PayPal</span>
                  </li>
                </ul>
                <ul className='creditCards__list'>
                  <li className='creditCards__list__item oxxo'>
                    <span>OXXO</span>
                  </li>
                  <li className='bankTransfer'>
                    Transferencia <br />
                    Bancaria
                  </li>
                </ul>
              </div>
            )}
            */}
          </React.Fragment>
        )}

        {section !== 'cart' && section !== 'review' && isMobile && (
          <React.Fragment>
            {/* <Covid19 /> */}
            {/* <ImportantMessage /> */}
            <CollapseBox content={content} header={header} />
          </React.Fragment>
        )}
        {isMobile && section === 'cart' && content}
        {/* {isMobile && section === 'review' && (
          <React.Fragment>
            <div className="c2a_container--return">
              <Link to="/cart" className="button-simple">
                Regresar
              </Link>
            </div>
          </React.Fragment>
        )} */}
      </div>
    );
  }
}

// Wrap component within reduxForm
SideBar = reduxForm({
  ...SHOPPING_CART_FORM_CONFIG.config,
})(SideBar);

// Map Redux Props and Actions and export component
const selector = formValueSelector(SHOPPING_CART_FORM_CONFIG.formName);
export default connect(
  (state) => {
    const shippingMethod = selector(state, 'shippingMethod');
    const couponDetails = state.cart.couponDetails;

    return {
      isLogged: state.users.isLogged,
      isGuest: state.users.isGuest,
      formErrors: getFormSyncErrors(SHOPPING_CART_FORM_CONFIG.formName)(state),
      shippingsCount: getCartNumberOfShippings(state),
      unavailableProducts: getUnavailableProducts(state),
      productsByStore: getAvailableProducts(state)
        ? getAvailableProducts(state).filter((o) => {
            return o.physical_properties.shipping_methods.find(
              (sm) => sm.is_available,
            );
          })
        : [], //getCartProductsByStoreAndShipping(state),
      addressShipping: selector(state, 'addressShipping'),
      addressBilling: selector(state, 'addressBilling'),
      shippingMethod,
      couponDetails,
    };
  },
  (dispatch) => ({
    dispatch,
    ...bindActionCreators(
      {
        openModalBox: modalBoxActions.open,
        closeModalBox: modalBoxActions.close,
        createMPCustomer: paymentActions.createMPCustomer,
      },
      dispatch,
    ),
  }),
)(SideBar);

//////////////////////////////
let ZipCodeComponent = (props) => {
  const { shipping_postal_code } = props;
  const onClickHandler = (e) => {
    props.getPriceByZipCode(shipping_postal_code);
  };
  return (
    <React.Fragment>
      <p className="header_zipCode">Calcular el costo de envío</p>
      <div className="form form_zipCode">
        <Field
          name="shipping_postal_code"
          id="shipping_postal_code"
          component={InputField}
          placeholder="Ingresa tu C.P."
        />
        <button type="submit" className="c2a_border" onClick={onClickHandler}>
          Calcular
        </button>
      </div>
    </React.Fragment>
  );
};
ZipCodeComponent = connect(
  (state, props) => ({
    shipping_postal_code: selector(state, 'shipping_postal_code') || '',
  }),
  (dispatch) =>
    bindActionCreators(
      {
        getPriceByZipCode: shoppingCartActions.getPriceByZipCode,
      },
      dispatch,
    ),
)(ZipCodeComponent);
