import Moment from 'dayjs';
import PropTypes from 'prop-types';
import Calendar from 'react-calendar';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import React, { Component } from 'react';
import { reduxForm, Fields, Field, FieldArray, formValueSelector } from 'redux-form';
import Sentry from '../../Utils/Sentry';

import CartProduct from './CartProduct';
import { isValidWorkingDay } from '../../Utils/dateUtils';
import { formatNumberToPrice } from '../../Utils/normalizePrice';
import { getAddress } from '../../Reducers/users.reducer';
import { getCartProductsByStoreAndShipping } from '../../Reducers';
import { SHOPPING_CART_FORM_CONFIG } from '../../Utils/shoppingCart/shoppingCartFormConfig';
import { ImportantMessage } from '../../Utils/modalbox/ImportantMessage/ImportantMessage';
import { Tooltip } from '../../Utils/Tooltip';
import { SHIPPING_METHODS } from '../../Constants';
import { trackWithGTM } from '../../Utils/trackingUtils';

/*------------------------------------------------------
//     SHOPPING CART REDUX FORM VALUE SELECTOR
------------------------------------------------------*/
const formSelector = formValueSelector(SHOPPING_CART_FORM_CONFIG.formName);

let dispatchDataLayerViewInit = false;


function dispatchDataLayerDelete(item) {
  console.log("item", item)


  try {

    if (process.env.CLIENT) {

      window.dataLayer.push({
        "event": "RemoveFromCart",
        "eventCategory": "Ecommerce",
        "eventAction": "Eliminar del carrito",
        "eventLabel": item?.product?.name,
        // Variable con nombre visible del producto
        "ecommerce": {
          "remove": {
            "products": [{
              "name": item?.product?.name,
              "id": item?.product?.id,
              "price": item?.product?.price,
              "brand": item?.product?.store?.name,
              "category": null,
              "variant": item?.attribute?.tree_string ? item?.attribute?.tree_string : null,
              "quantity": item?.quantity
            }]
          }
        },
      });

    }

  } catch (e) {
    console.log(e)
  }
}

function clearLayerText(text) {
  return text.replace(/\"/g, "")
}

function dispatchDataLayerView(orders) {
  if (!dispatchDataLayerViewInit) {
    try {
      if (process.env.CLIENT) {
        let allProducts = [];
        let resultProducts = "[";
        orders.map((order, indexOrder) => {
          order?.products.map((selectOrder, indexProduct) => {
            allProducts.push(`{
            "name": "${clearLayerText(selectOrder.product.name)}",
            "id": ${selectOrder.product.id},
            "price": "${selectOrder.product.price}",
            "brand": "${clearLayerText(selectOrder.product.store.name)}", 
            "category": null,
            "variant": "${selectOrder?.attribute?.tree_string ? clearLayerText(selectOrder?.attribute?.tree_string) : null}",  
            "quantity": ${selectOrder.quantity}
          }`);

          });
        })
        allProducts.map((product, index) => {
          resultProducts += product;
          resultProducts += allProducts.length == (index + 1) ? "" : ",";
        });
        resultProducts += "]";
        ///////////
        window.dataLayer.push({
          "event": "ViewCart",
          "eventCategory": "Ecommerce",
          "eventAction": "Ver carrito de compra",
          "eventLabel": null,
          "ecommerce": {
            "view": {
              "products": JSON.parse(resultProducts),
            }
          },
        });
      }
    } catch (e) {
      console.log(e)
    }
    dispatchDataLayerViewInit = true;
  }
}


/*------------------------------------------------------
//             DELIVERY DATES RENDERER
------------------------------------------------------*/
const DeliverySchedules = (field) => {
  const { pickupSchedules, shippingSchedules } = field;
  return pickupSchedules.reduce((acc, schedule) => {
    const foundSchedule = shippingSchedules.find((a) => a.id === schedule.id);
    return foundSchedule
      ? [
        ...acc,
        <li
          key={schedule.id}
          className={`button-option ${schedule.id === field.input.value.id ? 'active' : ''
            }`}
        >
          <button
            type="button"
            className={`button-tag ${schedule.className}`}
            onClick={(e) => {
              field.input.onChange(schedule);
              setTimeout(() => field.onDeliverySchedulesChange(schedule), 100);
            }}
          >
            {schedule.schedules.delivery}
          </button>
        </li>,
      ]
      : acc;
  }, []);
};

/*------------------------------------------------------
//             SHIPPING CALENDAR COMPONENT
------------------------------------------------------*/
const Calendario = (field) => (
  <Calendar
    locale="es"
    view="month"
    value={field.date}
    minDate={field.minDate}
    maxDate={field.maxDate}
    tileDisabled={({ date }) =>
      !isValidWorkingDay(
        date,
        {
          work_schedules: field.work_schedules,
          physical_properties: {
            ...field.physical_properties,
            minimum_shipping_date: field.minDate,
            maximum_shipping_date: field.maxDate,
            deactivatedDeliveryDaysByShippingMethod:
              field.deactivatedDeliveryDaysByShippingMethod,
          },
        },
        'CART',
      )
    }
    className="calendar-component"
    onChange={(selectedDate) => {
      field.input.onChange(selectedDate);
      setTimeout((e) => field.onCalendarDateChange(selectedDate), 100);
    }}
    tileClassName={({ date }) => {
      const className = 'calendar-component__tile';
      let tileType = !isValidWorkingDay(
        date,
        {
          work_schedules: field.work_schedules,
          physical_properties: {
            ...field.physical_properties,
            minimum_shipping_date: field.minDate,
            maximum_shipping_date: field.maxDate,
            deactivatedDeliveryDaysByShippingMethod:
              field.deactivatedDeliveryDaysByShippingMethod,
          },
        },
        'CART',
      )
        ? 'calendar-component__tile--disabled '
        : '';
      tileType =
        date.getTime() === field.date.getTime()
          ? `${tileType} calendar-component__tile--active`
          : tileType;
      tileType =
        date.getDay() === 6 || date.getDay() === 0
          ? `${tileType} calendar-component__tile--weekend`
          : tileType;
      return `${className} ${tileType}`;
    }}
  />
);

/*------------------------------------------------
//    ORDERS (BY STORE) LIST
------------------------------------------------*/
class OrdersList extends React.Component {
  state = {
    shouldRenderCalendar: false,
    shownShippingSchedules: false,
    shownShippingCode: false,
  };

  /**
   * onCalendarDateChange()
   * @param {string} selectedDate | Selected Date from calendar
   * @param {array} productsList | A list of products corresponding to this order
   */
  onCalendarDateChange = (selectedDate, order) => {
    // Transform date
    const selectedDateUTC = Moment(selectedDate).format('YYYY-MM-DD');

    // Update State
    setTimeout(
      () =>
        this.setState(
          {
            shipping_date: selectedDateUTC,
            shouldRenderCalendar: false,
          },
          () => {
            // Update Shopping Cart Product
            // We need to do this for every product on the order,
            // since the API expects delivery date and schedules per product.
            // * This must change in the future.
            order.products.forEach((product) =>
              this.props.onUpdateCartProduct({
                ...product,
                physical_properties: {
                  ...product.physical_properties,
                  shipping_date: selectedDateUTC,
                },
              }),
            );
          },
        ),
      600,
    );
  };

  /**
   * onDeliverySchedulesChange()
   * @param {int} selectedSchedule | Selected Date ID
   */
  onDeliverySchedulesChange = (selectedSchedule, order) => {
    // Update Shopping Cart Product
    // We need to do this for every product on the order,
    // since the API expects delivery date and schedules per product.
    // This will change in the future.
    order.products.forEach((product) =>
      this.props.onUpdateCartProduct({
        ...product,
        physical_properties: {
          ...product.physical_properties,
          shipping_schedule: selectedSchedule,
        },
      }),
    );
  };

  /**
   * onShippingMethodChange()
   * @param {object} shippingMethod | Shipping Method Object
   * @param {int} orderIndex | Number of order (orders array index)
   */
  onShippingMethodChange = (shippingMethod, orderIndex) => {
    const { fields } = this.props;

    this.props.reduxFormChange(
      `orders[${orderIndex}].physical_properties.shipping_price`,
      shippingMethod.price,
    );
    this.props.reduxFormChange(
      `orders[${orderIndex}].physical_properties.shipping_date`,
      shippingMethod.delivery_date,
    );

    // If selected shipping method is Standard.
    // Use as minimum_shipping_date, the date that the shipping provider sent us via API.
    if (shippingMethod && shippingMethod.slug === SHIPPING_METHODS.STANDARD.slug) {
      // this.props.reduxFormChange(
      //     `orders[${orderIndex}].physical_properties.minimum_shipping_date`,
      //     fields.get(orderIndex).products.reduce((acc, p)=> {
      //         if (!acc) {
      //             const sm = p.product.physical_properties.shipping.find(sm =>
      //                 sm.slug === shippingMethod.slug
      //             );
      //             return Moment(sm.delivery_date, 'YYYY-MM-DD', true).toDate();
      //         }
      //         return acc;
      //     }, null)
      // );
    }
  };

  /**
   * render()
   */
  render() {
    const {
      fields,
      onRemoveProduct,
      onUpdateCartProduct,
      pickupSchedules,
      reduxFormChange,
    } = this.props;

    return fields.map((item, index) => {
      const order = fields.get(index);

      // Only render component if the order has products.
      // Order may be empty if products are not available for shipping.
      if (!order.products.length) {
        return null;
      }

      // Shipping Price
      let shippingPrice = formatNumberToPrice(0);
      if (order.physical_properties.selected_shipping_method) {
        if (
          order.physical_properties.selected_shipping_method.slug ===
          SHIPPING_METHODS.STANDARD.slug
        ) {
          shippingPrice = 'Precio por estimar';
        } else {
          shippingPrice = formatNumberToPrice(
            order.physical_properties.shipping_price,
          );
        }
      }



      // Render Markup
      return (
        <div
          className="store-card"
          key={`${order.store.slug}-
                        ${order.physical_properties.shipping_date}-
                        ${order.physical_properties.shipping_schedule.id}`}
        >
          <div className="store-detail">
            <div className="thumbnail">
              <Link className="link" to={`/stores/${order.store.slug}`}>
                <img src={order.store.photo.small} alt="" />
              </Link>
            </div>
            <div className="store-detail__name">
              <Link className="link" to={`/stores/${order.store.slug}`}>
                {order.store.name}
              </Link>
            </div>
            <div className="store-detail__quantity">
              Productos:&nbsp;
              <span>{order.products.reduce((a, b) => a + b.quantity, 0)}</span>
            </div>
          </div>

          <FieldArray
            name={`${item}.products`}
            component={CartProductsList}
            removeProduct={onRemoveProduct}
            reduxFormChange={reduxFormChange}
            updateCartProduct={onUpdateCartProduct}
          />

          <div className="delivery_details-container">
            <div className="shipping-schedules detail">
              <strong className="name icon_store-shipping">Envío</strong>
            </div>

            <ShippingDateDetails
              index={index}
              fields={fields}
              order={item}
              pickupSchedules={pickupSchedules}
              onCalendarDateChange={this.onCalendarDateChange}
              onDeliverySchedulesChange={this.onDeliverySchedulesChange}
            />

            <AvailableShippingMethods
              index={index}
              fields={fields}
              order={item}
              onShippingMethodChange={this.onShippingMethodChange}
            />

            {/* <ZipCodeComponent order={item} code="11800" /> */}
            {/* <button className="button-simple" onClick={e => this.setState({ shownShippingCode: true })} >Calcular envío</button>
                        {shownShippingCode && } */}
          </div>
        </div>
      );
    });
  }
}

class ShippingDateDetails extends Component {
  state = {
    shouldRenderDetails: false,
    shouldRenderCalendar: false,
  };
  render() {
    const {
      order,
      fields,
      index,
      pickupSchedules,
      onCalendarDateChange,
      onDeliverySchedulesChange,
    } = this.props;
    const { shouldRenderDetails, shouldRenderCalendar } = this.state;
    const orderRef = fields.get(index);
    const physicalProperties = orderRef.physical_properties;
    const {
      selected_shipping_method: {
        deactivated_delivery_days: deactivatedDeliveryDaysByShippingMethod,
      },
    } = physicalProperties;
    const deliveryHourStarts =
      physicalProperties.shipping_schedule.schedules.deliveryStart;
    const deliveryHourEnds =
      physicalProperties.shipping_schedule.schedules.deliveryEnd;
    const isStandardShipping =
      physicalProperties.selected_shipping_method &&
      physicalProperties.selected_shipping_method.slug ===
      SHIPPING_METHODS.STANDARD.slug;

    // Construct human-readable shipping date details depending on shipping method.
    let shippingDateLabel = `Entrega el ${Moment(
      physicalProperties.shipping_date,
    ).format('D MMMM')} ${deliveryHourStarts} - ${deliveryHourEnds}.`;
    if (isStandardShipping) {
      const fixedDate = Moment(physicalProperties.shipping_date);
      const estimatedDate = fixedDate.add('2', 'day');
      shippingDateLabel = `Recíbelo entre el ${fixedDate.format(
        'D MMMM',
      )} - ${estimatedDate.format('D MMMM')}.`;
    }

    return (
      <React.Fragment>
        <div className="shipping_date-container">
          <p className="shipping_date">{shippingDateLabel}</p>
          <button
            className="button-simple"
            onClick={(e) => this.setState({ shouldRenderDetails: true })}
          >
            Cambiar fecha y hora
          </button>
        </div>
        {shouldRenderDetails && (
          <div className="delivery_details">
            <div className="delivery_details--day">
              <p>Día:</p>
              <div className="ui-calendar-selector">
                <div
                  className="ui-calendar-selector__day"
                  onClick={(e) => this.setState({ shouldRenderCalendar: true })}
                >
                  {Moment(
                    physicalProperties.shipping_date,
                    'YYYY-MM-DD',
                    true,
                  ).format('D MMMM YYYY')}
                </div>
                {shouldRenderCalendar && (
                  <Field
                    component={Calendario}
                    minDate={physicalProperties.minimum_shipping_date}
                    maxDate={physicalProperties.maximum_shipping_date}
                    name={`${order}.physical_properties.shipping_date`}
                    physical_properties={orderRef.store.physical_properties}
                    deactivatedDeliveryDaysByShippingMethod={
                      deactivatedDeliveryDaysByShippingMethod
                    }
                    vacations_ranges={orderRef.store.vacations_ranges}
                    date={Moment(
                      physicalProperties.shipping_date,
                      'YYYY-MM-DD',
                      true,
                    ).toDate()}
                    work_schedules={orderRef.store.work_schedules}
                    onCalendarDateChange={(selectedDate) =>
                      onCalendarDateChange(selectedDate, fields.get(index))
                    }
                  />
                )}
              </div>
            </div>
            {!isStandardShipping && (
              <div className="delivery_details--schedule shipping-schedules">
                <p>Hora:</p>
                <ul className="buttons_list">
                  <Field
                    component={DeliverySchedules}
                    pickupSchedules={pickupSchedules}
                    shippingSchedules={orderRef.store.shipping_schedules}
                    onDeliverySchedulesChange={(schedule) =>
                      onDeliverySchedulesChange(schedule, orderRef)
                    }
                    name={`${order}.physical_properties.shipping_schedule`}
                  />
                </ul>
              </div>
            )}
          </div>
        )}
      </React.Fragment>
    );
  }
}

/*------------------------------------------------
//          SHIPPING METHOD OPTIONS
------------------------------------------------*/
let AvailableShippingMethods = (props) => {
  const { order, fields, index, onShippingMethodChange } = props;
  const orderRef = fields.get(index);

  return orderRef.physical_properties.shipping_methods.map((m, i) => {
    let shippingPriceDetails = m.price !== null ? formatNumberToPrice(m.price) : '';
    if (m.slug === SHIPPING_METHODS.STANDARD.slug && shippingPriceDetails === '') {
      shippingPriceDetails = 'Costo en el siguiente paso.';
    }
    return (
      <div
        className={`shipping-method form ${!m.is_available ? 'shipping-method--disabled' : ''
          }`}
        key={m.slug}
      >
        <Field
          disabled={!m.is_available}
          component="input"
          type="radio"
          id={`physical_properties.selected_shipping_method.[${order}][${i}]`}
          className="dots shipping-method__option"
          name={`${order}.physical_properties.selected_shipping_method`}
          value={JSON.stringify(m)}
          format={(value) => JSON.stringify(value)}
          parse={(value) => {
            let jsonVal;
            try {
              jsonVal = JSON.parse(value);
            } catch (err) {
              Sentry.captureException(err);
            }
            return jsonVal;
          }}
          onChange={(e, newValue) => onShippingMethodChange(newValue, index)}
        />
        <label
          htmlFor={`physical_properties.selected_shipping_method.[${order}][${i}]`}
          className="dot shipping-method__label"
        >
          <strong className="provider_name">{m.name}</strong>
          <React.Fragment>
            <p className="price">{shippingPriceDetails}</p>
            {/*m.slug === SHIPPING_METHODS.STANDARD.slug ? (
              <Tooltip message="Precio estimado de envío específicado en el siguiente paso" />
            ) : (
              <Tooltip
                message={`Precio de envío ${formatNumberToPrice(m.price)}MX`}
              />
            )}
            {m.slug === SHIPPING_METHODS.PICKUP_POINT.slug && (
              <Tooltip message="Punto de recolección: Mercado Rosa en Antara Fashion Hall." />
            )*/}
          </React.Fragment>
        </label>
      </div>
    );
  });
};



AvailableShippingMethods = connect((state, props) => {
  const { order } = props;
  return {
    selected_shipping_method:
      formSelector(state, `${order}.selected_shipping_method`) || null,
  };
})(AvailableShippingMethods);

/*------------------------------------------------
//    CART PRODUCTS LIST COMPONENT
------------------------------------------------*/
const CartProductsList = ({
  fields,
  removeProduct,
  reduxFormChange,
  updateCartProduct,
}) => (
    <ul>
      {fields.map((product, index) => (
        <Fields
          component={CartProduct}
          names={[`${product}.note`, `${product}.quantity`]}
          key={`${product}.product.slug`}
          product={fields.get(index)}
          slice={`${product}`}
          removeProduct={(e) => {
            //console.log("delete item", fields.get(index))
            dispatchDataLayerDelete(fields.get(index));
            removeProduct(fields.get(index));
            fields.remove(index);
          }}
          updateCartProduct={() => updateCartProduct(fields.get(index))}
        />
      ))}

    </ul>
  );

/*------------------------------------------------
//   CART COMPONENT
------------------------------------------------*/
class Cart extends Component {
  // PropTypes / DefaultProps
  static propTypes = {
    //products: PropTypes.array.isRequired,
    onRemoveProduct: PropTypes.func.isRequired,
    onUpdateProduct: PropTypes.func.isRequired,
  };

  /**
   * constructor()
   * @param {object} props | Component properties
   */
  constructor(props) {
    super(props);
    this.state = {
      shipping_date: '',
      shouldRenderCalendar: false,
      isMobile: false,
    };
  }

  componentWillUnmount() {
    dispatchDataLayerViewInit = false;
  }
  componentDidMount() {
    const items = this.props.productsByStore.reduce(
      (a, b) => [
        ...a,
        ...b.products.map((p, i) => ({
          id: p.product.id,
          name: p.product.name,
          //variant: p.product.physical_properties.size.display_name, falta que Ara boy agregue key
          brand: b.store.name,
          quantity: p.quantity,
          price: p.price,
          dimension1: p.product.slug,
          dimension2: b.store.slug,
          // category: 'guides/google-tag-manager/enhanced-ecommerce',
        })),
      ],
      [],
    );

    //Send to Google Tag Manager
    trackWithGTM('eec.checkout', {
      actionField: {
        step: 1,
      },
      products: items,
    });

    this.onResizeHandler();
    window.addEventListener('resize', this.onResizeHandler);
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.onResizeHandler);
  }

  /**
   * onCalendarDateChange()
   * @param {date} selectedDate | New shipping date
   */
  onCalendarDateChange = (selectedDate) => {
    // Transform date into UTC timezone
    const selectedDateUTC = new Date(selectedDate);
    // Update State
    setTimeout(
      () =>
        this.setState(
          {
            shipping_date: selectedDateUTC,
            shouldRenderCalendar: false,
          },
          () => {
            // Update Shopping Cart Product
            this.props.onUpdateProduct();
          },
        ),
      600,
    );
  };

  /**
   * optionsSelect()
   * Return an array of <option />
   * @param {int} maxNum | Number of items to create
   */
  optionsSelect = (maxNum) => {
    const optionsList = [];
    for (let i = 1; i <= maxNum + 1; i++) {
      optionsList.push(
        <option key={i} value={i}>
          {i}
        </option>,
      );
    }
    return optionsList;
  };

  /**
   * onResizeHandler()
   * Window onResize Handler
   */
  onResizeHandler = () => {
    let isMobile = false;
    const w =
      window.innerWidth ||
      document.documentElement.clientWidth ||
      document.body.clientWidth;

    if (w < 727) {
      isMobile = true;
    }

    this.setState({
      isMobile,
    });
  };

  /**
   * render()
   */
  render() {
    const {
      onUpdateProduct,
      onRemoveProduct,
      pickupSchedules: { schedules: pickupSchedules },
      change: reduxFormChange,
      productsByStore,
    } = this.props;

    dispatchDataLayerView(productsByStore)

    return (
      <React.Fragment>
        {/* {this.state.isMobile && <ImportantMessage />} */}

        {productsByStore.length > 0 ? (
          <FieldArray
            name="orders"
            component={OrdersList}
            reduxFormChange={reduxFormChange}
            onRemoveProduct={onRemoveProduct}
            onUpdateCartProduct={onUpdateProduct}
            pickupSchedules={pickupSchedules}
          />
        ) : (
            <div>
              Lo sentimos, pero estos productos no pueden ser enviados&nbsp; a esta
              dirección seleccionada.
            </div>
          )}
      </React.Fragment>
    );
  }
}

// Wrap component within reduxForm
Cart = reduxForm({
  ...SHOPPING_CART_FORM_CONFIG.config,
})(Cart);

// Pass Redux state and actions to component
function mapStateToProps(state, props) {
  // Define or retrieve default form values
  let orders =
    formSelector(state, 'orders') || getCartProductsByStoreAndShipping(state);

  const addressesList = state.users.addresses.addresses;
  const selectedShippingAddress =
    formSelector(state, 'selectedShippingAddress') || undefined;
  // If 'shopping form' already has a selected address,
  // choose this address to keep persistance.
  let defaultAddress = addressesList.find((address) => {
    // Set selected address.
    if (selectedShippingAddress && selectedShippingAddress === address.uuid) {
      return address;
    }
  });

  if (!defaultAddress) {
    defaultAddress = addressesList.reduce((found, a, i, l) => {
      // If the address has not been found yet,
      // search whithin different options.
      if (!found) {
        let targetAddress = false;

        // If the address´s not been found,
        // search if this is the default address.
        if (a.default_address && !targetAddress) {
          targetAddress = a;
        }

        // If the above options failed to set an address,
        // choose the first address on the list.
        if (i === l.length - 1 && !targetAddress) {
          targetAddress = l[0];
        }

        // Return result.
        return targetAddress;
      }
      return found;
    }, false);
  }

  const initialValues = {
    ...(orders
      ? {
        orders: orders,
      }
      : {}),
    ...(defaultAddress
      ? {
        selectedShippingAddress: defaultAddress.uuid,
      }
      : {}),
    ...(defaultAddress
      ? {
        addressShipping: getAddress(defaultAddress),
      }
      : {}),
    ...(defaultAddress
      ? {
        addressBilling: getAddress(defaultAddress),
      }
      : {}),
  };

  // Return props
  return {
    pickupSchedules: state.app.pickupSchedules,
    coupon: formSelector(state, 'discount_code') || '',
    productsByStore: orders,
    initialValues: initialValues,
  };
}
export default connect(mapStateToProps)(Cart);
