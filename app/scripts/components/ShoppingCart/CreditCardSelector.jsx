import React, { Component } from 'react';
import { Field } from 'redux-form';

import CreditCardForm from './CreditCardForm';
import CardList from './CreditCardList';
import { InputField } from '../../../scripts/Utils/forms/formComponents.js';

import { IconPreloader } from '../../Utils/Preloaders';
import { PAYMENT_METHODS } from '../../Constants/';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

//////////////////////
export const ItemCreditCardList = (parentProps) => {
  const { creditCard, input, className = '', ...props } = parentProps;

  return (
    <div className="creditCard credit-card-option">
      <label htmlFor={props.id}>
        <div
          className={`logo card-provider ${creditCard.payment_method.display_name}`}
        />
        <div className="card-number">•••• {creditCard.last_four_digits}</div>
        <div className="card-exp">
          Exp. {`${creditCard.expiration_month}/${creditCard.expiration_year}`}
        </div>
      </label>
      <input type="radio" className={`${className}`} {...input} {...props} />
    </div>
  );
};

//////////////////////
export class CreditCardSelector extends Component {
  static defaultProps = {
    creditCards: [],
  };
  constructor(props) {
    super(props);

    this.state = {
      selectedCard: this.props.creditCards.length
        ? this.getCardByID(
            this.props.creditCards.find((c, i) => {
              if (c.is_default) {
                return true;
              }
              if (i == this.props.creditCards.length - 1) {
                return true;
              }
              return false;
            }).card_id,
          )
        : null,
      shouldRenderCreditCards: false,
      shouldRenderCreditCardForm: false,
    };
  }
  componentDidMount() {
    this.cardID = null;

    // Get Secure (hidden) form
    this.secureSavedCardForm = document.querySelector('#cardIdForm');

    // Clear Form
    // this.resetPaymentDetails();
    (() => {
      const { selectedCard } = this.state;
      this.props.onSecureNumberChange({
        cardProvider: selectedCard.payment_method.display_name,
        data: null,
        secureForm: this.secureSavedCardForm,
        isValid: false,
        saveCreditCard: false,
        customerId: selectedCard.customer.customer_id,
        isStoredCard: true,
        type: PAYMENT_METHODS.MERCADO_PAGO.slug,
      });
      return { target: { value: selectedCard?.card_id } };
    })();
  }

  getCardByID = (cardID) => {
    return this.props.creditCards.find((c) => c.card_id === cardID);
  };

  /**
   * openWindowForm()
   * Updates the component state. This method opens the credit card list view.
   * @returns {void}
   */
  openWindowForm = () => {
    // Clear Form
    this.resetPaymentDetails();
    this.setState({
      shouldRenderCreditCards: true,
      shouldRenderCreditCardForm: false,
    });
  };

  /**
   * closeCreditCardList()
   * Updates the component state. This method closes the credit card list view.
   * @returns {void}
   */
  closeCreditCardList = () => {
    // Clear Form
    this.resetPaymentDetails();
    this.setState({
      shouldRenderCreditCards: false,
      shouldRenderCreditCardForm: false,
    });
  };

  /**
   * openCreditCardForm()
   * Updates the component state. This method opens the 'new' credit card form.
   * @returns {void}
   */
  openCreditCardForm = () => {
    // Clear Form
    this.resetPaymentDetails();
    this.setState({
      shouldRenderCreditCardForm: true,
      shouldRenderCreditCards: false,
    });
  };

  /**
   * closeCreditCardForm()
   * Updates the component state. This method closes the 'new' credit card form.
   * @returns {void}
   */
  closeCreditCardForm = () => {
    // Clear Form
    this.resetPaymentDetails();
    this.setState({
      shouldRenderCreditCards: false,
      shouldRenderCreditCardForm: false,
    });
  };

  /**
   * onCreditCardChange()
   * Call parent´s handler when a user has selected/changed a credit card via the UI.
   * @param {object} e | Input event object.
   * @returns {void}
   */
  onCreditCardChange = (e) => {
    if (!e.target.value) return;

    const creditCard = this.getCardByID(e.target.value);

    // Wait some time before closing the UI creditCard list view (just for Interaction/UX purpouses).
    setTimeout(this.closeCreditCardList, 400);
    this.setState({
      selectedCard: creditCard,
    });

    // Call parent´s handler and pass the current creditCard object.
    this.props.onCreditCardChange({
      cardProvider: creditCard.payment_method.display_name,
      data: null,
      secureForm: this.secureSavedCardForm,
      isValid: false,
      saveCreditCard: false,
      customerId: creditCard.customer.customer_id,
      isStoredCard: true,
      type: PAYMENT_METHODS.MERCADO_PAGO.slug,
    });
  };

  /**
   * renderCreditCardsList()
   * @param {array} creditCards : An array of credit/debit cards
   * @returns {jsx} A portion of JSX containing a list of credit card previews.
   */
  renderCreditCardsList = (creditCards) => {
    return creditCards.length ? (
      <div className="credit-cards-list-container" styles={{ width: '100%' }}>
        <label className="cvv-confirmation__label" htmlFor="selectedCreditCard">
          Elige una tarjeta
        </label>
        <div className="credit-cards-list">
          {creditCards.map((creditCard) => (
            <Field
              type="radio"
              id={`creditCard_${creditCard.card_id}`}
              name="selectedCreditCard"
              creditCard={creditCard}
              key={creditCard.card_id}
              value={creditCard.card_id}
              onChange={this.onCreditCardChange}
              component={ItemCreditCardList}
            />
          ))}
        </div>
      </div>
    ) : null;
  };

  /**
   * validateSecurityNumber()
   * Validate Card information and updates payment method information
   */
  validateSecurityNumber = (e) => {
    const { selectedCard } = this.state;
    const securityNumber = e.target.value;

    // Proceed to fill up the form that will be sent to MercadoPago API
    if (securityNumber.length >= 3 && securityNumber.length <= 4) {
      this.fillSecureCardForm(selectedCard.card_id, securityNumber);
      this.props.onSecureNumberChange({
        cardProvider: selectedCard.payment_method.display_name,
        data: null,
        secureForm: this.secureSavedCardForm,
        isValid: true,
        saveCreditCard: false,
        customerId: selectedCard.customer.customer_id,
        isStoredCard: true,
        type: PAYMENT_METHODS.MERCADO_PAGO.slug,
      });

      // return this.props.onSecurityCodeValidate(this.secureSavedCardForm)
      // .then(response => console.log('then ', response))
      // .catch(error => console.log('catch: ', error));
    } else {
      this.resetPaymentDetails();
    }
  };

  //////
  //////
  //////
  resetPaymentDetails = () => {
    if (this.secureSavedCardForm) {
      this.clearSecureSavedCardForm();
    }

    this.props.onSecureNumberChange({
      cardProvider: null,
      data: null,
      secureForm: null,
      isValid: false,
      customerId: null,
      saveCreditCard: false,
      isStoredCard: true,
    });
  };

  /*
   * Set Payment Method Details
   * Fillup an HTML form with redux-form value.
   * (This is the one that´ll be sent to MercadoPago!)
   * @param {object} e : Event Object
   */
  fillSecureCardForm = (cardID, securityNumber = '') => {
    this.secureSavedCardForm.querySelector(
      "input[data-checkout='securityCode']",
    ).value = securityNumber;
    this.secureSavedCardForm.querySelector(
      "input[data-checkout='cardId']",
    ).value = cardID;
    return false;
  };

  /*
   * Set Payment Method Details
   * Clear HTML form values.
   */
  clearSecureSavedCardForm() {
    this.secureSavedCardForm.querySelector(
      "input[data-checkout='securityCode']",
    ).value = '';
    this.secureSavedCardForm.querySelector("input[data-checkout='cardId']").value =
      '';
  }

  render() {
    const {
      shouldRenderCreditCards,
      shouldRenderCreditCardForm,
      selectedCard,
    } = this.state;
    const { creditCards } = this.props;
    const {
      payment_method: { display_name },
    } = selectedCard;

    // TODO: Verify this conditional flow. Not sure if it´s well implemented.
    if (!shouldRenderCreditCards && !creditCards.length) {
      return <IconPreloader />;
    }

    //
    if (shouldRenderCreditCardForm) {
      return (
        <React.Fragment>
          <div style={{ textAlign: 'right' }}>
            <button className="back-button" onClick={this.closeCreditCardForm}>
              Cancelar
            </button>
          </div>
          <CreditCardForm
            // refresh={
            //     paymentMethod
            //     && paymentMethod.type === PAYMENT_METHODS.MERCADO_PAGO.slug
            // }
            onCreditCardUpdate={this.props.onCreditCardUpdate}
            onInvalidCreditCard={this.props.onInvalidCreditCard}
            dispatch={null}
          />
        </React.Fragment>
      );
    }

    // Render Output
    return (
      <React.Fragment>
        {!shouldRenderCreditCards ? (
          <React.Fragment>
            <div className="module selected-card">
              <label className="cvv-confirmation__label">
                Pagar con la tarjeta:
              </label>

              <div className="form__data credit-card-selector">
                <div
                  className={`logo card-provider ${selectedCard.payment_method.display_name}`}
                />
                <div className="card-number">
                  •••• {selectedCard.last_four_digits}
                </div>
                <div className="card-exp">
                  Exp.{' '}
                  {`${selectedCard.expiration_month}/${selectedCard.expiration_year}`}
                </div>
                {creditCards.length > 1 && (
                  <div
                    className="card-change"
                    onClick={() => {
                      this.openWindowForm();
                    }}
                  >
                    Cambiar
                  </div>
                )}
              </div>

              <div className="cvv-confirmation">
                <label className="cvv-confirmation__label" htmlFor="cc-cvv">
                  Confirma tu CVV
                </label>
                <Field
                  id="cc-cvv"
                  name="cc-cvv"
                  className="cvv-confirmation__input"
                  x-autocompletetype="cc-cvv"
                  autoComplete="card-cvv"
                  autoCorrect="off"
                  spellCheck="off"
                  pattern="\d*"
                  placeholder="CVV"
                  autoComplete="off"
                  maxLength="4"
                  component={InputField}
                  showErrorsOnTouch={true}
                  normalize={(value) => value.replace(/\D/g, '')}
                  onKeyUp={this.validateSecurityNumber}
                  onBlur={this.validateSecurityNumber}
                />
              </div>

              <div
                style={{ display: 'none' }}
                action=""
                method="post"
                id="cardIdForm"
                name="cardIdForm"
              >
                <fieldset>
                  <input
                    data-checkout="securityCode"
                    type="text"
                    autoComplete="off"
                    onPaste={() => false}
                    onCopy={() => false}
                    onCut={() => false}
                    onDrag={() => false}
                    onDrop={() => false}
                  />
                  <input data-checkout="cardId" type="text" name="cardId" />
                  <input type="hidden" name="paymentMethodId" />
                  <input type="hidden" name="token" />
                </fieldset>
              </div>
            </div>

            <div className="module new-card-button">
              <label className="cvv-confirmation__label">
                Pagar con una tarjeta diferente:
              </label>
              <button
                type="button"
                onClick={this.openCreditCardForm}
                className="button button-square--gray"
              >
                + Nueva Tarjeta
              </button>
            </div>
          </React.Fragment>
        ) : (
          // this.renderCreditCardsList(creditCards)
          <CardList
            CreditCards={creditCards}
            onCreditCardChange={this.onCreditCardChange}
          />
        )}
      </React.Fragment>
    );
  }
}

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
    ...bindActionCreators({}, dispatch),
  };
}

// Export Component
export default connect(null, mapDispatchToProps)(CreditCardSelector);
