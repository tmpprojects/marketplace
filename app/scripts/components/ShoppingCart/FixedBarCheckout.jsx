import React, { Component } from 'react';
import { Link } from 'react-router-dom';

import config from '../../../config';
import { ActionButton } from '../../Utils/ActionButton';
import PayPalButton from '../../Utils/PayPalButton';
import { formatNumberToPrice } from '../../Utils/normalizePrice';
import {
  PAYMENT_METHODS,
  SHOPPING_CART_SECTIONS,
} from '../../Constants/config.constants';

class FixedBarCheckout extends Component {
  renderReview = (isDisabled = false) => {
    const {
      confirmationSubmit,
      paymentMethod,
      cartTotal,
      grandTotal,
      shippingPrice,
      ...props
    } = this.props;

    // PayPal settings!
    const ENV = process.env.NODE_ENV === 'production' ? 'production' : 'sandbox';

    return (
      <React.Fragment>
        {(paymentMethod.type === PAYMENT_METHODS.MERCADO_PAGO.slug ||
          paymentMethod.type === PAYMENT_METHODS.BANK_TRANSFER.slug ||
          paymentMethod.type === PAYMENT_METHODS.OXXO.slug) && (
          <ActionButton
            icon="loading"
            className="c2a_square mobile call-to-action"
            onClick={confirmationSubmit}
            disabled={isDisabled}
            style={{ backgroundColor: '#e43271' }}
          >
            {paymentMethod.type === PAYMENT_METHODS.MERCADO_PAGO.slug
              ? `Pagar Ahora | ${formatNumberToPrice(grandTotal)}MX`
              : `Confirmar Orden | ${formatNumberToPrice(grandTotal)}MX`}
          </ActionButton>
        )}

        {paymentMethod.type === PAYMENT_METHODS.PAYPAL.slug && (
          <div className="paypal-payment">
            <span className="paypal-payment__title">Finalizar Compra</span>

            <PayPalButton
              commit
              className="paypal-payment__button mobile"
              env={ENV}
              currency={'MXN'}
              client={{
                sandbox: config.paypal.sandbox,
                production: config.paypal.production,
              }}
              style={{
                label: 'generic', // checkout | credit | pay | buynow | generic
                size: 'responsive', // small | medium | large | responsive
                shape: 'rect', // pill | rect
                color: 'gold', // gold | blue | silver | black
              }}
              payment={confirmationSubmit}
              onAuthorize={props.onPayPalAuthorize}
              onSuccess={props.onPaymentSuccess}
              onError={props.onPaymentError}
              onCancel={props.onPaymentCancel}
            />
          </div>
        )}
      </React.Fragment>
    );
  };

  render() {
    const {
      cartTotal,
      productsCount,
      shippingPrice,
      grandTotal,
      section,
      syncErrors,
      paymentMethod,
    } = this.props;
    let buttonToRender;
    let status = 'inactive';
    let stepHasErrors = false;

    // Do not render button if form is not completed and validated.
    if (
      section === SHOPPING_CART_SECTIONS.SHIPPING_PAYMENT &&
      (syncErrors.clientData || !paymentMethod || !paymentMethod.isValid)
    ) {
      return (
        <div className={`fixed-bar fixed-bar--${status}`}>
          <div className="fixed-bar__container">
            <React.Fragment>
              {/* <div className="price">
                                {formatNumberToPrice(grandTotal)}
                            </div> */}
              <div className="products-count">{productsCount}</div>
            </React.Fragment>

            <button
              onClick={(e) => null}
              className="c2a_square mobile call-to-action"
              disabled
            >{`Pagar Ahora | ${formatNumberToPrice(grandTotal)}MX`}</button>
          </div>
        </div>
      );
    }

    // Render different buttons
    // depending on current shopping cart´s section
    switch (section) {
      case SHOPPING_CART_SECTIONS.SHIPPING_PAYMENT:
        stepHasErrors = Object.keys(syncErrors).length || false;
        if (!stepHasErrors) {
          stepHasErrors = paymentMethod ? !paymentMethod.isValid : true;
        }
        status = !stepHasErrors ? 'active' : 'inactive';
        buttonToRender = this.renderReview(stepHasErrors);
        break;
      case SHOPPING_CART_SECTIONS.REVIEW:
        buttonToRender = this.renderReview(false);
        status = 'active';
        break;
      default:
      case SHOPPING_CART_SECTIONS.CART:
        status = 'active';
        buttonToRender = (
          <Link
            to="/checkout/shipping-payment"
            className="c2a_square mobile call-to-action"
            onClick={(e) => {
              if (!this.props.isGuest && !this.props.isLogged) {
                e.preventDefault();
                this.props.openAccountManagerWindow();
              }
            }}
          >
            Continuar | {formatNumberToPrice(cartTotal)}MX
          </Link>
        );
        break;
    }

    // Return markup
    return (
      <div className={`fixed-bar fixed-bar--${status}`}>
        <div className="fixed-bar__container">
          <React.Fragment>
            {/* <div className="price">
                            {formatNumberToPrice(cartTotal + shippingPrice)}
                        </div> */}
            <div className="products-count">{productsCount}</div>
          </React.Fragment>

          {buttonToRender}
        </div>
      </div>
    );
  }
}

export default FixedBarCheckout;
