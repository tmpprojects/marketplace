import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { ResponsiveImageFromURL } from '../../Utils/ImageComponents';
import { formatNumberToPrice } from '../../Utils/normalizePrice';
import { analyticsActions } from '../../Actions';

export class ProductsList extends Component {
  render() {
    const { items } = this.props;
    const trackClick = (product) => {
      this.props.trackProductClick(this.props.location, product.slug);
    };

    return (
      <ul className="products__list center">
        {items.map((product) => (
          <li className="product-landing product hotsale " key={product.slug}>
            <div className="product_container hotsale-none">
              <div className="thumbnail">
                <Link
                  to={`/stores/${product.store.slug}/products/${product.slug}`}
                  className="thumbnail__image hotsale-img"
                  onClick={() => trackClick(product)}
                >
                  {product.photo && (
                    <ResponsiveImageFromURL
                      src={product.photo.small}
                      alt={product.name}
                    />
                  )}
                  {product.discount !== '0.00' && (
                    <div className="hotsale-discount">
                      <span className="cr__text--caption cr__textColor--colorWhite">
                        -{Math.round(product.discount)}%
                      </span>
                    </div>
                  )}
                </Link>
              </div>
              <div className="product__info hotsale-product">
                <Link
                  className="product__name"
                  to={`/stores/${product.store.slug}/products/${product.slug}`}
                  onClick={() => trackClick(product)}
                >
                  {product.name.substring(0, 60)}
                  {product.name.length >= 60 && <span>...</span>}
                </Link>
                <div className="product__info-detail">
                  
                  <Link
                    className="store product__store-link"
                    to={`/stores/${product.store.slug}`}
                  >
                    {product.store.name}
                  </Link>
                  <p className="product__shippingMethod cr__text--caption cr__textColor--colorViolet400">
                    {product?.physical_properties?.shipping_methods?.find(
                      (i) => i === 'express-moto' || i === 'express-car',
                    ) && 'Envío express'}
                    {!product?.physical_properties?.shipping_methods?.find(
                      (i) => i === 'express-moto' || i === 'express-car',
                    ) &&
                      product?.physical_properties?.shipping_methods?.find(
                        (i) => i === 'standard',
                      ) &&
                      'Envío Nacional'}
                  </p>
                  <div
                    className="cr__textColor--colorGray400"
                    style={{ fontWeight: '500', fontSize: '0.9' }}
                  >
                    <span
                      className="cr__textColor--colorGray400"
                      style={{ fontWeight: '500', fontSize: '0.9' }}
                    >
                      {formatNumberToPrice(product?.price_without_discount)}MX
                    </span>
                  </div>

                  <span
                    className="cr__text--paragraph product__price-visa"
                    style={{ fontWeight: '600' }}
                  >
                    {formatNumberToPrice(product?.price)}MX
                  </span>
                  <p className="cr__text--caption" style={{ color: '#1a1f71' }}>
                    Sólo con{' '}
                    <img
                      style={{ width: '2.3em' }}
                      src={require('../../../images/icons/VISALogo--blue.svg')}
                      alt="visa-logo"
                    />
                  </p>
                  {/* <span className="shipping_date"> Recíbelo el {Moment(product.physical_properties.minimum_shipping_date, 'YYYY-MM-DD', true).format('D MMMM')}</span> */}
                </div>
                <Link to="" className="product__rating">
                  <img
                    src={require('images/store/ranking.png')}
                    className="ranking"
                    alt=""
                  />
                </Link>
              </div>
            </div>
          </li>
        ))}
      </ul>
    );
  }
}

// Map Redux Props and Actions to component
function mapStateToProps() {
  return {};
}
function mapDispatchToProps(dispatch) {
  const { trackProductClick } = analyticsActions;

  return bindActionCreators(
    {
      trackProductClick,
    },
    dispatch,
  );
}

// Export Component
export default connect(mapStateToProps, mapDispatchToProps)(ProductsList);
