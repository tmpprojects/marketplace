import React from 'react';
import { Link } from 'react-router-dom';
import Moment from 'dayjs';

import { ORDER_STATUS } from '../../Constants/orders.constants';
import { SHIPPING_METHODS } from '../../Constants';
import { ResponsiveImage } from '../../Utils/ImageComponents';
import { formatNumberToPrice } from '../../Utils/normalizePrice';

const displayStatus = (status) => {
  switch (status) {
    case ORDER_STATUS.NEW_ORDER:
      return <span className="new-order status-warning">Orden Nueva</span>;
    case ORDER_STATUS.PREPARING_ORDER:
      return (
        <span className="producing-order status-warning">Preparando Orden</span>
      );
    case ORDER_STATUS.ORDER_READY:
    case ORDER_STATUS.AWAITING_SHIPMENT:
      return (
        <span className="shipping-order status-warning">Esperando Recolección</span>
      );
    case ORDER_STATUS.ORDER_IN_TRANSIT:
      return (
        <span className="transiting-order status-warning">Orden en Tránsito</span>
      );
    case ORDER_STATUS.AWAITING_PAYMENT:
      return <span className="transiting-order status-warning">Esperando Pago</span>;
    case ORDER_STATUS.DELIVERED:
      return <span className="order-delivered status-warning">Entregada</span>;
    default:
      return <span>Orden Pendiente</span>;
  }
};

export default (props) => {
  const { items = [] } = props;

  return (
    <React.Fragment>
      {items.map((group) => (
        <div className="order" key={group.uid}>
          <div className="order_header">
            <span className="store">Orden: {group.uid}</span>
            <span className="status">
              {displayStatus(group.orders[0].physical_properties.status.value)}
            </span>
          </div>
          <div className="order_detail">
            <p className="order_created">
              Realizada: <span>{Moment(group.created).format('DD MMMM YYYY')}</span>
              <br />
            </p>

            <Link to={`/users/orders/${group.uid}`} className="button_view detail">
              Ver Detalle
            </Link>
          </div>

          {group.orders.map((order, i) => {
            const physicalProperties = order.physical_properties;
            const isStandardShipping =
              physicalProperties.shipping_method_name &&
              physicalProperties.shipping_method_name ===
                SHIPPING_METHODS.STANDARD.name;

            // Construct human-readable shipping date details depending on shipping method.
            let shippingDateLabel = `${Moment(
              physicalProperties.shipping_date,
            ).format('D MMMM')}.`;
            if (isStandardShipping) {
              const fixedDate = Moment(physicalProperties.shipping_date);
              const estimatedDate = fixedDate.add('2', 'day');
              shippingDateLabel = `Entre el ${fixedDate.format(
                'D MMMM',
              )} - ${estimatedDate.format('D MMMM')}.`;
            }

            return (
              <div className="store-order" key={`${group.uid}-${i}`}>
                <div className="summary">
                  <Link to={`/users/orders/${group.uid}`}>
                    <div className="summary__detail">
                      <div style={{ width: '100%' }}>
                        Entrega estimada:&nbsp;
                        <span>{shippingDateLabel}</span>
                      </div>

                      <div className="img_container">
                        <div className="product_img">
                          {order.store.photo && (
                            <ResponsiveImage
                              src={order.store.photo}
                              alt={order.store.name}
                            />
                          )}
                        </div>
                      </div>
                      <div className="products_container">
                        <p className="quantity">
                          {order.products.length}&nbsp; producto
                          {order.products.length > 1 ? 's' : ''}
                        </p>
                        <ul>
                          {order.products.map((product) => {
                            if (!product.product) {
                              return (
                                <li key={`unfound-product-${group.uid}`}>
                                  <span>Producto no disponible</span>
                                  <p>{order.store.name}</p>
                                </li>
                              );
                            }
                            return (
                              <li key={product.product.slug}>
                                <span>{product.product.name}</span>
                                <p>{order.store.name}</p>
                                {/*<p className="store">Mikla Design</p>*/}
                              </li>
                            );
                          })}
                          {order.products.length > 3 && (
                            <li>
                              <span>Otros...</span>
                            </li>
                          )}
                        </ul>
                      </div>
                      <div className="detail-container user">
                        <p>Subtotal: {formatNumberToPrice(order.products_price)}</p>
                        <div className="order_total">
                          <p>
                            <span>Total:</span>&nbsp;
                            {formatNumberToPrice(order.total_price)}
                          </p>
                        </div>
                      </div>
                    </div>
                  </Link>
                </div>
              </div>
            );
          })}
        </div>
      ))}
    </React.Fragment>
  );
};
