import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { CSSTransition } from 'react-transition-group';
import Moment from 'dayjs';

import StatusInfo from './StatusInfo';
import { ordersActions } from '../../Actions';
import { SHIPPING_METHODS } from '../../Constants/config.constants';
import { ORDER_STATUS } from '../../Constants/orders.constants';

class OrderStatus extends Component {
  getStatusStep = (hasPreparationProcess, status) => {
    if (hasPreparationProcess) {
      switch (status) {
        case ORDER_STATUS.NEW_ORDER:
          return 1;
        case ORDER_STATUS.PREPARING_ORDER:
          return 1;
        case ORDER_STATUS.ORDER_READY:
        case ORDER_STATUS.AWAITING_SHIPMENT:
          return 2;
        case ORDER_STATUS.ORDER_IN_TRANSIT:
          return 3;
        default:
          return 0;
      }
    }
    switch (status) {
      case ORDER_STATUS.NEW_ORDER:
        return 2;
      case ORDER_STATUS.ORDER_READY:
      case ORDER_STATUS.AWAITING_SHIPMENT:
        return 2;
      case ORDER_STATUS.ORDER_IN_TRANSIT:
        return 3;
      default:
        return 0;
    }
  };

  doTransition = () => {
    // this.setState({
    //     showTransition: !this.state.showTransition,
    // });
  };

  changeStatus = () => {
    const { order } = this.props;
    let nextStatus = order.physical_properties.can_store_change_status_to;

    //
    if (order.physical_properties.status.value === ORDER_STATUS.WAYBILL_GENERATED) {
      nextStatus = ORDER_STATUS.WAYBILL_GENERATED;
    }
    return this.props.updateStatusOrder(order.uid, nextStatus);
  };

  render() {
    //const { currentStep, hasPreparationProcess } = this.state;
    const { order } = this.props;
    const status = order.physical_properties.status.value;
    const hasPreparationProcess = order.physical_properties.has_preparation_process;
    const currentStep = this.getStatusStep(hasPreparationProcess, status);
    const shipping = order.physical_properties;
    const isExpress =
      shipping.shipping_method_slug === SHIPPING_METHODS.EXPRESS_BIKE.slug ||
      shipping.shipping_method_slug === SHIPPING_METHODS.EXPRESS_CAR.slug;

    const currentDay = Moment().format('YYYY-MM-DD');
    const remainingDayForDelivery = Moment(shipping.pickup_date).diff(
      currentDay,
      'days',
    );
    const COLOR_CODES = {
      BYPASS: '#1eb592',
      WARNING: '#ceb52b',
      ALERT: '#e6575d',
    };
    let statusColorCode = COLOR_CODES.BYPASS;
    if (remainingDayForDelivery < 3) statusColorCode = COLOR_CODES.WARNING;
    if (remainingDayForDelivery < 2) statusColorCode = COLOR_CODES.ALERT;

    let remainingDaysHeader = null;
    if (
      status === ORDER_STATUS.PREPARING_ORDER ||
      status === ORDER_STATUS.ORDER_READY
    ) {
      if (remainingDayForDelivery > 0) {
        remainingDaysHeader = (
          <strong
            style={{
              color: statusColorCode,
            }}
          >
            Entrega {Moment(shipping.pickup_date).format('DD MMMM YYYY')}
          </strong>
        );
      } else {
        remainingDaysHeader = (
          <strong
            style={{
              color: statusColorCode,
            }}
          >
            Entrega el día siguiente hábil
          </strong>
        );
      }
    }

    return (
      <div className="tracking">
        {/* HEADER of ORDER STATUS */}
        <div className="tracking">
          <div className="tracking__header">
            <p className="follow-up">
              {status === ORDER_STATUS.NEW_ORDER
                ? 'Nueva Orden'
                : 'Seguimiento de la Orden'}
              <br />
              {remainingDaysHeader}
            </p>
          </div>
        </div>

        {/* BREADCRUMBS FOLLOW-UP */}
        <div className="tracking__status navigation">
          <div className="navigation__breadcrumbs">
            <ul className="steps">
              <React.Fragment>
                {hasPreparationProcess && (
                  <li className="steps__item">
                    <div className={currentStep === 1 ? 'title active' : 'title'}>
                      {currentStep === 1 ? 'Preparando Productos' : ''}
                    </div>
                  </li>
                )}
                <li className="steps__item">
                  <div className={currentStep === 2 ? 'title active' : 'title'}>
                    {currentStep === 2 ? 'Programar Recolección' : ''}
                  </div>
                </li>
                <li className="steps__item">
                  <div className={currentStep === 3 ? 'title active' : 'title'}>
                    {currentStep === 3 ? 'Orden en Tránsito' : ''}
                  </div>
                </li>
              </React.Fragment>
            </ul>
          </div>
        </div>

        {/* EXPLANATION/INFO ABOUT CURRENT STEP */}
        <div>
          <StatusInfo
            status={status}
            order={order}
            shipping={shipping}
            // in={state === 'entered'}
            // transitionStatus={state}
            changeStatus={this.changeStatus}
            doTransition={this.doTransition}
            requestShipment={this.requestShipment}
          />

          {/* <CSSTransition
                        in={showTransition}
                        timeout={1000}
                        classNames="move"
                        // appear
                        // onExited={() => {
                        //     this.setState({
                        //         showTransition: true,
                        //     });
                        //   }}
                    >
                        {state => (
                            <StatusInfo
                            status={this.props.status} 
                            in={state === 'entered'}
                            transitionStatus={state}
                            shippingMethod={this.props.shippingMethod}
                            changeStatus={this.changeStatus}
                            doTransition={this.doTransition}
                            requestShipment={this.requestShipment}
                            />  
                        )}
                    </CSSTransition> */}
        </div>
      </div>
    );
  }
}

// Pass Redux state and actions to component
function mapStateToProps(state) {
  return {};
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      updateStatusOrder: ordersActions.updateStatusOrder,
    },
    dispatch,
  );
}
export default connect(mapStateToProps, mapDispatchToProps)(OrderStatus);
