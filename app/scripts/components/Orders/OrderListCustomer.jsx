import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import queryString from 'query-string';
import PropTypes from 'prop-types';

import { ordersActions } from '../../Actions';
import withPagination from '../../components/hocs/withPagination';
import { IconPreloader } from '../../Utils/Preloaders';
import OrderCustomer from './OrderCustomer';
import { userTypes } from '../../Constants';

const MAX_ITEMS_PER_PAGE = 8;
const PaginatedList = withPagination(OrderCustomer);

class OrderListCustomer extends React.Component {
  //PropTypes Definition
  static propTypes = {
    orders: PropTypes.shape({
      results: PropTypes.array.isRequired,
    }).isRequired,
  };
  static defaultProps = {
    orders: {
      results: [],
    },
  };

  constructor(props) {
    super(props);
    this.currentPage = this.getResultsPageFromQueryString(
      this.props.location.search,
    );
  }
  componentDidMount() {
    this.performSearch(this.currentPage);
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.location.search !== nextProps.location.search) {
      this.performSearch(
        this.getResultsPageFromQueryString(nextProps.location.search),
      );
    }
  }

  getResultsPageFromQueryString = (query) => {
    if (query) {
      const resultsPage = parseInt(queryString.parse(query).p, 10);
      return !isNaN(resultsPage) ? resultsPage : 1;
    }
    return 1;
  };

  performSearch = (page = 1) => {
    // Scroll to top of page
    // TODO: Scroll to top of results list instead of top of document.
    window.scrollTo(0, 0);
    const query = `page=${page}&page_size=${MAX_ITEMS_PER_PAGE}`;
    this.currentPage = parseInt(page, 10);
    this.props.getOrders(userTypes.CUSTOMER, query);
  };

  render() {
    const { orders } = this.props;
    const currentPage = this.currentPage;

    return (
      <div className="orderList">
        <div className="orderList_header">
          <p className="title">Mis compras</p>
          {/*<div className="search">
                        <form>
                            <fieldset>
                                <input type="text" placeholder="Buscar órdenes" />
                            </fieldset>
                        </form>
                    </div>*/}
        </div>

        {orders.results.length > 0 ? (
          <div className="orders-list">
            <PaginatedList
              items={orders.results}
              baseLocation="/users/orders?"
              maxItems={MAX_ITEMS_PER_PAGE}
              action={this.testingFunction}
              totalPages={orders.npages}
              page={currentPage}
            />
          </div>
        ) : orders.loading && !orders.results ? (
          <IconPreloader />
        ) : (
          <p>Aún no has realizado ninguna compra.</p>
        )}
      </div>
    );
  }
}
// Load Data for Server Side Rendering
OrderListCustomer.loadData = (reduxStore, routePath) => {
  const { match } = routePath;
  const resultsPage = match.params.p;
  const ordersQuery = `page=${resultsPage}&page_size=${MAX_ITEMS_PER_PAGE}`;
  const promises = [
    reduxStore.dispatch(ordersActions.getOrders(userTypes.CUSTOMER, ordersQuery)),
  ];
  return Promise.all(promises.map((p) => (p.catch ? p.catch((e) => e) : p)));
};

function mapStateToProps() {
  return {};
}
function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      getOrders: ordersActions.getOrders,
    },
    dispatch,
  );
}

// Export Component
export default connect(mapStateToProps, mapDispatchToProps)(OrderListCustomer);
