import Moment from 'dayjs';
import { connect } from 'react-redux';
import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Sentry from '../../Utils/Sentry';
import { bindActionCreators } from 'redux';
import {
  Field,
  reduxForm,
  FieldArray,
  getFormValues,
  getFormSyncErrors,
} from 'redux-form';

import { ORDERS_EMAIL, SHIPPING_METHODS } from '../../Constants/config.constants';
import { ORDER_STATUS } from '../../Constants/orders.constants';
import { ordersActions, statusWindowActions } from '../../Actions';
import { getCartProductsByStoreAndShipping } from '../../Reducers';
import { InputField } from './../../Utils/forms/formComponents';
import { required } from '../../Utils/forms/formValidators';
import validateDHLForm from './validateDHLForm';
import { ActionButton } from '../../Utils/ActionButton';
import promise from 'redux-promise-middleware';
import { IconPreloader } from '../../Utils/Preloaders';

const PACKAGE_TEMPLATE = {
  width: 10,
  height: 10,
  depth: 10,
  weight: 1,
};

const renderField = ({
  input,
  type,
  autoFocus,
  placeholder,
  fieldName,
  label = null,
  label2 = null,
  autoShowErrors = false,
  meta: { touched, error, warning },
  ...rest
}) => (
  <div className={`form__data form__data-${fieldName}`}>
    <input
      {...input}
      {...rest}
      id={input.name}
      placeholder={placeholder}
      type={type}
      autoFocus={autoFocus}
    />
    {label && (
      <label htmlFor={input.name} style={{ fontSize: '1em' }}>
        {label} {label2}
      </label>
    )}
    {(autoShowErrors || touched) &&
      ((error && <div className="form_status danger">{error}</div>) ||
        (warning && <div className="form_status warning">{warning}</div>))}
  </div>
);

const generalValidation = (value) => {
  let error = '';

  if (value === undefined) {
    error = 'No puedes dejar un campo vacío';
  } else {
    if (isNaN(value)) {
      error = 'El valor debe ser un número';
    }
    if (value < 1) {
      error = 'El valor no puede ser mayor a 1';
    }
  }
  return error;
};

// const lessThanThousand = value =>
//     value && value > 1000 ? 'Escribe un número menor o igual a 1000.' : undefined;

const lessThanTen = (value) =>
  value && value > 10 ? 'Escribe un número menor o igual a 10.' : undefined;

const lessThanForty = (value) =>
  value && value > 40 ? 'Escribe un número menor o igual a 40.' : undefined;

const lessThanHundredFifty = (value) =>
  value && value > 150 ? 'Escribe un número menor o igual a 150.' : undefined;

const GuidesList = ({ fields }) => (
  <React.Fragment>
    <div
      className="packages-number form__data"
      style={{ justifyContent: 'flex-start' }}
    >
      <label htmlFor="num-packages">
        Elige el número y características de cada paquete que enviarás:
      </label>
    </div>

    <div style={{ textAlign: 'left' }}>
      <select
        name="num-packages"
        style={{ width: '3.7em', height: '2.7em', marginLeft: '0.6em' }}
        onChange={(e) => {
          e.preventDefault();
          const value = e.target.value;
          if (value > fields.length) {
            const guidesToShow = value - fields.length;
            for (let i = 0; i < guidesToShow; i++) {
              fields.push(PACKAGE_TEMPLATE);
            }
          }
          const guidesToShow = fields.length - value;
          for (let i = 0; i < guidesToShow; i++) {
            fields.pop();
          }
        }}
      >
        <option value={1} selected>
          1
        </option>
        {/* <option value={2}>2</option>
        <option value={3}>3</option>
        <option value={4}>4</option>
        <option value={5}>5</option> */}
      </select>
    </div>

    <div className="guides">
      {fields.map((guide, index) => (
        <div className="guide" key={index}>
          <div className="guide__index">Caja {index + 1}:</div>
          <div className="specs">
            <Field
              name={`${guide}.weight`}
              component={renderField}
              autoShowErrors={false}
              validate={[required, lessThanTen]}
              min="1"
              max="10"
              placeholder="kg"
              label="Peso"
              label2="kg"
              style={{ width: '3.75em', height: '2.75em' }}
            />
            <Field
              name={`${guide}.width`}
              component={renderField}
              autoShowErrors={false}
              validate={[required, lessThanForty]}
              placeholder="cm"
              min="1"
              max="45"
              label="Ancho"
              label2="cm"
              style={{ width: '3.75em', height: '2.75em' }}
            />
            <Field
              name={`${guide}.height`}
              component={renderField}
              autoShowErrors={false}
              validate={[required, lessThanForty]}
              placeholder="cm"
              min="1"
              max="45"
              label="Alto"
              label2="cm"
              style={{ width: '3.75em', height: '2.75em' }}
            />
            <Field
              name={`${guide}.depth`}
              component={renderField}
              autoShowErrors={false}
              validate={[required, lessThanHundredFifty]}
              placeholder="cm"
              min="1"
              max="45"
              label="Profundidad"
              label2="cm"
              style={{ width: '3.75em', height: '2.75em' }}
            />
          </div>
        </div>
      ))}
    </div>
  </React.Fragment>
);

class StatusInfo extends Component {
  state = {
    loadingLabel: false,
  };
  /**
   * submitForm()
   */
  submitForm = () => {
    this.setState({ loadingLabel: true });

    const { order, formValues } = this.props;

    let guides = formValues.guides;

    guides = guides.map((g) => ({
      depth: Math.ceil(g.depth),
      width: Math.ceil(g.width),
      height: Math.ceil(g.height),
      weight: Math.ceil(g.weight), // / 1000
    }));

    // Make request to API
    this.props
      .requestDhlGuides(order.uid, { packages: guides })
      .then((response) => {
        this.props.openStatusWindow({
          type: 'success',
          message: 'La generación de guías se realizó correctamente.',
        });
      })
      .catch((error) => {
        if (error?.response?.status > 400) {
          this.onStatusError();
          Sentry.captureException(error);
        }
      })
      .finally(() => {
        this.setState({ loadingLabel: false });
      });
  };

  requestPickup = () => {
    const { order, scheduleDHLPickup } = this.props;

    // Make request to API
    scheduleDHLPickup(order.uid)
      .then((response) => {
        this.props.openStatusWindow({
          type: 'success',
          message: 'La recolección se programó correctamente.',
        });
      })
      .catch((error) => {
        if (error?.response?.status > 400) {
          this.onStatusError();
          Sentry.captureException(error);
        }
      });
  };

  onStatusError = () => {
    this.props.openStatusWindow({
      type: 'error',
      message: 'Error. Por favor, intenta más tarde.',
    });
  };

  renderLabelGenerator = () => {
    const { formErrors, submitFailed } = this.props;
    return (
      <React.Fragment>
        <p className="instructions">
          Es necesario que crees e imprimas una guía de rastreo por cada paquete que
          enviarás a tus clientes.
          <br />
          Sin ellas, el servicio de mensajería no podrá recolectar tu orden.
        </p>
        <div className="standard-shipping">
          <div className="steps active">
            <form className="form" style={{ marginTop: '2em' }}>
              <FieldArray name="guides" component={GuidesList} />
              <div className="form__data form__submit">
                {submitFailed &&
                  Object.keys(formErrors).length > 0 &&
                  Object.keys(formErrors).map((errKey) => {
                    const err = formErrors[errKey];

                    if (errKey === 'guides') {
                      return err.map((e) =>
                        Object.keys(e).reduce(
                          (acc, key) => (
                            <React.Fragment>
                              {acc}
                              <br />
                              {key}: {e[key]}
                            </React.Fragment>
                          ),
                          '',
                        ),
                      );
                    }

                    return `${errKey}: ${err}`;
                  })}
                {this.state.loadingLabel ? (
                  <div
                    style={{ position: 'relative', height: '55px', top: '-72px' }}
                  >
                    <button
                      className="c2a_border"
                      style={{ transform: 'rotate(90deg)' }}
                    >
                      <IconPreloader />
                    </button>
                  </div>
                ) : (
                  <ActionButton
                    icon="loading"
                    className="c2a_border action-button"
                    disabled={Object.keys(formErrors).length}
                    onClick={this.submitForm}
                    style={{ marginTop: '0.5em' }}
                  >
                    Generar guías
                  </ActionButton>
                )}
              </div>
            </form>
          </div>
        </div>
      </React.Fragment>
    );
  };

  /**
   * render()
   */
  render() {
    const { order, doTransition, changeStatus, formErrors } = this.props;

    const shipping = order.physical_properties;
    // ******************
    const nationwideShippingDetails = shipping.shipment_dhl;
    // *********************
    const nationwidePickupDetails = shipping.pickup_dhl;

    const isExpress =
      shipping.shipping_method_slug === SHIPPING_METHODS.EXPRESS_BIKE.slug ||
      shipping.shipping_method_slug === SHIPPING_METHODS.EXPRESS_CAR.slug;

    const status = shipping.status.value;
    const currentDay = Moment();
    switch (status) {
      case ORDER_STATUS.NEW_ORDER:
        return (
          <div className="tracking__status-info">
            <div className="new-order-info">
              <div className="title-step-container">
                <h5 className="title-step newOrder">Nueva orden</h5>
                <p className="message">¡Felicidades tienes una nueva venta!</p>
              </div>
              <p className="instructions">
                Manos a la obra.&nbsp;
                {/*Da <span>click al botón</span> para continuar con el proceso de tu venta.*/}
              </p>
              <ActionButton
                icon="loading"
                className="c2a_border action-button"
                onClick={() => {
                  doTransition();
                  changeStatus();
                }}
              >
                Comenzar Orden
              </ActionButton>
              {/* <button
                            className="c2a_border"
                            onClick={() => { doTransition(); changeStatus(); }}
                        >Comenzar Orden</button> */}
            </div>
          </div>
        );

      case ORDER_STATUS.PREPARING_ORDER: {
        const remainingDayForDelivery = Moment(shipping.pickup_date).diff(
          currentDay,
          'days',
        );
        const COLOR_CODES = {
          BYPASS: '#1eb592',
          WARNING: '#ceb52b',
          ALERT: '#e6575d',
        };
        let statusColorCode = COLOR_CODES.BYPASS;
        if (remainingDayForDelivery < 3) statusColorCode = COLOR_CODES.WARNING;
        if (remainingDayForDelivery < 2) statusColorCode = COLOR_CODES.ALERT;

        return (
          <div className={'tracking__status-info'}>
            <div className="producing-order-info">
              <div className="title-step-container">
                <h5 className="title-step handmade">Elaboración</h5>
                <p className="message">Elaborando con amor</p>
              </div>
              <p>
                {' '}
                <strong> Recolección:&nbsp;</strong>
                {Moment(shipping.pickup_date).format('DD MMMM YYYY')}
              </p>
              <p className="instructions">
                Es momento de <strong>preparar los productos</strong> de tu venta.
                <br />
                {/* Tienes&nbsp;
                                <strong
                                    style={{
                                        color: statusColorCode
                                    }}
                                >{Moment(shipping.pickup_date).format('DD MMMM YYYY')}</strong>&nbsp; */}
                Preparar, empaca y ten listo tu producto para la recolección.
              </p>
              <div className="container">
                <ActionButton
                  icon="loading"
                  className="c2a_border action-button"
                  onClick={() => {
                    doTransition();
                    changeStatus();
                  }}
                >
                  Mi orden está lista para recolección
                </ActionButton>
              </div>
            </div>
          </div>
        );
      }

      case ORDER_STATUS.ORDER_READY: {
        //Can order change status from 'order_ready' to 'vendor_awaiting_shipment'
        const canRequestPickup = shipping.can_store_change_status_to !== null;
        // const canRequestPickup = null;

        return (
          <div className={'tracking__status-info'}>
            {/* IF SHIPPING IS EXPRESS */}
            {shipping.shipping_method_slug === SHIPPING_METHODS.EXPRESS_BIKE.slug ||
            shipping.shipping_method_slug === SHIPPING_METHODS.EXPRESS_CAR.slug ? (
              <div className="pickup-order-info">
                <div className="title-step-container">
                  <h5 className="title-step pickup">Recolección</h5>
                  <p className="message name">Envío Express</p>
                </div>

                <p className="instructions">
                  <strong>¡Todo listo!</strong> La recolección de tu venta será
                  programada para el día <br />
                  <strong>
                    {Moment(shipping.pickup_date).format('DD MMMM YYYY')}
                  </strong>
                  .
                  <br />
                  Para evitar contratiempos, no olvides tener tu pedido listo y
                  etiquetado con el
                  <strong> número de orden {order.uid}</strong>.<br />
                  Si tienes cualquier duda sobre cómo preparar tu pedido,&nbsp;
                  contáctanos <a href={`mailto="${ORDERS_EMAIL}"`}>{ORDERS_EMAIL}</a>
                  .
                </p>
                <div className="container">
                  <ActionButton
                    disabled={!canRequestPickup}
                    icon="loading"
                    className="c2a_border action-button"
                    onClick={() => {
                      doTransition();
                      changeStatus();
                    }}
                  >
                    Programar Recolección
                  </ActionButton>
                  <br />

                  {/*!canRequestPickup && (
                    <div className="note">
                      El botón estará <strong>activo un día antes</strong> de la
                      fecha de recolección.
                    </div>
                  )*/}
                  <div className="pickup">
                    {' '}
                    <strong>Recolección:</strong>{' '}
                    {Moment(shipping.pickup_date).format('D MMMM YYYY')}
                  </div>
                </div>
              </div>
            ) : (
              // IF SHIPPING METHOD IS STANDARD/NATIONAL
              <div className="pickup-order-info packages">
                <div className="title-step-container">
                  <h5 className="title-step pickup">Recolección</h5>
                  <p className="message name">Envío Nacional</p>
                </div>

                {/* IF THE ORDER HAS NO DHL GUIDE(quote) */}
                {!nationwideShippingDetails ||
                !nationwideShippingDetails.hasOwnProperty('pdf_labels') ? (
                  this.renderLabelGenerator()
                ) : (
                  /* IF´VE FOUND A DHL GUIDE (quote) */
                  <div className="standard-shipping">
                    <div style={{ width: '100%', textAlign: 'center' }}>
                      <a
                        className="button-square--pink download-btn"
                        href={nationwideShippingDetails.pdf_labels}
                        target="_blank"
                        rel="noopener noreferrer"
                      >
                        Descargar guías
                      </a>
                    </div>

                    <div className="steps active">
                      {/*<div>
                        <p>
                            En caso de error, da click aquí para generar nuevamente tus guías
                            <button onClick={() => { this.setState({ accurateGuides: false }); }}>Generar de nuevo</button>
                        </p>
                    </div>*/}
                      <div className="instructions">
                        <p className="message">
                          ¡Listo!, ahora que ya{' '}
                          <strong>cuentas con tus guías</strong> puedes dar{' '}
                          <strong> click aquí</strong> para que programemos la
                          recolección de tu pedido
                        </p>
                        <ActionButton
                          disabled={!canRequestPickup}
                          icon="loading"
                          className="c2a_border collect-btn"
                          onClick={this.requestPickup}
                        >
                          Listo para recolección
                        </ActionButton>

                        {/*!canRequestPickup && (
                          <p className="note">
                            El botón estará <strong>activo un día antes</strong>{" "}
                            de la fecha de recolección.
                          </p>
                        )*/}
                      </div>
                    </div>
                  </div>
                )}
              </div>
            )}
          </div>
        );
      }

      case ORDER_STATUS.AWAITING_SHIPMENT:
        return !isExpress && !nationwideShippingDetails ? (
          <div>
            Lo sentimos pero se encontró un problema con tus guías de recolección.
            <br />
            Por favor, comunícate con nosotros para ayudarte con tu orden.
          </div>
        ) : (
          <div className="tracking__status-info">
            <div style={{ width: '100%', textAlign: 'center' }}>
              <h5 className="title-step pickup">Esperando Recolección</h5>
              ¡Listo!
              <br /> La recolección de tu pedido está programada para&nbsp;
              {Moment(shipping.pickup_date).format('D MMMM YYYY')}
              .<br />
              {/* DOWNLOAD DHL QUOTES */}
              {!isExpress &&
                (nationwideShippingDetails &&
                !nationwideShippingDetails.pdf_labels ? null : (
                  <a
                    className="button-square--pink download-btn"
                    href={nationwideShippingDetails.pdf_labels}
                    target="_blank"
                    rel="noopener noreferrer"
                    style={{ display: 'inline-block', margin: '0.5em auto' }}
                  >
                    Descargar guías
                  </a>
                ))}
              {/* END: DOWNLOAD DHL QUOTES */}
              {/* INSTRUCTIONS TO USER */}
              <p className="instructions">
                Para evitar contratiempos, no olvides tener tu pedido listo y
                etiquetado con el
                <strong> número de orden {order.uid}</strong>.<br />
                Si tienes cualquier duda sobre cómo preparar tu pedido,&nbsp;
                contáctanos <a href={`mailto="${ORDERS_EMAIL}"`}>{ORDERS_EMAIL}</a>.
              </p>
              {/* END: INSTRUCTIONS TO USER */}
              {/* PICKUP DETAILS */}
              {!isExpress && (
                <div className="info_pickup">
                  <p>
                    <span>Recolector:</span>Fedex
                  </p>
                  {nationwideShippingDetails.airway_bill_number && (
                    <p>
                      <span>Código de rastreo: </span>
                      {nationwideShippingDetails.airway_bill_number}
                    </p>
                  )}

                  <p>
                    <span>Fecha de recolección: </span>
                    {/*Moment(nationwidePickupDetails.next_pickup_day).format('D MMMM YYYY')*/}
                    {Moment(shipping.pickup_date).format('D MMMM YYYY')}
                  </p>
                </div>
              )}
              {/* END: PICKUP DETAILS */}
            </div>
          </div>
        );

      case ORDER_STATUS.ORDER_IN_TRANSIT:
        return (
          <div className="tracking__status-info">
            <div>
              <div className="title-step-container">
                <h5 className="title-step in-transit">Orden en Tránsito</h5>
              </div>
              <p className="instructions">
                Tu venta <strong>{order?.uid}</strong> ya va en camino a su destino.
              </p>

              {shipping?.shipping_method_slug === SHIPPING_METHODS.STANDARD.slug && (
                <div className="info_pickup">
                  <p>
                    <span>Recolector: </span>Fedex
                  </p>
                  <p>
                    <span>Código de rastreo: </span>
                    {nationwideShippingDetails?.airway_bill_number}
                  </p>
                  <p>
                    <span>Número de Confirmación: </span>
                    {nationwidePickupDetails?.confirmation_number}
                  </p>
                  <p>
                    <span>Fecha de recolección: </span>
                    {/*Moment(nationwidePickupDetails.next_pickup_day).format('DD MMMM YYYY')*/}
                    {Moment(shipping?.shipping_date).format('D MMMM YYYY')}
                  </p>
                </div>
              )}
            </div>
          </div>
        );

      default:
        return <div className={`tracking__status-info`}>No disponible</div>;
    }
  }
}

// Decorate the form component
StatusInfo = reduxForm({
  form: 'DHL_form',
  destroyOnUnmount: true, // <---- Preserve data. Dont destroy form on unmount
  enableReinitialize: true, // <---- Allow updates when 'initValues' prop change
  keepDirtyOnReinitialize: true, // <---- Prevent 'dirty' fields to update
  updateUnregisteredFields: true, // <---- Update unregistered fields.
  forceUnregisterOnUnmount: true, // <---- Unregister fields on unmount
  validate: (values) => {
    const errors = {};

    if (!values['num-packages'].value < 1) {
      errors['num-packages'] =
        'Es necesario incluir al menos un paquete en tu orden.';
    }
    if (!values.guides.length) {
      errors['num-packages'] =
        'Es necesario incluir al menos un paquete en tu orden.';
    }

    const membersArrayErrors = [];
    values.guides.forEach((guide, memberIndex) => {
      const memberErrors = {};

      // width
      if (!guide && guide.width < 10) {
        memberErrors.width = 'El ancho de tu caja debe medir mínimo 10cm.';
        membersArrayErrors[memberIndex] = memberErrors;
      }

      // height
      if (!guide && guide.height < 10) {
        memberErrors.height = 'El alto de tu caja debe medir mínimo 10cm.';
        membersArrayErrors[memberIndex] = memberErrors;
      }

      // depth
      if (!guide && guide.depth < 10) {
        memberErrors.depth = 'La profundidad de tu caja debe medir mínimo 10cm.';
        membersArrayErrors[memberIndex] = memberErrors;
      }
    });
    if (membersArrayErrors.length) {
      errors.guides = membersArrayErrors;
    }

    return errors;
  },
})(StatusInfo);

// Pass Redux state and actions to component
function mapStateToProps(state, ownProps) {
  return {
    initialValues: {
      orderID: ownProps.order.uid,
      guides: [PACKAGE_TEMPLATE],
      'num-packages': 1,
    },
    formValues: getFormValues('DHL_form')(state),
    formErrors: getFormSyncErrors('DHL_form')(state),
  };
}

function mapDispatchToProps(dispatch) {
  const { requestDhlGuides, scheduleDHLPickup } = ordersActions;
  return bindActionCreators(
    {
      requestDhlGuides,
      scheduleDHLPickup,
      openStatusWindow: statusWindowActions.open,
    },
    dispatch,
  );
}

// Export Component
export default connect(mapStateToProps, mapDispatchToProps)(StatusInfo);
