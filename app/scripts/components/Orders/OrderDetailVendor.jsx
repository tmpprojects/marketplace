import Moment from 'dayjs';
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Link } from 'react-router-dom';

import { formatNumberToPrice } from '../../Utils/normalizePrice';
import { ResponsiveImage } from '../../Utils/ImageComponents';
import {
  formatDate,
  addHourPeriodSuffix,
  formatDateToUTC,
} from '../../Utils/dateUtils';
import { Tooltip } from '../../Utils/Tooltip';
import { getLocalOrderStatus } from '../../Utils/orders/ordersUtils';
import { LOCAL_ORDER_STATUS, ORDER_STATUS } from '../../Constants/orders.constants';
import OrderStatus from './OrderStatus';
import { appActions, ordersActions, myStoreActions } from '../../Actions';
import { SHIPPING_METHODS, userTypes } from '../../Constants';
import { IconPreloader } from '../../Utils/Preloaders';

class OrderDetailVendor extends Component {
  componentDidMount() {
    this.props.getStoreMovements();
    this.props.getOrders(userTypes.VENDOR, 'page_size=250');
    this.props.getFullOrder(this?.props?.match?.params?.orderID);
  }
  getStatusClassName = (status) => {
    switch (status) {
      case ORDER_STATUS.NEW_ORDER:
        return 'new';
      case ORDER_STATUS.PREPARING_ORDER:
        return 'preparing';
      case ORDER_STATUS.ORDER_READY:
        return 'ready';
      case ORDER_STATUS.AWAITING_SHIPMENT:
        return 'awaiting-shipment';
      case ORDER_STATUS.ORDER_IN_TRANSIT:
        return 'transit';
      case ORDER_STATUS.AWAITING_SHIPMENT:
        return 'vendor_awaiting_shipment';
      default:
        return 'other';
    }
  };

  displayStatus = (status) => {
    const className = `status-display--${this.getStatusClassName(status)}`;
    switch (status) {
      case ORDER_STATUS.NEW_ORDER:
        return <div className={`${className} action-button`}>Comenzar Orden </div>;
      case ORDER_STATUS.PREPARING_ORDER:
        return (
          <span className={`status-display ${className}`}>Preparando Orden</span>
        );
      case ORDER_STATUS.ORDER_READY:
        return <span className={`status-display ${className}`}>Orden Lista</span>;
      case ORDER_STATUS.AWAITING_SHIPMENT:
        return (
          <span className={`status-display ${className}`}>
            Esperando Recolección
          </span>
        );
      case ORDER_STATUS.ORDER_IN_TRANSIT:
        return (
          <span className={`status-display ${className}`}>Orden en Tránsito</span>
        );
      case ORDER_STATUS.CANCELLED:
        return (
          <span className={`status-display ${className}`}>Orden Cancelada</span>
        );
      default:
        return <span className={`status-display ${className}`}>Entregada</span>;
    }
  };

  render() {
    const { orders, movements, fullOrder, ...props } = this.props;

    const orderID = props?.match?.params?.orderID;

    const payouts =
      fullOrder?.order?.balance_orders_stats?.map((orderBalance) => orderBalance) ||
      [];
    const orderDisplay = fullOrder?.order || {};

    const order = orders?.results?.find((o) => o?.uid === orderID) || '';

    // const productsCount = order?.products?.reduce((a, b) => a + b.quantity, 0);
    const localOrderStatus = getLocalOrderStatus(
      order?.physical_properties?.status?.value,
    );
    const apiOrderStatus = order?.physical_properties?.status?.value;

    return (
      <div className="storeApp__module storeApp__orders orderDetail">
        <Link to="/my-store/orders" className="button_view detail">
          Regresar a mis ventas
        </Link>

        <div className="header">
          <p className="title">Orden: {orderID || orderDisplay?.uid}</p>
          <p className="status">
            Status:&nbsp;
            {orders?.loading ? '' : this.displayStatus(apiOrderStatus)}
          </p>
        </div>

        <section className="order">
          {/* PICKUP DATE */}
          {orders?.loading ? (
            <IconPreloader />
          ) : (
            <React.Fragment>
              <div className="order_pickup-date">
                <span className="status">Fecha de recolección: </span>
                {Moment(orderDisplay?.physical_properties?.pickup_date).format(
                  'DD MMMM YYYY',
                )}

                {orderDisplay?.physical_properties?.shipping_method_slug !==
                  SHIPPING_METHODS.STANDARD?.slug && (
                  <React.Fragment>
                    &nbsp;|&nbsp;
                    {`${addHourPeriodSuffix(
                      order?.physical_properties?.shipping_schedule?.collection_start
                        .split(':')
                        .slice(0, -1)
                        .join(':'),
                    )} -  ${addHourPeriodSuffix(
                      order?.physical_properties?.shipping_schedule?.collection_end
                        .split(':')
                        .slice(0, -1)
                        .join(':'),
                    )}`}
                  </React.Fragment>
                )}
              </div>

              {/* END: PICKUP DATE

          {/* ORDER STATUS FLOW */}
              {localOrderStatus === LOCAL_ORDER_STATUS.IN_PROCESS ? (
                <div
                  className="store-order"
                  style={{
                    marginBottom: '1.5em',
                  }}
                >
                  <OrderStatus order={order} />
                </div>
              ) : null}
              {/* END: ORDER STATUS FLOW */}

              {/* ORDER DETAILS */}
              <div className="order_status">
                <div>
                  <span className="status">Realizada: </span>
                  {formatDate(formatDateToUTC(new Date(orderDisplay?.created)))}
                </div>
              </div>
              {/* END: ORDER DETAILS */}
              {/* GIFT DETAILS */}
              {orderDisplay?.order_group?.receiver_note !== '' && (
                <div
                  className="order_status"
                  style={{
                    flexFlow: 'column nowrap',
                    padding: '1em',
                    backgroundColor: '#faf7f5',
                  }}
                >
                  <b style={{ fontWeight: 'bold', marginBottom: '.25em' }}>
                    Esta orden es un regalo:
                  </b>
                  <div className="cr__text--paragraph">
                    <b style={{ fontWeight: 'bold' }}>Para:</b>
                    {` ${orderDisplay?.order_group?.receiver_name}`}
                  </div>
                  <div className="cr__text--paragraph">
                    <b style={{ fontWeight: 'bold' }}>Mensaje:</b>
                    <br />
                    {`${
                      orderDisplay?.order_group?.receiver_note === null
                        ? 'No hay mensaje'
                        : order?.order_group?.receiver_note
                    }`}
                  </div>
                </div>
              )}
              {/* END: GIFT DETAILS */}
            </React.Fragment>
          )}

          {/* PRICING DETAILS */}
          <div className="order_products">
            {!fullOrder?.loading && (
              <article className="product_container">
                {orderDisplay?.products?.map((product) => (
                  <div
                    className="product"
                    key={product?.product?.slug}
                    style={{
                      borderBottom: 'solid 1px #eee',
                      paddingBottom: '1em',
                    }}
                  >
                    <div className="product_detail">
                      <div className="product_detail-img">
                        <div className="img_container">
                          {product?.product?.photos[0] &&
                            'photo' in product?.product?.photos[0] && (
                              <ResponsiveImage
                                src={product?.product?.photos[0]?.photo}
                                alt={product?.product?.name}
                              />
                            )}
                        </div>
                      </div>

                      <div className="product_detail-data">
                        <p className="name">{product?.product?.name}</p>
                        {/*<p className="store">{product.product.store.name}</p>*/}
                        <p className="attribute">
                          <span>
                            {product?.attribute_names
                              ? `Detalle: ${product?.attribute_names}`
                              : ''}
                          </span>
                        </p>
                      </div>
                    </div>

                    <div className="product_price">
                      <p className="quantity">
                        Cantidad <span>{product?.quantity}</span>
                      </p>
                      <p className="price">
                        {formatNumberToPrice(product?.total_price)}MX
                      </p>
                    </div>

                    {product?.note ? (
                      <div className="product_notes">
                        <p className="subtitle">Notas</p>
                        <p className="note">{product?.note}</p>
                      </div>
                    ) : null}
                  </div>
                ))}
              </article>
            )}

            {!fullOrder?.loading && (
              <div className="total_container">
                <div className="total_price">
                  <p className="breakdown">
                    Precio de la orden
                    <Tooltip message="Venta del total de productos." />
                    <span className="quantity">
                      {parseFloat(payouts[0]?.order_amount, 10)?.toFixed(2)} MX
                    </span>
                  </p>
                  <p className="breakdown">
                    Precio de envío
                    <Tooltip message="Costo de envío." />
                    {payouts[0]?.shipment > 0 ? (
                      <span className="quantity">
                        -{parseFloat(payouts[0]?.shipment, 10)?.toFixed(2)} MX
                      </span>
                    ) : (
                      <span className="quantity">N/A</span>
                    )}
                  </p>
                  <p className="breakdown">
                    Promociones
                    {/* <Tooltip message='' /> */}
                    {payouts[0]?.discount_seller > 0 ? (
                      <span className="quantity">
                        -{parseFloat(payouts[0]?.discount_seller, 10)?.toFixed(2)} MX
                      </span>
                    ) : (
                      <span className="quantity">N/A</span>
                    )}
                  </p>

                  <p className="breakdown">
                    Comisión Canasta Rosa
                    <Tooltip message="Comisión por venta y pasarela de pago." />
                    <span className="quantity">
                      -{parseFloat(payouts[0]?.commission, 10)?.toFixed(2)} MX
                    </span>
                  </p>

                  <p className="breakdown">
                    Retenci&oacute;n IVA
                    <Tooltip message="Retención de IVA correspondiente a tus producto." />
                    {payouts[0]?.shipment > 0 ? (
                      <span className="quantity">
                        -{parseFloat(payouts[0]?.iva, 10)?.toFixed(2)} MX
                      </span>
                    ) : (
                      <span className="quantity">N/A</span>
                    )}
                  </p>

                  <p className="breakdown">
                    Retenci&oacute;n ISR
                    <Tooltip message="Retención de ISR correspondiente a tu entidad fiscal." />
                    {payouts[0]?.shipment > 0 ? (
                      <span className="quantity">
                        -{parseFloat(payouts[0]?.it, 10)?.toFixed(2)} MX
                      </span>
                    ) : (
                      <span className="quantity">N/A</span>
                    )}
                  </p>

                  <p className="breakdown total">
                    Total
                    <span className="quantity">
                      {parseFloat(payouts[0]?.total, 10)?.toFixed(2)} MX
                    </span>
                  </p>
                </div>
              </div>
            )}

            <p className="breakdown total noInvoice">
              *Esto no es una factura y su uso es meramente informativo.
            </p>
          </div>
          {/* END: PRICING DETAILS */}
        </section>
      </div>
    );
  }
}

function mapStateToProps(state) {
  const { myStore, orders } = state;
  return {
    movements: myStore.movements,
    fullOrder: orders.fullOrder,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      getStoreMovements: myStoreActions.getStoreMovements,
      getOrders: ordersActions.getOrders,
      // getStoreMovements: ordersActions.getOrderStats,
      updateStatusOrder: ordersActions.updateStatusOrder,
      getFullOrder: ordersActions.getFullOrder,
    },
    dispatch,
  );
}

// Export Component
export default connect(mapStateToProps, mapDispatchToProps)(OrderDetailVendor);
