import Moment from 'dayjs';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import queryString from 'query-string';
import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { Link, NavLink } from 'react-router-dom';

import { ResponsiveImage } from '../../Utils/ImageComponents';
import { formatNumberToPrice } from '../../Utils/normalizePrice';
import { ordersActions } from '../../Actions';
import {
  categorizedOrdersList,
  getVendorOrdersList,
} from '../../Reducers/orders.reducer';
import { getLocalOrderStatus } from '../../Utils/orders/ordersUtils';
import { LOCAL_ORDER_STATUS, ORDER_STATUS } from '../../Constants/orders.constants';
import withPagination from '../hocs/withPagination';
import { SHIPPING_METHODS, userTypes } from '../../Constants';
import OrderVendor from './OrderVendor';
import { IconPreloader } from '../../Utils/Preloaders';

const MAX_ITEMS_PER_PAGE = 10;
const DEFAULT_SORT = '';
const PaginatedList = withPagination(OrderVendor);

// NAVIGATION & SORTING CONSTANTS
const TAB_VIEWS = {
  ACTIVE: 'ACTIVE',
  ARCHIVE: 'ARCHIVE',
  SEARCH: 'SEARCH',
  ALL: 'ALL',
};

const ACTIVE_ORDER_STATUS_SORT_ORDER = [
  ORDER_STATUS.NEW_ORDER,
  ORDER_STATUS.AWAITING_SHIPMENT,
  ORDER_STATUS.ORDER_READY,
  ORDER_STATUS.PREPARING_ORDER,
  ORDER_STATUS.ORDER_IN_TRANSIT,
];

/*------------------------------------------------
//    ORDERS LIST COMPONENT
------------------------------------------------*/
class OrderListVendor extends Component {
  static propTypes = {
    orders: PropTypes.object.isRequired,
  };
  static defaultProps = {
    orders: {},
  };
  constructor(props) {
    super(props);
    this.currentPage = this.getResultsPageFromQueryString(
      this.props.location.search,
    );
    this.filter = this.getFilterFromQueryString(this.props.location.search);
    this.state = {
      activeTab: TAB_VIEWS.ACTIVE,
      activeFilter: null,
      searchTerm: '',
    };
  }

  componentWillMount() {
    // Prepare the component for a search
    // if (this.props.match.params.filter !== undefined) {
    //     this.filterOrders(
    //         this.props.match.params.filter
    //     );
    // }
  }

  componentDidMount() {
    this.performSearch(this.currentPage, this.filter);
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.location.search !== nextProps.location.search) {
      this.performSearch(
        this.getResultsPageFromQueryString(nextProps.location.search),
        this.getFilterFromQueryString(nextProps.location.search),
      );
    }

    // If the pathname (URL) has changed, filter orders
    // if (this.props.match.params.filter !== nextProps.match.params.filter) {
    //     this.filterOrders(
    //         this.props.match.params.filter
    //     );
    // }
  }

  getResultsPageFromQueryString = (query) => {
    if (query) {
      const resultsPage = parseInt(queryString.parse(query).p, 10);
      return !isNaN(resultsPage) ? resultsPage : 1;
    }
    return 1;
  };

  /**
   * getFilterFromQueryString()
   * @param {string} query
   * Extracts and return filter criteria from a query string.
   */
  getFilterFromQueryString = (query) => {
    if (query && queryString.parse(query).filter) {
      return queryString.parse(query).filter;
    }
    return DEFAULT_SORT;
  };

  performSearch = (page = 1, filter = DEFAULT_SORT) => {
    window.scrollTo(0, 0);

    let query = '';
    if (filter === 'in_process' || filter === 'paid') {
      query = `order_status=${filter}&page=${page}&page_size=${MAX_ITEMS_PER_PAGE}`;
    } else {
      query = `physical_properties__status=${filter}&page=${page}&page_size=${MAX_ITEMS_PER_PAGE}`;
    }

    this.currentPage = parseInt(page, 10);
    this.filter = filter;

    //Send to API
    this.props.getOrders(userTypes.VENDOR, query).then((response) => {
      this.props.getOrderStats();
    });
  };

  filterOrders = (filter) => {
    const catOrders = categorizedOrdersList(this.props.orders);
    //Update filtered orders in state
    this.setState({
      activeFilter: filter, //catOrders[filter]
    });
  };

  orderInProcess = (status) => {
    if (
      status === ORDER_STATUS.NEW_ORDER ||
      status === ORDER_STATUS.PREPARING_ORDER ||
      status === ORDER_STATUS.ORDER_READY ||
      status === ORDER_STATUS.AWAITING_SHIPMENT ||
      status === ORDER_STATUS.ORDER_IN_TRANSIT
    ) {
      return 'in-process';
    } else if (status === ORDER_STATUS.DELIVERED) {
      return 'delivered';
    } else if (
      status === ORDER_STATUS.CANCELLED ||
      status === ORDER_STATUS.OTHER ||
      status === ORDER_STATUS.FRAUD
    ) {
      return 'cancelled';
    }
    return 'none';
  };

  displayStatus = (status) => {
    const className = `status-display--${this.getStatusClassName(status)}`;
    switch (status) {
      case ORDER_STATUS.NEW_ORDER:
        return <div className={`${className} action-button`}>Comenzar Orden ></div>;
      case ORDER_STATUS.PREPARING_ORDER:
        return (
          <p>
            <span className={`status-display ${className}`}>Preparando Orden</span>
          </p>
        );
      case ORDER_STATUS.ORDER_READY:
        return (
          <p>
            <span className={`status-display ${className}`}>Orden Lista</span>
          </p>
        );
      case ORDER_STATUS.AWAITING_SHIPMENT:
        return (
          <p>
            <span className={`status-display ${className}`}>
              Esperando Recolección
            </span>
          </p>
        );
      case ORDER_STATUS.ORDER_IN_TRANSIT:
        return (
          <p>
            <span className={`status-display ${className}`}>En Tránsito</span>
          </p>
        );
      default:
        return (
          <p>
            <span className={`status-display ${className}`}>Entregada</span>
          </p>
        );
    }
  };

  filterAndOrderResults = (
    orders,
    view = TAB_VIEWS.ALL,
    filter = null,
    searchTerm = '',
  ) => {
    let filteredOrders = orders;

    // Tab view.
    switch (view) {
      case TAB_VIEWS.ARCHIVE:
        filteredOrders = this.getInactiveOrders(orders);
        break;

      case TAB_VIEWS.ACTIVE:
        filteredOrders = this.getActiveOrders(orders);
        break;

      case TAB_VIEWS.ALL:
      default:
        filteredOrders = orders;
    }

    // Seach Box.
    if (searchTerm.length) {
      const reg = new RegExp(searchTerm, 'gi');
      filteredOrders = filteredOrders.filter((o) => o.uid.match(reg));
    }

    // Return remaining results.
    return filteredOrders;
  };

  getActiveOrders = (orders) => {
    // By default, only show all 'active/open' orders
    return orders.filter(
      (o) =>
        o.physical_properties.status.value !== ORDER_STATUS.CANCELLED &&
        o.physical_properties.status.value !== ORDER_STATUS.DELIVERED &&
        o.physical_properties.status.value !== ORDER_STATUS.OTHER &&
        o.physical_properties.status.value !== ORDER_STATUS.FRAUD,
    );
  };

  getInactiveOrders = (orders) => {
    const activeOrders = this.getActiveOrders(orders);

    // Get all non 'active' orders
    return orders.filter(
      (o) => activeOrders.findIndex((fo) => o.id === fo.id) === -1,
    );
  };

  searchOrders = (term) => {
    this.setState({
      activeFilter: null,
      activeTab: TAB_VIEWS.ALL,
      searchTerm: term,
    });
  };

  changeTabView = (view) => {
    this.setState({
      activeFilter: null,
      activeTab: view,
      searchTerm: '',
    });
  };

  onChangeFilter = (event) => {
    const { target } = event;

    this.props.history.push(
      `${this.props.location.pathname}?filter=${target.value}`,
    );
  };

  render() {
    const { orders, location } = this.props;
    const statsCount = orders.stats.count_by_order_status || {};

    const newOrder = ORDER_STATUS.NEW_ORDER;
    const orderInPreparation = ORDER_STATUS.PREPARING_ORDER;
    const orderReady = ORDER_STATUS.ORDER_READY;
    const orderAwaitingShipment = ORDER_STATUS.AWAITING_SHIPMENT;
    const orderInTransit = ORDER_STATUS.ORDER_IN_TRANSIT;
    const orderDelivered = ORDER_STATUS.DELIVERED;

    // Render Component
    return (
      <section className="storeApp__module storeApp__orders orderListUI">
        <div className="orderListUI__header">
          <h3 className="title">Mis Órdenes</h3>

          <nav className="nav_filters">
            <select name="filters" onChange={this.onChangeFilter}>
              <option value={DEFAULT_SORT}>Todas ({statsCount.total_orders})</option>
              <option value={newOrder}>Nuevas ({statsCount[newOrder]})</option>
              <option value={orderInPreparation}>
                En Preparación ({statsCount[orderInPreparation]})
              </option>
              <option value={orderReady}>Listas ({statsCount[orderReady]})</option>
              <option value={orderAwaitingShipment}>
                Esperando Recolección ({statsCount[orderAwaitingShipment]})
              </option>
              <option value={orderInTransit}>
                En Tránsito ({statsCount[orderInTransit]})
              </option>
              <option value={orderDelivered}>
                Entregadas ({statsCount[orderDelivered]})
              </option>
              <option value={ORDER_STATUS.CANCELLED}>
                Canceladas ({statsCount[ORDER_STATUS.CANCELLED]})
              </option>
            </select>
            {/* <NavLink 
                            to="/"
                            className="filter-item"
                            activeClassName="active" 
                            onClick={e => {
                                e.preventDefault();
                                this.changeTabView(TAB_VIEWS.ACTIVE);
                            }}
                        >Activas ({this.getActiveOrders(orders).length})</NavLink> */}
            {/* <NavLink 
                            to="/"
                            className="filter-item"
                            activeClassName="active" 
                            onClick={e => {
                                e.preventDefault();
                                this.changeTabView(TAB_VIEWS.ARCHIVE);
                            }}
                        >Historial ({this.getInactiveOrders(orders).length})</NavLink> */}
          </nav>

          {/* <div className="search">
                        <form>
                            <fieldset>
                                <input 
                                    type="text" 
                                    placeholder="Buscar Orden" 
                                    value={this.state.searchTerm}
                                    onChange={e => this.searchOrders(e.target.value)}
                                />
                            </fieldset>
                        </form>
                    </div> */}
        </div>

        {orders.loading && !orders.results.length ? (
          <IconPreloader />
        ) : (
          <div className="orders-list_container">
            {orders.results.length ? (
              <PaginatedList
                items={orders.results}
                baseLocation={`/my-store/orders?filter=${this.getFilterFromQueryString(
                  location.search,
                )}`}
                maxItems={MAX_ITEMS_PER_PAGE}
                action={this.testingFunction}
                totalPages={orders.npages}
                page={orders.page}
              />
            ) : (
              <p className="default-message">
                No se encontraron órdenes con esta búsqueda.
              </p>
            )}
          </div>
        )}
      </section>
    );
  }
}

function mapStateToProps(state) {
  const vendorOrders = getVendorOrdersList(state);
  const stats = state.orders.vendor.stats || {};
  return {
    myStore: state.myStore.data,
    orders: {
      ...state.orders.vendor,
      results: vendorOrders,
      stats,
    },
  };
}
function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      getOrders: ordersActions.getOrders,
      getOrderStats: ordersActions.getOrderStats,
    },
    dispatch,
  );
}

// Export Component
export default connect(mapStateToProps, mapDispatchToProps)(OrderListVendor);
