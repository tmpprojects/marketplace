import React, { Component } from 'react';
import { Link, withRouter } from 'react-router-dom';
import Moment from 'dayjs';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { SHIPPING_METHODS } from '../../Constants';
import { formatNumberToPrice } from '../../Utils/normalizePrice';
import { LOCAL_ORDER_STATUS, ORDER_STATUS } from '../../Constants/orders.constants';

// Order´s Priority Status Dictionary.
const PRIORITY_STATUS = {
  HIGH: {
    name: 'Alta',
    value: 'high',
  },
  MEDIUM: {
    name: 'Media',
    value: 'medium',
  },
  LOW: {
    name: 'Baja',
    value: 'low',
  },
};

class OrderVendor extends Component {
  /**
   * getStatusClassName()
   * Maps order status with corresponding css class-name.
   * @param {string} orderStatus
   * @returns {string}
   */
  getStatusClassName = (orderStatus) => {
    switch (orderStatus) {
      case ORDER_STATUS.NEW_ORDER:
        return 'new';
      case ORDER_STATUS.PREPARING_ORDER:
        return 'preparing';
      case ORDER_STATUS.ORDER_READY:
        return 'ready';
      case ORDER_STATUS.AWAITING_SHIPMENT:
        return 'awaiting-shipment';
      case ORDER_STATUS.ORDER_IN_TRANSIT:
        return 'transit';
      case ORDER_STATUS.DELIVERED:
        return ORDER_STATUS.DELIVERED;
      case ORDER_STATUS.CANCELLED:
        return 'cancelled';
      default:
        return 'other';
    }
  };

  /**
   * getPriorityByShippingDate()
   * Define the priority of the order based on shipping date.
   * @param {string} shippingDate
   * @returns {obj}
   */
  getPriorityByShippingDate = (shippingDate) => {
    const daysUntilPickupDate = Moment(shippingDate).diff(Moment(), 'days');

    if (daysUntilPickupDate < 2) {
      return PRIORITY_STATUS.HIGH;
    } else if (daysUntilPickupDate < 3) {
      return PRIORITY_STATUS.MEDIUM;
    } else {
      return PRIORITY_STATUS.LOW;
    }
  };

  /**
   * displayStatus()
   * Display order status.
   * @param {string} orderStatus
   * @returns {jsx}
   */
  displayStatus = (orderStatus) => {
    const className = `status-display--${this.getStatusClassName(orderStatus)}`;
    switch (orderStatus) {
      case ORDER_STATUS.NEW_ORDER:
        return <div className={`${className} action-button`}>Comenzar Orden ></div>;
      case ORDER_STATUS.PREPARING_ORDER:
        return (
          <p>
            <span className={`status-display ${className}`}>Preparando Orden</span>
          </p>
        );
      case ORDER_STATUS.ORDER_READY:
        return (
          <p>
            <span className={`status-display ${className}`}>Orden Lista</span>
          </p>
        );
      case ORDER_STATUS.AWAITING_SHIPMENT:
        return (
          <p>
            <span className={`status-display ${className}`}>
              Esperando Recolección
            </span>
          </p>
        );
      case ORDER_STATUS.ORDER_IN_TRANSIT:
        return (
          <p>
            <span className={`status-display ${className}`}>En Tránsito</span>
          </p>
        );
      case ORDER_STATUS.DELIVERED:
        return (
          <p>
            <span className={`status-display ${className}`}>Entregada</span>
          </p>
        );
      case ORDER_STATUS.CANCELLED:
        return (
          <p>
            <span className={`status-display ${className}`}>Cancelada</span>
          </p>
        );
      default:
        return (
          <p>
            <span className={`status-display ${className}`}>-</span>
          </p>
        );
    }
  };

  /**
   * renderTableHeader()
   * Renders the header of orders table.
   */
  renderTableHeader = () => (
    <thead className="orders-list__header">
      <th className="column column--id"># Orden</th>
      <th className="column column--date">Recolección</th>
      <th className="column column--price">Precio</th>
      <th className="column column--products">Productos</th>
      <th className="column column--priority">Prioridad</th>
      <th className="column column--status">Status</th>
    </thead>
  );

  /**
   * isOrderOpen()
   * Check if the status of the order is 'open'.
   * Orders that are not 'delivered' or 'cancelled' are considered 'open'.
   * @param {string} orderStatus
   * @returns {bool}
   */
  isOrderOpen = (orderStatus) => {
    switch (orderStatus) {
      case ORDER_STATUS.CANCELLED:
      case ORDER_STATUS.DELIVERED:
      case ORDER_STATUS.PAID:
      case ORDER_STATUS.CANCELLED:
      case ORDER_STATUS.OTHER:
      case ORDER_STATUS.FRAUD:
        return false;
      default:
        return true;
    }
  };

  /**
   * render()
   */
  render() {
    const { items = [] } = this.props;

    return (
      <table className="orders-list">
        {this.renderTableHeader()}

        {items.map((order) => {
          // Dont show order if it has no products
          if (!order.products.length) return null;
          if (!order.products[0].product) {
            return (
              <tr className="order order--other" key={order?.uid}>
                <td className="column column--id">{order.uid}</td>
                <td
                  className="column column--products"
                  style={{ textAlign: 'center', padding: '1em' }}
                >
                  No disponible
                </td>
              </tr>
            );
          }

          // Setup Single Order Properties.
          const apiOrderStatus = order.physical_properties.status.value;
          const shippingDate = order.physical_properties.pickup_date;
          const orderPriority = this.isOrderOpen(apiOrderStatus)
            ? this.getPriorityByShippingDate(shippingDate)
            : PRIORITY_STATUS.LOW;
          const productCount = order.products.reduce((a, b) => a + b.quantity, 0);

          // Return order list item.
          return (
            <tr
              key={order.uid}
              onClick={(e) =>
                this.props.history.push(`/my-store/orders/${order.uid}`)
              }
              className={`order order--${this.getStatusClassName(apiOrderStatus)}`}
            >
              <td
                className={`column column--id
                                ${
                                  order.physical_properties.shipping_method_slug ===
                                    SHIPPING_METHODS.STANDARD.slug && 'standard'
                                }
                                ${
                                  order.products[0].product.slug ===
                                    'invite-code-to-create-store' && 'referrer'
                                }`}
              >
                {order.products[0].product.slug === 'invite-code-to-create-store' ? (
                  <span className="order-referrer">Recompensa</span>
                ) : (
                  <React.Fragment>
                    <Link
                      to={`/my-store/orders/${order.uid}`}
                      className="button_view"
                    >
                      {order.uid}
                    </Link>
                    {order.physical_properties.shipping_method_slug ===
                      SHIPPING_METHODS.STANDARD.slug && (
                      <span className="order-standard">Nacional</span>
                    )}
                    {order.physical_properties.shipping_method_slug ===
                      SHIPPING_METHODS.PICKUP_POINT.slug && (
                      <span className="order-standard">
                        {SHIPPING_METHODS.PICKUP_POINT.name}
                      </span>
                    )}
                  </React.Fragment>
                )}
              </td>
              <td className="column column--date">
                {Moment(shippingDate).format('D MMMM YYYY')}
              </td>
              <td className="column column--price">
                {formatNumberToPrice(order.products_price)}
              </td>
              <td className="column column--products">
                <span>[{productCount}]</span>
                {order.products.slice(0, 2).map((p) => {
                  if (!p.product) return <div>No disponible</div>; // <---- TODO: Should refactor this
                  return <span key={p.id}>{p.product.name}.&nbsp;</span>;
                })}
                {order.products.length > 3 && (
                  <li>
                    <span>Otros...</span>
                  </li>
                )}
              </td>
              <td className="column column--priority">
                <span className={`${orderPriority.value}`}>
                  {orderPriority.name}
                </span>
              </td>
              <td className="column column--status">
                {this.displayStatus(apiOrderStatus)}
              </td>
            </tr>
          );
        })}
      </table>
    );
  }
}

// Pass Redux state and actions to component
function mapStateToProps(state) {
  return {};
}
function mapDispatchToProps(dispatch) {
  return bindActionCreators({}, dispatch);
}

// Wrap component with router component
OrderVendor = withRouter(OrderVendor);
OrderVendor = connect(mapStateToProps, mapDispatchToProps)(OrderVendor);

// Export Component
export default OrderVendor;
