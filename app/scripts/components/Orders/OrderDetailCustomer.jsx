import React from 'react';
import PropTypes from 'prop-types';
import Moment from 'dayjs';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

import { formatNumberToPrice } from '../../Utils/normalizePrice';
import { userTypes, SHIPPING_METHODS, PAYMENT_METHODS } from '../../Constants';
import { ResponsiveImage } from '../../Utils/ImageComponents';
import { ORDER_STATUS } from '../../Constants/orders.constants';
import {
  formatDate,
  addHourPeriodSuffix,
  formatDateToUTC,
} from '../../Utils/dateUtils';

// Import Icons
const iconPayPal = require('../../../images/icons/payment/icon_paypal.svg');
const iconAmex = require('../../../images/icons/payment/icon_amex.svg');
const iconVisa = require('../../../images/icons/payment/icon_visa.svg');
const iconMastercard = require('../../../images/icons/payment/icon_mastercard.svg');
const iconOxxo = require('../../../images/icons/payment/icon_oxxo.svg');
const waitingShipping = require('../../../images/icons/schedules/icon_schedule.svg');
//
const OrderDetailCustomer = (props) => {
  const orderID = props.match.params.orderID;
  const order = props.orders.results.find((o) => o.uid === orderID);

  const displayStatus = (status) => {
    switch (status) {
      case ORDER_STATUS.NEW_ORDER:
        return <span className="new-order status-warning">Orden Nueva</span>;
      case ORDER_STATUS.PREPARING_ORDER:
        return (
          <span className="producing-order status-warning">Preparando Orden</span>
        );
      case ORDER_STATUS.ORDER_READY:
      case ORDER_STATUS.AWAITING_SHIPMENT:
        return (
          <span className="shipping-order status-warning">
            Esperando Recolección
          </span>
        );
      case ORDER_STATUS.ORDER_IN_TRANSIT:
        return (
          <span className="transiting-order status-warning">Orden en Tránsito</span>
        );
      case ORDER_STATUS.AWAITING_PAYMENT:
        return (
          <span className="transiting-order status-warning">Esperando Pago</span>
        );
      case ORDER_STATUS.DELIVERED:
        return <span className="order-delivered status-warning">Entregada</span>;
      default:
        return <span>Orden Pendiente</span>;
    }
  };

  //
  const renderPaymentMethod = () => {
    const { payment_provider_static_name: paymentMethod } = order;
    // CREDIT / DEBIT CARD
    switch (paymentMethod) {
      case 'mercado-pago': {
        const paymentResponse = order.payment_response;
        if (paymentResponse && 'response' in paymentResponse) {
          switch (paymentResponse.response.payment_method_id) {
            case 'master':
              return <img className="logo" src={iconMastercard} alt="Master Card" />;
            case 'visa':
              return <img className="logo" src={iconVisa} alt="Visa" />;
            case 'amex':
              return <img className="logo" src={iconAmex} alt="American Express" />;
            default:
              return <p>No identificado</p>;
          }
        }
        break;
      }

      // PAYPAL
      case 'paypal':
        return <img className="logo" src={iconPayPal} alt="PayPal" />;

      // OXXO
      case 'oxxo':
        return <img className="logo" src={iconOxxo} alt="Oxxo" />;

      // BANK TRANSFER
      case 'bank-transfer':
        return <span className="logo">Transferencia Bancaria</span>;

      default:
        return <p>No identificado</p>;
    }
  };

  //
  if (!order) return null;

  // Declare and format variables and properties
  const { user: profile, users: userInfo } = props;
  const shippingAddress = order.shipping_address;
  const physicalProperties = order.orders[0].physical_properties;
  const shippingPrice = physicalProperties.shipping_price;
  const productsCount = order.orders.reduce((a, b) => a + b.products.length, 0);
  const subtotalPrice = order.orders.reduce(
    (a, b) => a + parseInt(b.products_price, 10),
    0,
  );

  const isStandard =
    physicalProperties.shipping_method_name === SHIPPING_METHODS.STANDARD.name;
  const trackingId = physicalProperties.shipping_track_id;
  const trackingIdLink = physicalProperties?.shipping_track_url;
  const uniqueKey = physicalProperties.id;

  // Construct human-readable shipping date details depending on shipping method.
  let shippingDateLabel = 'Entrega el ';
  if (isStandard) {
    const fixedDate = Moment(physicalProperties.shipping_date);
    const estimatedDate = fixedDate.add('2', 'day');
    shippingDateLabel = `Entrega entre el ${fixedDate.format(
      'D MMMM',
    )} - ${estimatedDate.format('D MMMM')}.`;
  }
  console.log(props, 'propsOrderDetailCustomer');
  // Return component
  return (
    <div className="orderDetail">
      <Link to="/users/orders" className="button_view detail">
        Regresar a mis compras
      </Link>
      <div className="order_status">
        <p className="title">Orden: {order.uid}</p>
        <p className="date">
          <span className="subtitle">Realizada:</span>{' '}
          {Moment(order.created).format('DD MMMM YYYY')}
        </p>
        <span className="status">
          {displayStatus(physicalProperties.status.value)}
        </span>
      </div>

      <section className="order">
        {/*<div className="order_status">
                    <h5>Macorina</h5>
                </div>*/}

        <article className="order_address">
          <div className="user">
            <p className="subtitle">{`${profile.first_name} ${profile.last_name}`}</p>
            <span>{profile.email}</span>
          </div>

          <div className="order_address-shipping">
            <p className="subtitle">Dirección de envío</p>
            {shippingAddress ? (
              <span className="address">
                <span>
                  {`${shippingAddress.street}, 
                                    ${shippingAddress.neighborhood},`}
                  <br />
                  {`${shippingAddress.city}, ${shippingAddress.zip_code},
                                    ${shippingAddress.state}`}
                </span>
              </span>
            ) : (
              <span className="address">No Disponible</span>
            )}
          </div>

          {/*<div className="order_address-billing">
                        <p className="subtitle">Dirección de facturación</p>
                        {shippingAddress ? (
                            <span className="address">
                                <span>{shippingAddress.street_address} {shippingAddress.street_number}, {shippingAddress.neighborhood},<br/>
                                {shippingAddress.city}, {shippingAddress.zip_code}, {shippingAddress.state_name}</span>
                            </span>
                        ) : (
                            <span className="address">No Disponible</span>
                        )}
                    </div>*/}
        </article>

        <article className="order_methods">
          <div className="order_methods-payment">
            <p className="subtitle">Método de pago</p>
            {renderPaymentMethod()}

            {order.payment_provider_static_name === PAYMENT_METHODS.OXXO.slug && (
              <a
                target="_blank"
                rel="noopener noreferrer"
                className="payment-document-link button-simple"
                href={
                  order.payment_response.response.transaction_details
                    .external_resource_url
                }
              >
                Obtener código para realizar pago.
              </a>
            )}
          </div>
          <div className="order_methods-shipping">
            <p className="subtitle">Método de envío</p>
            <p className="method">
              {isStandard ? (
                <span>Standard Nacional</span>
              ) : (
                <React.Fragment>
                  <span>Express</span>
                </React.Fragment>
              )}
            </p>
            {isStandard && physicalProperties.status.value !== 'delivered' && (
              <div>
                <p className="subtitle">Guía de Rastreo:</p>
                <p>
                  {trackingId ? (
                    <a className="tracking-id" href={trackingIdLink}>
                      {trackingId}
                    </a>
                  ) : (
                    'Aún no disponible.'
                  )}
                </p>
              </div>
            )}
          </div>
          <div className="total">
            <p className="subtitle">Total</p>
            <span>{`${formatNumberToPrice(order.price)}MX`}</span>
          </div>
        </article>

        <div className="order_products">
          <div className="order_products-delivery">
            {/*<div className="date"> 
                            <span className="subtitle">Entrega: </span> 
                            12 Agosto 2018 | 02:00pm - 03:00pm
                        </div>*/}
          </div>

          <article className="product_container">
            {order.orders.map((store) =>
              store.products.map((product) => {
                const hasProduct = product.product;

                return (
                  <div
                    className="product"
                    key={
                      hasProduct
                        ? product.product.slug
                        : `product-notFound-${order.uid}`
                    }
                    style={{
                      borderBottom: 'solid 1px #eee',
                      paddingBottom: '1em',
                    }}
                  >
                    <div
                      className="product_detail"
                      key={
                        hasProduct
                          ? product.product.slug
                          : `product-notFound-${order.uid}`
                      }
                    >
                      {isStandard && (
                        <div
                          className="track-id"
                          style={{
                            marginBottom: '0.5em',
                            width: '100%',
                          }}
                        >
                          <b>Guía de Rastreo:</b>{' '}
                          {store.physical_properties.shipment_dhl
                            ? store.physical_properties.shipment_dhl
                                .airway_bill_number
                            : 'Aún no disponible.'}
                        </div>
                      )}

                      <div
                        className="product_detail-img"
                        key={
                          hasProduct
                            ? product.product.slug
                            : `product-notFound-${order.uid}`
                        }
                      >
                        <Link
                          target="_blank"
                          to={`/stores/${store.store.slug}/products/${product.product.slug}`}
                        >
                          <div className="img_container" key={uniqueKey}>
                            {hasProduct &&
                              product.product.photos[0] &&
                              'photo' in product.product.photos[0] && (
                                <ResponsiveImage
                                  src={product.product.photos[0].photo}
                                  alt={product.product.name}
                                />
                              )}
                          </div>
                        </Link>
                      </div>

                      <div className="product_detail-data" key={uniqueKey}>
                        <p className="cr__name cr__textColor--colorMain300">
                          {hasProduct ? (
                            <Link
                              target="_blank"
                              to={`/stores/${store.store.slug}/products/${product.product.slug}`}
                            >
                              <span>{product.product.name}</span>
                            </Link>
                          ) : (
                            'No disponible'
                          )}
                        </p>
                        <Link
                          to={`/stores/${store.store.slug}/products/`}
                          target="_blank"
                          className="store"
                        >
                          {store.store.name}
                        </Link>
                        <p className="attribute">
                          <span>
                            {product.attribute_names
                              ? `Detalle: ${product.attribute_names}`
                              : ''}
                          </span>
                        </p>

                        {store.physical_properties.shipping_schedule && (
                          <div className="date">
                            <span className="subtitle">{shippingDateLabel}</span>
                            <br />
                            {Moment(store.physical_properties.shipping_date).format(
                              'DD MMMM YYYY',
                            )}
                            &nbsp;
                            {!isStandard && (
                              <React.Fragment>
                                |
                                {` ${addHourPeriodSuffix(
                                  store.physical_properties.shipping_schedule.delivery_start
                                    .split(':')
                                    .slice(0, -1)
                                    .join(':'),
                                )}
                                                                - 
                                                                ${addHourPeriodSuffix(
                                                                  store.physical_properties.shipping_schedule.delivery_end
                                                                    .split(':')
                                                                    .slice(0, -1)
                                                                    .join(':'),
                                                                )}`}
                              </React.Fragment>
                            )}
                          </div>
                        )}
                      </div>
                    </div>

                    <div className="product_price">
                      <p className="quantity">
                        Cantidad <span>{product.quantity}</span>
                      </p>
                      <p className="price">
                        {formatNumberToPrice(product.total_price)}MX
                      </p>
                    </div>

                    {product.note ? (
                      <div className="product_notes">
                        <p className="subtitle">Notas</p>
                        <p className="note">{product.note}</p>
                      </div>
                    ) : null}
                  </div>
                );
              }),
            )}
          </article>

          <div className="total_container">
            <div className="total_price">
              <p className="breakdown">
                Subtotal&nbsp;
                <span className="products_count">
                  ({productsCount} producto{productsCount > 1 ? 's' : ''})
                </span>
                &nbsp;
                <span className="quantity">
                  {formatNumberToPrice(subtotalPrice)}MX
                </span>
              </p>
              <p className="breakdown">
                Envío {isStandard ? 'Standard Nacional' : 'Express'}
                <span className="quantity">
                  {formatNumberToPrice(shippingPrice)}MX
                </span>
              </p>
              <p className="breakdown total">
                Total&nbsp;
                <span className="quantity">
                  {formatNumberToPrice(order.price)}MX
                </span>
              </p>
            </div>
          </div>
        </div>
      </section>
    </div>
  );
};

// Pass Redux state and actions to component
function mapStateToProps({ orders, users }) {
  const customer = userTypes.CUSTOMER.toLowerCase();
  return {
    users,
    orders: orders[customer],
  };
}

export default connect(mapStateToProps)(OrderDetailCustomer);
