import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import './HoraRosa.scss';

let minutes = 60;
let magicDate = 1602691200000;
let target_date = magicDate + minutes * 60 * 1000;
let time_limit = minutes * 60 * 1000;
let days, hours, seconds; // variables for time units

function pad(n) {
  return (n < 10 ? '0' : '') + n;
}

export default class HoraRosaCountdown extends Component {
  constructor(props) {
    super(props);
    this.state = {
      copy: false,
      showCountdown: false,
      minutes: 0,
      seconds: 0,
    };
  }

  getCountdown() {
    // find the amount of "seconds" between now and target
    let current_date = new Date().getTime();
    let seconds_left = (target_date - current_date) / 1000;

    if (seconds_left >= 0) {
      days = pad(parseInt(seconds_left / 86400));
      seconds_left = seconds_left % 86400;

      hours = pad(parseInt(seconds_left / 3600));
      seconds_left = seconds_left % 3600;

      minutes = pad(parseInt(seconds_left / 60));
      seconds = pad(parseInt(seconds_left % 60));
      this.setState({
        ...this.state,
        minutes,
        seconds,
      });
    }
  }

  async componentDidMount() {
    //
    this.getCountdown();
    setInterval(() => {
      this.getCountdown();
    }, 1000);
    //
    let text = 'HORAROSA';
    let _this = this;
    let isPossible = await navigator.clipboard.writeText(text).then(
      function () {
        //console.log('Async: Copying to clipboard was successful!');
        return true;
      },
      function (err) {
        //console.error('Async: Could not copy text: ', err);
        return false;
      },
    );
    if (isPossible) {
      this.setState({
        ...this.state,
        copy: true,
      });
    }
  }

  render() {
    return null;
    return (
      <div
        className="horarosa__countdown horarosa__mouse"
        onClick={() => {
          if (this.state.copy) {
            window.alert('Copiamos a tu portapapeles el código: HORAROSA');
          }
        }}
      >
        <input type="hidden" id="set-time" value="60" />
        <div id="countdown">
          <div id="tiles" class="color-full">
            <span>{this.state.minutes}:</span>
            <span>{this.state.seconds}</span>
          </div>
          <div class="countdown-label">
            <div className="horarosa__discount">!Cupón activo ahora mismo!</div>
          </div>
        </div>
        {this.state.copy && (
          <div class="countdown-copy">* Haz clic para copiar el código</div>
        )}
      </div>
    );
  }
}
