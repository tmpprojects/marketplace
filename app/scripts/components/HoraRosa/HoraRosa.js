import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import './HoraRosa.scss';
import HoraRosaCountdown from './HoraRosaCountdown';
import Countdown from '../../components/Landing/Countdown/Countdown';
import { trackWithGTM } from '../../Utils/trackingUtils';
import { chooseBuenFinCoupon as checkIfIsHoraRosa } from '../../Utils/dateUtils';
/*
const onGTMTracking = () => {
  
  const data = {
    id: 'web',
    name: `horarosa`,
  };
  trackWithGTM('eec.impressionClick', [data], 'horarosa');
};
*/

const startHoraRosa = 11;
const startDayName = 'Thursday_';
export default class HoraRosa extends Component {
  constructor(props) {
    super(props);
    this.state = {
      horarosa: false,
      topHourHoraRosa: '',
      debug: false,
    };

    // if (process.env.CLIENT) {
    //   localStorage.setItem('horarosa', 'horarosa');
    //   console.log('setItem horarosa');
    // }
  }

  // async getHour() {
  //   let currentDate = await fetch('https://auth.canastarosa.com/date')
  //     .then((response) => response.json())
  //     .then((data) => data);
  //   //console.log(currentDate);
  //   if (currentDate.dayname == startDayName) {
  //     //its day
  //     if (currentDate.hour == startHoraRosa) {
  //       //its hour
  //       this.setState({
  //         ...this.state,
  //         horarosa: true,
  //       });
  //     }
  //   }
  // }

  enableHoraRosa = () => {
    const isHoraRosa = checkIfIsHoraRosa();
    if (isHoraRosa.discountToApply === 'HORAROSABF') {
      this.setState({
        ...this.state,
        horarosa: true,
        topHourHoraRosa: isHoraRosa.topHourHoraRosaIsActive,
      });
    } else {
      this.setState({
        ...this.state,
        horarosa: false,
        topHourHoraRosa: '',
      });
    }
  };

  componentDidMount() {
    //console.log('horarosa in component ver: ', this.props.location);
    // this.getHour();
    this.enableHoraRosa();
    if (this.props.location?.search) {
      if (this.props.location?.search.includes('horarosa')) {
        this.setState({ ...this.state, horarosa: true, debug: true });
      }
    }
    // onGTMTracking();
  }

  render() {
    //return null;
    if (!this.state.horarosa) {
      if (this.props?.children) {
        return this.props.children;
      } else {
        return null;
      }
    } else {
      return (
        <Link to={'/buen-fin/'} className="link-hora-rosa">
          <div
            className={!this.props.little ? 'horarosa' : 'horarosa horarosa__little'}
            style={{ backgroundColor: '#21212D' }}
          >
            <div className="banner">
              {!this.props.little && (
                <div className="bannerImg">
                  <img
                    src="https://canastarosa.s3.us-east-2.amazonaws.com/media/buenFin/horarosa-1-dark.jpg"
                    alt="main-banner"
                    style={{ padding: '0.6px', height: '100%' }}
                  ></img>
                </div>
              )}
              <div className="bannerElements">
                <div
                  className="bannerImg-2"
                  style={{ padding: '2.5em 1em 1em 1em' }}
                >
                  <img
                    src="https://canastarosa.s3.us-east-2.amazonaws.com/media/buenFin/horarosa-2-dark.jpg"
                    alt={'second-banner'}
                    style={{ width: '100%', padding: '16px' }}
                  ></img>
                </div>
                <div
                  className="bannerCountDown"
                  style={{
                    display: 'flex',
                    justifyContent: 'center',
                    width: '100%',
                    borderTop: '2px solid #37373e',
                    paddingTop: '1em',
                    paddingBottom: '1em',
                  }}
                >
                  <div
                    style={{
                      display: 'flex',
                      alignItems: 'center',
                      color: 'white',
                      fontSize: '1em',
                      padding: '0 1em',
                    }}
                  >
                    La oferta termina en:
                  </div>
                  <Countdown date={this.state.topHourHoraRosa} />
                  {/* <HoraRosaCountdown></HoraRosaCountdown> */}
                </div>
              </div>
            </div>
          </div>
        </Link>
      );
    }
  }
}
