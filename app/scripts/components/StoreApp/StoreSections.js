import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { renderRoutes } from 'react-router-config';

import { myStoreActions, statusWindowActions } from '../../Actions';

class StoreSections extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {
    return renderRoutes(this.props.route.routes, {
      myStore: this.props.myStore,
    });
  }
}

function mapStateToProps(state) {
  return {
    myStore: state.myStore.data,
  };
}
function mapDispatchToProps(dispatch) {
  const { updateProduct } = myStoreActions;
  return bindActionCreators(
    {
      openStatusWindow: statusWindowActions.open,
      updateProduct,
    },
    dispatch,
  );
}

export default connect(mapStateToProps, mapDispatchToProps)(StoreSections);
