import React from 'react';
import { NavLink } from 'react-router-dom';
import { connect, connectAdvanced } from 'react-redux';
import { renderRoutes } from 'react-router-config';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';

import config from '../../../config';
import { storeActions, myStoreActions, statusWindowActions } from '../../Actions';

class Submenu extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      positionClass: '',
    };
    this.initPosition = 0;
    this.onScroll = this.onScroll.bind(this);
  }
  componentDidMount() {
    this.initPosition = this.navigation.getBoundingClientRect().top;
    document.addEventListener('scroll', this.onScroll);
  }
  componentWillUnmount() {
    document.removeEventListener('scroll', this.onScroll);
  }

  onScroll() {
    const top = document.documentElement.scrollTop || document.body.scrollTop;
    const bottom =
      document.documentElement.scrollHeight || document.body.scrollHeight;

    if (top >= this.initPosition) {
      this.setState({
        positionClass: 'sticky',
      });
    } else {
      this.setState({
        positionClass: '',
      });
    }
  }

  render() {
    const { positionClass } = this.state;
    return (
      <nav
        className={`storeApp__submenu ${positionClass}`}
        ref={(navigation) => (this.navigation = navigation)}
      >
        <ul>
          <li className="storeApp__submenu-item">
            <NavLink to="/my-store/settings/info" activeClassName="active">
              Informaci&oacute;n General
            </NavLink>
          </li>
          <li className="storeApp__submenu-item">
            <NavLink to="/my-store/settings/payments" activeClassName="active">
              Pagos y Facturación
            </NavLink>
          </li>
          <li className="storeApp__submenu-item">
            <NavLink to="/my-store/settings/addresses" activeClassName="active">
              Direcciones
            </NavLink>
          </li>
          {/* <li className="storeApp__submenu-item"><NavLink to="/my-store/settings/gallery" activeClassName="active" >Galer&iacute;a</NavLink></li> */}
          {/*<li className="storeApp__submenu-item"><NavLink to="/my-store/settings/faqs" activeClassName="active" >FAQs</NavLink></li>*/}
        </ul>
      </nav>
    );
  }
}

class StoreSettings extends React.Component {
  /*
   * Constructor Function
   * @param {object} props React Component Properties
   */
  constructor(props) {
    super(props);
  }

  /*
   * React Component Life Cycle Functions
   */
  render() {
    const {
      myStore,
      route,
      getMercadoPagoPaymentLink,
      deletePaymentMethod,
    } = this.props;
    return (
      <div className="storeApp__settings">
        <Submenu />
        {renderRoutes(route.routes, {
          getMercadoPagoPaymentLink,
          deletePaymentMethod,
          myStore,
        })}
      </div>
    );
  }
}

// Add Redux state and actions to component´s props
function mapStateToProps(state) {
  return {};
}
function mapDispatchToProps(dispatch) {
  return bindActionCreators({}, dispatch);
}

// Export Component
export default connect(mapStateToProps, mapDispatchToProps)(StoreSettings);
