import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { renderRoutes } from 'react-router-config';

import { userTypes } from '../../Constants';
import { myStoreActions, statusWindowActions } from '../../Actions';
import { getVendorOrdersList } from '../../Reducers/orders.reducer';

class StoreOrders extends React.Component {
  render() {
    return renderRoutes(this.props.route.routes, {
      myStore: this.props.myStore,
      userType: userTypes.VENDOR,
      orders: this.props.orders,
    });
  }
}

function mapStateToProps(state) {
  const vendorOrders = getVendorOrdersList(state);
  return {
    myStore: state.myStore.data,
    orders: {
      ...state.orders.vendor,
      results: vendorOrders,
    },
  };
}

function mapDispatchToProps(dispatch) {
  const { updateProduct } = myStoreActions;
  return bindActionCreators(
    {
      openStatusWindow: statusWindowActions.open,
      updateProduct,
    },
    dispatch,
  );
}
export default connect(mapStateToProps, mapDispatchToProps)(StoreOrders);
