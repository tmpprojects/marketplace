import React from 'react';
import './_restoreNationalBanner.scss';

export default function RestoreNationalBanner() {
  return (
    <div className="cr__national">
      <div>
        <span className="cr__text--subtitle3 cr__textColor--colorDark300 cr__national--title">
          Paquetería nacional restaurado
        </span>{' '}
        <br />
        <span className="cr__text--paragraph cr__textColor--cr__national--subtitle">
          Nuevamente puedes programar la recolección y generar guías para tus órdenes
          nacionales
        </span>
      </div>
    </div>
  );
}
