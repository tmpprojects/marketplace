import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Link } from 'react-router-dom';
import DayJs from 'dayjs';
import Cookies from 'universal-cookie';

import { formatNumberToPrice } from '../../../Utils/normalizePrice';
import { categorizedOrdersList } from '../../../Reducers/orders.reducer';
import { userTypes } from '../../../Constants';
import { appActions, ordersActions } from '../../../Actions';
import config from '../../../../config';
import { ORDER_STATUS } from './../../../Constants/orders.constants';
import {
  VENDORS_PHONE,
  VENDORS_WHATSAPP_PLAIN_FORMAT,
} from './../../../Constants/config.constants';
import { shareOnFacebook, shareOnTwitter } from '../../../Utils/socialMediaUtils';
import PageHead from '../../../Utils/PageHead';
import './specialMessage.scss';
import { trackWithGTM } from '../../../Utils/trackingUtils';
import './RestoreNationalBanner/RestoreNationalBanner';
import RestoreNationalBanner from './RestoreNationalBanner/RestoreNationalBanner';

class Dashboard extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      orders: this.props.orders || [],
      stats: this.props.stats || {},
      sawMessage: false,
      hasViewedNationalBanner: true,
    };
  }

  componentDidMount() {
    this.props.getOrderStats();

    const cookie = window.localStorage.getItem('CRStoresMessage');

    if (cookie) {
      this.setState({ sawMessage: true });
    }

    // Banner Restore National Shipping
    const cookiesNationalBanner = new Cookies();

    const getRestoreNationalBannerCookie = cookiesNationalBanner.get(
      'CRRestoreNationalBanner',
    );

    if (!getRestoreNationalBannerCookie) {
      this.setState({ hasViewedNationalBanner: false });
    }

    window.addEventListener('onbeforeunload', this.setRestoreNationalBannerCookie());
  }

  componentWillUnmount() {
    window.removeEventListener(
      'onbeforeunload',
      this.setRestoreNationalBannerCookie(),
    );
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.orders !== nextProps.orders) {
      this.setState({
        orders: nextProps.orders,
      });
    }

    if (this.props.stats !== nextProps.stats) {
      this.setState({
        stats: nextProps.stats,
      });
    }
  }

  setRestoreNationalBannerCookie = () => {
    const cookies = new Cookies();

    const { hasViewedNationalBanner } = this.state;

    if (!hasViewedNationalBanner) {
      cookies.set('CRRestoreNationalBanner', true, { path: '/' });
    }
  };

  countOrders = (type) => {
    const { orders } = this.state;
    if (type === 'paid') {
      const ordersCount = orders.reduce(
        (a, b) => (b.payout_sent ? a + 1 : a + 0),
        0,
      );
      return ordersCount;
    }

    if (type === 'sales') {
      const totalSales = orders.reduce((a, b) => a + b.order_net, 0);
      return totalSales;
    }

    const ordersCount = orders.reduce(
      (a, b) => (b.physical_properties.status.value === type ? a + 1 : a + 0),
      0,
    );
    return ordersCount;
  };
  setCookie = () => {
    window.localStorage.setItem('CRStoresMessage', true);
    this.setState({ sawMessage: true });
  };
  onClickGTMTracking = (source) => {
    const data = {
      id: 'WelcomeCoupon',
      name: source,
      position: 1,
    };
    trackWithGTM('eec.impressionClick', [data], 'Share-WelcomeCoupon');
  };
  render() {
    const { orders, stats } = this.state;
    const { myStore } = this.props;
    const { name, slug, bank_account: bankAccount } = this.props.myStore;
    if (!orders && !stats) return null;
    const orderStatus = stats.count_by_order_status;
    if (!orderStatus) return null;

    const ordersInProcess =
      orderStatus[ORDER_STATUS.PREPARING_ORDER] +
      orderStatus[ORDER_STATUS.ORDER_READY] +
      orderStatus[ORDER_STATUS.AWAITING_SHIPMENT] +
      orderStatus[ORDER_STATUS.AWAITING_PAYMENT] +
      orderStatus[ORDER_STATUS.ORDER_IN_TRANSIT];

    const ordersCancelled =
      orderStatus[ORDER_STATUS.OTHER] +
      orderStatus[ORDER_STATUS.FRAUD] +
      orderStatus[ORDER_STATUS.CANCELLED];

    // UTM's Source
    const utmSourceFacebook = 'utm_source=fb';
    const utmSourceTwitter = 'utm_source=tw';
    const utmSourceWhatsapp = 'utm_source=wp';

    const myInviteCodes = myStore.invite_codes;

    const myCreateStoreCode = myInviteCodes.find(
      (rc) => rc.invite_to === 'create_store',
    );

    // Store URL
    const storeURL = `${config.frontend_host}/stores/${slug}/?`;
    const utmMyStoreURL = encodeURIComponent(
      '&utm_medium=seller-share&utm_campaign=seller-invite-visit-my-store',
    );
    const storeText = `Hola! Ya puedes comprar mis productos en línea, pagar con tarjeta y paypal, y recibirlos en la comodidad de tu casa. Visita mi tienda ${name} `;

    // New Store URL
    const newStoreURL = `${config.frontend_host}/about-us/sell/?`;
    const utmNewStoreURL = encodeURIComponent(
      '&utm_medium=seller-share&utm_campaign=seller-invite-store-creation',
    );
    const newStoreText = `Hola! Se que tienes un increíble producto y que podrías crecer rápidamente tus ventas con una tienda en línea. Crea tu tienda de manera rápida y sencilla en Canasta Rosa e ingresa mi código de referencia '${myCreateStoreCode.code}' al momento de activar tu tienda para que Canasta premie mi recomendación. Visita `;

    // Store subscription plan
    const planStatus = myStore?.plan?.status?.slug;
    const planExpiry = myStore?.plan?.next_payment_date;
    const planExpiryDateCount = DayJs(planExpiry).diff(DayJs(), 'day');
    const currentPlan = myStore?.plan?.plan;
    const isUsingTrial = currentPlan?.slug === 'pro-plan' && planStatus === 'trial';

    // Welcome Coupon
    const couponCode = myStore?.coupons?.slice(0, 1).map(({ code }) => code) || [];
    const couponCodeID =
      myStore?.coupons?.slice(0, 1).map(({ campaign }) => campaign) || [];
    // Welcome Coupon URL
    const utmWelcomeCouponURL = '&utm_campaign=welcome_coupon';
    const couponText = `Hola! Ya puedes comprar mis productos en línea y recibirlos en la comodidad de tu casa. Haz tu pedido con envío gratis solo ingresa el cupón ${couponCode}. Visita mi tienda ${name} `;

    const hasRFC = this.props?.myStore?.fiscal_registry === 'with-rfc';

    // Return JSX
    return (
      <div>
        {this.state.sawMessage ? (
          <section className="storeApp__module dashboard">
            <div className="dashboard__header">
              <h4 className="">Mi panel</h4>
              {myStore.is_active.bool && (
                <div className="dashboard__shareStore">
                  <h5 className="title">Comparte tu Tienda</h5>
                  <div className="socialMedia__share">
                    <div className="socialMedia__share--buttons">
                      <a
                        href="#"
                        onClick={(e) => {
                          e.preventDefault();
                          this.props
                            .createBitly(
                              `${storeURL}${utmSourceFacebook}${utmMyStoreURL}`,
                            )
                            .then((shortURL) => {
                              shareOnFacebook(shortURL, storeText);
                            });
                        }}
                        className="icon icon--facebook"
                      >
                        <span>Compartir</span>
                      </a>
                      <a
                        href="#"
                        onClick={(e) => {
                          e.preventDefault();
                          this.props
                            .createBitly(
                              `${storeURL}${utmSourceTwitter}${utmMyStoreURL}`,
                            )
                            .then((shortURL) => {
                              shareOnTwitter(shortURL, storeText);
                            });
                        }}
                        className="icon icon--twitter"
                      >
                        <span>Tweet</span>
                      </a>
                      <a
                        href="#"
                        onClick={(e) => {
                          e.preventDefault();
                          this.props
                            .createBitly(
                              `${storeURL}${utmSourceWhatsapp}${utmMyStoreURL}`,
                            )
                            .then((shortURL) => {
                              const text = encodeURIComponent(
                                `${storeText} ${shortURL}`,
                              );
                              window.location.href = `https://api.whatsapp.com/send?text=${text}`;
                            });
                        }}
                        data-action="share/whatsapp/share"
                        className="icon icon--whatsapp"
                      >
                        <span>Whatsapp</span>
                      </a>
                    </div>
                  </div>
                </div>
              )}
            </div>
            {/** RESTORE NATIONAL BANNER */}
            {!this.state.hasViewedNationalBanner && <RestoreNationalBanner />}

            {/** RFC BANNER REMIDER */}
            {!hasRFC && (
              <div className="dashboard__rfcBanner">
                <div className="dashboard__rfcBanner--wrapperContent">
                  <div className="dashboard__rfcBanner--icon">
                    <img
                      src={require('../../../../images/icons/icon_warning.svg')}
                    />
                  </div>
                  <div className="dashboard__rfcBanner--content">
                    <h5 className="cr__textColor--colorDark300 cr__text--subtitle3">
                      Evita una retención del 36% registrando tu R.F.C.
                    </h5>
                    <h6 className="cr__text--paragraph">
                      Con un RFC registrado en la plataforma tus retenciones
                      ser&aacute;n entre 8.4% y 13.4%
                    </h6>
                  </div>
                </div>
                <div className="dashboard__rfcBanner--button">
                  <Link
                    to="/my-store/settings/payments/#RFC"
                    className="cr__textColor--colorMain300 cr__text--paragraph"
                  >
                    Añadir RFC
                  </Link>
                </div>
              </div>
            )}

            {/** SUBSCRIPTION PLAN */}
            <div className="dashboard__plan">
              <div className="dashboard__plan-header">
                {/** Banner background */}
                {currentPlan.slug === 'free-plan' ? (
                  <React.Fragment>
                    <img
                      src={require('../../../../images/plans/rightup.svg')}
                      alt="Figura Banner"
                      className="dashboard__plan-ru"
                    />
                    <img
                      src={require('../../../../images/plans/leftdown.svg')}
                      alt="Figura Banner"
                      className="dashboard__plan-ld"
                    />
                  </React.Fragment>
                ) : (
                  <React.Fragment>
                    <img
                      src={require('../../../../images/plans/leftup.svg')}
                      alt="Figura Banner"
                      className="dashboard__plan-lu"
                    />
                    <img
                      src={require('../../../../images/plans/rightdown.svg')}
                      alt="Figura Banner"
                      className="dashboard__plan-rd"
                    />
                  </React.Fragment>
                )}
                {/** /Banner background */}

                {/** Header */}
                {currentPlan.slug === 'free-plan' ? (
                  <div className="dashboard__plan-wrapper">
                    {/** Title */}
                    <div>
                      <div className="dashboard__plan-type">
                        <img
                          src={require('../../../../images/plans/standardC.svg')}
                          className="dashboard__plan-iconStandard"
                          alt="Icono plan estándar"
                          aria-label="Icono plan estándar"
                        />
                        <h3 className="cr__plans-selection-container-title cr__text--subtitle2 dashboard__plan-title">
                          Plan {currentPlan.name}
                        </h3>
                      </div>
                    </div>
                    {/** Header */}
                  </div>
                ) : (
                  <div>
                    <div className="dashboard__plan-type">
                      <img
                        src={require('../../../../images/plans/proC.svg')}
                        className="dashboard__plan-iconStandard"
                        alt="Icono plan estándar"
                        aria-label="Icono plan estándar"
                      />
                      <h3 className="cr__plans-selection-container-title cr__text--subtitle2 dashboard__plan-title">
                        Plan {currentPlan.name}
                      </h3>
                    </div>

                    {/** Trial button
                    {isUsingTrial && (
                      <a
                        className="c2a_square cr__text--paragraph"
                        href="/my-store/subscription"
                        aria-label="Probar 30 días gratis"
                        data-test="subscribe-to-plan"
                      >
                        Prueba el Plan Pro 30 d&iacute;as gratis
                      </a>
                    )}
                    /Trial button */}
                  </div>
                )}
                {/** /Title */}
              </div>

              <div className="dashboard__plan-details">
                <div>
                  <strong style={{ fontWeight: 'bold' }}>Plan actual:</strong>{' '}
                  {currentPlan?.name}
                  {isUsingTrial ? ' (versión de prueba)' : ''}
                  <br />
                  {currentPlan.slug === 'pro-plan' && (
                    <React.Fragment>
                      <strong style={{ fontWeight: 'bold' }}>
                        {isUsingTrial
                          ? 'Vencimiento del plan: '
                          : 'Siguiente renovación: '}
                      </strong>{' '}
                      {isUsingTrial && planExpiryDateCount < 10 ? (
                        <React.Fragment>
                          Tu prueba gratis vence en {planExpiryDateCount} días.
                        </React.Fragment>
                      ) : (
                        <React.Fragment>
                          {DayJs(planExpiry).format('DD MMMM YYYY')}
                        </React.Fragment>
                      )}{' '}
                    </React.Fragment>
                  )}
                </div>

                {/** Call to action */}
                {(currentPlan.slug === 'free-plan' || isUsingTrial) && (
                  <Link
                    className="dashboard__plan__main-button"
                    to="/my-store/subscription"
                  >
                    Contrata el plan Plus
                  </Link>
                )}
              </div>
            </div>
            {/** /SUBSCRIPTION PLAN */}

            <div className="columns">
              <div className="columns-left">
                {/* ORDERS SUMMARY */}
                <div className="dashboard__module dashboard__orders">
                  <h5 className="module__title">Órdenes</h5>
                  <div className="module__content">
                    <div className="row">
                      <div className="kpi ">
                        {/* <Link to={`orders/?filter=${ORDER_STATUS.NEW}`}> */}
                        <Link to={`orders/?filter=${ORDER_STATUS.NEW_ORDER}`}>
                          <div className="count_new">
                            <span>{orderStatus[ORDER_STATUS.NEW_ORDER]}</span>
                          </div>
                          <div>
                            <div>Nuevas</div>
                            <div className="note">Este mes</div>
                          </div>
                        </Link>
                      </div>
                      <div className="kpi">
                        <Link to={`orders/?filter=${ORDER_STATUS.IN_PROCESS}`}>
                          <div className="count_in-process">
                            <span>{ordersInProcess}</span>
                          </div>
                          <div>En Proceso</div>
                        </Link>
                      </div>
                      <div className="kpi">
                        {/* <Link to={`orders/?filter=${ORDER_STATUS.DELIVERED}`}> */}
                        <Link to={`orders/?filter=${ORDER_STATUS.DELIVERED}`}>
                          <div>
                            <div className="count_delivered">
                              <span>{orderStatus[ORDER_STATUS.DELIVERED]}</span>
                            </div>
                            <div>Entregadas</div>
                          </div>
                        </Link>
                      </div>
                      <div className="kpi">
                        {/* <Link to={`orders/?filter=${ORDER_STATUS.PAID}`}> */}
                        <Link to={`orders/?filter=${ORDER_STATUS.PAID}`}>
                          <div className="count_paid">
                            <span>{orderStatus[ORDER_STATUS.PAID]}</span>
                          </div>
                          <div>
                            <div>Pagadas</div>
                            <div className="note">Totales</div>
                          </div>
                        </Link>
                      </div>
                      <div className="kpi">
                        <Link to={`orders/?filter=${ORDER_STATUS.CANCELLED}`}>
                          <div className="count_cancelled">
                            <span>{ordersCancelled}</span>
                          </div>
                          <div>Canceladas</div>
                        </Link>
                      </div>
                    </div>
                  </div>
                </div>

                <div className="dashboard__module dashboard__download">
                  <h5 className="module__title">Descargas</h5>
                  <div className="module__content">
                    <ul className="list">
                      <li className="download">
                        <a
                          className="download__link"
                          href="https://canastarosa.s3.us-east-2.amazonaws.com/download/download-kit/kit-bienvenida-cr.zip"
                          download
                        >
                          Kit de etiquetas de órdenes y tarjeta dedicatoria
                        </a>
                      </li>
                      <li className="download">
                        <a
                          className="download__link"
                          href="https://canastarosa.s3.us-east-2.amazonaws.com/download/download-guides/guia_envio_nacional.pdf"
                          target="_blank"
                          rel="noopener noreferrer"
                          download
                        >
                          Guía de empaque envíos Nacionales
                        </a>
                      </li>
                    </ul>
                  </div>
                </div>
                <div className="dashboard__module dashboard__national">
                  {myStore?.obligations?.ship_nationwide ? (
                    <React.Fragment>
                      <div className="content">
                        <div className="module__illustration is_national" />
                        <div className="module__note">
                          <h5 className="title">
                            ¡Felicidades tu tienda ahora es{' '}
                            <strong className="strong">NACIONAL</strong>!
                          </h5>
                          <p className="note download">
                            Guías de empaque y recomendación de Canasta Rosa.{' '}
                            <a
                              href="https://canastarosa.s3.us-east-2.amazonaws.com/download/download-guides/guia_envio_nacional.pdf"
                              target="_blank"
                              rel="noopener noreferrer"
                              className="link_download"
                              download
                            >
                              Descargar PDF
                            </a>
                          </p>
                        </div>
                      </div>
                      <div className="module__contact">
                        <p className="message">¿Tienes alguna duda?</p>
                        <a
                          className="c2a_square"
                          // href={`tel:+52-${VENDORS_PHONE}`}
                          href="mailto:info@canastarosa.com"
                        >
                          {' '}
                          Contáctanos
                        </a>
                        {/* <a
                        className="c2a_square whatsapp"
                        href={`https://api.whatsapp.com/send?phone=${VENDORS_WHATSAPP_PLAIN_FORMAT}`}
                        target="_blank"
                        rel="noopener noreferrer"
                      >
                        {' '}
                        Escribir ahora
                      </a> */}
                      </div>
                    </React.Fragment>
                  ) : (
                    <React.Fragment>
                      <div className="content">
                        <div className="module__illustration" />
                        <ul className="module__list">
                          <h5 className="title">
                            ¿por qué convertir tu tienda en Nacional?
                          </h5>
                          <li className="benefit">
                            Amplía tu marca
                            <span className="benefit__message">
                              Lleva tu marca a otro nivel.
                            </span>
                          </li>
                          <li className="benefit">
                            Nuevos clientes
                            <span className="benefit__message">
                              Potenciales clientes.
                            </span>
                          </li>
                          <li className="benefit">
                            Incremento de ventas
                            <span className="benefit__message">
                              Mayor alcance igual a más ventas.
                            </span>
                          </li>
                        </ul>
                      </div>
                      <div className="module__contact">
                        <p className="message">¿Tienes alguna duda?</p>
                        <a
                          className="c2a_square"
                          // href={`tel:+52-${VENDORS_PHONE}`}
                          href="mailto:info@canastarosa.com"
                        >
                          {' '}
                          Contáctanos
                        </a>
                        {/* <a
                        className="c2a_square whatsapp"
                        href={`https://api.whatsapp.com/send?phone=${VENDORS_WHATSAPP_PLAIN_FORMAT}`}
                        target="_blank"
                        rel="noopener noreferrer"
                      >
                        {' '}
                        Escribir ahora
                      </a> */}
                      </div>
                    </React.Fragment>
                  )}
                </div>

                {/* SELLS SUMMARY */}
                {/*
                <div className='dashboard__module dashboard__sales'>
                  <h5 className='module__title'>Ventas</h5>
                  <div className='module__content'>
                    <div className='kpi'>
                      <div className='title'>TOTAL ÓRDENES</div>
                      <div className='count'>{orderStatus.total_orders}</div>
                    </div>
                    <div className='kpi'>
                      <div>
                        <div className='title'>INGRESOS</div>
                        <div className='note'>Ver Detalle</div>
                      </div>
                      <div className='count'>
                        {formatNumberToPrice(stats.total_net_profit.toFixed())}{' '}
                        MX
                      </div>
                    </div>
                  </div>
                </div>
                */}
              </div>
              {/** WELCOME COUPON */}
              <div className="columns-right">
                {couponCode.length >= 1 && couponCodeID == 97 && (
                  <div className="cr__dashboard-welcomeCoupon">
                    <h4 className="cr__dashboard-welcomeCoupon-title cr__textColor--colorDark300 cr__text--subtitle2">
                      ¡Los primeros 30 días los envíos van por nuestra cuenta!
                    </h4>
                    <div className="cr__dashboard-welcomeCoupon-text cr__textColor--colorDark300 cr__text--paragraph">
                      <p>
                        Tus clientes podr&aacute;n utilizar el
                        <span className="cr__textColor--colorMain300">
                          {' '}
                          siguiente cup&oacute;n{' '}
                        </span>
                        para obtener un descuento del 100% en el precio del
                        env&iacute;o.
                      </p>
                    </div>
                    <div
                      className="cr__dashboard-welcomeCoupon-coupon"
                      onClick={() => navigator.clipboard.writeText(couponCode)}
                    >
                      <span className="cr__textColor--colorDark300 cr__text--subtitle2">
                        {couponCode}
                      </span>
                      <img
                        src={require('../../../../images/icons/icon_clipboard.svg')}
                      />
                    </div>
                    <div className="socialMedia__share">
                      <h5 className="cr__textColor--colorDark300 cr__text--paragraph">
                        Comparte tu c&oacute;digo
                      </h5>
                      <div className="socialMedia__share--buttons">
                        <a
                          href="#"
                          onClick={(e) => {
                            e.preventDefault();
                            this.props
                              .createBitly(
                                `${storeURL}${utmSourceFacebook}${utmWelcomeCouponURL}`,
                              )
                              .then((shortURL) => {
                                shareOnFacebook(shortURL, couponText);
                              });
                            this.onClickGTMTracking(utmSourceFacebook);
                          }}
                          className="icon icon--facebook"
                        >
                          <span>Compartir</span>
                        </a>
                        <a
                          href="#"
                          onClick={(e) => {
                            e.preventDefault();
                            this.props
                              .createBitly(
                                `${storeURL}${utmSourceTwitter}${utmWelcomeCouponURL}`,
                              )
                              .then((shortURL) => {
                                shareOnTwitter(shortURL, couponText);
                              });
                            this.onClickGTMTracking(utmSourceTwitter);
                          }}
                          className="icon icon--twitter"
                        >
                          <span>Tweet</span>
                        </a>
                        <a
                          href="#"
                          onClick={(e) => {
                            e.preventDefault();
                            this.props
                              .createBitly(
                                `${storeURL}${utmSourceWhatsapp}${utmWelcomeCouponURL}`,
                              )
                              .then((shortURL) => {
                                const text = encodeURIComponent(
                                  `${couponText} ${shortURL}`,
                                );
                                window.location.href = `https://api.whatsapp.com/send?text=${text}`;
                              });
                            this.onClickGTMTracking(utmSourceWhatsapp);
                          }}
                          data-action="share/whatsapp/share"
                          className="icon icon--whatsapp"
                        >
                          <span>Whatsapp</span>
                        </a>
                      </div>
                    </div>
                  </div>
                )}
                <div className="dashboard__module dashboard__crpro">
                  <div className="dark-rectangle">
                    <div>
                      <div>
                        ¿Buscas crecer tu marca?, <br /> conoce los:
                      </div>
                      <a
                        className="btn"
                        href="/pro/"
                        target="_blank"
                        rel="noopener noreferrer"
                      >
                        Servicios Canasta Rosa
                      </a>
                    </div>
                  </div>
                </div>
                {myStore.is_active.bool && (
                  <div className="dashboard__module dashboard__referrerCode">
                    <h5 className="module__title">Comparte tu Código de Referido</h5>
                    <div>
                      <span className="bold">Invita a tus amigos</span> a vender en
                      Canasta Rosa y obtén comisiones por cada tienda activa.
                    </div>
                    <div className="referrer">
                      Comparte <span className="bold">tu código</span>:
                    </div>
                    <div className="code_container">
                      <span className="cr__plans-selection-container-title cr__text--subtitle2">
                        {myCreateStoreCode.code}
                      </span>
                    </div>
                    <div className="socialMedia__share">
                      <div className="socialMedia__share--buttons">
                        <a
                          href="#"
                          onClick={(e) => {
                            e.preventDefault();
                            this.props
                              .createBitly(
                                `${newStoreURL}${utmSourceFacebook}${utmNewStoreURL}`,
                              )
                              .then((shortURL) => {
                                shareOnFacebook(shortURL, newStoreText);
                              });
                          }}
                          className="icon icon--facebook"
                        >
                          <span>Compartir</span>
                        </a>
                        <a
                          href="#"
                          onClick={(e) => {
                            e.preventDefault();
                            this.props
                              .createBitly(
                                `${newStoreURL}${utmSourceTwitter}${utmNewStoreURL}`,
                              )
                              .then((shortURL) => {
                                shareOnTwitter(shortURL, newStoreText);
                              });
                          }}
                          className="icon icon--twitter"
                        >
                          <span>Tweet</span>
                        </a>
                        <a
                          href="#"
                          onClick={(e) => {
                            e.preventDefault();
                            this.props
                              .createBitly(
                                `${newStoreURL}${utmSourceWhatsapp}${utmNewStoreURL}`,
                              )
                              .then((shortURL) => {
                                const text = encodeURIComponent(
                                  `${newStoreText} ${shortURL}`,
                                );
                                window.location.href = `https://api.whatsapp.com/send?text=${text}`;
                              });
                          }}
                          data-action="share/whatsapp/share"
                          className="icon icon--whatsapp"
                        >
                          <span>Whatsapp</span>
                        </a>
                      </div>
                    </div>
                  </div>
                )}
              </div>
            </div>

            {/*
                    <div className="dashboard__module dashboard__notifications">
                        <h5 className="module__title">Notificaciones</h5>
                        <div className="module__content">
                            <ul className="list">
                                <li className="">
                                    <div className="notif">
                                        <div className="message">
                                            <div className="note">Tienes una órden nueva</div>
                                            <div className="container-bub">
                                                <div className="bubble_new">Nueva</div>
                                                <div className="bubble_pending">Pendiente</div>
                                            </div>
                                        </div>
                                        <div className="time">20 min</div>
                                    </div>
                                </li>
                                <li>
                                    <div className="notif">
                                        <div className="message">
                                            <div className="note">Se realizó un  pago a tu saldo</div> 
                                        </div>
                                        <div className="time">20 min</div>
                                    </div>
                                </li>
                                <li>
                                    <div className="notif">
                                        <div className="message" >
                                            <div className="note">La orden CR-1467 esta por ser recolectada</div>
                                            <div className="container-bub">
                                                <div className="bubble_recollection">Recolección</div>
                                            </div>
                                        </div>
                                        <div className="time">20 min</div>
                                    </div>
                                </li>
                                <li>
                                    <div className="notif">
                                        <div className="message">
                                            <div className="note">Tienes una órden nueva</div>
                                            <div className="container-bub">
                                                <div className="bubble_urgent">Urgente</div>
                                            </div>
                                        </div>
                                        <div className="time">20 min</div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                */}

            {/* <div className="">
                    <div className="dashboard__module">
                        <h5 className="title">Productos</h5>
                        <ul>
                            <li>Más Vendidos</li>
                            <li>Agotados</li>
                            <li>Pronto por Agotarse</li>
                        </ul>
                    </div>
                </div> */}
          </section>
        ) : (
          <div className="cr__specialMessage">
            <div className="cr__specialMessage-main">
              <h5 className="cr__text--subtitle cr__textColor--colorDark400">
                ¡Hola!
              </h5>
              <p className="cr__text--subtitle3 cr__textColor--colorGray400">
                S&oacute;lo queremos recordarte los puntos de contacto <br />
                donde puedes encontrarnos. ¿C&oacute;mo podemos ayudarte?
              </p>
            </div>

            <div className="cr__specialMessage-content">
              <h5 className="cr__text--paragraph cr__textColor--colorDark400">
                Informaci&oacute;n general
              </h5>
              <h6 className="cr__text--paragraph cr__textColor--colorDark400">
                General:{' '}
                <a
                  style={{ textDecoration: 'none' }}
                  href="mailto:info@canastarosa.com"
                  className="cr__text--paragraph cr__textColor--colorMain300"
                >
                  info@canastarosa.com
                </a>
              </h6>
              <h6 className="cr__text--paragraph cr__textColor--colorDark400">
                Órdenes:{' '}
                <a
                  style={{ textDecoration: 'none' }}
                  href="mailto:pedidos@canastarosa.com"
                  className="cr__text--paragraph cr__textColor--colorMain300"
                >
                  pedidos@canastarosa.com
                </a>
              </h6>
            </div>

            <div className="cr__specialMessage-content">
              <h5 className="cr__text--paragraph cr__textColor--colorDark400">
                Pagos y facturaci&oacute;n
              </h5>
              <h6 className="cr__text--paragraph cr__textColor--colorDark400">
                Pagos:{' '}
                <a
                  style={{ textDecoration: 'none' }}
                  href="mailto:pagos@canastarosa.com"
                  className="cr__text--paragraph cr__textColor--colorMain300"
                >
                  pagos@canastarosa.com
                </a>
              </h6>
              <h6 className="cr__text--paragraph cr__textColor--colorDark400">
                Facturaci&oacute;n:{' '}
                <a
                  style={{ textDecoration: 'none' }}
                  href="mailto:pedidos@canastarosa.com"
                  className="cr__text--paragraph cr__textColor--colorMain300"
                >
                  facturacion@canastarosa.com
                </a>
              </h6>
            </div>

            <div className="cr__specialMessage-content">
              <h5 className="cr__text--paragraph cr__textColor--colorDark400">
                Eventos y servicios
              </h5>
              <h6 className="cr__text--paragraph cr__textColor--colorDark400">
                Membres&iacute;as y servicios PRO:{' '}
                <a
                  style={{ textDecoration: 'none' }}
                  href="mailto:pro@canastarosa.com"
                  className="cr__text--paragraph cr__textColor--colorMain300"
                >
                  pro@canastarosa.com
                </a>
              </h6>
              <h6 className="cr__text--paragraph cr__textColor--colorDark400">
                Informaci&oacute;n y participaci&oacute;n de eventos:{' '}
                <a
                  style={{ textDecoration: 'none' }}
                  href="mailto:eventos@canastarosa.com"
                  className="cr__text--paragraph cr__textColor--colorMain300"
                >
                  eventos@canastarosa.com
                </a>
              </h6>
            </div>

            <div className="cr__specialMessage-button">
              <button onClick={() => this.setCookie()}>Continuar</button>
            </div>
          </div>
        )}
      </div>
    );
  }
}

// Add Redux state and actions to component´s props
function mapStateToProps(state) {
  const { orders, myStore } = state;
  const stats = orders.vendor.stats;
  return {
    orders: orders.vendor.results,
    stats,
    myStore: myStore.data,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      getOrders: ordersActions.getOrders,
      getOrderStats: ordersActions.getOrderStats,
      createBitly: appActions.createBitly,
    },
    dispatch,
  );
}

// Export Component
export default connect(mapStateToProps, mapDispatchToProps)(Dashboard);
