import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { renderRoutes } from 'react-router-config';

import { myStoreActions, statusWindowActions } from '../../Actions';

const StoreProducts = (props) => (
  <div className="storeApp__products">
    {renderRoutes(props.route.routes, {
      myStore: props.myStore,
      products: props.products,
    })}
  </div>
);

// Map Redux Props and Actions to component
function mapStateToProps(state) {
  return {};
}
function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      openStatusWindow: statusWindowActions.open,
      updateProduct: myStoreActions.updateProduct,
    },
    dispatch,
  );
}

// Export Component
export default connect(mapStateToProps, mapDispatchToProps)(StoreProducts);
