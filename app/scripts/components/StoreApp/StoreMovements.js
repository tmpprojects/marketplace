import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Movements from './Movements/Movements';

class StoreMovements extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {
    return <Movements />;
  }
}

function mapStateToProps(state) {
  return {};
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({}, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(StoreMovements);
