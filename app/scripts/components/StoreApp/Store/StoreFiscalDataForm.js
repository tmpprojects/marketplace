import React from 'react';
import {
  Field,
  reduxForm,
  formValueSelector,
  getFormValues,
  getFormSyncErrors,
  getFormSubmitErrors,
  SubmissionError,
} from 'redux-form';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { getUIErrorMessage, getErrors } from '../../../Utils/errorUtils';
import {
  required,
  maxLength13,
  maxLength140,
  number,
} from '../../../Utils/forms/formValidators';
import { InputField } from '../../../Utils/forms/formComponents';
import { normalizePhone } from '../../../Utils/forms/formNormalizers';
import './StoreFiscalDataForm.scss';
import { modalBoxActions } from '../../../Actions';
import FiscalPopup from './FiscalPopup';
import FiscalNews from './FiscalNews';
import { myStoreActions, statusWindowActions } from './../../../Actions';
import { myStoreErrorTypes } from './../../../Constants/mystore.constants';

/*
 * Form Validation Function
 * @param {object} values : Form values
 */
const streetValidation = (value) =>
  value && value.length < 5 ? 'Escribe tu dirección fiscal completa.' : undefined;
const neighborhoodValidation = (value) =>
  value && value.length < 5
    ? 'Escribe el nombre completo de la colonia.'
    : undefined;
const cityValidation = (value) =>
  value && value.length < 3
    ? 'Escribe el nombre de la delegación/municipio.'
    : undefined;
const postalValidation = (value) =>
  value && value.length !== 5 ? 'Verifica tu código postal.' : undefined;
const lookForPhoneNumber = (value, allValues, c) => {
  let error = undefined;
  // Check if both phone number fields are empty
  if (!allValues.phone) {
    error = 'Escribe el número telefónico.';
  }
  return error;
};
const selectValidation = (value) =>
  value && value === 'none' ? 'Escoge una opción' : undefined;

let hiddenButton = '';

// const ValidateSaveButton = props => {
//   const checkFiscal = props.myStoreFiscal.fiscal_registry;

//   if (checkFiscal !== undefined && checkFiscal !== null && checkFiscal !== '') {
//     return null;
//   } else {
//     return (

//     );
//   }
// };

class StoreFiscalDataForm extends React.Component {
  /*
   * Constructor Function
   * @param {object} props React Component Properties
   */
  constructor(props) {
    super(props);

    this.state = {
      shouldRenderFiscalDetails:
        this.props.initialValues.fiscal_registry === 'with-rfc',
      hiddenButton,
    };

    //this.onSubmit = this.onSubmit.bind(this);
  }

  componentDidMount = () => {
    // this.openModalBoxNews();
    const checkFiscal = this.props.myStoreFiscal.fiscal_registry;
    if (checkFiscal !== undefined && checkFiscal !== null && checkFiscal !== '') {
      this.setState({
        hiddenButton: 'hide',
        shouldRenderFiscalDetails: this.state.shouldRenderFiscalDetails,
      });
    }
  };

  componentWillReceiveProps = (nextProps) => {
    this.buttonRef = React.createRef();
    if (
      this.props.initialValues.fiscal_registry !==
      nextProps.initialValues.fiscal_registry
    ) {
      this.setState({
        shouldRenderFiscalDetails:
          nextProps.initialValues.fiscal_registry === 'with-rfc',
      });
    }
  };

  onChangeFiscalData = (e) => {
    const { value } = e.target;
    this.setState({
      shouldRenderFiscalDetails: value === 'with-rfc',
    });
    this.props.onChangeFiscalRegistry(value);
  };

  /*
  updateStore = (stateUpdate = {}) => (
    // Update State and Store via API
    this.props.updateMyStore(stateUpdate)
    .then(response => {
        this.props.openStatusWindow({
            type: 'success',
            message: 'Información actualizada.'
        });
    }).catch(error => {
        // Display UI error messages
        const uiMessage = error.response.data.non_field_errors || getUIErrorMessage(error.response, myStoreErrorTypes);
        this.props.openStatusWindow({
            type: 'error',
            message: uiMessage
        });

        // Get Errors list
        let errorsList = getErrors(error.response.data, myStoreErrorTypes);
        errorsList = errorsList.reduce((a, b) => ({
            ...a,
            [b.label]: b.ui_message
        }), {});

        // Show errors on form fields
        if (errorsList) {
            throw new SubmissionError(errorsList);
        } else {
            throw new SubmissionError({
                _error: 'Ocurrió un problema al actualizar tu información. Por favor, intenta más tarde.'
            });
        }
    })
)
*/

  // validateSelection = () =>{

  // }

  submitForm = (formValues) => {
    // const {
    //     cover,
    //     photo,
    //     work_schedules,
    //     shipping_schedules,
    //     ...values
    // } = formValues;

    // // Prepare data for API
    // const storeData = {
    //     ...values,
    //     link_facebook: values.facebook,
    //     link_pinterest: values.pinterest,
    //     link_instagram: values.instagram
    // };

    // Update State and Store via API
    return this.updateStore(storeData);
  };

  openModalBox = (props) => {
    //this.RFCSubmit.click();
    if (this.state.shouldRenderFiscalDetails) {
      this.buttonRef.current.click();
      // this.props.onChangeFiscalRegistry('with-rfc');
    } else {
      this.props.onChangeFiscalRegistry('without-rfc');
    }
    // this.props.openModalBox(() => (
    //    <FiscalPopup closeModalBox={this.closeModalBox} />
    //  ));
  };

  openModalBoxNews = (props) => {
    this.props.openModalBox(() => <FiscalNews closeModalBox={this.closeModalBox} />);
  };

  closeModalBox = () => {
    this.props.closeModalBox();
  };

  // onSubmit = values => {
  //   const { initialValues } = this.props;
  //   const actionForm = Object.keys(initialValues).length > 1 ? 'update' : 'create';

  //   this.props.saveFiscalData(values, actionForm)
  //   .then(response => {
  //     console.log("dispara nuevo error");
  //     throw new SubmissionError({ rfc: 'RFC does not exist', _error: 'RFC does not exist' });
  //     // if (actionForm === 'create') {
  //     //   this.setState({ actionForm: 'update' });
  //     // }
  //   })
  //   .catch(error => {
  //     // // Display UI error messages
  //     // const uiMessage = error.response.data.non_field_errors || getUIErrorMessage(error.response, myStoreErrorTypes);
  //     // this.props.openStatusWindow({
  //     //     type: 'error',
  //     //     message: uiMessage
  //     // });
  //   });
  // }

  onSubmit = (formValues) => {
    if (
      formValues.hasOwnProperty('fiscalData') !== null &&
      formValues.fiscalData !== ''
    ) {
      const { initialValues } = this.props;
      const action = Object.keys(initialValues).length > 1 ? 'update' : 'create';
      let actionForm = '';

      if (action === 'create') {
        actionForm = this.props.addFiscalData;
      } else {
        actionForm = this.props.updateFiscalData;
      }

      return actionForm(formValues).then((response) => {
        if (response.hasOwnProperty('data')) {
          this.props.openStatusWindow({
            type: 'success',
            message: 'Información actualizada.',
          });
        } else if (response.hasOwnProperty('rfc')) {
          //If 400 error with message
          const uiMessage = response.rfc[0];
          this.props.openStatusWindow({
            type: 'error',
            message: 'Verifica tu RFC',
          });
          throw new SubmissionError({
            rfc: uiMessage,
            _error: 'Verifica tu RFC',
          });
        } else {
          //First other error
          const errorName = Object.keys(response)[0];
          // Display other error message
          this.props.openStatusWindow({
            type: 'error',
            message: 'Error. Por favor, intenta de nuevo.',
          });

          throw new SubmissionError({
            [errorName]: response[errorName][0],
            _error: response[errorName][0],
          });
        }
      });
    }
    return false;
  };

  render() {
    const {
      handleSubmit,
      options,
      formErrors,
      formValues,
      submitErrors,
    } = this.props;
    const formHasErrors = Object.keys(formErrors).length || false;
    const { shouldRenderFiscalDetails } = this.state;
    //handleSubmit
    const { ...props } = this.props;
    return (
      <div className="ui-detailed-block__content">
        {/*      

       --- { shouldRenderFiscalDetails } ---

       <pre>{JSON.stringify(this.state, null, 2) }</pre>
       <pre>{JSON.stringify(this.props., null, 2) }</pre>

        */}
        <form className="form billing__form" onSubmit={handleSubmit(this.onSubmit)}>
          <fieldset>
            <div className="form billing__option">
              <div className="option">
                <Field
                  type="radio"
                  className="dots"
                  id="not_FisicalData"
                  name="fiscal_registry"
                  value="without-rfc"
                  onChange={this.onChangeFiscalData}
                  component={({ input, meta, ...props }) => (
                    <React.Fragment>
                      <input {...input} {...props} />
                      <label htmlFor="not_FisicalData" className="dot">
                        No cuento con RFC
                      </label>
                      <span className="note note-style">
                        <img src="https://s3.us-east-2.amazonaws.com/canastarosa/temp/assets/info-ico.svg" />
                        <span>
                          Si no cuentas con RFC, puedes vender en Canasta Rosa.
                          <br />
                          Toma en cuenta que por motivo de regulaciones fiscales,
                          Canasta Rosa debe presentar declaraciones sobre todas las
                          ventas realizadas dentro de la plataforma.
                          <br /> Es por eso que tendremos que retener el total de IVA
                          y el máximo ISR (20%) que presenten tus productos.
                        </span>
                      </span>
                    </React.Fragment>
                  )}
                />
              </div>

              <div className="option">
                <Field
                  type="radio"
                  className="dots"
                  id="with_FisicalData"
                  name="fiscal_registry"
                  value="with-rfc"
                  onChange={this.onChangeFiscalData}
                  component={({ input, meta, ...props }) => (
                    <React.Fragment>
                      <input {...input} {...props} />
                      <label htmlFor="with_FisicalData" className="dot">
                        Cuento con RFC
                      </label>
                      <span className="note note-style">
                        <img src="https://s3.us-east-2.amazonaws.com/canastarosa/temp/assets/info-ico.svg" />
                        <span>
                          Canasta Rosa utiliza esta información para: <br />
                          1. Facturarte nuestras comisiones.
                          <br />
                          2. Realizar la deducción de IVA (0-8%) e ISR (2-5.4%)
                          correspondientes a tus productos y cumplir con nuestras
                          obligaciones fiscales.
                        </span>
                      </span>

                      <span className="note note-style">
                        <img src="https://s3.us-east-2.amazonaws.com/canastarosa/temp/assets/info-ico.svg" />
                        <span>
                          Canasta Rosa no realiza ninguna retención de impuestos si
                          eres persona moral.
                        </span>
                      </span>
                    </React.Fragment>
                  )}
                />
              </div>
            </div>
          </fieldset>

          {shouldRenderFiscalDetails && (
            <fieldset className="billing__form-fields">
              <div className="form__data form__data-field">
                <label className="title" htmlFor="entity_type">
                  Tipo persona *
                </label>
                <Field
                  id="entity_type"
                  className="display_name"
                  // {`${formErrors.entity_type || submitErrors.entity_type ? 'error' : ''}`}
                  name="entity_type"
                  component="select"
                  validate={[selectValidation]}
                >
                  <option value="none" className="display_name">
                    Elige un tipo de persona
                  </option>
                  {options.entities.map((entity, index) => (
                    <option key={index} value={entity.value}>
                      {entity.display_name}
                    </option>
                  ))}
                </Field>
              </div>

              <Field
                name="name"
                id="name"
                component={InputField}
                validate={required}
                label="Raz&oacute;n social *"
              />

              <Field
                name="rfc"
                id="rfc"
                component={InputField}
                validate={[required, maxLength13]}
                label="RFC *"
              />

              {formValues.errors && <div>{formValues.errors.rfc}</div>}

              <div className="billing__form-address">
                <Field
                  name="street"
                  id="street"
                  className="street"
                  component={InputField}
                  validate={[required, maxLength140, streetValidation]}
                  label="Domicilio fiscal *"
                  placeholder="Calle"
                />

                <div className="address_content">
                  <Field
                    name="exterior_number"
                    id="exterior_number"
                    component={InputField}
                    validate={required}
                    placeholder="Exterior"
                  />

                  <Field
                    name="interior_number"
                    id="interior_number"
                    component={InputField}
                    placeholder="Interior"
                  />
                </div>
              </div>

              <div className="half-content">
                <Field
                  name="colony"
                  id="colony"
                  className="field"
                  component={InputField}
                  validate={[required, neighborhoodValidation]}
                  placeholder="Colonia"
                />
                <Field
                  name="postal_code"
                  id="postal_code"
                  className="field"
                  component={InputField}
                  validate={[required, number, postalValidation]}
                  placeholder="C&oacute;digo postal"
                />
              </div>

              <div className="half-content">
                <Field
                  name="municipality"
                  id="municipality"
                  className="field"
                  component={InputField}
                  validate={[required, cityValidation]}
                  placeholder="Municipio/Delegaci&oacute;n"
                />
                <Field
                  id="state"
                  name="state"
                  className="state"
                  component="select"
                  placeholder="Estado"
                >
                  <option value={null}>Elige un Estado</option>
                  {options.states.map((state, index) => (
                    <option key={index} value={state.value}>
                      {state.display_name}
                    </option>
                  ))}
                </Field>
              </div>

              <div className="half-content">
                <Field
                  name="phone"
                  id="phone"
                  className="phone"
                  component={InputField}
                  validate={[required, lookForPhoneNumber]}
                  normalize={normalizePhone}
                  label="Tel&eacute;fono *"
                />
                <Field
                  name="email"
                  id="email"
                  className="email"
                  component={InputField}
                  validate={required}
                  label="Email *"
                />
              </div>

              <div className="regime">
                <label className="title" htmlFor="regimen">
                  R&eacute;gimen *
                </label>
                <Field
                  id="regime"
                  className="display_name"
                  // className={`${formErrors.regime || submitErrors.regime ? 'error' : ''}`}
                  name="regime"
                  component="select"
                  validate={[selectValidation]}
                >
                  <option value="none">Elige un r&eacute;gimen</option>
                  {options.regimes.map((regime, index) => (
                    <option key={index} value={regime.value}>
                      {regime.display_name}
                    </option>
                  ))}
                </Field>
              </div>

              {/* <div className="form__data-fileUpload">
                <input
                  type="file"
                  name="file"
                  id="rfc_picture"
                  className="upload_input"
                  accept="image/x-png,image/gif,image/jpeg,image/jpg"
                />
                <label htmlFor="rfc_picture">Agregar imagen RFC</label>
              </div> */}
              <div className="form-button_container">
                <button
                  type="submit"
                  disabled={formHasErrors}
                  className="c2a_square"
                  id="RFCSubmit"
                  ref={this.buttonRef}
                >
                  Guardar
                </button>
              </div>
            </fieldset>
          )}
        </form>
        {/*
        <div className="form-button_error">
        Ocurrio el siguiente error:
        </div>
        */}

        {/*
        <div
          className={
            'form-button_container save-and-update ' + this.state.hiddenButton
          }
        >
          <button
            onClick={this.openModalBox}
            className={'c2a_square ' + this.state.hiddenButton}
          >
            Guardar y actualizar
          </button>
        </div>
        */}
      </div>
    );
  }
}

// Wrap component within reduxForm
const formName = 'StoreFiscalData_Form';
StoreFiscalDataForm = reduxForm({
  form: formName,
  enableReinitialize: true,
  keepDirtyOnReinitialize: true, // <---- Prevent 'dirty' fields from updating
})(StoreFiscalDataForm);

// Add Redux state and actions to component´s props
const selector = formValueSelector(formName);
function mapStateToProps(state) {
  const { myStore } = state;
  let initialValues = {};

  // Attempt to get existing fiscal data info.
  if (myStore.data.fiscal_data && Object.keys(myStore.data.fiscal_data).length) {
    initialValues = {
      ...myStore.data.fiscal_data,
      state: myStore.data.fiscal_data.state.value,
      regime: myStore.data.fiscal_data.regime.value,
    };
  }

  return {
    myStoreFiscal: myStore.data,
    initialValues: {
      ...initialValues,
      fiscal_registry: myStore.data.fiscal_registry,
    },
    formValues: getFormValues('StoreFiscalData_Form')(state) || {},
    formErrors: getFormSyncErrors(formName)(state),
    submitErrors: getFormSubmitErrors('StoreFiscalData_Form')(state),
  };
}
function mapDispatchToProps(dispatch) {
  const { setMyStoreImprovements, addFiscalData, updateFiscalData } = myStoreActions;

  return bindActionCreators(
    {
      openModalBox: modalBoxActions.open,
      closeModalBox: modalBoxActions.close,
      setMyStoreImprovements,
      addFiscalData,
      updateFiscalData,
      openStatusWindow: statusWindowActions.open,
    },
    dispatch,
  );
}

// Export Component
export default connect(mapStateToProps, mapDispatchToProps)(StoreFiscalDataForm);
