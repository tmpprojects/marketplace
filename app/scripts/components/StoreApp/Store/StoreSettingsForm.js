import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
  Field,
  reduxForm,
  SubmissionError,
  formValueSelector,
  getFormValues,
} from 'redux-form';

import { UIIcon } from '../../../Utils/UIIcon';
import SchedulesManager from '../Utils/SchedulesManager';
import { getUIErrorMessage, getErrors } from '../../../Utils/errorUtils';
import { required, email } from '../../../Utils/forms/formValidators';
import { addHourPeriodSuffix, formatDateYYMMDD } from '../../../Utils/dateUtils';
import { ResponsiveImage } from '../../../Utils/ImageComponents.jsx';
import { getMyStoreDetail } from '../../../Reducers/mystore.reducer';
import {
  myStoreActions,
  statusWindowActions,
  typeformActions,
} from '../../../Actions';
import { myStoreErrorTypes } from '../../../Constants/mystore.constants';
import { InputField, TextArea } from '../../../Utils/forms/formComponents';
import {
  normalizePhone,
  removeDomainProtocolFromURL,
  normalizeToLowerCaseAndTrim,
} from '../../../Utils/forms/formNormalizers';
import withCounter from '../../hocs/withCounter';
import Calendar from 'react-calendar';
import { range } from '../../../Utils/genericUtils';
import SectionsForm from '../Sections/SectionsForm';
import StoreGalleryForm from './StoreGalleryForm';
import { Tooltip } from '../../../Utils/Tooltip';
import StoreStatus from './StoreStatus/StoreStatus';
import ErrorFormDisplay from '../Utils/ErrorFormDisplay/ErrorFormDisplay';

// SOCIAL NETWORKS FIELDS
const socialNetworkField = ({ input, label, meta, normalizeValue, ...props }) => (
  <div className={`form__data form__data-${props.className} social-media`}>
    <label className="title" htmlFor={props.id}>
      {label}
    </label>
    <input
      {...input}
      {...props}
      onBlur={(value) => input.onBlur(normalizeValue(value))}
    />
    {meta.touched &&
      ((meta.error && <div className="form_status danger">{meta.error}</div>) ||
        (meta.warning && <div className="form_status warning">{meta.warning}</div>))}
  </div>
);

// VALIDATION FOR PHONE/MOBILE NUMBERS
const lookForPhoneNumber = (value, allValues, c) => {
  let error;
  // Check if mobile phone number field is empty
  if (!allValues.mobile) {
    error = 'Escribe tu número telefónico.';
  }
  return error;
};

// FORM FIELDS FORMATTING
const TextAreaWithCounter = withCounter(TextArea);
const InputWithCounter = withCounter(InputField);
const SocialNetworkFieldInput = withCounter(socialNetworkField);

/*---------------------------------------------------
    STORE SETTINGS FORM COMPONENT
---------------------------------------------------*/
class StoreSettingsForm extends React.Component {
  /*
   * Constructor Function
   * @param {object} props React Component Properties
   */
  constructor(props) {
    super(props);
    this.state = {
      schedules: props.myStore.work_schedules,
      currentSchedule: null,
      shipping_schedules: props.myStore.shipping_schedules,
      shouldRenderSchedulesWindow: false,
      initialDate: [new Date(), new Date()],
      allVacations: props.myStore.vacations || [],
      shouldRenderCalendar: false,
      editedVacation: null,
      errorForm: false,
      errorsWordsList: {},
    };
  }

  componentDidMount() {
    this.getVacations();
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.myStore !== this.props.myStore) {
      this.setState({
        schedules: nextProps.myStore.work_schedules,
      });
    }
  }

  /**
   * onImageFieldChange()
   * Handler for photo (input) changes.
   * @param {object} event | Form Event.
   * @returns {Promise} | API call promise.
   */
  onImageFieldChange = (event) => {
    const fileData = event.target.files[0];
    this.setState({ file: fileData });
    event.preventDefault();

    // Define image section
    const imageSection =
      event.target.getAttribute('id') === 'profile_picture' ? 'photo' : 'cover';

    // Verify File size (8MB)
    if (event.target.files[0].size / 1024 / 1024 > 8) {
      event.target.value = '';
      this.props.openStatusWindow({
        type: 'error',
        message: 'Por favor, elige una imagen menor a 8MB.',
      });
      return false;
    }

    // Send File data to server via API Photo
    const formData = new FormData();
    formData.append(imageSection, fileData);

    return this.props
      .uploadProfilePhoto(formData)
      .then((response) => {
        this.props.openStatusWindow({
          type: 'success',
          message: 'Información actualizada.',
        });
      })
      .catch((error) => {
        this.props.openStatusWindow({
          type: 'error',
          message: 'Error. Por favor, intenta de nuevo.',
        });
        throw new SubmissionError({
          _error:
            'Ocurrió un problema al actualizar tu información. Por favor, intenta de nuevo.',
        });
      });
  };

  /**
   * onPickupScheduleChanged()
   * Update store pickup schedules through the API
   * @param {object} schedule | Pickup schedule object
   */
  onPickupScheduleChanged = (schedule) => {
    let { shipping_schedules } = this.state;

    // Add/Remove pickup schedules from main list
    const scheduleFoundIndex = shipping_schedules.findIndex(
      (a) => a.value === schedule.id,
    );
    if (scheduleFoundIndex === -1) {
      shipping_schedules = [...shipping_schedules, schedule];
    } else {
      shipping_schedules = shipping_schedules.filter((a) => a.value !== schedule.id);
    }
    // Update state
    this.setState(
      {
        shipping_schedules,
      },
      // When the component´s state gets updated,
      // send new schedule information to the API.
      () =>
        this.updateStore({
          shipping_schedules: shipping_schedules.map((s) => s.value),
        }),
    );
  };

  /**
   * onScheduleAdded()
   * Gets a 'schedule' object, update the component´s schedules list,
   * and send new timetables to API.
   * @param {object} newSchedule : A weekday(s) schedule object.
   */
  onScheduleAdded = (weekdays) => {
    const { schedules } = this.state;

    // Construct schedules list
    // Create a list of duplicated weekdays with updated values
    const replacedSchedules = schedules.reduce((accumulator, schedule) => {
      const foundIndex = weekdays.findIndex((a) => schedule.value === a.value);
      if (foundIndex !== -1) {
        return [...accumulator, weekdays[foundIndex]];
      }
      //
      return [...accumulator, schedule];
    }, []);

    // Create a list of new weekdays
    const newSchedules = weekdays.filter(
      (s) => replacedSchedules.findIndex((a) => a.value === s.value) === -1,
    );

    // Update state
    this.setState(
      {
        currentSchedule: null,
        shouldRenderSchedulesWindow: false,
        schedules: [...replacedSchedules, ...newSchedules],

        // When the component´s state gets updated,
        // send new schedule information to the API.
      },
      () => this.updateSchedules(),
    );
  };

  /**
   * closeScheduleManager()
   */
  closeScheduleManager = () => {
    this.setState({
      shouldRenderSchedulesWindow: false,
      currentSchedule: null,
    });
  };

  /**
   * openeScheduleManager()
   */
  openScheduleManager = () => {
    this.setState({
      currentSchedule: null,
      shouldRenderSchedulesWindow: true,
    });
  };

  /**
   * updateStore()
   * Updates part [PATCH] of the store´ s information
   * defined on the 'stateUpdate' parameter.
   * @param {object} stateUpdate | Object with store properties to update.
   * @returns {Promise} | API call promise
   */
  updateStore = (stateUpdate = {}) =>
    // Update State and Store via API
    this.props
      .updateMyStore(stateUpdate)
      .then((response) => {
        this.props.openStatusWindow({
          type: 'success',
          message: 'Información actualizada.',
        });
      })
      .catch((error) => {
        // Display UI error messages
        const uiMessage =
          error.response.data.non_field_errors ||
          getUIErrorMessage(error.response, myStoreErrorTypes);
        this.props.openStatusWindow({
          type: 'error',
          message: uiMessage,
        });

        // Get Errors list
        let errorsList = getErrors(error.response.data, myStoreErrorTypes);
        errorsList = errorsList.reduce(
          (a, b) => ({
            ...a,
            [b.label]: b.ui_message,
          }),
          {},
        );

        // Show errors on form fields
        if (errorsList) {
          throw new SubmissionError(errorsList);
        } else {
          throw new SubmissionError({
            _error:
              'Ocurrió un problema al actualizar tu información. Por favor, intenta más tarde.',
          });
        }
      });

  /**
   * submitForm()
   * Update the Basic Store Profile information
   * @param {object} formValues | Store information/properties to update.
   */
  submitForm = (formValues) => {
    const {
      cover,
      photo,
      work_schedules,
      shipping_schedules,
      ...values
    } = formValues;

    // Prepare data for API
    const storeData = {
      ...values,
      link_facebook: values.facebook,
      link_pinterest: values.pinterest,
      link_instagram: values.instagram,
    };

    // Update State and Store via API
    return this.updateStore(storeData);
  };

  /**
   * removeSchedule()
   * Removes a defined schedule from the store timetable list
   * and updates the API information.
   * @param {object} | Schedule object
   */
  removeSchedule = (schedule) => {
    if (
      confirm(
        '¿Estás seguro de querer eliminar este horario?\nLa acción no podrá deshacerse.',
      )
    ) {
      // Update state
      this.setState(
        {
          schedules: this.state.schedules.filter((a) => a.value !== schedule.value),

          // When the component´s state gets updated,
          // send new schedule information to the API.
        },
        () => this.updateSchedules(),
      );
    }
  };

  /**
   * updateSchedules()
   * Format schedules to match API´s expected format
   * and updates the store´ s timetable information
   */
  updateSchedules = () => {
    // Parse store schedules to match API expected format
    const work_schedules = this.state.schedules.map((c) => ({
      week_day: c.value,
      end: c.close.value,
      start: c.open.value,
    }));

    // Update Store Schedules
    this.updateStore({ work_schedules });
  };

  /**
   * renderPickupSchedules()
   * Render pickup schedules list.
   */
  renderPickupSchedules = () => {
    const {
      pickupSchedules: { schedules: pickupSchedules },
    } = this.props;
    const { shipping_schedules } = this.state;
    return pickupSchedules.map((schedule) => {
      const activeSchedule = shipping_schedules.find((a) => a.value === schedule.id);
      return (
        <li
          key={schedule.id}
          className={`button-option ${activeSchedule ? 'active' : ''}`}
        >
          <button
            type="button"
            onClick={(e) => this.onPickupScheduleChanged(schedule)}
            className={`button-tag ${schedule.className}`}
          >
            {schedule.schedules.pickup}
          </button>
        </li>
      );
    });
  };

  /**
   * renderWeekdayNames()
   * Format and returns a string of weekday names.
   * @param {array} weekdays : An array of weekdays {name, value}
   * @returns {string}
   */
  renderWeekdayNames = (weekdays) => {
    let displayString = '';
    let isConsecutive = false;

    // If we have only one weekday on the schedule,
    // Append it to the display string.
    if (weekdays.length < 2) {
      displayString = weekdays[0].name;

      // ...otherwise, iterate through weekdays,
      // to format a proper display string.
    } else {
      weekdays.reduce((a, b, i) => {
        // If we´re on the first iteration
        // or last iteration wasn´t a consecutive weekday.
        // Append current weekday and check for weekdays sequence.
        if (i === 1 || !isConsecutive) {
          displayString += a.name;

          // Verify if the schedule is a consecutive weekday sequence
          // and append corresponding symbol to display string.
          if (a.value + 1 === b.value) {
            isConsecutive = true;
            displayString += '-';
          } else {
            isConsecutive = false;
            displayString += ',';
          }

          // If we´re on last iteration. Append last weekday too.
          displayString += i === weekdays.length - 1 ? b.name : '';
          return b;
        }

        // Verify if next weekday is consecutive
        // of the current iteration
        if (a.value + 1 === b.value) {
          isConsecutive = true;
        } else {
          isConsecutive = false;
        }

        // If weekdays are not a consecutive sequence.
        // Append current weekday to display string.
        if (!isConsecutive) {
          displayString += a.name + ',';
        }

        // If we´re on the last iteration.
        // Append the last weekday too.
        if (i === weekdays.length - 1) {
          displayString += b.name;
        }

        // Return next weekday
        return b;
      });
    }
    return displayString;
  };

  /**
   * renderTimeTable()
   * Renders a list of weekdays and schedules
   * @returns {array}
   */
  renderTimeTable = () => {
    const { schedules } = this.state;

    return schedules.map((schedule) => (
      <li className="schedules-list__item" key={schedule.value}>
        <div className="schedules_info">
          <div className="weekday">{schedule.name}</div>
          <div className="schedule">
            {`${addHourPeriodSuffix(schedule.open.value)} 
                        - 
                        ${addHourPeriodSuffix(schedule.close.value)}`}
          </div>
        </div>

        <div className="schedules_actions">
          <UIIcon
            icon="delete"
            type="error"
            className="delete button"
            onClick={(e) => this.removeSchedule(schedule)}
          >
            Eliminar
          </UIIcon>
          <button
            className="button-simple"
            onClick={(e) => {
              this.setState({
                shouldRenderSchedulesWindow: true,
                currentSchedule: schedule,
              });
            }}
          >
            Editar
          </button>
        </div>
      </li>
    ));
  };

  onCalendarDateChange = (currentVacation) => {
    const { editedVacation } = this.state;
    const start = formatDateYYMMDD(currentVacation[0]);
    const end = formatDateYYMMDD(currentVacation[1]);
    if (editedVacation === null) {
      //Add Vacation
      // Parse store schedules to match API expected format
      const vacation = {
        end: end,
        start: start,
      };
      this.addVacation(vacation);
    } else {
      //Update Vacation
      const vacation = {
        end: end,
        start: start,
        range_id: editedVacation.range_id,
      };
      this.updateVacation(vacation);
    }
  };

  addVacation = (stateUpdate = {}) =>
    // Update State and Store via API
    this.props
      .addMyStoreVacations(stateUpdate)
      .then((response) => {
        this.setState({
          currentVacation: null,
          shouldRenderCalendar: false,
        });
        this.props.openStatusWindow({
          type: 'success',
          message: 'Información actualizada.',
        });
      })
      .catch((error) => {
        // Display UI error messages
        const uiMessage =
          error.response.data.non_field_errors ||
          getUIErrorMessage(error.response, myStoreErrorTypes);
        this.props.openStatusWindow({
          type: 'error',
          message: uiMessage,
        });

        // Get Errors list
        let errorsList = getErrors(error.response.data, myStoreErrorTypes);
        errorsList = errorsList.reduce(
          (a, b) => ({
            ...a,
            [b.label]: b.ui_message,
          }),
          {},
        );

        // Show errors on form fields
        if (errorsList) {
          throw new SubmissionError(errorsList);
        } else {
          throw new SubmissionError({
            _error:
              'Ocurrió un problema al actualizar tu información. Por favor, intenta más tarde.',
          });
        }
      });

  getVacations = () => {
    this.props
      .getMyStoreVacations()
      .then((response) => {})
      .catch((error) => {
        // Display UI error messages
        const uiMessage =
          error.response.data.non_field_errors ||
          getUIErrorMessage(error.response, myStoreErrorTypes);
        this.props.openStatusWindow({
          type: 'error',
          message: uiMessage,
        });

        // Get Errors list
        let errorsList = getErrors(error.response.data, myStoreErrorTypes);
        errorsList = errorsList.reduce(
          (a, b) => ({
            ...a,
            [b.label]: b.ui_message,
          }),
          {},
        );

        // Show errors on form fields
        if (errorsList) {
          throw new SubmissionError(errorsList);
        } else {
          throw new SubmissionError({
            _error:
              'Ocurrió un problema al actualizar tu información. Por favor, intenta más tarde.',
          });
        }
      });
  };

  renderVacations = () => {
    const { vacations } = this.props;
    return vacations.map((vacation) => (
      <div key={vacation?.id} className="ui-calendar-selector">
        <div
          className="ui-calendar-selector__day"
          onClick={(e) =>
            this.setState({
              shouldRenderCalendar: true,
              editedVacation: {
                start: vacation.start,
                end: vacation.end,
                range_id: vacation.id,
              },
            })
          }
        >
          {`${vacation.start} al ${vacation.end} `}
        </div>
        <div className="schedules_actions">
          <UIIcon
            icon="delete"
            type="error"
            className="delete button"
            onClick={(e) => this.deleteVacation(vacation)}
          >
            Eliminar
          </UIIcon>
        </div>
      </div>
    ));
  };

  deleteVacation = (vacation) =>
    // Update State and Store via API
    this.props
      .deleteMyStoreVacations(vacation.id)
      .then((response) => {
        this.setState({
          shouldRenderCalendar: false,
        });
        this.props.openStatusWindow({
          type: 'success',
          message: 'Información actualizada.',
        });
      })
      .catch((error) => {
        console.log(error);
      });

  updateVacation = (stateUpdate = {}) => {
    this.props
      .updateMyStoreVacations(stateUpdate)
      .then((response) => {
        this.setState({
          currentVacation: null,
          shouldRenderCalendar: false,
        });
        this.props.openStatusWindow({
          type: 'success',
          message: 'Información actualizada.',
        });
        this.setState;
      })
      .catch((error) => {
        console.log(error);
      });
  };

  openCalendar = () => {
    this.setState({
      shouldRenderCalendar: true,
    });
  };

  scrollTop = () => {
    window.scrollTo({ top: 250, behavior: 'smooth' });
  };

  checkField = (field = '', badWords = []) => {
    const fieldArray = field.toLowerCase().split(' ');

    const badWordsList = badWords.map((item) => item.toLowerCase());

    const filteredBadWords = fieldArray.filter((word) =>
      badWordsList.includes(word.toLowerCase()),
    );

    return filteredBadWords;
  };

  testOnSubmit = (formValues) => {
    this.setState({ errorForm: false, errorsWordsList: {} });

    const { list = [] } = this.props?.prohibitedWords;

    const { name, slogan, excerpt, description } = this.props.formValues;

    // If prohibited words list doesnt have items
    if (list.length === 0) {
      return this.submitForm(formValues);
    }

    if (list.length > 0) {
      const badWordsName = [...new Set(this.checkField(name, list))],
        badWordsSlogan = [...new Set(this.checkField(slogan, list))],
        badWordsExcerpt = [...new Set(this.checkField(excerpt, list))],
        badWordsDescription = [...new Set(this.checkField(description, list))];

      const items = [
        badWordsName,
        badWordsSlogan,
        badWordsExcerpt,
        badWordsDescription,
      ];

      if (items.some((item) => item.length > 0)) {
        this.setState((state) => ({
          ...state,
          errorsWordsList: {
            ...state.errorsWordsList,
            badWordsName,
            badWordsSlogan,
            badWordsExcerpt,
            badWordsDescription,
          },
          errorForm: true,
        }));
        this.scrollTop();
      } else {
        return this.submitForm(formValues);
      }
    }
  };

  /*
   * React Component Life Cycle Functions
   */

  render() {
    if (this.props.myStore.loading) {
      //return null;
    }
    //
    const { photo, cover, is_active } = this.props.myStore;
    const {
      handleSubmit,
      vacations,
      pickupSchedules: { schedules: pickupSchedules },
    } = this.props;
    const {
      shouldRenderSchedulesWindow,
      currentSchedule,
      schedules,
      shouldRenderCalendar,
      allVacations,
      errorForm,
      errorsWordsList,
    } = this.state;

    //TODO check if pending necessary
    const isStoreActive = is_active?.bool;
    //
    return (
      <section className="storeApp__module storeApp__main">
        <h3>Informaci&oacute;n General</h3>
        {/* <StoreGalleryForm></StoreGalleryForm> */}

        <div className="form form--annotated">
          <form onSubmit={handleSubmit(this.testOnSubmit)}>
            <fieldset className="ui-detailed-block cover">
              <div className="form__cover">
                <div className="form__cover-image">
                  {!cover ? (
                    <div className="ui_message">
                      Imagen de Portada
                      <br />
                      (1280px x 550px)
                    </div>
                  ) : (
                    <ResponsiveImage src={cover} alt="Cover Photo" />
                  )}
                </div>

                <div className="container_icon">
                  <input
                    type="file"
                    name="file"
                    id="cover_picture"
                    style={{ display: 'none' }}
                    onChange={this.onImageFieldChange}
                  />
                  <label htmlFor="cover_picture" className="icon_oval-add">
                    Editar foto de portada
                  </label>
                  <Tooltip message="Se recomienda una imagen de 1280px x 550px en formatos .png, .jpg y .gif a un tamaño máximo de 8MB." />
                </div>
              </div>

              <div className="form__profile">
                <div className="form__profile-image">
                  {!photo ? (
                    '' /*<div className = "ui_message">(800px x 800px)</div>*/
                  ) : (
                    <ResponsiveImage src={photo} alt="Foto de perfil" />
                  )}
                </div>
                <div className="container_icon">
                  <input
                    type="file"
                    name="file"
                    id="profile_picture"
                    style={{ display: 'none' }}
                    onChange={this.onImageFieldChange}
                  />
                  <label htmlFor="profile_picture" className="icon_oval-add">
                    Editar foto de perfil
                  </label>
                  <Tooltip message="Se recomienda una imagen de 800px x 800px en formatos .png, .jpg y .gif a un tamaño máximo de 8MB." />
                </div>
              </div>
            </fieldset>

            <div className="ui-detailed-block">
              <div className="ui-detailed-block__detail">
                <h3 className="heading">
                  Detalles de la Tienda
                  {/* <Tooltip message="" /> */}
                </h3>
                <div className="form__help">
                  <p className="detail">
                    Información que describe lo que haces, tus productos y tu
                    historia.
                  </p>
                </div>
              </div>
              <div className="ui-detailed-block__content">
                <Field
                  name="name"
                  maxLength={80}
                  component={InputWithCounter}
                  validate={[required]}
                  label="Nombre *"
                  className={`${
                    errorForm && errorsWordsList?.badWordsName.length > 0
                      ? ' errorForm'
                      : ''
                  } name`}
                />
                <ErrorFormDisplay
                  errorForm={errorForm}
                  type={errorsWordsList?.badWordsName}
                />
                <Field
                  name="slogan"
                  maxLength={70}
                  component={InputWithCounter}
                  label="T&iacute;tulo/Slogan de la tienda"
                  className={`${
                    errorForm && errorsWordsList?.badWordsSlogan.length > 0
                      ? ' errorForm'
                      : ''
                  } title`}
                />
                <ErrorFormDisplay
                  errorForm={errorForm}
                  type={errorsWordsList?.badWordsSlogan}
                />
                <Field
                  name="excerpt"
                  maxLength={200}
                  validate={[required]}
                  component={TextAreaWithCounter}
                  label="Descripci&oacute;n Corta *"
                  className={`${
                    errorForm && errorsWordsList?.badWordsExcerpt.length > 0
                      ? ' errorForm'
                      : ''
                  } resume`}
                />
                <ErrorFormDisplay
                  errorForm={errorForm}
                  type={errorsWordsList?.badWordsExcerpt}
                />
                <Field
                  validate={[]}
                  maxLength={100000}
                  name="description"
                  id="description"
                  component={TextAreaWithCounter}
                  label="Descripci&oacute;n Detallada"
                  className={`${
                    errorForm && errorsWordsList?.badWordsDescription.length > 0
                      ? ' errorForm'
                      : ''
                  } description`}
                />
                <ErrorFormDisplay
                  errorForm={errorForm}
                  type={errorsWordsList?.badWordsDescription}
                />
              </div>
            </div>

            <div className="ui-detailed-block">
              <div className="ui-detailed-block__detail">
                <h3 className="heading">
                  Información de Contacto
                  {/* <Tooltip message="" /> */}
                </h3>
                <div className="form__help">
                  <p className="detail">
                    Información que Canasta Rosa y tus clientes usarán para ponerse
                    en contacto contigo.
                  </p>
                </div>
              </div>
              <div className="ui-detailed-block__content">
                <Field
                  name="contact_mail"
                  maxLength={120}
                  id="mail__txt"
                  component={InputWithCounter}
                  validate={[required, email]}
                  normalize={normalizeToLowerCaseAndTrim}
                  label="Email *"
                  className="mail"
                />
                <Field
                  name="telephone"
                  id="phone__txt"
                  component={InputField}
                  normalize={normalizePhone}
                  validate={[lookForPhoneNumber]}
                  label="Télefono"
                  className="phone"
                />
                <Field
                  name="mobile"
                  id="mobile__txt"
                  component={InputField}
                  normalize={normalizePhone}
                  validate={[lookForPhoneNumber]}
                  label="Celular *"
                  className="mobile"
                />
                <Field
                  name="facebook"
                  maxLength={256}
                  id="facebook__txt"
                  component={SocialNetworkFieldInput}
                  normalizeValue={(e) =>
                    removeDomainProtocolFromURL(e.target.value, 'facebook.com')
                  }
                  label="https://facebook.com/"
                  className="facebook"
                />
                <Field
                  name="instagram"
                  maxLength={256}
                  id="instagram__txt"
                  component={SocialNetworkFieldInput}
                  normalizeValue={(e) =>
                    removeDomainProtocolFromURL(e.target.value, 'instagram.com')
                  }
                  label="https://instagram.com/"
                  className="instagram"
                />
                <Field
                  name="pinterest"
                  maxLength={256}
                  id="pinterest__txt"
                  component={SocialNetworkFieldInput}
                  normalizeValue={(e) =>
                    removeDomainProtocolFromURL(e.target.value, 'pinterest.com')
                  }
                  label="https://pinterest.com/"
                  className="pinterest"
                />
              </div>
            </div>

            <div className="form__data form__submit">
              <button
                className="c2a_square"
                disabled={this.props.pristine || this.props.submitting}
              >
                Guardar Cambios
              </button>
            </div>
          </form>

          <div className="ui-detailed-block form__data" />

          <StoreGalleryForm />
          <SectionsForm />

          <div className="ui-detailed-block shipping-schedules">
            <div className="ui-detailed-block__detail">
              <h3 className="heading">
                Detalles de Entregas y Pedidos
                {/* <Tooltip message="" /> */}
              </h3>
              <div className="form__help">
                <p className="detail">Especifica todos los detalles de entrega.</p>
              </div>
            </div>

            {shouldRenderSchedulesWindow ? (
              <div className="ui-detailed-block__content">
                <SchedulesManager
                  initialValues={currentSchedule}
                  onSubmit={this.onScheduleAdded}
                  onCancel={(e) => this.closeScheduleManager()}
                />
              </div>
            ) : (
              <div className="ui-detailed-block__content">
                <div className="store-schedules">
                  <div className="store-schedules__param schedules">
                    <div className="schedules_header">
                      <div className="param-info">
                        <strong className="name icon_store-schedules">
                          Horarios de la Tienda
                        </strong>
                        <p className="description">
                          Estos son los horarios y días laborales de tu tienda.
                        </p>
                      </div>
                      {schedules.length > 0 && (
                        <button
                          type="button"
                          onClick={(e) => this.openScheduleManager()}
                          className="shipping-schedules__add-button button-simple"
                        >
                          + Agregar Horarios
                        </button>
                      )}
                    </div>

                    <hr />

                    {schedules.length < 1 ? (
                      <button
                        type="button"
                        onClick={(e) => this.openScheduleManager()}
                        className="shipping-schedules__add-button button-square--white"
                      >
                        + Agregar Horarios
                      </button>
                    ) : (
                      <ol className="schedules-list">{this.renderTimeTable()}</ol>
                    )}
                  </div>
                </div>

                <hr />

                <div className="shipping-schedules">
                  <div className="param-info">
                    <strong className="name icon_store-shipping">
                      Horarios de recolección y entrega.
                    </strong>
                    <p className="description">
                      Para tu comodidad, Canasta Rosa tiene {pickupSchedules.length}{' '}
                      horarios disponibles para recolectar tus productos. Elige los
                      horarios que mejor se ajusten a tus necesidades.
                    </p>
                  </div>

                  <ul className="buttons_list">{this.renderPickupSchedules()}</ul>
                </div>
              </div>
            )}
          </div>

          {/* Vacaciones */}
          <div className="ui-detailed-block shipping-schedules">
            <div className="ui-detailed-block__detail">
              <h3 className="heading">
                Temporada de Vacaciones
                {/* <Tooltip message="" /> */}
              </h3>
              <div className="form__help">
                <p className="detail">
                  Todo tu catálogo se seguirá mostrando dentro de Canasta Rosa, pero
                  tus clientes no podrán realizar compras hasta que regreses de
                  vacaciones.
                </p>
              </div>
            </div>

            <div className="ui-detailed-block__content">
              <div className="store-schedules">
                <div className="store-schedules__param schedules">
                  <div className="schedules_header">
                    <div className="param-info">
                      <strong className="name icon_store-schedules">
                        Calendario de Vacaciones
                      </strong>
                      <p className="description">
                        Elige en el calendario los días que estarás de vacaciones.
                        <br />A tu regreso, no olvides dejar tiempo suficiente para
                        que puedas elaborar y entregar tus nuevas órdenes.
                      </p>
                    </div>
                    <hr />
                    {vacations.length > 0 && ( //si si hay alguna
                      <button
                        type="button"
                        onClick={(e) => this.openCalendar()}
                        className="shipping-schedules__add-button button-simple"
                      >
                        + Agregar Vacaciones
                      </button>
                    )}
                  </div>

                  {vacations.length < 1 ? ( //si no hay ninguna
                    <button
                      type="button"
                      onClick={(e) => this.openCalendar()}
                      className="shipping-schedules__add-button button-square--white"
                    >
                      + Agregar Vacaciones
                    </button>
                  ) : (
                    <ol className="vacations-list">{this.renderVacations()}</ol>
                  )}

                  {shouldRenderCalendar ? (
                    <Calendar
                      view="month"
                      locale="es"
                      value={this.state.initialDate}
                      className="calendar-component"
                      onChange={this.onCalendarDateChange}
                      selectRange={true}
                    />
                  ) : null}
                </div>
              </div>
            </div>
          </div>

          {/* Store Status Section (Closed / Paused Store) */}
          {isStoreActive && (
            <StoreStatus
              updateStoreStatus={this.props.updateStoreStatus}
              fetchMyStore={this.props.fetchMyStore}
              callTypeform={this.props.callTypeform}
            />
          )}
        </div>
      </section>
    );
  }
}

const scrollToInvalid = (errors) => {
  const invalidInput = Object.keys(errors)[0];
  if (invalidInput) {
    document.getElementsByName(invalidInput)[0].focus();
    window.scrollTo(
      0,
      window.pageYOffset +
        (document.getElementsByName(invalidInput)[0].getBoundingClientRect().top -
          document.querySelector('.storeApp__settings').offsetTop),
    );
  }
};

// Wrap component within reduxForm
StoreSettingsForm = reduxForm({
  form: 'myStoreSettings_form',
  enableReinitialize: true,
  keepDirtyOnReinitialize: true, // <---- Prevent 'dirty' fields to update
  onSubmitFail: (errors) => scrollToInvalid(errors),
})(StoreSettingsForm);

const selector = formValueSelector('myStoreSettings_form');

// Add Redux state and actions to component´s props
function mapStateToProps(state) {
  const { pickupSchedules, prohibitedWords } = state.app;
  const storeDetail = getMyStoreDetail(state);
  const formData = { ...storeDetail };
  const { vacations, store_status } = state.myStore;
  formData.location = storeDetail.location;
  formData.phone = storeDetail.telephone;
  formData.mobile = storeDetail.mobile;
  formData.instagram = storeDetail.link_instagram;
  formData.pinterest = storeDetail.link_pinterest;
  formData.facebook = storeDetail.link_facebook;
  const formValues = getFormValues('myStoreSettings_form')(state);

  return {
    pickupSchedules,
    formStatus: storeDetail.status,
    myStore: storeDetail,
    initialValues: formData,
    vacations,
    store_status,
    formValues,
    prohibitedWords,
  };
}
function mapDispatchToProps(dispatch) {
  const {
    updateMyStore,
    uploadProfilePhoto,
    addMyStoreVacations,
    getMyStoreVacations,
    deleteMyStoreVacations,
    updateMyStoreVacations,
    updateStoreStatus,
    fetchMyStore,
  } = myStoreActions;
  return bindActionCreators(
    {
      updateMyStore,
      uploadProfilePhoto,
      addMyStoreVacations,
      getMyStoreVacations,
      deleteMyStoreVacations,
      updateMyStoreVacations,
      openStatusWindow: statusWindowActions.open,
      updateStoreStatus,
      fetchMyStore,
      callTypeform: typeformActions.open,
    },
    dispatch,
  );
}

// Export Component
export default connect(mapStateToProps, mapDispatchToProps)(StoreSettingsForm);
