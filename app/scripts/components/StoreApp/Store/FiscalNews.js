import React from 'react';
import { Link } from 'react-router-dom';
import './FiscalNews.scss';

class FiscalNews extends React.Component {
  render() {
    return (
      <div className="container-fiscal-popup">
        <h4 className="addProductForm__title">ACCIÓN REQUERIDA</h4>
        <div className="container-fiscal-popup__msg">
          <img
            src="/images/crPro/icons/icon_workshops_active_pink.svg"
            alt="Eventos"
            className="fiscal-edit"
          />
          <br />
          Hemos implementado algunos cambios en la plataforma, por favor para
          continuar actualiza la información fiscal de tu tienda.
          <br />
        </div>
        <div className="container-fiscal-popup__options">
          <Link to="/my-store/settings/accounts" onClick={this.props.closeModalBox}>
            <button className="c2a_square">Ir ahora</button>
          </Link>
        </div>
      </div>
    );
  }
}
export default FiscalNews;
