import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Field, reduxForm, formValueSelector } from 'redux-form';
import PropTypes from 'prop-types';
import Sentry from '../../../Utils/Sentry';
import LittleLoader from '../../../components/LittleLoader/LittleLoader';

import { getAddress } from '../../../Reducers/users.reducer';
import {
  myStoreActions,
  statusWindowActions,
  modalBoxActions,
  userActions,
} from '../../../Actions';
import { InputField, TextArea } from '../../../Utils/forms/formComponents';
import { UIIcon } from '../../../Utils/UIIcon';
import AddressManager from '../../../Utils/AddressManager.jsx';
import AddressesList from '../../../Utils/AddressesList.jsx';

class StoreAddressForm extends React.Component {
  /*
   * Constructor Function
   * @param {object} props React Component Properties
   */
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
    };

    // Bind Scope to class methods
    this.openAddressWindow = this.openAddressWindow.bind(this);
    this.closeAddressWindow = this.closeAddressWindow.bind(this);
    this.onDeleteAddress = this.onDeleteAddress.bind(this);
    this.onAddAddress = this.onAddAddress.bind(this);
    this.onUpdateAddress = this.onUpdateAddress.bind(this);
    this.onAddressChange = this.onAddressChange.bind(this);
  }

  /*
   * ON ADD ADDRESS
   * Post a new User Address
   * @param {object} address : Address Info
   */
  onAddAddress(address) {
    // Add Address
    this.props.addAddress(address).then(() => this.props.fetchMyStore());

    // Close Modalbox
    this.props.closeModalBox();
  }

  /*
   * ON DELETE ADDRESS
   * Delete a User Address
   * @param {string} addressId : Address Id
   */
  onDeleteAddress(addressId) {
    this.props
      .deleteAddress(addressId)
      .then((response) => {
        this.props.openStatusWindow({
          type: 'success',
          message: 'La direccion se eliminó correctamente.',
        });
      })
      .catch((error) => {
        Sentry.captureException(error);
        this.props.openStatusWindow({
          type: 'error',
          message: 'No puedes eliminar la dirección seleccionada por defecto.',
        });
      });
  }

  /*
   * ON UPDATE ADDRESS
   * Update a User Address
   * @param {object} address : Address Info
   */
  onUpdateAddress(address) {
    this.props.closeModalBox();
    return this.props
      .updateAddress(address)
      .then((response) => {
        this.props.openStatusWindow({
          type: 'success',
          message: 'La direccion se actualizó correctamente.',
        });
      })
      .catch((error) => {
        this.props.openStatusWindow({
          type: 'error',
          message: 'Error. Por favor, intenta de nuevo.',
        });
      });
  }

  /*
   * ON ADDRESS CHANGE
   * Make selected value the default address
   * @param {object} e : Event Object
   * @param {object} address : Current field Address
   */
  onAddressChange(address) {
    // Update Default Pickup Address
    this.onUpdateAddress({
      uuid: address.uuid,
      pickup_address: true,
      //default_address: true
    });
  }

  async setDefault(address) {
    this.setState({ loading: true });
    // Update Default Pickup Address
    await this.onUpdateAddress({
      uuid: address.uuid,
      pickup_address: true,
      // default_address: true
    });
    window.location.reload();
    //this.onUpdateAddress(address);
  }

  /*
   * CLOSE ADDRESS WINDOW
   */
  closeAddressWindow() {
    this.props.closeModalBox();
  }

  /*
   * OPEN ADDRESS WINDOW
   * Opens modalbox to create a new term
   */
  openAddressWindow() {
    this.props.openModalBox(() => (
      <AddressManager onSubmit={this.onAddAddress} title="Nueva Dirección" />
    ));
  }

  /*
   * React Component Cycle Methods
   */
  makeDefault(address) {
    if (this.state.loading) {
      return (
        <div className="default-address">
          <div className="loadingdefault-address__button">
            <LittleLoader></LittleLoader>
          </div>
        </div>
      );
    } else {
      if (address.pickup_address == false) {
        return (
          <div
            onClick={(e) => {
              this.setDefault(address);
            }}
            className="default-address"
          >
            {' '}
            <div className="setdefault-address__button">
              Establecer como principal
            </div>
          </div>
        );
      } else {
        return (
          <div className="default-address">
            <div className="default-address__button">Dirección principal</div>
          </div>
        );
      }
    }
  }
  render() {
    return (
      <section className="storeApp__module storeApp_Addresses">
        <h3>Direcciones</h3>
        <p>
          Agrega la direcci&oacute;n de la sucursal o domicilio donde recolectaremos
          tus productos cuando se realice una compra.
        </p>

        <div className="form form--annotated">
          <form>
            <div className="ui-detailed-block__content">
              <div className="form__data form__data-actions">
                <button
                  type="button"
                  onClick={this.openAddressWindow}
                  className="button-square--gray button--add"
                >
                  Agregar nueva direcci&oacute;n
                </button>
              </div>

              {this.props.addresses
                ?.sort((a, b) => b.pickup_address - a.pickup_address)
                .map((address) => (
                  <div className="shipping__address" key={address.uuid}>
                    <Field
                      type="radio"
                      userType="seller"
                      address={address}
                      key={address.uuid}
                      value={address.uuid}
                      name="pickup_address"
                      component={AddressesList}
                      className="shipping__address"
                      changeAddress={this.onAddressChange}
                      id={`address_${address.uuid}`}
                      active={address.uuid === this.props.pickup_address}
                      updateAddress={this.onUpdateAddress}
                      deleteAddress={this.onDeleteAddress}
                    />
                    {this.makeDefault(address)}
                  </div>
                ))}
            </div>
          </form>
        </div>
      </section>
    );
  }
}

// Wrap component within reduxForm
StoreAddressForm = reduxForm({
  form: 'myStoreSettings_form',
  enableReinitialize: true,
  destroyOnUnmount: false,
})(StoreAddressForm);

// Add Redux state and actions to component´s props
const selector = formValueSelector('myStoreSettings_form');
function mapStateToProps(state) {
  const { users, form } = state;
  const defaultAddress = users.addresses.addresses.find((address) =>
    address.pickup_address ? address.uuid : false,
  );
  const selectedShippingAddress = selector(state, 'pickup_address');
  return {
    addresses: users.addresses.addresses.map((address) => getAddress(address)),
    initialValues: {
      pickup_address: defaultAddress ? defaultAddress.uuid : null,
    },
    pickup_address: selectedShippingAddress,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      openModalBox: modalBoxActions.open,
      closeModalBox: modalBoxActions.close,
      addAddress: userActions.addAddress,
      updateAddress: userActions.updateAddress,
      deleteAddress: userActions.deleteAddress,
      openStatusWindow: statusWindowActions.open,
      fetchMyStore: myStoreActions.fetchMyStore,
    },
    dispatch,
  );
}

// Export Component
StoreAddressForm = connect(mapStateToProps, mapDispatchToProps)(StoreAddressForm);
export default StoreAddressForm;
