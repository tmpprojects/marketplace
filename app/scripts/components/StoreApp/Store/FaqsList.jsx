import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { renderRoutes } from 'react-router-config';
import { bindActionCreators } from 'redux';
import { Field, FieldArray, reduxForm, SubmissionError } from 'redux-form';
import PropTypes from 'prop-types';

import config from '../../../../config';
import { InputField } from '../../../Utils/forms/formComponents';
import { myStoreActions, statusWindowActions } from '../../../Actions';
import { ResponsiveImage } from '../../../Utils/ImageComponents.jsx';
import { UIIcon } from '../../../Utils/UIIcon';

const Faq = ({ fields, item, updateFaq, deleteFaq }) => (
  <div>
    {fields.map((item, i) => {
      return (
        <div className="form__data-faqs" key={`${item}.id`}>
          <div className="faq">
            <Field
              id="question"
              name={`${item}.question`}
              className="question"
              onBlur={(e) =>
                updateFaq(fields.get(i).id, e.target.value, fields.get(i).excerpt)
              }
              component="input"
            />
            <div className="edit">
              <a href="#"></a>
            </div>
          </div>

          <div className="faq">
            <Field
              id="answer"
              name={`${item}.excerpt`}
              onBlur={(e) =>
                updateFaq(fields.get(i).id, fields.get(i).question, e.target.value)
              }
              component="textarea"
            />
            <div className="edit">
              <a href="#"></a>
            </div>
          </div>

          <div className="faqs__actions">
            <UIIcon
              icon="delete"
              type="error"
              className="delete button"
              onClick={() => deleteFaq(fields.get(i).id)}
            >
              Eliminar
            </UIIcon>
          </div>
        </div>
      );
    })}
  </div>
);
class FaqsList extends React.Component {
  /*
   * Constructor Function
   * @param {object} props React Component Properties
   */
  constructor(props) {
    super(props);

    // Bind Scope to class methods
    this.deleteFaq = this.deleteFaq.bind(this);
    this.updateFaq = this.updateFaq.bind(this);
  }

  /*
   * FORM FUNCTIONS
   */
  deleteFaq(faq_id) {
    return this.props
      .deleteFaq(faq_id)
      .then((response) => {
        this.props.openStatusWindow({
          type: 'success',
          message: 'Eliminamos el producto correctamente.',
        });
      })
      .catch((error) => {
        this.props.openStatusWindow({
          type: 'error',
          message: 'Error. Por favor, intenta de nuevo.',
        });
      });
  }
  updateFaq(faq_id, question, excerpt) {
    return this.props
      .updateFaq({ faq_id, question, excerpt })
      .then((response) => {
        this.props.openStatusWindow({
          type: 'success',
          message: 'La sección se actualizó correctamente.',
        });
      })
      .catch((error) => {
        this.props.openStatusWindow({
          type: 'error',
          message: 'Error. Por favor, intenta de nuevo.',
        });
      });
  }

  /*
   * React Component Life Cycle Functions
   */
  componentDidMount() {
    this.props.fetchMyStoreFaqs();
  }

  render() {
    const { handleSubmit, faqs = [] } = this.props;
    return (
      <FieldArray
        name="faqs"
        component={Faq}
        updateFaq={this.updateFaq}
        deleteFaq={this.deleteFaq}
      />
    );
  }
}

// Wrap component within reduxForm
FaqsList = reduxForm({
  form: 'myStoreFaqsList_form',
  enableReinitialize: true,
  destroyOnUnmount: false,
})(FaqsList);

// Add Redux state and actions to component´s props
function mapStateToProps({ myStore }) {
  return {
    faqs: myStore.faqs,
    initialValues: {
      faqs: myStore.faqs,
    },
  };
}

function mapDispatchToProps(dispatch) {
  const { fetchMyStoreFaqs, deleteFaq, updateFaq } = myStoreActions;
  return bindActionCreators(
    {
      fetchMyStoreFaqs,
      deleteFaq,
      updateFaq,
      openStatusWindow: statusWindowActions.open,
    },
    dispatch,
  );
}

// Export Component
export default connect(mapStateToProps, mapDispatchToProps)(FaqsList);
