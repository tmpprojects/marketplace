import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { renderRoutes } from 'react-router-config';
import { bindActionCreators } from 'redux';
import { Field, reduxForm, SubmissionError } from 'redux-form';
import PropTypes from 'prop-types';

import config from '../../../../config';
import {
  myStoreActions,
  statusWindowActions,
  modalBoxActions,
} from '../../../Actions';
import { InputField, TextArea } from '../../../Utils/forms/formComponents';
import { UIIcon } from '../../../Utils/UIIcon';
import FaqsManager from './FaqsManager.jsx';
import FaqsList from './FaqsList.jsx';

class StoreFaqsForm extends React.Component {
  /*
   * Constructor Function
   * @param {object} props React Component Properties
   */
  constructor(props) {
    super(props);
    this.state = {};

    // Bind Scope to class methods
    this.openFaqsWindow = this.openFaqsWindow.bind(this);
    this.closeFaqsWindow = this.closeFaqsWindow.bind(this);
  }

  /*
   * OPEN SECTIONS WINDOW
   * Opens modalbox to create a new term
   */
  openFaqsWindow() {
    this.props.openModalBox(() => (
      <FaqsManager closeWindow={this.closeFaqsWindow} />
    ));
  }
  closeFaqsWindow(id = null) {
    // Update form field on redux store
    if (id) {
      //this.props.change('id', faqId);
    }
    // Close Window
    this.props.closeModalBox();
  }

  componentDidMount() {}
  componentWillReceiveProps(nextProps) {}

  render() {
    const { faqs } = this.props;
    return (
      <section className="storeApp__module storeApp__settings">
        <h3>FAQs</h3>

        <div className="form form--boxed">
          <form>
            <fieldset>
              <div className="form__data form__data-actions">
                <button
                  type="button"
                  className="button-square--gray button--add"
                  onClick={this.openFaqsWindow}
                >
                  Agregar FAQ
                </button>
              </div>

              <FaqsList />
            </fieldset>
          </form>
        </div>
      </section>
    );
  }
}

// PropTypes Definition
StoreFaqsForm.propTypes = {
  //onSubmit: PropTypes.func.isRequired
};

// Wrap component within reduxForm
StoreFaqsForm = reduxForm({
  form: 'myStoreSettings_form',
  enableReinitialize: true,
  destroyOnUnmount: false,
})(StoreFaqsForm);

// Add Redux state and actions to component´s props
function mapStateToProps({ myStore }) {
  return {};
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      openModalBox: modalBoxActions.open,
      closeModalBox: modalBoxActions.close,
    },
    dispatch,
  );
}

// Export Component
StoreFaqsForm = connect(mapStateToProps, mapDispatchToProps)(StoreFaqsForm);
export default StoreFaqsForm;
