import React from 'react';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { SubmissionError } from 'redux-form';

import { myStoreActions, statusWindowActions } from '../../../Actions';
import { getUIErrorMessage } from '../../../Utils/errorUtils';
import { myStoreErrorTypes } from '../../../Constants/mystore.constants';
import {
  getBankDataOptions,
  getFiscalDataOptions,
} from '../../../Reducers/mystore.reducer';
import StoreFiscalDataForm from './StoreFiscalDataForm';
import StoreBankAccountForm from './StoreBankAccountForm';
import { trackWithGTM } from '../../../Utils/trackingUtils';

const mobile = require('./Alboassets/mobile.jpg');
const desktop = require('./Alboassets/desktop-tablet.jpg');

class StoreAccounts extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      shouldRenderBankForm: false,
      width: 0,
    };
    this.ref = React.createRef();
  }

  // onPaymentMethodDelete = provider => {
  //   if (
  //     confirm(
  //       '¿Estás seguro de querer eliminar este método de pago? Sin un método de pago válido, no podremos transferir el dinero de tus ventas.'
  //     )
  //   ) {
  //     this.props
  //       .deletePaymentMethod(provider)
  //       .then(() => this.props.fetchMyStore())
  //       .catch(() => this.props.fetchMyStore());
  //   }
  // };
  componentDidMount() {
    this.updateDimensions();
    window.addEventListener('resize', this.updateDimensions);
  }
  componentWillUnmount() {
    window.removeEventListener('resize', this.updateDimensions);
  }
  componentDidUpdate() {
    this.goToRFC();
  }
  goToRFC = () => {
    if (this.props.location.hash.includes('RFC')) {
      window.scrollTo(0, this.ref.current.offsetTop);
    }
  };

  onChangeFiscalRegistry = (registryType) => {
    this.props
      .updateMyStore({ fiscal_registry: registryType })
      .then((response) => this.props.fetchMyStore());
  };

  /**
   * onBankAccountAdd()
   * Adds a bank account to the system.
   * @param {object} accountDetails | Bank Account data
   */
  onBankAccountAdd = (accountDetails) => {
    // Send data to API
    this.props
      .addBankAccount(accountDetails)
      .then((response) => {
        this.onChangeFiscalRegistry('with-rfc');

        this.props.openStatusWindow({
          type: 'success',
          message: 'Información actualizada.',
        });
        this.setState({ shouldRenderBankForm: false });
      })
      .catch((error) => {
        // Display UI error messages
        const uiMessage =
          error.response.data.non_field_errors ||
          getUIErrorMessage(error.response, myStoreErrorTypes);
        console.log(
          myStoreErrorTypes.BANK_ACCOUNT_NUMBER_LENGTH.message,
          'respuesta',
        );
        this.props.openStatusWindow({
          type: 'error',
          message: uiMessage,
        });
        throw new SubmissionError({
          _error: uiMessage,
        });
      });
  };

  /**
   * onBankAccountRemove()
   * Removes a Bank Account
   * @param {object} accountId | Bank Account Object
   */

  // onBankAccountRemove = accountId => {
  //   if (
  //     confirm(
  //       '¿Estás seguro de querer eliminar este método de pago? Sin un método de pago válido, no podremos transferir el dinero de tus ventas.'
  //     )
  //   ) {
  //     // TODO: Here we should call the API requesting the account deletion
  //     // Is this implemented on the API yet?
  //     // this.props.deletePaymentMethod(provider)
  //     // .then(() => this.props.fetchMyStore())
  //     // .catch(() => this.props.fetchMyStore());
  //   }
  // };

  /**
   * onBankAccountCancelEdition()
   * Hides the bank account detail form
   */
  onBankAccountCancelEdition = (e) => {
    // Hide Bank Account detail window
    this.setState({ shouldRenderBankForm: false });
  };

  saveFiscalData = (formValues, action) => {
    if (
      formValues.hasOwnProperty('fiscalData') !== null &&
      formValues.fiscalData !== ''
    ) {
      let actionForm = '';
      if (action === 'create') {
        actionForm = this.props.addFiscalData(formValues);
      } else {
        actionForm = this.props.updateFiscalData(formValues);
      }

      return actionForm
        .then((response) => {
          if (response.hasOwnProperty('data')) {
            this.props.openStatusWindow({
              type: 'success',
              message: 'Información actualizada.',
            });
          }
          //If 400 error with message
          if (response.hasOwnProperty('rfc')) {
            const uiMessage = response.rfc[0];
            this.props.openStatusWindow({
              type: 'error',
              message: uiMessage,
            });
          }
        })
        .catch((error) => {
          // Display UI error messages
          const uiMessage =
            error.response.data.non_field_errors ||
            getUIErrorMessage(error.response, myStoreErrorTypes);
          this.props.openStatusWindow({
            type: 'error',
            message: uiMessage,
          });
          throw new SubmissionError({
            _error: uiMessage,
          });
        });
    }

    return false;
  };

  /**
   * showBankAccountResume
   * Renders a list of bank accounts resumes
   */
  showBankAccountResume = () => {
    const {
      myStore: { bank_account: bankAccount },
    } = this.props;
    return (
      <React.Fragment>
        <div className="form payment-provider__options bankAccount">
          <h5>Cuenta Bancaria</h5>
        </div>
        <div className="ui-detailed-block__content">
          {/*<div className="bank_account_add-button">
                <button type="button" className="button-simple"> + Agregar otra cuenta </button>
                    </div>*/}
          <ul className="bank_account_list">
            <li className="bank_account_list-item">
              <p>
                {' '}
                <span>Nombre Completo:</span> {bankAccount.name}
              </p>
              <p>
                {' '}
                <span>Banco:</span> {bankAccount.bank.display_name}
              </p>
              <p>
                {' '}
                <span>Cuenta de banco:</span>{' '}
                {bankAccount.account_number_type.display_name}
              </p>
              <p>
                {' '}
                <span>Número:</span> {bankAccount.account_number}
              </p>
              <div className="buttons_container">
                {/*                <button
                  type="button"
                  className="button-simple remove"
                  onClick={e => this.onBankAccountRemove(bankAccount)}
                >
                  Eliminar
                </button>
                */}
                <button
                  type="button"
                  className="button-simple"
                  onClick={(e) => this.setState({ shouldRenderBankForm: true })}
                >
                  Editar Cuenta
                </button>
              </div>
            </li>
          </ul>
        </div>
        <a
          href="https://app.adjust.com/lek5mmt"
          target="_blank"
          rel="noopener noreferrer"
          onClick={this.onClickGTMTracking}
        >
          <img
            src={this.state.width >= 768 ? desktop : mobile}
            alt="¡Hola Albo!"
            aria-label="Crea una cuenta en 5 minutos."
            loading="lazy"
            className="albo"
          />
        </a>
      </React.Fragment>
    );
  };
  onClickGTMTracking = () => {
    const data = {
      id: 'Albo banner',
      name: this.state.width >= 768 ? 'desktop' : 'mobile',
      position: 1,
    };
    trackWithGTM('eec.impressionClick', [data], 'Abrir cuenta Albo');
  };
  updateDimensions = () => {
    this.setState({ width: window.innerWidth });
  };

  // Define Components
  createAccount = () => {
    const {
      myStore: { bank_account: bankAccount },
    } = this.props;

    const { bankAccountOptions } = this.props;
    return (
      <div className="ui-detailed-block">
        <div className="ui-detailed-block__detail">
          <h3 className="heading">Proveedores de Pago</h3>
          <p className="detail">
            Canasta Rosa realiza el pago de tus ventas de manera quincenal a la
            cuenta que registres en esta sección. Para Canasta Rosa la satisfacción
            de nuestros clientes es prioridad. De esta manera, aseguramos la calidad
            garantizando el pago de todas las ordenes que entregues en buen estado.
          </p>
        </div>
        <div className="ui-detailed-block__content payment-provider__container">
          <div className="form payment-provider__options"></div>
          {bankAccount ? (
            this.showBankAccountResume()
          ) : (
            <React.Fragment>
              <StoreBankAccountForm
                initialValues={{
                  ...bankAccount,
                  bank: bankAccount ? bankAccount.bank.value : null,
                  account_number_type: bankAccount
                    ? bankAccount.account_number_type.value
                    : null,
                }}
                onSubmit={this.onBankAccountAdd}
                onCancel={this.onBankAccountCancelEdition}
                options={bankAccountOptions}
              />
            </React.Fragment>
          )}
        </div>
      </div>
    );
  };

  render() {
    const {
      myStore: { bank_account: bankAccount, ...myStore },
    } = this.props;
    const { invoiceOptions, bankAccountOptions, fiscalData } = this.props;

    const { shouldRenderBankForm } = this.state;

    // return if MyStore object is empty
    if (Object.keys(myStore).length === 0 && myStore.constructor === Object)
      return null;

    // Define component to render
    const renderComponent = this.createAccount();

    // Render Component
    return (
      <section className="storeApp__module storeApp__accountsPayment">
        <h3>M&eacute;todos de Pago</h3>
        {shouldRenderBankForm ? (
          <div className="form--annotated bank_account">
            <div className="ui-detailed-block">
              <div className="ui-detailed-block__detail">
                <h3 className="heading">Cuenta Bancaria</h3>
                <p className="detail">
                  Informaci&oacute;n de datos de tu cuenta bancaria para recibir los
                  pagos de tus ventas.
                </p>
              </div>
              <StoreBankAccountForm
                initialValues={{
                  ...bankAccount,
                  bank: bankAccount ? bankAccount.bank.value : null,
                  account_number_type: bankAccount
                    ? bankAccount.account_number_type.value
                    : null,
                }}
                onSubmit={this.onBankAccountAdd}
                onCancel={this.onBankAccountCancelEdition}
                options={bankAccountOptions}
                bankAccount={bankAccount}
              />
            </div>
          </div>
        ) : (
          <div className="form--annotated">{renderComponent}</div>
        )}

        <div className="form--annotated billing" ref={this.ref}>
          <div className="ui-detailed-block">
            <div className="ui-detailed-block__detail">
              <h3 className="heading">Datos de Facturaci&oacute;n</h3>
              <p className="detail">
                Canasta Rosa cumple con todas sus regulaciones fiscales, por eso,
                debemos presentar declaraciones sobre todas las ventas que realices
                dentro de la plataforma.
              </p>
              <p className="detail">
                Ingresa los datos fiscales de tu negocio para que podamos emitirte la
                facturación correspondiente a nuestras comisiones y realizar el
                cálculo a la retención de impuestos correspondiente. Las facturas se
                emitirán dentro de los primeros 5 días del mes posterior al
                facturado.
              </p>
            </div>

            <StoreFiscalDataForm
              onChangeFiscalRegistry={this.onChangeFiscalRegistry}
              //saveFiscalData={this.saveFiscalData}
              options={invoiceOptions}
            />
          </div>
        </div>
      </section>
    );
  }
}

// Add Redux state and actions to component´s props
function mapStateToProps(state) {
  const { myStore } = state;
  return {
    invoiceOptions: getFiscalDataOptions(state),
    bankAccountOptions: getBankDataOptions(state),
    fiscalData: myStore.data.fiscal_data,
  };
}

function mapDispatchToProps(dispatch) {
  const {
    fetchMyStore,
    //addFiscalData,
    //updateFiscalData,
    addBankAccount,
    updateMyStore,
  } = myStoreActions;
  return bindActionCreators(
    {
      fetchMyStore,
      updateMyStore,
      //addFiscalData,
      //updateFiscalData,
      addBankAccount,
      openStatusWindow: statusWindowActions.open,
    },
    dispatch,
  );
}

// Attach Router Props
StoreAccounts = withRouter(StoreAccounts);

// Export Component
export default connect(mapStateToProps, mapDispatchToProps)(StoreAccounts);
