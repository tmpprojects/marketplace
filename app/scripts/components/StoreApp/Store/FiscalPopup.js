import React from 'react';
import './FiscalPopup.scss';

class FiscalPopup extends React.Component {
  render() {
    return (
      <div className="container-fiscal-popup">
        <h4 className="addProductForm__title">CONFIRMACIÓN</h4>
        <div className="container-fiscal-popup__msg">
          Una vez guardado tu regimen, <strong>no vas a poderlo modificar.</strong>{' '}
          <br />
          ¿Estás seguro(a) de realizar este cambio?
        </div>
        <div className="container-fiscal-popup__options">
          <button
            className="c2a_square c2a_square-disabled"
            onClick={this.props.closeModalBox}
          >
            Cancelar
          </button>
          <button className="c2a_square">Sí, realizar el cambio</button>
        </div>
      </div>
    );
  }
}
export default FiscalPopup;
