import React from 'react';
import { Field, reduxForm, SubmissionError } from 'redux-form';
import PropTypes from 'prop-types';

import { modalBoxActions, myStoreActions } from '../../../Actions';
import { InputField, TextArea } from '../../../Utils/forms/formComponents';

/*------------------------------------------
SECTIONS MANAGER CLASS
------------------------------------------*/
class FaqsManager extends React.Component {
  // PropTypes / DefaultProps
  static defaultProps = {
    attribute_types: [],
    max_attributes: 5,
  };
  static propTypes = {
    attribute_types: PropTypes.array.isRequired,
    max_attributes: PropTypes.number.isRequired,
  };

  /*
   * Constructor Function
   * @param {object} props React Component Properties
   */
  constructor(props) {
    super(props);

    // Bind Scope to class methods
    this.onSubmit = this.onSubmit.bind(this);
    this.onCloseWindow = this.onCloseWindow.bind(this);
  }

  /*
   * On Finish Editing the manager
   * Propagates the state of the manager to the parent component
   */
  onSubmit(values) {
    const { addQuestion: question, addAnswer: excerpt } = values;
    this.props
      .dispatch(
        myStoreActions.addMyStoreFaqs({
          question,
          excerpt,
        }),
      )
      .then((response) => this.props.closeWindow(response.data.id))
      .catch((error) => this.props.closeWindow());
  }

  /*
   * On Close Manager
   * Close Attribute Manager
   */
  onCloseWindow() {
    this.props.closeWindow();
  }

  /*
   * React Component Life Cycle Functions
   */
  render() {
    const { handleSubmit } = this.props;

    return (
      <div className="modal-box-form storeApp-modal">
        <h4 className="storeApp-modal__title">Crea una nueva FAQ</h4>

        <form
          className="storeApp-modal__content modal-box-form__form"
          onSubmit={handleSubmit(this.onSubmit)}
        >
          <Field
            component={InputField}
            autoFocus
            id="addQuestion"
            name="addQuestion"
            label="Pregunta"
            className="sectionName"
          />
          <Field
            component={TextArea}
            autoFocus
            id="addAnswer"
            name="addAnswer"
            label="Respuesta"
            className="sectionName"
          />

          {
            // Save & Cancel Buttons
            <div className="storeApp-modal__navBottom">
              <button
                type="button"
                onClick={this.onCloseWindow}
                className="storeApp-modal__button storeApp-modal__button--cancel"
              >
                Cancelar
              </button>
              <button className="storeApp-modal__button">Guardar</button>
            </div>
          }
        </form>
      </div>
    );
  }
}

// Wrap component within reduxForm
FaqsManager = reduxForm({
  form: 'myStoreFaqs_form',
})(FaqsManager);

// Export Component
export default FaqsManager;
