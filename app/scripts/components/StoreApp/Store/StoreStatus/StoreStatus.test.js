import React from 'react';
import { Provider } from 'react-redux';
import Enzyme, { shallow } from 'enzyme';
import EnzymeAdapter from 'enzyme-adapter-react-16';
import configureStore from 'redux-mock-store';
import renderer from 'react-test-renderer';

import StoreStatus from './StoreStatus';

Enzyme.configure({ adapter: new EnzymeAdapter() });

const mockStore = configureStore([]);

describe('StoreStatus Component', () => {
  /**
   * Return ShallowWrapper containing node(s) with the given data-test value.
   * @param {ShallowWrapper} wrapper - Enzyme shallow wrapper to search within.
   * @param {string} val - Value of data-test attribute for search.
   * @returns {ShallowWrapper}
   */

  let store;
  let component;

  beforeEach((props = {}, state = null) => {
    store = mockStore({
      myState: 'sample text',
    });
    component = renderer.create(
      <Provider store={store}>
        <StoreStatus {...props} />
      </Provider>,
    );
  });

  test('StoreStatus connected to store and render Component', () => {
    expect(component).toBeDefined();
  });
  test('Should unmount correctly', () => {
    expect(component.unmount()).toBeTruthy;
  });
});
