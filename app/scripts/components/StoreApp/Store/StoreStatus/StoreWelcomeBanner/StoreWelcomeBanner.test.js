import React from 'react';
import Enzyme, { mount, shallow } from 'enzyme';
import EnzymeAdapter from 'enzyme-adapter-react-16';

import StoreWelcomeBanner from './StoreWelcomeBanner';

Enzyme.configure({ adapter: new EnzymeAdapter() });

describe('StoreWelcomeBanner Component', () => {
  /**
   * Return ShallowWrapper containing node(s) with the given data-test value.
   * @param {ShallowWrapper} wrapper - Enzyme shallow wrapper to search within.
   * @param {string} val - Value of data-test attribute for search.
   * @returns {ShallowWrapper}
   */

  const findByTestAttr = (wrapper, val) => wrapper.find(`[data-test="${val}"]`);

  const props = jest.fn();

  let wrapper;
  beforeEach(() => {
    wrapper = mount(<StoreWelcomeBanner props={props} />);
  });

  test('Should to be defined correctly', () => {
    expect(wrapper).toBeDefined();
  });
  test('Expect wrapper be different than null', () => {
    expect(wrapper).not.toBeNull();
  });
  test('Expect div wrapper length to be 0', () => {
    const wrapperDiv = findByTestAttr(wrapper, 'wrapper');
    expect(wrapperDiv.length).toBe(0);
  });
  test('should unmount correctly', () => {
    expect(wrapper.unmount()).toBeTruthy;
  });
});
