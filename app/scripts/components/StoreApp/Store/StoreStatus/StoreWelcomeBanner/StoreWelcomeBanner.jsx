import React, { useState, useEffect } from 'react';
import './_storeWelcomeBanner.scss';

export default function StoreWelcomeBanner(props) {
  const { isStoreActive } = props;

  const [showBanner, setShowBanner] = useState(false);

  useEffect(() => {
    //Check Local Storage Reactivation Item to show Welcome Banner
    const reactivationItem = window.sessionStorage.getItem('CRreactivationStore');

    if (reactivationItem) {
      setShowBanner(true);
    }
  });

  return (
    <>
      {showBanner && isStoreActive && (
        <div className="cr__welcomeBanner" data-test="wrapper">
          <div className="cr__welcomeBanner--container">
            <img
              src={require('../../../../../../images/storeApp/welcomeBanner--left.svg')}
              alt="Bienvenido"
              loading="lazy"
              className="cr__welcomeBanner--container--iconLeft"
            />
            <div className="cr__welcomeBanner--container--titles">
              <h5 className="cr__text--subtitle2 cr__textColor--colorMain300">
                ¡Bienvenido otra vez!
              </h5>
              <span className="cr__text--paragraph cr__textColor--colorDark300">
                Que gusto tenerte de vuelta
              </span>
            </div>
            <img
              src={require('../../../../../../images/storeApp/welcomeBanner--right.svg')}
              alt="Bienvenido"
              loading="lazy"
              className="cr__welcomeBanner--container--iconRight"
            />
          </div>
        </div>
      )}
    </>
  );
}
