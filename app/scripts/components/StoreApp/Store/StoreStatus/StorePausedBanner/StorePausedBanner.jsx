import React from 'react';
import ConfirmationModalBox from '../ModalBoxes/ConfirmationModalBox';
import './_storePausedBanner.scss';

export default function StorePausedBanner(props) {
  const {
    storeName,
    updateStoreStatus,
    fetchMyStore,
    openStatusWindow,
    openModalBox,
    closeModalBox,
  } = props;

  const setItemReactivationStore = () => {
    window.sessionStorage.setItem('CRreactivationStore', true);
  };

  const onActivateStore = () => {
    //Format for backend
    const activedStore = { store_status: 'active' };

    updateStoreStatus(activedStore)
      .then(() => {
        closeModalBox();
      })
      .then(() => {
        fetchMyStore();
      })
      .then(() => {
        setItemReactivationStore();
      })
      .then(() => {
        openStatusWindow({
          type: 'success',
          message: 'La tienda se activó correctamente',
        });
      })
      .catch(() => {
        openStatusWindow({
          type: 'error',
          message: 'Error. Por favor, intenta de nuevo.',
        });
        closeModalBox();
      });
  };

  const onConfirmModalBox = () => {
    openModalBox(() => (
      <ConfirmationModalBox
        onActivateStore={onActivateStore}
        closeModalBox={closeModalBox}
      />
    ));
  };

  return (
    <div className="cr__pausedStore" data-test="wrapper">
      <div className="cr__pausedStore--container">
        <div className="cr__pausedStore--img">
          <img
            src={require('../../../../../../images/storeApp/pausedStore.svg')}
            alt="Tienda Pausada"
            loading="lazy"
          />
        </div>
        <div className="cr__pausedStore--content">
          <div className="cr__pausedStore--content--text">
            <h5 className="cr__text--subtitle3 cr__textColor--colorDark300">
              La tienda "{storeName}" esta en PAUSA
            </h5>
            <h6
              className="cr__text--subtitle3 cr__textColor--colorDark300"
              data-test="titleh6"
            >
              Temporalmente desactivada
            </h6>
          </div>

          <div className="cr__pausedStore--content--button">
            <button onClick={() => onConfirmModalBox()} data-test="button">
              Volver abrir
            </button>
          </div>
        </div>
      </div>
    </div>
  );
}
