import React from 'react';
import Enzyme, { mount, shallow } from 'enzyme';
import EnzymeAdapter from 'enzyme-adapter-react-16';

import StorePausedBanner from './StorePausedBanner';

Enzyme.configure({ adapter: new EnzymeAdapter() });

describe('StorePausedBanner Component', () => {
  /**
   * Return ShallowWrapper containing node(s) with the given data-test value.
   * @param {ShallowWrapper} wrapper - Enzyme shallow wrapper to search within.
   * @param {string} val - Value of data-test attribute for search.
   * @returns {ShallowWrapper}
   */
  const findByTestAttr = (wrapper, val) => wrapper.find(`[data-test="${val}"]`);

  const props = jest.fn();

  const wrapper = shallow(<StorePausedBanner props={props} />);

  test('Should to be defined correctly', () => {
    expect(wrapper).toBeDefined();
  });
  test('Expect div wrapper length to be 1', () => {
    const wrapperDiv = findByTestAttr(wrapper, 'wrapper');
    expect(wrapperDiv.length).toBe(1);
  });
  test('Expect h5 title to render', () => {
    expect(wrapper.find('h5').equals(true));
  });
  test('Expect h6 title to render', () => {
    expect(wrapper.find('h6'));
  });
  test('Expect h6 element to render correct text', () => {
    const titleH6 = findByTestAttr(wrapper, 'titleh6');
    expect(titleH6.text().includes('Temporalmente desactivada')).toBe(true);
  });
  test('Expect button to render correct text', () => {
    const button = findByTestAttr(wrapper, 'button');
    expect(button.text().includes('Volver abrir')).toBe(true);
  });
  test('Should unmount correctly', () => {
    expect(wrapper.unmount()).toBeTruthy;
  });
});
