import React from 'react';
import Enzyme, { shallow, mount } from 'enzyme';
import EnzymeAdapter from 'enzyme-adapter-react-16';

import StoreInProgressBanner from './StoreInProgressBanner';

Enzyme.configure({ adapter: new EnzymeAdapter() });

const icon = require('../../../../../../images/icons/icon_warning--yellow.svg');

describe('StoreInProgressBanner Component', () => {
  /**
   * Return ShallowWrapper containing node(s) with the given data-test value.
   * @param {ShallowWrapper} wrapper - Enzyme shallow wrapper to search within.
   * @param {string} val - Value of data-test attribute for search.
   * @returns {ShallowWrapper}
   */

  const findByTestAttr = (wrapper, val) => wrapper.find(`[data-test="${val}"]`);

  const wrapper = shallow(<StoreInProgressBanner />);

  test('Should to be defined correctly', () => {
    expect(wrapper).toBeDefined();
  });
  test('Should render UI Wrapper', () => {
    const wrapperUI = findByTestAttr(wrapper, 'wrapperUI');
    expect(wrapperUI.length).toBe(1);
  });
  test('Should render the correct src image', () => {
    const image = findByTestAttr(wrapper, 'image');
    expect(image.find('img').props().src).toBe(icon);
  });
  test('Should render h5 tag', () => {
    expect(wrapper.find('h5'));
  });
  test('Should render the h5 text correctly', () => {
    const titleH5 = findByTestAttr(wrapper, 'titleH5');
    expect(titleH5.text().includes('Tu tienda esta en proceso de desactivarse'));
  });
  test('Should unmount correctly', () => {
    expect(wrapper.unmount()).toBeTruthy;
  });
});
