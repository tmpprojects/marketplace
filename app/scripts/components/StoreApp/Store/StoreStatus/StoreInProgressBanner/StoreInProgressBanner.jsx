import React from 'react';
import ConfirmationModalBox from '../ModalBoxes/ConfirmationModalBox';
import './_storeInProgressBanner.scss';

export default function StoreInProgressBanner(props) {
  const {
    updateStoreStatus,
    fetchMyStore,
    openStatusWindow,
    openModalBox,
    closeModalBox,
  } = props;

  const onActivateStore = () => {
    //Format for backend
    const activedStore = { store_status: 'active' };

    updateStoreStatus(activedStore)
      .then(() => {
        closeModalBox();
      })
      .then(() => {
        fetchMyStore();
      })
      .then(() => {
        openStatusWindow({
          type: 'success',
          message: 'La tienda se activó correctamente',
        });
      })
      .catch(() => {
        openStatusWindow({
          type: 'error',
          message: 'Error. Por favor, intenta de nuevo.',
        });
        closeModalBox();
      });
  };

  const onConfirmModalBox = () => {
    openModalBox(() => (
      <ConfirmationModalBox
        onActivateStore={onActivateStore}
        closeModalBox={closeModalBox}
      />
    ));
  };

  return (
    <div className="cr__storeInProgress" data-test="wrapperUI">
      <div className="cr__storeInProgress--container">
        <div className="cr__storeInProgress--icon">
          <img
            src={require('../../../../../../images/icons/icon_warning--yellow.svg')}
            alt="Tienda en Proceso de desactivarse"
            loading="lazy"
            data-test="image"
          />
          <h5
            className="cr__text--subtitle3 cr__textColor--colorDark300"
            data-test="titleH5"
          >
            Tu tienda esta en proceso de desactivarse
          </h5>
        </div>
        <div className="cr__storeInProgress--content">
          <span className="cr__textColor--colorDark300">
            Una vez las &oacute;rdenes y/o pagos pendientes sean completados
            recibir&aacute;s un correo notificando la desactivaci&oacute;n
            automática. O puedes volver abrir tu tienda{' '}
            <span
              className="cr__textColor--colorMain300 cr__storeInProgress--content--link"
              onClick={() => onConfirmModalBox()}
            >
              aquí
            </span>
          </span>
        </div>
      </div>
    </div>
  );
}
