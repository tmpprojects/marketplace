import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { modalBoxActions, statusWindowActions } from '../../../../Actions/';
import ModalBox from './ModalBoxes/ClosePauseModalBox';
import './storeStatus.scss';

function StoreStatus(props) {
  const {
    openModalBox,
    closeModalBox,
    updateStoreStatus,
    openStatusWindow,
    fetchMyStore,
    callTypeform,
  } = props;

  const openCloseStoreModalBox = () => {
    const content =
      'Al aceptar tu tienda no aparecerá en Canasta Rosa pero podrás volver a abrirla en cualquier momento.';

    openModalBox(() => (
      <ModalBox
        updateStoreStatus={updateStoreStatus}
        closeModalBox={closeModalBox}
        type={'CERRAR'}
        content={content}
        openStatusWindow={openStatusWindow}
        fetchMyStore={fetchMyStore}
        callTypeform={callTypeform}
      />
    ));
  };

  const openPauseStoreModalBox = () => {
    const content =
      'Al aceptar tu tienda no aparecerá en Canasta Rosa pero podrás volver a abrirla en cualquier momento.';

    openModalBox(() => (
      <ModalBox
        updateStoreStatus={updateStoreStatus}
        closeModalBox={closeModalBox}
        type={'PAUSAR'}
        content={content}
        openStatusWindow={openStatusWindow}
        fetchMyStore={fetchMyStore}
        callTypeform={callTypeform}
      />
    ));
  };

  return (
    <div className="ui-detailed-block shipping-schedules">
      <div className="ui-detailed-block__detail">
        <h3 className="heading">
          ESTADO DE MI TIENDA
          {/* <Tooltip message="" /> */}
        </h3>
        {/* <div className="form__help">
                <p className="detail">
                 
                </p>
              </div> */}
      </div>

      <div className="ui-detailed-block__content">
        <ul className="cr__storeStatus">
          <li className="cr__storeStatus-item">
            <div className="cr__storeStatus-item-text">
              <h5 className="cr__text--paragraph cr__textColor--colorDark300">
                Pausar tienda
              </h5>
              <p className="cr__text--caption cr__textColor--colorDark100">
                <span className="cr__textColor--colorDark100">
                  T&oacute;mate un descanso,
                </span>{' '}
                la configuraci&oacute;n de tu tienda permanecer&aacute; intacta.
              </p>
              <p className="cr__text--caption cr__textColor--colorDark100">
                <span className="cr__textColor--colorDark100">
                  Temporalmente no aparecer&aacute; tu tienda en Canasta Rosa.
                </span>{' '}
                Regresa a vender cuando tu lo decidas.
              </p>
            </div>
            <div className="cr__storeStatus-item-button">
              <button
                className="button-square--white"
                onClick={() => openPauseStoreModalBox()}
              >
                Pausar tienda
              </button>
            </div>
          </li>
          <li className="cr__storeStatus-item">
            <div className="cr__storeStatus-item-text">
              <h5 className="cr__text--paragraph cr__textColor--colorDark300">
                Cerrar tienda
              </h5>
              <p className="cr__text--caption cr__textColor--colorDark100">
                <span className="cr__textColor--colorDark100">
                  Tu tienda no aparecer&aacute; en Canasta Rosa
                </span>{' '}
                y quedar&aacute; desactivada por completo. Vuelve abrir tu tienda en
                cualquier momento.
              </p>
              <p className="cr__text--caption cr__textColor--colorDark100">
                <span className="cr__textColor--colorDark100">
                  Las entregas y pagos pendientes podr&aacute;s darle seguimiento
                </span>{' '}
                hasta ser completadas.
              </p>
            </div>
            <div className="cr__storeStatus-item-button">
              <button
                className="button-square--white"
                onClick={() => openCloseStoreModalBox()}
              >
                Cerrar tienda
              </button>
            </div>
          </li>
        </ul>
      </div>
    </div>
  );
}

function mapStateToProps(state) {
  return {};
}
function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      openModalBox: modalBoxActions.open,
      closeModalBox: modalBoxActions.close,
      openStatusWindow: statusWindowActions.open,
    },
    dispatch,
  );
}

// Export Component
export default connect(mapStateToProps, mapDispatchToProps)(StoreStatus);
