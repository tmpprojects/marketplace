import React from 'react';
import ConfirmationModalBox from '../ModalBoxes/ConfirmationModalBox';
import './_storeOff.scss';

export default function StoreOff(props) {
  const {
    storeName,
    updateStoreStatus,
    fetchMyStore,
    openStatusWindow,
    openModalBox,
    closeModalBox,
  } = props;

  const setItemReactivationStore = () => {
    window.sessionStorage.setItem('CRreactivationStore', true);
  };

  const onActivateStore = () => {
    //Format for backend
    const activedStore = { store_status: 'active' };

    updateStoreStatus(activedStore)
      .then(() => {
        closeModalBox();
      })
      .then(() => {
        fetchMyStore();
      })
      .then(() => {
        setItemReactivationStore();
      })
      .then(() => {
        openStatusWindow({
          type: 'success',
          message: 'La tienda se activó correctamente',
        });
      })
      .catch(() => {
        openStatusWindow({
          type: 'error',
          message: 'Error. Por favor, intenta de nuevo.',
        });
        closeModalBox();
      });
  };

  const onConfirmModalBox = () => {
    openModalBox(() => (
      <ConfirmationModalBox
        onActivateStore={onActivateStore}
        closeModalBox={closeModalBox}
      />
    ));
  };

  return (
    <div className="cr__storeOff" data-test="wrapperUI">
      <div className="cr__storeOff--textContainer">
        <div className="cr__storeOff--textContainer--title">
          <span className="cr__text--subtitle2 cr__textColor--colorDark300">
            La tienda "{storeName}" se encuentra inactiva
          </span>
        </div>
        <div className="cr__storeOff--textContainer--content">
          <p
            className="cr__text--paragraph cr__textColor--colorDark300"
            data-test="p"
          >
            Recuerda que puedes regresar en cualquier momento. Puedes activar tu
            tienda cuando quieras.
          </p>
          <button onClick={() => onConfirmModalBox()}>Volver abrir</button>
        </div>
      </div>

      <div className="cr__storeOff--img">
        <img
          src={require('../../../../../../images/storeApp/inactive_strore.svg')}
          alt="Tienda cerrada"
          loading="lazy"
          data-test="image"
        />
      </div>
    </div>
  );
}
