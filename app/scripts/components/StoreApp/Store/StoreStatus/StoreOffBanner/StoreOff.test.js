import React from 'react';
import Enzyme, { mount, shallow } from 'enzyme';
import EnzymeAdapter from 'enzyme-adapter-react-16';

import StoreOff from './StoreOff';

const srcImage = require('../../../../../../images/storeApp/inactive_strore.svg');

Enzyme.configure({ adapter: new EnzymeAdapter() });

describe('StoreOff Component', () => {
  /**
   * Return ShallowWrapper containing node(s) with the given data-test value.
   * @param {ShallowWrapper} wrapper - Enzyme shallow wrapper to search within.
   * @param {string} val - Value of data-test attribute for search.
   * @returns {ShallowWrapper}
   */

  const findByTestAttr = (wrapper, val) => wrapper.find(`[data-test="${val}"]`);

  let wrapper;
  wrapper = shallow(<StoreOff />);

  test('Should to be defined correctly', () => {
    expect(wrapper).toBeDefined();
  });
  test('Render wrapper UI', () => {
    const wrapperUI = findByTestAttr(wrapper, 'wrapperUI');
    expect(wrapperUI.length).toBe(1);
  });
  test('Expect h6 element to render correct text', () => {
    const paragraphText =
      'Recuerda que puedes regresar en cualquier momento. Puedes activar tu tienda cuando quieras.';
    const paragraph = findByTestAttr(wrapper, 'p');
    expect(paragraph.text().includes(paragraphText)).toBe(true);
  });
  test('Should render the correct source img', () => {
    const image = findByTestAttr(wrapper, 'image');
    expect(image.find('img').props().src).toBe(srcImage);
  });
  test('Should unmount correctly', () => {
    expect(wrapper.unmount()).toBeTruthy;
  });
});
