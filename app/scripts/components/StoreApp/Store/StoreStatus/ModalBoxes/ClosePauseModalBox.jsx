import React, { useState } from 'react';
import './_modalBox.scss';

export default function ModalBox(props) {
  const {
    closeModalBox,
    type,
    content,
    updateStoreStatus,
    openStatusWindow,
    fetchMyStore,
    callTypeform,
  } = props;

  const [disableButton, setDisableButton] = useState(false);

  // Check type of the modal
  const isPausedType = type === 'PAUSAR';

  // Variables for backend post
  const storeClosedBySeller = { store_status: 'deactivated_by_seller' };
  const storePausedBySeller = { store_status: 'paused_by_seller' };

  const onUpdateStoreStatus = () => {
    updateStoreStatus(isPausedType ? storePausedBySeller : storeClosedBySeller)
      .then(() => {
        fetchMyStore();
      })
      .then(() => {
        closeModalBox();
      })
      .then(() => {
        openStatusWindow({
          type: 'success',
          message: isPausedType
            ? 'La tienda se pausó correctamente'
            : 'La tienda se desactivó correctamente',
        });
      })
      .then(() => {
        callTypeform();
      })
      .catch((error) => {
        openStatusWindow({
          type: 'error',
          message: 'Error. Por favor, intenta de nuevo.',
        });
        closeModalBox();
      });
  };
  return (
    <div className="cr__modalBoxCloseStore" data-test="wrapperUI">
      <div className="cr__modalBoxCloseStore--title">
        <span className="cr__text--subtitle3 cr__textColor--colorDark300">
          ¿Podr&iacute;as confirmar que deseas {type} TU TIENDA?
        </span>
      </div>
      <div className="cr__modalBoxCloseStore--content">
        <span className="cr__text--paragraph cr__textColor--colorDark300">
          {content}
        </span>
      </div>
      <div className="cr__modalBoxCloseStore--buttons">
        <button
          className="cr__modalBoxCloseStore--buttons--accept"
          onClick={() => {
            onUpdateStoreStatus();
            setDisableButton(true);
          }}
          disabled={disableButton}
        >
          Aceptar
        </button>
        <button
          className="cr__modalBoxCloseStore--buttons--cancel"
          onClick={() => closeModalBox()}
        >
          Cancelar
        </button>
      </div>
    </div>
  );
}
