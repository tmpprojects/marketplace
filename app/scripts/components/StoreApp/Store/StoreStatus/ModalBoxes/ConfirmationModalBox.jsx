import React, { useState } from 'react';
import './_modalBox.scss';

export default function ConfirmationModalBox(props) {
  const { onActivateStore, closeModalBox } = props;

  const [disableButton, setDisableButton] = useState(false);

  return (
    <div className="cr__modalBoxCloseStore" data-test="wrapperUI">
      <div className="cr__modalBoxCloseStore--title">
        <span className="cr__text--subtitle3 cr__textColor--colorDark300">
          Conf&iacute;rmanos que deseas volver abrir tu tienda
        </span>
      </div>
      <div className="cr__modalBoxCloseStore--open">
        <span className="cr__text--paragraph cr__textColor--colorDark300">
          Regresa a Canasta Rosa y comienza a vender nuevamente
        </span>
      </div>
      <div className="cr__modalBoxCloseStore--buttons">
        <button
          className="cr__modalBoxCloseStore--buttons--accept"
          onClick={() => closeModalBox()}
        >
          Cancelar
        </button>
        <button
          className="cr__modalBoxCloseStore--buttons--cancel"
          onClick={() => {
            onActivateStore();
            setDisableButton(true);
          }}
          disabled={disableButton}
        >
          Aceptar
        </button>
      </div>
    </div>
  );
}
