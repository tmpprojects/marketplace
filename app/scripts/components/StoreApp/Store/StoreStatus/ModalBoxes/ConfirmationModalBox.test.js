import React from 'react';
import Enzyme, { shallow } from 'enzyme';
import EnzymeAdapter from 'enzyme-adapter-react-16';

import ConfirmationModalBox from './ConfirmationModalBox';

Enzyme.configure({ adapter: new EnzymeAdapter() });

describe('ConfirmationModalBox Component', () => {
  /**
   * Return ShallowWrapper containing node(s) with the given data-test value.
   * @param {ShallowWrapper} wrapper - Enzyme shallow wrapper to search within.
   * @param {string} val - Value of data-test attribute for search.
   * @returns {ShallowWrapper}
   */

  const findByTestAttr = (wrapper, val) => wrapper.find(`[data-test="${val}"]`);

  const props = jest.fn();

  const wrapper = shallow(<ConfirmationModalBox props={props} />);

  test('Should to be defined correctly', () => {
    expect(wrapper).toBeDefined();
  });
  test('Expect div wrapper length to be 1', () => {
    const wrapperDiv = findByTestAttr(wrapper, 'wrapperUI');
    expect(wrapperDiv.length).toBe(1);
  });
  test('Should unmount correctly', () => {
    expect(wrapper.unmount()).toBeTruthy;
  });
});
