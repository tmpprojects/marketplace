import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { SubmissionError } from 'redux-form';
import { arrayMove } from 'react-sortable-hoc';

import GalleryContainer from '../Utils/GalleryContainer';
import { CustomPreloader } from '../../../Utils/Preloaders';
import { myStoreActions, statusWindowActions } from '../../../Actions';
import { Tooltip } from '../../../Utils/Tooltip';

class StoreGalleryForm extends React.Component {
  // PropTypes / DefaultProps
  static propTypes = {
    store: PropTypes.object.isRequired,
    addImage: PropTypes.func.isRequired,
  };

  /*
   * Constructor Function
   * @param {object} props React Component Properties
   */
  constructor(props) {
    super(props);
    this.state = {
      photos: this.props.store.gallery,
    };
    // Bind Scope to class methods
    this.onFieldChange = this.onFieldChange.bind(this);
    this.onSortEnd = this.onSortEnd.bind(this);
    this.deleteImage = this.deleteImage.bind(this);
  }

  /*
   * React Component Life Cycle Functions
   */
  componentWillReceiveProps(nextProps) {
    if (this.props.store !== nextProps.store) {
      this.setState({
        photos: nextProps.store.gallery,
      });
    }
  }

  /**
   * onSortEnd()
   * Updates state to new gallery sorting order
   * and updates positions through API
   * @param {object} sortingProps | Image new sorting props
   */
  onSortEnd = (sortingProps) => {
    const { oldIndex, newIndex } = sortingProps;
    arrayMove(this.state.photos, oldIndex, newIndex).forEach((image, index) => {
      this.props.updateStorePhoto({
        ...image,
        order: index,
      });
    });
  };

  /*
   * FORM FUNCTIONS
   */
  onFieldChange(e) {
    e.preventDefault();

    // Verify File size (8MB)
    if (e.target.files[0].size / 1024 / 1024 > 8) {
      e.target.value = '';
      this.props.openStatusWindow({
        type: 'error',
        message: 'Por favor, elige una imagen menor a 8MB.',
      });
      return false;
    }

    // Get Image Field
    const formData = new FormData();
    formData.append('photo', e.target.files[0]);
    formData.append('slug', this.props.store.slug);
    formData.append('order', 0);
    this.props.addImage(formData);

    return;

    e.preventDefault();
    let uploadError = false;
    //const { target } = e;
    //console.log(e.target);
    //const files = Array.prototype.slice.call(target.files);

    files.forEach((file) => {
      //console.log(file)
      // Verify File size (8MB)
      if (file.size / 1024 / 1024 > 8) {
        target.value = '';
        uploadError = true;
      }
    });

    if (uploadError) {
      this.props.openStatusWindow({
        type: 'error',
        message: 'Por favor, elige una imagen menor a 8MB.',
      });
      return false;
    }

    // files.forEach(file => {
    //     // Get Image Field
    //     const formData = new FormData();
    //     formData.append('photo', '');
    //     formData.append('slug', this.props.store.slug);
    //     formData.append('order', 0);
    //     //this.props.addImage(formData);
    // });
  }

  /**
   * deleteImage()
   * Delete an image fromt the store Gallery
   * @param {object} formData | Image object
   */
  deleteImage(formData) {
    // Confirm Product Image removal
    if (
      confirm(
        '¿Estás seguro de querer eliminar esta imagen?\nLa acción no podrá deshacerse.',
      )
    ) {
      return this.props
        .deleteStorePhoto(formData)
        .then((response) => {
          this.props.openStatusWindow({
            type: 'success',
            message: 'Información actualizada.',
          });
        })
        .catch((error) => {
          this.props.openStatusWindow({
            type: 'error',
            message: 'Error. Por favor, intenta de nuevo.',
          });
          throw new SubmissionError({
            _error:
              'Ocurrió un problema al actualizar tu información. Por favor, intenta de nuevo.',
          });
        });
    }
  }

  /**
   * render()
   * Render component
   */
  render() {
    const { gallery: photos = [], loading } = this.props.store;
    const gallery = photos.map((item) => {
      return {
        id: item.id,
        src: item.photo,
        slug: this.props.store.slug,
        name: this.props.store.name,
      };
    }); //.slice().reverse();

    return (
      <div className="ui-detailed-block gallery_module">
        <div className="ui-detailed-block__detail">
          <h3 className="heading">
            Galer&iacute;a
            {/* <Tooltip message="" /> */}
          </h3>
          <div className="form__help">
            <p className="detail">
              Agrega hasta 6 fotografías para mostrar cada detalle del producto.{' '}
              <br />
              <strong>TIP: </strong> Formatos .png, .jpg y .gif a un tamaño máximo de
              8MB.
            </p>
          </div>
        </div>

        <div className="form--annotated ui-detailed-block__content ">
          <React.Fragment>
            {loading ? (
              <CustomPreloader>Cargando...</CustomPreloader>
            ) : (
              <form onSubmit={this.onFieldChange}>
                <fieldset>
                  <div className="form__data-fileUpload">
                    <input
                      multiple
                      type="file"
                      name="file"
                      id="gallery_picture"
                      className="upload_input"
                      accept="image/x-png,image/gif,image/jpeg,image/jpg"
                      onChange={this.onFieldChange}
                    />
                    <label htmlFor="gallery_picture">Agregar imagen</label>
                  </div>
                </fieldset>
              </form>
            )}

            <GalleryContainer
              items={gallery.slice(0, 6)}
              className="gallery"
              onSortEnd={this.onSortEnd}
              onImageDelete={this.deleteImage}
            />
          </React.Fragment>
        </div>
      </div>
    );
  }
}

// Add Redux state and actions to component´s props
function mapStateToProps({ myStore }) {
  return {
    store: myStore.data,
  };
}
function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      addImage: myStoreActions.addStorePhoto,
      deleteStorePhoto: myStoreActions.deleteStorePhoto,
      openStatusWindow: statusWindowActions.open,
      updateStorePhoto: myStoreActions.updateStorePhoto,
    },
    dispatch,
  );
}

// Export Component
StoreGalleryForm = connect(mapStateToProps, mapDispatchToProps)(StoreGalleryForm);
export default StoreGalleryForm;
