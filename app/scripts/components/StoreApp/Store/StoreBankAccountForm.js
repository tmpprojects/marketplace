import React, { Component } from 'react';
import { Field, reduxForm, formValueSelector } from 'redux-form';
import { InputField } from '../../../Utils/forms/formComponents';
import {
  required,
  number,
  charactersCLABE,
  charactersDebit,
  maxLength140,
} from '../../../Utils/forms/formValidators';
import { connect } from 'react-redux';
import { trackWithGTM } from '../../../Utils/trackingUtils';

// import { normalizeAccount } from '../../../Utils/forms/formNormalizers';

const mobile = require('./Alboassets/mobile.jpg');
const desktop = require('./Alboassets/desktop-tablet.jpg');

export class StoreBankAccountForm extends Component {
  state = {
    accountType: null,
    width: 0,
  };
  componentDidMount() {
    this.updateDimensions();
    window.addEventListener('resize', this.updateDimensions);
  }
  componentWillUnmount() {
    window.removeEventListener('resize', this.updateDimensions);
  }

  onClickGTMTracking = () => {
    const data = {
      id: 'Albo banner',
      name: this.state.width >= 768 ? 'desktop' : 'mobile',
      position: 1,
    };
    trackWithGTM('eec.impressionClick', [data], 'Abrir cuenta Albo');
  };
  updateDimensions = () => {
    this.setState({ width: window.innerWidth });
  };
  render() {
    const {
      handleSubmit,
      options,
      onCancel,
      bankAccount,
      submitting,
      pristine,
      accountNumberType,
    } = this.props;
    return (
      <div className="ui-detailed-block__content">
        <form className="form" onSubmit={handleSubmit}>
          <Field
            name="name"
            id="name"
            className="field"
            component={InputField}
            validate={[required, maxLength140]}
            label="Nombre Completo"
          />

          {/*
            <div className="form__data half-content">
                <Field
                    name="rfc"
                    id="rfc"
                    className="field"
                    component={InputField}
                    validate={required}
                    label="RFC *" />
            </div>
*/}

          <div className="form__data half-content">
            <div className="form__data-field select-banks">
              <label className="title" htmlFor="bank">
                Banco
              </label>
              <Field
                name="bank"
                id="bank"
                component="select"
                validate={required}
                // value={initialValues.bank}
              >
                <option value={null}>Elige un banco</option>
                {options.banks.map((bank) => (
                  <option key={bank.value} value={bank.value}>
                    {bank.display_name}
                  </option>
                ))}
              </Field>
            </div>

            <div className="form__data-field select-accounts">
              <label className="title" htmlFor="account_number_type">
                Tipo de cuenta
              </label>
              <Field
                id="account_number_type"
                name="account_number_type"
                component="select"
                validate={required}
              >
                <option value="Elige un tipo de cuenta">
                  Elige un tipo de cuenta
                </option>
                {options.type_account.map((type, index) => (
                  <option key={index} value={type.value}>
                    {type.display_name}
                  </option>
                ))}
              </Field>
            </div>
          </div>
          <Field
            name="account_number"
            id="account_number"
            component={InputField}
            label="Número"
            validate={[
              required,
              number,
              accountNumberType === '1' ? charactersCLABE : charactersDebit,
            ]}
          />
          <div className="form-button_container">
            <button type="button" className="button-simple" onClick={onCancel}>
              {!bankAccount ? null : 'Cancelar'}
            </button>
            <button
              className="c2a_square"
              disabled={pristine || submitting}
              type="submit"
            >
              Guardar
            </button>
          </div>
        </form>

        {/* <span className="note note-style">
          <img src="https://s3.us-east-2.amazonaws.com/canastarosa/temp/assets/info-ico.svg" alt="Importante" />
         <span>
            ¿No tienes cuenta de banco? Crea una desde tu celular en 5 minutos
            <a href="https://cuenca.com/" target="_blank" rel="noopener noreferrer" > aquí </a>
            y recibe tu tarjeta en tu domicilio.<br />
          </span>
        </span> */}
        <a
          href="https://app.adjust.com/lek5mmt"
          target="_blank"
          rel="noopener noreferrer"
          onClick={this.onClickGTMTracking}
        >
          <img
            src={this.state.width >= 768 ? desktop : mobile}
            alt="¡Hola Albo!"
            aria-label="Crea una cuenta en 5 minutos."
            loading="lazy"
            className="albo"
          />
        </a>
      </div>
    );
  }
}

// Wrap component within reduxForm
StoreBankAccountForm = reduxForm({
  form: 'StoreBankAccount_Form',
})(StoreBankAccountForm);

const selector = formValueSelector('StoreBankAccount_Form');

export default connect((state) => {
  const accountNumberType = selector(state, 'account_number_type');
  return {
    accountNumberType,
  };
})(StoreBankAccountForm);

// const normalizeAccount = value => {
//   const v = value.replace(/\s+/g, '').replace(/[^0-9]/gi, '');
//   const matches = v.match(/\d{4,18}/g);
//   const match = (matches && matches[0]) || '';
//   const parts = [];
//   for (let i = 0, len = match.length; i < len; i += 4) {
//     parts.push(match.substring(i, i + 4));
//   }
//   if (parts.length) {
//     return parts.join(' ');
//   }
//   return value;
// };
