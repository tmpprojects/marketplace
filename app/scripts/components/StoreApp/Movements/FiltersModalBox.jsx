import React, { useState } from 'react';
import Calendar from 'react-calendar';
import DayJs from 'dayjs';
import './filtersModalBox.scss';

/**
 * @const {array} listOfStatus List of all possible Order Status
 * @property {string} value Slug of the order status
 * @property {string} name Display name of the order status
 */
const listOfStatus: array = [
  {
    value: 'default',
    name: 'Selecciona un Status',
  },
  {
    value: 'order_in_payout_process',
    name: 'Orden en proceso de pago',
  },
  {
    value: 'order_cancelled',
    name: 'Orden cancelada',
  },
  {
    value: 'order_awaiting_payment',
    name: 'Orden esperando pago',
  },
  {
    value: 'order_paid',
    name: 'Orden pagada',
  },
  {
    value: 'partial_adjustment',
    name: 'Ajuste parcial',
  },
  {
    value: 'order_ready',
    name: 'Orden lista para pagar',
  },
  {
    value: 'order_payment_approved',
    name: 'Pago verificado',
  },
  {
    value: 'order_created',
    name: 'Orden creada',
  },
];
/**
 * Component that renders inside a Modal
 * box for selecting and applying filters
 * for Movements Component and API
 * @param {*} props
 */
export default function ModalBox(props) {
  /**
   * Set Visible or not the Calendar Component
   * @param {boolean} isCalendarShown
   * @function setCalendarShown
   */
  const [isCalendarShown, setCalendarShown] = useState(false);
  (isCalendarShown: boolean);
  /**
   * Assign new status
   * @param {string} status
   * @function setStatus
   */
  const [status, setStatus] = useState('default');
  (status: string);
  /**
   * Set new dates
   * @param {array} dates
   * @function setDates
   * @return {array | strings}
   */
  const [dates, setDates] = useState(null);
  (dates: array);
  /**
   * Default value for obtain dates (2) with react Calendar Library
   * @const {array} defaultDates
   */
  const defaultDates: array = [new Date(), new Date()];
  /**
   *It changes if the user calls @function onApplyFilters and @param status is null or it different of null
   *@param {boolean} statusError
   *@function setStatusError
   */
  const [statusError, setStatusError] = useState(false);
  (statusError: boolean);
  /**
   *It changes if the user calls @function onApplyFilters and @param dates has @const dates or its full with dates
   *@param {boolean} statusError
   *@function setStatusError
   */
  const [datesError, setDatesError] = useState(false);
  (datesError: boolean);
  /**
   * Validates that all needed data is fill, construct the query and call API
   * @function onApplyFilters
   */
  const onApplyFilters = () => {
    /**
     * Validates @param status and @param dates are
     * fill with the correct data otherwise call error
     * function @function setStatusError or @function setDatesError
     */
    if (status === 'default') return setStatusError(true);
    if (dates === null) return setDatesError(true);
    /**
     * 1. Checks that @param status and @param dates are full with correct data and
     * 2. @const foundStatus finds in @const listOfStatus the whole key of @param status
     * 3. Calls @function getValuesQueries as @props from Movements component pass @param date and @const foundStatus
     * 4. Calls @function getStoreMovements as @props from Movements component a Redux action to pass query as @param
     * 5. Calls @function closeModalBox as @props from Movements component a Redux action to close the component
     */
    if (dates !== null && status !== 'default') {
      const foundStatus = listOfStatus?.find((i) => i?.value === status);
      props.getValuesQueries({ dates, foundStatus });
      props.getStoreMovements(
        `?from=${DayJs(dates[0])?.format('YYYY-MM-DD')}&to=${DayJs(dates[1])?.format(
          'YYYY-MM-DD',
        )}&status=${status}`,
      );
      props.closeModalBox();
    }
  };
  /**
   * Set Array of dates (2) and closes calendar and if it was an error in display delete it
   * @function onChangeDate
   * @param {array} value
   */
  const onChangeDate = (value: array) => {
    setDates(value);
    setCalendarShown(false);
    if (datesError) return setDatesError(false);
  };
  /**
   * Selects the correct value of Select and displays it and if it was an erroe in display delete it
   * @function onChangeDate
   * @param {*} e
   */
  const onChangeStatus = (e: React.FormEvent<HTMLInputElement>) => {
    e.preventDefault();
    const { target } = e;
    setStatus(target.value);
    if (statusError) return setStatusError(false);
  };
  /**
   * Set default values on @param dates and @param status by calling @function setStatus and @function setDates
   * @function onSetDefaultValues
   */
  const onSetDefaultValues = () => {
    setDates(null);
    setStatus('default');
    setDatesError(false);
    setStatusError(false);
  };
  const { closeModalBox } = props;
  return (
    <div className="cr__filtersModalBox">
      <div className="cr__filtersModalBox-header">
        <span className="cr__text--subtitle2 cr__textColor--colorDark300 cr__filtersModalBox-header-title">
          Filtros
        </span>
        <span
          className="cr__text--subtitle2 cr__textColor--colorDark300 cr__filtersModalBox-header-close"
          onClick={() => closeModalBox()}
        >
          X
        </span>
      </div>

      <div className="cr__filtersModalBox-content">
        <div className="cr__filtersModalBox-selection">
          <span className=" cr__filtersModalBox-selection-title cr__text--subtitle3 cr__textColor--colorDark100">
            Status
          </span>
          <select
            onChange={onChangeStatus}
            value={status}
            className={`cr__text--paragraph cr__textColor--colorDark300 ${
              statusError ? 'borderError' : ''
            }`}
          >
            {listOfStatus?.map(({ value, name }) => (
              <option key={value} value={value}>
                {name}
              </option>
            ))}
          </select>
          {statusError && (
            <span className="cr__filtersModalBox-error cr__text--paragraph cr__textColor--colorRed300">
              Error. Selecciona un status.
            </span>
          )}
        </div>

        <div className="cr__filtersModalBox-dates">
          <h5 className="cr__filtersModalBox-dates-title cr__text--subtitle3 cr__textColor--colorDark100">
            Periodo
          </h5>
          <div
            className="cr__filtersModalBox-date"
            onClick={() => setCalendarShown(!isCalendarShown)}
          >
            <span
              className={`cr__filtersModalBox-dateTitle cr__text--paragraph cr__textColor--colorDark300 ${
                datesError ? 'borderError' : ''
              }`}
            >
              {dates !== null
                ? DayJs(dates[0])?.format('DD/MM/YYYY')
                : 'Selecciona una fecha'}
              {dates !== null && ` - ${DayJs(dates[1])?.format('DD/MM/YYYY')}`}
            </span>
            <img
              src={require('../../../../images/icons/icon_calendar.svg')}
              alt="Calendario"
            />
            {datesError && (
              <span className="cr__filtersModalBox-error cr__text--paragraph cr__textColor--colorRed300">
                Error. Selecciona una fecha.
              </span>
            )}
          </div>
          {isCalendarShown && (
            <div className="cr__filtersModalBox-calendarContainer">
              <Calendar
                onChange={onChangeDate}
                value={defaultDates}
                view="month"
                locale="es"
                className="cr__filtersModalBox-calendar"
                selectRange
              />
            </div>
          )}
        </div>
      </div>
      <div className="cr__filtersModalBox-buttons">
        <div
          className="cr__filtersModalBox-button  cr__text--subtitle3 cr__textColor--colorDark100"
          onClick={onSetDefaultValues}
        >
          Borrar
        </div>
        <div
          className="cr__filtersModalBox-button  cr__text--subtitle3 cr__textColor--colorMain300"
          onClick={onApplyFilters}
        >
          Aplicar Filtros
        </div>
      </div>
    </div>
  );
}
