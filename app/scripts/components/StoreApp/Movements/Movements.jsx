import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { withRouter } from 'react-router-dom';
import DayJs from 'dayjs';
import queryString from 'query-string';

import { myStoreActions, modalBoxActions } from '../../../Actions';
import { formatNumberToPrice } from '../../../Utils/normalizePrice';
import FiltersModalBox from './FiltersModalBox';
import PayoutsList from './PayoutsList';
import withPagination from '../../hocs/withPagination';
import { Tooltip } from '../../../Utils/Tooltip';
import { IconPreloader } from '../../../Utils/Preloaders';

class Movements extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      searchVal: '',
      payouts: [],
      valuesQueries: null,
      width: 1200,
      queryFilters: null,
    };
  }

  componentDidMount() {
    this.props.getStoreMovements();
    this.updateWidth();
    window.addEventListener('resize', this.updateWidth);
  }
  componentWillReceiveProps(nextProps) {
    if (this?.props?.movements !== nextProps?.movements) {
      this.setState({
        payouts: nextProps?.movements?.fiscal?.orders?.results,
      });
    }
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.updateWidth);
  }

  handleChange = (e) => {
    const { name, value } = e.target;

    const {
      fiscal: { orders: payouts },
    } = this.props.movements;

    const found = payouts.results.filter((e) => {
      return e.uid.toLowerCase().includes(value.toLowerCase());
    });
    this.setState({
      [name]: value,
      payouts: found,
    });
  };

  handleSubmit = (e) => {
    e.preventDefault();
  };
  /**
   * Calls @function openModalBox as @props a Redux Action it passes a @param FiltersModalBox
   * @function openFilters
   */
  openFilters = () => {
    this.props.openModalBox(() => (
      <FiltersModalBox
        closeModalBox={this.props.closeModalBox}
        getValuesQueries={this.getValuesQueries}
        getStoreMovements={this.props.getStoreMovements}
      />
    ));
  };
  /**
   * It sets new state of local @state valuesQueries
   * @function getValuesQueries
   * @param {object} values
   */
  getValuesQueries = (values) => {
    this.setState({ valuesQueries: values });
  };
  /**
   * 1. Evalutes which Key Value will remove from @state valuesQueries
   * 2. @const updatedState receives the new state without the key that was selected on previous step
   * 3. Update @state valuesQueries
   * 4. Evaluates if the key exist, to call api with the new state or call api with remaining key on @state valuesQueries
   * @function deleteKeyValuesQueries
   * @param {string} value
   */
  deleteKeyValuesQueries = (value) => {
    const { valuesQueries = {} } = this.state;

    if (value === 'dates') {
      const updatedState = {
        foundStatus: valuesQueries?.foundStatus,
      };
      this.setState({
        valuesQueries: updatedState,
      });
      if (valuesQueries?.foundStatus) {
        this.props.getStoreMovements(`?status=${valuesQueries?.foundStatus?.value}`);
        this.setState({
          queryFilters: `&status=${valuesQueries?.foundStatus?.value}`,
        });
      } else {
        this.props.getStoreMovements();
      }
    }

    if (value === 'foundStatus') {
      const updatedState = {
        dates: valuesQueries?.dates,
      };
      this.setState({
        valuesQueries: updatedState,
      });
      if (valuesQueries?.dates) {
        this.props.getStoreMovements(
          `?from=${DayJs(valuesQueries?.dates[0]).format('YYYY-MM-DD')}&to=${DayJs(
            valuesQueries?.dates[1],
          ).format('YYYY-MM-DD')}`,
        );
        this.setState({
          queryFilters: `&from=${DayJs(valuesQueries?.dates[0]).format(
            'YYYY-MM-DD',
          )}&to=${DayJs(valuesQueries?.dates[1]).format('YYYY-MM-DD')}`,
        });
      } else {
        this.props.getStoreMovements();
      }
    }
  };
  /**
   * It updates the innerWidth
   * @function updateWidth
   */
  updateWidth = () => {
    this.setState({ width: window.innerWidth });
  };

  render() {
    const { payouts = [], searchVal, valuesQueries = {}, width } = this.state;

    const {
      movements: { fiscal = {}, payouts_orders },
      movements,
    } = this.props;

    // if (!fiscal) return null;s

    return (
      <section className="storeApp__module movements">
        <div className="cr__payouts">
          <div className="cr__payouts-information">
            <span className="cr__textColor--colorDark300 cr__text--subtitle">
              Mis ventas
            </span>
            <span className="cr__textColor--colorDark300 cr__text--subtitle2">
              Ganancias: <span>{formatNumberToPrice(fiscal?.sales_net)}MXN</span>
            </span>
            <span className="cr__textColor--colorDark300 cr__text--subtitle2">
              Saldo pendiente: <span>{formatNumberToPrice(fiscal?.balance)}MXN</span>
            </span>
            <span className="cr__textColor--colorDark300 cr__text--subtitle3">
              Total de ventas: <span>{payouts_orders?.count?.totalMovements}</span>
            </span>
          </div>

          {width >= 768 ? (
            <div className="cr__payouts-filtersNSearch">
              <div className="cr__payouts-filtersNSearch-left">
                <div className="cr__payouts-search">
                  <form onSubmit={this.handleSubmit}>
                    <input
                      type="text"
                      name="searchVal"
                      value={searchVal}
                      placeholder="Buscar Orden"
                      onChange={this.handleChange}
                      className="cr__payouts-search-input"
                    />
                    <input className="cr__payouts-search-icon" />
                  </form>
                </div>
                <div className="cr__payouts-filterButton">
                  <a onClick={() => this.openFilters()}>
                    <img
                      src={require('../../../../images/icons/icon_filter2020.svg')}
                    />

                    <span className="cr__text--subtitle3 cr__textColor--colorDark300">
                      Filtros
                    </span>
                  </a>
                </div>
              </div>

              <div className="cr__payouts-filtersNSearch-right">
                <div className="cr__payouts-downloadXLSX">
                  <a
                    href={fiscal?.download?.xsls}
                    target="_blank"
                    rel="noopener noreferrer"
                    download
                  >
                    Descargar XLSX
                  </a>
                </div>
              </div>
            </div>
          ) : (
            <div className="cr__payouts-mobileContainer">
              <div className="cr__payouts-search">
                <form onSubmit={this.handleSubmit}>
                  <input
                    type="text"
                    name="searchVal"
                    value={searchVal}
                    placeholder="Buscar Orden"
                    onChange={this.handleChange}
                    className="cr__payouts-search-input"
                  />
                  <input className="cr__payouts-search-icon" />
                </form>
              </div>
              <div className="cr__payouts-filtersNSearch-mobile">
                <div className="cr__payouts-filterButton">
                  <a onClick={() => this.openFilters()}>
                    <img
                      src={require('../../../../images/icons/icon_filter2020.svg')}
                    />
                  </a>
                </div>
                <div className="cr__payouts-downloadXLSX">
                  <a
                    href={fiscal?.download?.xsls}
                    target="_blank"
                    rel="noopener noreferrer"
                    download
                  >
                    Descargar XLSX
                  </a>
                </div>
              </div>
            </div>
          )}
          <div
            className={`${
              valuesQueries?.dates || valuesQueries?.foundStatus
                ? 'cr__payouts-containerDates'
                : ''
            }`}
          >
            {valuesQueries?.dates && (
              <div className="cr__payouts-clearDate cr__text--subtitle3">
                <h6>
                  {DayJs(valuesQueries?.dates[0])?.format('DD/MM/YYYY') +
                    '-' +
                    DayJs(valuesQueries?.dates[1])?.format('DD/MM/YYYY')}
                </h6>
                <span onClick={() => this.deleteKeyValuesQueries('dates')}>X</span>
              </div>
            )}
            {valuesQueries?.foundStatus && (
              <div className="cr__payouts-clearStatus cr__text--subtitle3">
                <h6>{valuesQueries?.foundStatus?.name}</h6>
                <span onClick={() => this.deleteKeyValuesQueries('foundStatus')}>
                  X
                </span>
              </div>
            )}
          </div>
        </div>

        {!movements?.loading ? (
          <PayoutsList items={payouts || []} />
        ) : (
          <IconPreloader />
        )}
      </section>
    );
  }
}

// Add Redux state and actions to component´s props
function mapStateToProps(state) {
  const { myStore } = state;
  return {
    movements: myStore?.movements,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      getStoreMovements: myStoreActions.getStoreMovements,
      openModalBox: modalBoxActions.open,
      closeModalBox: modalBoxActions.close,
    },
    dispatch,
  );
}

Movements = withRouter(Movements);

// Export Component
export default connect(mapStateToProps, mapDispatchToProps)(Movements);
