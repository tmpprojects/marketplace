import React from 'react';
import Enzyme, { shallow, mount } from 'enzyme';
import EnzymeAdapter from 'enzyme-adapter-react-16';

import FiltersModalBox from './FiltersModalBox';

Enzyme.configure({ adapter: new EnzymeAdapter() });

describe('FiltersModalBox Component', () => {
  /**
   * Factory function to create a ShallowWrapper for the App component.
   * @function setup
   * @param {object} props - Component props specific to this setup.
   * @param {object} state - Initial state for setup.
   * @returns {ShallowWrapper}
   */
  const setup = (props = {}, state = null) => {
    const wrapper = shallow(<FiltersModalBox {...props} />);
    if (state) wrapper.setState(state);
    return wrapper;
  };
  /**
   * Return ShallowWrapper containing node(s) with the given data-test value.
   * @param {ShallowWrapper} wrapper - Enzyme shallow wrapper to search within.
   * @param {string} val - Value of data-test attribute for search.
   * @returns {ShallowWrapper}
   */
  const wrapper = setup();

  test('it should to be defined correctly', () => {
    expect(wrapper).toBeDefined();
  });
});
