import React from 'react';
import { Link } from 'react-router-dom';
import { formatDate } from '../../../Utils/dateUtils';

export default function PayoutsList({ items }) {
  return (
    <div className="transaction">
      <div className="transaction-list">
        <table className="transaction-list__table">
          <thead>
            {/* <Tooltip message="Precio total del producto." /> */}
            <tr className="transaction-list__header cr__text--paragraph cr__textColor--colorDark300">
              <th className="transaction-list__header-order">No. de Orden</th>
              <th className="transaction-list__header-date">Fecha del pedido</th>
              <th className="transaction-list__header-type">Tipo</th>
              <th className="transaction-list__header-reference">Referencia</th>
              <th className="transaction-list__header-status">Status</th>

              <th className="transaction-list__header-orderPrice">
                Precio de la orden
              </th>
              <th className="transaction-list__header-shipping">Costo de envío</th>
              <th className="transaction-list__header-shipping">Promociones</th>
              <th className="transaction-list__header-fullPrice">
                Orden + costo de envío
              </th>
              <th className="transaction-list__header-percentage">
                % Plan de tu tienda
              </th>
              <th className="transaction-list__header-comission">
                Comisión Canasta Rosa
              </th>
              <th className="transaction-list__header-iva">Retención I.V.A.</th>
              <th className="transaction-list__header-isr">Retención I.S.R.</th>
              <th className="transaction-list__header-isr">Total</th>
              <th className="transaction-list__header-total">Monto por cobrar</th>
            </tr>
          </thead>

          {items?.length === 0 ? (
            <tbody>
              <tr>
                <td colSpan="7" className="withoutMovements">
                  No se encontró ningún movimiento.
                </td>
              </tr>
            </tbody>
          ) : (
            <tbody>
              {items?.map(({ balance_orders, uid, payouts: paymentsInfo }) => (
                <tr
                  key={`payout-${uid}`}
                  className="transaction-list__item cr__text--paragraph cr__textColor--colorDark300"
                >
                  {balance_orders.map(
                    (
                      {
                        date_order,
                        shipment,
                        percentage_marketplace_fee,
                        commission,
                        order_amount,
                        total,
                        total_before_withhold,
                        total_to_pay,
                        iva,
                        it,
                        status,
                        discount_seller,
                      },
                      i,
                    ) => (
                      <React.Fragment key={i}>
                        <td>
                          <Link to={`/my-store/orders/${uid}`}>{uid}</Link>
                        </td>
                        <td>{formatDate(new Date(date_order))}</td>
                        <td>Orden</td>
                        <td>{paymentsInfo.map(({ reference }) => reference)}</td>
                        <td className={status.includes('pagada') ? 'paidOrder' : ''}>
                          {status}
                        </td>
                        <td>{total_before_withhold?.toFixed(2)}MXN</td>
                        <td>
                          {shipment > 0 ? `${shipment?.toFixed(2)}MXN` : 'N/A'}
                        </td>
                        <td>
                          {discount_seller > 0 ? `${discount_seller}MXN` : 'N/A'}
                        </td>
                        <td>{order_amount}MXN</td>
                        <td>{percentage_marketplace_fee * 100}%</td>
                        <td>{commission?.toFixed(2)}MXN</td>
                        <td>{iva ? `${iva}MXN` : 'N/A'}</td>
                        <td>{it ? `${it}MXN` : 'N/A'}</td>
                        <td>{total}MXN</td>
                        <td className={status.includes('pagada') ? 'paidOrder' : ''}>
                          {total_to_pay?.toFixed(2)}MXN
                        </td>
                      </React.Fragment>
                    ),
                  )}
                </tr>
              ))}
            </tbody>
          )}
        </table>
      </div>
    </div>
  );
}
