import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { renderRoutes } from 'react-router-config';
import Dashboard from './Dashboard/Dashboard';

class StoreDashboard extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {
    return <Dashboard />;
  }
}

function mapStateToProps(state) {
  return {};
}
function mapDispatchToProps(dispatch) {
  return bindActionCreators({}, dispatch);
}
export default connect(mapStateToProps, mapDispatchToProps)(StoreDashboard);
