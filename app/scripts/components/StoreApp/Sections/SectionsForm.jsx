import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { renderRoutes } from 'react-router-config';
import { bindActionCreators } from 'redux';
import { Field, reduxForm, SubmissionError } from 'redux-form';
import PropTypes from 'prop-types';

import config from '../../../../config';
import { myStoreActions, modalBoxActions } from '../../../Actions';
import { InputField, TextArea } from '../../../Utils/forms/formComponents';
import { ResponsiveImage } from '../../../Utils/ImageComponents.jsx';
import SectionsManager from '../Products/SectionsManager.jsx';
import SectionsList from './SectionsList';
import { Tooltip } from '../../../Utils/Tooltip';

class SectionsForm extends React.Component {
  // PropTypes / DefaultProps
  static defaultProps = {
    sections: [],
  };
  static propTypes = {
    sections: PropTypes.array.isRequired,
  };

  /*
   * Constructor Function
   * @param {object} props React Component Properties
   */
  constructor(props) {
    super(props);
    this.state = {};

    // Bind Scope to class methods
    this.openSectionsWindow = this.openSectionsWindow.bind(this);
    this.closeSectionsWindow = this.closeSectionsWindow.bind(this);
  }

  /*
   * OPEN SECTIONS WINDOW
   * Opens modalbox to create a new section
   */
  openSectionsWindow() {
    this.props.openModalBox(() => (
      <SectionsManager closeWindow={this.closeSectionsWindow} />
    ));
  }
  closeSectionsWindow(sectionSlug = null) {
    // Update form field on redux store
    if (sectionSlug) {
      this.props.change('section', sectionSlug);
    }
    // Close Window
    this.props.closeModalBox();
  }

  /*
   * React Component Life Cycle Functions
   */
  componentDidMount() {}
  componentWillMount() {}

  render() {
    //const { name, title, excerpt, description, photo, cover } = this.state;
    const { handleSubmit, sections, categories } = this.props;
    const { product_category, product_section } = this.state;

    return (
      <div className="ui-detailed-block sections">
        <div className="ui-detailed-block__detail">
          <h3 className="heading">
            Secciones en mi tienda
            {/* <Tooltip message="" /> */}
          </h3>
          <div className="form__help">
            <p className="detail">
              Agrupa artículos relacionados en tu tienda (Ej. Pasteles, Bebés, Día de
              las Madres).
            </p>
          </div>
        </div>
        <div className="form form--boxed ui-detailed-block__content">
          <form onSubmit={handleSubmit}>
            <div className="form__data form__data-sections">
              {sections.length <= 0 ? (
                <button
                  type="button"
                  onClick={this.openSectionsWindow}
                  className="sections__button button-square--white"
                >
                  + Agregar Secciones
                </button>
              ) : (
                <button
                  type="button"
                  onClick={this.openSectionsWindow}
                  className="sections__button button-simple button-simple--pink"
                >
                  + Agregar Secciones
                </button>
              )}
              <div className="sections">
                {sections.length > 0 && (
                  <Field
                    component="select"
                    className="sections__select"
                    name="section"
                  >
                    <option value={null}>Elige una Sección...</option>
                    {sections.map((item, index) => (
                      <option key={item.slug} value={item.slug}>
                        {item.name}
                      </option>
                    ))}
                  </Field>
                )}
              </div>
            </div>
          </form>
          <SectionsList />
        </div>
      </div>
    );
  }
}

// Wrap component within reduxForm
SectionsForm = reduxForm({
  form: 'myStoreEditProduct_form',
  enableReinitialize: true,
  destroyOnUnmount: false, // <------ preserve form data
  forceUnregisterOnUnmount: true, // <------ unregister fields on unmount
})(SectionsForm);

// Add Redux state and actions to component´s props
function mapStateToProps({ myStore }) {
  return {
    initialValues: {
      ...myStore.active_product,
    },
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      openModalBox: modalBoxActions.open,
      closeModalBox: modalBoxActions.close,
    },
    dispatch,
  );
}

// Export Component
SectionsForm = connect(mapStateToProps, mapDispatchToProps)(SectionsForm);
export default SectionsForm;
