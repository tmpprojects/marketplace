import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { renderRoutes } from 'react-router-config';
import { bindActionCreators } from 'redux';
import { Field, FieldArray, reduxForm, SubmissionError } from 'redux-form';
import PropTypes from 'prop-types';

import config from '../../../../config';
import { InputField } from '../../../Utils/forms/formComponents';
import { myStoreActions, statusWindowActions } from '../../../Actions';
import { ResponsiveImage } from '../../../Utils/ImageComponents.jsx';
import { UIIcon } from '../../../Utils/UIIcon';
import withCounter from '../../hocs/withCounter';

const InputWithCounter = withCounter(InputField);

const Section = ({ fields, item, updateSection, deleteSection }) => (
  <div>
    {fields.map((item, index) => (
      <div className="section__info" key={`${item}.slug`}>
        <Field
          name={`${item}.name`}
          maxLength={50}
          component={InputWithCounter}
          component="input"
          className="section"
          placeholder="Sección"
          onBlur={(e) => updateSection(fields.get(index).slug, e.target.value)}
        />

        <div className="section__actions">
          <div className="edit">
            <a href="#"></a>
          </div>
          <UIIcon
            icon="delete"
            type="error"
            className="delete button"
            onClick={() => deleteSection(fields.get(index).slug)}
          >
            Eliminar
          </UIIcon>
        </div>
      </div>
    ))}
  </div>
);

class SectionsList extends React.Component {
  /*
   * Constructor Function
   * @param {object} props React Component Properties
   */
  constructor(props) {
    super(props);

    // Bind Scope to class methods
    this.deleteSection = this.deleteSection.bind(this);
    this.updateSection = this.updateSection.bind(this);
  }

  /*
   * FORM FUNCTIONS
   */
  deleteSection(slug) {
    return this.props
      .deleteSection(slug)
      .then((response) => {
        this.props.openStatusWindow({
          type: 'success',
          message: 'La sección se eliminó correctamente.',
        });
      })
      .catch((error) => {
        this.props.openStatusWindow({
          type: 'error',
          message: 'Error. Por favor, intenta de nuevo.',
        });
      });
  }
  updateSection(slug, newName) {
    return this.props
      .updateSection({ slug, name: newName })
      .then((response) => {
        this.props.openStatusWindow({
          type: 'success',
          message: 'La sección se actualizó correctamente.',
        });
      })
      .catch((error) => {
        this.props.openStatusWindow({
          type: 'error',
          message: 'Error. Por favor, intenta de nuevo.',
        });
      });
  }

  /*
   * React Component Life Cycle Functions
   */
  render() {
    const { handleSubmit, sections = [] } = this.props;
    return (
      <div>
        <form className="section-list-container">
          <fieldset className="form sections-list">
            <FieldArray
              name="sections"
              className="section__info"
              component={Section}
              updateSection={this.updateSection}
              deleteSection={this.deleteSection}
            />
          </fieldset>
        </form>
      </div>
    );
  }
}

// Wrap component within reduxForm
SectionsList = reduxForm({
  form: 'myStoreSectionsList_form',
  enableReinitialize: true,
  destroyOnUnmount: false,
})(SectionsList);

// Add Redux state and actions to component´s props
function mapStateToProps({ myStore }) {
  return {
    sections: myStore.sections,
    initialValues: {
      sections: myStore.sections,
    },
  };
}

function mapDispatchToProps(dispatch) {
  const { deleteSection, updateSection, fetchProductList } = myStoreActions;
  return bindActionCreators(
    {
      fetchProductList,
      updateSection,
      deleteSection,
      openStatusWindow: statusWindowActions.open,
    },
    dispatch,
  );
}

// Export Component
export default connect(mapStateToProps, mapDispatchToProps)(SectionsList);
