import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Field, reduxForm, getFormValues } from 'redux-form';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

import { getProductDetailForm } from '../../../Reducers/mystore.reducer';
import { modalBoxActions, statusWindowActions } from '../../../Actions';
import SectionsManager from './SectionsManager.jsx';
import CheckboxListSelector from '../../../Utils/CheckboxListSelector';
import { Tooltip } from '../../../Utils/Tooltip';
import ProductGalleryForm from './ProductGalleryFormNew';
import ProductStockForm from './ProductStockForm';
import ProductShippingForm from './ProductShippingForm';
import { InputField, TextArea } from '../../../Utils/forms/formComponents';
import ModalBoxAddCategory from './ModalBoxAddCategory/ModalBoxAddCategory';
import ModalBoxAddInterests from './ModalBoxAddInterests/ModalBoxAddInterests';
import withCounter from '../../hocs/withCounter';
import { IconPreloader } from '../../../Utils/Preloaders';
import OutOfStock from './OutOfStock/OutOfStock';
import { UIIcon } from '../../../Utils/UIIcon.jsx';
import ErrorFormDisplay from '../Utils/ErrorFormDisplay/ErrorFormDisplay';

const CategorySelect = ({
  input,
  className = '',
  type,
  meta: { touched, error },
  children,
}) => {
  return (
    <div>
      <select multiple {...input} className={`${className}`}>
        {children}
      </select>
      {touched && error && <span className="form_status danger big">{error}</span>}
    </div>
  );
};

// FORM FIELDS FORMATTING
const TextAreaWithCounter = withCounter(TextArea);
const InputWithCounter = withCounter(InputField);

class ProductDetailsForm extends React.Component {
  // PropTypes / DefaultProps
  static defaultProps = {
    sections: [],
    categories: [],
  };
  static propTypes = {
    sections: PropTypes.array.isRequired,
    categories: PropTypes.array.isRequired,
  };

  /*
   * Constructor Function
   * @param {object} props React Component Properties
   */
  constructor(props) {
    super(props);
    this.state = {
      categories: props.initialValues
        ? [
            {
              name: props.initialValues.name,
              value: props.initialValues.value,
            },
          ]
        : [],
      submitDisabled: true,
      addStock: false,
      editingInterest: false,
    };
    this.ref = React.createRef();
  }

  componentDidUpdate() {
    if (this.state.addStock) {
      this.scrollToMyRef();
      this.setState({ addStock: false });
    }
  }
  /*
   * React Component Life Cycle Functions
   */
  componentWillReceiveProps(nextProps) {
    if (this.props.product !== nextProps.product) {
      this.forceUpdate();
    }
    if (this.props.sections !== nextProps.sections) {
      this.forceUpdate();
    }
    if (this.props?.productDetail.error !== nextProps?.productDetail.error) {
      window.location.href = '';
    }
  }

  /**
   * onCategoriesChange()
   * Updates Redux form when a category is added
   * @param {array} categories | Array of categories
   */
  onCategoriesChange = (categories) => {
    this.setState({
      categories,
    });
    this.props.change('category', categories);
  };

  /**
   * onSectionChange()
   * Updates Redux form when a the section of a product has changed
   * @param {string} section | Selected section slug
   */
  onSectionChange = (section) => {
    this.props.change('section', section);
  };

  /**
   * OPEN SECTIONS WINDOW
   * Opens modalbox to create a new section
   */
  openSectionsWindow = () => {
    this.props.openModalBox(() => (
      <SectionsManager closeWindow={this.closeSectionsWindow} />
    ));
  };

  /**
   * closeSectionsWindow()
   * Closes store´s sections modal box.
   * @param {string} sectionSlug
   */
  closeSectionsWindow = (sectionSlug = null) => {
    // Update form field on redux store
    if (sectionSlug) {
      this.props.change('section', sectionSlug);
    }
    // Close Window
    this.props.closeModalBox();
  };
  onAddStock = () => {
    this.setState({ addStock: true });
  };
  scrollToMyRef = () => {
    window.scrollTo(0, this.ref.current.offsetTop);
  };

  onRenderOutofStockBanner = () => {
    const { productDetail } = this.props;

    if (productDetail?.attributes?.length > 0) {
      const attributes = productDetail?.attributes?.some(
        (item) => item?.attribute_stock?.quantity <= 5,
      );
      return attributes;
    } else {
      const quantity = productDetail?.quantity <= 5;
      return quantity;
    }
  };
  onSubmitCategory = (value = [], type = '') => {
    const productValues = {
      ...this.props.formValues,
      new_category: value,
    };
    let confirmation;
    if (type === 'delete') {
      confirmation = confirm(
        '¿Estás seguro de querer eliminar la categoría?. La operación no podrá deshacerse.',
      );
    }
    if (confirmation || type === 'add') {
      this.props
        ?.onSubmit(productValues)
        .then((response) => {
          this.props.openStatusWindow({
            type: 'success',
            message:
              type === 'add'
                ? 'La categoría se agregó correctamente.'
                : 'La categoría se eliminó correctamente',
          });
        })
        .catch((error) => {
          this.props.openStatusWindow({
            type: 'error',
            message: 'Error. Por favor, intenta de nuevo.',
          });
        });
    }
  };

  onSubmitInterest = (interest) => {
    const { formValues } = this.props;

    const savedInterests = formValues?.interest?.map((item) => item?.slug);

    const productValues = {
      ...formValues,
      interest: [...savedInterests, interest],
    };

    this.props
      ?.onSubmit(productValues)
      .then((response) => {
        this.props.openStatusWindow({
          type: 'success',
          message: 'El interés se agregó correctamente.',
        });
      })
      .catch((error) => {
        this.props.openStatusWindow({
          type: 'error',
          message: 'Error. Por favor, intenta de nuevo.',
        });
      });
  };

  onDeleteInterest = (slug) => {
    const { formValues } = this.props;

    const confirmation = confirm(
      '¿Estás seguro de querer eliminar el interés?. La operación no podrá deshacerse.',
    );

    const filteredList =
      formValues?.interest?.length === 0
        ? []
        : formValues?.interest?.filter((item) => item.slug !== slug);

    const backendFormat = filteredList?.map((item) => item?.slug);

    const productValues = {
      ...formValues,
      interest: backendFormat,
    };

    if (confirmation) {
      this.props
        ?.onSubmit(productValues)
        .then((response) => {
          this.props.openStatusWindow({
            type: 'success',
            message: 'El interés se eliminó correctamente.',
          });
        })
        .catch((error) => {
          this.props.openStatusWindow({
            type: 'error',
            message: 'Error. Por favor, intenta de nuevo.',
          });
        });
    }
  };

  onEditInterest = (index, interest) => {
    const { formValues } = this.props;

    const format = formValues?.interest?.map((item) => item.slug);

    format.splice(index, 1, interest?.slug);

    const productValues = {
      ...formValues,
      interest: format,
    };

    this.props
      ?.onSubmit(productValues)
      .then((response) => {
        this.props.openStatusWindow({
          type: 'success',
          message: 'El interés se editó correctamente.',
        });
      })
      .catch((error) => {
        this.props.openStatusWindow({
          type: 'error',
          message: 'Error. Por favor, intenta de nuevo.',
        });
      })
      .finally(() => {
        this.onEditingState(false);
      });
  };

  onEditingState = (value = false) => {
    this.setState({
      editingInterest: value,
    });
  };

  createBreadcrumbsTree = (
    categories,
    targetCategorySlug = '',
    found = false,
    parentPath = [],
  ) => {
    const tester = categories.reduce(
      (a, b) => {
        let result = a;

        // If category is not found, proceed to recursive loop.
        if (!result.found) {
          // If we've found the matching category, return current result
          if (b.slug === targetCategorySlug) {
            result = {
              path: [...result.path, { name: b.name, slug: b.slug }],
              found: true,
            };

            // else, search whithin current category children.
          } else if (b.children.length) {
            const innerCategoryPath = this.createBreadcrumbsTree(
              b.children,
              targetCategorySlug,
              false,
              [...result.path, { name: b.name, slug: b.slug }],
            );

            if (innerCategoryPath.found) {
              result = innerCategoryPath;
            }
          }
        }
        // Return current path.
        return result;
      },
      { found, path: parentPath },
    );

    return tester;
  };

  openModalBoxAddCategory = () => {
    const breadcrumbsPath = this.createBreadcrumbsTree(
      this?.props?.categories,
      this?.props?.productDetail?.new_category[0]?.slug,
    ).path;

    this.props.openModalBox(() => (
      <ModalBoxAddCategory
        closeModalBox={this.props.closeModalBox}
        marketCategories={this.props.categories}
        initialValues={this.props.initialValues}
        onSubmitCategory={this.onSubmitCategory}
        createBreadcrumbsTree={this.createBreadcrumbsTree}
        breadcrumbsPathForm={breadcrumbsPath}
      />
    ));
  };
  openModalBoxAddInterests = (index) => {
    const breadcrumbsPath = this.createBreadcrumbsTree(
      this?.props?.interests,
      this?.props?.productDetail?.interest.length
        ? this?.props?.productDetail?.interest[index]?.slug
        : '',
    ).path;

    this.props.openModalBox(() => (
      <ModalBoxAddInterests
        closeModalBox={this.props.closeModalBox}
        marketInterests={this.props.interests}
        initialValues={this.props.initialValues}
        onSubmitInterest={this.onSubmitInterest}
        createBreadcrumbsTree={this.createBreadcrumbsTree}
        breadcrumbsPathForm={breadcrumbsPath}
        onEditInterest={this.onEditInterest}
        editingInterest={this.state.editingInterest}
        onEditingState={this.onEditingState}
        index={index}
      />
    ));
  };

  interestsPath = (interestSlug) => {
    const path = this.createBreadcrumbsTree(this?.props?.interests, interestSlug)
      .path;
    return (
      path?.length > 0 && (
        <ul>
          {path?.map(({ slug, name }, i) => (
            <li
              className={`cr__textColor--colorDark300 cr__text--paragraph ${
                i + 1 === path?.length ? 'bold' : ''
              }`}
              key={slug}
            >
              {' '}
              {name} {i + 1 !== path?.length && ' > '}
            </li>
          ))}
        </ul>
      )
    );
  };
  /**
   * render()
   * Renders component
   */
  render() {
    const {
      handleSubmit,
      sections,
      productDetail,
      myStore,
      errorsWordsList,
      errorForm,
    } = this.props;

    // if (myStore.loading || productDetail?.loading) return <IconPreloader />;

    const productSection = productDetail.section.length
      ? [
          {
            ...productDetail?.section[0],
            value: productDetail?.section[0]?.slug,
          },
        ]
      : [];

    const allSections = sections.map((section) => ({
      ...section,
      value: section.slug,
    }));

    // Find the National method on the array
    const getNacionalMethod = myStore?.available_shipping_methods_in_pickup_address?.find(
      (method) => method?.slug === 'standard',
    );
    // Save the permision key on variable
    const hasNationalPermission = getNacionalMethod?.store_has_obligation;

    // Categories Breadcrumb
    const breadcrumbsPathCategories = this.createBreadcrumbsTree(
      this?.props?.categories,
      this?.props?.productDetail?.new_category?.length
        ? this?.props?.productDetail?.new_category[0]?.slug
        : '',
    ).path;

    if (productDetail?.updating || productDetail?.loading) return <IconPreloader />;

    const isOufOfStock = this.onRenderOutofStockBanner();

    return (
      <section className="storeApp__module storeApp__products storeApp__main">
        <div className="container__links">
          <Link className="link_back" to="/my-store/products">
            Productos
          </Link>
          {productDetail.status && myStore.is_active.bool ? (
            <Link
              className="status_link"
              to={`/stores/${productDetail.store.slug}/products/${productDetail.slug}`}
            >
              Ver producto
            </Link>
          ) : (
            <span className="status_inactive">No visible</span>
          )}
        </div>

        {isOufOfStock && <OutOfStock onAddStock={this.onAddStock} />}

        <h3 className="title">{productDetail.name}</h3>

        <div className=" form form--annotated productDetail">
          <form onSubmit={handleSubmit} className="form">
            <fieldset>
              <div className=" ui-detailed-block">
                <div className="ui-detailed-block__detail">
                  <h3 className="cr__text--paragraph cr__textColor--colorDark300 heading2">
                    Detalle del Producto
                    <Tooltip message="Información que describe todo acerca de tu producto." />
                  </h3>
                  <div className="form__help">
                    <p className="cr__text--paragraph cr__textColor--colorDark100">
                      {' '}
                      El nombre y descripci&oacute;n de tu producto son los elementos
                      m&aacute;s importantes para que los clientes encuentren tus
                      productos. Elige palabras clave que describan claramente tu
                      producto. Estas ayudar&aacute;n a tener m&aacute;s relevancia
                      cuando los usuarios busquen productos relacionados.
                    </p>
                  </div>
                </div>

                <div className="ui-detailed-block__content">
                  <div className="form__data form__data-name">
                    <div className="field_details">
                      <label htmlFor="name_txt">Nombre</label>
                    </div>
                    <Field
                      id="name_txt"
                      name="name"
                      maxLength={50}
                      component={InputWithCounter}
                      className={
                        errorForm && errorsWordsList?.badWordsName.length > 0
                          ? ' errorForm'
                          : ''
                      }
                    />
                    <ErrorFormDisplay
                      errorForm={errorForm}
                      type={errorsWordsList?.badWordsName}
                    />
                  </div>

                  <div className="form__data form__data-detail">
                    <div className="field_details">
                      <label htmlFor="description">
                        Descripci&oacute;n Detallada
                      </label>

                      <div className="form__help">
                        <p className="cr__text--paragraph cr__textColor--colorDark100">
                          Cuéntale a tus compradores sobre sus características y
                          especificaciones.
                        </p>
                      </div>
                    </div>
                    <Field
                      validate={[]}
                      maxLength={10000}
                      id="description"
                      name="description"
                      className="description"
                      component={TextAreaWithCounter}
                      className={
                        errorForm && errorsWordsList?.badWordsDescription.length > 0
                          ? ' errorForm'
                          : ''
                      }
                    />
                    <ErrorFormDisplay
                      errorForm={errorForm}
                      type={errorsWordsList?.badWordsDescription}
                    />
                  </div>
                </div>
              </div>

              <ProductGalleryForm {...this.props} />
              <div ref={this.ref}>
                <ProductStockForm {...this.props} />
              </div>

              <div className=" ui-detailed-block">
                <div className="ui-detailed-block__detail">
                  <h3 className="cr__text--paragraph cr__textColor--colorDark300 heading2">
                    Clasificación del Producto
                  </h3>
                  <div className="form__help">
                    <p className="cr__text--paragraph cr__textColor--colorDark100">
                      Ayuda a tus compradores a encontrar más rápido tus productos
                      asignándoles una categoría. Al categorizar adecuadamente tus
                      productos, obtienen mayor relevancia y tus clientes descubrirán
                      tu tienda con mayor facilidad.
                    </p>
                  </div>
                </div>

                <div className="ui-detailed-block__content">
                  {this?.props?.productDetail?.updating ? (
                    <IconPreloader />
                  ) : (
                    <>
                      <div className="form__data form__data-categories">
                        <div className="field_details">
                          <label
                            htmlFor="detail_txt"
                            className="cr__text--subtitle3 cr__textColor--colorDark300 sectionTitle"
                          >
                            Categorías
                            {/* <Tooltip message="" /> */}
                          </label>
                          <div className="form__help detail">
                            <p className="cr__text--paragraph cr__textColor--colorDark100">
                              Asigna la categor&iacute;a que mejor describa tu
                              producto. Esta será la categor&iacute;a de tu producto
                              dentro de Canasta Rosa.
                            </p>
                          </div>
                        </div>

                        <div className="sections cr__addCategory">
                          <div className="cr__addCategory--add">
                            {!this?.props?.productDetail?.new_category?.length >
                            0 ? (
                              <button
                                type="button"
                                onClick={this.openModalBoxAddCategory}
                                className="cr__text--caption cr__textColor--colorDark100 sections__button button-square--grey"
                              >
                                + Agregar categor&iacute;a
                              </button>
                            ) : (
                              <div className="cr__addCategory--buttons borderTop">
                                <div className="cr__addCategory--buttons--tree">
                                  {breadcrumbsPathCategories?.length > 0 && (
                                    <ul>
                                      {breadcrumbsPathCategories?.map(
                                        ({ slug, name }, i) => (
                                          <li
                                            key={slug}
                                            className={`cr__textColor--colorDark300 cr__text--paragraph ${
                                              i + 1 ===
                                              breadcrumbsPathCategories?.length
                                                ? 'bold'
                                                : ''
                                            }`}
                                          >
                                            {' '}
                                            {name}{' '}
                                            {i + 1 !==
                                              breadcrumbsPathCategories?.length &&
                                              ' > '}
                                          </li>
                                        ),
                                      )}
                                    </ul>
                                  )}
                                </div>

                                <div className="cr__addCategory--buttons--icons">
                                  <div className="edit">
                                    <a onClick={this.openModalBoxAddCategory} />
                                  </div>
                                  <div className="cr__addCategory--buttons--icons--uiicon">
                                    <UIIcon
                                      icon="delete"
                                      type="error"
                                      onClick={() => {
                                        this.onSubmitCategory([], 'delete');
                                      }}
                                    />
                                  </div>
                                </div>
                              </div>
                            )}
                          </div>
                        </div>
                      </div>

                      <div className="form__data form__data-categories">
                        <div className="field_details">
                          <label
                            htmlFor="detail_txt"
                            className="cr__text--subtitle3 cr__textColor--colorDark300 sectionTitle"
                          >
                            Intereses
                            {/* <Tooltip message="" /> */}
                          </label>
                          <div className="form__help detail">
                            <p className="cr__text--paragraph cr__textColor--colorDark100">
                              Los intereses son una manera m&aacute;s amigable de
                              clasificar tus productos. Categorizando por
                              inter&eacute;s ayudas a tus clientes a encontrar tus
                              productos conforme a su estilo de vida dentro de
                              Canasta Rosa.
                            </p>
                          </div>
                        </div>

                        <div className="sections cr__addInterest">
                          <div className="cr__addInterest--add">
                            {this?.props?.productDetail?.interest?.length < 3 && (
                              <button
                                type="button"
                                onClick={() => {
                                  this.openModalBoxAddInterests();
                                  this.onEditingState(false);
                                }}
                                className="cr__text--caption cr__textColor--colorDark100 sections__button button-square--grey"
                              >
                                + Agregar inter&eacute;s
                              </button>
                            )}
                            {this?.props?.productDetail?.interest?.length > 0 && (
                              <>
                                {this?.props?.productDetail?.interest
                                  ?.slice(0, 3)
                                  ?.map((item, i) => (
                                    <div
                                      key={i}
                                      className="cr__addInterest--buttons borderTop"
                                    >
                                      <div className="cr__addInterest--buttons--tree">
                                        {this.interestsPath(item?.slug)}
                                      </div>

                                      <div className="cr__addInterest--buttons--icons">
                                        <div className="edit">
                                          <a
                                            onClick={() => {
                                              this.openModalBoxAddInterests(i);
                                              this.onEditingState(true);
                                            }}
                                          />
                                        </div>
                                        <div className="cr__addInterest--buttons--icons--uiicon">
                                          <UIIcon
                                            icon="delete"
                                            type="error"
                                            onClick={() => {
                                              this.onDeleteInterest(item?.slug);
                                            }}
                                          />
                                        </div>
                                      </div>
                                    </div>
                                  ))}
                              </>
                            )}
                          </div>
                        </div>
                      </div>
                    </>
                  )}

                  <div className="form__data form__data-sections">
                    <div className="field_details">
                      <label
                        htmlFor="detail_txt"
                        className="cr__text--subtitle3 cr__textColor--colorDark300 sectionTitle"
                      >
                        Secci&oacute;nes en mi tienda
                      </label>

                      <div className="form__help detail">
                        <p className="cr__text--paragraph cr__textColor--colorDark100">
                          Agrupa artículos relacionados en tu tienda (Ej. Pasteles,
                          Bebés, Día de las Madres).
                        </p>
                      </div>
                    </div>

                    <div className="sections">
                      {sections?.length > 0 && (
                        <CheckboxListSelector
                          multipleChoice={false}
                          options={allSections}
                          title="Elige una Sección..."
                          initialValues={productSection}
                          onSubmit={this.onSectionChange}
                        />
                      )}
                      <button
                        type="button"
                        onClick={this.openSectionsWindow}
                        className="cr__text--caption cr__textColor--colorDark100 sections__button button-square--grey"
                      >
                        + Agregar Secci&oacute;n
                      </button>
                    </div>
                  </div>
                </div>
              </div>
              <ProductShippingForm myStore={myStore} />

              {!hasNationalPermission && (
                <div className=" ui-detailed-block">
                  <div className="ui-detailed-block__detail">
                    <h3 className="cr__text--paragraph cr__textColor--colorDark300 heading2">
                      Convierte tu producto en nacional
                      {/* <Tooltip message='' /> */}
                    </h3>
                    {/* <div className="form__help">
                                        <p>Completa el siguiente formulario para volver tu producto nacional.</p>
                                    </div> */}
                  </div>
                  <div className="ui-detailed-block__content">
                    <div className="form__data form__data-categories">
                      <div className="field_details">
                        <p htmlFor="detail_txt">
                          Completa el siguiente formulario para volver tu producto
                          nacional.
                          {/* <Tooltip message='' /> */}
                        </p>
                        <div className="form__help cr__googleforms">
                          <div>
                            <a
                              href="https://docs.google.com/forms/d/e/1FAIpQLSeXqaOU3KcD0I7Og1DVvJasx1g4YqEgwHlbrSfx1sBOLIsbOA/viewform"
                              target="_blank"
                              rel="noopener noreferrer"
                              className="cr__text--subtitle3"
                            >
                              Ir al formulario
                            </a>
                          </div>
                        </div>
                      </div>
                      <div className="sections" />
                    </div>
                  </div>
                </div>
              )}
            </fieldset>
          </form>
        </div>
      </section>
    );
  }
}

// Wrap component within reduxForm
ProductDetailsForm = reduxForm({
  form: 'myStoreEditProduct_form',
  destroyOnUnmount: false, // <---- Preserve data. Dont destroy form on unmount
  enableReinitialize: true, // <---- Allow updates when 'initValues' prop change
  keepDirtyOnReinitialize: true, // <---- Prevent 'dirty' fields to update
  updateUnregisteredFields: true, // <---- Update unregistered fields.
  forceUnregisterOnUnmount: true, // <---- Unregister fields on unmount,
  validate: (formValues) => {
    const errors = {};
    if (!formValues.category || formValues.category === '') {
      errors.category = 'Es necesario que eligas una categoría.';
      errors._error = 'Es necesario que eligas una categoría.';
    }
    return errors;
  },
})(ProductDetailsForm);

// Add Redux state and actions to component´s props
function mapStateToProps(state) {
  const productDetail = getProductDetailForm(state);

  const formValues = getFormValues('myStoreEditProduct_form')(state);

  return {
    productDetail,
    initialValues: productDetail,
    formValues: formValues,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      openModalBox: modalBoxActions.open,
      closeModalBox: modalBoxActions.close,
      openStatusWindow: statusWindowActions.open,
    },
    dispatch,
  );
}

// Export Component
ProductDetailsForm = connect(
  mapStateToProps,
  mapDispatchToProps,
)(ProductDetailsForm);
export default ProductDetailsForm;
