import React from 'react';
import './outOfStock.scss';

export default function OutOfStock(props) {
  return (
    <div className="cr__outOfStock">
      <div className="cr__outOfStock--img">
        <img
          src={require('../../../../../images/icons/icon_warning.svg')}
          alt="Actualizar inventario"
        />
      </div>
      <div className="cr__outOfStock--content">
        <h5 className="cr__text--paragraph">
          Este producto cuenta con muy poco inventario o inventario agotado.&emsp;
        </h5>
        <a
          onClick={() => props?.onAddStock()}
          className="cr__text--paragraph cr__textColor--colorMain300"
        >
          Actualizar Inventario
        </a>
      </div>
    </div>
  );
}
