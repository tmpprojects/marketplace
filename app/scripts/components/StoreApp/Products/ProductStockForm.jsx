import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Field, reduxForm, formValueSelector, getFormValues } from 'redux-form';

import { getProductDetailForm } from '../../../Reducers/mystore.reducer';
import { myStoreActions, modalBoxActions } from '../../../Actions';
import { SwitchCheckBox } from '../../../Utils/forms/formComponents';
import {
  formatNumberToPrice,
  formatPriceToNumber,
  formatNumberToPriceMyStore,
} from '../../../Utils/normalizePrice';
import { AttributeManager } from './AttributesManager.jsx';
import { Tooltip } from '../../../Utils/Tooltip';

const minPrice = (price) =>
  formatNumberToPriceMyStore(price) < 1 ? 'Precio mínimo $1.00MN' : undefined;

const FieldNormalizedOnBlur = ({
  input: { onBlur, ...input },
  meta: { touched, error, warning },
  normalizeOnBlur,
  ...props
}) => {
  return (
    <div className={`form__data`}>
      <input
        {...props}
        {...input}
        onBlur={(event) => {
          const value =
            event && event.target && event.target.hasOwnProperty('value')
              ? event.target.value
              : event;
          const newValue = normalizeOnBlur ? normalizeOnBlur(value) : value;
          onBlur(newValue);
        }}
        onFocus={(e) => e.target.select()}
      />
      {touched && error && <div className="form_status danger">{error}</div>}
    </div>
  );
};

class ProductStockForm extends React.Component {
  /*
   * Constructor Function
   * @param {object} props React Component Properties
   */
  constructor(props) {
    super(props);
    this.state = {
      attributesWindow_isVisible: false,
      attributes_list: [],
      variations_object: [],
      shouldManageInventory: props.productEditValues
        ? this.inventoryValueToBoolean(
            props.productEditValues.physical_properties.is_available,
          )
        : 1,
      testObject: { combinations: [], single: [], mapping: [] },
    };

    // Bind Scope to class methods
    this.openAttribuesWindow = this.openAttribuesWindow.bind(this);
    this.closeAttribuesWindow = this.closeAttribuesWindow.bind(this);
    this.onFinishEditing = this.onFinishEditing.bind(this);
    this.renderAttributesTable = this.renderAttributesTable.bind(this);
    this.getCombinations = this.getCombinations.bind(this);
    this.onFieldChange = this.onFieldChange.bind(this);
    this.variationsUpdater = this.variationsUpdater.bind(this);
    this.formatStockFields = this.formatStockFields.bind(this);
    this.tester = this.tester.bind(this);
    this.inventoryValueToBoolean = this.inventoryValueToBoolean.bind(this);
  }

  /*
   * Component Life Cylce Methods
   */
  componentDidMount() {
    this.props.fetchIVAList();
    if (this.props.productVariations) {
      const shouldManageInventory = this.props.productEditValues.physical_properties
        ? this.inventoryValueToBoolean(
            this.props.productEditValues.physical_properties.is_available,
          )
        : true;
      this.setState({
        variations_object: this.props.productVariations,
        testObject: this.tester(this.props.productVariations),
        attributes_list: this.props.productEditValues.attribute_list,
        shouldManageInventory,
      });
    }
  }
  componentWillReceiveProps(nextProps) {
    if (nextProps.productEditValues !== this.props.productEditValues) {
      const shouldManageInventory = nextProps.productEditValues.physical_properties
        ? this.inventoryValueToBoolean(
            nextProps.productEditValues.physical_properties.is_available,
          )
        : true;
      this.setState({
        variations_object: nextProps.productVariations,
        // attributes_list: nextProps.productEditValues.attribute_list,
        shouldManageInventory,
      });
    }
    if (nextProps.productVariations !== this.props.productVariations) {
      this.setState({
        variations_object: nextProps.productVariations,
        testObject: this.tester(nextProps.productVariations),
      });
    }
  }

  tester(atts, parent = { combinations: [], single: [], flag: false, mapping: [] }) {
    if (!atts) return false;

    //
    const lookForCombinations = (attributesList = []) => {
      return attributesList.reduce((foundModifier, item) => {
        if (foundModifier) return true;
        if (item.children) {
          return lookForCombinations(item.children) || item.attribute_stock !== null;
        }
        return item.attribute_stock !== null;
      }, false);
    };
    const listingHasCombinations = parent.flag || lookForCombinations(atts);

    //
    return atts.reduce(
      (acc, item, index) => {
        // Define Init Variables
        const retValue = {};
        ({
          attribute_type: retValue.attribute_type,
          attribute_stock: retValue.attribute_stock,
          attribute_uuid: retValue.attribute_uuid,
          value: retValue.value,
        } = item);
        let combinations = [],
          single = [],
          flag = parent.flag,
          mapping = [];

        // If item has children, make recursive inclusion
        if (item.children) {
          if (!listingHasCombinations) {
            flag = true;
          }
          if (!flag && item.attribute_stock === null) {
            combinations = [...parent.combinations, retValue];
            mapping = [...parent.mapping, index];
          } else if (!flag && item.attribute_stock !== null) {
            combinations = [...parent.combinations, retValue];
            mapping = [...parent.mapping, index];
          } else {
            combinations = [...parent.combinations];
            mapping = [...parent.mapping];
          }

          const childrenCombinations = this.tester(item.children, {
            combinations,
            single,
            flag: flag || (!flag && item.attribute_stock !== null) ? true : false,
            mapping,
          });
          combinations = [...acc.combinations, ...childrenCombinations.combinations];
          single = [...childrenCombinations.single];
          mapping = [...acc.mapping, ...childrenCombinations.mapping];

          if (flag) {
            single = [[...acc.single[0], retValue], ...childrenCombinations.single];
            combinations = [
              ...childrenCombinations.combinations.filter((item) => item),
            ];
            mapping = [...childrenCombinations.mapping];
          }

          return {
            combinations,
            single,
            flag,
            mapping,
          };

          // ...else, return raw values
        } else {
          if (!listingHasCombinations) {
            flag = true;
          }
          if (flag) {
            single = [[...acc.single[0], retValue]];
          }

          if (!flag && item.attribute_stock === null) {
            combinations = [...acc.combinations, [...parent.combinations, retValue]];
            mapping = [...acc.mapping, [...parent.mapping, index]];
          } else if (!flag && item.attribute_stock !== null) {
            combinations = [...acc.combinations, [...parent.combinations, retValue]];
            mapping = [...acc.mapping, [...parent.mapping, index]];
            flag = true;
          } else {
            combinations = [[...parent.combinations.filter((item) => item)]];
            mapping = [[...parent.mapping]];
          }

          return {
            combinations,
            single,
            flag,
            mapping,
          };
        }
      },
      { combinations: [], single: [[]], flag: false, mapping: [] },
    );
  }

  /*
   * OPEN ATTRIBUTES MANAGER
   */
  openAttribuesWindow() {
    this.props.openModalBox(() => (
      <AttributeManager
        attribute_types={this.props.attributeTypes}
        initialData={this.state.attributes_list}
        finishEditing={this.onFinishEditing}
        closeWindow={this.closeAttribuesWindow}
      />
    ));

    // Show Attributes Window
    this.setState({
      attributesWindow_isVisible: true,
    });
  }

  /*
   * CLOSE ATTRIBUTES MANAGER
   */
  closeAttribuesWindow() {
    // Close modal box
    this.props.closeModalBox();
  }

  formatStockFields(productVariationsList) {
    return productVariationsList.reduce((_prev, _next) => {
      let modifiedStock = null;
      if (_next.attribute_stock) {
        modifiedStock = {
          price: parseInt(_next.attribute_stock.price_without_discount) || 1,
          quantity: parseInt(_next.attribute_stock.quantity) || 1,
        };
      }
      if (_next.children) {
        let children = this.formatStockFields(_next.children.slice());
      }

      let parent = {
        ..._next,
        attribute_stock: {
          ..._next.attribute_stock,
          ...modifiedStock,
        },
      };
      return [..._prev, parent];
    }, []);
  }

  variationsUpdater(variationsMap, target, values) {
    return variationsMap.reduce((_prev, _next, _index) => {
      const index = parseInt(variationsMap.shift());
      let parent = target[index];
      if (variationsMap.length && parent.children && parent.children.length) {
        const childIndex = parseInt(variationsMap[0]);
        let modifiedChild = this.variationsUpdater(
          variationsMap,
          parent.children,
          values,
        );
        const updateChildren = parent.children.map((item, i) => {
          if (i === childIndex) {
            return modifiedChild;
          } else {
            return item;
          }
        });

        return {
          ...parent,
          children: [...updateChildren],
        };
      }

      return {
        ...parent,
        attribute_stock: {
          ...values,
          price: formatNumberToPriceMyStore(values.price),
        },
      };
    }, {});
  }

  /**
   * onInventoryControlChange()
   * Set product type and update redux form
   * @param {int} productType | Product type (inventario/bajo pedido)
   */
  onInventoryControlChange = (e) => {
    const { value } = e.target;
    this.setState({
      shouldManageInventory: value === 1,
    });

    // Update form field on redux store
    this.props.change('physical_properties.is_available', value);
  };
  /**
   * inventoryValueToBoolean()
   * Standarize and return product type value
   * @param {int} value
   */
  inventoryValueToBoolean(value = '2') {
    return value.toString() === '1' ? true : false;
  }

  /*
   * ON PRICE/QUANTITY VALUE CHANGE (VARIATIONS LIST)
   * @param {object} e : Form Event
   */
  onFieldChange(e) {
    const { variations_object } = this.state;
    const parentRow = e.target.parentNode.parentNode;
    const map = e.target.getAttribute('data-index').split(',');
    const newValues = {
      price: parentRow.querySelector('.price input')
        ? parentRow.querySelector('.price input').value
        : 1,
      quantity: parentRow.querySelector('.quantity input')
        ? parentRow.querySelector('.quantity input').value
        : 0,
      visible: parentRow.querySelector('.visible input')
        ? parentRow.querySelector('.visible input').value
        : true,
    };

    // Get corresponding variation 'row'
    // and update values
    const updatedValues = this.variationsUpdater(
      map.slice(),
      variations_object.slice(),
      newValues,
    );

    // Replace updated row on variations object
    this.setState(
      {
        variations_object: variations_object.map((item, index) =>
          index === parseInt(map[0]) ? updatedValues : item,
        ),
      },
      () => {
        // Update form field on redux store
        this.props.change('attributes', this.state.variations_object);
      },
    );
  }

  /*
   * ON FINISH EDITING ON ATTRIBUTES MANAGER
   * @param {array} attributesList : List of attributes
   */
  onFinishEditing(attributesList) {
    // Structure Variation Lists
    let table_flat = [];
    let table_mixed = [];
    attributesList.forEach((item) => {
      if (item.priceModifier_isSelected) {
        table_mixed.push(item);
      } else {
        table_flat.push(item);
      }
    });
    let variations_object = this.normalizeVariations([
      ...table_mixed,
      ...table_flat,
    ]);

    // Update State
    this.setState(
      {
        attributes_list: attributesList,
        attributesWindow_isVisible: false,
        variations_object,
        testObject: this.tester(variations_object),
      },
      () => {
        // Update form field on redux store
        this.props.change('attributes', variations_object);
      },
    );
  }

  normalizeVariations(attributesList) {
    if (!attributesList.length) return [];

    //
    let target = [];
    if (attributesList.length == 1) {
      const parent = attributesList[0];
      target = parent.attributes.map((item) => {
        let accumulator = {};
        accumulator.attribute_stock = null;
        if (parent.priceModifier_isSelected) {
          accumulator.attribute_stock = {
            price: 1.0,
            quantity: 1,
          };
        }
        accumulator.attribute_extradata = null;
        accumulator.attribute_uuid = {
          is_visible: true,
        };
        accumulator.attribute_type = parent.attribute_type;
        accumulator.value = item;
        return accumulator;
      });
    } else {
      const parent = attributesList[0];
      target = parent.attributes.reduce((attribute_types, item) => {
        let accumulator = {};
        accumulator.attribute_extradata = null;
        accumulator.attribute_uuid = {
          is_visible: true,
        };
        accumulator.attribute_type = parent.attribute_type;
        accumulator.value = item;
        accumulator.attribute_stock = null;
        if (parent.attributes) {
          const children = attributesList.slice(1);
          accumulator.children = this.normalizeVariations(children);
          if (
            parent.priceModifier_isSelected &&
            !children[0].priceModifier_isSelected
          ) {
            accumulator.attribute_stock = {
              price: 1.0,
              quantity: 1,
            };
          }
        }

        attribute_types.push(accumulator);
        return attribute_types;
      }, []);
    }
    return target;
  }

  getCombinations(attributeList) {
    let state = {
      combinations: [[]],
      mapping: [[]],
    };
    return attributeList.reduce((_prev, _next, index) => {
      return {
        mapping: [].concat.apply(
          [],
          _prev.mapping.map((x, i) => {
            return _next.attributes.map((y, j) => {
              return x.concat([j]);
            });
          }),
        ),
        combinations: [].concat.apply(
          [],
          _prev.combinations.map((x, i) => {
            return _next.attributes.map((y, j) => {
              return x.concat([y]);
            });
          }),
        ),
      };
    }, state);
  }

  /*
   * RENDER ATTRIBUTE TABLES
   */
  renderAttributesTable() {
    if (!this.state.variations_object) return false;
    const styleType = { display: 'flex', flex: '1' };
    const fieldsStyle = {};
    const { combinations, single, mapping } = this.state.testObject;
    const test_table = combinations.map((combination, i) => {
      let parent = [];
      // Build Table Headers on First iteration
      if (i === 0) {
        const table_headers = (
          <tr className="variations-list__item variations-list--headers">
            {combination.map((item, j) => (
              <th key={j} className="name">
                {item.attribute_type}
              </th>
            ))}
            <th className="price" style={{ textAlign: 'center' }}>
              Precio
            </th>
            <th
              className="quantity"
              style={{
                textAlign: 'center',
                display: this.state.shouldManageInventory ? 'block' : 'none',
              }}
            >
              Cantidad
            </th>
            <th className="visible" style={{ textAlign: 'center' }}>
              Visible
            </th>
          </tr>
        );
        parent = [...parent, table_headers];
      }
      // Build rows of product combinations
      parent = [
        ...parent,
        <tr key={i} className="variations-list__item">
          {combination.map((item, j) => (
            <td key={j} className="name">
              {item.value}
            </td>
          ))}

          <td className="price">
            <input
              value={mapping[i].reduce(
                (a, b, j, c) =>
                  j === c.length - 1
                    ? a[b].attribute_stock.price_without_discount
                    : a[b].children,
                this.state.variations_object,
              )}
              name={`price[${mapping[i]}]`}
              onChange={this.onFieldChange}
              onBlur={(e) => {
                e.target.value = formatPriceToNumber(e.target.value);
              }}
              type="text"
              placeholder="$1.00"
              data-index={`${mapping[i]}`}
            />
          </td>
          <td
            className={`quantity ${
              mapping[i].reduce(
                (a, b, j, c) =>
                  j === c.length - 1 ? a[b].attribute_stock.quantity : a[b].children,
                this.state.variations_object,
              ) <= 5
                ? 'withoutStock'
                : ''
            }`}
            style={{
              display: this.state.shouldManageInventory ? 'block' : 'none',
            }}
          >
            <input
              value={mapping[i].reduce(
                (a, b, j, c) =>
                  j === c.length - 1 ? a[b].attribute_stock.quantity : a[b].children,
                this.state.variations_object,
              )}
              name={`quantity[${mapping[i]}]`}
              onChange={this.onFieldChange}
              type="number"
              placeholder="0"
              min="0"
              data-index={`${mapping[i]}`}
            />
          </td>
          <td className="visible">
            {/*<button value={mapping[i].reduce((a, b, j, c) => j === c.length-1 ? a[b].attribute_stock.visible : a[b].children, this.state.variations_object)} name={`visible[${mapping[i]}]`} onChange={this.onFieldChange} type="button" data-index={`${[...mapping[i]]}`}  >True</button>*/}

            {/*<Field
                            component={SwitchCheckBox}
                            checked='true'
                            id={`visible_${mapping[i].join('-')}`}
                            name={`visible_${mapping[i].join('-')}`}
                        onChange={() => console.log('asdasasa asdasd')} />*/}
          </td>
        </tr>,
      ];
      return parent;
    });

    return (
      <div className="variations-list">
        <div className="variations-list__table">
          {combinations[0] && combinations[0].length > 0 && test_table}
        </div>
        {single.map((item, index) => {
          return (
            <div key={index} className="variations-list__table">
              {item.map((attribute, attributeIndex) => {
                return (
                  attributeIndex === 0 && (
                    <div
                      key={attributeIndex}
                      className="variations-list__item variations-list--headers"
                    >
                      <div className="name">{attribute.attribute_type}</div>
                      <div style={styleType}>
                        {/*<div className="visible">Visible</div>*/}
                      </div>
                    </div>
                  )
                );
              })}
              {item.map((attribute, attributeIndex) => {
                return (
                  <div
                    key={attributeIndex}
                    className="variations-list__item"
                    style={styleType}
                  >
                    <div className="name">{attribute.value}</div>
                    <div style={styleType}>
                      {/*<Field
                                                    component={SwitchCheckBox}
                                                    checked='true'
                                                    id={`visible_${mapping[i].join('-')}`}
                                                    name={`visible_${mapping[i].join('-')}`}
                                                onChange={() => console.log('asdasasa asdasd')} />*/}
                    </div>
                  </div>
                );
              })}
            </div>
          );
        })}
      </div>
    );
  }

  /*
   * React Component Life Cycle Functions
   */
  render() {
    const { handleSubmit } = this.props;
    const {
      testObject,
      productEditValues,
      shouldManageInventory,
      variations_object = [],
      attributesWindow_isVisible,
    } = this.state;
    const {
      myStore: { listIVA },
    } = this.props;

    const priceModule = (
      <React.Fragment>
        <div className="form__data" id="price">
          <label htmlFor="price_txt">Precio:</label>
          {!this.props.initialValues.attributes_change_price ? (
            <Field
              id="price_txt"
              name="price"
              component={FieldNormalizedOnBlur}
              normalizeOnBlur={formatNumberToPriceMyStore}
              validate={[minPrice]}
              type="number"
            />
          ) : (
            <span className="note">Introduce el precio en las variaciones</span>
          )}
        </div>
        <fieldset>
          <div className="field_details">
            {/* <label className="detail_txt">Atributos y Variaciones <Tooltip message=""/></label> */}
            <div className="form__help">
              <p>
                Agrega variaciones y atributos a tu producto. Puedes controlar su
                precio e inventario individualmente.
              </p>
            </div>
          </div>

          <React.Fragment>
            <button
              type="button"
              onClick={this.openAttribuesWindow}
              className="cr__text--caption cr__textColor--colorDark100 addAttributes-btn button-square--grey"
            >
              {variations_object.length < 1
                ? '+ Agregar Variaciones'
                : 'Editar Variaciones'}
            </button>

            {(testObject.single || testObject.combinations) &&
              this.renderAttributesTable()}
          </React.Fragment>
        </fieldset>
      </React.Fragment>
    );

    //
    return (
      <div className="ui-detailed-block">
        <div className="ui-detailed-block__detail">
          <h3 className="cr__text--paragraph cr__textColor--colorDark300 heading2">
            Precios y Atributos
            {/* <Tooltip message="" /> */}
          </h3>
          <div className="form__help">
            <p className="cr__text--paragraph cr__textColor--colorDark100">
              Agrega variaciones y atributos a tu producto. Puedes controlar su
              precio e inventario individualmente.
            </p>
          </div>
        </div>

        <div className="form form--annotated productStock ui-detailed-block__content">
          <div className="form product-type">
            <fieldset>
              <div className="field_details">
                <label
                  htmlFor="detail_txt"
                  className="cr__text--subtitle3 cr__textColor--colorDark300 sectionTitle"
                >
                  Tipo de producto
                  <Tooltip message="" />
                </label>
                <div className="form__help detail">
                  <p className="cr__text--paragraph cr__textColor--colorDark100">
                    ¿El artículo se elabora bajo pedido o cuenta con un inventario?
                  </p>
                </div>
              </div>

              <div className="form product-type__option">
                <div className="option">
                  <Field
                    type="radio"
                    className="dots"
                    id="is_available-stock"
                    name="physical_properties.is_available"
                    value="1"
                    onChange={() => this.onInventoryControlChange}
                    component="input"
                  />
                  <label htmlFor="is_available-stock" className="dot">
                    Inventario{' '}
                  </label>

                  {shouldManageInventory && (
                    <div className="option_detail">
                      <div className="form__data form__data--quantity">
                        <label htmlFor="quantity_txt">Cantidad:</label>
                        <Field
                          id="quantity_txt"
                          name="quantity"
                          component="input"
                          type="number"
                          min="0"
                          step="1"
                        />
                      </div>
                      {priceModule}
                    </div>
                  )}
                </div>
                <div className="option">
                  <Field
                    type="radio"
                    className="dots"
                    id="is_available-custom"
                    name="physical_properties.is_available"
                    value="2"
                    onChange={this.onInventoryControlChange}
                    component="input"
                  />
                  <label htmlFor="is_available-custom" className="dot">
                    A Medida{' '}
                  </label>
                  {!shouldManageInventory && (
                    <div className="option_detail">
                      <div className="form__data form__data--prepare">
                        <label htmlFor="prepare_txt">Tiempo de elaboración:</label>
                        <Field
                          id="prepare_txt"
                          name="physical_properties.fabrication_time"
                          component="input"
                          type="number"
                        />
                        <span className="txt">días.</span>
                      </div>
                      <div className="form__data form__data--quantity">
                        <label htmlFor="quantity_txt--custom">
                          Cantidad:
                          <span className="note">
                            Máximo de productos por compra
                          </span>
                        </label>
                        <Field
                          id="quantity_txt"
                          name="quantity"
                          component="input"
                          type="number"
                          min="0"
                          step="1"
                        />
                      </div>
                      {priceModule}
                    </div>
                  )}
                </div>

                {/** PRODUCT TAXES */}
                <div className="option">
                  <div className="field_details">
                    <label
                      htmlFor="detail_txt"
                      className="cr__text--subtitle3 cr__textColor--colorDark300 sectionTitle"
                    >
                      Impuesto a productos
                      {/* <Tooltip message="" /> */}
                    </label>
                    <div className="form__help detail">
                      <p className="cr__text--paragraph cr__textColor--colorDark100">
                        Selecciona el IVA correspondiente a tu producto.
                        <br />
                        La deducción de impuestos depende del tipo y categoría de tu
                        producto.
                        {/* Si no estás seguro de de qué opción
                        elegir,{' '}
                        <a href="#" target="_blank">
                          puedes consultar mayor información aquí
                        </a> */}
                      </p>
                    </div>
                  </div>
                  {listIVA
                    ?.sort((a, b) => a.value - b.value)
                    .map((item) => (
                      <React.Fragment>
                        <Field
                          type="radio"
                          className="dots"
                          name="percentage_iva"
                          value={item.value}
                          component="input"
                          key={item.id}
                          id={`tax-${item.id}`}
                        />
                        <label
                          htmlFor={`tax-${item.id}`}
                          className="dot"
                          style={{ marginRight: '1em' }}
                        >
                          IVA ${parseFloat(item.value, 10) * 100}%
                        </label>
                      </React.Fragment>
                    ))}
                </div>
                {/** /PRODUCT TAXES */}
              </div>
            </fieldset>
          </div>
        </div>
      </div>
    );
  }
}

// Wrap component within reduxForm
const formName = 'myStoreEditProduct_form';
// ProductStockForm = reduxForm({
//   form: formName,
//   asyncBlurFields: [],
//   destroyOnUnmount: false, // <---- Preserve data. Dont destroy form on unmount
//   enableReinitialize: true, // <---- Allow updates when 'initValues' prop change
//   keepDirtyOnReinitialize: true, // <---- Prevent 'dirty' fields to update
//   updateUnregisteredFields: true, // <---- Update unregistered fields.
//   forceUnregisterOnUnmount: true, // <---- Unregister fields on unmount
// })(ProductStockForm);

// Add Redux state and actions to component´s props
const selector = formValueSelector(formName);
function mapStateToProps(state) {
  const productDetail = getProductDetailForm(state);

  return {
    productVariations: selector(state, 'attributes'),
    productEditValues: getFormValues(formName)(state),
    initialValues: productDetail,
  };
}
function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      openModalBox: modalBoxActions.open,
      closeModalBox: modalBoxActions.close,
      updateProductAttributes: myStoreActions.updateProductAttributes,
      fetchIVAList: myStoreActions.fetchIVAList,
    },
    dispatch,
  );
}

// Export Component
ProductStockForm = connect(mapStateToProps, mapDispatchToProps)(ProductStockForm);
export default ProductStockForm;
