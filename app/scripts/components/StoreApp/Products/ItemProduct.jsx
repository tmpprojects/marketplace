import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Sentry from '../../../Utils/Sentry';

import { myStoreActions, statusWindowActions } from '../../../Actions';
import { ResponsiveImage } from '../../../Utils/ImageComponents';
import { UIIcon } from '../../../Utils/UIIcon';
import Placeholder from '../../../../images/placeholder/placeholder--productPage.jpg';

class ItemProduct extends React.Component {
  deleteProduct = (slug) => {
    const confirmation = confirm(
      '¿Estás seguro de querer eliminar este producto?. La operación no podrá deshacerse.',
    );
    if (confirmation) {
      // Delete Product Image

      return this.props
        .deleteProduct(slug)
        .then((response) => {
          this.props.openStatusWindow({
            type: 'success',
            message: 'Eliminamos el producto correctamente.',
          });
          this.props.fetchProductList();
        })
        .catch((error) => {
          Sentry.captureException(error);
          this.props.openStatusWindow({
            type: 'error',
            message: 'Error. Por favor, intenta de nuevo.',
          });
        });
    }
  };

  render() {
    const { items = [] } = this.props;
    return items.map((item) => {
      const photoObject = item.photo || {};
      return (
        <li
          key={item.slug}
          className={`product ${item.quantity === 0 ? 'withoutStock' : ''}`}
        >
          <div className="product__wrapper" style={{ borderRadius: '3px' }}>
            <div className="product__image">
              <Link to={`/my-store/products/edit/${item.slug}`}>
                <ResponsiveImage
                  alt={photoObject.small.length > 0 ? item.name : ''}
                  src={photoObject}
                />
              </Link>
            </div>

            <div className="product__info">
              <div className="product__name">
                <Link
                  to={`/my-store/products/edit/${item.slug}`}
                  className="cr__textColor--colorDark200 cr__text--paragraph name"
                >
                  {item.name}
                </Link>
              </div>
              <div className="price">
                <span className="strong cr__textColor--colorDark200 cr__text--paragraph">
                  ${item.price + ' '} MXN
                </span>
              </div>

              <div className="stock_quantity">
                {item.physical_properties &&
                item.physical_properties.is_available.value === 2 ? (
                  <div className="quantity cr__textColor--colorDark200 cr__text--paragraph">
                    <span className="">{item.quantity} en inventario</span>
                  </div>
                ) : item.quantity === 0 ? (
                  <div className="quantity without-stock cr__textColor--colorRed300 cr__text--paragraph">
                    <span className="">Sin Inventario</span>
                  </div>
                ) : item.quantity <= 5 ? (
                  <div className="quantity">
                    <span className=" cr__textColor--colorRed300 cr__text--paragraph">
                      {item.quantity} en inventario
                    </span>
                  </div>
                ) : (
                  <div className="quantity cr__textColor--colorDark200 cr__text--paragraph">
                    <span className="">{item.quantity} en inventario</span>
                  </div>
                )}
              </div>

              <div className="aditional__detail">
                {item.status.value === 'public' && item.store.is_active ? (
                  <Link
                    className="cr__text--caption cr__textColor--colorRed300 status_link"
                    to={`/stores/${item.store.slug}/products/${item.slug}`}
                  >
                    Vista previa
                  </Link>
                ) : item.status.value === 'public' && !item.store.is_active ? (
                  <span className="status_inactive">No visible</span>
                ) : (
                  <span className="cr__text--caption cr__textColor--colorDark200">
                    Borrador
                  </span>
                )}

                {item.physical_obligations.ship_nationwide &&
                  item.store.obligations.ship_nationwide && (
                    <p className="cr__text--caption cr__textColor--colorViolet300">
                      Nacional
                    </p>
                  )}
              </div>

              <div className="product__actions">
                <UIIcon
                  icon="delete"
                  type="error"
                  className="delete button"
                  onClick={() => this.deleteProduct(item.slug)}
                >
                  Eliminar
                </UIIcon>
                {/*<button className="publish button-square--white" onClick={() => this.publishProduct(item.slug)}>Publicar</button>*/}
              </div>
            </div>
          </div>
        </li>
      );
    });
  }
}
// Add Redux state and actions to component´s props
function mapStateToProps(state) {
  const { myStore } = state;
  return {
    myStore: myStore.data,
  };
}
function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      deleteProduct: myStoreActions.deleteProduct,
      openStatusWindow: statusWindowActions.open,
      fetchProductList: myStoreActions.fetchProductList,
    },
    dispatch,
  );
}

// Export Component
export default connect(mapStateToProps, mapDispatchToProps)(ItemProduct);
