import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { SubmissionError } from 'redux-form';
import queryString from 'query-string';

import config from '../../../../config';
import addProduct from '../../hocs/addProduct';
import { statusWindowActions, myStoreActions } from '../../../Actions';
import ItemProduct from './ItemProduct';
import withPagination from '../../../components/hocs/withPagination';
import TableItemProduct from './TableItemProduct/TableItemProduct';
import { IconPreloader } from '../../../Utils/Preloaders';

const MAX_ITEMS_PER_PAGE = 12;
const AddProductButton = addProduct(Link);

/**
 * ProductsList Component (with pagination)
 * Wraps a list of products within 'withPagination' HOC,
 * and displays a paginated component.
 */
const PaginatedListTable = withPagination(TableItemProduct);

const PaginatedListCards = withPagination(ItemProduct);

class ProductsList extends React.Component {
  // PropTypes Definition
  // static propTypes = {
  //     products: PropTypes.shape({
  //         results: PropTypes.array.isRequired
  //     }).isRequired
  // };
  // static defaultProps = {
  //     products: {
  //         results: []
  //     }
  // };

  /*
   * Constructor Function
   * @param {object} props React Component Properties
   */
  constructor(props) {
    super(props);

    // Bind Scope to class methods
    this.submitForm = this.submitForm.bind(this);
    this.publishProduct = this.publishProduct.bind(this);
    this.currentPage = this.getResultsPageFromQueryString(
      this.props.location.search,
    );
    this.state = {
      products_view: 'list',
    };
  }

  componentDidMount() {
    //this.props.fetchProductList();
    this.performSearch(this.currentPage);
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.location.search !== nextProps.location.search) {
      this.performSearch(
        this.getResultsPageFromQueryString(nextProps.location.search),
      );
    }
  }

  /*
   * FORM FUNCTIONS
   */
  submitForm(values) {
    const formData = {};
    ({
      location: formData.location,
      mail: formData.contact_mail,
      phone: formData.telephone,
      mobile: formData.mobile,
      instagram: formData.link_instagram,
      pinterest: formData.link_pinterest,
      facebook: formData.link_facebook,
    } = values);

    // Update State and Store via API
    return this.props
      .updateMyStore(formData)
      .then((response) => {
        this.props.openStatusWindow({
          type: 'success',
          message: 'Información actualizada.',
        });
      })
      .catch((error) => {
        this.props.openStatusWindow({
          type: 'error',
          message: 'Error. Por favor, intenta de nuevo.',
        });
        throw new SubmissionError({
          _error:
            'Ocurrió un problema al actualizar tu información. Por favor, intenta de nuevo.',
        });
      });
  }

  /**
   * fetchContent()
   * Retrieves results from the API. Call redux actions.
   * @param {string} query | Query string to send to API
   */
  fetchContent = (query) => Promise.all([this.props.fetchProductList(query)]);

  performSearch = (page = 1) => {
    // Scroll to top of page
    // TODO: Scroll to top of results list instead of top of document.
    window.scrollTo(0, 0);
    const query = `page=${page}&page_size=${MAX_ITEMS_PER_PAGE}`;
    this.currentPage = parseInt(page, 10);
    this.fetchContent(query);
  };

  /**
   * getResultsPageFromQueryString()
   * @param {string} query
   * Extracts the page number and returns it as an int number.
   */
  getResultsPageFromQueryString = (query) => {
    if (query) {
      const resultsPage = parseInt(queryString.parse(query).p, 10);
      return !isNaN(resultsPage) ? resultsPage : 1;
    }
    return 1;
  };

  publishProduct(slug) {
    return;
    // Delete Product Image
    return this.props
      .deleteProductPhoto(formData)
      .then((response) => {
        this.props.openStatusWindow({
          type: 'success',
          message: 'Información actualizada.',
        });
      })
      .catch((error) => {
        this.props.openStatusWindow({
          type: 'error',
          message: 'Error. Por favor, intenta de nuevo.',
        });
        throw new SubmissionError({
          _error:
            'Ocurrió un problema al actualizar tu información. Por favor, intenta de nuevo.',
        });
      });
  }

  /*
   * React Component Life Cycle Functions
   */
  render() {
    const { products_view } = this.state;

    const { products, myStore } = this.props;

    if (!products.results) {
      return null;
    }

    const currentPage = this.currentPage;

    if (myStore.status === 'loading') return <IconPreloader />;

    return (
      <div>
        <section className="storeApp__module storeApp__products storeApp__main cr__products">
          <div className="cr__products--title">
            <span className="cr__text--subtitle cr__textColor--colorDark300 ">
              Mis Productos
            </span>
          </div>

          <div className="container__views--buttons">
            {/* <p className="national-shipping">Nacionales</p> */}
            <div className="container__views--buttons--left">
              <button
                className={`button-simple view_list ${
                  products_view === 'list' ? 'activeButton' : ''
                }`}
                onClick={() => this.setState({ products_view: 'list' })}
              >
                Lista
              </button>
              <span className="cr__separator" />
              <button
                className={`button-simple view_cards ${
                  products_view === 'cards' ? 'activeButton' : ''
                }`}
                onClick={() => this.setState({ products_view: 'cards' })}
              >
                Cuadros
              </button>
            </div>
          </div>
          <div className="product-list-container form form--annotated">
            {!products.results.length ? (
              <div className="withoutProducts">
                A&uacute;n no tienes productos en tu tienda.
                <br />
                <AddProductButton to="/my-store/products/add" className="c2a_square">
                  Agregar Producto
                </AddProductButton>
              </div>
            ) : (
              <React.Fragment>
                {products_view === 'list' ? (
                  <PaginatedListTable
                    items={products?.results}
                    baseLocation="/my-store/products/?"
                    maxItems={MAX_ITEMS_PER_PAGE}
                    action={this.testingFunction}
                    totalPages={products.npages}
                    page={currentPage}
                    {...this.props}
                  />
                ) : (
                  <ul
                    className={`products__view ${
                      this.state.products_view === 'cards'
                        ? 'products__view--cards'
                        : ''
                    }`}
                  >
                    <PaginatedListCards
                      items={products?.results}
                      baseLocation="/my-store/products/?"
                      maxItems={MAX_ITEMS_PER_PAGE}
                      action={this.testingFunction}
                      totalPages={products.npages}
                      page={currentPage}
                      {...this.props}
                    />
                  </ul>
                )}
              </React.Fragment>
            )}
          </div>
        </section>
      </div>
    );
  }
}
const buildQuery = (query) => {
  return `page=${query.page}&page_size=${MAX_ITEMS_PER_PAGE}`;
};
// Load Data for Server Side Rendering
ProductsList.loadData = (reduxStore, routePath) => {
  const { match } = routePath;
  const resultsPage = match.params.p;

  //const resultsPage = 1;
  const searchQuery = buildQuery({
    page: resultsPage,
    MAX_ITEMS_PER_PAGE,
  });
  const promises = [
    reduxStore.dispatch(myStoreActions.fetchProductList(searchQuery)),
  ];
  return Promise.all(promises.map((p) => (p.catch ? p.catch((e) => e) : p)));
};
// Add Redux sta te and actions to component´s props
function mapStateToProps(state) {
  return {
    myStore: state.myStore,
  };
}
function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      openStatusWindow: statusWindowActions.open,
      fetchProductList: myStoreActions.fetchProductList,
    },
    dispatch,
  );
}

// Export Component
export default connect(mapStateToProps, mapDispatchToProps)(ProductsList);
