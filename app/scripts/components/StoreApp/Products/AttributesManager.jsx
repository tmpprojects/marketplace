import React from 'react';
import PropTypes from 'prop-types';

import { InputField } from '../../../Utils/forms/formComponents';
import withCounter from '../../hocs/withCounter';

const getUniqueKey = () => new Date().getTime();
const AttributeModel = {
  attribute_type: 0,
  attribute_name: '',
  attributes: [],
  priceModifier_isSelected: false,
};

// SOCIAL NETWORKS FIELDS
const AttributeField = ({ input, ...props }) => (
  <input
    {...input}
    {...props}
    type="text"
    value={this.state.attribute_input}
    onChange={this.onAttributeNameChange}
    autoFocus
  />
);

const AttributeFieldInput = withCounter(AttributeField);
/*------------------------------------------
ATTRIBUTES COMPONENT CLASS
------------------------------------------*/
class AttributesComponent extends React.Component {
  // PropTypes / DefaultProps
  static propTypes = {
    attributes: PropTypes.array.isRequired,
  };

  /*
   * Constructor Function
   * @param {object} props React Component Properties
   */
  constructor(props) {
    super(props);

    // Initi State
    const initialState = this.props.initialData || { ...AttributeModel };
    this.state = {
      has_attributeType: initialState.attributes.length > 0,
      attribute_input: '',
      ...initialState,
    };

    // Bind Scope to class methods
    this.onAttributeChange = this.onAttributeChange.bind(this);
    this.onPriceModifierChange = this.onPriceModifierChange.bind(this);
    this.onAttributeNameChange = this.onAttributeNameChange.bind(this);
    this.onAttributeNameSubmit = this.onAttributeNameSubmit.bind(this);
    this.addAttribute = this.addAttribute.bind(this);
    this.deleteAttribute = this.deleteAttribute.bind(this);
    this.deleteAttributeType = this.deleteAttributeType.bind(this);
    this.renderAttributes = this.renderAttributes.bind(this);
  }

  /*
   * Render Attributes
   * Mount iterable attributes list
   */
  renderAttributes() {
    const { attributes } = this.state;

    return attributes.map((item, index) => {
      return (
        <div key={index} className="attribute">
          {item}
          <button
            onClick={() => this.deleteAttribute(index)}
            className="attribute__delete"
          >
            Eliminar
          </button>
        </div>
      );
    });
  }

  /*
   * Attribute Type Change
   * Mount iterable attributes list
   * @param {object} event : Form Event
   */
  onAttributeChange(event) {
    event.preventDefault();
    const { name, value, selectedIndex } = event.target;
    let newState = {
      ...this.state,
      attribute_type: value,
      attribute_name: event.nativeEvent.target[selectedIndex].text,
    };

    // Update State only if there´s a valid option selected
    if (value !== '0') {
      this.props.updateAttribute(newState, this.props.attributeIndex);
      this.setState({
        ...newState,
        has_attributeType: true,
      });

      // If attribute type is invalid. Reset Component
    } else {
      this.props.updateAttribute(null, this.props.attributeIndex);
      this.setState({
        ...AttributeModel,
        has_attributeType: false,
      });
    }
  }

  /*
   * Delete Attributes Type
   */
  deleteAttributeType() {
    this.props.deleteAttribute(this.props.attributeIndex);
  }

  /*
   * Price Modifier Change Handler
   * @param {object} event : Form Event
   */
  onPriceModifierChange(event) {
    //event.preventDefault();
    const newState = {
      ...this.state,
      priceModifier_isSelected: event.target.checked,
    };

    // Update State
    this.props.updateAttribute(newState, this.props.attributeIndex);
    this.setState({
      ...newState,
    });
  }

  /*
   * Attribute Name Input Handler
   * @param {object} event : Form Event
   */
  onAttributeNameChange(event) {
    event.preventDefault();
    this.setState({
      attribute_input: event.target.value,
    });
  }

  /*
   * On Attribute Add
   * @param {object} event : Form Event
   */
  addAttribute() {
    let newState = {
      ...this.state,
      attributes: [this.state.attribute_input, ...this.state.attributes],
      attribute_input: '',
    };

    // Add Atribute to list
    // TODO: Validate user input
    this.props.updateAttribute(newState, this.props.attributeIndex);
    this.setState({
      ...newState,
    });

    return false;
  }

  /*
   * On Delete Attribute
   * @param {int} targetIndex : Attribute Index
   */
  deleteAttribute(targetIndex) {
    let newState = {
      ...this.state,
      attributes: this.state.attributes.filter(
        (item, index) => index != targetIndex,
      ),
    };

    // Delete Atribute from list
    // TODO: Validate user input
    this.props.updateAttribute(newState, this.props.attributeIndex);
    this.setState({
      ...newState,
    });
  }

  /*
   * On Attribute Name Submit
   * @param {object} event :Form Event
   */
  onAttributeNameSubmit(event) {
    event.preventDefault();
    this.addAttribute();
  }

  /*
   * React Component Life Cycle Functions
   */
  render() {
    const { has_attributeType, attributes } = this.state;
    return (
      <div className="attribute-component">
        <div className="attribute-type form">
          <label className="attribute-type__label">Tipo de variación:</label>
          <select
            onChange={this.onAttributeChange}
            value={this.state.attribute_type}
            name="attributesList"
            type="select"
            className="attribute-type__attribute-types"
          >
            <option value="0">Elige una variación...</option>
            {this.props.attributes.map((item) => (
              <option value={item.slug} key={item.id}>
                {item.name}
              </option>
            ))}
          </select>

          <button
            className="attribute-type__delete"
            onClick={this.deleteAttributeType}
          >
            Eliminar
          </button>
        </div>

        {
          // If we have a valid 'Attribute Type'
          // render the list of 'Attribute Values' and a form to add more.
          has_attributeType && (
            <div>
              <div className="attributes">
                <form
                  onSubmit={this.onAttributeNameSubmit}
                  className="addAtributtes"
                >
                  {/* <Field
                                    name="attribute_name"
                                    maxLength={80}
                                    className="addAtributtes__input"
                                    component={AttributeFieldInput}
                                    placeholder="Nombre de la variación"
                                /> */}
                  <input
                    type="text"
                    maxLength="35"
                    value={this.state.attribute_input}
                    onChange={this.onAttributeNameChange}
                    autoFocus
                    name="attribute_name"
                    placeholder="Nombre de la variación (máx 35 caracteres)"
                    className="addAtributtes__input"
                  />
                  <button
                    type="button"
                    onClick={this.addAttribute}
                    className="addAtributtes__button"
                  >
                    Agregar
                  </button>
                </form>

                {attributes.length > 0 && (
                  <ul className="attributes__list">{this.renderAttributes()}</ul>
                )}
              </div>

              <div className="price-modifier">
                <input
                  id={`attribute_modifiesPrice${this.props.attributeIndex}`}
                  onChange={this.onPriceModifierChange}
                  type="checkbox"
                  name={`attribute_modifiesPrice`}
                  checked={this.state.priceModifier_isSelected}
                  className="price-modifier__check"
                />
                <label
                  htmlFor={`attribute_modifiesPrice${this.props.attributeIndex}`}
                  className="price-modifier__label"
                >
                  Este atributo modifica el precio o cantidad del artículo.
                </label>
              </div>
            </div>
          )
        }
      </div>
    );
  }
}

/*------------------------------------------
ATTRIBUTES MANAGER CLASS
------------------------------------------*/
export class AttributeManager extends React.Component {
  // PropTypes / DefaultProps
  static defaultProps = {
    attribute_types: [],
    max_attributes: 5,
  };
  static propTypes = {
    attribute_types: PropTypes.array.isRequired,
    max_attributes: PropTypes.number.isRequired,
  };

  /*
   * Constructor Function
   * @param {object} props React Component Properties
   */
  constructor(props) {
    super(props);

    // Set initial State
    let initState = {
      has_attributes: false,
      attributes: [],
    };
    if (this.props.initialData && this.props.initialData.length > 0) {
      initState = {
        has_attributes: this.props.initialData.length > 0 || false,
        attributes: this.props.initialData,
      };
    }
    this.state = initState;

    // Bind Scope to class methods
    this.onAttributeUpdate = this.onAttributeUpdate.bind(this);
    this.onAttributeAdd = this.onAttributeAdd.bind(this);
    this.onAttributeDelete = this.onAttributeDelete.bind(this);
    this.renderAttributes = this.renderAttributes.bind(this);
    this.onFinishEditing = this.onFinishEditing.bind(this);
    this.onCloseWindow = this.onCloseWindow.bind(this);
  }

  /*
   * On Attribute Update
   * @param {object} newAttribute : New Attribute Object
   * @param {int} attributeIndex : Attribute Index
   */
  onAttributeUpdate(newAttribute, attributeIndex) {
    if (!newAttribute) {
      return;
    }

    // Update Component
    this.setState({
      has_attributes: true,
      attributes: this.state.attributes.map((item, index) => {
        if (index === attributeIndex) {
          return newAttribute;
        }
        return item;
      }),
    });
  }

  /*
   * On Add Attribute
   * Adds a new 'Attribute Component' subset
   */
  onAttributeAdd() {
    const newAttribute = {
      ...AttributeModel,
      attribute_id: getUniqueKey(),
    };
    this.setState({
      attributes: [
        ...this.state.attributes,
        {
          ...newAttribute,
        },
      ],
    });
  }

  /*
   * On Delete Attribute
   * Removes an 'Attribute Component' subset
   */
  onAttributeDelete(attributeIndex) {
    const filteredAttributes = this.state.attributes.filter(
      (item, index) => index !== attributeIndex,
    );
    this.setState({
      attributes: filteredAttributes,
      has_attributes: filteredAttributes.length > 0,
    });
  }

  /*
   * On Finish Editing the manager
   * Propagates the state of the manager to the parent component
   */
  onFinishEditing() {
    const validAttributes = this.state.attributes.filter(
      (item, index) => item.attributes.length > 0,
    );
    this.setState({
      attributes: validAttributes,
    });
    this.props.finishEditing(validAttributes);
    this.onCloseWindow();
  }

  /*
   * On Close Manager
   * Close Attribute Manager
   */
  onCloseWindow() {
    this.props.closeWindow();
  }

  /*
   * Render Attribute Components Subsets
   */
  renderAttributes() {
    return this.state.attributes.map((item, index) => {
      const id = item.attribute_id || 0;
      return (
        <div key={`item-${id}`} className="attributes-manager__list">
          <AttributesComponent
            initialData={item}
            attributeIndex={index}
            attributes={this.props.attribute_types}
            deleteAttribute={this.onAttributeDelete}
            updateAttribute={this.onAttributeUpdate}
          />
        </div>
      );
    });
  }

  /*
   * React Component Life Cycle Functions
   */
  render() {
    const { attributes, has_attributes } = this.state;
    return (
      <div className="attribute-manager storeApp-modal">
        <h4 className="storeApp-modal__title">Edita las variaciones del producto</h4>

        <div className="storeApp-modal__content">
          {
            // List Attributes
            this.renderAttributes()
          }

          {
            // Show the button to add more attributes
            // if we have at least on attribute component
            // and less than the maximum allowed
            attributes.length < this.props.max_attributes && (
              <div className="manager-add">
                <button
                  type="button"
                  onClick={this.onAttributeAdd}
                  className="manager-add__button"
                >
                  + Agregar Variación
                </button>
              </div>
            )
          }

          {
            // Save & Cancel Buttons
            <div className="storeApp-modal__navBottom">
              <button
                type="button"
                onClick={this.onCloseWindow}
                className="storeApp-modal__button storeApp-modal__button--cancel"
              >
                Cancelar
              </button>
              <button
                type="button"
                onClick={this.onFinishEditing}
                className="storeApp-modal__button"
              >
                Guardar
              </button>
            </div>
          }
        </div>
      </div>
    );
  }
}
