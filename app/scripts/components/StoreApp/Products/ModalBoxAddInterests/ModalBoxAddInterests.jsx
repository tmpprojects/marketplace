import React, { useState, useEffect, useRef } from 'react';
import { CSSTransition } from 'react-transition-group';
import { trackWithGTM } from '../../../../Utils/trackingUtils';
import ArrowDown from '../../../../../images/icons/arrows/arrow_down_gray.svg';
import IconDelete from '../../../../../images/icons/icon_delete.svg';
import IconNext from '../../../../../images/icons/arrows/arrow_next_gray.svg';
import '../_modalBoxAddCategory.scss';

const initialValues = {
  name: null,
  slug: null,
  children: null,
  parent: null,
};
const levelInitialValues = {
  level2: null,
  level3: null,
  level4: null,
};
const onGTMTracking = (value) => {
  const data = {
    id: 'MyStore',
    name: `${value}`,
    position: 1,
  };
  trackWithGTM('eec.impressionClick', [data], 'MyStore-Interests');
};

export default function ModalBoxAddInterest(props) {
  useEffect(() => {
    if (selectedInterest?.slug === null) {
      setSelectedInterestTree(breadcrumbsPathForm);
    }
  }, [selectedInterest?.slug]);
  const [toggleSelect, setToggleSelect] = useState(false);

  const [selectedInterest, setSelectedInterest] = useState(initialValues);

  const [selectedInterestTree, setSelectedInterestTree] = useState(null);

  const [fatherInterest, setFatherInterest] = useState(levelInitialValues);

  const [level2, setLevel2] = useState(initialValues);
  const [level3, setLevel3] = useState(initialValues);
  const [level4, setLevel4] = useState(initialValues);

  const [navigationLevel, setNavigationLevel] = useState(1);

  const finalBreadCrumbsTree = () => {
    const finalArray = selectedInterestTree.filter(
      (item) => item.slug !== selectedInterest?.slug,
    );

    return finalArray?.map(({ slug, name }, i) => (
      <span key={slug} style={{ lineHeight: '1.6em' }}>
        {' '}
        {name} {i + 1 !== finalArray?.length && '>'}
      </span>
    ));
  };

  //React Transition Group Setup
  const ref = useRef(null);

  useEffect(() => {
    let refHeight = ref.current?.firstChild.offsetHeight || 0;

    // To get the correct height on the list box
    refHeight++;

    setDropdownHeight(refHeight);
  }, [toggleSelect, navigationLevel]);

  const [dropdownHeight, setDropdownHeight] = useState(0);

  function calcDropdownHeight(el) {
    const height = el.offsetHeight || 0;

    setDropdownHeight(height);
  }

  const {
    marketInterests,
    closeModalBox,
    initialValues: productInitialValues,
    onSubmitInterest,
    createBreadcrumbsTree,
    breadcrumbsPathForm,
    onEditInterest,
    editingInterest,
    onEditingState,
    index,
  } = props;

  return (
    <div className="cr__modalBox--addCategory">
      <div className="cr__modalBox--addCategory--header">
        <span className="cr__textColor--colorDark300 cr__text--subtitle3">
          Intereses
        </span>
      </div>
      <div className="cr__modalBox--addCategory--content">
        <div className="cr__modalBox--addCategory--content--instructions">
          <h5 className="cr__text--paragraph cr__textColor--colorDark300">
            Selecciona hasta 3 opciones donde tu producto se describa de la mejor
            manera.
          </h5>
          <h6 className="cr__text--caption cr__textColor--colorDark100">
            <span className="cr__textColor--colorDark300">Pro Tip:</span> Intenta ser
            lo más específico que puedas, así lograrás que tu producto se muestre
            dentro de todo el árbol de categorías.
          </h6>

          {selectedInterest?.name !== null && selectedInterestTree?.length > 1 && (
            <h5 className="cr__text--paragraph cr__textColor--colorDark300 cr__modalBox--addCategory--content--instructions--tree">
              Tu producto se encuentra en:
              <br />
              {selectedInterestTree.length > 0 && finalBreadCrumbsTree()}
            </h5>
          )}
        </div>
        <div
          className="cr__modalBox--addCategory--content--selectorContainer cursor"
          onClick={() => {
            setToggleSelect((toggleSelect) => !toggleSelect);
            setNavigationLevel(1);
          }}
        >
          <div>
            <span
              className={` cr__text--paragraph ${
                selectedInterest === initialValues
                  ? 'cr__textColor--colorGray300'
                  : 'cr__textColor--colorDark300'
              }`}
            >
              {selectedInterest === initialValues &&
                !productInitialValues?.interest[index]?.display &&
                'Elije un interés...'}

              {selectedInterest === initialValues &&
                productInitialValues?.interest[index]?.display &&
                productInitialValues?.interest[index]?.display}

              {selectedInterest?.name !== null && selectedInterest?.name}
            </span>
          </div>

          <div className="cr__modalBox--addCategory--content--selectorContainer--img">
            <img
              src={toggleSelect ? IconDelete : ArrowDown}
              alt={toggleSelect ? 'Cerrar Selector' : 'Abrir Selector'}
              className={toggleSelect ? 'close' : 'open'}
            />
          </div>
        </div>

        {toggleSelect && (
          <div
            className="cr__modalBox--addCategory--content--selector"
            style={{ height: dropdownHeight }}
            ref={ref}
          >
            <CSSTransition
              in={toggleSelect}
              timeout={300}
              unmountOnExit
              onEnter={calcDropdownHeight}
            >
              <div>
                {/* //Level1 */}
                {navigationLevel === 1 && (
                  <React.Fragment>
                    {marketInterests?.map(({ name, slug, children }) => (
                      <div
                        key={slug}
                        className="cr__modalBox--addCategory--content--level1 cursor"
                        onClick={() => {
                          setSelectedInterest({ name, slug, children });
                          children.length === 0 && setToggleSelect(false);
                          setSelectedInterestTree(
                            createBreadcrumbsTree(marketInterests, slug).path,
                          );
                          setNavigationLevel(2);
                          setLevel2({ name, slug, children });
                          setFatherInterest({
                            ...fatherInterest,
                            level2: name,
                          });
                        }}
                      >
                        <div className="cr__text--paragraph cr__textColor--colorDark300 cursor">
                          <span>{name}</span>
                        </div>

                        {children?.length >= 1 && (
                          <div className="cr__modalBox--addCategory--content--level1--img cursor">
                            <img src={IconNext} alt="Siguiente Nivel" />
                          </div>
                        )}
                      </div>
                    ))}
                  </React.Fragment>
                )}
                {/* //Level2 */}
                {navigationLevel === 2 && (
                  <React.Fragment>
                    <div
                      className="cr__modalBox--addCategory--content--fatherCategory cursor"
                      onClick={() => {
                        setNavigationLevel(1);
                      }}
                    >
                      <div className="cr__modalBox--addCategory--content--fatherCategory--img">
                        <img src={IconNext} alt="Siguiente Nivel" />
                      </div>
                      <div className="cr__text--subtitle3 cr__textColor--colorDark300">
                        <span>{fatherInterest?.level2}</span>
                      </div>
                    </div>
                    {level2?.children?.map(({ name, slug, children, parent }) => (
                      <div
                        key={slug}
                        className="cr__modalBox--addCategory--content--level2 cursor"
                        onClick={() => {
                          setSelectedInterest({ name, slug, children, parent });
                          children.length === 0 && setToggleSelect(false);
                          setSelectedInterestTree(
                            createBreadcrumbsTree(marketInterests, slug).path,
                          );
                          setNavigationLevel(3);
                          setLevel3({ name, slug, children });
                          setFatherInterest({
                            ...fatherInterest,
                            level3: name,
                          });
                        }}
                      >
                        <div className="cr__text--paragraph cr__textColor--colorDark300 cursor">
                          <span>{name}</span>
                        </div>
                        {children?.length >= 1 && (
                          <div className="cr__modalBox--addCategory--content--level2--img cursor">
                            <img src={IconNext} alt="Siguiente Nivel" />
                          </div>
                        )}
                      </div>
                    ))}
                  </React.Fragment>
                )}
                {/* //Level3 */}
                {navigationLevel === 3 && (
                  <React.Fragment>
                    <div
                      className="cr__modalBox--addCategory--content--fatherCategory cursor"
                      onClick={() => {
                        setNavigationLevel(2);
                      }}
                    >
                      <div className="cr__modalBox--addCategory--content--fatherCategory--img ">
                        <img src={IconNext} alt="Siguiente Nivel" />
                      </div>
                      <div className="cr__text--subtitle3 cr__textColor--colorDark300 ">
                        <span>{fatherInterest?.level3}</span>
                      </div>
                    </div>

                    {level3?.children?.map(({ name, slug, children, parent }) => (
                      <div
                        key={slug}
                        className="cr__modalBox--addCategory--content--level3 cursor"
                        onClick={() => {
                          setSelectedInterest({ name, slug, children, parent });
                          children.length === 0 && setToggleSelect(false);
                          setSelectedInterestyTree(
                            createBreadcrumbsTree(marketInterests, slug).path,
                          );
                          setNavigationLevel(4);
                          setLevel4({ name, slug, children });
                          setFatherInterest({
                            ...fatherInterest,
                            level4: name,
                          });
                        }}
                      >
                        <div className="cr__text--paragraph cr__textColor--colorDark300 cursor">
                          <span>{name}</span>
                        </div>
                        {children?.length >= 1 && (
                          <div className="cr__modalBox--addCategory--content--level3--img cursor">
                            <img src={IconNext} alt="Siguiente Nivel" />
                          </div>
                        )}
                      </div>
                    ))}
                  </React.Fragment>
                )}
                {/* //Level4 */}
                {navigationLevel === 4 && (
                  <React.Fragment>
                    <div
                      className="cr__modalBox--addCategory--content--fatherCategory cursor"
                      onClick={() => {
                        setNavigationLevel(3);
                      }}
                    >
                      <div className="cr__modalBox--addCategory--content--fatherCategory--img">
                        <img src={IconNext} alt="Siguiente Nivel" />
                      </div>
                      <div className="cr__text--subtitle3 cr__textColor--colorDark300">
                        <span>{fatherInterest?.level4}</span>
                      </div>
                    </div>

                    {level4?.children?.map(({ name, slug, children, parent }) => (
                      <div
                        key={slug}
                        className="cr__modalBox--addCategory--content--level4 cursor"
                      >
                        <div
                          className="cr__text--paragraph cr__textColor--colorDark300 cursor"
                          onClick={() => {
                            setSelectedInterest({ name, slug, children, parent });
                            children.length === 0 && setToggleSelect(false);
                            setSelectedInterestTree(
                              createBreadcrumbsTree(marketInterests, slug).path,
                            );
                          }}
                        >
                          <span>{name}</span>
                        </div>
                      </div>
                    ))}
                  </React.Fragment>
                )}
              </div>
            </CSSTransition>
          </div>
        )}
      </div>
      <div className="cr__modalBox--addCategory--buttons">
        <a
          className="cr__text--paragraph cursor"
          onClick={() => {
            closeModalBox();
            onEditingState();
          }}
        >
          Cancelar
        </a>
        <button
          className={`cr__text--paragraph cr__modalBox--addCategory--buttons--save cursor ${
            selectedInterest === initialValues ? 'disable' : 'button'
          }`}
          onClick={() => {
            closeModalBox();
            {
              editingInterest
                ? onEditInterest(index, selectedInterest)
                : onSubmitInterest(selectedInterest?.slug);
            }
            onGTMTracking(selectedInterest?.slug);
          }}
          disabled={selectedInterest === initialValues}
        >
          Guardar
        </button>
      </div>
    </div>
  );
}
