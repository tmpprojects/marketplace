import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Field, getFormValues } from 'redux-form';
import { Link } from 'react-router-dom';
import { default as reduxActions } from 'redux-form/lib/actions';

import { getProductDetailForm } from '../../../Reducers/mystore.reducer';
import { Tooltip } from '../../../Utils/Tooltip';
import { SHIPPING_METHODS } from '../../../Constants';
import { statusWindowActions, myStoreActions } from '../../../Actions';
import { IconPreloader } from '../../../Utils/Preloaders';

const FORM_NAME = 'myStoreEditProduct_form';
const { change } = reduxActions;

class ProductShippingForm extends React.Component {
  /*
   * Constructor Function
   * @param {object} props | React Component Properties
   */
  state = {
    shippingMethods: this?.props.initialValues.physical_properties.shipping,
  };

  componentWillReceiveProps(nextProps) {
    if (this.props?.productEditValues !== nextProps?.productEditValues) {
      this.setState({
        shippingMethods: nextProps?.productEditValues?.physical_properties?.shipping,
      });
    }
  }

  /**
   * Shipping Methods
   * Toggles selected shipping method.
   * @param {method} method | Selected method objects
   */
  onShippingMethodChange = (method) => {
    let { shippingMethods } = this.state;

    const methodFoundIndex = shippingMethods?.findIndex(
      (a) => a.slug === method.slug,
    );

    // Toogle shipping method.
    if (methodFoundIndex === -1) {
      shippingMethods = [...shippingMethods, method];
    } else {
      shippingMethods = shippingMethods?.filter((a) => a.slug !== method.slug);
    }

    // Update redux form values.
    this.props.change(FORM_NAME, 'physical_properties.shipping', shippingMethods);

    // Update state
    this.setState({
      shippingMethods,
    });
  };

  switchShippingMethodsUIButtons = () => {
    const { productEditValues, initialValues, myStore } = this.props;

    const { shippingMethods = [] } = this.state;

    return myStore?.available_shipping_methods_in_pickup_address?.map((method) => {
      const activeMethod =
        shippingMethods?.find((a) => a.slug === method.slug) || '';

      return (
        <li key={method.slug} className="items">
          {method.name}
          <label htmlFor={method.slug} className="switch">
            <Field
              component="input"
              id={method.slug}
              type="checkbox"
              name={method.slug}
              onChange={(e) => this.onShippingMethodChange(method)}
              checked={activeMethod}
              disabled={productEditValues?.updating || initialValues?.loading}
              value={activeMethod || ''}
            />
            <span className="slider round" />
          </label>
        </li>
      );
    });
  };

  /**
   * Filter ExpressMethods
   */
  renderExpressMethods = () => {
    const methods = this.switchShippingMethodsUIButtons();

    const expressMethod = methods.filter(
      (method) => method.key.indexOf('express') > -1,
    );
    return (
      <div className="radio-icons">
        <div className="radio-icons__container">
          <div className="shipping-method_container">
            <div className="dots">
              <p className="method express">
                Express&nbsp;
                <span className="info">
                  S&oacute;lo Ciudad de M&eacute;xico, Guadalajara, Monterrey y
                  Morelia.
                </span>
              </p>
            </div>
          </div>
          <ul className="options-container">
            {expressMethod}
            {expressMethod.length <= 0 && (
              <li>
                <p>
                  En la direcci&oacute;n de recolecci&oacute;n no hay m&eacute;todos
                  de env&iacute;o disponibles.
                </p>{' '}
                <br />
                <Link
                  to="/my-store/settings/addresses"
                  className="cr__textColor--colorMain300"
                >
                  Cambiar direcci&oacute;n
                </Link>
              </li>
            )}
          </ul>
        </div>
      </div>
    );
  };

  /**
   * Message in Standard Method
   */
  renderStandardMethod = () => {
    return (
      <div className="radio-icons standard">
        <div className="radio-icons__container">
          <div className="shipping-method_container">
            <p className="method standard">
              Standard
              <span className="info">Interior de la Rep&uacute;blica Mexicana</span>
              <span
                className="info"
                style={{ fontSize: '0.675em', lineHeight: '1.4em' }}
              >
                Contacta al staff de Canasta Rosa.
              </span>
            </p>
          </div>
          <div className="note_container">
            <p className="note download">
              Guías de empaque y recomendación de Canasta Rosa.
              <a
                href="https://canastarosa.s3.us-east-2.amazonaws.com/download/download-guides/guia-envio-nacional.pdf"
                target="_blank"
                rel="noopener noreferrer"
              >
                Descargar PDF
              </a>
            </p>
          </div>
        </div>
      </div>
    );
  };

  /**
   * render()
   */
  render() {
    // const shippingMethods = props?.initialValues?.physical_properties?.shipping;

    const { productEditValues, myStore } = this.props;

    if (!productEditValues) {
      return null;
    }
    return (
      <div className="ui-detailed-block">
        <div className="ui-detailed-block__detail">
          <h3 className="cr__text--paragraph cr__textColor--colorDark300 heading2">
            Envíos
            {/* <Tooltip message="" /> */}
          </h3>
          <div className="form__help">
            <p className="cr__text--paragraph cr__textColor--colorDark100">
              Para seleccionar el mejor método de envío,&nbsp; necesitamos saber el
              tiempo de vida de tu producto y características que mejor describan tu
              producto.
            </p>
          </div>
        </div>

        <div className=" form--annotated ui-detailed-block__content shipping">
          <fieldset>
            <div className="form__data productPerishable">
              <div className="field_details">
                <label className="title" htmlFor="resume_txt">
                  Tipo de Producto
                </label>
              </div>

              <div className="perishable">
                <div className="radio-icons">
                  <div className="radio-icons__container">
                    <Field
                      name="physical_properties.life_time"
                      id="type_perishable_chk"
                      component="input"
                      type="radio"
                      value="1"
                      normalize={(value) => value.toString()}
                      className="dots"
                    />
                    <label htmlFor="type_perishable_chk" className="dot label">
                      Perecedero
                    </label>
                  </div>

                  <div className="radio-icons__container">
                    <Field
                      name="physical_properties.life_time"
                      id="type_noPerishable_chk"
                      component="input"
                      type="radio"
                      value="0"
                      normalize={(value) => value.toString()}
                      className="dots"
                    />
                    <label htmlFor="type_noPerishable_chk" className="dot label">
                      No Perecedero
                    </label>
                  </div>
                </div>
                {/*
                                <div className="fragile_container">
                                    <div 
                                        className={`button-option ${
                                            productEditValues.physical_properties.is_fragile === true 
                                                ? 'active' 
                                                : ''
                                        }`}
                                    >
                                        <Field
                                            id="fragile"
                                            name="physical_properties.is_fragile"
                                            className="button-tag"
                                            component="input"
                                            type="checkbox"
                                        />
                                        <label 
                                            htmlFor="fragile" 
                                            className="fragile"
                                        >Fr&aacute;gil </label>
                                    </div>
                                </div>
                                */}
              </div>
            </div>

            <div className="form__data productSizes">
              <div className="field_details">
                <label className="title">Peso del producto</label>
              </div>

              <div className="shipping-sizes">
                <div className="form form__data weight">
                  <label htmlFor="weight">Peso</label>
                  <Field
                    id="weight"
                    name="physical_properties.weight"
                    component="input"
                    placeholder="0"
                    className="field_weight"
                    type="number"
                    min="0"
                    max="99"
                  />
                  <span>kg.</span>
                </div>

                <div className="radio-icons">
                  <div className="radio-icons__container shipping-sizes__sm">
                    <Field
                      name="physical_properties.size"
                      id="type_xs_chk"
                      component="input"
                      type="radio"
                      value="1"
                      checked={productEditValues.physical_properties.size === '1'}
                      className="dots"
                    />
                    <label htmlFor="type_xs_chk" className="dot label">
                      Pequeño <span className="info">Hasta 5kg</span>
                    </label>
                  </div>

                  <div className="radio-icons__container shipping-sizes__m">
                    <Field
                      name="physical_properties.size"
                      id="type_sm_chk"
                      component="input"
                      type="radio"
                      value="2"
                      checked={productEditValues.physical_properties.size === '2'}
                      className="dots"
                    />
                    <label htmlFor="type_sm_chk" className="dot label">
                      Mediano&nbsp;
                      <span className="info">Hasta 15kg</span>
                    </label>
                  </div>

                  <div className="radio-icons__container shipping-sizes__l">
                    <Field
                      name="physical_properties.size"
                      id="type_m_chk"
                      component="input"
                      type="radio"
                      value="3"
                      checked={productEditValues.physical_properties.size === '3'}
                      className="dots"
                    />
                    <label htmlFor="type_m_chk" className="dot label">
                      Grande <span className="info">Hasta 30kg</span>
                    </label>
                  </div>
                </div>
              </div>
            </div>

            {myStore?.pickup_address !== null ? (
              <div className="form__data productShipping">
                <div className="field_details">
                  <label className="title">M&eacute;todos de Env&iacute;o</label>
                </div>

                <div className="shipping-method">
                  {this.renderExpressMethods()}
                  {productEditValues?.physical_obligations?.ship_nationwide
                    ? this.renderStandardMethod()
                    : null}
                </div>
              </div>
            ) : (
              <div className="form__data productShipping">
                <div className="field_details">
                  <label className="title">M&eacute;todos de Env&iacute;o</label>
                </div>
                <div className="cr__googleforms">
                  <p>
                    Para seleccionar un m&eacute;todo de env&iacute;o es necesario
                    tener una dirección de recoleccion
                  </p>
                  <Link
                    to="/my-store/settings/addresses"
                    className="cr__text--subtitle3 shippingForm"
                  >
                    Agregar direcci&oacute;n
                  </Link>
                </div>
              </div>
            )}
          </fieldset>
        </div>
      </div>
    );
  }
}

// Add Redux state and actions to component´s props
function mapStateToProps(state) {
  const { app } = state;
  const productDetail = getProductDetailForm(state);
  // Return Porps
  return {
    productEditValues: getFormValues(FORM_NAME)(state),
    initialValues: productDetail,
    platformShippingMethods: app.shippingMethods,
  };
}
function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      change,
      openStatusWindow: statusWindowActions.open,
      updateProduct: myStoreActions.updateProduct,
    },
    dispatch,
  );
}

// Export Component
ProductShippingForm = connect(
  mapStateToProps,
  mapDispatchToProps,
)(ProductShippingForm);
export default ProductShippingForm;
