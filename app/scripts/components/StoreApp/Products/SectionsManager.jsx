import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Field, reduxForm, SubmissionError } from 'redux-form';
import PropTypes from 'prop-types';

import {
  modalBoxActions,
  myStoreActions,
  statusWindowActions,
} from '../../../Actions';
import { InputField } from '../../../Utils/forms/formComponents';
import withCounter from '../../hocs/withCounter';

const InputWithCounter = withCounter(InputField);

const defaultError = 'Error. Por favor, intenta más tarde. ';
/*------------------------------------------
SECTIONS MANAGER CLASS
------------------------------------------*/
class SectionsManager extends React.Component {
  // PropTypes / DefaultProps
  static defaultProps = {
    attribute_types: [],
    max_attributes: 5,
  };
  static propTypes = {
    attribute_types: PropTypes.array.isRequired,
    max_attributes: PropTypes.number.isRequired,
  };

  /*
   * Constructor Function
   * @param {object} props React Component Properties
   */
  constructor(props) {
    super(props);

    // Bind Scope to class methods
    this.onSubmit = this.onSubmit.bind(this);
    this.onCloseWindow = this.onCloseWindow.bind(this);
  }

  /*
   * On Finish Editing the manager
   * Propagates the state of the manager to the parent component
   */
  onSubmit(values) {
    const { sectionName } = values;
    this.props
      .dispatch(myStoreActions.addMyStoreSection(sectionName))
      .then((response) => {
        this.props.closeWindow(response.data.slug);
        this.props.openStatusWindow({
          type: 'success',
          message: 'La sección se agregó correctamente',
        });
      })
      .catch((error) => {
        const message = JSON.parse(event?.currentTarget?.response);
        this.props.openStatusWindow({
          type: 'error',
          message: message?.name ? message?.name : defaultError,
        });
        this.props.closeWindow();
      });
  }

  /*
   * On Close Manager
   * Close Attribute Manager
   */
  onCloseWindow() {
    this.props.closeWindow();
  }

  /*
   * React Component Life Cycle Functions
   */
  render() {
    const { handleSubmit } = this.props;

    return (
      <div className="modal-box-form storeApp-modal">
        <h4 className="storeApp-modal__title">Crea una nueva Sección</h4>

        <form
          className="storeApp-modal__content modal-box-form__form"
          onSubmit={handleSubmit(this.onSubmit)}
        >
          <Field
            component={InputWithCounter}
            maxLength={50}
            autoFocus
            id="sectionName"
            name="sectionName"
            className="sectionName"
            label="Título de la Sección"
          />

          {
            // Save & Cancel Buttons
            <div className="storeApp-modal__navBottom">
              <button
                type="button"
                onClick={this.onCloseWindow}
                className="storeApp-modal__button storeApp-modal__button--cancel"
              >
                Cancelar
              </button>
              <button className="storeApp-modal__button">Guardar</button>
            </div>
          }
        </form>
      </div>
    );
  }
}

// Wrap component within reduxForm
SectionsManager = reduxForm({
  form: 'myStoreSectionsAdd_form',
})(SectionsManager);

function mapDispatchToProps(dispatch) {
  const { open } = statusWindowActions;
  return bindActionCreators(
    {
      openStatusWindow: open,
    },
    dispatch,
  );
}

SectionsManager = connect(null, mapDispatchToProps)(SectionsManager);

// Export Component
export default SectionsManager;
