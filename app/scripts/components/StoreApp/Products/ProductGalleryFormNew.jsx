import React, { useState } from 'react';
import { connect } from 'react-redux';
import { reduxForm, getFormValues } from 'redux-form';
import { bindActionCreators } from 'redux';
import { arrayMove } from 'react-sortable-hoc';

import { statusWindowActions } from '../../../Actions';
import { IconPreloader } from '../../../Utils/Preloaders';
import { getProductDetailForm } from '../../../Reducers/mystore.reducer';
import GalleryContainer from '../Utils/GalleryContainer';

const MAX_IMAGES = 6;

function ProductGalleryFormNew(props) {
  const [over, setOver] = useState(false);

  const [uploading, setUploading] = useState(true);

  const onUploadMultipleImages = (e, type) => {
    e.persist();
    e.preventDefault();
    e.stopPropagation();

    // Extra validation (uploading) to being able to drag and drop on
    //  the whole drop area and also being able to drag and move all images.
    if (uploading) {
      e.stopPropagation();
      //
      let filesArray =
        type === 'onDrag'
          ? Array.from(e?.dataTransfer?.files)
          : Array.from(e?.target?.files) || [];

      // Verfication is a img accepted format.
      filesArray?.every((item) => {
        if (!item?.name?.toLowerCase()?.match(/\.(jpg|jpeg|png|gif)$/)) {
          filesArray = [];
          setOver(false);
          props.openStatusWindow({
            type: 'error',
            message: 'Estos son los únicos formatos aceptados .png, .jpg y .gif ',
          });
          return false;
        }
      });
      // Verification they are uploading less images than the MAX_IMAGES per product.
      if (filesArray?.length + props.initialValues.photos.length > MAX_IMAGES) {
        filesArray = [];
        props.openStatusWindow({
          type: 'error',
          message: `Solo puedes incluir ${MAX_IMAGES} imágenes por producto.`,
        });
        return false;
      }
      // Verification that all images are under 8MB.
      filesArray?.forEach(({ size }) => {
        if (size / 1024 / 1024 > 8) {
          filesArray = type === 'onDrag' ? [] : '';
          props.openStatusWindow({
            type: 'error',
            message: 'Por favor, elige imagenes con un peso menor a 8MB.',
          });
          return false;
        }
      });
      // Once each image passed all verifications --> send to Backend one by one.
      filesArray?.forEach((item, i) => {
        const formData = new FormData();
        formData.append('photo', item);
        formData.append('slug', props.productEditValues.slug);
        props.addImage(formData);
      });
    }
  };

  const onSetOverTrue = (e) => {
    e.preventDefault();
    e.stopPropagation();
    setOver(true);
  };
  const onSetOverFalse = (e) => {
    e.preventDefault();
    e.stopPropagation();
    setOver(false);
  };

  const gallery =
    props?.initialValues?.photos?.map((item) => ({
      ...item,
      src: item.photo,
      slug: props.product.slug,
      name: props.product.name,
    })) || [];

  /**
   * onSortEnd()
   * Updates state to new gallery sorting order
   * and updates positions through API
   * @param {object} sortingProps | Image new sorting props
   */
  const onSortEnd = (sortingProps) => {
    // Avoiding to call drag and drop function (onUploadMultipleImages)
    setUploading(false);
    setOver(false);
    // end
    const { oldIndex, newIndex } = sortingProps;
    props.updateGalleryOrder({
      ...props?.initialValues?.photos[oldIndex],
      order: newIndex,
    });
    return;
  };
  return (
    <div className="ui-detailed-block gallery_module">
      <div className="ui-detailed-block__detail">
        <h3 className="cr__text--paragraph cr__textColor--colorDark300 heading2">
          Galer&iacute;a
          {/* <Tooltip message="" /> */}
        </h3>
        <div className="form__help">
          <p className="cr__text--paragraph cr__textColor--colorDark100">
            Formatos .png, .jpg y .gif a un tamaño máximo de 8MB.
          </p>
        </div>
      </div>

      <div
        className={`form--annotated ui-detailed-block__content newfileUpload ${
          over ? 'activeDrag' : ''
        }`}
        onDrop={(e) => {
          setUploading(true);
          onUploadMultipleImages(e, 'onDrag');
          onSetOverFalse(e);
        }}
        onDragOver={(e) => {
          e.persist();
          onSetOverTrue(e);
        }}
        // onDragEnter={(e) => {
        //   onSetOverTrue(e);
        // }}
        onDragLeave={(e) => {
          e.persist();
          // setTimeout(() => onSetOverFalse(e), 300);
          onSetOverFalse(e);
        }}
        // onDragEnd={(e) => {
        //   onSetOverFalse(e);
        // }}
      >
        {props?.activeProduct?.loading ? (
          <div className="cr__loaderContainer">
            <IconPreloader />
            <p className="cr__text--colorDark300 cr__text--paragraph">
              Trabajando en tu galer&iacute;a
            </p>
            <span className="cr__text--colorDark100 cr__text--caption">
              Esto puede tomar un momento.
            </span>
          </div>
        ) : (
          <div
            className="form__data-fileUpload"
            onDrop={(e) => {
              setUploading(true);
              onUploadMultipleImages(e, 'onDrag');
              onSetOverFalse(e);
            }}
          >
            <input
              type="file"
              multiple
              name="file"
              id="gallery_picture"
              className="upload_input"
              accept="image/x-png,image/gif,image/jpeg,image/jpg"
              onChange={(e) => {
                setUploading(true);
                onUploadMultipleImages(e, 'onInput');
              }}
            />
            <label
              htmlFor="gallery_picture"
              className="cr__text--paragraph cr__text--colorDark300"
            >
              Agregar imagen
            </label>
            <span
              className={`cr__text--caption ${
                over ? 'cr__textColor--colorMain300' : 'cr__textColor--colorDark100'
              }`}
            >
              Arrastra tus im&aacute;genes para subirlas
            </span>
            <div className="cr__galleryContainer">
              <GalleryContainer
                items={gallery || []}
                className="gallery"
                onSortEnd={onSortEnd}
                onImageDelete={props.deleteImage}
                setUploading={setUploading}
              />
            </div>
          </div>
        )}
      </div>
    </div>
  );
}
// Wrap component within reduxForm
const formName = 'myStoreEditProduct_form';
ProductGalleryFormNew = reduxForm({
  form: formName,
  destroyOnUnmount: false, // <---- Preserve data. Dont destroy form on unmount
  enableReinitialize: true, // <---- Allow updates when 'initValues' prop change
  keepDirtyOnReinitialize: true, // <---- Prevent 'dirty' fields to update
  updateUnregisteredFields: true, // <---- Update unregistered fields.
  forceUnregisterOnUnmount: true, // <---- Unregister fields on unmount,
})(ProductGalleryFormNew);

// Add Redux state and actions to component´s props
function mapStateToProps(state) {
  const { myStore } = state;

  return {
    product: getProductDetailForm(state),
    initialValues: getProductDetailForm(state),
    productEditValues: getFormValues(formName)(state),
    activeProduct: myStore.active_product,
  };
}
function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      openStatusWindow: statusWindowActions.open,
    },
    dispatch,
  );
}
// Export Component
ProductGalleryFormNew = connect(
  mapStateToProps,
  mapDispatchToProps,
)(ProductGalleryFormNew);
export default ProductGalleryFormNew;
