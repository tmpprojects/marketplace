import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { myStoreActions, statusWindowActions } from '../../../../Actions';
import './tableItemProduct.scss';
import Placeholder from '../../../../../images/placeholder/placeholder--productPage.jpg';

const errorImage = (e) => {
  e.target.src = Placeholder;
};

function TableItemProduct(props) {
  const deleteProduct = (slug) => {
    const confirmation = confirm(
      '¿Estás seguro de querer eliminar este producto?. La operación no podrá deshacerse.',
    );
    if (confirmation) {
      // Delete Product Image
      return props
        .deleteProduct(slug)
        .then((response) => {
          props.openStatusWindow({
            type: 'success',
            message: 'Eliminamos el producto correctamente.',
          });
          props.fetchProductList();
        })
        .catch((error) => {
          props.openStatusWindow({
            type: 'error',
            message: 'Error. Por favor, intenta de nuevo.',
          });
        });
    }
  };

  const { items } = props;
  return (
    <div className="cr__tableProduct">
      <table>
        <thead>
          <tr>
            <th className="cr__text--subtitle3 cr__textColor--colorDark300 product">
              Producto
            </th>
            <th className="cr__text--subtitle3 cr__textColor--colorDark300 price">
              Precio
            </th>
            <th className="cr__text--subtitle3 cr__textColor--colorDark300 stocks">
              Inventario
            </th>
            <th className="cr__text--subtitle3 cr__textColor--colorDark300 category">
              Categor&iacute;a
            </th>
            <th className="cr__text--subtitle3 cr__textColor--colorDark300 status">
              Publicado
            </th>
            <th className="cr__text--subtitle3 cr__textColor--colorDark300 shippingMethod ">
              Env&iacute;o
            </th>
            <th className="cr__text--subtitle3 cr__textColor--colorDark300 delete">
              Eliminar
            </th>
          </tr>
        </thead>

        <tbody>
          {items?.map((item) => (
            <tr key={item?.slug}>
              <td className="product">
                <Link to={`/my-store/products/edit/${item.slug}`}>
                  <img
                    src={item?.photo?.small}
                    alt={item?.name}
                    onError={errorImage}
                    loading="lazy"
                  />
                  <span className="cr__text--paragraph cr__textColor--colorDark300">
                    {item?.name}
                  </span>
                </Link>
              </td>

              <td className="cr__text--paragraph cr__textColor--colorDark300 price">
                ${item?.price + ' '}MXN
              </td>

              {item?.physical_properties?.is_available?.value === 1 ? (
                <td className="stocks">
                  {item?.low_stock?.is_low_stock ? (
                    <span className="cr__textColor--colorRed300">
                      Variaci&oacute;n con poco inventario
                    </span>
                  ) : (
                    <>
                      <span
                        className={`cr__text--paragraph ${
                          item?.stock_total <= 5 || item?.quantity <= 0
                            ? 'cr__textColor--colorRed300'
                            : 'cr__textColor--colorDark300'
                        }`}
                      >
                        {item?.stock_total ? item?.stock_total : item?.quantity}{' '}
                        {item?.stock_total === 1 || item?.quantity === 1
                          ? 'pieza'
                          : 'piezas'}
                      </span>
                      <br />
                      {item?.num_attributes > 0 && (
                        <span className="cr__text--caption numAttributes">
                          Entre {item?.num_attributes}
                          {item?.num_attributes === 1
                            ? ' variación'
                            : ' variaciones'}
                        </span>
                      )}
                    </>
                  )}
                </td>
              ) : item?.physical_properties?.is_available?.value === 2 ? (
                <td className="stocks">
                  <span className="cr__text--paragraph cr__textColor--colorDark300">
                    Elaborado bajo pedido
                  </span>
                </td>
              ) : (
                <td className="stocks">
                  <span className="cr__text--paragraph cr__textColor--colorDark300">
                    Sin definirse
                  </span>
                </td>
              )}

              {/* Todo: Change to new variable */}
              <td className="cr__text--paragraph cr__textColor--colorDark300 category">
                {item?.new_category?.length > 0
                  ? item?.new_category[0]?.display
                  : 'Sin Categoría'}
              </td>
              <td className="cr__text--paragraph cr__textColor--colorDark300 status">
                {item?.status?.display_name?.replace(
                  'Público (Visible para todos)',
                  'Publicado',
                )}
              </td>
              <td className="cr__text--paragraph cr__textColor--colorDark300 shippingMethod">
                {item?.physical_obligations?.ship_nationwide
                  ? 'Nacional'
                  : item?.physical_properties?.shipping?.length > 0
                  ? 'Express'
                  : 'Sin Método de Envío'}
              </td>
              <td>
                <i
                  className="cr__icon--trash cr__text--paragraph delete"
                  onClick={() => deleteProduct(item.slug)}
                />
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
}

// Add Redux state and actions to component´s props
function mapStateToProps(state) {
  return {};
}
function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      deleteProduct: myStoreActions.deleteProduct,
      openStatusWindow: statusWindowActions.open,
      fetchProductList: myStoreActions.fetchProductList,
    },
    dispatch,
  );
}

// Export Component
export default connect(mapStateToProps, mapDispatchToProps)(TableItemProduct);
