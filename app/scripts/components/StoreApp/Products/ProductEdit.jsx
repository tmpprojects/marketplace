import React from 'react';
import { NavLink } from 'react-router-dom';
import { connect } from 'react-redux';
import { renderRoutes } from 'react-router-config';
import { bindActionCreators } from 'redux';
import { Field, reduxForm, SubmissionError } from 'redux-form';
import { default as reduxActions } from 'redux-form/lib/actions';
import PropTypes from 'prop-types';
import Sentry from '../../../Utils/Sentry';

import { getProductDetailForm } from '../../../Reducers/mystore.reducer';
import { SwitchCheckBox } from '../../../Utils/forms/formComponents';
import { formatNumberToPriceMyStore } from '../../../Utils/normalizePrice';
import { myStoreActions, statusWindowActions, appActions } from '../../../Actions';
import { IconPreloader } from '../../../Utils/Preloaders';

const { submit, destroy } = reduxActions;

/*///////////////////////////////////////////////////
    BottomMenu Component
///////////////////////////////////////////////////*/
let BottomMenu = (props) => {
  const {
    product,
    handleSubmit,
    openStatusWindow,
    activeSwitch,
    updateSwitch,
  } = props;

  if (!product) return null;

  const onSubmitPublish = () => {
    if (
      product?.physical_properties?.shipping?.length > 0 ||
      product?.physical_obligations?.ship_nationwide
    ) {
      props.dispatch(submit('myStoreEditProduct_form'));
    } else {
      props.openStatusWindow({
        type: 'error',
        message: 'Es necesario tener al menos un método de envío activo.',
      });
      unClicked();
    }
  };

  const onSubmitSave = () => {
    props.dispatch(submit('myStoreEditProduct_form'));
  };

  const unClicked = () => {
    updateSwitch(true);
    document.getElementById('status_chk').click();
    updateSwitch(false);
  };

  return (
    <form className="controlNavBar">
      <ul className="controlNavBar__navigation">
        {/* <li className="controlNavBar__item" onClick={previewProduct}>Vista Previa</li> */}
        <li className="controlNavBar__item publish">
          <span className="status">
            {product.status ? 'Publicado' : 'No Publicado'}
          </span>
          <div className="switch">
            <Field
              component={SwitchCheckBox}
              id="status_chk"
              type="checkbox"
              name="status"
              style="pink"
              label={product.status ? 'Publicado' : 'No Publicado'}
              onChange={() => {
                product?.updating || activeSwitch
                  ? unClicked
                  : setTimeout(onSubmitPublish, 500);
              }}
            />
          </div>
        </li>
        <li
          data-test-name="save-button"
          className="controlNavBar__item draft"
          onClick={!product?.updating ? onSubmitSave : undefined}
        >
          Guardar{' '}
        </li>
      </ul>
    </form>
  );
};

BottomMenu = reduxForm({
  form: 'myStoreEditProduct_form',
  destroyOnUnmount: false, // <------ preserve form data
  forceUnregisterOnUnmount: true, // <------ unregister fields on unmount
})(BottomMenu);
BottomMenu = connect(({ myStore }) => ({
  product: myStore.active_product,
  initialValues: myStore.active_product,
}))(BottomMenu);

/*///////////////////////////////////////////////////
    PRODUCT EDIT COMPONENT
///////////////////////////////////////////////////*/
class ProductEdit extends React.Component {
  state = {
    activeSwitch: false,
    errorForm: false,
    errorsWordsList: {},
  };
  /*
   * React Component Life Cycle Functions
   */
  componentWillMount() {
    this.props.dispatch(destroy('myStoreEditProduct_form'));
  }
  componentDidMount() {
    this.props.fetchProduct(this.props.match.params.slug);
    this.props.fetchAttributeTypes();
    this.props.fetchCategories();
    this.props.shippingMethods();
    this.props.fetchInterests();
  }
  componentWillReceiveProps(nextProps) {
    if (this.props.match.params.slug !== nextProps.match.params.slug) {
      this.props.fetchProduct(nextProps.match.params.slug);
    }
  }

  /**
   * updateGalleryOrder()
   * @param {array} gallery | Gallery of images with new order positions
   */
  updateGalleryOrder = (photo) => {
    this.props
      .updateProductPhoto(this.props.product.slug, photo.file, photo.order)
      .then(() => this.props.fetchProduct(this.props.match.params.slug))
      .then(() =>
        this.props.openStatusWindow({
          type: 'success',
          message: 'La galería se actualizo con éxito',
        }),
      )
      .catch(() => {
        this.props.openStatusWindow({
          type: 'error',
          message: 'Erro. Por favor, intenta de nuevo.',
        });
      });
  };

  /**
   * addImage()
   * Uploads an image to the product gallery
   * @param {object} productData
   */
  addImage = (productData) => {
    // Update Product
    return this.props
      .addProductPhoto(productData)
      .then((response) => {
        this.props.openStatusWindow({
          type: 'success',
          message: 'Información actualizada.',
        });
      })
      .catch((error) => {
        this.props.openStatusWindow({
          type: 'error',
          message: 'Error. Por favor, intenta de nuevo.',
        });
        throw new SubmissionError({
          _error:
            'Ocurrió un problema al actualizar tu información. Por favor, intenta de nuevo.',
        });
      });
  };

  /**
   * deleteImage()
   * Deletes an image from a product gallery.
   * @param {object} productData
   */
  deleteImage = (productData) => {
    // Confirm Product Image removal
    if (
      confirm(
        '¿Estás seguro de querer eliminar esta imagen?\nLa acción no podrá deshacerse.',
      )
    ) {
      return this.props
        .deleteProductPhoto(productData)
        .then((response) => {
          this.props.openStatusWindow({
            type: 'success',
            message: 'Información actualizada.',
          });
        })
        .catch((error) => {
          this.props.openStatusWindow({
            type: 'error',
            message: 'Error. Por favor, intenta de nuevo.',
          });
          throw new SubmissionError({
            _error: `Ocurrió un problema al actualizar tu información.{' '}
                            Por favor, intenta de nuevo.`,
          });
        });
    }
  };

  /**
   * updateProduct()
   * Updates product information via API
   * @params {object} productValues | Redux form values
   */
  updateProduct = (productValues) => {
    const status = productValues.status ? 'public' : 'draft';
    // Format data for API
    const formData = {
      ...productValues,
      status,
      product_type: 'physical',
      slug: this.props.product.slug || productValues?.slug,
      price: formatNumberToPriceMyStore(productValues?.price) || '1.00',
      quantity: productValues?.quantity ? productValues?.quantity : 10,
      // category: this.props.category.map(category => category.value),
      new_category:
        productValues?.new_category?.length > 0
          ? [productValues?.new_category[0].slug]
          : [],
      section: productValues.section.length ? productValues.section[0].slug : null,
    };
    // If set, remove categories defined by users. This is set only by admins.
    delete formData.category;

    // Update Product
    return this.props
      .updateProduct(formData)
      .then((response) => {
        this.props.openStatusWindow({
          type: 'success',
          message: 'Información actualizada.',
        });
      })
      .catch((error) => {
        Sentry.captureException(event);
        this.props.openStatusWindow({
          type: 'error',
          message: 'Error. Por favor, intenta de nuevo.',
        });
      });
  };
  updateSwitch = (value) => {
    this.setState({ activeSwitch: value });
  };

  scrollTop = () => {
    window.scrollTo({ top: 0, behavior: 'smooth' });
  };

  checkField = (field = '', badWords = []) => {
    const fieldArray = field.toLowerCase().split(' ');

    const badWordsList = badWords.map((item) => item.toLowerCase());

    const filteredBadWords = fieldArray.filter((word) =>
      badWordsList.includes(word.toLowerCase()),
    );

    return filteredBadWords;
  };

  testOnSubmit = (productValues) => {
    this.setState({ errorForm: false, errorsWordsList: {} });

    const { list = [] } = this.props?.prohibitedWords;

    const { name, description } = productValues;

    // If prohibited words list doesnt have items
    if (list.length === 0) {
      return this.updateProduct(productValues);
    }

    if (list.length > 0) {
      const badWordsName = [...new Set(this.checkField(name, list))],
        badWordsDescription = [...new Set(this.checkField(description, list))];

      const items = [badWordsName, badWordsDescription];

      if (items.some((item) => item.length > 0)) {
        this.setState((state) => ({
          errorsWordsList: {
            ...state.errorsWordsList,
            badWordsName,
            badWordsDescription,
          },
          errorForm: true,
        }));
        this.scrollTop();
      } else {
        return this.updateProduct(productValues);
      }
    }
  };
  /**
   * render()
   */
  render() {
    // if (this.props.loadingData) return <IconPreloader />;
    return (
      <div>
        {/* <StepsDisplay productSlug={this.props.product.slug} /> */}

        {renderRoutes(this.props.route.routes, {
          addImage: this.addImage,
          myStore: this.props.myStore,
          onSubmit: this.testOnSubmit,
          sections: this.props.sections,
          deleteImage: this.deleteImage,
          categories: this.props.categories,
          attributeTypes: this.props.attributeTypes,
          updateGalleryOrder: this.updateGalleryOrder,
          shippingMethods: this.props.shippingMethods.results,
          interests: this.props.interests,
          errorsWordsList: this.state.errorsWordsList,
          errorForm: this.state.errorForm,
        })}

        <BottomMenu
          onSubmit={this.updateProduct}
          history={this.props.history}
          myStore={this.props.myStore}
          openStatusWindow={this.props.openStatusWindow}
          updateSwitch={this.updateSwitch}
          activeSwitch={this.state.activeSwitch}
        />
      </div>
    );
  }
}

// Add Redux state and actions to component´s props
function mapStateToProps(state) {
  const { myStore, app } = state;
  const productDetail = getProductDetailForm(state);

  return {
    loadingData: productDetail.loading,
    sections: myStore.sections,
    categories: myStore.categories,
    product: productDetail,
    attributeTypes: myStore.attribute_types,
    shippingMethods: app.shippingMethods,
    interests: myStore.interests,
    prohibitedWords: app.prohibitedWords,
  };
}
function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      dispatch,
      fetchProduct: myStoreActions.fetchProduct,
      openStatusWindow: statusWindowActions.open,
      updateProduct: myStoreActions.updateProduct,
      addProductPhoto: myStoreActions.addProductPhoto,
      updateProductPhoto: myStoreActions.updateProductPhoto,
      deleteProductPhoto: myStoreActions.deleteProductPhoto,
      fetchCategories: myStoreActions.fetchMyStoreCategories,
      fetchAttributeTypes: myStoreActions.fetchAttributeTypes,
      fetchProductAttributesList: myStoreActions.fetchProductAttributesList,
      shippingMethods: appActions.getShippingMethods,
      fetchInterests: myStoreActions.fetchMyStoreInterests,
    },
    dispatch,
  );
}

// Decorate Component
ProductEdit = connect(mapStateToProps, mapDispatchToProps)(ProductEdit);

// Preload Data for Server Side Rendering
ProductEdit.loadData = (reduxStore, routeObject) =>
  Promise.all([
    reduxStore.dispatch(myStoreActions.fetchAttributeTypes()),
    reduxStore.dispatch(myStoreActions.fetchMyStoreCategories()),
    reduxStore
      .dispatch(myStoreActions.fetchProduct(routeObject.match.params.slug))
      .catch((e) => {
        throw e;
      }),
  ]);

// Export Component
export default ProductEdit;
