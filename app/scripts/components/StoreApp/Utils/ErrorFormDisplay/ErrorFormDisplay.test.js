import React from 'react';
import Enzyme, { shallow } from 'enzyme';
import EnzymeAdapter from 'enzyme-adapter-react-16';

import ErrorFormDisplay from './ErrorFormDisplay';

Enzyme.configure({ adapter: new EnzymeAdapter() });

describe('ErrorFormDisplay Component', () => {
  /**
   * Return ShallowWrapper containing node(s) with the given data-test value.
   * @param {ShallowWrapper} wrapper - Enzyme shallow wrapper to search within.
   * @param {string} val - Value of data-test attribute for search.
   * @returns {ShallowWrapper}
   */
  const copy = 'Este campo contiene palabras no permitidas:';

  const type = ['mota', '420'];

  const wrapper = shallow(<ErrorFormDisplay errorForm={true} type={type} />);

  test('Should to be defined correctly', () => {
    expect(wrapper).toBeDefined();
  });

  test('Should render correct text copy', () => {
    expect(wrapper.find('span').text().includes(copy));
  });
  test('Should render one ul tag', () => {
    expect(wrapper.find('ul')).toHaveLength(1);
  });
  test('Should not render any li (words) tag, pass an empty array', () => {
    const localType = [];
    const localWrapper = shallow(
      <ErrorFormDisplay errorForm={true} type={localType} />,
    );
    expect(localWrapper.find('li')).toHaveLength(0);
  });

  test('Should not render any li (words) tag, pass an empty array and errorForm to false', () => {
    const localType = [];
    const localWrapper = shallow(
      <ErrorFormDisplay errorForm={false} type={localType} />,
    );
    expect(localWrapper.find('li')).toHaveLength(0);
  });

  test('Should not render if another object type its passed', () => {
    const localType = {};
    const localWrapper = shallow(
      <ErrorFormDisplay errorForm={true} type={localType} />,
    );
    expect(localWrapper.find('li')).toHaveLength(0);
  });

  test('Should render two li items (two words pass as type)', () => {
    expect(wrapper.find('li')).toHaveLength(2);
  });

  test('Should unmount correctly', () => {
    expect(wrapper.unmount()).toBeTruthy;
  });
});
