import React, { Fragment } from 'react';

export default function ErrorFormDisplay(props) {
  const { errorForm = false, type } = props;
  return (
    <Fragment>
      {errorForm && Array.isArray(type) && type?.length > 0 && (
        <div className="cr__text--caption cr__textColor--colorRed300 errorsWords">
          <span>Este campo contiene palabras no permitidas:</span>
          <ul>
            {type?.map((word, i) => (
              <li key={i}>{word}</li>
            ))}
          </ul>
        </div>
      )}
    </Fragment>
  );
}
