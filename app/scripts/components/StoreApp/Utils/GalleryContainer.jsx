import React from 'react';
import { Transition, TransitionGroup } from 'react-transition-group';
import { SortableContainer, SortableElement } from 'react-sortable-hoc';

import { UIIcon } from '../../../Utils/UIIcon.jsx';
import { ResponsiveImageFromURL } from '../../../Utils/ImageComponents.jsx';

//
const duration = 300;
const defaultStyle = {
  transition: `all ${duration}ms ease-in-out`,
  opacity: 0,
  transform: 'scale(0.9)',
};
const transitionStyles = {
  entering: { opacity: 0, transform: 'scale(0.9)' },
  entered: { opacity: 1, transform: 'scale(1)' },
};

const Thumbnail = SortableElement(({ item, state, deleteImage, setUploading }) => (
  <li className="thumbnail" onClick={() => setUploading(false)}>
    <div className="thumbnail__icons">
      <UIIcon
        icon="delete"
        type="error"
        onClick={() => {
          deleteImage({
            slug: item.slug,
            src: item.src.original.split('/').pop(),
            photo: item.src,
          });
        }}
      />
    </div>
    <span className="note_img-primary cr__text--caption">Principal</span>
    <ResponsiveImageFromURL src={item?.src?.small} alt={item?.name || ''} />
  </li>
));
// const Thumbnail = ({ item, state, deleteImage }) => (
//     <div
//         className="thumbnail"
//         style={{
//             ...defaultStyle,
//             ...transitionStyles[state]
//         }}>
//         <div className="thumbnail__icons">
//             <UIIcon
//                 icon="delete"
//                 type="error"
//                 onClick={() => {
//                     deleteImage({
//                         slug: item.slug,
//                         src: item.src.original.split('/').pop(),
//                         photo: item.src
//                     });
//                 }} />
//         </div>
//         <ResponsiveImage src={item.src} alt={item.name} />
//     </div>
// );

// const Fade = ({ children, ...props }) => (
//     <Transition
//       {...props}
//       classNames="fade"
//     >
//         {(state) => (
//             { children }
//         )}
//     </Transition>
// );

const SortableList = SortableContainer(({ items, deleteImage, setUploading }) => (
  <ul className="gallery">
    {items.map((item, index) => (
      <Thumbnail
        item={item}
        index={index}
        key={`item-${item.id}`}
        deleteImage={deleteImage}
        setUploading={setUploading}
      />
    ))}
  </ul>
));

class GalleryContainer extends React.Component {
  render() {
    const { onImageDelete, onSortEnd, items, setUploading, setOver } = this.props;
    return (
      <SortableList
        axis="xy"
        items={items}
        onSortEnd={onSortEnd}
        pressDelay={200}
        helperClass="ui-draggable"
        deleteImage={onImageDelete}
        setUploading={setUploading}
      />
    );
  }
}

export default GalleryContainer;
