import React from 'react';
import PropTypes from 'prop-types';

import { range } from '../../../Utils/genericUtils';
import ListSelector from '../../../Utils/ListSelector';
import { addHourPeriodSuffix, WEEK_OBJECT } from '../../../Utils/dateUtils';
import CheckboxListSelector from '../../../Utils/CheckboxListSelector';

// Construct opening/closing schedules
const openingSchedulesList = range(6, 11).reduce((a, b) => {
  const formattedHour = b < 10 ? `0${b}` : b;
  return [
    ...a,
    {
      name: `${formattedHour}:00am`,
      value: `${formattedHour}:00`,
    },
    {
      name: `${formattedHour}:30am`,
      value: `${formattedHour}:30`,
    },
  ];
}, []);

const closingSchedulesList = range(12, 20).reduce((a, b, i, j) => {
  let formattedHour = b > 12 ? `${b - 12}` : b;
  formattedHour = formattedHour < 10 ? `0${formattedHour}` : formattedHour;
  return [
    ...a,
    {
      name: `${formattedHour}:00pm`,
      value: `${b}:00`,
    },
    i < j.length - 1 && {
      name: `${formattedHour}:30pm`,
      value: `${b}:30`,
    },
  ];
}, []);

/*---------------------------------------------------
    SCHEDULES WINDOW COMPONENT
---------------------------------------------------*/
export default class SchedulesManager extends React.Component {
  // Static Properties Definition
  static propTypes = {
    onSubmit: PropTypes.func.isRequired,
    onCancel: PropTypes.func,
    initialValues: PropTypes.object,
  };
  static defaultProps = {
    onSubmit: (schedules) => schedules,
    onCancel: (e) => false,
  };

  /**
   * constructor()
   * @param {object} props : Component properties
   */
  constructor(props) {
    super(props);
    this.state = {
      open: props.initialValues ? props.initialValues.open : null,
      close: props.initialValues ? props.initialValues.close : null,
      weekdays: props.initialValues
        ? [
            {
              name: props.initialValues.name,
              value: props.initialValues.value,
            },
          ]
        : [],
      submitDisabled: true,
    };
    this.ID = props.initialValues ? props.initialValues.id : this.generateID();
  }
  componentDidMount() {
    // Enable/Disable 'save' button
    this.validateSchedules();
  }

  /**
   * onCancel()
   * Cancel actions component
   */
  onCancel = () => {
    this.props.onCancel();
  };

  /**
   * onSubmit()
   * Call parent callback when this component´s been submitted
   * @param {object} e : DOM Event
   */
  onSubmit = (e) => {
    const { weekdays, open, close } = this.state;

    // Execute parent callback
    const selectedWeekdays = weekdays.map((weekday) => ({
      ...weekday,
      open,
      close,
    }));
    this.props.onSubmit(selectedWeekdays);
  };

  /**
   * onOptionsAdded()
   * Handler for weekday selections. Updates component state
   * @param {array} options : Array of weekdays
   */
  onOptionsAdded = (options) => {
    this.setState(
      {
        weekdays: options,
      },
      () => this.validateSchedules(),
    );
  };

  /**
   * onTimetableChange()
   * Updates the component state with the user defined timetable values
   * @param {object} value : Selected Object
   * @param {string} type : Timetable type ['open', 'close']
   */
  onTimetableChange = (value, type) => {
    // Update State
    this.setState(
      {
        [type]: value,
      },
      () => this.validateSchedules(),
    );
  };

  /**
   * validateSchedules()
   * Validates this component´s forms.
   * @returns {bool}
   */
  validateSchedules = () => {
    const { weekdays, open, close } = this.state;

    // TODO: Make form validations
    const isValid = weekdays.length > 0 && open && close;

    // Update State
    this.setState({
      submitDisabled: !isValid,
    });
  };

  /**
   * generateID()
   * generates a random ID to identify this component
   * @returns {string} id
   */
  generateID = () => {
    function s4() {
      return Math.floor((1 + Math.random()) * 0x10000)
        .toString(16)
        .substring(1);
    }
    return `${s4()}${s4()}-${s4()}${s4()}`;
  };

  /**
   * render()
   */
  render() {
    const { weekdays, submitDisabled, open, close } = this.state;
    const timetableOpen = open && {
      ...open,
      name: addHourPeriodSuffix(open.value),
    };
    const timetableClose = close && {
      ...close,
      name: addHourPeriodSuffix(close.value),
    };

    // Return Component markup
    return (
      <div className="schedules-window" id={`schedules-window_${this.ID}`}>
        <div className="schedules-window__param weedays">
          <div className="param-info">
            <strong className="name icon_store-schedules">Días</strong>
            <p className="description">
              Selecciona los días laborales de tu tienda.
            </p>
          </div>

          <CheckboxListSelector
            initialValues={weekdays}
            options={WEEK_OBJECT}
            title="Selecciona Días"
            onSubmit={this.onOptionsAdded}
            onChange={(options) =>
              options.sort((a, b) =>
                a.value < b.value ? -1 : a.value > b.value ? 1 : 0,
              )
            }
          />
        </div>
        <div className="schedules-window__param timetable">
          <div className="param-info">
            <strong className="name icon_store-time">Horarios</strong>
            <p className="description">
              Elige los horarios en que puedes recibir pedidos.
            </p>
          </div>
          <div className="listSelector_schedules">
            <ListSelector
              title="Apertura"
              initialValues={timetableOpen}
              options={openingSchedulesList}
              onChange={(value) => this.onTimetableChange(value, 'open')}
            />
            <ListSelector
              title="Cierre"
              initialValues={timetableClose}
              options={closingSchedulesList}
              onChange={(value) => this.onTimetableChange(value, 'close')}
            />
          </div>
        </div>

        <hr />

        <div className="schedules-window__param controls">
          <button className="modal__button-cancel" onClick={this.onCancel}>
            Cancelar
          </button>
          <button
            className="modal__button"
            disabled={submitDisabled}
            onClick={this.onSubmit}
          >
            Guardar
          </button>
        </div>
      </div>
    );
  }
}
