import React, { Component } from 'react';
import { Link, NavLink } from 'react-router-dom';
import { Field, reduxForm, formValueSelector } from 'redux-form';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import addProduct from '../../components/hocs/addProduct';
import { ResponsiveImage } from '../../Utils/ImageComponents.jsx';
import { InputField } from '../../Utils/forms/formComponents';
import { myStoreActions, statusWindowActions } from '../../Actions';
import { maxLength20, storeReferrerCode } from '../../Utils/forms/formValidators';
import Cookies from 'universal-cookie';

const cookies = new Cookies();
const fakeObject = [
  {
    invited_by_user: {
      full_name: 'Cookie',
      store: 'Cookie',
    },
    invited_with_role: 'store',
    invited_to: 'create_store',
  },
];

const AddProductButton = addProduct(Link);
class StoreSideBar extends Component {
  constructor(props) {
    super(props);
    this.state = {};

    this.setCookie = this.setCookie.bind(this);
  }

  setCookie() {
    this.props.addReferrerCodeCookie(fakeObject);
    cookies.set('referrerCode', JSON.stringify(fakeObject), { path: '/' });
  }

  onSubmit = (formValues) => {
    this.props
      .addReferrerCode({
        code: formValues.referrer,
      })
      .then((response) => {
        this.props.openStatusWindow({
          type: 'success',
          message: 'Tu código se aplicó correctamente.',
        });
      })
      .catch((error) => {
        if (Array.isArray(error.response.data.code)) {
          this.props.openStatusWindow({
            type: 'error',
            message: 'Tu código es inválido',
          });
        } else {
          const errorName = error.response.data.code.split(';');
          const errorMessage = errorName[1].slice(1);
          this.props.openStatusWindow({ type: 'error', message: errorMessage });
        }
      });
  };

  renderReferrerInput() {
    const { handleSubmit } = this.props;
    return (
      <div className="storeMenu__referrerCode">
        <div className="question note">
          ¿Abriste tu tienda por recomendación de alguien?
        </div>
        {/* onSubmit={handleSubmit(this.onSubmit)} */}
        <form className="referrer-form" onSubmit={handleSubmit(this.onSubmit)}>
          {/* <input
                type="text"
                name="referrerCode"
                placeholder="Escribelo aquí..."
                value={this.state.referrerCode}
                onChange={this.getReferredCode}
                maxLength="20"
                /> */}
          <Field
            component={InputField}
            autoFocus
            className="referrer-input"
            name="referrer"
            id="referrer"
            placeholder="Ingresa su código aquí"
            validate={[maxLength20, storeReferrerCode]}
          />

          {/* {submitting && <IconPreloader />}
                {(invalid && error) && (
                    <div className="form_status danger big">{error}</div>
                )} */}
          <div className="btns-container">
            {/* <div><a className="no-code" onClick={this.setCookie}>No cuento con ninguno.</a></div> */}
            <div onClick={handleSubmit(this.onSubmit)}>
              <a className="send">Enviar Código</a>
            </div>
          </div>
        </form>

        {/*!<div className="referrer_error">
                Ocurrio un error al ingresar este código.
        </div> */}
      </div>
    );
  }

  render() {
    const { open, myStore, toogleMenu, isStoreClosed, isStorePaused } = this.props;

    const enteredCode = myStore.entered_codes
      ? myStore.entered_codes.find((ec) => ec.invited_to === 'create_store')
      : null;

    return (
      <aside className="storeMenu">
        <div className="storeMenu_container">
          <div className="logo">
            <Link to="/" className="logo__button">
              <picture>
                <source
                  media="(min-width: 45.5em)"
                  srcSet={require('images/logo/header_logo_full.svg')}
                />
                <img
                  src={require('images/logo/header_logo.svg')}
                  alt="Home"
                  loading="lazy"
                />
              </picture>
            </Link>
            {/* <a href="">
                        <img src={require('images/header_logo.svg')} alt="Home" />
                    </a> */}
          </div>
          <div className="menu">
            <a href="#" onClick={() => toogleMenu()}>
              Menu
            </a>
          </div>
        </div>

        <div className={open ? 'open' : 'closed'}>
          <div className="storeMenu_dropdown">
            <div className="storeMenu__profile storeMenu__module">
              <div className="imageContainer">
                <div className="imageContainer__wrapper">
                  {!myStore.photo || myStore.photo === '' ? (
                    ''
                  ) : (
                    <ResponsiveImage src={myStore.photo} alt={myStore.name} />
                  )}
                </div>
              </div>

              {myStore.is_active.bool ? (
                <div className="storeMenu__profile-info">
                  <h3>{myStore.name}</h3>
                  <Link to={`/stores/${myStore.slug}/`} className="c2a_border">
                    Ver mi Tienda
                  </Link>
                </div>
              ) : (
                <div className="storeMenu__profile-info">
                  <h3>{myStore.name}</h3>
                </div>
              )}
              <div className="storeMenu__new-article storeMenu__module">
                {!isStoreClosed ? (
                  <AddProductButton
                    to="/my-store/products/add"
                    className="c2a_square"
                  >
                    + Nuevo Producto
                  </AddProductButton>
                ) : (
                  <button className="c2a_square">+ Nuevo Producto</button>
                )}
              </div>
            </div>

            <nav className="storeMenu__navigation storeMenu__module">
              <ul>
                <li
                  onClick={() => toogleMenu()}
                  className="storeMenu__navigation-item"
                >
                  <NavLink to="/my-store/dashboard" activeClassName="active">
                    Mi panel
                  </NavLink>
                </li>
                <li
                  onClick={() => toogleMenu()}
                  className="storeMenu__navigation-item"
                >
                  <NavLink to="/my-store/settings" activeClassName="active">
                    Mi Tienda
                  </NavLink>
                </li>
                <li
                  onClick={() => toogleMenu()}
                  className="storeMenu__navigation-item"
                >
                  <NavLink to="/my-store/products" activeClassName="active">
                    Productos
                  </NavLink>
                </li>
                <li
                  onClick={() => toogleMenu()}
                  className="storeMenu__navigation-item"
                >
                  <NavLink to="/my-store/orders" activeClassName="active">
                    &Oacute;rdenes
                  </NavLink>
                </li>
                <li
                  onClick={() => toogleMenu()}
                  className="storeMenu__navigation-item"
                >
                  <NavLink to="/my-store/movements" activeClassName="active">
                    Mis Ventas
                  </NavLink>
                </li>
                <li
                  onClick={() => toogleMenu()}
                  className="storeMenu__navigation-item"
                >
                  <NavLink to="/my-store/subscription" activeClassName="active">
                    Subscripci&oacute;n
                  </NavLink>
                </li>

                {/* <li 
                                onClick={() => toogleMenu()} 
                                className="storeMenu__navigation-item"
                            >
                                <NavLink 
                                    to="/my-store/sections" 
                                    activeClassName="active"
                                >Mis Secciones</NavLink></li> */}
                {/* <li 
                                className="storeMenu__navigation-item"
                            ><a href="#">Notificaciones</a></li> */}
              </ul>
            </nav>

            <div className="storeMenu__new-crPro storeMenu__module">
              <p className="note">
                ¿Quieres crecer tu marca? Conoce los servicios de
              </p>
              <NavLink
                to="/pro"
                className="link"
                activeClassName="active"
                target="_blank"
              >
                Canasta Rosa Pro
                <span />
              </NavLink>
            </div>
            {!isStoreClosed && !enteredCode && this.renderReferrerInput()}
          </div>
        </div>
      </aside>
    );
  }
}

// Wrap component within reduxForm
const formName = 'myStoreReferrerCode_Form';
StoreSideBar = reduxForm({
  form: formName,
  enableReinitialize: true,
  keepDirtyOnReinitialize: true, // <---- Prevent 'dirty' fields from updating
})(StoreSideBar);

// Add Redux state and actions to component´s props
const selector = formValueSelector(formName);

// Redux Map Functions
function mapStateToProps() {
  return {};
}
function mapDispatchToProps(dispatch) {
  const {
    addReferrerCode,
    addReferrerCodeCookie,
    getEnteredReferrerCodes,
  } = myStoreActions;

  return bindActionCreators(
    {
      addReferrerCode,
      getEnteredReferrerCodes,
      openStatusWindow: statusWindowActions.open,
    },
    dispatch,
  );
}

StoreSideBar = connect(mapStateToProps, mapDispatchToProps)(StoreSideBar);

// Export Component
export default StoreSideBar;
