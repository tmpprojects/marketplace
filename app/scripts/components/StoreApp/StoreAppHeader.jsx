import React from 'react';
import { Component } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { modalBoxActions } from '../../Actions';
import { ProfileMenuDropdown } from '../ProfileMenuDropdown.jsx';

class StoreAppHeader extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
    };
  }

  componentDidMount() {}

  isMenuOpen() {
    this.setState({
      open: !this.state.open,
    });
  }
  handleClick(_type) {
    this.props.dispatch(modalBoxActions.open(_type));
  }

  render() {
    return (
      <header style={{ position: 'fixed' }}>
        <div className="navContainer">
          <div className="logo">
            <Link to="/">
              <picture>
                <source
                  media="(min-width: 36em)"
                  srcSet={require('../../../images/logo/header_logo_full.svg')}
                />
                <img
                  src={require('../../../images/logo/header_logo.svg')}
                  alt="Home"
                />
              </picture>
            </Link>
          </div>
        </div>
      </header>
    );
  }
}

function mapStateToProps({ auth, users }) {
  return {
    auth,
    user: users,
  };
}

const connectedHeader = connect(mapStateToProps)(StoreAppHeader);
export { connectedHeader as StoreAppHeader };
