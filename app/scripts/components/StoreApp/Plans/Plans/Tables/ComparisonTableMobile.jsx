import React, { useState, useEffect } from 'react';
import checkmark from '../../../../../../images/plans/checkmark.svg';
import line from '../../../../../../images/plans/horizontal-line.svg';

export default function ComparisonTableMobile(props) {
  const [activeTab, setActiveTap] = useState(1);
  const tabs = [
    {
      plan: 'Standard',
      value: 1,
    },
    {
      plan: 'Plus',
      value: 2,
    },
  ];

  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);
  const { proPlanPrice = '', comissionPro = '', comissionStandard = '' } = props;

  return (
    <div className="cr__plans-specifications mobile" data-test="mobile-table">
      <h6
        className="cr__plans-specifications-title cr__text--subtitle2 cr__textColor--colorDark300"
        data-test="main-title"
      >
        Comparaci&oacute;n de planes
      </h6>
      <div className="cr__plans-tabs">
        <ul className="cr__plans-tabs-list" data-test="list">
          {tabs.map(({ plan, value }) => (
            <li
              key={value}
              className={`cr__plans-tabs-list-item cr__text--subtitle3 cr__textColor--colorDark300 ${
                activeTab === value ? plan : ''
              }`}
              onClick={() => {
                setActiveTap(value);
              }}
              aria-label={`Plan ${plan}`}
              data-test="tab-button"
            >
              {plan}
            </li>
          ))}
        </ul>
      </div>
      {activeTab === 1 && (
        <div className="cr__plans-tabs-standard">
          <table className="cr__textColor--colorDark300">
            <tbody>
              <tr>
                <td className="cr__text--subtitle3 section-parent single-margin">
                  Tarifas
                </td>
                <td className="cr__text--subtitle3 section-parent" />
              </tr>
              <tr>
                <td className="cr__text--paragraph section-child">Tarifa mensual</td>
                <td className="cr__text--paragraph section-child right weight">
                  Gratis
                </td>
              </tr>
              <tr>
                <td className="cr__text--paragraph section-child">
                  Comisi&oacute;n por el total de la venta
                  <p className="cr__textColor--colorGray400">
                    *Precio del producto + costo del envío
                  </p>
                </td>
                <td className="cr__text--paragraph section-child right">
                  {`${comissionStandard}%`}
                </td>
              </tr>
              <tr>
                <td className="cr__text--subtitle3 section-parent extra-margin">
                  Beneficios
                </td>
                <td className="section-parent" />
              </tr>
              <tr>
                <td className="cr__text--paragraph section-child">
                  Tienda On-Line (Incluye página web para vender en línea)
                </td>
                <td className="cr__text--paragraph section-child right">
                  <img src={checkmark} alt="checkmark" className="checkmark" />
                </td>
              </tr>
              <tr>
                <td className="cr__text--paragraph section-child">
                  Productos ilimitados
                </td>
                <td className="cr__text--paragraph section-child right">
                  <img src={checkmark} alt="checkmark" className="checkmark" />
                </td>
              </tr>
              <tr>
                <td className="cr__text--paragraph section-child">
                  M&eacute;todos de pago para clientes:
                </td>
                <td className="section-child" />
              </tr>
              <tr>
                <td className="cr__text--paragraph section-child">
                  <h6 className="marginLeft">
                    Tarjeta de Cr&eacute;dito y D&eacute;bito
                  </h6>
                </td>
                <td className="cr__text--paragraph section-child right">
                  <img src={checkmark} alt="checkmark" className="checkmark" />
                </td>
              </tr>
              <tr>
                <td className="cr__text--paragraph section-child">
                  <h6 className="marginLeft">Paypal</h6>
                </td>
                <td className="cr__text--paragraph section-child right">
                  <img src={checkmark} alt="checkmark" className="checkmark" />
                </td>
              </tr>
              <tr>
                <td className="cr__text--paragraph section-child">
                  <h6 className="marginLeft">D&eacute;positos y Transferencias</h6>
                </td>
                <td className="cr__text--paragraph section-child right">
                  <img src={checkmark} alt="checkmark" className="checkmark" />
                </td>
              </tr>
              <tr>
                <td className="cr__text--paragraph section-child">
                  <h6 className="marginLeft">Pago en OXXO</h6>
                </td>
                <td className="cr__text--paragraph section-child right">
                  <img src={checkmark} alt="checkmark" className="checkmark" />
                </td>
              </tr>
              <tr>
                <td className="cr__text--paragraph section-child">Ventas seguras</td>
                <td className="cr__text--paragraph section-child right">
                  <img src={checkmark} alt="checkmark" className="checkmark" />
                </td>
              </tr>
              <tr>
                <td className="cr__text--subtitle3 section-parent extra-margin">
                  Servicios
                </td>
                <td className="cr__text--subtitle3 section-parent" />
              </tr>
              <tr>
                <td className="cr__text--paragraph section-child">Env&iacute;os:</td>
                <td className="section-child" />
              </tr>
              <tr>
                <td className="cr__text--paragraph section-child">
                  <h6 className="marginLeft">
                    Env&iacute;os express* y por paquetería a nivel nacional
                  </h6>
                </td>
                <td className="cr__text--paragraph section-child right">
                  <img src={checkmark} alt="checkmark" className="checkmark" />
                </td>
              </tr>
              {/* <tr>
                <td className="cr__text--paragraph section-child">
                  <h6 className="marginLeft">
                    Tarifa preferencial para paqueter&iacute;a nacional
                  </h6>
                </td>
                <td className="cr__text--paragraph section-child right">
                  <img src={line} alt="horizontal line" className="checkmark" />
                </td>
              </tr> */}
              <tr>
                <td className="cr__text--paragraph section-child">
                  Atenci&oacute;n a clientes:
                </td>
                <td className="section-child" />
              </tr>
              <tr>
                <td className="cr__text--paragraph section-child">
                  <h6 className="marginLeft">Servicio a compradores</h6>
                </td>
                <td className="cr__text--paragraph section-child right">
                  <img src={checkmark} alt="checkmark" className="checkmark" />
                </td>
              </tr>
              <tr>
                <td className="cr__text--paragraph section-child">
                  <h6 className="marginLeft">
                    Servicio a tiendas por correo electr&oacute;nico{' '}
                  </h6>
                </td>
                <td className="cr__text--paragraph section-child right">
                  <img src={checkmark} alt="checkmark" className="checkmark" />
                </td>
              </tr>
              <tr>
                <td className="cr__text--paragraph section-child">
                  <h6 className="marginLeft">
                    Acceso a Category Manager para consultor&iacute;as sobre
                    C&oacute;mo Vender M&aacute;s y soporte con tu tienda.
                    <br />
                    <span className="cr__textColor--colorGray400">
                      ** El soporte operativo para cuesti&oacute; de pagos,
                      facturaci&oacute;n, &oacute;rdenes y entregas seguir&aacute;
                      siendo a trav&eacute;s del chat de soporte
                    </span>
                  </h6>
                </td>
                <td className="cr__text--paragraph section-child right">
                  <img src={line} alt="horizontal line" className="checkmark" />
                </td>
              </tr>
              {/* <tr>
                <td className="cr__text--paragraph section-child">
                  Soporte T&eacute;cnico
                </td>
                <td className="cr__text--paragraph section-child right">
                  <img src={line} alt="horizontal line" className="checkmark" />
                </td>
              </tr> */}
              <tr>
                <td className="cr__text--subtitle3 section-parent extra-margin">
                  Herramientas
                </td>
                <td className="cr__text--subtitle3 section-parent" />
              </tr>
              <tr>
                <td className="cr__text--paragraph section-child">
                  Tablero de Control
                </td>
                <td className="cr__text--paragraph section-child right">
                  <img src={checkmark} alt="checkmark" className="checkmark" />
                </td>
              </tr>
              <tr>
                <td className="cr__text--paragraph section-child">
                  Gesti&oacute;n de pautas y servicios PRO (adquiridos aparte) sin
                  costo adicional
                  {/* <p className="cr__textColor--colorGray400">
                    *Consulta a tu Category Manager para conocer los detalles
                  </p> */}
                </td>
                <td className="cr__text--paragraph section-child right">
                  <img src={line} alt="horizontal line" className="checkmark" />
                </td>
              </tr>
              <tr>
                <td className="cr__text--paragraph section-child">
                  Estad&iacute;sticas y Reportes Avanzados
                </td>
                <td className="cr__text--paragraph section-child right">
                  <img src={line} alt="horizontal line" className="checkmark" />
                </td>
              </tr>
              <tr>
                <td className="cr__text--paragraph section-child">
                  Chat directo con clientes
                </td>
                <td className="cr__text--paragraph section-child right">
                  <img src={line} alt="horizontal line" className="checkmark" />
                </td>
              </tr>
              <tr>
                <td className="cr__text--paragraph section-child">
                  Descuentos y cupones
                </td>
                <td className="cr__text--paragraph section-child right">
                  <img src={line} alt="horizontal line" className="checkmark" />
                </td>
              </tr>
            </tbody>
          </table>
        </div>
      )}
      {activeTab === 2 && (
        <div className="cr__plans-tabs-pro">
          <table className="cr__textColor--colorDark300">
            <tbody>
              <tr>
                <td className="cr__text--subtitle3 section-parent single-margin">
                  Tarifas
                </td>
                <td className="cr__text--subtitle3 section-parent" />
              </tr>
              <tr>
                <td className="cr__text--paragraph section-child">Tarifa mensual</td>
                <td className="cr__text--paragraph section-child right weight">
                  ${proPlanPrice}MX
                </td>
              </tr>
              <tr>
                <td className="cr__text--paragraph section-child">
                  Comisi&oacute;n por el total de la venta
                  <p className="cr__textColor--colorGray400">
                    *Precio del producto + costo del envío
                  </p>
                </td>
                <td className="cr__text--paragraph section-child right">{`${comissionPro}%`}</td>
              </tr>
              <tr>
                <td className="cr__text--subtitle3 section-parent extra-margin">
                  Beneficios
                </td>
                <td className="section-parent" />
              </tr>
              <tr>
                <td className="cr__text--paragraph section-child">
                  Tienda On-Line (Incluye página web para vender en línea)
                </td>
                <td className="cr__text--paragraph section-child right">
                  <img src={checkmark} alt="checkmark" className="checkmark" />
                </td>
              </tr>
              <tr>
                <td className="cr__text--paragraph section-child">
                  Productos ilimitados
                </td>
                <td className="cr__text--paragraph section-child right">
                  <img src={checkmark} alt="checkmark" className="checkmark" />
                </td>
              </tr>
              <tr>
                <td className="cr__text--paragraph section-child">
                  Tus clientes pueden pagar con:
                </td>
                <td className="section-child" />
              </tr>
              <tr>
                <td className="cr__text--paragraph section-child">
                  <h6 className="marginLeft">
                    Tarjeta de Cr&eacute;dito y D&eacute;bito
                  </h6>
                </td>
                <td className="cr__text--paragraph section-child right">
                  <img src={checkmark} alt="checkmark" className="checkmark" />
                </td>
              </tr>
              <tr>
                <td className="cr__text--paragraph section-child">
                  <h6 className="marginLeft">Paypal</h6>
                </td>
                <td className="cr__text--paragraph section-child right">
                  <img src={checkmark} alt="checkmark" className="checkmark" />
                </td>
              </tr>
              <tr>
                <td className="cr__text--paragraph section-child">
                  <h6 className="marginLeft">D&eacute;positos y Transferencias</h6>
                </td>
                <td className="cr__text--paragraph section-child right">
                  <img src={checkmark} alt="checkmark" className="checkmark" />
                </td>
              </tr>
              <tr>
                <td className="cr__text--paragraph section-child">
                  <h6 className="marginLeft">Pago en OXXO </h6>
                </td>
                <td className="cr__text--paragraph section-child right">
                  <img src={checkmark} alt="checkmark" className="checkmark" />
                </td>
              </tr>
              <tr>
                <td className="cr__text--paragraph section-child">Ventas seguras</td>
                <td className="cr__text--paragraph section-child right">
                  <img src={checkmark} alt="checkmark" className="checkmark" />
                </td>
              </tr>
              <tr>
                <td className="cr__text--subtitle3 section-parent extra-margin">
                  Servicios
                </td>
                <td className="cr__text--subtitle3 section-parent" />
              </tr>
              <tr>
                <td className="cr__text--paragraph section-child">Env&iacute;os:</td>
                <td className="section-child" />
              </tr>
              <tr>
                <td className="cr__text--paragraph section-child">
                  <h6 className="marginLeft">
                    Env&iacute;os express* y por paquetería a nivel nacional
                  </h6>
                </td>
                <td className="cr__text--paragraph section-child right">
                  <img src={checkmark} alt="checkmark" className="checkmark" />
                </td>
              </tr>
              <tr>
                <td className="cr__text--paragraph section-child">
                  <h6 className="marginLeft">
                    Tarifa preferencial para paqueter&iacute;a nacional
                  </h6>
                </td>
                <td className="cr__text--paragraph section-child right">
                  <img src={checkmark} alt="checkmark" className="checkmark" />
                </td>
              </tr>
              <tr>
                <td className="cr__text--paragraph section-child">
                  Atenci&oacute;n a clientes:
                </td>
                <td className="section-child" />
              </tr>
              <tr>
                <td className="cr__text--paragraph section-child">
                  <h6 className="marginLeft">Servicio a compradores></h6>
                </td>
                <td className="cr__text--paragraph section-child right">
                  <img src={checkmark} alt="checkmark" className="checkmark" />
                </td>
              </tr>
              <tr>
                <td className="cr__text--paragraph section-child">
                  <h6 className="marginLeft">
                    Servicio a tiendas por correo electr&oacute;nico
                  </h6>
                </td>
                <td className="cr__text--paragraph section-child right">
                  <img src={checkmark} alt="checkmark" className="checkmark" />
                </td>
              </tr>
              <tr>
                <td className="cr__text--paragraph section-child">
                  <h6 className="marginLeft">
                    Acceso a Category Manager para consultor&iacute;as sobre
                    C&oacute;mo Vender M&aacute;s y soporte con tu tienda.
                    <br />
                    <span className="cr__textColor--colorGray400">
                      ** El soporte operativo para cuesti&oacute; de pagos,
                      facturaci&oacute;n, &oacute;rdenes y entregas seguir&aacute;
                      siendo a trav&eacute;s del chat de soporte
                    </span>
                  </h6>
                </td>
                <td className="cr__text--paragraph section-child right">
                  <img src={checkmark} alt="checkmark" className="checkmark" />
                </td>
              </tr>
              {/* <tr>
                <td className="cr__text--paragraph section-child">
                  Soporte T&eacute;cnico
                </td>
                <td className="cr__text--paragraph section-child right">
                  <img src={checkmark} alt="checkmark" className="checkmark" />
                </td>
              </tr> */}
              <tr>
                <td className="cr__text--subtitle3 section-parent extra-margin">
                  Herramientas
                </td>
                <td className="cr__text--subtitle3 section-parent" />
              </tr>
              <tr>
                <td className="cr__text--paragraph section-child">
                  Tablero de Control
                </td>
                <td className="cr__text--paragraph section-child right">
                  <img src={checkmark} alt="checkmark" className="checkmark" />
                </td>
              </tr>
              <tr>
                <td className="cr__text--paragraph section-child">
                  Gesti&oacute;n de pautas y servicios PRO (adquiridos aparte) sin
                  costo adicional
                  {/* <p className="cr__textColor--colorGray400">
                    *Consulta a tu Category Manager para conocer los detalles
                  </p> */}
                </td>
                <td className="cr__text--paragraph section-child right">
                  <img src={checkmark} alt="checkmark" className="checkmark" />
                </td>
              </tr>
              <tr>
                <td className="cr__text--paragraph section-child">
                  Estad&iacute;sticas y Reportes Avanzados
                </td>
                <td className="cr__text--paragraph section-child right">
                  Pr&oacute;ximamente
                </td>
              </tr>
              <tr>
                <td className="cr__text--paragraph section-child">
                  Chat directo con clientes
                </td>
                <td className="cr__text--paragraph section-child right">
                  Pr&oacute;ximamente
                </td>
              </tr>
              <tr>
                <td className="cr__text--paragraph section-child">
                  Descuentos y cupones
                </td>
                <td className="cr__text--paragraph section-child right">
                  Pr&oacute;ximamente
                </td>
              </tr>
            </tbody>
          </table>
        </div>
      )}
    </div>
  );
}
