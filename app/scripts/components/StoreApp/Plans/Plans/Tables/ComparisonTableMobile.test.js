import React from 'react';
import Enzyme, { shallow, mount } from 'enzyme';
import EnzymeAdapter from 'enzyme-adapter-react-16';

import ComparisonTableMobile from './ComparisonTableMobile';

Enzyme.configure({ adapter: new EnzymeAdapter() });

describe('ComaparisonTableMobile Component', () => {
  /**
   * Factory function to create a ShallowWrapper for the App component.
   * @function setup
   * @param {object} props - Component props specific to this setup.
   * @param {object} state - Initial state for setup.
   * @returns {ShallowWrapper}
   */
  const setup = (props = { activeTab: 1 }, state = null) => {
    const setActiveTab = jest.fn();
    const wrapper = shallow(
      <ComparisonTableMobile {...props} setActiveTab={setActiveTab} />,
    );
    if (state) wrapper.setState(state);
    return wrapper;
  };
  /**
   * Return ShallowWrapper containing node(s) with the given data-test value.
   * @param {ShallowWrapper} wrapper - Enzyme shallow wrapper to search within.
   * @param {string} val - Value of data-test attribute for search.
   * @returns {ShallowWrapper}
   */
  const findByTestAttr = (wrapper, val) => wrapper.find(`[data-test="${val}"]`);

  const wrapper = setup();

  test('should to be defined correctly', () => {
    expect(wrapper).toBeDefined();
  });
  test('should snapshot correctly', () => {
    expect(wrapper).toMatchSnapshot();
  });
  test('renders component without error', () => {
    const mobileTable = findByTestAttr(wrapper, 'mobile-table');
    expect(mobileTable.length).toBe(1);
  });
  test('renders title ', () => {
    const mainTitle = findByTestAttr(wrapper, 'main-title');
    expect(mainTitle.length).toBe(1);
  });
  test('render list of tabs without error ', () => {
    const list = findByTestAttr(wrapper, 'list');
    expect(list.children()).toHaveLength(2);
  });
  test('render 2 buttons, equally number of tabs', () => {
    const tabButton = findByTestAttr(wrapper, 'tab-button');
    expect(tabButton.length).toBe(2);
  });
  test('Should be able to click the tab', () => {
    const tabButton = findByTestAttr(wrapper, 'tab-button');
    tabButton.at(0).simulate('click');
  });
  // test('Should be able to click the tab2', () => {
  //   const wrapper = setup();
  //   const setActiveTab = jest.fn();
  //   const tabButton = findByTestAttr(wrapper, 'tab-button');
  //   tabButton.at(0).simulate('click');
  // });
  test('should unmount correctly', () => {
    expect(wrapper.unmount()).toBeTruthy;
  });
});
