import React from 'react';
import mockStore from 'redux-mock-store';
import { Provider } from 'react-redux';
import renderer from 'react-test-renderer';
import Enzyme, { shallow } from 'enzyme';
import EnzymeAdapter from 'enzyme-adapter-react-16';
import reducer from '../../../../Reducers/mystore.reducer';

import Plans from './Plans';
import { createStore } from 'redux';

Enzyme.configure({ adapter: new EnzymeAdapter() });

// const storeFactory = (initialState) => {
//   return createStore(reducer, initialState);
// }
// const setup = (initialState = {}) => {
//    const store = storeFactory(initialState);
//   const wrapper = shallow(<Plans store={store} />);
//   console.log(wrapper.debug());
// };
//  setup();

// const setup = (props = {}, state = null) => {
//   const wrapper = shallow(<Plans {...props} />);
//   if (state) wrapper.setState(state);
//   return wrapper;
// };
// const findByTestAttr = (wrapper, val) => {
//   return wrapper.find(`[data-test="${val}"]`);
// }

test('Component connected to store.', () => {
  let store;
  let component;
  beforeEach((props = {}, state = null) => {
    store = mockStore({
      myState: 'sample text',
    });
    component = renderer.create(
      <Provider store={store}>
        <Plans {...props} />
      </Provider>,
    );
  });
});

// test('State tab should be 1', () => {
//   const wrapper = setup();
//
//   expect(wrapper.state()).toEqual({ width: 1200 });
// });
