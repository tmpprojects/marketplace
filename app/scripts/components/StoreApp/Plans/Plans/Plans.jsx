import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import {
  myStoreActions,
  statusWindowActions,
  modalBoxActions,
} from '../../../../Actions';
import { myStoreErrorTypes } from '../../../../Constants/mystore.constants';
import { getUIErrorMessage } from '../../../../Utils/errorUtils';
import { IconPreloader } from '../../../../Utils/Preloaders';
import ComparisonTableWeb from './Tables/ComparisonTableWeb';
import ComparisonTableMobile from './Tables/ComparisonTableMobile';
import ModalBoxWelcome from '../ModalBoxes/ModalBoxWelcome';
import checkmark from '../../../../../images/plans/checkmark.svg';
import Manager from '../Manager/Myaccount';
import ErrorBoundary from '../ErrorBoundary/ErrorBoundary';

class Plans extends Component {
  state = {
    width: 1200,
  };

  componentDidMount() {
    this.props.getMyStorePlans();
    this.updateDimensions();
    window.addEventListener('resize', this.updateDimensions);
  }
  componentDidUpdate() {}
  componentWillUnmount() {
    window.removeEventListener('resize', this.updateDimensions);
  }
  openWelcomeWindow() {
    this.props.openModalBox(() => (
      <ModalBoxWelcome closeModalBox={this.props.closeModalBox} />
    ));
  }
  updateDimensions = () => {
    this.setState({ width: window.innerWidth });
  };

  subscribeToFreeTrial = () => {
    this.props
      .startFreeTrial()
      .then(() => {
        if (this.props.storePlans.free_trial === 200) {
          this.props.fetchMyStore();
          this.openWelcomeWindow();
        }
      })
      .catch((error) => {
        // Display UI error messages
        const uiMessage =
          error.response.data.non_field_errors ||
          getUIErrorMessage(error.response, myStoreErrorTypes);
        this.props.openStatusWindow({
          type: 'error',
          message: uiMessage,
        });
      });
  };
  render() {
    const { myStore = {} } = this.props;
    const { plans = [] } = this.props?.storePlans;

    if (this.props?.storePlans?.loading) return <IconPreloader />;
    if (!plans?.length) return null;

    // Get Subscription Plans details
    const standardPlan = plans?.find((plan) => plan?.slug === 'free-plan');
    const proPlan = plans?.find((plan) => plan?.slug === 'pro-plan');
    const proPlanPrice = parseFloat(proPlan?.price);
    const standardPlanComission = standardPlan?.variables
      .map((v) => v.value * 100)
      .join('');
    const proPlanComission = proPlan?.variables.map((v) => v.value * 100).join('');

    // Current plan details
    const currentPlan = myStore?.plan.plan;
    const hasUsedTrial = myStore?.plan?.is_trial_used;
    // Return JSX
    return (
      <React.Fragment>
        {currentPlan?.slug === 'free-plan' && !hasUsedTrial ? (
          <div className="cr__plans" data-test="component-plans">
            <h6 className="cr__plans-title cr__text--subtitle cr__textColor--colorDark400">
              Subscripci&oacute;n
            </h6>
            <div className="cr__plans-banner" data-test="banner">
              <img
                src={require('../../../../../images/plans/leftup.svg')}
                alt="Figura Banner"
                className="cr__plans-banner-lu"
              />
              <img
                src={require('../../../../../images/plans/leftdown.svg')}
                alt="Figura Banner"
                className="cr__plans-banner-ld"
              />
              <img
                src={require('../../../../../images/plans/rightup.svg')}
                alt="Figura Banner"
                className="cr__plans-banner-ru"
              />
              <img
                src={require('../../../../../images/plans/rightdown.svg')}
                alt="Figura Banner"
                className="cr__plans-banner-rd"
              />
              <h1 className="cr__plans-banner-title cr__text--subtitle cr__textColor--colorDark400">
                TODO PARA QUE{' '}
              </h1>
              <h1 className="cr__plans-banner-title cr__text--subtitle cr__textColor--colorDark400">
                TU NEGOCIO CREZCA
              </h1>
              <h6 className="cr__plans-banner-subtitle cr__text--paragraph cr__textColor--colorDark100">
                Conoce las herramientas y soluciones
              </h6>
              <h6 className="cr__plans-banner-subtitle cr__text--paragraph cr__textColor--colorDark100">
                que se ajustan a tus necesidades
              </h6>
            </div>
            <div className="cr__plans-selection" data-test="selection-plan">
              <ul className="cr__plans-selection-list">
                <li className="cr__plans-selection-list-item">
                  <div className="cr__plans-selection-container">
                    <img
                      src={require('../../../../../images/plans/standardC.svg')}
                      className="cr__plans-selection-container-iconStandard"
                      alt="Icono plan estándar"
                      aria-label="Icono plan estándar"
                    />
                    <div className="text-mobile cr__textColor--colorDark300">
                      <h5 className="cr__plans-selection-container-title cr__text--subtitle2">
                        Standard
                      </h5>
                      <h6 className="cr__plans-selection-container-subtitle cr__text--paragraph">
                        Siempre gratuito
                      </h6>
                      <p className="cr__plans-selection-container-paragraph cr__text--paragraph">
                        Todo lo que necesitas para comenzar a vender en l&iacute;nea
                      </p>
                      {currentPlan.slug === 'free-plan' && (
                        <h5 className="cr__plans-selection-container-plan cr__text--paragraph">
                          <img
                            src={checkmark}
                            alt="Palomeado"
                            className="checkmarkPurple"
                          />
                          Este es tu plan actual
                        </h5>
                      )}
                    </div>
                  </div>
                </li>
                <li className="cr__plans-selection-list-item">
                  <div className="cr__plans-selection-container">
                    <img
                      src={require('../../../../../images/plans/proC.svg')}
                      className="cr__plans-selection-container-iconPro"
                      alt="figure"
                    />
                    <div className="text-mobile cr__textColor--colorDark300">
                      <h5 className="cr__plans-selection-container-title cr__text--subtitle2 ">
                        Plus
                      </h5>
                      <h6 className="cr__plans-selection-container-subtitle cr__text--paragraph">
                        ${proPlanPrice}MX mensuales
                      </h6>
                      <p className="cr__plans-selection-container-paragraph cr__text--paragraph">
                        Mejores tarifas, soporte especializado y herramientas para
                        crecer tu negocio
                      </p>
                      {myStore?.plan?.is_trial_used ? (
                        // <h5 className='cr__plans-selection-container-plan cr__text--paragraph'>
                        //   <img
                        //     src={checkmark}
                        //     alt='Palomeado'
                        //     className='checkmarkPurple'
                        //   />
                        //   Este es tu plan actual
                        // </h5>
                        <button
                          className="cr__plans-selection-container-button c2a_square cr__text--paragraph"
                          // onClick={() => this.joiningAgain()}
                          aria-label="Regresar a Plus"
                          data-test="subscribe-to-plan"
                        >
                          Regresar a Plus
                        </button>
                      ) : (
                        <button
                          className="cr__plans-selection-container-button c2a_square cr__text--paragraph"
                          onClick={() => this.subscribeToFreeTrial()}
                          aria-label="Probar 30 días gratis"
                          data-test="subscribe-to-plan"
                        >
                          Probar 30 d&iacute;as gratis
                        </button>
                      )}
                    </div>
                  </div>
                </li>
              </ul>
            </div>
            {this.state.width >= 768 ? (
              <ComparisonTableWeb
                proPlanPrice={proPlanPrice}
                comissionPro={proPlanComission}
                comissionStandard={standardPlanComission}
              />
            ) : (
              <ComparisonTableMobile
                proPlanPrice={proPlanPrice}
                comissionPro={proPlanComission}
                comissionStandard={standardPlanComission}
              />
            )}
          </div>
        ) : (
          <ErrorBoundary>
            <Manager
              myStore={this.props.myStore}
              openModalBox={this.props.openModalBox}
              closeModalBox={this.props.closeModalBox}
              openStatusWindow={this.props.openStatusWindow}
              fetchMyStore={this.props.fetchMyStore}
            />

            <div className="cr__plans" data-test="component-plans">
              {this.state.width >= 768 ? (
                <ComparisonTableWeb
                  proPlanPrice={proPlanPrice}
                  comissionPro={proPlanComission}
                  comissionStandard={standardPlanComission}
                />
              ) : (
                <ComparisonTableMobile
                  proPlanPrice={proPlanPrice}
                  comissionPro={proPlanComission}
                  comissionStandard={standardPlanComission}
                />
              )}
            </div>
          </ErrorBoundary>
        )}
      </React.Fragment>
    );
  }
}

// Redux Map Functions
function mapStateToProps(state) {
  const { myStore } = state;
  return {
    storePlans: myStore.store_plans,
  };
}

function mapDispatchToprops(dispatch) {
  return bindActionCreators(
    {
      getMyStorePlans: myStoreActions.getMyStorePlans,
      startFreeTrial: myStoreActions.startFreeTrial,
      fetchMyStore: myStoreActions.fetchMyStore,
      openStatusWindow: statusWindowActions.open,
      openModalBox: modalBoxActions.open,
      closeModalBox: modalBoxActions.close,
    },
    dispatch,
  );
}
// Export Connected Component
export default connect(mapStateToProps, mapDispatchToprops)(Plans);
