import React, { Component, createRef } from 'react';
import { CSSTransition } from 'react-transition-group';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import DayJs from 'dayjs';
import axios from 'axios';
import { Link, NavLink, withRouter } from 'react-router-dom';

import {
  myStoreActions,
  statusWindowActions,
  modalBoxActions,
} from '../../../../Actions';
import { myStoreErrorTypes } from '../../../../Constants/mystore.constants';
import { getUIErrorMessage } from '../../../../Utils/errorUtils';
import ModalBoxWelcome from '../ModalBoxes/ModalBoxWelcome';
import ModalBoxStripe from '../ModalBoxes/ModalBoxStripe';
import Tab2Payments from './Tabs/Tab2/Tab2Payments';
import Standard from './Tables/Standard';
import Pro from './Tables/Pro';
import SkeletonTab1Manager from './Tabs/Skeletons/Tab1Manager';
import SeemorePlansWeb from './Tabs/Tab1/SeemorePlansWeb';
import SeemorePlansMobile from './Tabs/Tab1/SeemorePlansMobile';

const mainTabs = [
  {
    name: 'Resúmen',
    value: 1,
  },
  // {
  //   name: 'Pagos',
  //   value: 2,
  // },
];
const Fade = ({ children, ...props }) => (
  <CSSTransition {...props} timeout={1000} classNames="fade">
    {children}
  </CSSTransition>
);
class Myaccount extends Component {
  constructor(props) {
    super(props);
    this.state = {
      activeTab: 1,
      activeTable: 0,
      width: 0,
      paymentStatus: null,
      selectedPayment: null,
      paymentId: null,
    };
    this.tables = createRef();
    this.plans = createRef();
  }
  componentDidMount() {
    this.props.getMySavedCards();
    this.props.getUserProfile();
    this.props.getStripePlans();

    //paymentStatus
    this.checkPaymentStatus();

    //
    this.updateDimensions();
    window.addEventListener('resize', this.updateDimensions);
  }
  componentWillUnmount() {
    window.removeEventListener('resize', this.updateDimensions);
  }

  checkPaymentStatus = () => {
    if (!this.props.location) return;
    const paymentStatus = new URLSearchParams(this.props.location.search);

    //
    switch (paymentStatus.get('payment')) {
      case 'error':
        //this.setState({ paymentStatus: { status: 'cancelled', message: '' } });
        break;
      case 'success':
        this.setState({
          paymentStatus: {
            status: 'success',
            message:
              'Felicidades. Tu subscripción está siendo activada. Esta operación pueden tardar algunos minutos.',
          },
        });
        break;
      case 'cancelled':
        this.setState({
          paymentStatus: {
            status: 'cancelled',
            message:
              'Se produjo un error al procesar tu pago. Por favor prueba con otro método de pago o intentalo más tarde.',
          },
        });
        break;
      default:
        break;
    }
  };

  setActiveTab = (value) => {
    this.setState({ activeTab: value });
  };

  setActiveTable = (value) => {
    if (value === this.state.activeTable) {
      this.setState({ activeTable: 0 });
      window.scrollTo(0, this.plans.current.offsetTop);
    } else {
      this.setState({ activeTable: value });
      setTimeout(() => {
        window.scrollTo(50, this.tables.current.offsetTop);
      }, 300);
    }
  };

  updateDimensions = () => {
    this.setState({ width: window.innerWidth });
  };
  cancelCurrentPlan = () => {
    const comfirmation = confirm(
      '¿Estás seguro de querer cancelar tu subscripción?',
    );
    if (comfirmation) {
      this.props
        .cancelProPlan()
        .then(() => {
          if (this.props.storePlans.cancel_plan === 200) {
            this.props.fetchMyStore();
            this.props.openStatusWindow({
              type: 'success',
              message: 'Tu subscripción se ha cancelado correctamente.',
            });
          }
        })
        .catch((error) => {
          console.log(error, 'cancel plan');
          this.props.openStatusWindow({
            type: 'error',
            message:
              'No pudimos cancelar en este momento. Intenta más tarde o comunícate con nosotros.',
          });
        });
    }
  };
  payToSuscribeStripe = (infoToSubscribe) => {
    this.props
      .createStripeSubscription(infoToSubscribe)
      .then(() => {
        if (this.props.storePlans.createStripeSubscription === 200) {
          this.props.fetchMyStore();
          this.props.openStatusWindow({
            type: 'success',
            message: 'Tu pago se proceso correctamente.',
          });
        }
      })
      .catch((error) => {
        console.log(error, 'Subscribe Stripe');
        // Display UI error messages
        this.props.openStatusWindow({
          type: 'error',
          message: 'No pudimos procesar tu pago. Intenta más tarde.',
        });
      });
  };
  openWelcomeWindow() {
    this.props.openModalBox(() => (
      <ModalBoxWelcome closeModalBox={this.props.closeModalBox} />
    ));
  }
  openAddCard() {
    this.props.openModalBox(() => (
      <ModalBoxStripe
        savingACard={this.savingACard}
        closeModalBox={this.props.closeModalBox}
      />
    ));
  }

  savingACard = (id, cardInfo) => {
    this.props
      .saveCard({ stripe_card_token: id })
      .then(() => {
        if (this.props.storePlans?.saveACard === 200) {
          this.props.fetchMyStore();
          this.props.openStatusWindow({
            type: 'success',
            message: `Tarjeta ${cardInfo.brand.toUpperCase()} con terminación ${
              cardInfo.last4
            } se agrego correctamente`,
          });
          this.props.closeModalBox();
        }
      })
      .catch((err) => {
        console.log(err, 'save card');
        this.props.openStatusWindow({
          type: 'error',
          message: 'Tu tarjeta no pudo ser guardada.',
        });
        this.props.closeModalBox();
      });
  };

  requestPayPalPaymentDetails = () => {
    return this.props
      .subscribeToPayPal('pro-plan')
      .then((response) => {
        window.location = response.data.link;
      })
      .catch((err) => console.log(err));
  };
  setDefaultCard = (id) => {
    this.props
      .setDefaultSavedCard({ card_id: id })
      .then(() => {
        if (this.props.storePlans?.setDefault === 200) {
          this.props.fetchMyStore();
          this.props.openStatusWindow({
            type: 'success',
            message: 'Tarjeta actualizada correctamente.',
          });
        }
      })
      .catch((error) => {
        console.log(error, 'set default card');
        // Display UI error messages
        this.props.openStatusWindow({
          type: 'error',
          message: 'Se produjo un error. Intenta más tarde.',
        });
      });
  };
  deleteSavedCard = (id) => {
    this.props
      .deleteSavedCard(id)
      .then(() => {
        if (this.props.storePlans?.deleteCard === 204) {
          this.props.fetchMyStore();
          this.props.openStatusWindow({
            type: 'success',
            message: 'Tarjeta eliminada correctamente.',
          });
        }
      })
      .catch((error) => {
        console.log(error, 'Erase card');
        // Display UI error messages
        this.props.openStatusWindow({
          type: 'error',
          message: 'No pudimos eliminar tu tarjeta. Intenta más tarde.',
        });
      });
  };
  processSelectedPayment = () => {
    const stripeProPlan = this.props.storePlans?.stripePlans?.find(
      (plan) => plan.slug === 'pro-plan',
    );
    const infoToSubscribe = {
      plan: stripeProPlan?.stripe_id,
      stripe_payment_method_id: this.state.paymentId,
    };
    if (this.state.selectedPayment === null) {
      this.props.openStatusWindow({
        type: 'error',
        message: 'Selecciona una forma de pago',
      });
    }
    if (this.state.selectedPayment === 'stripe') {
      console.log(infoToSubscribe, 'info');
      this.props
        .createStripeSubscription(infoToSubscribe)
        .then(() => {
          if (this.props.storePlans.createStripeSubscription === 200) {
            this.props.fetchMyStore();
            this.props.openStatusWindow({
              type: 'success',
              message: 'Tu pago se proceso correctamente.',
            });
          }
        })
        .catch((error) => {
          console.log(error, 'Subscribe Stripe');
          // Display UI error messages
          this.props.openStatusWindow({
            type: 'error',
            message: 'No pudimos procesar tu pago, intenta más tarde.',
          });
        });
    }
    if (this.state.selectedPayment === 'paypal') {
      this.requestPayPalPaymentDetails();
    }
  };
  setSelectedPayment = (paymentMethod) => {
    this.setState({ selectedPayment: paymentMethod });
  };
  setPaymentId = (id) => {
    this.setState({ paymentId: id });
  };
  // subscribeToFreeTrial = () => {
  //   this.props
  //     .startFreeTrial()
  //     .then(() => {
  //       // if (this.props.storePlans.free_trial === 200) {
  //       //   this.openWelcomeWindow();
  //       // }
  //       console.log('welcome');
  //     })
  //     .catch((error) => {
  //       // Display UI error messages
  //       const uiMessage =
  //         error.response.data.non_field_errors ||
  //         getUIErrorMessage(error.response, myStoreErrorTypes);
  //       this.props.openStatusWindow({
  //         type: 'error',
  //         message: uiMessage,
  //       });
  //     });
  // };

  logoCardClass = (cardName) => {
    switch (cardName) {
      case 'mastercard':
        cardName = 'master';
        break;
      case 'diners':
        cardName = 'dinersClub';
        break;
      case 'unionpay':
        cardName = 'unionPay';
        break;
      default:
        cardName;
    }
    return `cr__icon--${cardName} cr__cardProvider-icon`;
  };

  Tab1Manager = () => {
    const { activeTable, width, paymentStatus } = this.state;
    const { myStore = {}, storePlans = {} } = this.props;
    const { plans = [] } = this.props?.storePlans;
    const listSavedCards = storePlans?.listSavedCards;
    const nextMonthPayment = myStore?.plan?.payment_provider.slug;
    const defaultPaymentIdStripe = myStore?.plan?.default_payment_method;
    const defaultCardId = storePlans?.userProfile?.stripe?.default_card_id;
    const daysToEndTrial = DayJs(
      DayJs(myStore?.plan?.next_payment_date).diff(DayJs()),
    ).format('DD');
    // const stripeProPlan = storePlans?.stripePlans?.find(
    //   (plan) => plan.slug === 'pro-plan'
    // );
    // const infoToSubscribe = {
    //   plan: stripeProPlan?.stripe_id,
    //   stripe_payment_method_id: defaultCardId,
    // };
    if (!plans.length) return null;

    // ** Format Vigencia del plan*/
    // const daysActivePlan =
    //   plans !== undefined &&
    //   DayJs(myStore.plan.next_payment_date).format('d MMMM YYYY');
    // const formatedFullDateActivePlan = [...daysActivePlan].join('');

    // Get subscription plans details
    const standardPlan = plans.find((plan) => plan.slug === 'free-plan');
    const proPlan = plans.find((plan) => plan.slug === 'pro-plan');
    const proPlanPrice = parseFloat(proPlan?.price);
    const standardPlanComission = standardPlan.variables
      .map((v) => v.value * 100)
      .join('');
    const proPlanComission = proPlan.variables.map((v) => v.value * 100).join('');
    // Current plan details
    const planStatus = myStore.plan.status.slug;
    const planExpiry = myStore.plan.next_payment_date;
    const planExpiryDateCount = DayJs(planExpiry).diff(DayJs(), 'day');
    const currentPlan = myStore?.plan?.plan;
    const nextMonthPlan = myStore?.plan?.next_plan?.slug;
    const isUsingTrial = currentPlan.slug === 'pro-plan' && planStatus === 'trial';

    // Return JSX
    return (
      <div className="cr__plansManager-mainTab ">
        {paymentStatus && (
          <div
            className={`cr__plansManager-mainTab-paymentStatus ${
              paymentStatus?.status === 'success' ? 'paymentSuccess' : 'paymentError'
            }`}
          >
            {paymentStatus?.message}
          </div>
        )}
        <div className="cr__plansManager-mainTab-section mainBorder">
          <div className="cr__text--paragraph cr__textColor--colorDark300 sectionTitle">
            <h5>Res&uacute;men</h5>
          </div>
          <div className="cr__plansManager-mainTab-content">
            <ul className="cr__plansManager-mainTab-content-item">
              <li className="cr__plansManager-mainTab-content-item-list cr__text--paragraph cr__textColor--colorDark300 border">
                <div>
                  <h6 className="cr__text--paragraph cr__textColor--colorDark300">
                    Plan
                  </h6>
                </div>
                <div>
                  <span className="cr__text--paragraph cr__textColor--colorDark400">
                    {currentPlan?.name.replace('Plan', '')}
                  </span>
                  <span className="cr__text--caption cr__textColor--colorGray300">
                    {isUsingTrial && '(Versión prueba)'}
                  </span>

                  {isUsingTrial && planExpiryDateCount < 10 && (
                    <span className="cr__text--caption cr__textColor--colorYellow400 underline">
                      quedan {planExpiryDateCount} días.
                    </span>
                  )}
                </div>
              </li>
              <li className="cr__plansManager-mainTab-content-item-list cr__text--paragraph cr__textColor--colorDark300 border">
                <div>
                  <h6 className="cr__text--paragraph cr__textColor--colorDark300">
                    Fin del periodo
                  </h6>
                </div>
                <div>
                  <h6 className="cr__text--paragraph cr__textColor--colorDark400">
                    {DayJs(planExpiry).format('DD MMMM YYYY')}
                  </h6>
                </div>
              </li>
              <li className="cr__plansManager-mainTab-content-item-list cr__text--paragraph cr__textColor--colorDark300 border">
                <div>
                  <h6 className="cr__text--paragraph cr__textColor--colorDark300">
                    Tarifa mensual
                  </h6>
                </div>
                <div>
                  <h6 className="cr__text--paragraph cr__textColor--colorDark400">
                    {currentPlan?.slug.includes('free')
                      ? 'Gratis'
                      : `$${proPlanPrice}`}
                  </h6>
                </div>
              </li>
              <li className="cr__plansManager-mainTab-content-item-list cr__text--paragraph cr__textColor--colorDark300">
                <div>
                  <h6 className="cr__text--paragraph cr__textColor--colorDark300">
                    Comisi&oacute;n por venta
                  </h6>
                </div>
                <div>
                  <h6 className="cr__text--paragraph cr__textColor--colorDark400">
                    {currentPlan?.slug.includes('free')
                      ? `${standardPlanComission}%`
                      : `${proPlanComission}%`}
                  </h6>
                </div>
              </li>
            </ul>
          </div>
        </div>
        <div className="cr__plansManager-mainTab-section mainBorder">
          <div className="cr__text--paragraph cr__textColor--colorDark300 sectionTitle">
            <h5>M&eacute;todos de pago</h5>
            <p>
              Los cambios a tu m&eacute;todo de pago se efectuar&aacute;n
              inmediatamente. Los cargos futuros se realizar&aacute;n a esta cuenta.
            </p>
          </div>
          <div className="cr__plansManager-mainTab-content">
            <div className="cr__plansManager-mainTab-content-title">
              <h6 className="cr__text--subtitle3 cr__textColor--colorDark300">
                Selecciona una forma de pago.
              </h6>
            </div>
            <div>
              {Array.isArray(listSavedCards) && listSavedCards.length ? (
                <ul>
                  {listSavedCards.map(({ id, card }) => (
                    <div
                      key={id}
                      className="cr__plansManager-mainTab-content-cardList"
                      onClick={() => {
                        this.setSelectedPayment('stripe');
                        this.setPaymentId(id);
                      }}
                    >
                      <input
                        type="radio"
                        name="setDefault"
                        value={id}
                        checked={
                          (this.state.selectedPayment === 'stripe' &&
                            id === this.state.paymentId) ||
                          (nextMonthPlan === 'pro-plan' &&
                            nextMonthPayment === 'stripe' &&
                            defaultPaymentIdStripe === id)
                        }
                        className="cr__plansManager-mainTab-content-cardList-input"
                        onChange={() => {
                          this.setSelectedPayment('stripe');
                          this.setPaymentId(id);
                        }}
                      />
                      <label>
                        <div>
                          <i
                            className={this.logoCardClass(card?.brand)}
                            aria-label={`${card?.brand} Logo`}
                          />
                          <span className="cr__text--paragraph cr__textColor--colorDark300">
                            •••• {card.last4}
                          </span>
                        </div>
                        <div className="cr__plansManager-mainTab-content-cardList-exp">
                          <span className="cr__text--paragraph cr__textColor--colorDark300">
                            {card.exp_month.toString().length === 1
                              ? '0' + card.exp_month
                              : card.exp_month}
                            /
                          </span>
                          <span className="cr__text--paragraph cr__textColor--colorDark300">
                            {card.exp_year.toString().substring(2, 4)}
                          </span>
                          <a onClick={() => this.deleteSavedCard(id)}>
                            <i className="cr__icon--trash cr__text--paragraph"></i>
                          </a>
                        </div>
                      </label>
                    </div>
                  ))}
                </ul>
              ) : (
                <h6 className="cr__text--paragraph cr__textColor--colorGray300 cr__plansManager-mainTab-content-withoutCards">
                  No tienes ninguna tarjeta guardada
                </h6>
              )}
              <div className="cr__plansManager-mainTab-content-addPayment">
                <a
                  className="cr__text--paragraph cr__textColor--colorMain300"
                  onClick={() => this.openAddCard()}
                >
                  Agregar tarjeta
                </a>
              </div>
              <div
                className="cr__plansManager-mainTab-content-paypal"
                onClick={() => this.setSelectedPayment('paypal')}
              >
                <input
                  type="radio"
                  name="setDefault"
                  checked={
                    (nextMonthPlan === 'pro-plan' &&
                      nextMonthPayment === 'paypal') ||
                    this.state.selectedPayment === 'paypal'
                    //  &&
                    //   !defaultPaymentIdStripe
                  }
                  onChange={() => {
                    this.setSelectedPayment('paypal');
                  }}
                />
                <label>
                  <img
                    src={require('../../../../../images/icons/payment/icon_paypal.svg')}
                    alt="paypal"
                  />
                  Paypal
                </label>
              </div>
            </div>
            <div className="cr__plansManager-mainTab-content-subscribe">
              {nextMonthPlan === 'free-plan' && (
                <button onClick={() => this.processSelectedPayment()}>
                  Subscribirme
                </button>
              )}
            </div>
          </div>
        </div>
        <div className="cr__plansManager-mainTab-section">
          <div className="cr__text--paragraph cr__textColor--colorDark300 titles">
            <h5 ref={this.Plans}>Planes</h5>
            <p>Elige el plan que se ajuste mejor a las necesidades de tu negocio.</p>
          </div>
          <div className="cr__plansManager-selection">
            <ul className="cr__plansManager-selection-list">
              {width >= 768 ? (
                <SeemorePlansWeb
                  myStore={myStore}
                  activeTable={activeTable}
                  proPlanPrice={proPlanPrice}
                  setActiveTable={this.setActiveTable}
                  // subscribeToFreeTrial={this.subscribeToFreeTrial}
                  cancelCurrentPlan={this.cancelCurrentPlan}
                  nextMonthPlan={nextMonthPlan}
                  processSelectedPayment={this.processSelectedPayment}
                />
              ) : (
                <SeemorePlansMobile
                  myStore={myStore}
                  activeTable={activeTable}
                  proPlanPrice={proPlanPrice}
                  setActiveTable={this.setActiveTable}
                  // subscribeToFreeTrial={this.subscribeToFreeTrial}
                  cancelCurrentPlan={this.cancelCurrentPlan}
                  nextMonthPlan={nextMonthPlan}
                  openModalBox={this.props.openModalBox}
                  closeModalBox={this.props.close}
                  processSelectedPayment={this.processSelectedPayment}
                />
              )}
            </ul>
          </div>
        </div>
        <Fade in={activeTable !== 0}>
          <div className="cr__plansManager-tables" ref={this.tables}>
            {activeTable === 1 && <Standard comission={standardPlanComission} />}
            {activeTable === 2 && (
              <Pro comission={proPlanComission} price={proPlanPrice} />
            )}
          </div>
        </Fade>
      </div>
    );
  };
  render() {
    if (this.props.storePlans.loading) return <SkeletonTab1Manager />;
    const { activeTab } = this.state;
    return (
      <div className="cr__plansManager">
        <h6 className="cr__plansManager-title cr__text--subtitle cr__textColor--colorDark400">
          Subscripci&oacute;n
        </h6>
        <div className="cr__plansManager-tabs">
          <ul className="cr__plansManager-tabs-list mainBorder">
            {mainTabs.map(({ value, name }) => (
              <li
                key={value}
                onClick={() => this.setActiveTab(value)}
                className={`cr__plansManager-tabs-list-item cr__text--subtitle3 cr__textColor--colorDark300 ${
                  activeTab === value ? 'activeTab' : ''
                }`}
              >
                {name}
              </li>
            ))}
          </ul>
        </div>
        {activeTab === 1 && <this.Tab1Manager />}
        {/* {activeTab === 2 && <Tab2Payments />} */}
      </div>
    );
  }
}

// Redux Map Functions
function mapStateToProps(state) {
  const { myStore } = state;
  return {
    storePlans: myStore.store_plans,
  };
}

function mapDispatchToprops(dispatch) {
  return bindActionCreators(
    {
      cancelProPlan: myStoreActions.cancelPlan,
      subscribeToPayPal: myStoreActions.subscribeToPayPal,
      getMySavedCards: myStoreActions.getMySavedCards,
      deleteSavedCard: myStoreActions.deleteSavedCard,
      getStripePlans: myStoreActions.getStripePlans,
      createStripeSubscription: myStoreActions.createStripeSubscription,
      saveCard: myStoreActions.saveCard,
      setDefaultSavedCard: myStoreActions.setDefaultSavedCard,
      getUserProfile: myStoreActions.getUserProfile,
    },
    dispatch,
  );
}
// Connect Component
Myaccount = connect(mapStateToProps, mapDispatchToprops)(Myaccount);

// Wrap componente with router
export default withRouter(Myaccount);
