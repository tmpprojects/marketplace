import React from 'react';
import { Provider } from 'react-redux';
import renderer from 'react-test-renderer';
import Enzyme, { shallow } from 'enzyme';
import EnzymeAdapter from 'enzyme-adapter-react-16';

import Myaccount from './Myaccount';

Enzyme.configure({ adapter: new EnzymeAdapter() });

test('Component connected to store.', () => {
  let store;
  let component;
  beforeEach(() => {
    store = mockStore({
      myState: 'sample text',
    });
    component = renderer.create(
      <Provider store={store}>
        <Myaccount />
      </Provider>,
    );
  });
});
