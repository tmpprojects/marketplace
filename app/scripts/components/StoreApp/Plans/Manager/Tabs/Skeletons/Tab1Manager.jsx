import React from 'react';

export default function SkeletonTab1Manager() {
  return (
    <div className="cr__skeleton-tab1">
      <h6 className="cr__plansManager-title cr__text--subtitle cr__textColor--colorDark400">
        Subscripci&oacute;n
      </h6>
      <div className="cr__skeleton-tab1-section1 ">
        <ul className="cr__skeleton-tab1-section1-list ">
          <li className="cr__skeleton-tab1-section1-item1 styles" />
          <li className="cr__skeleton-tab1-section1-item2 styles" />
        </ul>
      </div>
      <div className="cr__skeleton-tab1-section2 ">
        <ul className="cr__skeleton-tab1-section2-list ">
          <li className="cr__skeleton-tab1-section2-item1">
            <ul>
              <li className="styles" />
              <li className="styles" />
            </ul>
          </li>
          <li className="cr__skeleton-tab1-section2-item2 styles" />
        </ul>
      </div>
      <div className="cr__skeleton-tab1-section3 ">
        <ul className="cr__skeleton-tab1-section3-list ">
          <li className="cr__skeleton-tab1-section3-item1">
            <ul>
              <li className="styles" />
              <li className="styles" />
            </ul>
          </li>
          <li className="cr__skeleton-tab1-section3-item2">
            <ul>
              <li className="styles" />
              <li className="styles" />
            </ul>
          </li>
        </ul>
      </div>
    </div>
  );
}
