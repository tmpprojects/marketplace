import React from 'react';
import checkmark from '../../../../../../../images/plans/checkmark.svg';

export default function SeemorePlansWeb(props) {
  const {
    myStore,
    activeTable,
    proPlanPrice,
    setActiveTable,
    // subscribeToFreeTrial,
    cancelCurrentPlan,
    nextMonthPlan,
    processSelectedPayment,
  } = props;
  return (
    <React.Fragment>
      <li className="cr__plansManager-selection-list-item">
        <div className="cr__plans-selection-container">
          <div className="cr__plansManager-selection-container-double">
            <img
              src={require('../../../../../../../images/plans/standardC.svg')}
              className="cr__plansManager-selection-container-iconStandard"
              alt="Icono plan estándar"
              aria-label="Icono plan estándar"
            />
            <div className="text-mobile cr__textColor--colorDark300">
              <h5 className="cr__plansManager-selection-container-title cr__text--subtitle2">
                Standard
              </h5>
              <h6 className="cr__plansManager-selection-container-subtitle cr__text--paragraph">
                Siempre gratuito
              </h6>
              <p className="cr__plansManager-selection-container-paragraph cr__text--paragraph cr__textColor--colorGray400">
                Todo lo que necesitas para comenzar a vender en l&iacute;nea
              </p>
              <div className="cr__plansManager-selection-container-buttons">
                {myStore.plan.plan.slug === 'free-plan' ? (
                  <h5 className="cr__plansManager-selection-container-plan cr__text--paragraph">
                    <img src={checkmark} alt="Palomeado" className="checkmark" />
                    Este es tu plan actual
                  </h5>
                ) : (
                  nextMonthPlan !== 'free-plan' && (
                    <button
                      className="cr__plansManager-selection-container-button cr__text--caption"
                      onClick={() => cancelCurrentPlan()}
                      aria-label="Regresar a plan Standard"
                      data-test="subscribe-to-plan"
                    >
                      Regresar a Standard
                    </button>
                  )
                )}
              </div>
            </div>
          </div>
          {/* <div
            className='cr__plansManager-table-standard cr__text--paragraph cr__textColor--colorBlack300'
            onClick={() => setActiveTable(1)}
          >
            <span>Ver m&aacute;s beneficios</span>
            <img
              src={require('../../../../../../../images/plans/arrowdown.svg')}
              alt='Mostrar más'
              className={`${activeTable === 1 ? 'rotate' : ''}`}
            />
          </div> */}
        </div>
      </li>
      <li className="cr__plansManager-selection-list-item">
        <div className="cr__plansManager-selection-container">
          <div className="cr__plansManager-selection-container-double">
            <img
              src={require('../../../../../../../images/plans/proC.svg')}
              className="cr__plansManager-selection-container-iconPro"
              alt="Icono plan pro"
              aria-label="Icono plan pro"
            />
            <div className="text-mobile cr__textColor--colorDark300">
              <h5 className="cr__plansManager-selection-container-title cr__text--subtitle2 ">
                Plus
              </h5>
              <h6 className="cr__plansManager-selection-container-subtitle cr__text--paragraph">
                ${proPlanPrice}MX mensuales
              </h6>
              <p className="cr__plansManager-selection-container-paragraph cr__text--paragraph cr__textColor--colorGray400">
                Mejores tarifas, soporte especializado y herramientas para crecer tu
                negocio
              </p>
              <div className="cr__plansManager-selection-container-buttons">
                {myStore?.plan?.plan?.slug === 'pro-plan' ? (
                  <h5 className="cr__plansManager-selection-container-plan cr__text--paragraph">
                    <img src={checkmark} alt="Palomeado" className="checkmark" />
                    Este es tu plan actual
                  </h5>
                ) : (
                  <button
                    className="cr__plansManager-selection-container-button cr__text--paragraph"
                    // onClick={() => subscribeToFreeTrial()}
                    onClick={() => processSelectedPayment()}
                    aria-label="Probar 30 días gratis"
                    data-test="subscribe-to-plan"
                  >
                    Seleccionar
                  </button>
                )}
              </div>
            </div>
          </div>
          {/* <div
            className='cr__plansManager-table-pro cr__text--paragraph cr__textColor--colorBlack300'
            onClick={() => setActiveTable(2)}
          >
            <span>Ver m&aacute;s beneficios</span>
            <img
              src={require('../../../../../../../images/plans/arrowdown.svg')}
              alt='Mostrar más'
              className={`${activeTable === 2 ? 'rotate' : ''}`}
            />
          </div> */}
        </div>
      </li>
    </React.Fragment>
  );
}
