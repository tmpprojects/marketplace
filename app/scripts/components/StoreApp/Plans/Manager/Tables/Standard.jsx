import React from 'react';
import checkmark from '../../../../../../images/plans/checkmark.svg';
import line from '../../../../../../images/plans/horizontal-line.svg';

export default function Standard(props) {
  return (
    <div id="activeTable" className="cr__plansManager-tables-standard">
      <table className="cr__textColor--colorDark300">
        <tbody>
          <tr>
            <td className="cr__text--subtitle3 section-parent single-margin">
              Tarifas
            </td>
            <td className="cr__text--subtitle3 section-parent" />
          </tr>
          <tr>
            <td className="cr__text--paragraph section-child">Tarifa mensual</td>
            <td className="cr__text--paragraph section-child right weight">
              Gratis
            </td>
          </tr>
          <tr>
            <td className="cr__text--paragraph section-child">
              Comisi&oacute;n por venta
            </td>
            <td className="cr__text--paragraph section-child right">
              {`${props.comission}%`}
            </td>
          </tr>
          <tr>
            <td className="cr__text--subtitle3 section-parent extra-margin">
              Beneficios
            </td>
            <td className="section-parent" />
          </tr>
          <tr>
            <td className="cr__text--paragraph section-child">
              Tienda On-Line (Incluye página web para vender en línea)
            </td>
            <td className="cr__text--paragraph section-child right">
              <img src={checkmark} alt="checkmark" className="checkmark" />
            </td>
          </tr>
          <tr>
            <td className="cr__text--paragraph section-child">
              Productos ilimitados
            </td>
            <td className="cr__text--paragraph section-child right">
              <img src={checkmark} alt="checkmark" className="checkmark" />
            </td>
          </tr>
          <tr>
            <td className="cr__text--paragraph section-child">
              Tus clientes pueden pagar con:
            </td>
            <td className="section-child" />
          </tr>
          <tr>
            <td className="cr__text--paragraph section-child">
              <h6 className="marginLeft">
                Tarjeta de Cr&eacute;dito y D&eacute;bito
              </h6>
            </td>
            <td className="cr__text--paragraph section-child right">
              <img src={checkmark} alt="checkmark" className="checkmark" />
            </td>
          </tr>
          <tr>
            <td className="cr__text--paragraph section-child">
              <h6 className="marginLeft">Paypal</h6>
            </td>
            <td className="cr__text--paragraph section-child right">
              <img src={checkmark} alt="checkmark" className="checkmark" />
            </td>
          </tr>
          <tr>
            <td className="cr__text--paragraph section-child">
              <h6 className="marginLeft">D&eacute;positos y Transferencias</h6>
            </td>
            <td className="cr__text--paragraph section-child right">
              <img src={checkmark} alt="checkmark" className="checkmark" />
            </td>
          </tr>
          <tr>
            <td className="cr__text--paragraph section-child">
              <h6 className="marginLeft">Pago en OXXO</h6>
            </td>
            <td className="cr__text--paragraph section-child right">
              <img src={checkmark} alt="checkmark" className="checkmark" />
            </td>
          </tr>
          <tr>
            <td className="cr__text--paragraph section-child">Ventas seguras</td>
            <td className="cr__text--paragraph section-child right">
              <img src={checkmark} alt="checkmark" className="checkmark" />
            </td>
          </tr>
          <tr>
            <td className="cr__text--subtitle3 section-parent extra-margin">
              Servicios
            </td>
            <td className="cr__text--subtitle3 section-parent" />
          </tr>
          <tr>
            <td className="cr__text--paragraph section-child">Env&iacute;os:</td>
            <td className="section-child" />
          </tr>
          <tr>
            <td className="cr__text--paragraph section-child">
              <h6 className="marginLeft">
                Env&iacute;os express* y env&iacute;os de paqueter&iacute;a
                nacionales
              </h6>
            </td>
            <td className="cr__text--paragraph section-child right">
              <img src={checkmark} alt="checkmark" className="checkmark" />
            </td>
          </tr>
          {/* <tr>
            <td className="cr__text--paragraph section-child">
              <h6 className="marginLeft">
                Tarifa preferencial para paqueter&iacute;a nacional
              </h6>
            </td>
            <td className="cr__text--paragraph section-child right">
              <img src={line} alt="horizontal line" className="checkmark" />
            </td>
          </tr> */}
          <tr>
            <td className="cr__text--paragraph section-child">
              Atenci&oacute;n a clientes:
            </td>
            <td className="section-child" />
          </tr>
          <tr>
            <td className="cr__text--paragraph section-child">
              <h6 className="marginLeft">Servicio a compradores</h6>
            </td>
            <td className="cr__text--paragraph section-child right">
              <img src={checkmark} alt="checkmark" className="checkmark" />
            </td>
          </tr>
          <tr>
            <td className="cr__text--paragraph section-child">
              <h6 className="marginLeft">
                Servicio a tiendas por correo electr&oacute;nico{' '}
              </h6>
            </td>
            <td className="cr__text--paragraph section-child right">
              <img src={checkmark} alt="checkmark" className="checkmark" />
            </td>
          </tr>
          <tr>
            <td className="cr__text--paragraph section-child">
              <h6 className="marginLeft">
                Servicio personalizado de Category Manager
              </h6>
            </td>
            <td className="cr__text--paragraph section-child right">
              <img src={line} alt="horizontal line" className="checkmark" />
            </td>
          </tr>
          {/* <tr>
            <td className="cr__text--paragraph section-child">
              Soporte T&eacute;cnico
            </td>
            <td className="cr__text--paragraph section-child right">
              <img src={line} alt="horizontal line" className="checkmark" />
            </td>
          </tr> */}
          <tr>
            <td className="cr__text--subtitle3 section-parent extra-margin">
              Herramientas
            </td>
            <td className="cr__text--subtitle3 section-parent" />
          </tr>
          <tr>
            <td className="cr__text--paragraph section-child">Tablero de Control</td>
            <td className="cr__text--paragraph section-child right">
              <img src={checkmark} alt="checkmark" className="checkmark" />
            </td>
          </tr>
          <tr>
            <td className="cr__text--paragraph section-child">
              Gesti&oacute;n de pautas y servicios PRO (adquiridos aparte) sin costo
              adicional
            </td>
            <td className="cr__text--paragraph section-child right">
              <img src={line} alt="horizontal line" className="checkmark" />
            </td>
          </tr>
          <tr>
            <td className="cr__text--paragraph section-child">
              Estad&iacute;sticas y Reportes Avanzados
            </td>
            <td className="cr__text--paragraph section-child right">
              <img src={line} alt="horizontal line" className="checkmark" />
            </td>
          </tr>
          <tr>
            <td className="cr__text--paragraph section-child">
              Chat directo con clientes
            </td>
            <td className="cr__text--paragraph section-child right">
              <img src={line} alt="horizontal line" className="checkmark" />
            </td>
          </tr>
          <tr>
            <td className="cr__text--paragraph section-child">
              Descuentos y cupones
            </td>
            <td className="cr__text--paragraph section-child right">
              <img src={line} alt="horizontal line" className="checkmark" />
            </td>
          </tr>
        </tbody>
      </table>
    </div>
  );
}
