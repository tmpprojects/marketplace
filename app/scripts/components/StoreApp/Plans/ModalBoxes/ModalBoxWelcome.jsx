import React, { Component } from 'react';
import '../../../../../styles/storeApp/Plans/_modalboxes.scss';

export default class ModalBoxCards extends Component {
  render() {
    const { closeModalBox } = this.props;
    return (
      <div className="cr__plans__modalBox-welcome">
        <img
          src={require('../../../../../images/plans/leftupB.svg')}
          alt="Figura Banner"
          className="cr__plans__modalBox-welcome-lu"
        />
        <img
          src={require('../../../../../images/plans/leftdown.svg')}
          alt="Figura Banner"
          className="cr__plans__modalBox-welcome-ld"
        />
        <div className="cr__plans__modalBox-welcome-text">
          <h6 className="cr__plans__modalBox-welcome-text-title cr__text--subtitle2 cr__textColor--colorDark300">
            BIENVENIDO A{' '}
          </h6>
          <h6 className="cr__plans__modalBox-welcome-text-title cr__text--subtitle2 cr__textColor--colorDark300">
            CANASTA ROSA <span className="cr__textColor--colorMain300">PRO</span>
          </h6>
          <span className="cr__plans__modalBox-welcome-text-paragraph cr__text--paragraph cr__textColor--colorDark100">
            A partir de este momento, disfrutas de tarifas m&aacute;s bajas, soporte
            especializado y tendr&aacute;s acceso a herramientas exclusivas para
            crecer tu negocio.
          </span>{' '}
          <br />
          <a
            onClick={() => closeModalBox()}
            className="cr__plans__modalBox-welcome-text-button cr__text--paragraph cr__textColor--colorDark300"
          >
            Continuar
          </a>
        </div>
        <img
          src={require('../../../../../images/plans/rightdownB.svg')}
          alt="Figura Banner"
          className="cr__plans__modalBox-welcome-rd"
        />
        <img
          src={require('../../../../../images/plans/leftdownB.svg')}
          alt="Figura Banner"
          className="cr__plans__modalBox-welcome-rd2"
        />
        <img
          src={require('../../../../../images/plans/rightdownB.svg')}
          alt="Figura Banner"
          className="cr__plans__modalBox-welcome-rd3"
        />
      </div>
    );
  }
}
