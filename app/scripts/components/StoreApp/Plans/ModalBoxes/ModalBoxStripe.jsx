import React, { useState } from 'react';
import {
  CardNumberElement,
  CardCvcElement,
  CardExpiryElement,
  useStripe,
  useElements,
} from '@stripe/react-stripe-js';
import '../../../../../styles/storeApp/Plans/_modalboxes.scss';

export default function ModalBoxStripe(props) {
  const stripe = useStripe();
  const elements = useElements();
  const [errorForm, setErrorForm] = useState('');
  const [errorCode, setErrorCode] = useState('');
  const { closeModalBox, savingACard } = props;
  const handleSubmit = async (event) => {
    event.preventDefault();
    if (!stripe || !elements) return;

    const { error, paymentMethod } = await stripe.createPaymentMethod({
      type: 'card',
      card: elements.getElement(CardNumberElement),
    });

    if (!error) {
      const { id, card } = paymentMethod;
      savingACard(id, card);
    }
    if (error) {
      setErrorForm(error?.message);
      setErrorCode(error?.code);
      console.log(error?.message);
    }
  };

  const ELEMENT_OPTIONS = {
    style: {
      base: {
        fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
        fontSmoothing: 'antialiased',
        fontSize: '1.05em',
        '::placeholder': {
          color: '#B7B7B7',
        },
      },
      invalid: {
        color: '#fa755a',
        iconColor: '#fa755a',
        ':focus': {
          color: '#fa755a',
        },
      },
      complete: {
        border: '1px solid #1eb592',
      },
    },
  };
  return (
    <div className="cr__plans__modalBox-stripe">
      <div className="cr__plans__modalBox-stripe-header">
        <span className="cr__text--subtitle3 cr__textColor--colorDark300">
          Agregar tarjeta
        </span>
        <span
          className="cr__text--subtitle3 cr__plans__modalBox-stripe-header-x"
          onClick={() => closeModalBox()}
        >
          X
        </span>
      </div>
      {errorForm !== '' && (
        <div className="cr__plans__modalBox-stripe-error">
          <p className="cr__text--caption cr__textColor--colorDark300">
            {errorForm}
          </p>
        </div>
      )}
      <div className="cr__plans__modalBox-stripe-form">
        <form onSubmit={handleSubmit}>
          <div className="cr__plans__modalBox-stripe-form-cardNumberContainer">
            <label
              htmlFor="cardNumber"
              className="cr__text--paragraph cr__textColor--colorDark300"
            >
              N&uacute;mero de la tarjeta
            </label>
            <div
              className={`cardElement ${
                errorCode === 'incomplete_number' ? 'borderError' : ''
              }`}
            >
              <CardNumberElement id="cardNumber" options={ELEMENT_OPTIONS} />
            </div>
          </div>
          <div className="cr__plans__modalBox-stripe-form-expiryCvcContainer">
            <div className="cr__plans__modalBox-stripe-form-expiryContainer">
              <label
                htmlFor="expiry"
                className="cr__text--paragraph cr__textColor--colorDark300"
              >
                Expiraci&oacute;n
              </label>
              <div
                className={`cardElement ${
                  errorCode === 'incomplete_expiry' ? 'borderError' : ''
                }`}
              >
                <CardExpiryElement id="expiry" options={ELEMENT_OPTIONS} />
              </div>
            </div>
            <div className="cr__plans__modalBox-stripe-form-cvcContainer">
              <label
                htmlFor="cvc"
                className="cr__text--paragraph cr__textColor--colorDark300"
              >
                CVC
              </label>
              <div
                className={`cardElement ${
                  errorCode === 'incomplete_cvc' ? 'borderError' : ''
                }`}
              >
                <CardCvcElement id="cvc" options={ELEMENT_OPTIONS} />
              </div>
            </div>
          </div>
          <div className="cr__plans__modalBox-stripe-buttons">
            <button
              className="cr__plans__modalBox-stripe-buttons-cancel cr__text--caption"
              onClick={() => closeModalBox()}
            >
              Cancelar
            </button>
            <button
              className="cr__plans__modalBox-stripe-buttons-add cr__text--caption"
              type="submit"
              disabled={!stripe}
            >
              Agregar tarjeta
            </button>
          </div>
        </form>
      </div>
    </div>
  );
}
