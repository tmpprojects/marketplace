import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { myStoreActions } from '../../../Actions';
import { IconPreloader } from '../../../Utils/Preloaders';
import Plans from './Plans/Plans';
import Manager from './Manager/Myaccount';

class Main extends Component {
  render() {
    if (this.props.storePlans.loading) return <IconPreloader />;

    const { myStore = {} } = this.props;
    const currentPlan = myStore.plan.plan.slug;

    return (
      <React.Fragment>
        {currentPlan === 'free-plan' ? <Plans /> : <Manager />}
      </React.Fragment>
    );
  }
}

// Redux Map Functions
function mapStateToProps(state) {
  const { myStore } = state;
  return {
    storePlans: myStore.store_plans,
  };
}

function mapDispatchToprops(dispatch) {
  return bindActionCreators(
    {
      getMyStorePlans: myStoreActions.getMyStorePlans,
    },
    dispatch,
  );
}
// Export Connected Component
export default connect(mapStateToProps, mapDispatchToprops)(Main);
