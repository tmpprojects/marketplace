import React from 'react';
import PropTypes from 'prop-types';
import RatingStar from './RatingStar';
import { range } from '../../Utils/genericUtils';

/*---------------------------------------------------
    CHECKBOX LIST SELECTOR UI COMPONENT
---------------------------------------------------*/
class Rating extends React.Component {
  ratingRange = range(1, 5);
  state = {
    hoverDisplay: false,
    hoverRating: 0,
  };

  onClick = (e, value) => {
    // Bubble up event
    this.props.onClick(e, value);
  };

  renderStar = (index) => {
    const { value: rating = 0 } = this.props;
    const ratingPercentage = (rating / 5) * 100;
    const ratingSteps = 100 / this.ratingRange.length;
    const coloredStars = ratingPercentage / ratingSteps;

    //console.log(ratingPercentage, ratingSteps, coloredStars)
    const reverseOffset = this.ratingRange.length - (index - 1);
    //console.log(`${rating} -> ${ratingPercentage}%: `, index, ratingPercentage - (ratingPercentage-(reverseOffset * ratingSteps)), (ratingSteps * index))
    //console.log(ratingPercentage, reverseOffset, (reverseOffset * ratingSteps), ratingPercentage - (reverseOffset * ratingSteps));
    console.log(
      ratingPercentage - reverseOffset * ratingSteps,
      ratingSteps * index,
      (ratingPercentage - reverseOffset * ratingSteps) / (ratingSteps * index),
    );

    return (
      <RatingStar
        disabled
        value={(reverseOffset * ratingSteps) / (ratingSteps * index)}
        key={index}
        index={index}
        label={index}
        onClick={onClick}
      />
    );
  };

  enableHoverRating = (index) => {
    this.setState({ hoverDisplay: true, hoverRating: index });
  };
  disableHoverRating = (index) => {
    this.setState({ hoverDisplay: false, hoverRating: 0 });
  };

  render() {
    const { interactive, rating = 0 } = this.props;
    const { hoverDisplay, hoverRating } = this.state;
    let totalRating = Math.round(rating);

    if (hoverDisplay) {
      totalRating = Math.round(hoverRating);
    }

    const ratingStars = this.ratingRange.map((index) => (
      <RatingStar
        interactive={interactive}
        key={index}
        active={index <= totalRating}
        index={index}
        label={index}
        onClick={(e) => this.onClick(e, index)}
        enableHoverRating={this.enableHoverRating}
        disableHoverRating={this.disableHoverRating}
      />
    ));

    return (
      <div className="ui-list-rating">
        <ul className="ui-list-rating__stars">{ratingStars}</ul>
      </div>
    );
  }
}

Rating.propTypes = {
  disabled: PropTypes.bool,
  interactive: PropTypes.bool,
  rating: PropTypes.number.isRequired,
  onClick: PropTypes.func,
};
Rating.defaultProps = {
  disabled: true,
  interactive: false,
  onClick: () => false,
};

export default Rating;
