import React from 'react';
import { Component } from 'react';
import PropTypes from 'prop-types';
import Rating from './Rating';

/*---------------------------------------------------
    CHECKBOX LIST SELECTOR UI COMPONENT
---------------------------------------------------*/
export class RatingDetails extends Component {
  render() {
    return (
      <div className="ui-list-rating">
        <ul>
          <li>
            <Rating />
          </li>
          <li>
            <Rating />
          </li>
          <li>
            <Rating />
          </li>
          <li>
            <Rating />
          </li>
          <li>
            <Rating />
          </li>
        </ul>
      </div>
    );
  }
}
