import React from 'react';
import PropTypes from 'prop-types';

/*---------------------------------------------------
    CHECKBOX LIST SELECTOR UI COMPONENT
---------------------------------------------------*/
class RatingStar extends React.Component {
  constructor(props) {
    super(props);
    this.ratingStar = React.createRef();
  }
  componentDidMount() {
    // If component is not 'interactive' add event listeners
    if (this.props.interactive) {
      this.ratingStar.current.addEventListener('mouseover', this.onMouseOver);
      this.ratingStar.current.addEventListener('mouseout', this.onMouseOut);
    }
  }
  componentWillUnmount() {
    // Remove Listeners
    if (!this.props.interactive) {
      this.ratingStar.current.removeEventListener('mouseover', this.onMouseOver);
      this.ratingStar.current.removeEventListener('mouseout', this.onMouseOut);
    }
  }

  onMouseOver = () => {
    this.props.enableHoverRating(this.props.index);
  };
  onMouseOut = () => {
    this.props.disableHoverRating(this.props.index);
  };

  renderSvgObject = () => (
    <svg
      width="0"
      height="0"
      viewBox="0 0 16 15"
      version="1.1"
      xmlns="http://www.w3.org/2000/svg"
    >
      <defs>
        <clipPath id="ratingStar" fill="#9f609c" stroke="#BABABA" strokeWidth="0.3">
          <polygon
            points="8,12.2 3.3,14.6 4.2,9.4 0.4,5.7 5.6,4.9 8,0.2 10.4,4.9 15.6,5.7 11.8,9.4 
12.7,14.6 "
          />
        </clipPath>
      </defs>
    </svg>
  );

  render() {
    const { active, label, onClick } = this.props;
    const backgroundPosition = active ? 1 : 0;

    return (
      <li className="star__item" ref={this.ratingStar}>
        {/*<input id="1"
                    name=""
                    type="checkbox"
                    value=""
                />
                <label className="checkmark" htmlFor="1" >1</label>*/}
        {/* <div className="slider" style={{ width: `${backgroundPosition * 100}%` }} /> */}
        <button
          type="button"
          className={`icon ${active ? 'active' : ''}`}
          onClick={onClick}
        >
          {label}
        </button>
        {/* {this.renderSvgObject()} */}
      </li>
    );
  }
}

RatingStar.propTypes = {
  label: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  interactive: PropTypes.bool,
  active: PropTypes.bool,
  enableHoverRating: PropTypes.func,
  disableHoverRating: PropTypes.func,
  onClick: PropTypes.func.isRequired,
};
RatingStar.defaultProps = {
  label: 1,
  interactive: false,
  active: false,
  enableHoverRating: () => false,
  disableHoverRating: () => false,
};

export default RatingStar;
