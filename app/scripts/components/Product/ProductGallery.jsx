import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Transition } from 'react-transition-group';
import { ResponsiveImage, StandardImage } from '../../Utils/ImageComponents';

//DEFAULT STYLES
const duration = 120;
const defaultStyle = {
  transition: `opacity ${duration}ms ease-in-out`,
  opacity: 1,
};
const transitionStyles = {
  entering: { opacity: 0 },
  entered: { opacity: 1 },
};

// CLASS DEFINITION
export default class ProductGallery extends Component {
  // PropTypes / DefaultProps
  static propTypes = {
    gallery: PropTypes.array.isRequired,
  };

  constructor(props) {
    super(props);
    this.state = {
      currentImage: this.props.gallery[0],
      animationFlag: true,
    };
    this.transitionInterval = 0;
    this.changeImage = this.changeImage.bind(this);
  }

  componentDidMount() {
    const Hammer = typeof window !== 'undefined' ? require('hammerjs') : undefined;

    const myElement = this.slideWrapper.current;
    const mc = new Hammer.Manager(myElement);
    const swipe = new Hammer.Swipe({ direction: Hammer.DIRECTION_HORIZONTAL });

    // Custom options
    mc.add([swipe]);
    mc.on('swipeleft swiperight', (ev) => {
      switch (ev.type) {
        case 'swiperight':
          this.prevSlide();
          break;
        case 'swipeleft':
          this.nextSlide();
          break;
        default:
          break;
      }
    });
  }
  componentWillReceiveProps(nextProps) {
    if (this.props.gallery !== nextProps.gallery) {
      this.changeImage(nextProps.gallery[0]);
    }
  }
  componentWillUnmount() {
    clearInterval(this.transitionInterval);
  }

  changeImage = (nextImage) => {
    this.setState({
      animationFlag: false,
    });

    this.transitionInterval = setTimeout(() => {
      this.setState({
        currentImage: nextImage,
        animationFlag: true,
      });
    }, duration);
  };

  slideWrapper = React.createRef();

  nextSlide = () => {
    let currentImage = this.props.gallery.indexOf(this.state.currentImage);
    currentImage++;
    if (currentImage > this.props.gallery.length - 1) {
      currentImage = 0;
    }
    this.setState({ currentImage: this.props.gallery[currentImage] });
  };

  prevSlide = () => {
    let currentImage = this.props.gallery.indexOf(this.state.currentImage);
    currentImage--;
    if (currentImage < 0) {
      currentImage = this.props.gallery.length - 1;
    }
    this.setState({ currentImage: this.props.gallery[currentImage] });
  };

  render() {
    const { gallery } = this.props;
    const { currentImage, animationFlag } = this.state;
    return (
      <div className="gallery ">
        <div className="gallery__wrapper" ref={this.slideWrapper}>
          <Transition in={animationFlag} timeout={duration} appear>
            {(state) => (
              <div
                className="gallery__main"
                style={{
                  ...defaultStyle,
                  ...transitionStyles[state],
                }}
              >
                {currentImage ? (
                  <StandardImage
                    itemProp="image"
                    lazy={false}
                    src={currentImage.photo.medium}
                    alt={this.props.gallery
                      .indexOf(this.state.currentImage)
                      .toString()}
                  />
                ) : (
                  <div className="message">Imagen no disponible por el momento</div>
                )}
              </div>
            )}
          </Transition>

          <ul className="gallery__thumbnails">
            {gallery.map((image, i) => (
              <li className="thumbnail" key={`${i}`}>
                <a
                  onClick={() => this.changeImage(image)}
                  className="thumbnail__button"
                >
                  <ResponsiveImage
                    itemProp="image"
                    src={image.photo}
                    alt={`Photo ${i}`}
                  />
                </a>
              </li>
            ))}
          </ul>
        </div>
      </div>
    );
  }
}
