import React from 'react';
import { Component } from 'react';
import { Link } from 'react-router-dom';
import { ResponsiveImageFromURL } from '../../Utils/ImageComponents';
import { formatNumberToPrice } from '../../Utils/normalizePrice';

export class SimilarItem extends Component {
  componentDidMount() {}

  render() {
    const { store } = this.props;
    return (
      <section className="similar-items">
        <h3 className="title--main">Artículos Similares</h3>

        <div className="product-list-container wrapper--center">
          <ul className="products-list">
            {this.props.productsList.map((item, i) => {
              return (
                <li className="product" key={item.slug}>
                  <div className="product__wrapper">
                    <div className="product__image">
                      <Link to={`/stores/${store.slug}/products/${item.slug}`}>
                        {item.photo && (
                          <ResponsiveImageFromURL
                            src={item.photo.small}
                            alt={item.name}
                          />
                        )}
                      </Link>

                      <div className="product__marker">
                        <span className="price">
                          {formatNumberToPrice(item.price)}MX
                        </span>
                      </div>
                    </div>

                    <div className="product__info">
                      <Link
                        className="product-name"
                        to={`/stores/${item.store.slug}/products/${item.slug}`}
                      >
                        {item.name}
                      </Link>
                      <Link className="store-name" to={`/stores/${item.store.slug}`}>
                        {item.store.name}
                      </Link>
                    </div>
                  </div>
                </li>
              );
            })}
          </ul>
        </div>
      </section>
    );
  }
}
