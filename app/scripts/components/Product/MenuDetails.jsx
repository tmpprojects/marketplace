import React from 'react';
import { Component } from 'react';
import { Link } from 'react-router-dom';

export class MenuDetails extends Component {
  componentDidMount() {}

  render() {
    return (
      <nav className="info_container">
        <div className="menu_details">
          <ul>
            <li>
              <a href="" className="active">
                Detalles
              </a>
            </li>
            <li>
              <a href="">Envío</a>
            </li>
            <li>
              <a href="">Otros</a>
            </li>
            <li>
              <a href="">Reseñas (48)</a>
            </li>
          </ul>
        </div>

        <div className="detail_info">
          <div className="detail_info--category">
            <a className="dropdown">Descripción</a>
          </div>
          <div className="txt description">
            <ul>
              <li> {excerpt} </li>
            </ul>
          </div>
        </div>

        <div className="detail_info">
          <div className="detail_info--category">
            <a className="dropdown">Detalles</a>
          </div>
          <div className="txt">
            <p>{description}</p>
          </div>
          <div className="author">
            <div className="author__info">
              <img src={'/images/header_profile@2x.png'} alt="" />
              <div>
                <p>Creado por: Elena Almaguer</p>
                <a href="#">@helen_crafts</a>
              </div>
            </div>
          </div>
        </div>

        <div className="detail_info">
          <div className="detail_info--category">
            <a className="dropdown">Reseñas (48)</a>
          </div>
          <div className="txt"></div>
        </div>
      </nav>
    );
  }
}
