import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { ResponsiveImageFromURL } from '../../Utils/ImageComponents';
import { formatNumberToPrice } from '../../Utils/normalizePrice';

export class SiderbarItem extends Component {
  render() {
    const { store } = this.props;
    return (
      <aside
        itemScope
        itemType="http://schema.org/Brand"
        className="products_container"
      >
        <div className="store-name">
          <p>Más productos de:</p>
          <div itemProp="logo" className="store-name__photo">
            <Link to={`/stores/${store.slug}`} className="store-name__link">
              <ResponsiveImageFromURL src={store.photo.small} alt={store.name} />\
            </Link>
          </div>
          <h4 itemProp="name" className="store-name__title">
            <Link to={`/stores/${store.slug}`} className="store-name__link">
              {store.name}
            </Link>
          </h4>
        </div>

        <div className="product-list-container">
          <ul className="products-list">
            {this.props.productsList.map((product, i) => (
              <li
                itemProp="isSimilarTo"
                itemScope
                itemType="http://schema.org/Product"
                className="product"
                key={product.slug}
              >
                <div className="product__wrapper">
                  <div className="product__image">
                    <Link
                      itemProp="url"
                      to={`/stores/${store.slug}/products/${product.slug}`}
                    >
                      {product.photo && (
                        <ResponsiveImageFromURL
                          itemProp="image"
                          src={product.photo.small}
                          alt={product.name}
                        />
                      )}
                    </Link>

                    {/* DISCOUNT TAG */}
                    {parseFloat(product.discount, 10) > 0 && (
                      <div
                        style={{
                          position: 'absolute',
                          bottom: '.5em',
                          right: '.5em',
                          padding: '0.1em .3em',
                          background: '#1eb592',
                          color: '#ffffff',
                        }}
                      >
                        <span className="cr__text--caption cr__textColor--colorWhite">
                          {`-${parseInt(product.discount)}%`}
                        </span>
                      </div>
                    )}
                    {/* /DISCOUNT TAG */}
                  </div>

                  <div className="product__info">
                    <Link
                      itemProp="name"
                      className="product-name"
                      to={`/stores/${store.slug}/products/${product.slug}`}
                    >
                      {product.name}
                    </Link>

                    {/* STIKE THROUGH PRICE */}
                    {parseFloat(product.discount, 10) > 0 && (
                      <div
                        style={{ color: '#D87041' }}
                        className="product__price line-through"
                      >
                        Antes
                        <span style={{ textDecoration: 'line-through' }}>
                          {' '}
                          {formatNumberToPrice(product.price_without_discount)}
                          MX
                        </span>
                      </div>
                    )}
                    {/* /STIKE THROUGH PRICE */}

                    <div className="post__info--price">
                      {/* <span>@{store.slug}</span> */}
                      <span itemProp="price" className="price">
                        {parseFloat(product.discount, 10) > 0
                          ? formatNumberToPrice(product.price)
                          : formatNumberToPrice(product.price_without_discount)}
                        MX
                      </span>
                    </div>
                  </div>
                </div>
              </li>
            ))}
          </ul>
          {/*<a className="posts__loadMore c2a_round" >Mostrar M&aacute;s</a>*/}
        </div>
      </aside>
    );
  }
}
