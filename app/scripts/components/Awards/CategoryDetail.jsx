import React from 'react';
import { Link } from 'react-router-dom';

export default (props) => {
  const { category, closeWindow } = props;

  const nominees = (
    <div className="nominees">
      <h5 className="title">Nominados</h5>
      <ul className="nominees__list">
        {category.nominees.map((n, i) => (
          <li className="item">
            <Link to={`./stores/${n.link_store}`} className="item__nominee">
              <div className="photo_container">
                <img src={n.photo_logo} alt={n.name} className="photo" />
              </div>
              <p className="store">
                {n.name} <br />
                <span className="owner">{n.owner}</span>
              </p>
            </Link>
          </li>
        ))}
      </ul>
    </div>
  );

  const winners = (
    <React.Fragment>
      <h5 className="title"> Ganadores</h5>
      <ul className="winners">
        {category.winners.map((w, i) => (
          <li className="winner">
            <div className="photo_container">
              <img src={w.photo} alt={w.name} className="photo" />
            </div>
            <h5 className="title color">{w.place}</h5>
            <p className="store">
              {w.name} <br />
              <span className="owner">{w.owner}</span>
            </p>
          </li>
        ))}
      </ul>
    </React.Fragment>
  );

  return (
    <div className="category__detail" id={category.slug}>
      <div className="category__detail-button">
        <button className="close" onClick={closeWindow}>
          Cerrar
        </button>
      </div>
      <div className="category__detail-header">
        <img src={category.img} alt={category.slug} className="icon__category" />
        <div className="info">
          <h4 className="title">{category.name}</h4>
          <p>{category.description}</p>
        </div>
      </div>
      <div className="category__detail-container">
        {category.slug === 'unica' ||
        category.slug === 'social' ||
        category.slug === 'crecer' ? (
          <h5 className="soon">Próximamente los ganadores</h5>
        ) : (
          nominees
        )}
        {/* {winners} */}
      </div>
    </div>
  );
};
