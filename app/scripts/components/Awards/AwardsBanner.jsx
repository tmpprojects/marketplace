import React from 'react';
import './awardsBanner.scss';

export default class AwardsBanner extends React.Component {
  render() {
    return (
      <section className="awards__banner wrapper--center">
        <div className="awards__banner-container">
          <h5>
            La mejores marcas de emprendedores mexicanos.
            <br /> <b>CANASTA ROSA AWARDS</b>
          </h5>
          {/* <a 
                        href="https://docs.google.com/forms/d/e/1FAIpQLSe8C2LeqkedW9djf8g5-rIB38REnBCqFtlreGLhVsTYiM6mAw/viewform" 
                        className="button__square" 
                        rel="noopener noreferrer"
                        target="_blank"
                    >
                        Nomina a tu marca favorita aquí
                    </a> */}
          <a href="/awards/" className="button__square" rel="noopener noreferrer">
            Conoce a los nominados
          </a>
        </div>
      </section>
    );
  }
}
