import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Redirect } from 'react-router-dom';
import { Field, formValueSelector, reduxForm, SubmissionError } from 'redux-form';
import withCounter from '../hocs/withCounter';
import { IconPreloader } from '../../Utils/Preloaders';
import { InputField } from '../../Utils/forms/formComponents';
import { myStoreActions, modalBoxActions } from '../../Actions';
import ErrorFormDisplay from '../StoreApp/Utils/ErrorFormDisplay/ErrorFormDisplay';
import {
  required,
  minLength8,
  minValue2,
  email,
  number,
} from '../../Utils/forms/formValidators';

const InputWithCounter = withCounter(InputField);

class CreateProductModalBox extends Component {
  constructor(props) {
    super(props);
    this.state = {
      productCreated: false,
      productID: null,
      errorForm: false,
      productBadWords: [],
    };
    this.createProduct = this.createProduct.bind(this);
    this.testProduct = this.testProduct.bind(this);
  }

  testProduct(product) {
    this.setState({ errorForm: false });

    const { productName } = product;

    const { list = [] } = this.props?.prohibitedWords;

    // If prohibited words list doesnt have items
    if (list.length === 0) {
      return this.createProduct(product);
    }

    if (list.length > 0) {
      const productArray = productName.toLowerCase().split(' ');

      const finalList = list.map((item) => item.toLowerCase());

      const productWordsList = productArray.filter((word) =>
        finalList.includes(word.toLowerCase()),
      );

      if (productWordsList.length > 0) {
        this.setState({ productBadWords: productWordsList, errorForm: true });
      } else {
        this.createProduct(product);
      }
    }
  }

  /**
   * createProduct()
   * @param {object} product : New Product
   */
  createProduct(product) {
    const { productName } = product;
    if (productName !== '' && productName !== null && productName !== undefined) {
      // Fill required fields
      const formData = {
        name: productName,
        price: 100,
        quantity: 10,
        status: 'draft',
        product_type: 'physical',
        category: [],
        section: null,
        tags: [],
        work_schedules: [],
        attributes: [],
        content_search: '',
      };

      // Send product information to API
      return this.props
        .addProduct(formData)
        .then((response) => {
          this.setState({
            productCreated: true,
            productID: response.data.slug,
          });
          this.props.closeModalBox();
        })
        .catch((error) => {
          const firstError =
            error.response.data[Object.keys(error.response.data)[0]];
          throw new SubmissionError({
            _error: firstError[0],
          });
        });
    }
  }

  /**
   * render()
   */
  render() {
    const { handleSubmit, error, submitting, invalid, productName } = this.props;

    const { productBadWords, errorForm } = this.state;

    // If product is succesfully created, redirect to 'product edit' section
    // else, render 'new product' form.
    return (
      (this.state.productCreated && (
        <Redirect to={`/my-store/products/edit/${this.state.productID}`} />
      )) || (
        <div className="modalBox__addProduct modalBox--shadow modalBox--rounded">
          <h4 className="addProductForm__title">Crea un producto:</h4>
          <form onSubmit={handleSubmit(this.testProduct)}>
            <fieldset>
              <Field
                component={InputWithCounter}
                maxLength={50}
                autoFocus
                className="name"
                name="productName"
                id="productName"
                placeholder="Nombre de tu producto"
              />

              {submitting && <IconPreloader />}
              {invalid && error && (
                <div className="form_status danger big">{error}</div>
              )}

              <ErrorFormDisplay errorForm={errorForm} type={productBadWords} />

              <div className="form__data form__data-buttons">
                <button
                  type="button"
                  onClick={this.props.closeModalBox}
                  className="form__data-button cancel"
                >
                  Cancelar
                </button>
                <button
                  className="form__data-button submit c2a--square"
                  disabled={!productName}
                >
                  Crear Producto
                </button>
              </div>
            </fieldset>
          </form>
        </div>
      )
    );
  }
}

// Wrap component within reduxForm
CreateProductModalBox = reduxForm({
  form: 'myStoreAddProduct_form',
  // validate: validateField,
})(CreateProductModalBox);

const selector = formValueSelector('myStoreAddProduct_form');
// Add Redux state and actions to component´s props
function mapStateToProps(state) {
  const productName = selector(state, 'productName');

  return {
    prohibitedWords: state.app.prohibitedWords,
    productName,
  };
}
function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      openModalBox: modalBoxActions.open,
      closeModalBox: modalBoxActions.close,
      addProduct: myStoreActions.addProduct,
    },
    dispatch,
  );
}
CreateProductModalBox = connect(
  mapStateToProps,
  mapDispatchToProps,
)(CreateProductModalBox);

export default (ChildComponent) => {
  // New Component
  class AddProductButton extends Component {
    constructor(props) {
      super(props);
    }
    onClick() {
      this.props.openModalBox(CreateProductModalBox);
    }
    render() {
      const { auth, openModalBox, addProduct, ...props } = this.props;
      return <div onClick={this.onClick.bind(this)} {...props} />;
    }
  }

  // Add Redux state and actions to component´s props
  function mapStateToProps({ auth }) {
    return { auth };
  }
  function mapDispatchToProps(dispatch) {
    return bindActionCreators(
      {
        openModalBox: modalBoxActions.open,
        addProduct: myStoreActions.addProduct,
      },
      dispatch,
    );
  }

  // Return Connected Object
  return connect(mapStateToProps, mapDispatchToProps)(AddProductButton);
};
