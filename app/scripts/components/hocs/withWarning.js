import React, { Component } from 'react';
import PropTypes from 'prop-types';

export default (ChildComponent) => {
  class Warning extends Component {
    // static propTypes = {
    //     maxLength: PropTypes.number.isRequired,
    //     value: PropTypes.string,
    //     onChange: PropTypes.func
    // };
    // static defaultProps = {
    //     value: '',
    //     onChange: e => false
    // };

    render() {
      const { ...props } = this.props;

      return (
        <React.Fragment>
          <div className="warning">
            Para brindarte un mejor servicio, ayúdanos a confirmar que los datos de
            tu dirección sean correctos.
          </div>
          <ChildComponent {...props} />
        </React.Fragment>
      );
    }
  }

  // Export Component
  return Warning;
};
