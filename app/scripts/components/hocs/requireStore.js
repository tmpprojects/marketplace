import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';

import { getUserProfile } from '../../Reducers/users.reducer';

export default (ChildComponent) => {
  class RequireStore extends Component {
    render() {
      switch (this.props.hasStore) {
        case false:
        case undefined:
        case null:
          return <Redirect to="/stores/create" />;
        default:
          return <ChildComponent {...this.props} />;
      }
    }
  }
  function mapStateToProps(state) {
    return {
      hasStore: getUserProfile(state).has_store,
    };
  }

  return connect(mapStateToProps)(RequireStore);
};
