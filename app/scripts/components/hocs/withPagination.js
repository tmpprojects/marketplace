import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { NavLink } from 'react-router-dom';

import { range } from '../../Utils/genericUtils';

const LEFT_PAGE = 'LEFT';
const RIGHT_PAGE = 'RIGHT';

export default (ChildComponent) => {
  class ContainerPagination extends Component {
    static propTypes = {
      items: PropTypes.array.isRequired,
      totalPages: PropTypes.number.isRequired,
      page: PropTypes.number,
      maxItems: PropTypes.number,
      action: PropTypes.func,
    };
    static defaultProps = {
      page: 1,
      maxItems: 12,
      action: () => {},
    };

    componentWillReceiveProps(nextProps) {
      if (this.props.items !== nextProps.items) {
        this.setState({
          items: nextProps.items,
        });
      }
    }

    fetchData = (page) => {
      const { maxItems } = this.props;
      this.props.action(page, maxItems);
    };

    gotoNextPage = () => this.fetchData(this.props.page + 1);
    gotoPrevPage = () => this.fetchData(this.props.page - 1);
    gotoPage = (page) => this.fetchData(page);

    fetchPageNumbers = () => {
      const totalPages = this.props.totalPages ? this.props.totalPages : 1;
      const currentPage = this.props.page ? this.props.page : 1;
      const pageNeighbours = 2; //this.pageNeighbours;

      /**
       * totalNumbers: the total page numbers to show on the control
       * totalBlocks: totalNumbers + 2 to cover for the left(<) and right(>) controls
       */
      const totalNumbers = pageNeighbours * 2 + 3;
      const totalBlocks = totalNumbers + 2;

      if (totalPages > totalBlocks) {
        const startPage = Math.max(2, currentPage - pageNeighbours);
        const endPage = Math.min(totalPages - 1, currentPage + pageNeighbours);
        let pages = range(startPage, endPage);

        /**
         * hasLeftSpill: has hidden pages to the left
         * hasRightSpill: has hidden pages to the right
         * spillOffset: number of hidden pages either to the left or to the right
         */
        const hasLeftSpill = startPage > 2;
        const hasRightSpill = totalPages - endPage > 1;
        const spillOffset = totalNumbers - (pages.length + 1);

        switch (true) {
          // handle: (1) < {5 6} [7] {8 9} (10)
          case hasLeftSpill && !hasRightSpill: {
            const extraPages = range(startPage - spillOffset, startPage - 1);
            pages = [LEFT_PAGE, ...extraPages, ...pages];
            break;
          }

          // handle: (1) {2 3} [4] {5 6} > (10)
          case !hasLeftSpill && hasRightSpill: {
            const extraPages = range(endPage + 1, endPage + spillOffset);
            pages = [...pages, ...extraPages, RIGHT_PAGE];
            break;
          }

          // handle: (1) < {4 5} [6] {7 8} > (10)
          case hasLeftSpill && hasRightSpill:
          default: {
            pages = [LEFT_PAGE, ...pages, RIGHT_PAGE];
            break;
          }
        }
        return [1, ...pages, totalPages];
      }

      return range(1, totalPages);
    };

    render() {
      const { items, page: currentPage, totalPages, location } = this.props;
      const pageButtons = this.fetchPageNumbers().map((pageIndex, index) => {
        if (pageIndex === LEFT_PAGE) {
          return (
            <div key="nav-prev" className="prev">
              <NavLink
                to={`${this.props.baseLocation}&p=${currentPage - 1}`}
                className="button"
              >
                Anterior
              </NavLink>
            </div>
          );
        }

        if (pageIndex === RIGHT_PAGE) {
          return (
            <div key="nav-next" className="next">
              <NavLink
                to={`${this.props.baseLocation}&p=${currentPage + 1}`}
                className="button"
              >
                <span>Siguiente</span>
              </NavLink>
            </div>
          );
        }

        return (
          <li key={pageIndex} className={'number page-item'}>
            <NavLink
              to={`${this.props.baseLocation}&p=${pageIndex}`}
              className="button"
              activeClassName="button--active"
              isActive={() => currentPage === pageIndex}
            >
              <span className="button__index">{pageIndex}</span>
            </NavLink>
          </li>
        );
      });

      // Return Markup
      return (
        <React.Fragment>
          <ChildComponent items={items} location={!location ? '' : location} />

          <div className="pagination">
            <ul className="page-numbers">{pageButtons}</ul>
          </div>
        </React.Fragment>
      );
    }
  }

  // Export Component
  ContainerPagination.displayName = `withPaginator(${getDisplayName(
    ChildComponent,
  )})`;
  return ContainerPagination;
};

function getDisplayName(WrappedComponent) {
  return WrappedComponent.displayName || WrappedComponent.name || 'Component';
}
