import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { trackWithGTM, trackWithFacebookPixel } from '../../Utils/trackingUtils';

const withAnalyticsTracker = (ChildComponent, options = {}) => {
  const trackPage = (pageData) => {
    trackWithGTM('Pageview', pageData.url);
    trackWithGTM('Pageview_Criteo', pageData.email);
    trackWithFacebookPixel('track', 'PageView');
  };

  class HOC extends Component {
    componentDidMount() {
      const page = this.props.location.pathname;
      const email = encodeURI(this.props.email); //For Criteo
      trackPage({
        url: page,
        email,
      });
    }
    componentWillReceiveProps(nextProps) {
      const currentPage = this.props.location.pathname;
      const nextPage = nextProps.location.pathname;
      const email = encodeURI(nextProps.email);
      if (currentPage !== nextPage) {
        trackPage({
          url: nextPage,
          email,
        });
      }
    }
    render() {
      return <ChildComponent {...this.props} />;
    }
  }

  // Export Component
  HOC.displayName = `withAnalyticsTracker(${getDisplayName(ChildComponent)})`;
  return connect(mapStateToProps, mapDispatchToProps)(HOC);
};

function getDisplayName(WrappedComponent) {
  return WrappedComponent.displayName || WrappedComponent.name || 'Component';
}

// Map Redux Props and Actions to component
function mapStateToProps(state) {
  return {
    email: state.users.isLogged ? state.users.profile.email : '',
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({}, dispatch);
}

// Export Component
export default withAnalyticsTracker;
