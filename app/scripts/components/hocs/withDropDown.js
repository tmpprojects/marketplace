import React, { Component } from 'react';
import PropTypes from 'prop-types';

export default (ChildComponent) => {
  class DropDownMenu extends Component {
    static propTypes = {
      active: PropTypes.bool.isRequired,
    };
    static defaultProps = {
      active: false,
    };
    closeDelayInterval = 0;
    wrapperRef = React.createRef();

    /**
     * React Lifecycle methods
     */
    componentDidMount() {
      // Add DOM event listeners
      window.addEventListener('resize', this.onResize);
      document.addEventListener('mousedown', this.handleClickOutside);
      this.onResize();
    }
    componentWillUnmount() {
      // Remove Listeners
      window.removeEventListener('resize', this.onResize);
      document.removeEventListener('mousedown', this.handleClickOutside);
    }

    handleClickOutside = (event) => {
      const dropdown = this.wrapperRef.current;
      // Check if this component has
      if (dropdown && !dropdown.contains(event.target)) {
        this.props.closeMenu();
      }
    };

    onResize = () => {
      const dropdown = this.wrapperRef.current;
      if (dropdown && this.props.isMobile) {
        dropdown.style.left = `-${dropdown.offsetParent.offsetLeft}px`;
      } else if (dropdown) {
        dropdown.style.left = `${
          -dropdown.getBoundingClientRect().width +
          dropdown.offsetParent.getBoundingClientRect().width
        }px`;
      }
    };

    render() {
      const { ...props } = this.props;
      return (
        <div
          className={`menu-dropdown ${props.active ? 'active' : ''}`}
          ref={this.wrapperRef}
        >
          <ChildComponent {...props} />
        </div>
      );
    }
  }

  // Export Component
  return DropDownMenu;
};
