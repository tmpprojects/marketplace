import React from 'react';
import PropTypes from 'prop-types';

const syncComponent = (chunkName, mod) => {
  // Get desired component
  const Component = mod.default ? mod.default : mod;

  // Define HOC component
  const SyncComponent = ({ staticContext, ...otherProps }) => {
    // Collect 'chunk' names
    // These are the references to the javascript files
    // that would be inserted into the html template
    if (staticContext.splitPoints) {
      staticContext.splitPoints.push(chunkName);
    }

    return <Component {...otherProps} staticContext={staticContext} />;
  };

  // Synced component propTypes
  SyncComponent.propTypes = {
    staticContext: PropTypes.object,
  };
  SyncComponent.defaultProps = {
    staticContext: undefined,
  };

  // If component has a 'loadData' method,
  // add it to this synced HOC
  SyncComponent.loadData = Component.loadData || null;

  // return synced HOC
  return SyncComponent;
};

// Export Component
export default syncComponent;
