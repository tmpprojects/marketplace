import React, { Component } from 'react';
import PropTypes from 'prop-types';
import '../../../styles/hocs/_withCounter.scss';

export default (ChildComponent) => {
  class Counter extends Component {
    static propTypes = {
      maxLength: PropTypes.number.isRequired,
    };
    constructor(props) {
      super(props);
      this.state = {
        init: false,
      };
    }

    componentWillReceiveProps(nextProps) {
      if (this.props.input.value !== nextProps.input.value) {
        this.setState({ init: true });
      }
    }

    render() {
      const { init } = this.state;
      const { ...props } = this.props;
      const charsLeft = Math.max(
        0,
        this.props.maxLength - this.props.input.value.length,
      );

      let textColor = '';
      let classNameText = '';
      switch (true) {
        case charsLeft < 10:
          // textColor = '#e6575d';
          classNameText = 'text-danger-form';
          break;
        case charsLeft < 20:
          // textColor = '#ceb52b';
          classNameText = 'text-warning-form';
          break;
        default:
          break;
      }

      return (
        <div className="container">
          <ChildComponent {...props} />
          {init && (
            <div className="formatChars">
              <p className={classNameText}>
                <span>{charsLeft}</span> caracteres.
              </p>
            </div>
          )}
        </div>
      );
    }
  }

  // Export Component
  return Counter;
};
