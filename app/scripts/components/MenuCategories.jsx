import React from 'react';
import { Component } from 'react';
import { Link, NavLink } from 'react-router-dom';

export default ({ categories = [] }) => (
  <nav className="menu_categories">
    <ul>
      <div>
        {categories.map((category, i) => (
          <li key={category.slug}>
            <NavLink
              className="link"
              to={`/category/${category.slug}`}
              onClick={() => false}
            >
              {category.name}
            </NavLink>
          </li>
        ))}
        <li key="envio-nacional">
          <NavLink className="link" to="/category/envio-nacional">
            Envío Nacional
          </NavLink>
        </li>
      </div>

      {/* <div>
                <li className="search">
                    <form>
                        <fieldset>
                            <input type="text" placeholder="Buscar productos/tiendas" />
                        </fieldset>
                    </form>
                </li>
            </div> */}
    </ul>
  </nav>
);
