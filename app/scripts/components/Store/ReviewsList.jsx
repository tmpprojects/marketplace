import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';

import { modalBoxActions } from '../../Actions';
import Rating from '../Rating/Rating';
import Reviews from './Reviews';
import ModalRating from '../../Utils/ModalRating';

class ReviewsList extends React.Component {
  constructor(props) {
    super(props);
    this.currentPage = 1;
  }

  /*
   * openRatingWindow()
   * Opens modalbox to rating product
   * @param {object} productPurchase | Product Purchase
   * @param {int} value | Initial Rating Value to display on Rating component
   */
  openRatingWindow = (productPurchase, value = 0) => {
    this.props.openModalBox(() => (
      <ModalRating productPurchase={productPurchase} rating={value} />
    ));
  };

  /**
   * render()
   */
  render() {
    const {
      reviews,
      reviewsCount,
      rating,
      showAddButton,
      showProductDetails,
      pageLocation,
      pendingReviews,
      profile: { profile_photo, first_name, last_name },
    } = this.props;

    const profile = {
      profile_photo,
      first_name,
      last_name,
    };

    return (
      <React.Fragment>
        <div className="review_title">
          <div className="rating-header">
            <h3 className="rating-title">Reseñas</h3>
            <div className="rating-container">
              <span className="score">{`${rating.toFixed(1)}  `}</span>
              <Rating rating={rating} />
              <div className="votes-count">({reviewsCount} calificaciones)</div>
            </div>
          </div>
          {/*showAddButton && (
                        <button 
                            onClick={e => false}
                            className="button-square--white"
                        >Escribir reseña</button> 
                    )*/}
        </div>
        <div className="review_list">
          <Reviews
            profile={profile}
            reviews={pendingReviews}
            showProductDetails={showProductDetails}
          />
          <Reviews reviews={reviews} showProductDetails={showProductDetails} />
        </div>
      </React.Fragment>
    );
  }
}

// Map Redux Props and Actions to component
function mapStateToProps(state) {
  return {
    profile: state.users.profile,
  };
}
function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      openModalBox: modalBoxActions.open,
      closeModalBox: modalBoxActions.close,
    },
    dispatch,
  );
}

// Connect Component to Redux
ReviewsList = connect(mapStateToProps, mapDispatchToProps)(ReviewsList);

// Define Default Props
ReviewsList.propTypes = {
  reviews: PropTypes.array.isRequired,
  showProductDetails: PropTypes.bool,
  reviewsCount: PropTypes.number,
  rating: PropTypes.number,
  showAddButton: PropTypes.bool,
  purchase: PropTypes.object.isRequired,
};
ReviewsList.defaultProps = {
  reviews: [],
  showProductDetails: false,
  reviewsCount: 0,
  rating: 0,
  showAddButton: false,
};

// Export Component
export default ReviewsList;
