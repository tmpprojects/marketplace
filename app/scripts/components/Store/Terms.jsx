import React from 'react';
import { Component } from 'react';
import { Link } from 'react-router-dom';

export class Terms extends Component {
  componentDidMount() {}

  render() {
    return (
      <div className="info_politc">
        <div className="">
          <a href="" className="dropdown">
            Políticas
          </a>
        </div>

        <div className="category">
          <h5 className="subtitle">Envío</h5>
          <h5 className="subtitle-secundary">Processing time</h5>
          <p>
            The time I need to prepare an order for shipping varies. For details, see
            individual items.
          </p>
        </div>
        <div className="category">
          <h5 className="subtitle-secundary">Customs and Import Taxes</h5>
          <p>
            The time I need to preBuyers are responsible for any customs and import
            taxes that may apply. I'm not responsible for delays due to customs.
          </p>
        </div>
        <div className="category">
          <h5 className="subtitle">Opciones de Pago</h5>
          <p>
            Can't guarantee the security of these options. For details on how to pay
            with these methods, contact me.Other Method.
          </p>
        </div>
        <div className="category">
          <h5 className="subtitle">Devoluciones</h5>
          <h5 className="subtitle-secundary">I gladly accept cancellations</h5>
          <p>Request a cancellation within: 7 days of purchase</p>
          <h5 className="subtitle-secundary">I gladly accept cancellations</h5>
          <p>But please contact me if you have any problems with your order.</p>
        </div>
        <div className="category">
          <h5 className="subtitle">Políticas de Privacidad</h5>
          <p>
            I will only use your shipping and billing address, and contact
            information
          </p>
          <ul>
            <li>To communicate with you about your order</li>
            <li>To fulfill your order</li>
            <li>For legal reasons (like paying taxes)</li>
          </ul>
        </div>
      </div>
    );
  }
}
