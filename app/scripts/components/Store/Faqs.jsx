import React from 'react';
import { Component } from 'react';
import { Link } from 'react-router-dom';

export class Faqs extends Component {
  componentDidMount() {}

  render() {
    const { faqsData } = this.props;
    return (
      <div className="info_faqs" id={this.props.id}>
        <div>
          <a href="" className="dropdown">
            Preguntas Frecuentes
          </a>
        </div>
        {faqsData.map((faq) => (
          <div className="category">
            <h5 className="subtitle">{faq.question}</h5>
            <p></p>
          </div>
        ))}
      </div>
    );
  }
}
