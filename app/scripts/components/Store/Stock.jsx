import React from 'react';
import { Link } from 'react-router-dom';

import { ResponsiveImageFromURL } from '../../Utils/ImageComponents';
import { formatNumberToPrice } from '../../Utils/normalizePrice';
import ContainerPagination from '../../Utils/ContainerPagination';
import { PRODUCT_TYPES } from '../../Constants/config.constants';

export default (props) => {
  const { items, id, openDetailsWindow, store } = props;

  const dispatchDataLayerClick = (id)  => {
    console.log("dispatchDataLayerClick stock");
    globalThis.googleAnalytics.productClick(id);

  };

  const renderCustomProduct = (product) => {
    const productType = product.product_type.value;

    // Construct Link.
    const linkTarget = {
      pathname: `/pro/servicios/${product.slug}`,
      state: {
        scrollToTop: true,
      },
    };
    const clickHandler = (e) => openDetailsWindow(product);
    if (productType === PRODUCT_TYPES.EVENT) {
      linkTarget.pathname = `/pro/eventos/${product.slug}`;
    }
    // Return markup.
    return (
      <React.Fragment>
        <div className="thumbnail">
          <div className="thumbnail__image">
            {product.photo && (
              <Link
                to={linkTarget}
                key={product.slug}
                className="product"
                onClick={clickHandler}
              >
                <ResponsiveImageFromURL
                  src={product.photo.small}
                  alt={product.name}
                />
              </Link>
            )}
          </div>
        </div>
        <div className="product__info">
          <p className="product__name">
            <Link
              to={linkTarget}
              key={product.slug}
              className="product"
              onClick={clickHandler}
            >
              {product.name}
            </Link>
          </p>
          <div className="product__info-detail">
            <p className="store product__store-link">{product.store.name}</p>
            <p className="product__shippingMethod cr__text--caption cr__textColor--colorViolet400">
              {product?.physical_properties?.shipping_methods?.find(
                (i) => i === 1 || i === 2,
              ) && 'Envío express'}
              {!product?.physical_properties?.shipping_methods?.find(
                (i) => i === 1 || i === 2,
              ) &&
                product?.physical_properties?.shipping_methods?.find(
                  (i) => i === 3,
                ) &&
                'Envío Nacional'}
            </p>
            {parseFloat(product.discount, 10) > 0 && (
              <div>
                Antes{' '}
                <span className="product__price line-through">
                  {' '}
                  {formatNumberToPrice(product.price_without_discount)}MX
                </span>
              </div>
            )}
            <span className="product__price">
              {' '}
              {formatNumberToPrice(product.price)}MX
            </span>
          </div>
        </div>
      </React.Fragment>
    );
  };

  const renderPhysicalProduct = (product) => (
    <li className="product" key={product.slug}>
      <div
        className="thumbnail"
        style={{
          position: 'relative',
        }}
      >
    
        <Link
          to={`/stores/${product.store.slug}/products/${product.slug}`}
          className="thumbnail__image"
          onClick={() => dispatchDataLayerClick(product.id)}
        >
          {product.photo && (
            <ResponsiveImageFromURL src={product.photo.small} alt={product.name} />
          )}
        </Link>

        {/* DISCOUNT TAG */}

        {parseFloat(product.discount, 10) > 0 && (
          <div
            style={{
              position: 'absolute',
              bottom: '.5em',
              right: '.5em',
              padding: '0.1em .3em',
              background: '#1eb592',
              color: '#ffffff',
            }}
          >
            <span className="cr__text--caption cr__textColor--colorWhite">
              {`-${parseInt(product.discount)}%`}
            </span>
          </div>
        )}
        {/* /DISCOUNT TAG */}
      </div>
      <div className="product__info">
        <Link
          className="product__name"
          to={`/stores/${product.store.slug}/products/${product.slug}`}
          onClick={() => dispatchDataLayerClick(product.id)}
        >
          {product.name}
        </Link>

        <div className="product__info-detail">
          
          <Link
            className="store product__store-link"
            to={`/stores/${product.store.slug}`}
            
          >
            {product.store.name}
          </Link>
          <p className="product__shippingMethod cr__text--caption cr__textColor--colorViolet400">
            {product?.physical_properties?.shipping_methods?.find(
              (i) => i === 'express-moto' || i === 'express-car',
            ) && 'Envío express'}
            {!product?.physical_properties?.shipping_methods?.find(
              (i) => i === 'express-moto' || i === 'express-car',
            ) &&
              product?.physical_properties?.shipping_methods?.find(
                (i) => i === 'standard',
              ) &&
              'Envío Nacional'}
          </p>

          {/* STIKE THROUGH PRICE */}
          {parseFloat(product.discount, 10) > 0 && (
            <div
              style={{ color: '#D87041' }}
              className="product__price line-through"
            >
              Antes
              <span style={{ textDecoration: 'line-through' }}>
                {' '}
                {formatNumberToPrice(product.price_without_discount)}MX
              </span>
            </div>
          )}
          {/* /STIKE THROUGH PRICE */}

          <span className="product__price">
            {' '}
            {parseFloat(product.discount, 10) > 0
              ? formatNumberToPrice(product.price)
              : formatNumberToPrice(product.price_without_discount)}
            MX
          </span>
        </div>
        <Link to="" className="product__rating">
          <img
            src={require('images/store/ranking.png')}
            className="ranking"
            alt=""
          />
        </Link>
      </div>
    </li>
  );

  const item = items?.map((product) =>
    product.product_type.value === PRODUCT_TYPES.PHYSICAL
      ? renderPhysicalProduct(product)
      : renderCustomProduct(product),
  );

  return (
    <div className="products__wrapper" id={id}>
      <div className="mobile listing-info">
        <div className="listing-info__counter">
          Producto{items.length !== 1 ? 's' : ''} <span>({items.length})</span>
        </div>
        {/* <a href="" className="c2a_border listing-info__filters" >Filtros</a> */}
      </div>

      <ul className="products__list">{item}</ul>
    </div>
  );
};
