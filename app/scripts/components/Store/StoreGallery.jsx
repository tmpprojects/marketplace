import React from 'react';
import { Component } from 'react';
import PropTypes from 'prop-types';
import { Transition } from 'react-transition-group';
import {
  ResponsiveImage,
  ResponsiveImageFromURL,
} from '../../Utils/ImageComponents';

//DEFAULT STYLES
const duration = 120;
const defaultStyle = {
  transition: `opacity ${duration}ms ease-in-out`,
  opacity: 1,
};
const transitionStyles = {
  entering: { opacity: 0 },
  entered: { opacity: 1 },
};

// CLASS DEFINITION
export default class ProductGallery extends Component {
  // PropTypes / DefaultProps
  static propTypes = {
    gallery: PropTypes.array.isRequired,
  };
  constructor(props) {
    super(props);
    this.state = {
      currentImage: this.props.gallery[0],
      animationFlag: true,
    };
    this.changeImage = this.changeImage.bind(this);
  }
  componentWillReceiveProps(nextProps) {
    if (this.props.gallery !== nextProps.gallery) {
      this.changeImage(nextProps.gallery[0]);
    }
  }

  /**
   * changeImage()
   * @param {object} nextImage : Image object
   */
  changeImage(nextImage) {
    // Update State
    this.setState({
      animationFlag: false,
    });

    // Change image after a time delay
    setTimeout(() => {
      this.setState({
        currentImage: nextImage,
        animationFlag: true,
      });
    }, duration);
  }

  /**
   * render()
   */
  render() {
    const { gallery } = this.props;
    const { currentImage, animationFlag } = this.state;
    return (
      <div className="about_container--photo">
        <Transition in={animationFlag} timeout={duration} appear={true}>
          {(state) => (
            <div
              className="photo"
              style={{
                ...defaultStyle,
                ...transitionStyles[state],
              }}
            >
              <ResponsiveImage
                src={currentImage.photo}
                alt={`Galería de la tienda ${currentImage.order}`}
              />
            </div>
          )}
        </Transition>
        <div className="views">
          <ul>
            {gallery.map((image, i) => {
              return (
                <li className="thumbnail" key={i}>
                  <a
                    onClick={() => this.changeImage(image)}
                    className="thumbnail__button"
                  >
                    <ResponsiveImageFromURL
                      src={image.photo.small}
                      alt={`Galería de la tienda ${image.order}`}
                    />
                  </a>
                </li>
              );
            })}
          </ul>
        </div>
      </div>
    );
  }
}
