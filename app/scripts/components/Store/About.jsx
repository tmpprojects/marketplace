import React from 'react';
import PropTypes from 'prop-types';

import StoreGallery from './StoreGallery';
import { addHourPeriodSuffix } from '../../Utils/dateUtils';
import { socialNetworksURL } from '../../Constants/config.constants';

const About = (props) => {
  const {
    slogan = '',
    description = '',
    social_links = [],
    gallery = [],
    telephone,
    mobile,
    contact_mail,
    location,
    work_schedules,
  } = props.storeData;

  // Generate Social Media Links
  const socialMediaLinks_list = social_links.map((item, index) => {
    let networkName = item.name.charAt(0).toUpperCase() + item.name.slice(1);
    const networkPrefix = socialNetworksURL[item.name];
    return !networkPrefix ? null : (
      <div className="socialMedia__list_container" key={index}>
        <li className={`socialMedia__list__item ${item.name}`}>
          <a href={`${networkPrefix}${item.link}`} target="_blank">
            {networkName}
          </a>
        </li>
      </div>
    );
  });

  // Return Component
  return (
    <div className="about" id={props.id}>
      <div className="about_container">
        <div className="about_container--info storeInfo">
          <h4>Sobre la tienda</h4>
          <p className="bold">{slogan}</p>

          <div className="storeInfo__content">
            {description}
            <br />

            <div className="store-schedules">
              <p className="name">Horarios de la Tienda</p>
              <ol className="schedules-list">
                {work_schedules.map((schedule) => (
                  <li className="schedules-list__item" key={schedule.value}>
                    <div className="weekday">{schedule.name}</div>
                    <div className="schedule">
                      {`${addHourPeriodSuffix(schedule.open.value)} 
                                            - 
                                            ${addHourPeriodSuffix(
                                              schedule.close.value,
                                            )}`}
                    </div>
                  </li>
                ))}
              </ol>
            </div>

            {/*(location !== '') && (
                            <div className="details__location">
                                <p>{contact_mail}</p>
                                <span className="location">{location} | </span>
                                <span>
                                    {telephone !== '' ? `${telephone} | ` : '' }
                                    {mobile !== '' ? mobile : '' }
                                </span>
                            </div>
                        )*/}
          </div>

          {/* COMMENTED BY ISRAEL REQUEST */}
          {/* <div className="follow_container"> */}
          {/* COMMENTED BY ISRAEL REQUEST */}

          {/* <a href="#" className="follow c2a_border"><span>Seguir Tienda</span></a> */}

          {/* COMMENTED BY ISRAEL REQUEST */}
          {/* {socialMediaLinks_list.length > 0 && (
              <div>
                <p className="bold social">Visítanos en nuestras redes sociales</p>
                <nav className="socialMedia">
                  <ul className="socialMedia__list">{socialMediaLinks_list}</ul>
                </nav>
              </div>
            )}
          </div> */}
          {/* COMMENTED BY ISRAEL REQUEST */}
        </div>

        {gallery.length > 0 && <StoreGallery gallery={gallery.slice(0, 6)} />}
      </div>
    </div>
  );
};

// PropTypes Definition
About.propTypes = {
  storeData: PropTypes.object.isRequired,
};
export default About;
