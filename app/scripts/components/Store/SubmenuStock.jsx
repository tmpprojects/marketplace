import React from 'react';
import { Component } from 'react';
import { NavLink } from 'react-router-dom';
import './../../../styles/store/submenu-store.scss';

export default (props) => {
  const { sections, store } = props;

  const searchProducts = function (filter) {
    props.searchProducts(filter);
  };
  const formSubmit = function (e) {
    e.preventDefault();
    const term = e.target.querySelector('[name="s"]').value;
    searchProducts(`&search=${term}`);
  };

  return (
    <nav className="products__submenu">
      <ul>
        {/* <li className="search">
                    <form onSubmit={formSubmit}>
                        <fieldset>
                            <input name="s" type="text" placeholder="Buscar en esta tienda" />
                        </fieldset>
                    </form>
                </li>
                <li>
                    <a
                        className="active"
                        type="button"
                        onClick={(e) => {
                            e.preventDefault();
                            searchProducts(``);
                        }}>
                        Todos <span></span>
                    </a>
                </li> */}

        {/* { sections.map( section => (
                    <li key={section.slug}>
                        <a
                            type="button"
                            onClick={(e)=>{
                                e.preventDefault();
                                searchProducts(`&section__slug=${section.slug}`);
                            }}>
                            {section.name.charAt(0).toUpperCase() + section.name.slice(1)} {/*<span>10</span>*/}
        {/* </a>
                    </li>
                 */}
        <li className="search">
          <form onSubmit={formSubmit} className="searchForm">
            <fieldset>
              <input name="s" type="text" placeholder="Buscar en esta tienda" />
            </fieldset>
          </form>
          <NavLink
            exact
            to={`/stores/${store}/`}
            className="category__link"
            activeClassName="active"
            type="button"
            onClick={() => {
              searchProducts(``);
            }}
          >
            Todos
          </NavLink>
        </li>
        {sections.map((section) => (
          <li key={section.slug}>
            <NavLink
              exact
              to={`/stores/${store}/&section__slug=${section.slug}/`}
              className="category__link"
              activeClassName="active"
              onClick={() => {
                searchProducts(`&section__slug=${section.slug}`);
              }}
            >
              {section.name.charAt(0).toUpperCase() + section.name.slice(1)}
            </NavLink>
          </li>
        ))}
      </ul>
      {/* <li className="contact-owner">
                    <a href="">Contactar al vendedor</a>
                </li> */}
    </nav>
  );
};
