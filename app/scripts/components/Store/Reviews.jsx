import React from 'react';
import Moment from 'dayjs';
import PropTypes from 'prop-types';

import { ResponsiveImage, StandardImage } from '../../Utils/ImageComponents';
import Rating from '../Rating/Rating';

const Reviews = (props) => {
  return props.reviews.map((review) => (
    <div className="review_content" key={review.id}>
      <div className="review_content--photo">
        {props.profile ? (
          <ResponsiveImage
            src={props.profile.profile_photo}
            alt={`${props.profile.first_name} ${props.profile.last_name}`}
          />
        ) : (
          <ResponsiveImage
            src={review.user.profile_photo}
            alt={`${review.user.first_name} ${review.user.last_name}`}
          />
        )}
      </div>
      <div className="review_content--review">
        {props.profile ? (
          <span className="user_name">{`${props.profile.first_name}`}</span>
        ) : (
          <span className="user_name">{`${review.user.first_name}`}</span>
        )}
        <span className="date">{Moment(review.created).format('DD MMMM YYYY')}</span>
        {review.is_approved === null && (
          <span className="status">Bajo revisión</span>
        )}

        <Rating rating={review.product_score} />

        {props.showProductDetails && (
          <div className="product-details">
            <h4 className="product-details__title">{review.product.name}</h4>
            <StandardImage
              className="product-details__thumbnail"
              src={review.product.photo.small}
              alt={`${review.product.name}`}
            />
          </div>
        )}

        {review.comment !== '' && <p>{review.comment}</p>}
      </div>
    </div>
  ));
};

Reviews.propTypes = {
  reviews: PropTypes.array.isRequired,
  showProductDetails: PropTypes.bool,
};
Reviews.defaultProps = {
  reviews: [],
  showProductDetails: false,
};

export default Reviews;
