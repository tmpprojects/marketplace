import React from 'react';
import { Component } from 'react';
import { Link, NavLink } from 'react-router-dom';

export class Menu extends Component {
  render() {
    return (
      <nav className="menu-store">
        <ul>
          <li>
            <a href="#stock" className="active">
              Artículos
            </a>
          </li>
          {/*<li>
                        <a href="#reviews">Reviews</a>
                    </li>*/}
          <li>
            <a href="#about">Información</a>
          </li>
          {/* <li>
                        <a href="#terms">Términos y condiciones</a>
                    </li>
                    <li>
                        <a href="#faqs">FAQs</a>
                    </li> */}
        </ul>
      </nav>
    );
  }
}
