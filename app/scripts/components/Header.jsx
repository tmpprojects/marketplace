import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Link, NavLink, withRouter } from 'react-router-dom';

import { getCartProductsCount } from '../Reducers';
import { modalBoxActions, appActions, userActions } from '../Actions';
import SearchMenuDropdown from './SearchMenuDropdown.jsx';
import { getUserProfile } from '../Reducers/users.reducer';
import MainMenu from './NewMenu/MainMenu';

class Header extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isProfileMenuOpen: false,
      isSearchMenuOpen: false,
      isSearchBarOpen: false,
    };
  }

  /*
   * On Search Submit
   * Redirect user to Search Results Page
   */
  onSearchSubmit = (e) => {
    e.preventDefault();
    this.search_field.classList.remove('search--open');
    this.props.history.push(`/search/${e.target.querySelector('input').value}`);
  };

  /*
   * Open Modal Box
   * Open Modal box for Login/Register/PasswordRecovery etc...
   * @param {object} component : Which react component should we open?
   */
  openModalBox(_type) {
    this.props.openModalBox(_type);
  }

  /*
   * Toggle Search Box
   */
  toggleSearchBox = () => {
    this.setState({
      isSearchBarOpen: !this.state.isSearchBarOpen,
    });
  };

  /*
   * Close All Dropdown Menus
   */
  closeDropDownMenu = () => {
    this.setState({
      isProfileMenuOpen: false,
      isSearchMenuOpen: false,
    });
  };

  previewCategories = (children) =>
    children.map((item, i) => (
      <React.Fragment>
        <li>
          <NavLink
            to={`/category/${item.slug}#results_list`}
            itemProp="url"
            exact
            activeClassName="active"
            isActive={(match, loc) =>
              RegExp('^(/*)$').test(loc.pathname) ||
              RegExp('^/market').test(loc.pathname)
            }
          >
            {item.name}
          </NavLink>

          {item.children.length > 0 && (
            <ul style={{ padding: '0 2em', margin: '0 auto 1em' }}>
              {this.previewCategories(item.children)}
            </ul>
          )}
        </li>
      </React.Fragment>
    ));

  /**
   * renderSearchCategoriesDropdown()
   * Renders a Categories DropdownMenu with a seach box. (Used for 'mobile' sizes)
   */
  renderSearchCategoriesDropdown = () => (
    <SearchMenuDropdown
      closeMenu={this.closeDropDownMenu}
      history={this.props.history}
      categories={this.props.marketCategories}
      active={this.state.isSearchMenuOpen}
      dispatch={this.props.dispatch}
    />
  );

  render() {
    let toolbar = null;
    let storeButton;
    const {
      user,
      appSection,
      cartProductsCount,
      isLogged,
      isMobile,
      history,
      marketCategories,
      openModalBox,
      logout,
      dispatch,
    } = this.props;

    // Filter Menu Options Depending on User Status
    if (isLogged) {
      // Filter Options if User Has a Store
      if (!user.has_store) {
        storeButton = (
          <li className="toolbar__text login">
            <Link to="/stores/create">Abre una Tienda</Link>
          </li>
        );
      } else {
        storeButton = (
          <li className="toolbar__item store">
            <Link to="/my-store/dashboard">Mi Tienda</Link>
          </li>
        );
      }

      // Menu Options
      toolbar = <ul className="toolbar">{storeButton}</ul>;
    }

    return (
      <MainMenu
        user={user}
        isLogged={isLogged}
        appSection={appSection}
        history={history}
        cartProductsCount={cartProductsCount}
        openModalBox={openModalBox}
        isMobile={isMobile}
        logout={logout}
        dispatch={dispatch}
        marketCategories={marketCategories}
      />
    );
  }
}

// Pass Redux state and actions to component
function mapStateToProps(state) {
  const { cart, users, app } = state;
  const user = getUserProfile(state);
  return {
    user,
    cart,
    appSection: app.section,
    isMobile: app.isMobile,
    isLogged: users.isLogged,
    cartProductsCount: getCartProductsCount(state),
    marketCategories: app.marketCategories.categories,
  };
}
function mapDispatchToProps(dispatch) {
  const { getSearchResults, openSearchBar, closeSearchBar } = appActions;
  return bindActionCreators(
    {
      getSearchResults,
      openModalBox: modalBoxActions.open,
      openSearchBar,
      closeSearchBar,
      dispatch,
      logout: userActions.logout,
    },
    dispatch,
  );
}

// Wrap component with router component
Header = withRouter(Header);
Header = connect(mapStateToProps, mapDispatchToProps)(Header);

// Export Component
export default Header;
