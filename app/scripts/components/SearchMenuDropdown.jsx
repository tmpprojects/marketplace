import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';

import withDropDown from './hocs/withDropDown';

class SearchMenuDropdown extends Component {
  /*
   * On Search Submit
   * Redirect user to Search Results Page
   */
  onSearchSubmit = (e) => {
    e.preventDefault();
    this.props.history.push(`/search/${this.search_field.value}`);
    this.props.closeMenu();
  };

  render() {
    const { categories = [] } = this.props;
    return (
      <nav className="menu-dropdown__container">
        <ul>
          <li className="search">
            <form onSubmit={this.onSearchSubmit}>
              <fieldset>
                <input
                  type="text"
                  ref={(field) => {
                    this.search_field = field;
                  }}
                  placeholder="Buscar productos/tiendas"
                />
              </fieldset>
            </form>
          </li>

          {categories.map((category, i) => (
            <li
              itemProp="name"
              onClick={this.props.closeMenu}
              key={category.slug}
              className={category.slug}
            >
              <NavLink itemProp="url" to={`/category/${category.slug}`}>
                {category.name}
              </NavLink>
            </li>
          ))}

          <li
            itemProp="name"
            onClick={this.props.closeMenu}
            className="envio-nacional"
          >
            <NavLink itemProp="url" to="/category/envio-nacional">
              Envío Nacional
            </NavLink>
          </li>
        </ul>
      </nav>
    );
  }
}

// Export Component
export default withDropDown(SearchMenuDropdown);
