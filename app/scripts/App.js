import { hot } from 'react-hot-loader';
import { default as reduxActions } from 'redux-form/lib/actions';
import Sentry from './Utils/Sentry';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { renderRoutes } from 'react-router-config';
import Cookies from 'universal-cookie';
import { ModifyCache } from '../scripts/Actions/localRedis';
import LogRocket from 'logrocket';
import { formValueSelector } from 'redux-form';
import { dataLayer } from './Utils/dataLayer/dataLayer';

const { change } = reduxActions;

import {
  appActions,
  userActions,
  landingActions,
  modalBoxActions,
  shoppingCartActions,
} from './Actions';
import { APP_SECTIONS } from '../scripts/Constants/config.constants';
import { SHOPPING_CART_FORM_CONFIG } from './Utils/shoppingCart/shoppingCartFormConfig';
import withAnalyticsTracker from '../scripts/components/hocs/withAnalyticsTracker';
import config from '../config';

import '../styles/main.scss';
import '../styles/_orders.scss';
import SEO from '../statics/SEO.json';
import PageHead from './Utils/PageHead';
import Header from './components/Header';
import Footer from './components/Footer';
import ModalBox from './Utils/ModalBox';
import ScrollToTop from './Utils/ScrollToTop';
import { StatusWindow } from './Utils/StatusWindow';
import Search from './components/Search/SearchForm';
import defineHoverClass from './Utils/defineHoverClass';
import MenuCategories from './components/MenuCategories';
import ShoppingCartResume from './Utils/ShoppingCartResume';
import GetCoupon from './Utils/modalbox/GetCoupon';
import LeaveSite from './Utils/modalbox/LeaveSite';
import CookieConsent from './components/Home/CookieConsent';
import NewHeader from './components/Header2020/Desktop/Header';
import NewHeaderMobile from './components/Header2020/Mobile/HeaderMobile';
//
class EventObserver {
  constructor() {
    this.observers = [];
  }

  subscribe(fn) {
    this.observers.push(fn);
  }
  unsubscribe(fn) {
    this.observers = this.observers.filter((subscriber) => subscriber !== fn);
  }
  broadcast(data) {
    this.observers.forEach((subscriber) => subscriber(data));
  }
}
globalThis.shippingAddressObserver = new EventObserver();

//
const getUserAddress = (state) => {
  const formSelector = formValueSelector(SHOPPING_CART_FORM_CONFIG.formName);
  const addressesList = state.users.addresses.addresses;
  const selectedShippingAddress =
    formSelector(state, 'selectedShippingAddress') || undefined;
  // If 'shopping form' already has a selected address,
  // choose this address to keep persistance.
  let defaultAddress = addressesList.find((address) => {
    // Set selected address.
    if (selectedShippingAddress && selectedShippingAddress === address.uuid) {
      return address;
    }
  });

  if (!defaultAddress && !state.users.isLogged) {
    const cookie = window.localStorage.getItem('shippingAddress');
    if (cookie) {
      defaultAddress = JSON.parse(cookie);
    }
  }

  if (!defaultAddress) {
    defaultAddress = addressesList.reduce((found, a, i, l) => {
      // If the address has not been found yet,
      // search whithin different options.
      if (!found) {
        let targetAddress = false;

        // If the address´s not been found,
        // search if this is the default address.
        if (a.default_address && !targetAddress) {
          targetAddress = a;
        }

        // If the above options failed to set an address,
        // choose the first address on the list.
        if (i === l.length - 1 && !targetAddress) {
          targetAddress = l[0];
        }

        // Return result.
        return targetAddress;
      }
      return found;
    }, false);
  }

  return defaultAddress;
};

/**
 * getAppSection()
 * @param {string} routePath : URL path to analyze
 * @return {string} Returns a string indicating the current App Section
 */
const getAppSection = (routePath) => {
  // Get App section/page based on URL Path Location
  const locationPath = routePath.split('/');
  const sectionPath = locationPath[1];

  let section;
  switch (sectionPath) {
    case APP_SECTIONS.STORE_APP:
      section = APP_SECTIONS.STORE_APP;
      break;
    case APP_SECTIONS.PRO:
      section = APP_SECTIONS.PRO;
      break;
    case APP_SECTIONS.SHOPPING_CART:
      section = APP_SECTIONS.SHOPPING_CART;
      break;
    case APP_SECTIONS.CART:
      section = APP_SECTIONS.CART;
      break;
    case APP_SECTIONS.INSPIRE:
      section = APP_SECTIONS.INSPIRE;
      break;
    case APP_SECTIONS.ABOUT_US:
      section = APP_SECTIONS.ABOUT_US;
      break;
    case APP_SECTIONS.AWARDS:
      section = APP_SECTIONS.AWARDS;
      break;
    case APP_SECTIONS.LANDING:
      section = APP_SECTIONS.LANDING;
      break;
    default:
      section = APP_SECTIONS.MARKET;
  }

  // Return section name
  return section;
};

/*------------------------------------------
//	APP COMPONENT
------------------------------------------*/

class App extends Component {
  constructor(props) {
    super(props);
    dataLayer();
    this.state = {
      search_term: null,
      searchRedirect: false,
      shoppingCartState: false,
      width: null,
      hidingMenu: true,
    };
    this.shoppingCartDelay = 2200;
    this.shoppingCartInterval = 0;
    const { staticContext = {} } = props;
    staticContext.isLogged = this.props.currentUser
      ? this.props.currentUser.isLogged
      : false;
  }

  componentDidMount() {


    //
    if (!this.props.currentUser.isLogged) {
      // Get User Status
      this.props
        .getCurrentUser()
        .then((response) => this.getUserData())
        .catch((e) => {
          const shippingAddress = getUserAddress(this.props.state);
          if (shippingAddress) {
            this.props.addGuestAddress(shippingAddress);
          }
        });
    } else {
      this.getUserData();
    }

    // Get Cart Products
    if (!this.props.cart.previouslyLoaded) {
      this.props.fetchShoppingCart();
    }
    // Get Market Categories
    if (!this.props.marketCategories.previouslyLoaded) {
      this.props.getMarketCategories();
    }
    // Get Merket Interests
    if (!this.props.marketInterests.previouslyLoaded) {
      this.props.getMarketInterests();
    }
    // Get Pickup Schedules
    if (!this.props.pickupSchedules.previouslyLoaded) {
      this.props.getPickupSchedules();
    }
    // Get shipping methods
    if (!this.props.shippingMethods.previouslyLoaded) {
      this.props.getShippingMethods();
    }
    // Get payment methods
    if (!this.props.paymentMethods.previouslyLoaded) {
      this.props.getPaymentMethods();
    }
    // Get collections list
    if (!this.props.collections.landingsList.previouslyLoaded) {
      this.props.fetchCollectionsList();
    }
    // Get prohibited Words
    if (!this.props.prohibitedWords.previouslyLoaded) {
      this.props.getProhibitedWords();
    }

    // Modalbox
    const cookies = new Cookies();
    const promoModalShown = cookies.get('promoCodeModal');

    // Only popup --> home
    if (this.props.location.pathname == '/') {
      if (!this.props.currentUser.isLogged && !promoModalShown) {
        cookies.set('promoCodeModal', true, { path: '/' });
        if (process.env.CLIENT) {
          // Delay the loading time of the modalbox to improve performance
          // window.setTimeout(() => {
          //   console.log("OPEN");
          // }, 9000);
          // this.props.openModalBox(() => <GetCoupon />);
        }
      }
    } else {
      // console.log("not SHOW OPEN MODAL BOX");
    }
    // window.addEventListener('beforeunload', this.showModalBox);

    // Load Facebook Libraries and check login status
    // only when app is running on browser (client side)
    if (process.env.CLIENT) {
      // Add 'hover class' to Document if client is not a touch device
      defineHoverClass();

      //
      function Deferred() {
        const self = this;
        this.promise = new Promise((resolve, reject) => {
          self.reject = reject;
          self.resolve = resolve;
        });
      }
      window.fbLoaded = new Deferred();

      // When Facebook libraries are loaded
      // get logged user data
      window.fbAsyncInit = function () {
        //SDK loaded, initialize it
        FB.init({
          appId: config.facebookAuth.clientID,
          cookie: true,
          version: 'v2.11',
        });
        window.fbLoaded.resolve();

        //check user session and refresh it
        FB.getLoginStatus((response) => {
          //console.log('status', response);
          if (response.status === 'connected') {
            //user is authorized
          } else {
            //user is not authorized
          }
        });
      };

      // Load Facebook JavaScript SDK
      (function (d, s, id) {
        let js,
          fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) {
          return;
        }
        js = d.createElement(s);
        js.id = id;
        js.src = '//connect.facebook.com/en_US/sdk.js';
        fjs.parentNode.insertBefore(js, fjs);
      })(document, 'script', 'facebook-jssdk');

      // Load MercadoPagos SDK
      (function (d, s, id) {
        let js,
          fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) {
          return;
        }
        js = d.createElement(s);
        js.id = id;
        fjs.parentNode.insertBefore(js, fjs);
        js.onload = () => {
          Mercadopago.setPublishableKey(config.mercadopagoAuth.publicKey);
        };
        js.src = 'https://secure.mlstatic.com/sdk/javascript/v1/mercadopago.js';
      })(document, 'script', 'mercadopago-sdk');

      // App Window Resize listener
      window.addEventListener('resize', this.onResize);
      this.onResize();
    }
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.cart.products !== nextProps.cart.products) {
      clearInterval(this.shoppingCartInterval);
      this.shoppingCartInterval = setTimeout(() => {
        this.setState({
          shoppingCartState: false,
        });
      }, this.shoppingCartDelay);

      // Update State
      this.setState({
        shoppingCartState: true,
      });
    }

    // Get App section/page based on URL Path Location
    if (this.props.location.pathname !== nextProps.location.pathname) {
      // Update section
      const sectionName = getAppSection(nextProps.location.pathname);
      this.props.setAppSection(sectionName);
    }
  }

  componentWillUnmount() {
    window.removeEventListener('beforeunload', this.showModalBox);
    window.removeEventListener('resize', this.onResize);
  }

  getUserData = () => {
    // Get User Addresses
    if (!this.props.currentUser.addresses.previouslyLoaded) {
      this.props.getUserAddresses();
    }
    // Get User Reviews
    if (!this.props.currentUser.reviews.pending.previouslyLoaded) {
      this.props.getUserReviews(
        '?page=1&page_size=6&review__isnull=true',
        'pending',
      );
    }
    if (!this.props.currentUser.reviews.completed.previouslyLoaded) {
      this.props.getUserReviews(
        '?page=1&page_size=6&review__isnull=false',
        'completed',
      );
    }

    // Set deafult shipping address
    const shippingAddress = getUserAddress(this.props.state);
    if (shippingAddress) {
      this.props.changeShippingAddress(shippingAddress);
    }

    // This is an example script - don't forget to change it!
    LogRocket.identify(this.props.currentUser.profile.username, {
      name: `${this.props.currentUser.profile.first_name} ${this.props.currentUser.profile.last_name}`,
      email: this.props.currentUser.profile.email,

      // Add your own custom user variables here, ie:
      subscriptionType: 'pro',
    });

    //Clear local cache
    ModifyCache();
  };
  // onHidingMenu = (value) => {
  //   this.setState({ hidingMenu: value });
  // };

  onResize = () => {
    const w =
      window.innerWidth ||
      document.documentElement.clientWidth ||
      document.body.clientWidth;
    this.props.isMobile(w < 600);
    this.setState({
      width: w,
    });
  };

  showModalBox = () => {
    this.props.openModalBox(() => <LeaveSite />);
  };

  componentDidCatch(error, errorInfo) {
    Sentry.withScope((scope) => {
      Object.keys(errorInfo).forEach((key) => {
        scope.setExtra(key, errorInfo[key]);
      });
      Sentry.captureException(error);
    });
  }

  render() {
    const {
      appSection,
      statusWindow,
      openSearchBar,
      marketCategories,
      marketInterests,
    } = this.props;

    // Render App
    return (
      <ScrollToTop>
        <PageHead />

        <div className={`app-layout ${appSection}`}>
          {openSearchBar && <Search />}

          {(appSection === APP_SECTIONS.MARKET ||
            appSection === APP_SECTIONS.INSPIRE ||
            appSection === APP_SECTIONS.PRO ||
            appSection === APP_SECTIONS.CART ||
            appSection === APP_SECTIONS.AWARDS ||
            appSection === APP_SECTIONS.LANDING) && (
              <>
                <div className="header-fix" tandindex="-1">
                  <NewHeader
                    appSection={this.props.appSection}
                    history={this.props.history}
                    currentUser={this.props.currentUser}
                    getUserAddresses={this.props.getUserAddresses}
                    marketCategories={marketCategories.categories}
                    cart={this.props.cart}
                    // onHidingMenu={this.onHidingMenu}
                    // hidingMenu={this.state.hidingMenu}
                    user={this.props.currentUser}
                  />
                </div>
                <div className="header-fix-mobile">
                  <NewHeaderMobile
                    appSection={this.props.appSection}
                    history={this.props.history}
                    currentUser={this.props.currentUser}
                    getUserAddresses={this.props.getUserAddresses}
                    marketCategories={marketCategories.categories}
                    marketInterests={marketInterests.interests}
                    cart={this.props.cart}
                    // onHidingMenu={this.onHidingMenu}
                    // hidingMenu={this.state.hidingMenu}
                    user={this.props.currentUser}
                  />
                </div>
              </>
            )}

          <div
            className={
              this.state.hidingMenu
                ? 'app-body-layout'
                : 'app-body-layout-hidingMenu'
            }
          >
            <div style={{ flex: '1' }} className="body-sections">
              {renderRoutes(this.props.route.routes, {
                //staticContext: this.props.staticContext
              })}
            </div>

            {appSection !== APP_SECTIONS.STORE_APP &&
              appSection !== APP_SECTIONS.AWARDS && <Footer />}

            <ModalBox />

            {/* {appSection === APP_SECTIONS.MARKET && (
              <ShoppingCartResume state={this.state.shoppingCartState} />
            )} */}

            {statusWindow.shouldDisplay && (
              <StatusWindow
                type={statusWindow.type}
                message={statusWindow.message}
              />
            )}
            <CookieConsent />
          </div>
        </div>
      </ScrollToTop>
    );
  }
}

// Wrapp application with Analytics tracker
App = withAnalyticsTracker(App);

// Load Data for Server Side Rendering
App.loadData = (reduxStore, routeData) => {
  const sectionName = getAppSection(routeData.location);
  return Promise.all([
    reduxStore.dispatch(landingActions.fetchListData()),
    reduxStore.dispatch(appActions.getShippingMethods()),
    reduxStore.dispatch(appActions.getPaymentMethods()),
    reduxStore.dispatch(appActions.getPickupSchedules()),
    reduxStore.dispatch(appActions.getMarketCategories()),
    reduxStore.dispatch(appActions.getMarketInterests()),
    reduxStore.dispatch(appActions.setAppSection(sectionName)),
    reduxStore.dispatch(shoppingCartActions.fetchShoppingCart()),

    //----------------------------------------------------//
    // User data is not mandatory.
    // Wrapp action on other promise so we can ALWAYS resolve
    // important for SSR (Server Side Rendering)
    new Promise((resolve) =>
      reduxStore.dispatch(userActions.listAddresses()).then(resolve).catch(resolve),
    ),
    new Promise((resolve) =>
      reduxStore.dispatch(userActions.getCurrentUser()).then(resolve).catch(resolve),
    ),
    new Promise((resolve) =>
      reduxStore
        .dispatch(
          userActions.getUserReviews(
            '?page=1&page_size=6&review__isnull=true',
            'pending',
          ),
        )
        .then(resolve)
        .catch(resolve),
    ),
    new Promise((resolve) =>
      reduxStore
        .dispatch(
          userActions.getUserReviews(
            '?page=1&page_size=6&review__isnull=false',
            'completed',
          ),
        )
        .then(resolve)
        .catch(resolve),
    ),
    //----------------------------------------------------//
  ]);
};

// Pass Redux state and actions to component
function mapStateToProps(state) {
  const { users, modalBox, statusWindow, app, cart, inspire, landing } = state;

  // Pass Properties
  return {
    state,
    cart,
    modalBox,
    statusWindow,
    currentUser: users,
    collections: landing,
    appSection: app.section,
    openSearchBar: app.openSearchBar,
    paymentMethods: app.paymentMethods,
    pickupSchedules: app.pickupSchedules,
    shippingMethods: app.shippingMethods,
    inspireTags: inspire.tags.results || [],
    marketCategories: app.marketCategories,
    marketInterests: app.marketInterests,
    prohibitedWords: app.prohibitedWords,
  };
}
function mapDispatchToProps(dispatch) {
  const {
    isMobile,
    setAppSection,
    getFeaturedStores,
    getShippingMethods,
    getPaymentMethods,
    getPickupSchedules,
    getMarketCategories,
    getMarketInterests,
    getProhibitedWords,
  } = appActions;
  const { fetchListData: fetchCollectionsList } = landingActions;
  const { open: openModalBox } = modalBoxActions;
  const {
    getCurrentUser,
    listAddresses: getUserAddresses,
    getUserReviews,
    addGuestAddress,
  } = userActions;
  const {
    changePaymentMethod,
    fetchShoppingCart,
    changeShippingAddress,
  } = shoppingCartActions;

  // return Actions
  return {
    dispatch,
    ...bindActionCreators(
      {
        isMobile,
        openModalBox,
        setAppSection,
        addGuestAddress,
        getCurrentUser,
        fetchShoppingCart,
        fetchCollectionsList,
        changeShippingAddress,
        getUserReviews,
        getUserAddresses,
        getFeaturedStores,
        getShippingMethods,
        getPaymentMethods,
        getPickupSchedules,
        getMarketCategories,
        getMarketInterests,
        getProhibitedWords,
        changePaymentMethod,
      },
      dispatch,
    ),
  };
}

// Export App
App = connect(mapStateToProps, mapDispatchToProps)(App);
export default hot(module)(App);
