import { useState, useEffect } from 'react';

// Hook
export function useWindowSize() {
  // Initialize state with undefined width/height so server and client renders match
  const [windowSize, setWindowSize] = useState({
    width: undefined,
    height: undefined,
  });

  useEffect(() => {
    // Handler to call on window resize
    function handleResize() {
      // Set window width/height to state
      setWindowSize({
        width:
          window.innerWidth ||
          document.documentElement.clientWidth ||
          document.body.clientWidth,
        height:
          window.innerHeight ||
          document.documentElement.clientHeight ||
          document.body.clientHeight,
      });
    }

    // Add event listener
    window.addEventListener('resize', handleResize);

    // Call handler right away so state gets updated with initial window size
    handleResize();

    // Remove event listener on cleanup
    return () => window.removeEventListener('resize', handleResize);
  }, []); // Empty array ensures that effect is only run on mount

  return windowSize;
}

export function useIsMobile() {
  const [isMobile, setIsMobile] = useState(false);

  let size = useWindowSize();

  useEffect(() => {
    if (size.width !== undefined) {
      if (size.width < 769) {
        setIsMobile(true);
      } else {
        setIsMobile(false);
      }
    }
  }, [size.width]);

  return isMobile;
}

export function useIsTablet() {
  const [isTablet, setIsTablet] = useState(false);

  let size = useWindowSize();

  useEffect(() => {
    if (size.width !== undefined) {
      if (size.width <= 769 && size.width > 426) {
        setIsTablet(true);
      } else {
        setIsTablet(false);
      }
    }
  }, [size.width]);

  return isTablet;
}
