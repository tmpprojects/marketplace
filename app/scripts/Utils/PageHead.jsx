import React from 'react';
import { Helmet } from 'react-helmet';
import SEO from '../../statics/SEO.json';
import { facebookAuth } from '../../config';

export default (props) => {
  const pageTags = props.attributes || SEO.default;
  if (!pageTags) return null;

  return (
    <Helmet>
      <html amp lang="es" />
      <title>{pageTags.title}</title>
      <meta property="fb:app_id" content={`${facebookAuth.clientID}`} />
      {pageTags.meta !== undefined &&
        pageTags.meta.map((tag, i) => <meta key={i} {...tag} />)}
      {pageTags.tags !== undefined && (
        <meta name="tags" content={pageTags.tags.join(', ')} />
      )}
      {pageTags.jsonLD !== undefined && (
        <script type="application/ld+json">{`${JSON.stringify(
          pageTags.jsonLD,
        )}`}</script>
      )}
    </Helmet>
  );
};
