import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';
import { Transition } from 'react-transition-group';

import { statusWindowActions } from '../Actions';

const duration = 280;
const initY = '-100px';
const targetY = '16px';
const defaultStyle = {
  transition: `all ${duration}ms ease-in-out`,
  opacity: 0,
  bottom: initY,
};
const transitionStyles = {
  entering: { opacity: 0, bottom: initY },
  entered: { opacity: 1, bottom: targetY },
  exiting: { opacity: 1, bottom: targetY },
  exited: { opacity: 0, bottom: initY },
};

class StatusWindow extends Component {
  state = {
    isVisible: true,
  };
  aliveTimer = 0;

  closeWindow = () => {
    this.setState({
      isVisible: false,
    });
  };

  componentDidMount() {
    this.aliveTimer = setTimeout(this.closeWindow, 3000);
  }

  componentWillUnmount() {
    clearTimeout(this.aliveTimer);
  }

  render() {
    return (
      <Transition
        in={this.state.isVisible}
        timeout={duration}
        appear={true}
        onExited={() => setTimeout(this.props.closeWindow, 300)}
      >
        {(status) => (
          <div
            style={{
              ...defaultStyle,
              ...transitionStyles[status],
            }}
            className={`statusWindow statusWindow--${this.props.type}`}
            ref={(windowContainer) => {
              this.windowContainer = windowContainer;
            }}
          >
            {this.props.message}
          </div>
        )}
      </Transition>
    );
  }
}

// PropTypes Definition
StatusWindow.propTypes = {
  type: PropTypes.string.isRequired,
  message: PropTypes.string.isRequired,
};

function mapStateToProps() {
  return {};
}
function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      closeWindow: statusWindowActions.close,
    },
    dispatch,
  );
}

const connectedComponent = connect(
  mapStateToProps,
  mapDispatchToProps,
)(StatusWindow);
export { connectedComponent as StatusWindow };
