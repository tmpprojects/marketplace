import React from 'react';
import { Component, PureComponent } from 'react';
import PropTypes from 'prop-types';

// CONSTANTS AND STYLE DEFINITIONS
const IMAGE_FORMATS = {
  HORIZONTAL: 'horizontal',
  VERTICAL: 'vertical',
};
const duration = 200;
const staticStyle = {
  opacity: 1,
};
const defaultStyle = {
  position: 'absolute',
  transition: `all ${duration}ms ease-in-out`,
};
const transitionStyles = {
  loading: { opacity: 0 },
  loaded: { opacity: 1 },

  [IMAGE_FORMATS.HORIZONTAL]: { width: '100%', height: 'auto' },
  [IMAGE_FORMATS.VERTICAL]: { width: 'auto', height: '100%' },
};

/** --------------------------------------------------
 *      RESPONSIVE IMAGE FROM SOURCE COMPONENT
 * ---------------------------------------------------
 * Preloads and Render an image element that scales proportionally
 * on 'resize' and 'load' events depending on 'parentNode' dimmensions.
 */
export class ResponsiveImageFromURL extends PureComponent {
  // PropTypes Definition
  static propTypes = {
    src: PropTypes.string.isRequired,
    alt: PropTypes.string.isRequired,
    alt: PropTypes.string.isRequired,
    lazy: PropTypes.bool,
  };
  static defaultProps = {
    lazy: true,
    src: '',
    alt: '',
  };

  /**
   * constructor()
   * @param {object} props : Component Properties
   */
  constructor(props) {
    super(props);
    this.state = {
      format: IMAGE_FORMATS.HORIZONTAL,
      status: 'idle',
    };
    this.observer = null;
  }

  /**
   * loadImage()
   * If the image is not loaded on the DOM,
   * set 'load' and 'resize' events to control proportional (ratio) scaling
   */
  loadImage = () => {
    this.image.removeEventListener('load', this.handleLoad);
    this.image.addEventListener('load', this.handleLoad);
    window.removeEventListener('resize', this.handleResize);
    window.addEventListener('resize', this.handleResize);

    //
    if (this.props.lazy) {
      // Load Image
      this.observer.observe(this.image);
    } else {
      this.image.src = this.props.src;
    }
  };

  /**
   * handleResize()
   * @param {object} e : Resize event
   */
  handleResize = (e) => {
    // Resize Image
    const imageFormat = this.resizeImage(this.image);

    // Update State
    this.setState({
      status: 'loaded',
      format: imageFormat,
    });
  };

  /**
   * handleLoad()
   * @param {object} e : Load event
   */
  handleLoad = (e) => {
    // Resize Image
    const imageFormat = this.resizeImage(e.target);

    // Update State
    this.setState({
      status: 'loaded',
      format: imageFormat,
    });
  };

  /**
   * resizeImage()
   * @param {element} image : Image Node
   * @return {string} Image ratio format IMAGE_FORMATS.HORIZONTAL || IMAGE_FORMATS.VERTICAL
   */
  resizeImage = (image) => {
    if (!image) return;

    // Get Ratios
    const parentRatio = image.parentNode.offsetHeight / image.parentNode.offsetWidth;
    const imageRatio = image.offsetHeight / image.offsetWidth;

    // Get Image Format
    let imageFormat;
    if (imageRatio >= parentRatio) {
      imageFormat = IMAGE_FORMATS.HORIZONTAL;
    } else {
      imageFormat = IMAGE_FORMATS.VERTICAL;
    }

    // Return image format
    return imageFormat;
  };

  /*
   * React Component Life Cycle Functions
   */
  componentWillUnmount() {
    if (this.observer) {
      this.observer.disconnect();
    }
    this.observer = null;
    window.removeEventListener('resize', this.handleResize);
    this.image.removeEventListener('load', this.handleLoad);
  }
  componentWillReceiveProps(nextProps) {
    if (this.props !== nextProps) {
      this.loadImage();
    }
  }
  componentDidMount() {
    this.observer = new IntersectionObserver(
      (entries, observer) => {
        entries.forEach((entry) => {
          const { isIntersecting, intersectionRatio } = entry;
          if (isIntersecting === true || intersectionRatio > 0) {
            entry.target.src = this.props.src;
            if (this.observer) {
              this.observer.unobserve(entry.target);
            }
          }
        });
      },
      { rootMargin: '0px 0px 200px 0px' },
    );
    this.loadImage();
  }
  render() {
    const { format, status } = this.state;
    const { className = '', alt, src } = this.props;
    let styles = {
      ...staticStyle,
      ...defaultStyle,
    };

    // Determine image styles
    if (status === 'loaded') {
      styles = {
        ...styles,
        ...transitionStyles[status],
        ...transitionStyles[format],
      };
    }

    // Render Component
    return (
      <img
        style={styles}
        className={className}
        ref={(image) => (this.image = image)}
        src={src}
        alt={alt}
      />
    );
  }
}

/** ----------------------------------------
 *      RESPONSIVE IMAGE COMPONENT
 * -----------------------------------------
 * Recieves a mandatory object with multiple image size sources.
 * Uses <picture /> element to render images responsively,
 * if an image is not available, it searches for the best fit.
 * Subscribes to 'resize' and 'load' events to handle image size and ratio
 * based on 'parentNode' dimmensions.
 */
export class ResponsiveImage extends PureComponent {
  // PropTypes Definition
  static propTypes = {
    src: PropTypes.object.isRequired,
    alt: PropTypes.string.isRequired,
    lazy: PropTypes.bool,
  };
  static defaultProps = {
    lazy: true,
    src: '',
    alt: '',
  };

  /**
   * constructor()
   * @param {object} props : Component Properties
   */
  constructor(props) {
    super(props);
    this.state = {
      format: IMAGE_FORMATS.HORIZONTAL,
      status: 'idle',
    };
    this.observer = null;
  }

  /**
   * loadImage()
   * If the image is not loaded on the DOM,
   * set 'load' and 'resize' events to control proportional (ratio) scaling
   */
  loadImage = () => {
    this.image.removeEventListener('load', this.handleLoad);
    this.image.addEventListener('load', this.handleLoad);
    window.removeEventListener('resize', this.handleResize);
    window.addEventListener('resize', this.handleResize);

    //
    if (this.props.lazy) {
      // Load Image
      this.observer.observe(this.image);
    } else {
      this.image.src = this.getImageSource('medium');
    }
  };

  /**
   * handleResize()
   * @param {object} e : Resize event
   */
  handleResize = (e) => {
    // Resize Image
    const imageFormat = this.resizeImage(this.image);

    // Update State
    this.setState({
      status: 'loaded',
      format: imageFormat,
    });
  };

  /**
   * handleLoad()
   * @param {object} e : Load event
   */
  handleLoad = (e) => {
    // Resize Image
    const imageFormat = this.resizeImage(e.target);

    // Update State
    this.setState({
      status: 'loaded',
      format: imageFormat,
    });
  };

  /**
   * resizeImage()
   * @param {element} image : Image Node
   * @return {string} Image ratio format IMAGE_FORMATS.HORIZONTAL || IMAGE_FORMATS.VERTICAL
   */
  resizeImage = (image) => {
    if (!image) return;

    // Get Ratios
    const parentRatio = image.parentNode.offsetHeight / image.parentNode.offsetWidth;
    const imageRatio = image.offsetHeight / image.offsetWidth;

    // Get Image Format
    let imageFormat;
    if (imageRatio >= parentRatio) {
      imageFormat = IMAGE_FORMATS.HORIZONTAL;
    } else {
      imageFormat = IMAGE_FORMATS.VERTICAL;
    }

    // Return image format
    return imageFormat;
  };

  /**
   * getImageSource()
   * @param {string} size : Image size
   * @return {string} Returns image source of the specified size
   */
  getImageSource = (size = 'small') => {
    return this.props.src[size];
  };

  /**
   * getSourceElements()
   * Returns a list of available <sources /> to populate <picture /> element
   */
  getSourceElements = () => {
    const sizes = ['(max-width: 60em)', '(min-width: 60em)', '(min-width: 80em)'];
    const list = {
      small: this.props.src.small,
      medium: this.props.src.medium,
      big: this.props.src.big,
    };
    let usedIndex = 0;
    const sources = Object.keys(list).reduce((sources, size, index) => {
      if (size !== 'original' && list[size] !== '' && list[size]) {
        const newSources = [
          ...sources,
          <source
            key={index}
            srcSet={list[size]}
            className="responsive-image"
            media={`${sizes[usedIndex]}`}
          />,
        ];
        usedIndex++;
        return newSources;
      }
      return sources;
    }, []);
    return sources;
  };

  /*
   * React Component Life Cycle Functions
   */
  componentWillUnmount() {
    if (this.observer) {
      this.observer.disconnect();
    }
    this.observer = null;
    window.removeEventListener('resize', this.handleResize);
    this.image.removeEventListener('load', this.handleLoad);
  }
  componentWillReceiveProps(nextProps) {
    if (this.props !== nextProps) {
      this.loadImage();
    }
  }
  componentDidMount() {
    this.observer = new IntersectionObserver((entries, observer) => {
      entries.forEach((entry) => {
        const { isIntersecting, intersectionRatio } = entry;
        if (isIntersecting === true || intersectionRatio > 0) {
          entry.target.src = this.getImageSource('medium');
          if (this.observer) {
            this.observer.unobserve(entry.target);
          }
        }
      });
    });
    this.loadImage();
  }

  render() {
    const { format, status } = this.state;
    const { className = '', alt, src } = this.props;
    let styles = {
      ...staticStyle,
      ...defaultStyle,
    };

    // Determine image styles
    if (status === 'loaded') {
      styles = {
        ...styles,
        ...transitionStyles[status],
        ...transitionStyles[format],
      };
    }

    // Render Component
    return (
      <picture className={`responsive-picture ${className}`}>
        {this.getSourceElements()}
        <img
          style={styles}
          className="responsive-image"
          src={src}
          alt={alt}
          ref={(image) => (this.image = image)}
        />
      </picture>
    );
  }
}

/** ----------------------------------------
 *      STANDARD IMAGE COMPONENT
 * -----------------------------------------
 * Preloads and Render a 'static' (standard) <img /> element.
 */
export class StandardImage extends PureComponent {
  // PropTypes Definition
  static propTypes = {
    src: PropTypes.string.isRequired,
    alt: PropTypes.string.isRequired,
    lazy: PropTypes.bool,
  };
  static defaultProps = {
    lazy: true,
    src: '',
    alt: '',
  };

  /**
   * constructor()
   * @param {object} props : Component Properties
   */
  constructor(props) {
    super(props);
    this.state = {
      status: 'idle',
    };
    this.observer = null;
  }

  /**
   * loadImage()
   * If the image is not loaded on the DOM, subscribe to a 'load' event
   */
  loadImage = () => {
    this.image.removeEventListener('load', this.handleLoad);
    this.image.addEventListener('load', this.handleLoad);

    //if (this.state.status !== 'loaded') {
    if (this.props.lazy) {
      // Load Image
      this.observer.observe(this.image);
    } else {
      this.image.src = this.props.src;
    }
  };

  /**
   * handleLoad()
   * @param {object} e : Load event
   */
  handleLoad = (e) => {
    // Update State
    this.setState({
      status: 'loaded',
    });
  };

  /*
   * React Component Life Cycle Functions
   */
  componentWillUnmount() {
    if (this.observer) {
      this.observer.disconnect();
    }
    this.observer = null;
    this.image.removeEventListener('load', this.handleLoad);
  }
  componentWillReceiveProps(nextProps) {
    if (this.props !== nextProps) {
      this.loadImage();
    }
  }
  componentDidMount() {
    this.observer = new IntersectionObserver((entries, observer) => {
      entries.forEach((entry) => {
        const { isIntersecting, intersectionRatio } = entry;
        if (isIntersecting === true || intersectionRatio > 0) {
          entry.target.src = this.props.src;
          if (this.observer) {
            this.observer.unobserve(entry.target);
          }
        }
      });
    });
    this.loadImage();
  }
  render() {
    const { status } = this.state;
    const { className = '', alt, src } = this.props;
    let styles = {
      ...staticStyle,
      ...defaultStyle,
    };

    // Determine image styles
    if (status === 'loaded') {
      styles = {
        ...styles,
        ...transitionStyles[status],
        ...transitionStyles[IMAGE_FORMATS.HORIZONTAL],
        position: 'relative',
      };
    }

    // Render Component
    return (
      <img
        style={styles}
        className={className}
        ref={(image) => (this.image = image)}
        src={src}
        alt={alt}
      />
    );
  }
}
