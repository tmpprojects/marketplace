import React, { Component } from 'react';
import Sentry from '../../Utils/Sentry';
import errorBoundary from '../../../images/illustration/errorBoundary.svg';
import './errorBoundary.scss';

class Error extends Component {
  static getDerivedStateFromError(error) {
    return { hasError: true };
  }

  state = {
    hasError: false,
  };

  compomentDidCatch(error) {
    console.log(error, 'Error');
    Sentry.captureException(error);
  }

  errorUi = () => (
    <div className="cr__error">
      <img
        src={errorBoundary}
        alt="Estamos presentado un problema en está sección"
        aria-label="Estamos presentado un problema en está sección"
      />
      <p className="cr__text--subtitle3 cr__textColor--colorDark300">
        Estamos presentando un problema en esta sección, ya estamos trabajando para
        resolverlo a la brevedad puedes seguir navegando en cualquier parte del
        sitio.
      </p>
    </div>
  );

  render() {
    const { children } = this.props;
    const { hasError } = this.state;

    if (hasError) return <this.errorUi />;

    if (!hasError) return children;
  }
}

export default Error;
