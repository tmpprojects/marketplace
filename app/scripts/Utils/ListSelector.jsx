import React from 'react';
import { Component } from 'react';
import PropTypes from 'prop-types';

/*---------------------------------------------------
    LIST SELECTOR UI COMPONENT
---------------------------------------------------*/
export default class ListSelector extends Component {
  // Static Properties Definition
  static propTypes = {
    open: PropTypes.bool,
    title: PropTypes.string,
    initialValues: PropTypes.object,
    options: PropTypes.array.isRequired,

    onOpen: PropTypes.func.isRequired,
    onClose: PropTypes.func.isRequired,
    onRemove: PropTypes.func.isRequired,
    onChange: PropTypes.func.isRequired,
  };
  static defaultProps = {
    open: false,
    title: 'Elige una opción.',
    onOpen: () => false,
    onClose: () => false,
    onRemove: () => false,
    onChange: (options) => options,
  };

  /**
   * constructor()
   * @param {object} props : Component properties
   */
  constructor(props) {
    super(props);
    this.state = {
      isOpen: props.open,
      selectedOption: props.initialValues || null,
    };
    this.ID = this.generateID();
  }

  /**
   * generateID()
   * generates a random ID to identify this component
   * @returns {string} id
   */
  generateID() {
    function s4() {
      return Math.floor((1 + Math.random()) * 0x10000)
        .toString(16)
        .substring(1);
    }
    return `${s4()}${s4()}-${s4()}${s4()}`;
  }

  /**
   * toggleList()
   * Toggles (open/closed) options list state.
   * @param {bool} force : Force the component to a fixed state
   */
  toggleList = (force) => {
    const isOpen = force !== undefined ? force : !this.state.isOpen;
    this.setState({
      isOpen,
    });
  };

  /**
   * closeWindow()
   * Execute parent´s callback
   */
  closeWindow = () => {
    this.toggleList(false);
  };

  /**
   * onOptionSelected()
   * Handler for 'onSelect' events on component´s list options
   * @param {object} selectedOption : Selected Option
   */
  onOptionSelected = (selectedOption) => {
    // Update component state
    this.setState({
      isOpen: false,
      selectedOption,
    });

    // Execute parent´s callback and send selected options
    this.props.onChange(selectedOption);
  };

  /**
   * renderOption()
   * Renders a single option for the 'options' list.
   * @param {object} option : Option to render
   * @param {int} id : Random integer
   */
  renderOption = (option, id) => {
    const { selectedOption } = this.state;
    const isActive =
      selectedOption && option.value === selectedOption.value ? 'active' : '';
    return (
      <div
        key={id}
        className={`options__item ${isActive}`}
        onClick={(e) => this.onOptionSelected(option)}
      >
        {option.name}
      </div>
    );
  };

  /**
   * render()
   */
  render() {
    const { title, options } = this.props;
    const { isOpen, selectedOption } = this.state;

    return (
      <div className="ui-list-selector">
        <div
          className="ui-list-selector__selection"
          onClick={(e) => this.toggleList()}
        >
          {selectedOption ? selectedOption.name : title}
        </div>
        <ul
          style={{ display: isOpen ? 'block' : 'none' }}
          className="ui-list-selector__options options hours"
        >
          {options.map((option, index) => this.renderOption(option, index))}
        </ul>
      </div>
    );
  }
}
