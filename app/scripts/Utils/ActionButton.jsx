import React, { Component } from 'react';
import { Transition } from 'react-transition-group';
import { IconPreloader } from './Preloaders';

const duration = 280;

// ActionButton Component
export class ActionButton extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      transition: false,
      disabled: this.props.disabled || false,
    };

    // Bind scope to methods
    this.timeout = 0;
    this.cancelablePromise = null;
    this.onButtonClick = this.onButtonClick.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.disabled !== nextProps.disabled) {
      // Update Button State
      this.setState({
        disabled: nextProps.disabled,
      });
    }
  }
  componentWillUnmount = () => {
    clearTimeout(this.timeout);
    if (this.cancelablePromise) {
      this.cancelablePromise.cancel();
    }
  };

  onButtonClick = (e) => {
    // Update Button State
    this.setState(
      {
        transition: true,
        disabled: true,
      },
      () => {
        // Execute parent onClick callback
        const returnValue = this.props.onClick(e);

        // If return value is a Promise, wait for the response to update button state
        if (returnValue && returnValue instanceof Promise) {
          this.cancelablePromise = this.makeCancelable(returnValue);
          //debugger;
          this.cancelablePromise.promise
            .then(() => {
              this.timeout = setTimeout(() => {
                // Re-enable button
                this.setState({
                  transition: false,
                  disabled: false,
                });
              }, 2000);
            })
            .catch((error) => {
              // Re-enable button
              this.setState({
                transition: false,
                disabled: false,
              });
            });

          // else, update button state immediatly
        } else {
          // Re-enable button
          this.setState({
            transition: false,
            disabled: false,
          });
        }
      },
    );
  };

  makeCancelable = (promise) => {
    let hasCanceled = false;
    const wrappedPromise = new Promise((resolve, reject) => {
      promise.then(
        (val) => (hasCanceled ? null : resolve(val)),
        (error) => null,
        //error => hasCanceled ? reject({isCanceled: true}) : reject(error)
      );
    });

    return {
      promise: wrappedPromise,
      cancel() {
        hasCanceled = true;
      },
    };
  };

  render() {
    const { disabled, transition } = this.state;
    const { className, icon, type = 'button', ...restProps } = this.props;
    let iconGraphic = '';

    // Define Icon depending on style
    switch (icon) {
      case 'check':
        iconGraphic = (
          <svg
            x="0px"
            y="0px"
            width="32px"
            height="32px"
            viewBox="0 0 32 32"
            className="checkmark checkmark-active"
          >
            <path
              fill="none"
              stroke="#ffffff"
              strokeWidth="2"
              strokeLinecap="square"
              strokeMiterlimit="10"
              d="M9,17l3.9,3.9c0.1,0.1,0.2,0.1,0.3,0L23,11"
            />
          </svg>
        );
        break;
      case 'loading':
        iconGraphic = (
          <IconPreloader
            style={{
              background: '#fff',
              margin: 0,
              top: '-14px',
              width: '25px',
              height: '25px',
            }}
          />
        );
        break;
      default:
        break;
    }

    return (
      <button
        {...restProps}
        type={type}
        disabled={disabled}
        className={`action-button action-button--${type} ${className}`}
        onClick={this.onButtonClick}
      >
        <Transition in={transition} timeout={duration}>
          {(state) => (
            <span className={`action-button__label action-button__label--${state}`}>
              {this.props.children}
            </span>
          )}
        </Transition>

        <Transition in={transition} timeout={duration}>
          {(state) => (
            <span className={`action-button__icon action-button__icon--${state}`}>
              {iconGraphic}
            </span>
          )}
        </Transition>
      </button>
    );
  }
}
