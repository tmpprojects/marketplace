/*------------------------------------------------
//    TRACKING UTILITY
------------------------------------------------*/
const createFunctionWithTimeout = function (callback, optTimeout) {
  let called = false;

  function fn() {
    if (!called) {
      called = true;
      callback();
    }
  }
  setTimeout(fn, optTimeout || 1000);
  return fn;
};

export const trackWithGTM = function (action, extraInfo, list) {
  if (!action) return false;

  if (
    process.env.ENABLE_GOOGLE_ANALYTICS &&
    process.env.CLIENT &&
    window.dataLayer
  ) {
    switch (action) {
      case 'Pageview':
        window.dataLayer.push({
          event: action,
          url: extraInfo,
        });
        break;
      case 'eec.purchase':
        window.dataLayer.push({
          event: action,
          ecommerce: {
            currencyCode: 'MXN',
            purchase: extraInfo,
          },
        });
        break;
      case 'eec.remove':
        window.dataLayer.push({
          event: action,
          ecommerce: {
            remove: extraInfo,
          },
        });
        break;
      case 'eec.add':
        window.dataLayer.push({
          event: action,
          currencyCode: 'MXN',
          ecommerce: {
            add: extraInfo,
          },
        });
        break;
      case 'Search':
        window.dataLayer.push({
          event: action,
          searchTerm: extraInfo,
        });
        break;
      case 'eec.impressionClick':
        window.dataLayer.push({
          event: 'eec.impressionClick',
          ecommerce: {
            click: {
              actionField: { list },
              products: extraInfo,
            },
          },
        });
        break;
      case 'eec.detail':
        window.dataLayer.push({
          event: 'eec.detail',
          ecommerce: {
            detail: {
              actionField: { list: extraInfo.list },
              products: extraInfo.products,
            },
          },
        });
        break;
      case 'eec.checkout':
        window.dataLayer.push({
          event: 'eec.checkout',
          ecommerce: {
            ...extraInfo,
          },
        });
        break;
      case 'eec.checkout.option':
        window.dataLayer.push({
          event: 'checkoutOption',
          ecommerce: {
            checkout_option: { actionField: extraInfo },
          },
        });
        break;
      case 'Pageview_Criteo':
        window.dataLayer.push({
          event: action,
          email: extraInfo,
        });
        break;
      case 'ProductPage_Criteo':
        window.dataLayer.push({
          event: action,
          PageType: 'ProductPage',
          ProductID: extraInfo,
        });
        break;
      case 'ListingPage_Criteo':
        window.dataLayer.push({
          event: action,
          PageType: 'ListingPage',
          ProductIDList: extraInfo,
        });
        break;
      case 'BasketPage_Criteo':
        window.dataLayer.push({
          event: action,
          PageType: 'BasketPage',
          ProductBasketProducts: extraInfo,
        });
        break;
      case 'TransactionPage_Criteo':
        window.dataLayer.push({
          event: action,
          PageType: 'TransactionPage',
          ProductTransactionProducts: extraInfo.products,
          TransactionID: extraInfo.transactionId,
        });
        break;
      case 'eec.MyStore':
        window.dataLayer.push({
          event: 'eec.MyStore',
          ecommerce: {
            click: {
              actionField: { list },
              products: extraInfo,
            },
          },
        });
        break;
      default:
        return false;
    }
  }
};

export const trackWithFacebookPixel = (action, event, extraInfo = {}) => {
  if (process.env.ENABLE_GOOGLE_ANALYTICS && process.env.CLIENT && window.fbLoaded) {
    window.fbLoaded.promise.then(() => {
      fbq(action, event, extraInfo);
    });
  }
};
