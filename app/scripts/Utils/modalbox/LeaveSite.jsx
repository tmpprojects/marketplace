import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Link } from 'react-router-dom';

import { modalBoxActions } from '../../Actions';

class LeaveSite extends React.Component {
  render() {
    return (
      <div className="modal_leaveSite">
        <p className="header">Espera</p>
        <p className="header-subtitle">¡No te vayas!</p>
        <p className="text">
          Olvidas algo en tu carrito. <br />
          Obtén el envío gratis de tu compra usando el código: <br />
          <span>DONTGO</span>
        </p>
        <Link to="/cart" className="c2a_square">
          Ir a mi carrito
        </Link>
        <button
          className="button-simple"
          type="button"
          onClick={this.props.closeModalBox}
        >
          No gracias.
        </button>
      </div>
    );
  }
}

// Map Redux Props and Actions to component
function mapStateToProps(state) {
  const { modalBox } = state;
  return {
    modalBox,
  };
}
function mapDispatchToProps(dispatch) {
  const { close: closeModalBox } = modalBoxActions;
  return bindActionCreators(
    {
      closeModalBox,
    },
    dispatch,
  );
}

// Export Component
export default connect(mapStateToProps, mapDispatchToProps)(LeaveSite);
