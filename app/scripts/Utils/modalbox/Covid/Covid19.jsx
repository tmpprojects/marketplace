import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { modalBoxActions } from '../../../Actions';
import Covid19ModalBox from './Covid19ModalBox';
// import './covid.scss';

export class Covid19 extends React.Component {
  constructor(props) {
    super(props);

    // Bind Scope to class methods
    this.openAssistentWindow = this.openAssistentWindow.bind(this);
  }

  openAssistentWindow() {
    this.props.openModalBox(() => <Covid19ModalBox />);
  }
  render() {
    return (
      <div className="cr__covid">
        <a>
          <ul className="cr__covid-list">
            <li className="cr__covid-img-lefthand">
              <img src={require('./hand_left.svg')} alt="Covid" />
            </li>
            <li className="cr__covid-img-alert">
              <img src={require('./icon_alert.svg')} alt="Covid" />
            </li>
            <li className="cr__covid-textContainer">
              {/* <h5 className='cr__covid-textContainer-title'>
              
            </h5>{' '} */}
              <p className="cr__covid-textContainer-subtitle">
                Debido a la contingencia actual, nuestras entregas podrían presentar
                una demora fuera de lo habitual. Agradecemos tu comprensión.
              </p>
              <h5 className="cr__covid-textContainer-mobile">
                Debido a la contingencia actual, nuestras entregas podrían presentar
                una demora fuera de lo habitual.
              </h5>
            </li>
            <li className="cr__covid-img-righthand">
              <img src={require('./hand_right.svg')} alt="Covid" />
            </li>
          </ul>
        </a>
      </div>
    );
  }
}
function mapStateToProps() {
  return {};
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      openModalBox: modalBoxActions.open,
      closeModalBox: modalBoxActions.close,
    },
    dispatch,
  );
}

// Export Component
Covid19 = connect(mapStateToProps, mapDispatchToProps)(Covid19);
export default Covid19;
