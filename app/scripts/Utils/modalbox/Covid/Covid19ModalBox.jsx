import React, { Component } from 'react';

export default class Covide19ModalBox extends Component {
  render() {
    return (
      <div className="cr__covidModalBox">
        <p className="cr__covidModalBox-text">
          Debido al estado actual de contingencia, es probable que nuestro servicio
          no opere de manera habitual. Queremos resolver posibles dudas de nuestros
          compradores y vendedores.
        </p>
        <p className="cr__covidModalBox-text">
          &#9679; Para no alentar la curva de contagio de COVID-19, Canasta Rosa
          pide, tanto a compradores, vendedores, y todos sus mensajeros, mantener
          distancia y no realizar ninguna entrega mano a mano.
        </p>
        <p className="cr__covidModalBox-text">
          &#9679; Debido a nuestra alta demanda, es probable que algunas entregas
          sufran algunos retrasos o se realicen en horarios posteriores a los
          solicitados.
        </p>
        <p className="cr__covidModalBox-text">
          &#9679; Recomendamos altamente que pidas tus productos para eventos
          importantes con un día de anticipación. Estos siempre pueden ser guardados
          en refrigeración.
        </p>
        <p className="cr__covidModalBox-text">
          &#9679; Si tienes un pedido programado y el status de tu orden aparece
          como: “Sin status” o “En tránsito”, no te preocupes, tu pedido está siendo
          atendido y programado para el día que solicitaste su entrega.
        </p>
        <p className="cr__covidModalBox-text">
          &#9679; Es probable que experimentes un tiempo de respuesta inusual con
          nuestro servicio al cliente, pero te responderemos apenas sea posible. Si
          nos escribiste por chat, te contactaremos por el mismo medio, pero por
          favor, no nos contactes por múltiples vías.
        </p>
        <p className="cr__covidModalBox-text">
          &#9679; Agradecemos tu comprensión ante cualquier inconveniente y recuerda
          lavarte las manos.
        </p>
      </div>
    );
  }
}
