import React from 'react';

import { ResponsiveImage } from '../ImageComponents';

//
export const ShippingCdmx = () => (
  <div className="modalShippingCdmx">
    <div className="container_img">
      <ResponsiveImage src={require('../../../images/store/modal_photo2.jpg')} />
    </div>

    <div className="container_info">
      <h3>Recibe amor en tu puerta</h3>
      <p className="subtitle">Las mejores creaciones merecen la mejor atención.</p>
      <p>
        {' '}
        Hoy sólo contamos con entregas dentro de la <br />{' '}
        <span className="underline">Cuidad de México</span>.
      </p>
      <p className="note">
        {' '}
        Espera más ciudades pronto y recibe los mejores productos.
      </p>
    </div>
  </div>
);
