import React from 'react';
import { Link } from 'react-router-dom';

const FailBuy = ({
  title = 'Ups',
  link = null,
  linkTitle = 'Continuar',
  content = null,
  onClick: clickHandler = null,
  ...props
}) => (
  <div className="modalFailBuy">
    <h4 />
    <h5>{title}</h5>
    <div dangerouslySetInnerHTML={{ __html: content }} />
    <br />
    <p>
      ¿Necesitas ayuda para completar tu compra? <br />
      <strong>
        <a href="/soporte" target="_blank">
          haz click aquí.
        </a>
      </strong>
    </p>
    <br />
    {link !== null && (
      <Link to={link} className="modalBox__link" onClick={clickHandler}>
        {linkTitle}
      </Link>
    )}
  </div>
);
export default FailBuy;
