import React from 'react';
import { withRouter } from 'react-router-dom';
import { matchPath } from 'react-router';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { reduxForm, Field } from 'redux-form';
import axios from 'axios';

import config from '../../../config';
import { IconPreloader } from '../Preloaders';
import { ActionButton } from '../ActionButton';
import { ResponsiveImage, ResponsiveImageFromURL } from '../ImageComponents';
import { formatNumberToPrice } from '../normalizePrice';
import ProductGallery from '../../components/Product/ProductGallery';
import { getProductDetail, getStoreDetail } from '../../Reducers';
import { storeActions } from '../../Actions';
import CollapseBox from '../CollapseBox';
import { required } from '../forms/formValidators';
import { normalizePhone } from '../forms/formNormalizers';
import { CANASTAROSA_PRO_SLUG } from '../../Constants/config.constants';

// Helper Constants
const VIEW_STATE = {
  DESCRIPTION: 'DESCRIPTION',
  FORM: 'FORM',
  SUCCESS: 'SUCCESS',
  ERROR: 'ERROR',
};

/*----------------------------------------------------
                ServicesModalBox CLASS
----------------------------------------------------*/
class ServicesModalBox extends React.Component {
  constructor(props) {
    super(props);

    // Get 'react-router' match properties
    // with the 'matchPath' helper function.
    let loc = '/stores/:store/services/:slug';
    if (RegExp('^/pro').test(this.props.history.location.pathname)) {
      loc = '/pro/servicios/:slug';
    }
    this.match = matchPath(this.props.history.location.pathname, {
      path: loc,
      exact: true,
      strict: false,
    });

    // Define initial state.
    this.state = {
      viewState: VIEW_STATE.DESCRIPTION,
      storeSections: [],
    };
  }
  componentWillMount() {
    // Reset redux Store.
    this.props.resetProductDetail();
  }
  componentDidMount() {
    // Fetch Product & Store data
    this.props.fetchStore(CANASTAROSA_PRO_SLUG);
    this.props.fetchProductDetail(CANASTAROSA_PRO_SLUG, this.match.params.slug);

    // Fetch Store Sections
    axios
      .get(
        `${config.frontend_host}/services/api/v1/market/stores/${CANASTAROSA_PRO_SLUG}/sections/`,
      )
      .then((response) => {
        this.setState({
          storeSections: response.data,
        });
      })
      .catch((error) => {
        console.log(error);
      });
  }

  /**
   * onSubmit()
   * Send form to the backend
   * @param {object} formValues | Form Values
   */
  onSubmit = (formValues) => {
    axios
      .post(`${config.frontend_host}/tools/cr-pro/service-request/`, {
        ...formValues,
        service: this.props.product.name,
      })
      .then((response) => {
        const isSuccess = response.data.status === 1;
        this.setState({
          viewState: isSuccess ? VIEW_STATE.SUCCESS : VIEW_STATE.ERROR,
        });
      })
      .catch((error) => {
        this.setState({
          viewState: VIEW_STATE.ERROR,
        });
      });
  };

  /**
   * closeContactForm()
   * Change state to 'description' view.
   */
  closeContactForm = () => {
    this.setState({
      viewState: VIEW_STATE.DESCRIPTION,
    });
  };

  /**
   * openContactForm()
   * Change state to 'form' view.
   */
  openContactForm = () => {
    this.setState({
      viewState: VIEW_STATE.FORM,
    });
  };

  /**
   * renderDescription()
   * Render 'Description' view.
   */
  renderDescription = () => {
    const { product } = this.props;
    const section = this.state.storeSections.find((s) => s.id === product.section);
    const header = <h5>Descripción</h5>;
    const content = <p>{product.description}</p>;
    return (
      <div className="service__box-content">
        <div className="image">
          <div className="image__thumbnail">
            <ResponsiveImage
              src={product?.photos?.find((p) => p.order === 0)?.photo}
              alt={product.name}
            />
          </div>
        </div>
        <div className="detail">
          <div className="detail__header">
            <h4>{product.name}</h4>
            {section && <p>{section.name}</p>}

            <div className="detail__header--c2a">
              {product?.price > 2 && (
                <div>
                  <h4>{product?.price}</h4>
                  {/*<p className="delivery-time">Entrega:<span> 20 días</span></p>*/}
                </div>
              )}
              <div className="container">
                <button
                  className="c2a_square"
                  onClick={(e) => this.openContactForm()}
                >
                  {product.call_to_action_label !== ''
                    ? product.call_to_action_label
                    : 'Más información'}
                </button>
              </div>
            </div>
          </div>
          <div className="detail__description">
            <CollapseBox content={content} header={header} />

            <div className="description">
              {header}
              {content}
            </div>
          </div>
        </div>
      </div>
    );
  };

  /**
   * renderForm()
   * Render 'Form' view.
   */
  renderForm = () => {
    const { handleSubmit } = this.props;
    return (
      <div className="contact_form">
        <form className="form" onSubmit={handleSubmit(this.onSubmit)}>
          <h5>Envíanos tus datos y nosotros en breve te contáctaremos </h5>
          <fieldset>
            <label htmlFor="name">Nombre *</label>
            <Field component="input" type="text" name="name" id="name" />
          </fieldset>
          <div className="half_field">
            <fieldset>
              <label htmlFor="email">Email *</label>
              <Field component="input" type="text" name="email" id="email" />
            </fieldset>
            <fieldset>
              <label htmlFor="phone">Celular *</label>
              <Field component="input" type="text" name="phone" id="phone" />
            </fieldset>
          </div>
          <fieldset>
            <label htmlFor="store">Tienda/Marca</label>
            <Field component="input" type="text" name="store" id="store" />
          </fieldset>
          <div className="container_c2a">
            <button
              type="button"
              className="modal__button-cancel"
              onClick={(e) => this.closeContactForm()}
            >
              Cancelar
            </button>
            <button className="c2a_square">Enviar</button>
          </div>
        </form>
      </div>
    );
  };

  /**
   * renderError()
   * Render 'Submit Form Error' view.
   */
  renderError = () => (
    <div className="window_state--error">
      <h3>¡Opps!</h3>
      <p>Algo salio mal, intentalo nuevamente</p>
    </div>
  );

  /**
   * renderSuccess()
   * Render 'Submit Success Error' view.
   */
  renderSuccess = () => (
    <div className="window_state--success">
      <h3>¡Hemos recibido tu correo!</h3>
      <p>En breve nos prodremos en contacto contigo</p>
    </div>
  );

  /**
   * render()
   */
  render() {
    // Extract properties and state
    const { storeData: store, product } = this.props;
    const { viewState } = this.state;
    if (product.loading || store.loading) return null;

    // Define the view to render
    let view;
    switch (viewState) {
      case VIEW_STATE.FORM:
        view = this.renderForm();
        break;
      case VIEW_STATE.SUCCESS:
        view = this.renderSuccess();
        break;
      case VIEW_STATE.ERROR:
        view = this.renderError();
        break;
      case VIEW_STATE.DESCRIPTION:
      default:
        view = this.renderDescription();
        break;
    }

    // Render Markup
    return <div className="service__box">{view}</div>;
  }
}

// Wrap component within reduxForm
ServicesModalBox = reduxForm({
  form: 'canastarosaProServices_form',
  destroyOnUnmount: true,
})(ServicesModalBox);

// Map Redux Props and Actions to component
function mapStateToProps(state) {
  const { store } = state;
  return {
    product: store.productDetail, //getProductDetail(state),
    storeData: getStoreDetail(state),
    storeSections: store.sections,
  };
}
function mapDispatchToProps(dispatch) {
  const {
    fetchStore,
    fetchProducts,
    fetchProductDetail,
    resetProductDetail,
  } = storeActions;
  return bindActionCreators(
    {
      fetchStore,
      fetchProducts,
      fetchProductDetail,
      resetProductDetail,
    },
    dispatch,
  );
}

// Export Component
ServicesModalBox = withRouter(ServicesModalBox);
export default connect(mapStateToProps, mapDispatchToProps)(ServicesModalBox);
