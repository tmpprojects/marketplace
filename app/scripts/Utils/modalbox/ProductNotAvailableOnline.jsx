import React from 'react';
import {
  ORDERS_EMAIL,
  ORDERS_PHONE,
  ORDERS_PHONE_PLAIN_FORMAT,
} from '../../Constants/config.constants';

export default ({ product, store }) => (
  <div className="modalDisableBuy">
    <h4>¿C&oacute;mo adquiero este producto?</h4>
    <p>
      Actualmente, este art&iacute;culo no est&aacute; disponible para compra en
      l&iacute;nea, pero puedes conseguirlo directamente con nosotros en
      <a
        className="link"
        href={`mailto:${ORDERS_EMAIL}?subject=Solicitud de Pedido para ${store}&body=Hola,\n\r me interesa comprar el artículo "${product}" de la tienda "${store}". Gracias.`}
      >
        {ORDERS_EMAIL}
      </a>
      .
    </p>
  </div>
);
