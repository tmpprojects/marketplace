import React, { Component } from 'react';
import {
  ORDERS_PHONE,
  BUTLER_PHONE_PLAIN_FORMAT,
} from '../../Constants/config.constants';

export class AssistantModalBox extends Component {
  render() {
    return null;

    /* Temporarily disable in February */
    return (
      <div className="assistant_modalbox">
        <div className="container_info">
          <h3>
            ¡Hola! Yo soy tu <br /> <span className="note">Mayordomo Rosa</span>
          </h3>
          {/* <p className="subtitle">
                        ¡Es la persona que solucionará cualquier necesidad que tengas!
                    </p> */}
          <p>
            Mi misión es salvarte la vida y ahorrarte tiempo.
            <br />
            Me dedico a buscar cualquier cosa que necesites; como pasteles de último
            momento, regalos únicos, detalles para una fiesta, pedidos especiales y
            más. Pídeme lo que quieras, incluso productos o servicios que no
            encuentres en Canasta Rosa.
          </p>
          <p className="contact">Contacta al Mayordomo Rosa</p>
          <div>
            <a
              className="c2a_round contact_assistant phone"
              href={`tel:+52-${ORDERS_PHONE}`}
            >
              {' '}
              Llámame
            </a>
            {/*
                        Temporarily disable in February  
                        <a 
                            className="c2a_round contact_assistant whatsapp" 
                            href={`https://api.whatsapp.com/send?phone=${BUTLER_PHONE_PLAIN_FORMAT}`}
                            target="_blank"
                            rel="noopener noreferrer"
                        > Escríbeme</a>
                        */}
          </div>
          <span className="contact_text">O llámame al {ORDERS_PHONE}</span>
        </div>
      </div>
    );
  }
}
