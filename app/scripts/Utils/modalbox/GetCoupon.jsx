import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Link } from 'react-router-dom';
import { Field, reduxForm, SubmissionError } from 'redux-form';

import { required, email } from '../../Utils/forms/formValidators';
import { normalizeToLowerCaseAndTrim } from '../../Utils/forms/formNormalizers';
import { modalBoxActions, appActions } from '../../Actions';

/*
 * Form Validation Function
 * @param {object} values : Form values
 */
const validate = (values) => {
  const errors = {};

  // Email
  if (!values.email) {
    errors.email = 'Escribe tu email';
  } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) {
    errors.email = 'Escribe un email válido.';
  }

  // Return errors
  return errors;
};

/*
 * Form Warnings Function
 * @param {object} values : Form values
 */
const warn = (values) => {
  const warnings = {};
  return warnings;
};

/*
 * Redner Custom Form Fields
 * @param {object} props : Field Properties
 */
const renderField = ({
  input,
  type,
  autoFocus,
  placeholder,
  fieldName,
  meta: { touched, error, warning },
}) => (
  <div className={`form__data form__data-${fieldName}`}>
    <input {...input} placeholder={placeholder} type={type} autoFocus={autoFocus} />
    {touched &&
      ((error && <div className="form_status danger">{error}</div>) ||
        (warning && <div className="form_status warning">{warning}</div>))}
  </div>
);

export class GetCoupon extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      success: false,
    };

    // Bind scope to methods
    this.formSubmit = this.formSubmit.bind(this);
  }

  formSubmit(formData) {
    return this.props
      .coupon(formData)
      .then((response) => {
        this.setState({
          success: true,
        });
      })
      .catch((error) => {
        throw new SubmissionError({
          _error: 'Correo no elegible para este cupón.',
        });
      });
  }

  render() {
    return null;
    const { handleSubmit, error } = this.props;
    const { success } = this.state;

    return !success ? (
      <div className="modal_getCoupon">
        <h5>Recibe amor en tu puerta</h5>
        <p>Ingresa tu email y obtén el</p>
        <p className="discount">
          $50 <span className="currency">MXN</span>
          <br />
          <span className="text">de descuento</span>
        </p>
        <p>en tu siguiente compra.</p>
        <form className="form" onSubmit={handleSubmit(this.formSubmit)}>
          <fieldset>
            <Field
              autoFocus
              name="email"
              validate={[required, email]}
              normalize={normalizeToLowerCaseAndTrim}
              component={renderField}
              type="text"
              placeholder="Ingresa tu email"
              id="email"
            />
            {error && (
              <div className="form__data">
                <div
                  className="form_status danger"
                  style={{
                    padding: '.75em',
                    background: 'rgb(250, 247, 245, 0.75)',
                  }}
                >
                  {error}
                </div>
              </div>
            )}
          </fieldset>
          <button className="c2a_square" type="submit">
            Obtener mi código
          </button>
        </form>
        <button
          className="button-simple"
          type="submit"
          onClick={this.props.closeModalBox}
        >
          No gracias
        </button>
        {/*<span className="note">No acumulable con otras promociones.</span>*/}
      </div>
    ) : (
      <div className="modal_getCoupon--message">
        <div className="accountConfirm">
          <h2>¡Hola!</h2>
          <p className="message">
            Revisa tu <span>email.</span> <br />
            Te hemos enviado tu <span>código de descuento</span>, aplícalo en tu{' '}
            <span>siguiente compra</span>.
          </p>
          <a className="c2a_square" onClick={this.props.closeModalBox}>
            Ok
          </a>
        </div>
      </div>
    );
  }
}

GetCoupon = reduxForm({
  form: 'coupon',
  validate,
  warn,
})(GetCoupon);

// Map Redux Props and Actions to component
function mapStateToProps(state) {
  return {};
}

function mapDispatchToProps(dispatch) {
  const { close: closeModalBox } = modalBoxActions;
  const { coupon } = appActions;
  return bindActionCreators(
    {
      coupon,
      closeModalBox,
    },
    dispatch,
  );
}

// Export Component
export default connect(mapStateToProps, mapDispatchToProps)(GetCoupon);
