import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { modalBoxActions } from '../../../Actions';

export class ImportantMessage extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className="cr__covid">
        <a>
          <ul className="cr__covid-list" style={{}}>
            <li className="cr__covid-img-lefthand">
              <img
                src={
                  'https://canastarosa.s3.us-east-2.amazonaws.com/media/messages/half1.png'
                }
                alt="Covid"
              />
            </li>
            <li className="cr__covid-img-alert">
              <img
                src={
                  'https://canastarosa.s3.us-east-2.amazonaws.com/media/messages/icon_alert.svg'
                }
                alt="Covid"
              />
            </li>
            <li className="cr__covid-textContainer">
              {/* <h5 className='cr__covid-textContainer-title'>
              
            </h5>{' '} */}
              <p className="cr__covid-textContainer-subtitle">
                Todavía estas a tiempo de sorprender a los que más quieres, <br />{' '}
                realiza tus compras con{' '}
                <span style={{ fontWeight: '600' }}>
                  envío express y recibe antes de Navidad.
                </span>
              </p>
              <div
                className="cr__covid-textContainer-mobile"
                style={{ width: '110%', marginLeft: '-2em' }}
              >
                Todavía estas a tiempo de sorprender a los que más quieres, realiza
                tus compras con{' '}
                <span style={{ fontWeight: '600' }}>
                  envío express y recibe antes de Navidad.
                </span>
              </div>
            </li>
            <li className="cr__covid-img-righthand">
              <img
                src={
                  'https://canastarosa.s3.us-east-2.amazonaws.com/media/messages/half2.png'
                }
                alt="Covid"
              />
            </li>
          </ul>
        </a>
      </div>
    );
  }
}
function mapStateToProps() {
  return {};
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      openModalBox: modalBoxActions.open,
      closeModalBox: modalBoxActions.close,
    },
    dispatch,
  );
}

// Export Component
ImportantMessage = connect(mapStateToProps, mapDispatchToProps)(ImportantMessage);
export default ImportantMessage;
