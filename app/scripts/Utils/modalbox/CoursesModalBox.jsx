import React from 'react';
import { connect } from 'react-redux';

import CollapseBox from '../CollapseBox';

/*----------------------------------------------------
                CoursesModalBox CLASS
----------------------------------------------------*/
class CoursesModalBox extends React.Component {
  /**
   * renderDescription()
   * Render 'Description' view.
   */
  renderDescription = () => {
    const { product } = this.props;
    const header = <h5>Descripción</h5>;
    const content = (
      <p
        dangerouslySetInnerHTML={{
          __html: product.content.rendered,
        }}
      />
    );

    return (
      <div className="service__box-content">
        <div className="image">
          <div className="image__thumbnail">
            {product._embedded['wp:featuredmedia'] && (
              <img
                src={product._embedded['wp:featuredmedia'][0].source_url}
                alt={product.title.rendered}
              />
            )}
          </div>
        </div>
        <div className="detail">
          <div className="detail__header">
            <h4>{product.title.rendered}</h4>
            <p>Cursos y Talleres</p>
            <div className="container-c2a">
              {product.acf.public_document && (
                <a
                  href={product.acf.public_document}
                  className="c2a_square"
                  target="_blank"
                  rel="noopener noreferrer"
                >
                  Descargar PDF
                </a>
              )}
            </div>
          </div>
          <div className="detail__description">
            <CollapseBox content={content} header={header} />

            <div className="description">
              {header}
              {content}
            </div>
          </div>
        </div>
      </div>
    );
  };

  /**
   * render()
   */
  render() {
    return <div className="service__box">{this.renderDescription()}</div>;
  }
}
// Map Redux Props and Actions to component
function mapStateToProps(state) {
  return {};
}
function mapDispatchToProps(dispatch) {
  return {};
}

// Export Component
export default connect(mapStateToProps, mapDispatchToProps)(CoursesModalBox);
