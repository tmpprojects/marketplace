import axios from 'axios';
import config from '../../config';
import getCookie from '../Utils/getCookie';

// Create Custom instance of AXIOS for browser
export default axios.create({
  baseURL: config.proxyPath,
  headers: {
    'X-CSRFToken': getCookie('csrftoken'),
  },
});
