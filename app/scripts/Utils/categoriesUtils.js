/**
 * pickChildrenCategories()
 * @param {string} categorySlug| A Category Slug taken from url param
 * @param {array} currentNode| Current Node of a tree data structure.
 * @returns {array or empty array} Category Object
 */
export const pickChildrenCategories = (categorySlug, currentNode) => {
  let currentChild, result;

  for (let i = 0; i < currentNode.length; i++) {
    if (categorySlug === currentNode[i].slug) {
      return currentNode[i].children;
    }
    currentChild = currentNode[i].children;

    // Search in the current child
    result = pickChildrenCategories(categorySlug, currentChild);

    // Return the result if the node has been found
    if (result.length > 0) {
      return result;
    }
  }

  result = [];
  // The node has not been found and we have no more options
  return result;
};
