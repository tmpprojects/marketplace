export function cloudFront(src) {
  // http://d1jr9u46s1u46u.cloudfront.net/media/__sized__/featured/market/banner/da8443aa29a3828896b1ce01e4d1f45a-crop-c0-5__0-5-960x640.png
  if (src !== null && src !== '') {
    const cloudFlare = 'd1jr9u46s1u46u.cloudfront.net';
    const baseDomain = 'canastarosa.s3.amazonaws.com';
    const CDNImage = src.replace(baseDomain, cloudFlare);
    return CDNImage;
  }
  return null;
}
