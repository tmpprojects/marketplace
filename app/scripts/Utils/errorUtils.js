import { APPLICATION_ENVIRONMENTS } from '../Constants/config.constants';

const ENVIRONMENT =
  process.env.NODE_ENV === APPLICATION_ENVIRONMENTS.PRODUCTION
    ? APPLICATION_ENVIRONMENTS.PRODUCTION
    : APPLICATION_ENVIRONMENTS.DEVELOPMENT;

export const getUIErrorMessage = (error, errorTypes) => {
  let errorObject = null;
  if (error) {
    const errors = Object.keys(error.data).map((e) => e);
    const type = Object.keys(errorTypes).find(
      (errType) => errorTypes[errType].label === errors[0],
    );

    if (type) {
      errorObject = errorTypes[type];
    } else {
      errorObject = {
        ui_message: 'Se produjo un error. Por favor intenta de nuevo más tarde.',
      };
    }
  }
  //Raven.captureException(error, { extra: error });
  return errorObject.ui_message;
};

export const getErrorInPayment = (errorInfoArray, productsArray, errorMessages) => {
  let error;
  let storeName;
  let productName;
  let productPointer = 0;
  let defaultMessage = errorMessages['default_message'];
  try {
    if (errorInfoArray.length > 0 && productsArray.length > 0) {
      for (let i = 0; i < errorInfoArray.length; i++) {
        error = errorInfoArray[i];
        storeName = productsArray[i].product.store.name;
        productName = productsArray[i].product.name;
        //store is on vacation
        if ('physical_properties' in error) {
          if ('shipping_date' in error.physical_properties) {
            const {
              physical_properties: { shipping_date },
            } = error;
            if (shipping_date.includes('vacation_range')) {
              let details = errorMessages['vacations'].details.replace(
                '::storeName::',
                storeName,
              );
              details = details.replace('::productName::', productName);
              return {
                title: errorMessages['vacations'].title,
                details,
              };
            }
          }
        }
        // when product is unpublished
        if ('products' in error && 'store' in error) {
          if (error.store[0].includes('no existe')) {
            let details = errorMessages['unpublished'].details.replace(
              '::productName::',
              productName,
            );
            details = details.replace(
              '::productAttributes::',
              productsArray[i]?.attribute?.tree_string
                ? productsArray[i]?.attribute?.tree_string
                : '',
            );
            details = details.replace('::storeName::', storeName);

            return {
              title: errorMessages['unpublished'].title,
              details,
            };
          }
        }
        //product out of stock
        if ('products' in error && !('store' in error)) {
          for (let j = 0; j < error.products.length; j++) {
            if ('product' in error.products[j]) {
              if (error.products[j].product.includes('out_of_stock')) {
                productPointer = i + j;
                let details = errorMessages['out_of_stock'].details.replace(
                  '::productName::',
                  productsArray[productPointer].product.name,
                );
                details = details.replace(
                  '::productAttributes::',
                  productsArray[productPointer]?.attribute?.tree_string
                    ? productsArray[productPointer]?.attribute?.tree_string
                    : '',
                );

                return {
                  title: errorMessages['out_of_stock'].title,
                  details,
                };
              }
            }
          }
        }
        if ('store' in error && !('products' in error)) {
          if (error.store[0].includes('no existe')) {
            let regexExpression = /=(.*?)\s/;
            let storeSlug = error.store[0].split(regexExpression)[1];
            let foundStore = productsArray.find((product) => {
              return product.product.store.slug === storeSlug;
            });

            let details = errorMessages['store_is_closed'].details.replace(
              '::storeName::',
              foundStore.product.store.name,
            );

            return {
              title: errorMessages['store_is_closed'].title,
              details,
            };
          }
        }
      }
      //any other case
      return defaultMessage;
    }
  } catch (error) {
    console.log(error);
    return defaultMessage;
  }
};
export const getError = (errorList, errorTypes) => {
  let error = null;
  if (errorList) {
    const errorKeys = Object.keys(errorList).map((k) => k);
    const errorType = Object.keys(errorTypes).find(
      (k) => errorTypes[k].label === errorKeys[0],
    );

    if (errorType) {
      error = errorTypes[errorType];
    } else {
      error = {
        ui_message: 'Se produjo un error. Por favor intenta de nuevo más tarde.',
      };
    }
  }
  // Raven.captureException(error, {
  //     extra: error
  // });

  return error;
};
export const getErrors = (errorList, errorTypes) => {
  let errors = [];
  if (errorList) {
    const errorKeys = Object.keys(errorList).map((k) => k);
    errors = errorKeys.reduce((acc, curr) => {
      const foundErrType = Object.keys(errorTypes).find(
        (k) => errorTypes[k].label === curr,
      );
      if (foundErrType) {
        return [
          ...acc,
          {
            ...errorTypes[foundErrType],
            type: foundErrType,
          },
        ];
      }
      return acc;
    }, errors);
  }
  // Raven.captureException(errors, {
  //     extra: errors
  // });

  return errors;
};

/*-----------------------------------------------
// SENTRY CONFIGURATION
-----------------------------------------------*/
export const initRavenInstance = () => {
  let SentryInstance;
  const config = {
    dsn:
      ENVIRONMENT === APPLICATION_ENVIRONMENTS.PRODUCTION
        ? 'https://2411a6c639cd4ecd8fbb4e9d42979490@sentry.io/1187373'
        : '',
    debug: ENVIRONMENT === APPLICATION_ENVIRONMENTS.DEVELOPMENT,
    environment: ENVIRONMENT,
    release: `canastarosa-frontend@sha:${process.env.VERSION}`,
  };

  // Switch between Sentry SDKs
  if (process.env.CLIENT) {
    SentryInstance = require('@sentry/browser');
    SentryInstance.init(config);
  } else {
    SentryInstance = require('@sentry/node');
    SentryInstance.init({
      ...config,
      integrations: [new SentryInstance.Integrations.RewriteFrames()],
    });
  }
  return SentryInstance;
};
export const getRavenMiddleware = (Sentry) => (store) => (next) => (action) => {
  const reduxStore = store.getState();
  Sentry.configureScope((scope) => {
    //scope.setTag('user_mode', 'admin');
    scope.setTag('app_environment', process.env.CLIENT ? 'browser' : 'node');
    scope.setUser({ email: reduxStore.users.profile.email });
    scope.addEventProcessor(async (event, hint) => ({
      ...event,
      extra: {
        ...event.extra,
        'redux-store': {
          user: reduxStore.users.profile,
          cart: reduxStore.cart.products,
          myStore: reduxStore.myStore.data,
        },
        'redux-lastAction': action,
      },
    }));
  });

  // Add Redux Action as a breadcrumb on Sentry logs.
  Sentry.addBreadcrumb({
    category: 'redux-action',
    message: action.type,
  });

  // Return action to redux flow
  return next(action);
};
