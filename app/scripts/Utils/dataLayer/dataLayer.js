function dataLayer() {
    globalThis.googleAnalytics = {
        productImpression: (dataLayerContent = [], dataLayerBreadcrumbs = null) => {
            if (process.env.CLIENT) {
                //CategoryPage.jsx, CardsTrending.jsx, InterestPage.jsx
                try {
                    let checkEventLabel = globalThis.googleAnalytics.utils.clearLayerText(dataLayerBreadcrumbs) ? globalThis.googleAnalytics.utils.clearLayerText(dataLayerBreadcrumbs) : null;
                    window.dataLayer.push({
                        "event": "ProductImpression",
                        "eventCategory": "Ecommerce",
                        "eventAction": "Impresion de productos",
                        "eventLabel": checkEventLabel,
                        "ecommerce": {
                            "currencyCode": "MXN",
                            "impressions": dataLayerContent
                        }
                    });
                    globalThis.googleAnalytics.currentImpression = {
                        event: "ProductImpression",
                        dataLayerBreadcrumbs,
                        dataLayerContent
                    }
                } catch (e) {
                    console.log("ERROR: productImpression --->", e)
                }

            }
        },
        productClick: (idProduct = 0) => {


            let currentImpression = globalThis.googleAnalytics.currentImpression.dataLayerContent;
            let found = false;
            console.log("currentImpression  --> ", currentImpression)
            currentImpression.map((item, index) => {

                if (Number(item.id) == Number(idProduct)) {
                    found = item;
                    // console.log("Found: ", item)
                } else {
                     //  console.log("Not Found: ", item)
                }
            })

            if (process.env.CLIENT && found) {
                //CategoryPage.jsx, CardsTrending.jsx, InterestPage.jsx
                try {
                    let checkEventLabel = globalThis.googleAnalytics.currentImpression.dataLayerBreadcrumbs ? globalThis.googleAnalytics.currentImpression.dataLayerBreadcrumbs : null
                    window.dataLayer.push({
                        "event": "ProductClick",
                        "eventCategory": "Ecommerce",
                        "eventAction": "Click en producto",
                        "eventLabel": checkEventLabel,
                        "ecommerce": {
                            "click": {
                                "actionField": { 'list': checkEventLabel },      // Variable con el nombre de la categoria principal, Colección o Tienda
                                "products": [{

                                    "name": found?.name,       // Variable con nombre visible del producto
                                    "id": found?.id,  //  Variable con SKU
                                    "price": found?.price,  //  Variable con precio visible del producto en el listado
                                    "brand": found?.store?.name ? found?.store?.name : found?.brand,  //  Variable tienda comercializa el producto
                                    "category": checkEventLabel,  //  Variable cn nombre de la Categoria, Colección, Tienda o Intereses.
                                    "variant": null,  // Variable Color, tamaño, presentación para los productos que apliquen
                                    "list": "Colecciones", // Lista de producto - Categorías, Colecciones, Tiendas, Intereses
                                    "position": found?.position // Variable Posición del producto en la lista de izquierda a derecha arriba hacia abajo
                                }]


                            }
                        }
                    });
                    globalThis.googleAnalytics.currentProductClick = {
                        event: "ProductClick",
                        dataLayerBreadcrumbs: checkEventLabel,
                        dataLayerContent: found
                    }
                } catch (e) {
                    console.log("ERROR: productClick --->", e)
                }

            }
        },
        ProductDetail: (dataLayerContent = [], dataLayerBreadcrumbs = null) => {
            if (process.env.CLIENT) {
                //CategoryPage.jsx, CardsTrending.jsx, InterestPage.jsx
                try {
                    let checkEventLabel = globalThis.googleAnalytics.utils.clearLayerText(dataLayerBreadcrumbs) ? globalThis.googleAnalytics.utils.clearLayerText(dataLayerBreadcrumbs) : null;
                    window.dataLayer.push({
                        "event": "ProductDetail",
                        "eventCategory": "Ecommerce",
                        "eventAction": "Detalle de producto",
                        "eventLabel": checkEventLabel,
                        "ecommerce": {
                            "detail": {
                                "actionField": {
                                    "list": checkEventLabel,
                                },
                            },
                        },
                        "products": [dataLayerContent],
                    });
                    globalThis.googleAnalytics.currentImpression = {
                        event: "ProductImpression",
                        dataLayerBreadcrumbs,
                        dataLayerContent
                    }
                } catch (e) {
                    console.log("ERROR: ProductDetail --->", e)
                }

            }
        },
        promoView: (dataLayerContent = [], category = null) => {
            if (process.env.CLIENT) {
                //CategoryPage.jsx, CardsTrending.jsx, InterestPage.jsx
                try {
                    window.dataLayer.push({
                        "event": "promoView",
                        "eventCategory": "Ecommerce",
                        "eventAction": "Promoción",
                        "eventLabel": category,
                        "ecommerce": {
                            "promoView": [
                                // set dataLayerContent
                                {
                                    'id': 'BuenFin_2020',    // Variable con el ID asignado a la promoción       
                                    'name': 'Buen Fin 2020',  // Variable con el nombre visible de la promoción
                                    'creative': 'banner1', //Variable con el nombre del creativo que se imprime en la promoción
                                    'position': 'slot1' // Variable Posición del producto en la lista de izquierda a derecha arriba hacia abaj
                                }
                            ]
                        }
                    });

                } catch (e) {
                    console.log("ERROR: productImpression --->", e)
                }

            }
        },
        checkoutStep1: (dataLayerContent = []) => {
            if (process.env.CLIENT) {
                //CategoryPage.jsx, CardsTrending.jsx, InterestPage.jsx
                try {
                    window.dataLayer.push({
                        "event": "checkout",
                        "eventCategory": "Ecommerce",
                        "eventAction": "Checkout",
                        "eventLabel": "Step 1", // Variable con el paso de la tramitación de la compra
                        "ecommerce": {
                            "checkout": {
                                "actionField": { "step": 1, "option": "Envío" },
                                "products": [dataLayerContent],
                            },
                        },
                        "eventCallback": function () {
                            document.location = "shipping-payment";

                        },

                    });
                } catch (e) {
                    console.log("ERROR: checkoutStep1 --->", e)
                }
            }
        },
        checkoutStep2: (option = '') => {
            if (process.env.CLIENT) {
                //CategoryPage.jsx, CardsTrending.jsx, InterestPage.jsx
                try {
                    window.dataLayer.push({
                        "event": "checkoutOption",
                        "eventAction": "Checkout",
                        "eventLabel": "Step 2",
                        "ecommerce": {
                            "checkout_option": {
                                "actionField": { "step": 2, "option": option }
                            }
                        }
                    });


                } catch (e) {
                    console.log("ERROR: checkoutStep2 --->", e)
                }
            }
        },
        purchase: (dataLayerContent = [], actionField = {}) => {
            if (process.env.CLIENT) {
                //CategoryPage.jsx, CardsTrending.jsx, InterestPage.jsx
                try {
                    window.dataLayer.push({
                        "event": "Purchase",
                        "eventCategory": "Ecommerce",
                        "eventAction": "Transacción",
                        "eventLabel": actionField?.id,
                        "ecommerce": {
                            "purchase": {
                                "actionField": actionField,
                                "products": [dataLayerContent],
                            }
                        }
                    });

                } catch (e) {
                    console.log("ERROR: purchase --->", e)
                }
            }
        },
        promotionClick: (dataLayerContent = [], category = null) => {
            if (process.env.CLIENT) {
                //CategoryPage.jsx, CardsTrending.jsx, InterestPage.jsx
                try {
                    window.dataLayer.push({
                        "event": "promotionClick",
                        "eventCategory": "Ecommerce",
                        "eventAction": "Promoción",
                        "eventLabel": category,
                        "ecommerce": {
                            "promoClick": [
                                // set dataLayerContent
                                {
                                    'id': 'BuenFin_2020',    // Variable con el ID asignado a la promoción
                                    'name': 'Buen Fin 2020',  // Variable con el nombre visible de la promoción
                                    'creative': 'banner1', //Variable con el nombre del creativo que se imprime en la promoción
                                    'position': 'slot1' // Variable Posición del producto en la lista de izquierda a derecha arriba hacia abaj
                                }
                            ]
                        }
                    });

                } catch (e) {
                    console.log("ERROR: promotionClick --->", e)
                }

            }
        },
        currentImpression: {},
        currentProductClick: {},
        utils: {
            clearLayerText: (text) => {
                try {
                    if (Boolean(text)) {
                        return text.replace(/\"/g, "")
                    }
                } catch (e) {
                    console.log("clearLayerText", e)
                }
                return "";
            },
            closeArray: (index, dataLayerResults) => {
                try {
                    if (Boolean(dataLayerResults)) {
                        if (dataLayerResults.length) {
                            return index < (dataLayerResults.length - 1) ? ',' : ']';
                        }


                    }
                } catch (e) {
                    console.log("closeArray", e)
                }
                return "";
            },
            checkBreadcrumbs: (breadcrumbsPath) => {
                try {
                    if (Boolean(breadcrumbsPath)) {
                        if (breadcrumbsPath.length) {
                            return breadcrumbsPath.length ? `${globalThis.googleAnalytics.utils.clearLayerText(breadcrumbsPath[breadcrumbsPath.length - 1].name)}` : null
                        }
                    }
                } catch (e) {
                    console.log("checkBreadcrumbs", e)
                }
                return null;
            }
        }
    }
}

// }${index < (dataLayerResults.length - 1) ? ',' : ']'}

export { dataLayer }