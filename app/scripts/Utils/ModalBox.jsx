import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import { modalBoxActions } from '../Actions';

class ModalBox extends Component {
  handleClick(e) {
    if (e.target === this.boxContainer) {
      e.preventDefault();
      this.props.dispatch(modalBoxActions.close());
      return;
    }
  }

  render() {
    const { window: Window } = this.props.modalBox;
    if (!this.props.modalBox.visible || Window === undefined) {
      return null;
    }

    return (
      <div
        className="modalBox"
        ref={(boxContainer) => {
          this.boxContainer = boxContainer;
        }}
        onClick={(e) => this.handleClick(e)}
      >
        <div className="modalBox__overlay" />
        <div className="modalBox__container">
          <Window />
        </div>
      </div>
    );
  }
}

// PropTypes Definition
ModalBox.propTypes = {
  window: PropTypes.object.isRequired,
};
ModalBox.defaultProps = {
  window: {},
};

// Add Redux state and actions to component´s props
function mapStateToProps({ modalBox }) {
  return { modalBox };
}

// Export Connected Component
export default connect(mapStateToProps)(ModalBox);
