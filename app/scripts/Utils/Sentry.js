const Sentry = process.env.CLIENT
  ? require('@sentry/browser')
  : require('@sentry/node');

export default Sentry;
