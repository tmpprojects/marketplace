import React from 'react';
import PropTypes from 'prop-types';

// Simple Animated Icon Preloader
export const IconPreloader = ({ className = '', ...props }) => (
  <div className={`preloader ${className}`} {...props} />
);

// TODO: Improve component.
// This is intended to show custom content/components as children.
export const CustomPreloader = (props) => <div className="">props.children</div>;

// TODO: This component is intended to be a percentage preloader
export const ProgressPreloader = (props) => <div className="preloader" />;
