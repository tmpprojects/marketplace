export const registerFacebookPixelEvent = (action, event) => {
  if (
    process.env.NODE_ENV === 'production' &&
    process.env.CLIENT &&
    window.fbLoaded
  ) {
    window.fbLoaded.promise.then(() => {
      fbq(action, event);
    });
  }
};
