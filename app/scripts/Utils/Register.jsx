import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Field, reduxForm, SubmissionError } from 'redux-form';

import { authenticationActions, userActions, modalBoxActions } from '../Actions';
import { normalizeToLowerCaseAndTrim } from './forms/formNormalizers';
import { userRegisterErrorTypes } from '../Constants/user.constants';
import { required, email, password } from './forms/formValidators';
import { getUIErrorMessage } from './errorUtils';
import config from '../../config';
import Login from './Login.jsx';

/*
 * Form Validation Function
 * @param {object} values : Form values
 */
const password_confirmation = (value, allValues) =>
  value && value !== allValues.password
    ? 'Las contraseñas nos coinciden.'
    : undefined;
const name_validation = (value) =>
  value && value.length < 3 ? 'Escribe tu nombre completo.' : undefined;
const lastName_validation = (value) =>
  value && value.length < 3 ? 'Escribe tus apellidos..' : undefined;

/*
 * Redner Custom Form Fields
 * @param {object} props : Field Properties
 */
const renderField = ({
  input,
  label,
  type,
  autoFocus,
  placeholder,
  fieldName,
  meta: { touched, error, warning },
}) => (
  <div className={`form__data form__data-${fieldName}`}>
    <input {...input} placeholder={placeholder} type={type} autoFocus={autoFocus} />
    {touched &&
      ((error && <div className="form_status danger">{error}</div>) ||
        (warning && <div className="form_status warning">{warning}</div>))}
  </div>
);

/*
 * React Register component
 */
class Register extends Component {
  constructor(props) {
    super(props);

    this.state = {
      success: false,
    };

    // Bind scope to methods
    this.formSubmit = this.formSubmit.bind(this);
    this.loginCallback = this.loginCallback.bind(this);
  }

  /*
   * FORM FUNCTIONS
   */
  formSubmit(formData) {
    // Register user
    return this.props
      .register(formData)
      .then((response) => {
        this.setState({
          success: true,
        });
      })
      .catch((error) => {
        const uiMessage = getUIErrorMessage(error.response, userRegisterErrorTypes);
        throw new SubmissionError({
          _error: uiMessage,
        });
      });
  }

  /*
   * FACEBOOK LOGIN CALLBACK
   * @param {object} response : Server Response from Facebook
   */
  loginCallback(response) {
    if (response.authResponse) {
      const accessToken = { access_token: response.authResponse.accessToken };
      this.props
        .socialLogin(accessToken, 'facebook')
        .then((res) => (window.location = config.login_redirect_url))
        .catch((error) => {});
    } else {
      console.log(
        'Facebook Login: User cancelled login or did not fully authorize.',
      );
    }
  }

  /*
   * React Component Life Cycle Functions
   */
  render() {
    const { success } = this.state;
    const { handleSubmit, submitting, error } = this.props;

    if (!success) {
      return (
        <div>
          <div className="modalRegister">
            <div className="title">
              <h4>Crea una cuenta</h4>
              <a href="#" onClick={() => this.props.openModalBox(Login)}>
                Inicia Sesión
              </a>
            </div>

            <div className="form_register">
              <form
                className="form_color form"
                name="registerForm"
                onSubmit={handleSubmit(this.formSubmit)}
              >
                <Field
                  autoFocus
                  name="first_name"
                  fieldName="firstName"
                  component={renderField}
                  label="Nombre"
                  type="text"
                  validate={[required, name_validation]}
                  placeholder="Nombre"
                />

                <Field
                  name="last_name"
                  fieldName="lastName"
                  component={renderField}
                  label="Apellidos"
                  type="text"
                  validate={[required, lastName_validation]}
                  placeholder="Apellidos"
                />

                <Field
                  name="email"
                  fieldName="email"
                  component={renderField}
                  label="Correo"
                  type="text"
                  normalize={normalizeToLowerCaseAndTrim}
                  validate={[required, email]}
                  placeholder="Correo"
                />

                <Field
                  name="password"
                  fieldName="password"
                  component={renderField}
                  label="Contraseña"
                  type="password"
                  validate={[required, password]}
                  placeholder="Contraseña"
                />

                <Field
                  name="password_conf"
                  fieldName="confirmation"
                  component={renderField}
                  label="Confirmación"
                  type="password"
                  validate={[required, password, password_confirmation]}
                  placeholder="Confirmación"
                />

                {/* success && <Redirect to="/users/profile/" /> */}
                {error && (
                  <div className="form_status danger form_status--big">{error}</div>
                )}
                {submitting && (
                  <img
                    alt="Cargando"
                    src="data:image/gif;base64,R0lGODlhEAAQAPIAAP///wAAAMLCwkJCQgAAAGJiYoKCgpKSkiH/C05FVFNDQVBFMi4wAwEAAAAh/hpDcmVhdGVkIHdpdGggYWpheGxvYWQuaW5mbwAh+QQJCgAAACwAAAAAEAAQAAADMwi63P4wyklrE2MIOggZnAdOmGYJRbExwroUmcG2LmDEwnHQLVsYOd2mBzkYDAdKa+dIAAAh+QQJCgAAACwAAAAAEAAQAAADNAi63P5OjCEgG4QMu7DmikRxQlFUYDEZIGBMRVsaqHwctXXf7WEYB4Ag1xjihkMZsiUkKhIAIfkECQoAAAAsAAAAABAAEAAAAzYIujIjK8pByJDMlFYvBoVjHA70GU7xSUJhmKtwHPAKzLO9HMaoKwJZ7Rf8AYPDDzKpZBqfvwQAIfkECQoAAAAsAAAAABAAEAAAAzMIumIlK8oyhpHsnFZfhYumCYUhDAQxRIdhHBGqRoKw0R8DYlJd8z0fMDgsGo/IpHI5TAAAIfkECQoAAAAsAAAAABAAEAAAAzIIunInK0rnZBTwGPNMgQwmdsNgXGJUlIWEuR5oWUIpz8pAEAMe6TwfwyYsGo/IpFKSAAAh+QQJCgAAACwAAAAAEAAQAAADMwi6IMKQORfjdOe82p4wGccc4CEuQradylesojEMBgsUc2G7sDX3lQGBMLAJibufbSlKAAAh+QQJCgAAACwAAAAAEAAQAAADMgi63P7wCRHZnFVdmgHu2nFwlWCI3WGc3TSWhUFGxTAUkGCbtgENBMJAEJsxgMLWzpEAACH5BAkKAAAALAAAAAAQABAAAAMyCLrc/jDKSatlQtScKdceCAjDII7HcQ4EMTCpyrCuUBjCYRgHVtqlAiB1YhiCnlsRkAAAOwAAAAAAAAAAAA=="
                  />
                )}

                <div className="form__data">
                  <input
                    value="Entrar"
                    type="submit"
                    name="register_submit"
                    value="Entrar"
                    placeholder="Entrar"
                    className="c2a_round"
                  />
                </div>
              </form>

              {/* <span>O</span>

                        <div className="form_register__facebook">
                            <div className="login_button" onClick={() => {
                                FB.login( response => {
                                    if (response.authResponse) {
                                        this.loginCallback(response);
                                    }
                                }, {scope: config.facebookAuth.profileFields.join()});
                            }} >
                            <span>Reg&iacute;strate con Facebook</span>
                            </div>
                        </div> */}
            </div>

            <div className="footer_form">
              <p>
                Al registrarte a Canasta Rosa, aceptas nuestros <br />
                <Link
                  to="/legales/terminos-condiciones"
                  onClick={this.props.closeModalBox}
                >
                  Términos y Condiciones
                </Link>{' '}
                y
                <Link to="/legales/privacidad" onClick={this.props.closeModalBox}>
                  {' '}
                  Avisos de Privacidad
                </Link>
                .
              </p>
            </div>
          </div>
        </div>
      );
    }

    return (
      <div className="modalRegisterConfirmation">
        <div className="message">
          <h2>Activa tu cuenta</h2>
          <p>
            Estás a <span>sólo un paso</span> de formar parte de nuestra comunidad.
          </p>
          <p>
            Te hemos enviado un <span>correo electrónico</span> con información
            para&nbsp;
            <span>activar tu cuenta</span>.
          </p>
          <a className="c2a_square" onClick={this.props.closeModalBox}>
            Ok
          </a>
        </div>
      </div>
    );
  }
}

// Wrap component within reduxForm
Register = reduxForm({
  form: 'register_form',
})(Register);

// Redux Map Functions
function mapStateToProps({ registration }) {
  const { status, payload } = registration;
  return {
    status,
    payload,
  };
}
function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      register: userActions.register,
      socialLogin: authenticationActions.socialLogin,
      openModalBox: modalBoxActions.open,
      closeModalBox: modalBoxActions.close,
    },
    dispatch,
  );
}

// Export Connected Component
Register = connect(mapStateToProps, mapDispatchToProps)(Register);
export default Register;
