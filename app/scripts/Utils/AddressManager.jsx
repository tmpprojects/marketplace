import React from 'react';
import { connect } from 'react-redux';
import Sentry from '../Utils/Sentry';
import { bindActionCreators } from 'redux';
import { Field, reduxForm, getFormValues } from 'redux-form';

import { ActionButton } from './ActionButton';
import { required } from './forms/formValidators';
import { InputField, SelectField } from './forms/formComponents';
import { modalBoxActions, userActions } from '../Actions';
import { STATES_MEXICO } from '../Constants/config.constants';

const DEFAULT_LOCATION = { lng: 19.39068, lat: -99.2836984 };

/*
 * Form Validation Function
 * @param {object} values : Form values
 */
const streetValidation = (value) =>
  value && value.length < 3
    ? 'Escribe una dirección con mínimo 3 caracteres.'
    : undefined;
const neighborhoodValidation = (value) =>
  !value.length ? 'Selecciona tu colonia.' : undefined;
const cityValidation = (value) =>
  !value.length ? 'Selecciona tu alcaldía o municipio.' : undefined;
const postalValidation = (value) =>
  value && value.length !== 5 ? 'Escribe tu código postal.' : undefined;

class AddressManager extends React.Component {
  static defaultProps = {
    onCancel: (e) => null,
  };
  state = {
    isMapLoaded: false,
    shouldDrawMap: false,
    neighborhoodList: [],
    stateList: [],
    municipalityList: [],
  };
  componentDidMount() {
    this.initMap();

    // If component initializes with an address data,
    // search for the address via Google Maps Geolocation API
    if (this.props.initialValues) {
      // Update Address Form
      // this.geocodeByAddress(this.props.initialValues);
      this.updateLocalityFormFields(this.props.initialValues.zip_code);

      // If component has no initialValues,
      // attempt to use browser´s navigator geolocation
    } else {
      this.getGeoLocation();
    }
  }
  onCloseWindow = () => {
    this.props.onCancel();
    this.props.dispatch(modalBoxActions.close());
  };

  addressAutoCompleteInput = React.createRef();
  addressAutoCompleteList = undefined;
  addressMap = undefined;
  addressMarker = undefined;
  addressGeocoder = undefined;
  addressLocationDefault = { lat: 23.831489, lng: -102.343187 };

  /**
   * initMap()
   * Initializies a Google Map and other Location/Address related objects
   */
  initMap = () => {
    // Create Google Map object
    this.addressMap = new google.maps.Map(document.getElementById('map'), {
      center: this.addressLocationDefault,
      zoom: 10,
      mapTypeControl: false,
      streetViewControl: false,
      fullscreenControl: false,
    });

    // Creates Google Geocoder object
    this.addressGeocoder = new google.maps.Geocoder();

    // Create Google AutoComplete object
    this.addressAutoCompleteList = new google.maps.places.Autocomplete(
      this.addressAutoCompleteInput.current,
    );
    this.addressAutoCompleteList.setComponentRestrictions({ country: ['mx'] });
    this.addressAutoCompleteList.setFields([
      'address_components',
      'geometry',
      'icon',
      'name',
    ]);
    this.addressAutoCompleteList.addListener('place_changed', () => {
      this.setState({
        isMapLoaded: false,
      });
      const place = this.addressAutoCompleteList.getPlace();
      this.positionMarker(place.geometry.location);
      this.centerMap(place.geometry.location);
      this.updateFormValues(place);
    });

    // Create and Position Marker on map
    this.addressMarker = new google.maps.Marker({
      map: this.addressMap,
      anchorPoint: new google.maps.Point(0, -29), // TODO: Set anchor point and zoom level
      position: this.addressLocationDefault,
      //draggable: true,
      animation: google.maps.Animation.DROP,
    });
    //this.addressMarker.addListener('dragend', this.onMarkerDrag);
  };

  /**
   * onMarkerDrag()
   * Google Maps´ marker 'drag' event handler
   * Centers the map based on markers´ location
   */
  onMarkerDrag = () => {
    const lat = this.addressMarker.getPosition().lat();
    const lng = this.addressMarker.getPosition().lng();
    const location = { lat, lng };
    this.centerMap(location);

    // Geocode current location and fill in the address form
    this.getPlaceFromLocation(location, 'location').then((place) =>
      this.updateFormValues(place),
    );
  };

  /**
   * getGeoLocation()
   * Gets users location using the HTML5´s navigation API
   */
  getGeoLocation = () => {
    // Check if navigator API is available
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition((position) => {
        const location = {
          lat: position.coords.latitude,
          lng: position.coords.longitude,
        };

        // Center marker and map
        this.positionMarker(location);
        this.centerMap(location);

        // Geocode current location and fill in the address form
        this.getPlaceFromLocation(location, 'location').then((place) => {
          this.updateFormValues(place);
          this.setState({
            isMapLoaded: true,
          });
        });
      });

      // Center Map on default Location
    } else {
      this.centerMap(DEFAULT_LOCATION);
      this.positionMarker(DEFAULT_LOCATION);
    }
  };

  /**
   * geocodeByAddress()
   * Geocodes an address string to get a location
   */
  geocodeByAddress = (currentAddress) => {
    if (!currentAddress) return;

    // Get address fields
    const street = currentAddress.street_address || '';
    const num = currentAddress.num_ext || '';
    const col = currentAddress.neighborhood || '';
    const cp = currentAddress.zip_code || '';
    const city = currentAddress.city || '';

    // Compose address and try to 'geocode' it to get a location.
    const address = `${street} ${num} ${col} ${cp} ${city}`;
    this.getPlaceFromLocation(address, 'address')
      .then((place) => {
        // Center map and marke
        this.positionMarker(place.geometry.location);
        this.centerMap(place.geometry.location);
        this.updateFormValues(place);
      })
      .catch((error) => console.log(error));
  };

  /**
   * centerMap()
   * Centers the map
   */
  centerMap = (location) => {
    this.addressMap.setCenter(location);
    this.addressMap.setZoom(17);
  };

  /**
   * positionMarker()
   * Position the marker
   * @param {lat, lng} location
   */
  positionMarker = (location) => {
    this.addressMarker.setPosition(location);
    this.addressMarker.setVisible(true);
  };

  /**
   * getPlaceFromLocation()
   * Get a 'place' based on the location coordinates or address string.
   * @param {object || string} location | A location object ({lat, lng}) or an address string
   * @param {string} type | The type of locations sent on the first parameter (string || object)
   */
  getPlaceFromLocation = (location, type) =>
    new Promise((resolve, reject) => {
      this.addressGeocoder.geocode({ [type]: location }, (results, status) => {
        if (status === 'OK') {
          resolve(results[0]);
          return;
        }
        reject(`Geocode was not successful for the following reason: ${status}`);
      });
    });

  /**
   * getPostalCodeDetails()
   */
  getPostalCodeDetails = (value) => {
    //
    if (value && value.length >= 5) {
      return this.updateLocalityFormFields(value);
    }
  };

  /**
   * updateFormValues()
   * Update redux form based on a Google Geocode 'place' object.
   */
  updateFormValues = (place) => {
    const { initialValues } = this.props;

    // Update lat/lng fields
    const latitude = place.geometry.location.lat().toFixed(8);
    this.props.change('latitude', latitude);
    const longitude = place.geometry.location.lng().toFixed(8);
    this.props.change('longitude', longitude);

    // Update 'default address' fields
    this.props.change(
      'pickup_address',
      initialValues ? initialValues.pickup_address : false,
    );
    this.props.change(
      'default_address',
      initialValues ? initialValues.default_address : false,
    );

    // Define an undefined zip_code
    let zipCode;

    // Get each component of the address from the 'place' details
    // and fill the corresponding field on the form.
    for (let i = 0; i < place.address_components.length; i++) {
      const addressType = place.address_components[i].types[0];
      const value = place.address_components[i].long_name;

      // Save values to redux form
      switch (addressType) {
        case 'street_number':
          this.props.change('num_ext', value);
          break;
        case 'route':
          this.props.change('street_address', value);
          break;
        case 'administrative_area_level_1': {
          const localityList = Object.keys(STATES_MEXICO);
          const locality = localityList.find((s) => STATES_MEXICO[s] === value);
          this.props.change('state', locality || 'DF');
          break;
        }
        case 'country':
          this.props.change('country', { value: 'MX', display_name: 'México' });
          break;
        case 'postal_code':
          zipCode = value;
          break;
        default:
      }
    }

    // Verify if 'place' return by Google, does not includes a zip_code
    // use current address zip_code as default.
    if (!zipCode && initialValues) {
      zipCode = initialValues.zip_code;
    }
    this.props.change('zip_code', zipCode);

    // Based on the zip_code,
    // get location details (state, settlement, city, etc...)
    if (zipCode) {
      this.updateLocalityFormFields(zipCode);
    }

    // Clear Google Maps Place Autocomplete field. (for UX pourposes)
    setTimeout(() => {
      this.addressAutoCompleteInput.current.value = '';
    }, 10);
  };

  updateLocalityFormFields = (zipCode) => {
    const { initialValues } = this.props;

    this.props
      .getAddressDetailsByZipCode(zipCode)
      .then((response) => {
        const locationDetails = response.data;
        // Verify address format.
        const neighborhoodList = locationDetails.settlements || [];
        let colonia;
        if (initialValues && initialValues.neighborhood) {
          colonia = neighborhoodList.find(
            (s) => s.name.toLowerCase() === initialValues.neighborhood.toLowerCase(),
          );
        }
        if (!colonia) {
          if (neighborhoodList.length === 1) {
            this.props.change('neighborhood', neighborhoodList[0].name);
          } else {
            this.props.change('neighborhood', null);
          }
        } else {
          this.props.change('neighborhood', colonia.name);
        }

        // Municipality
        this.props.change('city', locationDetails.municipality);

        // State
        this.props.change('state', locationDetails.state);

        // Update State.
        this.setState({
          neighborhoodList,
          municipalityList: [locationDetails.municipality],
          stateList: [locationDetails.state],
        });
      })
      .catch((error) => Sentry.captureException(error));
  };

  renderFormAddress = () => {
    const {
      handleSubmit,
      submit,
      submitting,
      pristine,
      valid,
      initialValues,
    } = this.props;
    const { neighborhoodList, municipalityList, stateList } = this.state;
    const availableStates = Object.keys(STATES_MEXICO).reduce((l, s) => {
      const stateFound = stateList.find((i) => i === STATES_MEXICO[s]);
      if (stateFound) {
        return [...l, { name: STATES_MEXICO[s], value: s }];
      }
      return l;
    }, []);

    return (
      <form
        onSubmit={handleSubmit}
        className="storeApp-modal__content modal-box-form__form form"
      >
        <div className="container">
          <Field
            component={InputField}
            id="address1"
            name="street_address"
            validate={[required, streetValidation]}
            className="streetName"
            label="Calle*"
            showErrorsOnTouch={!initialValues}
          />
        </div>

        <div className="container">
          <Field
            component={InputField}
            id="exterior_number"
            name="num_ext"
            validate={[required]}
            className="streetNumber"
            label="Número Exterior*"
            showErrorsOnTouch={!initialValues}
          />

          <Field
            component={InputField}
            id="interior_number"
            name="num_int"
            className="suiteNumber"
            label="Número Interior"
          />
        </div>

        <div className="container">
          <Field
            id="neighborhood"
            name="neighborhood"
            validate={[required, neighborhoodValidation]}
            className="neighborhood"
            label="Colonia*"
            component={SelectField}
            showErrorsOnTouch={!initialValues}
          >
            <option value="">Elige una opción</option>
            {neighborhoodList.map((i) => (
              <option key={i.id} value={i.name}>
                {i.name}
              </option>
            ))}
          </Field>

          <Field
            component={InputField}
            id="zip_code"
            name="zip_code"
            validate={[required, postalValidation]}
            type="number"
            maxLength="5"
            className="postalCode"
            onChange={(e, value) => this.getPostalCodeDetails(value)}
            label="C.P.*"
          />
        </div>

        <div className="container">
          <Field component="input" id="latitude" name="latitude" hidden />
          <Field component="input" id="longitude" name="longitude" hidden />
        </div>

        <div className="container container--half">
          <Field
            id="city"
            name="city"
            validate={[required, cityValidation]}
            className="city select"
            label="Alcaldía / Municipio*"
            component={SelectField}
            showErrorsOnTouch={!initialValues}
          >
            {municipalityList.length ? (
              municipalityList.map((i) => (
                <option key={i} value={i}>
                  {i}
                </option>
              ))
            ) : (
              <option value="">Elige una opción</option>
            )}
          </Field>

          <Field
            id="state"
            name="state"
            validate={[required]}
            className="state select"
            label="Estado*"
            component={SelectField}
            showErrorsOnTouch={!initialValues}
          >
            {availableStates.length ? (
              availableStates.map((i) => (
                <option key={i.value} value={i.value}>
                  {i.name}
                </option>
              ))
            ) : (
              <option value="">Elige una opción</option>
            )}
          </Field>
        </div>

        <Field
          component={InputField}
          id="addressName"
          name="address_name"
          className="addressName"
          label="Alias"
          placeholder="Casa, Trabajo, Novi@, Sucursal..."
        />

        <div className="storeApp-modal__navBottom-addressNav">
          <button
            type="button"
            onClick={this.onCloseWindow}
            className="storeApp-modal__button storeApp-modal__button--cancel"
          >
            Cancelar
          </button>
          <ActionButton
            onClick={submit}
            disabled={pristine || submitting || !valid}
            className="storeApp-modal__button"
          >
            Guardar
          </ActionButton>
        </div>
      </form>
    );
  };

  render() {
    const { handleSubmit, title } = this.props;
    const { isMapLoaded } = this.state;
    const formAddress = this.renderFormAddress();
    return (
      <div className="modal-box-form storeApp-modal modal-map">
        {title ? <h4 className="storeApp-modal__title">{title}</h4> : null}

        <div className="map_container">
          <div style={{ height: '100%', display: isMapLoaded }}>
            <div id="map" />
          </div>
        </div>

        <form
          onSubmit={handleSubmit}
          className="
                        storeApp-modal__content
                        modal-box-form__form
                        form form-search_address
                    "
        >
          <div id="pac-container">
            <input
              type="text"
              id="pac-input"
              name="search_address"
              className="search_address"
              placeholder="Buscar dirección"
              ref={this.addressAutoCompleteInput}
            />
          </div>
        </form>

        {formAddress}
      </div>
    );
  }
}

// Wrap component within reduxForm
const formName = 'AddressManager_form';
AddressManager = reduxForm({
  form: formName,
})(AddressManager);

// Pass Redux state and actions to component
function mapStateToProps(state) {
  return {
    currentAddress: getFormValues(formName)(state),
  };
}
function mapDispatchToProps(dispatch) {
  const { getAddressDetailsByZipCode } = userActions;
  return bindActionCreators(
    {
      getAddressDetailsByZipCode,
    },
    dispatch,
  );
}

// Export Connected Component
export default connect(mapStateToProps, mapDispatchToProps)(AddressManager);
