import React from 'react';
import { Component } from 'react';
import PropTypes from 'prop-types';

const duration = 200;
const defaultStyle = {
  transition: `all ${duration}ms ease-in-out`,
  opacity: 0,
  position: 'absolute',
};
const transitionStyles = {
  loading: { opacity: 0 },
  loaded: { opacity: 1 },

  horizontal: { width: '100%', height: 'auto' },
  vertical: { width: 'auto', height: '100%' },
};

// Icon
export class UIIcon extends React.Component {
  static propTypes = {
    icon: PropTypes.string.isRequired,
  };
  render() {
    const { className: classProps = '', ...restProps } = this.props;
    let icon = '';

    switch (this.props.icon) {
      case 'delete':
        return (
          <div
            className={`ui-icon ui-icon--${this.props.type} ${classProps}`}
            {...restProps}
          >
            <div className="ui-icon__icon">
              <svg version="1.1" viewBox="0 0 469.404 469.404">
                <path
                  d="M310.399,235.083L459.88,85.527c12.545-12.546,12.545-32.972,0-45.671L429.433,9.409
                                c-12.547-12.546-32.971-12.546-45.67,0l-149.48,149.558L85.642,10.327c-12.546-12.546-32.972-12.546-45.67,0L9.524,40.774
                                c-12.546,12.546-12.546,32.972,0,45.671l148.64,148.639L9.678,383.495c-12.546,12.546-12.546,32.971,0,45.67l30.447,30.447
                                c12.546,12.546,32.972,12.546,45.67,0l148.487-148.41l148.792,148.793c12.547,12.546,32.973,12.546,45.67,0l30.447-30.447
                                c12.547-12.546,12.547-32.972,0-45.671L310.399,235.083z"
                />
              </svg>
            </div>
          </div>
        );
    }
  }
}
