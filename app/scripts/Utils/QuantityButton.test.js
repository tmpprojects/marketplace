import React from 'react';
import { shallow, mount } from 'enzyme';
import { shallowToJson } from 'enzyme-to-json';
import QuantityButton from './QuantityButton';

describe('<QuantityButton>', () => {
  let wrapper;
  const mockOnQuantityChange = jest.fn();

  beforeAll(() => {
    const props = {
      quantity: 0,
      onQuantityChange: mockOnQuantityChange,
      maxQuantity: 10,
      store: {
        getState: jest.fn(),
        subscribe: jest.fn(),
        dispatch: jest.fn(),
      },
    };

    wrapper = mount(<QuantityButton {...props} />);

    wrapper.setState({
      quantity: props.quantity,
      selectedQuantity: props.quantity,
      maxQuantity: props.maxQuantity,
    });
  });

  it('should to be defined correctly', () => {
    expect(wrapper).toBeDefined();

    //expect(component).toMatchSnapshot();
    // expect(container.firstChild.classList.contains('foo')).toBe(true)
  });

  it('should snapshot correctly', () => {
    expect(wrapper).toMatchSnapshot();
  });

  it('should minus render correctly', () => {
    const clickMinus = wrapper.find('button#minus');

    // clickMinus.simulate('click', {
    //   target: { name: 'plus' }
    // });
    expect(clickMinus.length).toBe(1);
    // expect(mockOnQuantityChange).toBeCalled();
  });

  it('should plus render correctly', () => {
    const clickPlus = wrapper.find('button#minus');
    expect(clickPlus.length).toBe(1);
  });

  it('should unmount correctly', () => {
    expect(wrapper.unmount()).toBeTruthy;
  });

  // wrapper
  // .find('button#submit_form')
  // .simulate('click');

  //   component
  // .find('#agreetoterms')
  // .simulate('change', {target: {checked: true}});

  // component.find('#input').simulate('keydown', { keyCode: 70 });

  //   const component = shallow(<MyComponent />);
  // const result = component.instance().callMethod();

  // expect(wrapper.state('form_submitted')).toEqual(true);
  //wrapper.unmount();
  //expect(component).toMatchSnapshot();
  //expect(wrapper).toBeDefined();
  // expect(true).toBeTruthy();
  //});
  // it('state increments when plus button is clicked', () => {
  //     const mockFunction = jest.fn();
  //     const component = mount(
  //         <QuantityButton
  //             onQuantityChange={mockFunction}
  //             quantity={1}
  //             maxQuantity={10}
  //         />
  //     );
  //     component.find('button#plus').simulate('click');
  //     expect(component.state('selectedQuantity')).toEqual(2);
  //     component.unmount();
  // });
  // it('state descends when minus button is clicked', () => {
  //     const mockFunction = jest.fn();
  //     const component = mount(
  //         <QuantityButton
  //             onQuantityChange={mockFunction}
  //             quantity={2}
  //             maxQuantity={10}
  //         />
  //     );
  //     component.find('button#minus').simulate('click');
  //     expect(component.state('selectedQuantity')).toEqual(1);
  //     component.unmount();
  // });
  // it('function gets called after click', () => {
  //     const mockFunction = jest.fn();
  //     const component = mount(
  //         <QuantityButton
  //             onQuantityChange={mockFunction}
  //             quantity={1}
  //             maxQuantity={10}
  //         />
  //     );
  //     component.find('button#plus').props().onClick();
  //
  //     component.unmount();
  // });
});

/*

// import the module to mock
import axios from 'axios';
// wrap the module in jest.mock()
jest.mock('axios');
test('should fetch users', () => {
  const users = [{first_name: 'Ross'}];
  const resp = {data: users};
  // append .mockResolvedValue(<return value>) to the module method
  axios.get.mockResolvedValue(resp);
  // carry out your test
  return expect(resp.data).toEqual(users));
});

*/
