import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Field, reduxForm, SubmissionError } from 'redux-form';

import Password from './Password';
import { IconPreloader } from './Preloaders';
import { getUIErrorMessage } from '../Utils/errorUtils';
import { userLoginErrorTypes } from '../Constants/user.constants';
import { required, email, password } from './forms/formValidators';
import { authenticationActions, userActions, modalBoxActions } from '../Actions';

/*
 * NORMALIZE CREDENTIALS
 * @param {object} props : Field Properties
 */
const cleanUserName = (value) => value.toLowerCase().trim();
const cleanPassword = (value) => value.trim();

/*
 * Render Custom Form Fields
 * @param {object} props : Field Properties
 */
const renderField = ({
  input,
  label,
  type,
  autoFocus,
  meta: { touched, error, warning },
}) => (
  <div className="form__data">
    <input {...input} placeholder={label} type={type} autoFocus={autoFocus} />
    {touched &&
      ((error && <div className="form_status danger">{error}</div>) ||
        (warning && <div className="form_status warning">{warning}</div>))}
  </div>
);

/**
 * FormLogin Component
 * @param {*} props | Component Props
 */
let FormLogin = (props) => {
  const {
    openModalBox,
    handleSubmit,
    success,
    pristine,
    error,
    submitting,
    login,
  } = props;

  /*
   * FORM FUNCTIONS
   * @param {objecty} formData : Data of the Redux-Form
   */
  const formSubmit = (formData) =>
    // Try to log user in
    login(formData.email, formData.password)
      .then((response) => {
        props.onFormSubmit();
        props.closeModalBox();
      })
      .catch((e) => {
        //'Nombre de usuario o contraseña inválidos.'
        const uiMessage =
          e.response.data.non_field_errors ||
          getUIErrorMessage(e.response, userLoginErrorTypes);

        // Register error
        throw new SubmissionError({
          _error: uiMessage,
        });
      });

  return (
    <div className="modalLogin">
      <div>
        <form
          className="form_color"
          name="loginForm"
          onSubmit={handleSubmit(formSubmit)}
        >
          <Field
            autoFocus
            name="email"
            component={renderField}
            label="Correo"
            type="text"
            normalize={cleanUserName}
            validate={[required, email]}
            placeholder="Correo"
          />

          <Field
            name="password"
            component={renderField}
            label="Contraseña"
            type="password"
            normalize={cleanPassword}
            validate={[required, password]}
            placeholder="Contraseña"
          />

          {success && window.location.reload()}
          {error && <div className="form_status danger big">{error}</div>}
          {submitting && <IconPreloader />}

          <div className="form__data">
            <input
              type="submit"
              name="login_submit"
              placeholder="Entrar"
              value="Entrar"
              disabled={pristine || submitting}
            />
          </div>
        </form>

        {/* <span>O</span> 
                <div className="form_register__facebook">
                    <div className="login_button" onClick={() => {
                            FB.login( response => {
                                if (response.authResponse) {
                                    this.loginCallback(response);
                                }
                            }, {scope: config.facebookAuth.profileFields.join()});
                        }} >
                        <span>Contin&uacute;a con Facebook</span>
                    </div>
                </div> */}
      </div>

      <div className="footer_form">
        <a
          href="#"
          onClick={(e) => {
            e.preventDefault();
            openModalBox(Password);
          }}
        >
          ¿Olvidaste tu contraseña?
        </a>
      </div>
    </div>
  );
};

// Wrap component within reduxForm
FormLogin = reduxForm({
  form: 'login_form',
})(FormLogin);

// Redux Map Functions
function mapStateToProps({ auth }) {
  return {};
}
function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      login: userActions.login,
      socialLogin: authenticationActions.socialLogin,
      openModalBox: modalBoxActions.open,
      closeModalBox: modalBoxActions.close,
    },
    dispatch,
  );
}

// Export Connected Component
export default connect(mapStateToProps, mapDispatchToProps)(FormLogin);
