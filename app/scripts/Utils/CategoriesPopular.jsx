import React from 'react';
import { Link } from 'react-router-dom';

export default () => (
  <section className="categoriesPopular  wrapper--center">
    <h3 className="title--main">Categor&iacute;as Populares</h3>

    <ul className="categoriesPopular__list">
      <li className="category">
        <div className="category__wrapper">
          <h4>
            <Link to="/category/comida-v2/">Comida</Link>
          </h4>
          <Link to="/category/comida-v2/" className="category__image">
            <img
              src={require('../../images/inspire/category_delicious.jpg')}
              alt="Comida"
            />
          </Link>
        </div>
      </li>

      <li className="category">
        <div className="category__wrapper">
          <h4>
            <Link to="/category/salud-y-belleza-v2/">Salud y Belleza</Link>
          </h4>
          <Link to="/category/salud-y-belleza-v2/" className="category__image">
            <img
              src={require('../../images/inspire/salud-y-belleza.jpg')}
              alt="Saludable"
            />
          </Link>
        </div>
      </li>

      <li className="category">
        <div className="category__wrapper">
          <h4>
            <Link to="/category/infantil-v2/">Infantil</Link>
          </h4>
          <Link to="/category/infantil-v2/" className="category__image">
            <img
              src={require('../../images/inspire/category_childhood.jpg')}
              alt="Infantil"
            />
          </Link>
        </div>
      </li>

      <li className="category">
        <div className="category__wrapper">
          <h4>
            <Link to="/category/flores-y-arreglos-v2/">Flores y Arreglos</Link>
          </h4>
          <Link to="/category/flores-y-arreglos-v2/" className="category__image">
            <img
              src={require('../../images/inspire/flores-arreglos.jpg')}
              alt="Fiestas"
            />
          </Link>
        </div>
      </li>

      <li className="category">
        <div className="category__wrapper">
          <h4>
            <Link to="/category/arte-y-diseno-v2/">Dise&ntilde;o</Link>
          </h4>
          <Link to="/category/arte-y-diseno-v2/" className="category__image">
            <img
              src={require('../../images/inspire/category_design.jpg')}
              alt="Diseño"
            />
          </Link>
        </div>
      </li>
    </ul>
  </section>
);
