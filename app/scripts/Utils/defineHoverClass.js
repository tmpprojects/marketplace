export default function () {
  'use strict';
  function touchStart() {
    htmlElement.classList.remove(className);
    htmlElement.removeEventListener('touchstart', touchStart);
  }

  const className = 'hover-active';
  if (typeof window !== 'undefined') {
    if (!('addEventListener' in window)) {
      return;
    }
    document.body.classList.add(className);
  } else {
    return;
  }

  const htmlElement = document.querySelector(`.${className}`);
  htmlElement.addEventListener('touchstart', touchStart);
}
