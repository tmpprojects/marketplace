import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { modalBoxActions } from '../Actions';
import { AssistantModalBox } from './modalbox/AssistantModalBox';
import { trackWithGTM } from '../Utils/trackingUtils';

export class Assistant extends React.Component {
  constructor(props) {
    super(props);

    // Bind Scope to class methods
    this.openAssistentWindow = this.openAssistentWindow.bind(this);
  }

  openAssistentWindow() {
    this.props.openModalBox(() => <AssistantModalBox />);
  }
  render() {
    return null;
    /* Temporarily disable in February */
    return (
      <section className="assistant wrapper--center">
        <div className="assistant__container">
          <h5>¿Buscas recomendaciones para un regalo?</h5>
          <p>Aquí te damos la solución</p>
          <button
            type="button"
            onClick={() => {
              this.openAssistentWindow();
            }}
            id="Mayordomo Rosa"
            className="c2a_round assistant__button gtm_click"
          >
            Contacta al Mayordomo Rosa
          </button>
        </div>
      </section>
    );
  }
}
function mapStateToProps() {
  return {};
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      openModalBox: modalBoxActions.open,
      closeModalBox: modalBoxActions.close,
    },
    dispatch,
  );
}

// Export Component
Assistant = connect(mapStateToProps, mapDispatchToProps)(Assistant);
export default Assistant;
