import React, { Component } from 'react';

import Pagination from './Pagination';

export default class ContainerPagination extends Component {
  constructor() {
    super();
    this.state = {
      items: ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k'],
      currentPage: 1,
      itemsPerPage: 3,
    };
  }

  render() {
    const { items, currentPage, itemsPerPage } = this.state;

    // Logic for displaying current
    const indexOfLast = currentPage * itemsPerPage;
    const indexOfFirst = indexOfLast - itemsPerPage;
    const currentItems = items.slice(indexOfFirst, indexOfLast);

    const renderItems = currentItems.map((item, i) => {
      return <li key={i}>{item}</li>;
    });

    return (
      <React.Fragment>
        <ul>{renderItems}</ul>
        <Pagination items={items} itemsPerPage={itemsPerPage} />
      </React.Fragment>
    );
  }
}
