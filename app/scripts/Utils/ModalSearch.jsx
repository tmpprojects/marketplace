import React from 'react';
import { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import { modalBoxActions } from '../Actions';
import { Search } from '../components/Search/SearchForm';

class ModalSearch extends Component {
  constructor(props) {
    super(props);
  }

  handleClick(e) {
    if (e.target === this.boxContainer) {
      e.preventDefault();
      this.props.dispatch(modalBoxActions.close());
      return;
    }
  }

  render() {
    const { window: Window } = this.props.modalBox;
    if (!this.props.modalBox.visible || Window === undefined) {
      return null;
    }

    return (
      <div
        className="modalSearch"
        ref={(boxContainer) => (this.boxContainer = boxContainer)}
        onClick={(e) => this.handleClick(e)}
      >
        <div className="modalSearch__container">
          <Window />
        </div>
      </div>
    );
  }
}

// PropTypes Definition
ModalSearch.propTypes = {
  window: PropTypes.object.isRequired,
};
ModalSearch.defaultProps = {
  window: {},
};

// Add Redux state and actions to component´s props
function mapStateToProps({ modalBox }) {
  return { modalBox };
}

ModalSearch = connect(mapStateToProps, mapDispatchToProps)(ModalSearch);
export default ModalSearch;
