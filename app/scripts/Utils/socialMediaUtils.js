import config from '../../config';

/**
 * shareOnFacebook()
 * @param {string} url : URL to share on Facebook
 */
export const shareOnFacebook = (url, msg = null, callback = null) => {
  // Open window UI
  FB.ui(
    {
      href: url,
      quote: msg,
      method: 'share',
      //display: 'popup',
      app_id: config.facebookAuth.clientID,

      // Returns an optional callback
    },
    callback,
  );
};

/**
 * shareOnTwitter()
 * @param {string} url : URL to share on Twitter
 * @param {string} msg : Message to share
 */
export const shareOnTwitter = (url, msg = '') => {
  const text = encodeURIComponent(msg);
  const shareUrl = `https://twitter.com/intent/tweet?url=${url}&text=${text}`;

  // Open window
  const twitterWindow = window.open(shareUrl, 'ShareOnTwitter', getWindowOptions());

  // Unlink emerging window from current window
  // to rmeove potential security vulnerabilities.
  twitterWindow.opener = null; // 2
};

/**
 * getWindowOptions()
 * Get size and position for a javascript pop-up window
 * @returns {string} Returns a string with configuration
 */
const getWindowOptions = function () {
  // Calculate size and position
  const width = 500,
    height = 350;
  const left = window.innerWidth / 2 - width / 2,
    top = window.innerHeight / 2 - height / 2;

  // Return configuration
  return [
    'resizable,scrollbars,status',
    'height=' + height,
    'width=' + width,
    'left=' + left,
    'top=' + top,
  ].join();
};
