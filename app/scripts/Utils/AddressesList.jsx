import React, { useEffect } from 'react';
import { connect } from 'react-redux';

import { bindActionCreators } from 'redux';
import { googleMapsID } from '../../config';

import { modalBoxActions, userActions } from '../Actions';
import AddressManager from './AddressManager';

const mapPlaceholderThumbnail = require('../../images/utils/map-location-placeholder.svg');

const AddressesList = (parentProps) => {
  const {
    address,
    input,
    meta,
    userType,
    className = '',
    deleteAddress = () => false,
    updateAddress = () => false,
    changeAddress = () => false,
    openModalBox,
    closeModalBox,
    listAddresses,
    active,
    isLogged = true,
    ...props
  } = parentProps;

  const onChangeAddress = function (e) {
    input.onChange(address);
    makeDefaultAddress(address);
  };

  const onRemoveAddress = function () {
    if (
      confirm(`¿Estás seguro de querer eliminar esta dirección?\n
        Esta acción no podrá deshacerse.`)
    ) {
      deleteAddress(address.uuid);
    }
  };
  const openAddressWindow = function () {
    openModalBox(() => (
      <AddressManager
        onSubmit={updateAddress}
        title="Editar dirección"
        initialValues={address}
      />
    ));
  };
  const makeDefaultAddress = (address) => {
    // updateAddress({ uuid: address.uuid, default_address: true });
    updateAddress({ ...address, default_address: true });

    // listAddresses();
    // if (window.location.pathname.includes('/users/profile')) {
    // updateAddress({ uuid: address.uuid, pickup_address: true });
    // listAddresses();
  };
  // Render Component
  return (
    <div className={`address ${input?.checked ? 'active' : ''}`}>
      <div className="form__data form">
        <input
          type="radio"
          className={`${className}`}
          {...input}
          {...props}
          onChange={onChangeAddress}
        />
        <label htmlFor={props.id}>
          <div className="map_container" onClick={() => makeDefaultAddress(address)}>
            {address.latitude && address.longitude ? (
              <img
                alt=""
                src={`https://maps.googleapis.com/maps/api/staticmap?center=${address.latitude},${address.longitude}&zoom=16&scale=2&size=112x112&markers=size:small%7Ccolor:0xff0000%7Clabel:1%7C${address.latitude},${address.longitude}&key=${googleMapsID}`}
              />
            ) : (
              <img alt="Ubicación" src={mapPlaceholderThumbnail} />
            )}
          </div>
          <div className="info" onClick={() => makeDefaultAddress(address)}>
            <strong style={{ fontWeight: 'bold' }}>{address.address_name} </strong>
            <span>
              {address.street_address} {address.num_ext},&nbsp;
              {address.num_int ? `${address.num_int},` : ''}
              {address.neighborhood}, {address.city}, {address.zip_code}.
            </span>
          </div>
        </label>
      </div>
      <div className="address_c2a">
        {isLogged && (
          <button
            type="button"
            className="button-simple remove"
            onClick={onRemoveAddress}
          >
            Eliminar
          </button>
        )}
        <button type="button" className="button-simple" onClick={openAddressWindow}>
          Editar
        </button>
      </div>
    </div>
  );
};

//
function mapStateToProps() {
  return {};
}
function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      openModalBox: modalBoxActions.open,
      closeModalBox: modalBoxActions.close,
    },
    dispatch,
  );
}

// Export Component

export default connect(mapStateToProps, mapDispatchToProps)(AddressesList);
