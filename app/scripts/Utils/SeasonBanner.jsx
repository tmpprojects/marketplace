import React from 'react';
import { Link } from 'react-router-dom';

export default class SeasonBanner extends React.Component {
  gotoURL = (targetURL) => (window.location.href = targetURL);

  render() {
    return (
      <section
        onClick={() => this.gotoURL('/category/buen-fin')}
        className="season-banner wrapper--center"
      >
        <div className="season-banner__container">
          <h3 className="title">Envío Gratis + 10%</h3>
          <p className="text">de descuento en la sección BUEN FIN </p>
          <h3 className="code">Código: BUENFIN</h3>
          {/* <Link 
                        className="season-banner__button" 
                        to={`/category/san-valentin`}
                    >Regalos con amor</Link> */}
        </div>
      </section>
    );
  }
}
