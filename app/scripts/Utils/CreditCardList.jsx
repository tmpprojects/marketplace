import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

// ICONS
const iconMastercard = require('../../images/icons/payment/icon_mastercard.svg');
const iconVisa = require('../../images/icons/payment/icon_visa.svg');
const iconAmex = require('../../images/icons/payment/icon_amex.svg');
const iconOxxo = require('../../images/icons/payment/icon_oxxo.svg');
const iconPaypal = require('../../images/icons/payment/icon_paypal-checkout.svg');

const CreditCardList = (parentProps) => {
  const {
    creditCard,
    input,
    className = '',
    changeCreditCard = () => false,
    active,
    ...props
  } = parentProps;

  const onChangeCreditCard = function (e) {
    input.onChange(e.target.value);
    //changeCreditCard(creditCard);
  };
  const onClickCreditCard = function (e) {
    changeCreditCard(creditCard);
  };

  // Render Component
  return (
    // <div className="creditCard creditCardOption">
    // <div>
    //     <img
    //         className="logo master-card"
    //         src={iconMastercard}
    //         alt="Master Card"
    //     />
    // </div>
    // <label htmlFor={props.id}>
    //     <span>
    //         XXXX XXXX XXXX {creditCard.card_id.slice(-4)}
    //     </span>
    // </label>
    //     <div>
    // <input
    //     type="radio"
    //     className={`${className}`}
    //     {...input}
    //     {...props}
    //     onChange={onChangeCreditCard}
    //     onClick={onClickCreditCard}
    // />
    //     </div>
    // </div>
    <div className="creditCard creditCardOption">
      <label htmlFor={props.id}>
        <div
          className={`logo card-provider ${creditCard.payment_method.display_name}`}
        />
        <div>
          <span>XXXX XXXX XXXX {creditCard.last_four_digits}</span>
        </div>
      </label>
      <input
        type="radio"
        className={`${className}`}
        {...input}
        {...props}
        onChange={onChangeCreditCard}
      />
    </div>
  );

  <div className="form__data creditCard">
    <div>
      <img className="logo master-card" src={iconMastercard} alt="Master Card" />
    </div>

    <div>
      <span>XXXX XXXX XXXX {defaultCreditCard.slice(-4)}</span>
    </div>
    <span className="arrow" onClick={this.openWindowForm}></span>
  </div>;
};

function mapStateToProps() {
  return {};
}
function mapDispatchToProps(dispatch) {
  return bindActionCreators({}, dispatch);
}

// Export Component
export default connect(mapStateToProps, mapDispatchToProps)(CreditCardList);
