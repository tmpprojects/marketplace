import Moment from 'dayjs';

const MONTH_NAMES = [
  'Enero',
  'febrero',
  'Marzo',
  'Abril',
  'Mayo',
  'Junio',
  'Julio',
  'Agosto',
  'Septiembre',
  'Octubre',
  'Noviembre',
  'Diciembre',
];
export const WEEK_OBJECT = [
  { name: 'Lunes', value: 0, description: '' },
  { name: 'Martes', value: 1, description: '' },
  { name: 'Miércoles', value: 2, description: '' },
  { name: 'Jueves', value: 3, description: '' },
  { name: 'Viernes', value: 4, description: '' },
  { name: 'Sábado', value: 5, description: '' },
  { name: 'Domingo', value: 6, description: '' },
];

/*------------------------------------------------
//    FORMAT DATE UTILITY
------------------------------------------------*/
export const formatDate = function (date) {
  const day = date.getDate();
  const monthIndex = date.getMonth();
  const year = date.getFullYear();
  return `${day} ${MONTH_NAMES[monthIndex]} ${year}`;
};
export const formatDateYYMMDD = function (date) {
  const day = date.getDate();
  let monthIndex = date.getMonth() + 1;
  monthIndex = monthIndex < 10 ? `0${monthIndex}` : `${monthIndex}`;
  const year = date.getFullYear();
  return `${year}-${monthIndex}-${day}`;
};
export const formatDateToUTC = (date) =>
  new Date(date.getUTCFullYear(), date.getUTCMonth(), date.getUTCDate());

/*------------------------------------------------
//   CONVERT TIME FROM 24hr TO 12hr FORMAT
------------------------------------------------*/
export const formatHour24To12 = function (time) {
  const timeArray = time
    .split(':')
    .map((a) => (isNaN(parseInt(a, 10)) ? 0 : parseInt(a, 10)));

  return timeArray.reduce((a, b, i) => {
    let formattedTime = b;
    if (i === 0) {
      formattedTime = b > 12 ? b - 12 : b;
    }
    formattedTime = formattedTime > 9 ? formattedTime : `0${formattedTime}`;
    return `${a}${formattedTime}${i < timeArray.length - 1 ? ':' : ''}`;
  }, '');
};
export const addHourPeriodSuffix = function (time) {
  let periodSuffix = 'am';
  const timeComponents = time?.split(':') || [];
  let hour = parseInt(timeComponents?.shift(), 10);

  // Verify hour period
  if (hour > 12) {
    periodSuffix = 'pm';
    hour -= 12;
  }

  // Format and return new hour
  hour = hour > 9 ? hour : `0${hour}`;
  return `${[hour, ...timeComponents].join(':')}${periodSuffix}`;
};

/**
 * isValidWorkingDay()
 * Check if the date, passed as an argument, is a valid
 * working/shipping date.
 * @param {date} date | A JS Date object
 * @param {product} product | A JS Object with the characteristics of a product
 * @param {section} section | String to determine what logic applies, needed because fucntion is used in to differents environments with different data structure for the product param
 * @returns bool
 */
export const isValidWorkingDay = (date, product, section) => {
  //destructure minimum and maximun shipping date from product properties
  const {
    physical_properties: { minimum_shipping_date, maximum_shipping_date },
  } = product;

  // Mark date as invalid if it´s before the minimum shipping date
  if (Moment(date).isBefore(minimum_shipping_date, 'YYYY-MM-DD', true)) {
    return false;
  }

  // Mark date as invalid if its after the maximum shipping date
  if (Moment(date).isAfter(maximum_shipping_date, 'YYYY-MM-DD', true)) {
    return false;
  }

  // --Check if store has deactivated delivery days.

  // section = PRODUCT_PAGE
  if (section === 'PRODUCT_PAGE') {
    const {
      physical_properties: { deactivated_delivery_days },
    } = product;
    if (Object.keys(deactivated_delivery_days).length > 0) {
      //Object destructuring and assigning with default values in case keys doesnt exist,
      const {
        'express-car': deactivatedDaysExpressCar = [],
        'express-moto': deactivatedDaysExpressMoto = [],
        standard: deactivatedDaysStandard = [],
      } = deactivated_delivery_days;

      //mixing arrays of deactivated days
      const mixedDeactivatedDays = [
        ...deactivatedDaysExpressCar,
        ...deactivatedDaysExpressMoto,
        ...deactivatedDaysStandard,
      ];

      //obtaninig unique values
      const mixedAndUniqueDeactivatedDays = [...new Set(mixedDeactivatedDays)];
      let isDeactivated = false; // Deactivated flag

      // isDeactivated is true If date is in mixedAndUniqueDeactivatedDays array
      isDeactivated =
        (
          mixedAndUniqueDeactivatedDays.find((day) => {
            return Moment(day).isSame(Moment(date));
          }) || []
        ).length > 0;
      //     // If this day the store is deactivated, mark it as invalid
      if (isDeactivated) return false;
    }
  }

  //section = CART
  if (section === 'CART') {
    //destructure property of deactivatedDeliveryDaysByShippingMethod
    const {
      physical_properties: { deactivatedDeliveryDaysByShippingMethod },
    } = product;
    if (deactivatedDeliveryDaysByShippingMethod.length > 0) {
      let isDeactivated = false; // Deactivated flag
      // isDeactivated is true If date is in deactivatedDeliveryDaysByShippingMethod array
      isDeactivated =
        (
          deactivatedDeliveryDaysByShippingMethod.find((day) => {
            return Moment(day).isSame(Moment(date));
          }) || []
        ).length > 0;
      //     // If this day the store is deactivated, mark it as invalid
      if (isDeactivated) return false;
    }
  }

  // Get the weekday number from the current date
  // and find if its a valid working day for this product/store.
  const weekDay = parseInt(Moment(date).format('d'), 10);
  const weekdayIndex = weekDay === 0 ? 6 : weekDay - 1;

  if (section === 'PRODUCT_PAGE') {
    const workingDay = product.work_schedules.find(
      (schedule) => schedule.value === weekdayIndex,
    );
    if (!workingDay) return false; // Disable if this is not a work-day
  }

  if (section === 'CART') {
    const workingDay = product.work_schedules.find(
      (schedule) => schedule.week_day.value === weekdayIndex,
    );
    if (!workingDay) return false; // Disable if this is not a work-day
  }

  // Disable All Sundays
  if (weekDay === 0 && !Moment(date).isSame(Moment('2020-06-21'))) return false;

  // Otherwise, this is a validshipping day
  return true;
};

export const chooseBuenFinCoupon = () => {
  let now = Moment(); //obtaning just this moment in date and time and parsing to dayJS object.

  let discountToApply = '';
  let topHourHoraRosaIsActive = '';
  const nov6 = Moment(new Date(now.year(), 10, 6));
  const nov18 = Moment(new Date(now.year(), 10, 18));
  const nov19 = Moment(new Date(now.year(), 10, 19));
  const nov20 = Moment(new Date(now.year(), 10, 20));
  const nov24 = Moment(new Date(now.year(), 10, 24));
  const nov25 = Moment(new Date(now.year(), 10, 25));
  const nov26 = Moment(new Date(now.year(), 10, 26));
  const nov27 = Moment(new Date(now.year(), 10, 27));
  const nov28 = Moment(new Date(now.year(), 10, 28));
  const nov29 = Moment(new Date(now.year(), 10, 29));
  const nov30 = Moment(new Date(now.year(), 10, 30));
  const dic1 = Moment(new Date(now.year(), 11, 1));
  const dic2 = Moment(new Date(now.year(), 11, 2));
  const dic3 = Moment(new Date(now.year(), 11, 3));
  const dic4 = Moment(new Date(now.year(), 11, 4));
  const dic5 = Moment(new Date(now.year(), 11, 5));
  const dic6 = Moment(new Date(now.year(), 11, 6));

  const horaRosa12Nov = {
    start: Moment('2020-11-12T17:00:00.000Z'), //17:00:00 equals 11:00am Mexico City
    end: Moment('2020-11-12T18:00:00.000Z'), //18:00:00 equals 12:00pm Mexico City
  };
  const horaRosa16Nov = {
    start: Moment('2020-11-16T17:00:00.000Z'), //17:00:00 equals 11:00am Mexico City
    end: Moment('2020-11-16T18:00:00.000Z'), //18:00:00 equals 12:00pm Mexico City
  };
  const horaRosa13Nov = {
    start: Moment('2020-11-13T18:00:00.000Z'), //18:00:00 equals 12:00pm Mexico City
    end: Moment('2020-11-13T19:00:00.000Z'), //19:00:00 equals 13:00 Mexico City
  };

  const horaRosa17Nov = {
    start: Moment('2020-11-17T18:00:00.000Z'), //18:00:00 equals 12:00pm Mexico City
    end: Moment('2020-11-17T19:00:00.000Z'), //19:00:00 equals 13:00 Mexico City
  };

  //For testing
  const horaRosa11Nov = {
    start: Moment('2020-11-13T13:00:00.000Z'),
    end: Moment('2020-11-13T14:00:00.000Z'),
  };

  if (now.isSame(nov6, 'day')) discountToApply = '10DISCBF';
  else if (now.isSame(nov18, 'day')) discountToApply = '10DISCBF';
  else if (now.isSame(nov19, 'day')) discountToApply = '10DISCBF';
  else if (now.isSame(nov20, 'day')) discountToApply = '10DISCBF';
  else if (now.isSame(nov24, 'day')) discountToApply = 'FSBFCM';
  else if (now.isSame(nov25, 'day')) discountToApply = 'FSBFCM';
  else if (now.isSame(nov26, 'day')) discountToApply = 'FSBFCM';
  else if (now.isSame(nov27, 'day')) discountToApply = 'FSBFCM';
  else if (now.isSame(nov28, 'day')) discountToApply = 'FSBFCM';
  else if (now.isSame(nov29, 'day')) discountToApply = 'FSBFCM';
  else if (now.isSame(nov30, 'day')) discountToApply = 'FSBFCM';
  else if (now.isSame(dic1, 'day')) discountToApply = 'COMADRE2';
  else if (now.isSame(dic2, 'day')) discountToApply = 'COMADRE2';
  else if (now.isSame(dic3, 'day')) discountToApply = 'MOMSTER2';
  else if (now.isSame(dic4, 'day')) discountToApply = 'MOMSTER2';
  else if (now.isSame(dic5, 'day')) discountToApply = 'MOMSTER2';
  else if (now.isSame(dic6, 'day')) discountToApply = 'MOMSTER2';
  else if (now.isBetween(horaRosa11Nov.start, horaRosa11Nov.end, 'minute', '[)')) {
    discountToApply = 'HORAROSABF';
    topHourHoraRosaIsActive = horaRosa11Nov.end.format('YYYY-MM-DDTHH:mm:ss');
  } else if (now.isBetween(horaRosa12Nov.start, horaRosa12Nov.end, 'minute', '[)')) {
    discountToApply = 'HORAROSABF';
    topHourHoraRosaIsActive = horaRosa12Nov.end.format('YYYY-MM-DDTHH:mm:ss');
  } else if (now.isBetween(horaRosa13Nov.start, horaRosa13Nov.end, 'minute', '[)')) {
    discountToApply = 'HORAROSABF';
    topHourHoraRosaIsActive = horaRosa13Nov.end.format('YYYY-MM-DDTHH:mm:ss');
  } else if (now.isBetween(horaRosa16Nov.start, horaRosa16Nov.end, 'minute', '[)')) {
    discountToApply = 'HORAROSABF';
    topHourHoraRosaIsActive = horaRosa16Nov.end.format('YYYY-MM-DDTHH:mm:ss');
  } else if (now.isBetween(horaRosa17Nov.start, horaRosa17Nov.end, 'minute', '[)')) {
    discountToApply = 'HORAROSABF';
    topHourHoraRosaIsActive = horaRosa17Nov.end.format('YYYY-MM-DDTHH:mm:ss');
  } else {
    discountToApply = 'none';
    topHourHoraRosaIsActive = '';
  }

  return { discountToApply, topHourHoraRosaIsActive };
};
