import {
  validateShoppingForm,
  warnShoppingForm,
} from '../../components/ShoppingCart/validateShoppingForm';

const cartFormName = 'shoppingOrder_form';
export const SHOPPING_CART_FORM_CONFIG = {
  formName: cartFormName,
  config: {
    form: cartFormName,
    destroyOnUnmount: false, // <---- Preserve data. Dont destroy form on unmount
    enableReinitialize: true, // <---- Allow updates when 'initValues' prop change
    keepDirtyOnReinitialize: true, // <---- Prevent 'dirty' fields to update
    updateUnregisteredFields: true, // <---- Update unregistered fields.
    forceUnregisterOnUnmount: true, // <---- Unregister fields on unmount
    //onSubmit: a => { return new Promise((resolve, reject) => console.log('submitted.')); },
    validate: validateShoppingForm,
    warn: warnShoppingForm,
  },
};
