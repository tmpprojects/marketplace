import React, { Component } from 'react';
import { Link } from 'react-router-dom';

export default class Pagination extends Component {
  constructor() {
    super();
    this.state = {};
    this.handleClick = this.handleClick.bind(this);
  }

  handleClick(e) {
    e.preventDefault();
    // this.setState({
    //   currentPage: Number(e.target.id)
    // });
  }

  render() {
    const { items, itemsPerPage } = this.props;

    // Logic for displaying page numbers
    const pageNumbers = [];
    for (let i = 1; i <= Math.ceil(items.length / itemsPerPage); i++) {
      pageNumbers.push(i);
    }

    const renderPageNumbers = pageNumbers.map((number) => (
      <li key={number} id={number} onClick={this.handleClick} className="number">
        <Link to="">{number}</Link>
      </li>
    ));

    return (
      <div className="pagination">
        <div className="prev">
          <Link to="" className="button">
            {' '}
            <span>Previous</span>{' '}
          </Link>
        </div>

        <ul className="page-numbers">
          <li className="number active">
            <Link to="">1</Link>
          </li>
          <li className="number">
            <Link to="">2</Link>
          </li>
          <li className="number">
            <Link to="">3</Link>
          </li>
          <li className="number">...</li>
          <li className="number">
            <Link to="">10</Link>
          </li>
        </ul>

        <div className="next">
          <Link to="" className="button">
            {' '}
            <span>Next</span>{' '}
          </Link>
        </div>
      </div>
    );
  }
}
