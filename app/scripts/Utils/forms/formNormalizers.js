// PHONE NUMBER NORMALIZER
export const normalizePhone = (value, previousValue) => {
  if (!value) {
    return value;
  }

  // Clean string
  const onlyNums = value.replace(/[^\d]/g, '');
  if (onlyNums.length <= 3) {
    return onlyNums;
  }
  if (onlyNums.length <= 4) {
    return onlyNums;
  }
  if (onlyNums.length <= 6) {
    return onlyNums.slice(0, 2) + '-' + onlyNums.slice(2);
  }
  if (onlyNums.length <= 9) {
    return onlyNums.slice(0, 4) + '-' + onlyNums.slice(4);
  }
  if (onlyNums.length <= 11) {
    return (
      onlyNums.slice(0, 2) + '-' + onlyNums.slice(2, 6) + '-' + onlyNums.slice(6, 13)
    );
  }

  return (
    onlyNums.slice(0, 5) + '-' + onlyNums.slice(5, 9) + '-' + onlyNums.slice(9, 13)
  );
};

// URI NORMALIZER
export const normalizeUrl = (value, previousValue) => {
  if (!value) {
    return value;
  }

  // Clean string
  const trimmedValue = value.trim();
  const cleanValue = trimmedValue.replace(/(^\w+:|^)\/\//, '');
  return `https://${cleanValue}`;
};
// Remove protocol and domain from URI
export const removeDomainProtocolFromURL = (value, socialNetwork) => {
  // Clean string
  const trimmedValue = value.trim();

  const patterns = {};
  patterns.protocol = '^(http(s)?(://))?(www.)?';
  patterns.domain = '[a-zA-Z0-9-_.]+/';
  const regex = new RegExp(patterns.protocol + patterns.domain, 'gi'); //new RegExp('(^\\w+:|^)//(www.)*' + socialNetwork + '/', 'gi');

  let cleanedUri = trimmedValue.replace(regex, '');
  cleanedUri = cleanedUri.replace(/\/$/, '');
  return cleanedUri;
};
// Add protocol and domain to URI
const addDomainProtocolFromURL = (value, previousValue, socialNetwork) => {
  if (!value) {
    return value;
  }
  // Clean string
  const trimmedValue = value.trim();
  const regex = new RegExp('(^\\w+:|^)//(www.)*' + socialNetwork + '/', 'gi');
  const cleanedUri = trimmedValue.replace(regex, '');
  return `https://${socialNetwork}/${cleanedUri}`;
};

// TO LOWER CASE
export const normalizeToLowerCase = (value, previousValue) => {
  return value.toLowerCase();
};
export const normalizeToLowerCaseAndTrim = (value, previousValue) => {
  return value.toLowerCase().trim();
};

// To Number
export const normalizeStringToNumber = (value, previousValue) => {
  return parseInt(value);
};
