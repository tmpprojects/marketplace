import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Field } from 'redux-form';
import { BrowserBackend } from '@sentry/browser';

/*
 * Render Custom Input Field
 * @param {object} props : Field Properties
 */
export const InputField = ({
  input,
  label,
  className: classSuffix = '',
  showErrorsOnTouch = true,
  meta,
  ...props
}) => {
  const getErrorClass = () => {
    if (!showErrorsOnTouch) {
      return 'error';
    } else if (meta.touched) {
      return 'error';
    }
    return '';
  };
  const renderErrors = () => (
    <div>
      {(meta.error && <div className="form_status danger">{meta.error}</div>) ||
        (meta.warning && <div className="form_status warning">{meta.warning}</div>)}
    </div>
  );
  return (
    <div className={`form__data form__data-${classSuffix}`}>
      <label className="title" htmlFor={props.id}>
        {label}
      </label>
      <input {...input} className={!meta.valid ? getErrorClass() : ''} {...props} />
      {!showErrorsOnTouch ? renderErrors() : meta.touched ? renderErrors() : null}
    </div>
  );
};

/*
 * Render Custom Text Area
 * @param {object} props : Field Properties
 */
export const TextArea = ({
  input,
  label,
  autoFocus,
  className: classSuffix = '',
  showErrorsOnTouch = true,
  meta,
  onChange = () => false, // <------ FIX
  ...rest
}) => {
  const getErrorClass = () => {
    if (!showErrorsOnTouch) {
      return 'error';
    } else if (meta.touched) {
      return 'error';
    }
    return '';
  };

  const getWarningClass = (classSuffix) => {
    let warningClass = '';
    switch (classSuffix) {
      case 'giftReceiverNote':
        warningClass = 'warning--giftReceiverNote';
        break;
      default:
        warningClass = 'warning';
    }
    return warningClass;
  };
  const renderErrors = () => (
    <div>
      {(meta.error && <div className="form_status danger">{meta.error}</div>) ||
        (meta.warning && (
          <div className={`form_status ${getWarningClass(classSuffix)}`}>
            {meta.warning}
          </div>
        ))}
    </div>
  );

  return (
    <div className={`form__data form__data-${classSuffix}`}>
      <label className="title" htmlFor={input.name}>
        {label}
      </label>
      <textarea
        {...input}
        {...rest}
        onChange={(e) => {
          input.onChange(e);
          onChange(e);
        }}
        className={!meta.valid ? getErrorClass() : ''}
      />
      {/*touched && (error && <div className="form_status danger">{error}</div>)*/}
      {!showErrorsOnTouch ? renderErrors() : meta.touched ? renderErrors() : null}
    </div>
  );
};

/*
 * Render Custom 'Select' Field
 * @param {object} props : Field Properties
 */
export const SelectField = (props) => {
  const {
    input,
    meta,
    type,
    label,
    children,
    showErrorsOnTouch = true,
    className: classSuffix = '',
    ...rest
  } = props;

  const renderErrors = () => (
    <div>
      {(meta.error && <div className="form_status danger">{meta.error}</div>) ||
        (meta.warning && <div className="form_status warning">{meta.warning}</div>)}
    </div>
  );
  const getErrorClass = () => {
    if (!showErrorsOnTouch || meta.touched) {
      return 'error';
    }
    return '';
  };

  return (
    <div className={`form__data form__data-${classSuffix}`}>
      <label className="title" htmlFor={rest.id}>
        {label}
      </label>
      <select {...input} className={!meta.valid ? getErrorClass() : ''}>
        {children}
      </select>
      {(!showErrorsOnTouch && meta.error && renderErrors()) ||
        (meta.touched && meta.error && renderErrors())}
    </div>
  );
};

/*
 * Render Custom Password Field
 * @param {object} props : Field Properties
 */
export const PasswordField = ({
  input,
  label,
  autoFocus,
  className: classSuffix = '',
  meta: { touched, error },
  ...rest
}) => (
  <div className={`form__data form__data-${classSuffix}`}>
    <input {...input} {...rest} type="password" />
    {touched && error && <div className="form_status danger">{error}</div>}
  </div>
);

/*
 * Render Custom Password Field
 * @param {object} props : Field Properties
 */
export const SwitchCheckBox = ({ input, meta, ...props }) => {
  const componentClass = 'form-switch';
  const customStyle = props.style ? `${componentClass}--${props.style}` : '';
  return (
    <div className={`${componentClass} ${customStyle}`}>
      <input {...input} id={props.id} type="checkbox" />
      <label htmlFor={props.id}>
        <span className="checked" />
        <span className="unchecked" />
        <div className="slider" />
      </label>
    </div>
  );
};

/*
 * Render Checkbox group
 * @param {object} props : Field Properties
 */
export default class CheckboxGroup extends Component {
  static propTypes = {
    options: PropTypes.arrayOf(
      PropTypes.shape({
        label: PropTypes.string.isRequired,
        value: PropTypes.string.isRequired,
      }),
    ).isRequired,
  };

  field = ({ input, meta, options }) => {
    const { name, onChange } = input;
    const { touched, error } = meta;
    const inputValue = input.value;

    const checkboxes = options.map(({ label, value }, index) => {
      const handleChange = (event) => {
        const arr = [...inputValue];
        if (event.target.checked) {
          arr.push(value);
        } else {
          arr.splice(arr.indexOf(value), 1);
        }
        return onChange(arr);
      };
      const checked = inputValue.includes(value);

      return (
        <label key={`checkbox-${index}`}>
          <input
            type="checkbox"
            name={`${name}[${index}]`}
            value={value}
            checked={checked}
            onChange={handleChange}
          />
          <span>{label}</span>
        </label>
      );
    });

    return (
      <div>
        <div>{checkboxes}</div>
        {touched && error && <p className="error">{error}</p>}
      </div>
    );
  };

  render() {
    return <Field {...this.props} type="checkbox" component={this.field} />;
  }
}
