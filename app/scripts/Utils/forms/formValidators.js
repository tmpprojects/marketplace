import { containsEmoji } from '../genericUtils';

export const required = (value) => (value ? undefined : 'Campo obligatorio');
export const requiredGift = (value) =>
  value ? undefined : 'Datos del destinatario';

export const minLength = (min) => (value) =>
  value && value.length < min ? `Mínimo ${min} caracteres.` : undefined;
export const maxLength = (max) => (value) =>
  value && value.length > max ? `Máximo ${max} o menos caracteres.` : undefined;
export const numberChracters = (chractersRequired, pronoun, type) => (value) =>
  value.length === chractersRequired
    ? undefined
    : `${pronoun} ${type} debe contener ${chractersRequired} digitos`;

export const charactersCLABE = numberChracters(18, 'La', 'CLABE');
export const charactersDebit = numberChracters(16, 'La', 'Tarjeta de debito');
export const maxLength13 = maxLength(13);
export const minLength2 = minLength(2);
export const minLength3 = minLength(3);
export const minLength8 = minLength(8);
export const maxLength4 = maxLength(14);
export const maxLength15 = maxLength(15);
export const maxLength20 = maxLength(20);
export const maxLength50 = maxLength(50);
export const maxLength140 = maxLength(140);
export const maxLength500 = maxLength(500);
export const number = (value) =>
  value && isNaN(Number(value)) ? 'Debe ser un número.' : undefined;
export const minValue = (min) => (value) =>
  value && value < min ? `Mínimo ${min} caracteres.` : undefined;
export const minValue18 = minValue(18);
export const minValue8 = minValue(8);
export const minValue2 = minValue(2);

export const email = (value) =>
  value && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value)
    ? 'Email inválido'
    : undefined;
export const storeReferrerCode = (value) =>
  value && /[^A-Za-z0-9-]/gi.test(value) ? 'Caracteres no válidos' : undefined;
export const password_strong = (value) =>
  value && /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{8,}$/.test(value)
    ? 'Porfavor elige una contraseña más segura'
    : undefined;
export const password = (value) =>
  value && !/^[0-9a-zA-Z._-]{8,15}$/.test(value)
    ? 'Elige entre 8 y 15 caracteres alfanuméricos.'
    : undefined;
export const url = (value) =>
  value &&
  !/https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/.test(
    value,
  )
    ? 'Porfavor escriba una URL válida'
    : undefined;

export const validateCreditCardForm = (values) => {
  const errors = {};
  if (!values) {
    errors.emptyForm = 'Form must not be empty';
    return errors;
  }

  if (!values['cc-name']) {
    errors['cc-name'] = 'Introduce un nombre.';
  } else if (values['cc-name'].length < 3) {
    errors['cc-name'] = 'Debes introducir más de 2 caracteres en este campo.';
  } else if (containsEmoji(values['cc-name'])) {
    errors['cc-name'] = 'Por favor introduce caracteres válidos en este campo.';
  }

  if (!values['cc-number']) {
    errors['cc-number'] = 'Introduce un número de tarjeta.';
  } else if (values['cc-number'].length < 16) {
    errors['cc-number'] = 'Introduce un número de tarjeta válido.';
  }

  if (!values['cc-exp']) {
    errors['cc-exp'] = 'Introduce una fecha.';
  } else if (values['cc-exp'].length < 7) {
    errors['cc-exp'] = 'Introduce una fecha válida. ';
  }

  if (!values['cc-cvc']) {
    errors['cc-cvc'] = 'Introduce un código de tarjeta. ';
  } else if (values['cc-cvc'].length < 3) {
    errors['cc-cvc'] = 'Introduce un código de tarjeta válido. ';
  }

  if (!values['issuerList']) {
    errors['issuerList'] = 'Selecciona un emisor. ';
  }
  return errors;
};
