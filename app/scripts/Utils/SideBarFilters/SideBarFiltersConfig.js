const SideBarFiltersFormName = 'sidebarFilters_form';
export const SIDEBAR_FILTERS_FORM_CONFIG = {
  formName: SideBarFiltersFormName,
  config: {
    form: SideBarFiltersFormName,
    //keepDirtyOnReinitialize: true,
    enableReinitialize: true, // <---- Allow updates when 'initValues' prop change
    destroyOnUnmount: true, // <---- Preserve data. Dont destroy form on unmount
  },
};

export const DEFAULT_FILTERS = {
  delivery_day: 'all',
  delivery_day_picked: null,
  shipping: 'shipping_all',
  price: '',
  stores: [],
};
