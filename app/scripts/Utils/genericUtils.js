/**
 * range()
 * @param {int} start : Beginning index
 * @param {int} stop : Ending index
 * @param {int} step : Flagged int indicating sequence steps
 * @returns {array} : An array with resulting range sequence
 */
export const range = (start, stop, step = 1) => {
  if (typeof stop === 'undefined') {
    stop = start;
    start = 0;
  }
  if (typeof step === 'undefined') {
    step = 1;
  }
  if ((step > 0 && start >= stop) || (step < 0 && start <= stop)) {
    return [];
  }

  const result = [];
  for (let i = start; step > 0 ? i <= stop : i >= stop; i += step) {
    result.push(i);
  }

  return result;
};

export const containsEmoji = (string) => {
  const regexDetectEmoji = RegExp(
    /(?:[\u2700-\u27bf]|(?:\ud83c[\udde6-\uddff]){2}|[\ud800-\udbff][\udc00-\udfff]|[\u0023-\u0039]\ufe0f?\u20e3|\u3299|\u3297|\u303d|\u3030|\u24c2|\ud83c[\udd70-\udd71]|\ud83c[\udd7e-\udd7f]|\ud83c\udd8e|\ud83c[\udd91-\udd9a]|\ud83c[\udde6-\uddff]|\ud83c[\ude01-\ude02]|\ud83c\ude1a|\ud83c\ude2f|\ud83c[\ude32-\ude3a]|\ud83c[\ude50-\ude51]|\u203c|\u2049|[\u25aa-\u25ab]|\u25b6|\u25c0|[\u25fb-\u25fe]|\u00a9|\u00ae|\u2122|\u2139|\ud83c\udc04|[\u2600-\u26FF]|\u2b05|\u2b06|\u2b07|\u2b1b|\u2b1c|\u2b50|\u2b55|\u231a|\u231b|\u2328|\u23cf|[\u23e9-\u23f3]|[\u23f8-\u23fa]|\ud83c\udccf|\u2934|\u2935|[\u2190-\u21ff])/g,
  );
  if (regexDetectEmoji.test(string)) {
    return true;
  } else {
    return false;
  }
};
export const checkOnlineStatus = async () => {
  try {
    const online = await fetch(
      'https://canastarosa.s3.us-east-2.amazonaws.com/json-services/PaymentErrors.json', // this is a resource that must be always availabe
      {
        mode: 'no-cors',
      },
    );
    if (online) return true;
  } catch (err) {
    return false;
  }
};

export const upperCaseEachWord = (str) => {
  let splitStr = str.toLowerCase().split(' ');
  console.log('splitStr', splitStr);
  for (let i = 0; i < splitStr.length; i++) {
    // Assign it back to the array
    splitStr[i] = splitStr[i].charAt(0).toUpperCase() + splitStr[i].substring(1);
  }
  console.log('join', splitStr.join(' '));
  // Directly return the joined string
  return splitStr.join(' ');
};
