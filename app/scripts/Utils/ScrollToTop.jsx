import React, { Component } from 'react';
import { withRouter } from 'react-router';

class ScrollToTop extends Component {
  componentDidUpdate(prevProps) {
    const { location } = this.props;
    const { state = undefined } = location;
    let scrollControl = true;
    let scrollYPosition = 0;

    // Config and Options
    if (state && state.hasOwnProperty('scrollToTop')) {
      scrollControl = state.scrollToTop;
    }
    if (state && state.hasOwnProperty('scrollToPosition')) {
      scrollControl = true;
      scrollYPosition = state.scrollToPosition;
    }
    // TODO: Inmplement this functionality
    if (state && state.hasOwnProperty('scrollToElement')) {
      scrollControl = true;
      //scrollYPosition = state.scrollToTop;
    }

    // If location has changed
    // and scroll control is active
    if (location !== prevProps.location && scrollControl !== false) {
      window.scrollTo(0, scrollYPosition);
    }
  }

  render() {
    return this.props.children;
  }
}
export default withRouter(ScrollToTop);
