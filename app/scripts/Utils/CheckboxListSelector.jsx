import React, { Component } from 'react';
import PropTypes from 'prop-types';

/*---------------------------------------------------
    CHECKBOX LIST SELECTOR UI COMPONENT
---------------------------------------------------*/
export default class CheckboxListSelector extends Component {
  // Static Properties Definition
  static propTypes = {
    open: PropTypes.bool,
    title: PropTypes.string,
    multipleChoice: PropTypes.bool,
    initialValues: PropTypes.arrayOf(
      PropTypes.shape({
        name: PropTypes.string.isRequired,
        value: PropTypes.any.isRequired,
      }),
    ),
    options: PropTypes.arrayOf(
      PropTypes.shape({
        name: PropTypes.string.isRequired,
        value: PropTypes.any.isRequired,
      }),
    ).isRequired,
    onOpen: PropTypes.func.isRequired,
    onClose: PropTypes.func.isRequired,
    onRemove: PropTypes.func.isRequired,
    onChange: PropTypes.func.isRequired,
    onSubmit: PropTypes.func.isRequired,
  };
  static defaultProps = {
    open: false,
    multipleChoice: true,
    title: 'Elige una opción.',
    initialValues: [],
    onOpen: () => false,
    onClose: () => false,
    onRemove: () => false,
    onChange: (options) => options,
  };

  /**
   * constructor()
   * @param {object} props : Component properties
   */
  constructor(props) {
    super(props);
    this.state = {
      isOpen: props.open,
      selectedOptions: this.props.initialValues,
    };
    this.ID = this.generateID();
  }
  componentDidUpdate(prevProps, prevState) {
    // Activate listeners if selector list is open.
    if (prevState.isOpen !== this.state.isOpen) {
      if (this.state.isOpen) {
        this.addListeners();
      } else {
        this.removeListeners();
      }
    }
  }
  componentWillUnmount() {
    // Remove all DOM listeners
    this.removeListeners();
  }

  /**
   * onOptionSelected()
   * Handler for 'onChange' events on component´s (check-)list options
   * @param {object} element : Form event
   */
  onOptionSelected = (element) => {
    const { value } = element;
    const { multipleChoice } = this.props;
    let selectedOptions = this.state.selectedOptions;

    // Attempt to find the current selected option on the list.
    // We cast 'values' as Strings so we ensure that value types are the same
    const foundIndex = selectedOptions.findIndex(
      (option) => String(option.value) === String(value),
    );

    // If the component is in 'mulptiple-choice' mode,
    // Create a lis of choices.
    if (multipleChoice) {
      // If option was not found, add the current option to the list
      if (foundIndex === -1) {
        selectedOptions = [
          ...selectedOptions,
          this.props.options.find((a) => String(a.value) === String(value)),
        ];
        // ...else, remove it from the list
      } else {
        selectedOptions = selectedOptions.filter(
          (a) => String(a.value) !== String(value),
        );
      }

      // Else, replace current selected option.
    } else {
      selectedOptions = [
        this.props.options.find((a) => String(a.value) === String(value)),
      ];
    }

    // Sort options by logic defined on parent (optional)
    selectedOptions = this.props.onChange(selectedOptions);

    // Update component state
    this.setState(
      { selectedOptions },
      // Save settings
      () => this.saveSettings(),
    );
  };

  /**
   * onCheckboxRemove()
   * Execute parent callback to remove this component.
   * @param {object} e : Click Event
   */
  onCheckboxRemove = (e) => {
    // Invoke parent callback and send component´s ID
    this.props.onRemove(this.ID);
  };

  /**
   * onClickOutsideList()
   * Closes the selector list
   * if users click outside the component
   */
  onClickOutsideList = (e) => {
    this.closeWindow();
    this.removeListeners();
  };

  /**
   * closeWindow()
   * Execute parent´s callback
   */
  closeWindow = () => {
    this.toggleList(false);
  };

  /**
   * onSave()
   * Save user selected options and execute parent´s callback
   */
  saveSettings = () => {
    // Execute parent´s callback and send selected options
    this.props.onSubmit(this.state.selectedOptions);
  };

  /**
   * toggleList()
   * Toggles (open/closed) options list state.
   * @param {bool} force : Force the component to a fixed state
   */
  toggleList = (force) => {
    const isOpen = force !== undefined ? force : !this.state.isOpen;
    this.setState({
      isOpen,
    });
  };

  /**
   * generateID()
   * generates a random ID to identify this component
   * @returns {string} id
   */
  generateID = () => {
    function s4() {
      return Math.floor((1 + Math.random()) * 0x10000)
        .toString(16)
        .substring(1);
    }
    return `${s4()}${s4()}-${s4()}${s4()}`;
  };

  /**
   * preventEventBubbling()
   * Prevents the selector list from closing
   * if user clicks inside the component
   */
  preventEventBubbling = (e) => {
    const checkbox = e.currentTarget.querySelector('input[type=checkbox]');
    //checkbox.checked = !checkbox.checked;
    //this.onOptionSelected(checkbox);
    //e.stopPropagation()
  };

  /**
   * removeListeners()
   * Remove all DOM event listeners
   */
  removeListeners = () => {
    document.body.removeEventListener('click', this.onClickOutsideList);

    // Options listeners
    [].slice
      .call(document.querySelectorAll('.ui-checkbox-list .options__item'))
      .forEach((option) => {
        option.removeEventListener('click', this.preventEventBubbling, false);
      });
  };

  /**
   * addListeners()
   * Add listeners to window and selector list options
   */
  addListeners = () => {
    // Body listener
    document.body.addEventListener('click', this.onClickOutsideList, false);

    // Add click listener to al list options
    [].slice
      .call(document.querySelectorAll('.ui-checkbox-list .options__item'))
      .forEach((option) => {
        option.addEventListener('click', this.preventEventBubbling, false);
      });
  };

  /**
   * renderOption()
   * Renders a single option for the 'options' list.
   * @param {object} option : Option to render
   * @param {int} id : Random integer
   */
  renderOption = (option, id) => {
    const optionID = `${this.ID}_${id}_options`;
    return (
      <div key={id} className="options__item">
        <input
          id={optionID}
          name={optionID}
          type="checkbox"
          value={option.value}
          onChange={(e) => this.onOptionSelected(e.target)}
          checked={
            this.state.selectedOptions.findIndex((o) => o.value === option.value) !==
            -1
          }
        />
        <label className="checkmark" htmlFor={optionID}>
          {option.name}
        </label>
      </div>
    );
  };

  /**
   * render()
   */
  render() {
    const { title, options } = this.props;
    const { isOpen, selectedOptions } = this.state;

    return (
      <div className="ui-checkbox-list">
        <div
          className="ui-checkbox-list__selector"
          onClick={(e) => this.toggleList()}
        >
          {selectedOptions.length ? selectedOptions[0].name : title}
        </div>
        <ul
          style={{ display: isOpen ? 'block' : 'none' }}
          className="ui-checkbox-list__options options"
        >
          {options.map((option, index) => this.renderOption(option, index))}
        </ul>
      </div>
    );
  }
}
