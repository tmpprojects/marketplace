import React from 'react';
import { Component } from 'react';
import { Link, Redirect } from 'react-router-dom';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Field, reduxForm, SubmissionError } from 'redux-form';

import { required, email, password } from './forms/formValidators';
import { userLoginErrorTypes } from '../Constants/user.constants';
import { authenticationActions, userActions, modalBoxActions } from '../Actions';
import { getUIErrorMessage } from '../Utils/errorUtils';
import { normalizeToLowerCaseAndTrim } from './forms/formNormalizers';
import { IconPreloader } from '../Utils/Preloaders';
import { ActionButton } from './ActionButton';
import config from '../../config';
import Register from '../Utils/Register';
import Password from '../Utils/Password';

/*
 * NORMALIZE CREDENTIALS
 * @param {object} props : Field Properties
 */
const cleanPassword = (value, previousValue) => {
  return value.trim();
};

/*
 * Render Custom Form Fields
 * @param {object} props : Field Properties
 */
const renderField = ({
  input,
  label,
  type,
  autoFocus,
  meta: { touched, error, warning },
}) => (
  <div className="form__data">
    <input {...input} placeholder={label} type={type} autoFocus={autoFocus} />
    {touched &&
      ((error && <div className="form_status danger">{error}</div>) ||
        (warning && <div className="form_status warning">{warning}</div>))}
  </div>
);

/*
 * React Login component
 */
class Login extends Component {
  constructor(props) {
    super(props);

    // Initial State
    this.state = {
      success: false,
    };

    // Bind Methods
    this.formSubmit = this.formSubmit.bind(this);
  }

  /*
   * FACEBOOK LOGIN CALLBACK
   * @param {object} response : Server Response from Facebook
   */
  loginCallback(response) {
    if (response.authResponse) {
      const accessToken = { access_token: response.authResponse.accessToken };
      this.props
        .socialLogin(accessToken, 'facebook')
        .then((response) => window.location.reload()) // window.location = config.login_redirect_url)
        .catch((error) => {});
    } else {
      console.log(
        'Facebook Login: User cancelled login or did not fully authorize.',
      );
    }
  }

  /*
   * FORM FUNCTIONS
   * @param {objecty} formData : Data of the Redux-Form
   */
  formSubmit(formData) {
    const { email: mail, password: pwd } = formData;

    // Try to log user in
    return this.props
      .login(mail, pwd)
      .then((response) => {
        this.setState({
          success: true,
        });
        this.props.closeModalBox();
      })
      .catch((error) => {
        //'Nombre de usuario o contraseña inválidos.'
        const uiMessage =
          error.response.data.non_field_errors ||
          getUIErrorMessage(error.response, userLoginErrorTypes);
        throw new SubmissionError({
          _error: uiMessage,
        });
      });
  }

  render() {
    const { handleSubmit, submitting, error } = this.props;
    const { success } = this.state;

    return (
      <div>
        <div className="modalLogin">
          <div className="title">
            <h4>Inicia sesión</h4>
            <a href="#" onClick={() => this.props.openModalBox(Register)}>
              Crea una cuenta
            </a>
          </div>

          <div className="modalLoginInputs">
            <form
              className="form_color"
              name="loginForm"
              onSubmit={handleSubmit(this.formSubmit)}
            >
              <Field
                autoFocus
                name="email"
                component={renderField}
                label="Correo"
                type="text"
                normalize={normalizeToLowerCaseAndTrim}
                validate={[required, email]}
                placeholder="Correo"
              />

              <Field
                name="password"
                component={renderField}
                label="Contraseña"
                type="password"
                normalize={cleanPassword}
                validate={[required, password]}
                placeholder="Contraseña"
              />

              {success && window.location.reload()}
              {error && <div className="form_status danger big">{error}</div>}
              {submitting && <IconPreloader />}

              <div className="form__data">
                <input
                  value="Entrar"
                  type="submit"
                  name="login_submit"
                  placeholder="Entrar"
                  disabled={submitting}
                />
              </div>
            </form>

            <span>O</span>

            <div className="form_register__facebook">
              <div
                className="login_button"
                onClick={() => {
                  FB.login(
                    (response) => {
                      if (response.authResponse) {
                        this.loginCallback(response);
                      }
                    },
                    { scope: config.facebookAuth.profileFields.join() },
                  );
                }}
              >
                <span>Contin&uacute;a con Facebook</span>
              </div>
            </div>
          </div>

          <div className="footer_form">
            <a
              href="#"
              onClick={(e) => {
                e.preventDefault();
                this.props.openModalBox(Password);
              }}
            >
              ¿Olvidaste tu contraseña?
            </a>
          </div>
        </div>
      </div>
    );
  }
}

// Wrap component within reduxForm
Login = reduxForm({
  form: 'login_form',
})(Login);

// Redux Map Functions
function mapStateToProps({ auth }) {
  const { loggingIn } = auth;
  return {
    loggingIn,
  };
}
function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      login: userActions.login,
      socialLogin: authenticationActions.socialLogin,
      openModalBox: modalBoxActions.open,
      closeModalBox: modalBoxActions.close,
    },
    dispatch,
  );
}

// Export Connected Component
Login = connect(mapStateToProps, mapDispatchToProps)(Login);
export default Login;
