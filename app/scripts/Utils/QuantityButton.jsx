import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { statusWindowActions } from '../Actions';

let timeDemo;
export class QuantityButton extends React.Component {
  static propTypes = {
    quantity: PropTypes.number.isRequired,
    maxQuantity: PropTypes.number.isRequired,
    onQuantityChange: PropTypes.func.isRequired,
  };

  //Put required props
  constructor(props) {
    super(props);
    this.quantityInput = React.createRef();
    this.state = {
      selectedQuantity: this.props.quantity,
      maxQuantity: this.props.maxQuantity,
    };
  }
  componentWillReceiveProps(nextProps) {
    if (this.props.maxQuantity !== nextProps?.maxQuantity) {
      this.setState({ maxQuantity: nextProps?.maxQuantity });
    }
  }

  handleClick = (e) => {
    if (e) {
      e.preventDefault();
      const { name } = e.target;
      const { selectedQuantity, maxQuantity } = this.state;
      let newQuantity = selectedQuantity;

      switch (name) {
        case 'plus':
          if (selectedQuantity < maxQuantity) {
            newQuantity++;
          } else {
            this.props.openStatusWindow({
              type: 'error',
              message: `La cantidad máxima de este producto es ${maxQuantity}.`,
            });
          }
          break;
        case 'minus':
          if (selectedQuantity !== 1) {
            newQuantity--;
          } else {
            this.props.openStatusWindow({
              type: 'error',
              message: 'La cantidad mínima por producto es 1.',
            });
          }
          break;
        default:
          break;
      }

      //Update selectedQuantity state
      this.setState({ selectedQuantity: newQuantity });

      //Change value of input
      this.quantityInput.current.value = newQuantity;
      //Set timer to change update quantity in parent component, so
      //we can get only the last value
      clearInterval(timeDemo);
      timeDemo = window.setTimeout(() => {
        this.props.onQuantityChange(newQuantity);
      }, 300);
      // return newQuantity;
    }
  };

  handleKeyUp = (e) => {
    e.preventDefault();
    const { value } = e.target;
    const { maxQuantity } = this.state;
    let newValue = value;
    const reg = /^\d+$/;
    let message = 'Por favor, escribe un número válido.';
    const isNumber = reg.test(newValue);
    const isAboveMaximum = newValue > maxQuantity;

    //Change message if is the case
    if (isAboveMaximum) {
      message = `Por favor, elige una cantidad menor a ${maxQuantity}.`;
    }
    if (newValue === '' || newValue == 0) {
      message = 'La cantidad mínima por producto es 1.';
    }

    //If one of these errors
    if (!isNumber || isAboveMaximum || newValue === '' || newValue == 0) {
      //Change value of input to ''
      e.target.value = '';
      //Send error message
      this.props.openStatusWindow({
        type: 'error',
        message,
      });

      //Update selectedQuantity state to minimum and change form
      this.setState({ selectedQuantity: 1 });
      clearInterval(timeDemo);
      timeDemo = window.setTimeout(() => {
        this.props.onQuantityChange(1);
      }, 300);
    } else {
      newValue = parseInt(newValue, 10);
      //If value is correct , update selectedQuantity state
      this.setState({ selectedQuantity: newValue });
      //Set timer to change update quantity in parent component, so
      //we can get only the last value
      clearInterval(timeDemo);
      timeDemo = window.setTimeout(() => {
        this.props.onQuantityChange(newValue);
      }, 300);
    }
  };

  render() {
    const { selectedQuantity, maxQuantity } = this.state;

    return (
      <div className="quantity_button">
        <button
          name="minus"
          id="minus"
          className={selectedQuantity > 1 ? 'in-stock' : ''}
          onClick={this.handleClick}
        >
          -
        </button>
        <input
          id="quantityInput"
          ref={this.quantityInput}
          name="selectedQuantity"
          defaultValue={selectedQuantity}
          placeholder={1}
          onKeyUp={this.handleKeyUp}
        />
        <button
          name="plus"
          id="plus"
          className={selectedQuantity < maxQuantity ? 'in-stock' : ''}
          onClick={this.handleClick}
        >
          +
        </button>
      </div>
    );
  }
}

// Add Redux state and actions to component´s props
function mapStateToProps() {
  return {};
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      openStatusWindow: statusWindowActions.open,
    },
    dispatch,
  );
}

// Export Component
// StoreGalleryForm = connect(mapStateToProps, mapDispatchToProps)(StoreGalleryForm);
// export default StoreGalleryForm;
export default connect(mapStateToProps, mapDispatchToProps)(QuantityButton);
