import React from 'react';
import { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Field, reduxForm, SubmissionError } from 'redux-form';
import { userActions, modalBoxActions } from '../Actions';

/*
 * Form Validation Function
 * @param {object} values : Form values
 */
const validate = (values) => {
  const errors = {};

  // Email
  if (!values.email) {
    errors.email = 'Escribe tu email';
  } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) {
    errors.email = 'Escribe un email válido.';
  }

  // Return errors
  return errors;
};

/*
 * Form Warnings Function
 * @param {object} values : Form values
 */
const warn = (values) => {
  const warnings = {};
  return warnings;
};

/*
 * Redner Custom Form Fields
 * @param {object} props : Field Properties
 */
const renderField = ({
  input,
  label,
  type,
  autoFocus,
  placeholder,
  fieldName,
  meta: { touched, error, warning },
}) => (
  <div className={`form__data form__data-${fieldName}`}>
    <input {...input} placeholder={placeholder} type={type} autoFocus={autoFocus} />
    {touched &&
      ((error && <div className="form_status danger">{error}</div>) ||
        (warning && <div className="form_status warning">{warning}</div>))}
  </div>
);

/*
 * React Password Recovery component
 */
class Password extends Component {
  constructor(props) {
    super(props);
    this.state = {
      success: false,
    };

    // Bind scope to methods
    this.formSubmit = this.formSubmit.bind(this);
  }

  /*
   * FORM FUNCTIONS
   * @param {objecty} formData : Data of the Redux-Form
   */
  formSubmit(formData) {
    // Recover Password
    return this.props
      .passwordRecover(formData)
      .then((response) => {
        this.setState({
          success: true,
        });
      })
      .catch((error) => {
        throw new SubmissionError({
          _error: 'Ya existe un usuario con este email.',
        });
      });
  }

  /*
   * React Component Life Cycle Functions
   */
  render() {
    const { handleSubmit, pristine, submitting, error } = this.props;
    const { success } = this.state;

    if (!success) {
      return (
        <div>
          <div className="modalPasswordForget">
            <div className="title">
              <h4>Restablecer contraseña</h4>
            </div>
            <div className="form_password" onSubmit={handleSubmit(this.formSubmit)}>
              <p>
                Ingresa la dirección de email con que te registrate y recibirás un
                correo con instrucciones para restablecer tu contraseña.
              </p>
              <form className="form_color">
                <fieldset>
                  <Field
                    autoFocus
                    name="email"
                    component={renderField}
                    label="Email"
                    type="text"
                    placeholder="Email"
                  />

                  <div className="form__data form__data-submit">
                    <input
                      type="submit"
                      name="register_submit"
                      placeholder="Enviar"
                      disabled={pristine || submitting}
                    />
                  </div>
                </fieldset>
              </form>
            </div>
          </div>
        </div>
      );
    } else {
      return (
        <div className="modalPasswordForget_message">
          <div className="accountConfirm">
            <h2>¡Hey!</h2>
            <p>
              Te hemos enviado un <span>correo electrónico </span> con instrucciones
              para <span>recuperar tu contraseña</span>.{' '}
            </p>
            <a className="c2a_square" onClick={this.props.closeModal}>
              Ok
            </a>
          </div>
        </div>
      );
    }
  }
}

// Wrap component within reduxForm
Password = reduxForm({
  form: 'password_recover',
  validate,
  warn,
})(Password);

// Redux Map Functions
function mapStateToProps(state) {
  return {};
}
function mapDispatchToProps(dispatch) {
  const { passwordRecover } = userActions;
  return bindActionCreators(
    {
      passwordRecover,
      closeModal: modalBoxActions.close,
    },
    dispatch,
  );
}

// Export Connected Component
Password = connect(mapStateToProps, mapDispatchToProps)(Password);
export default Password;
