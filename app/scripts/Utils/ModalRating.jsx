import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import queryString from 'query-string';

import { modalBoxActions, userActions } from '../Actions';
import Rating from '../components/Rating/Rating';
import { Field, reduxForm, getFormValues, getFormSyncErrors } from 'redux-form';
import { InputField, TextArea } from '../Utils/forms/formComponents';
import {
  required,
  maxLength50,
  maxLength500,
  minLength2,
} from '../Utils/forms/formValidators';
import { ActionButton } from '../Utils/ActionButton';

const RatingScore = ({ topic, rating, onClick, meta: { error }, showError }) => (
  <React.Fragment>
    <Rating
      interactive
      rating={rating}
      onClick={(e, value) => onClick(e, value, topic)}
    />
    {showError && error && (
      <div className="rating_status form_status danger">{error}</div>
    )}
  </React.Fragment>
);

class ModalRating extends Component {
  // static propTypes = {
  //     productPurchase: PropTypes.object.isRequired,
  //     title: PropTypes.string,
  //     comment: PropTypes.string,
  //     logisticScore: PropTypes.number,
  //     productScore: PropTypes.number
  // }
  // static defaultProps = {
  //     title: '',
  //     comment: '',
  //     logisticScore: 0,
  //     productScore: 0
  // }
  constructor(props) {
    super(props);
    this.state = {
      submit: false,
      title: props.title,
      comment: props.comment,
      logisticScore: props.logisticScore,
      productScore: props.productScore,
      showError: false,
    };
  }

  componentDidMount() {
    const { productScore } = this.props;

    if (productScore) {
      this.props.change('product_score', productScore);
    }
  }
  /**
   * componentWillReceiveProps()
   */
  componentWillReceiveProps(nextState) {
    if (nextState.productScore !== this.props.productScore) {
      this.setState({ productScore: nextState.productScore });
    }
    if (nextState.logisticScore !== this.props.logisticScore) {
      this.setState({ logisticScore: nextState.logisticScore });
    }
  }

  /**
   * onRatingChange()
   * Update Rating Values State.
   * @param {obj} e | Mouse event
   * @param {int} rating | Selected Rating Value
   * @param {string} topic | Rating Subject
   */
  onRatingChange = (e, rating, topic) => {
    switch (topic) {
      case 'product':
        this.props.change('product_score', rating);
        break;
      case 'logistics':
        this.props.change('logistics_score', rating);
        break;
      default:
        break;
    }
  };

  /**
   * submitForm()
   * Submits the user review to the database.
   * Prevent form from submitting (default behaviour).
   * @param {obj} e | Form event
   */
  submitForm = (e) => {
    const { productPurchase, formValues, formErrors, page } = this.props;

    const { title, comment, product_score, logistics_score } = formValues;

    if (Object.keys(formErrors).length !== 0) {
      return this.setState({ showError: true });
    }
    // Send Review to DB
    this.props
      .submitProductReview({
        ...productPurchase,
        title,
        comment,
        product_score,
        logistics_score,
        order_product: productPurchase.id,
      })
      .then((response) => {
        //Informs the parent component that there was a submit so the parent re-render the reviews
        this.props.updateReviews();
        this.setState({ submit: true });
      })
      .catch((error) => {
        console.log('Error submitting review: ', error);
      });
  };

  /**
   * updateValue()
   * Save user input on component´s state (state controlled form)
   * @param {obj} e | Form event
   */
  updateValue = (e) => {
    e.preventDefault();
    const { name, value } = e.target;
    this.setState({
      [name]: value,
    });
  };

  /**
   * showReviewForm()
   * @returns JSX | User review form
   */
  showReviewForm = () => {
    const { productPurchase, formValues } = this.props;

    const { showError } = this.state;
    if (!formValues) {
      return null;
    }

    const {
      product_score: productScore,
      logistics_score: logisticsScore,
    } = formValues;

    return (
      <div className="modalRating">
        <h4 className="review-container--title">
          ¿Qué opinas de{' '}
          <span className="product-name">{productPurchase.product.name}</span>?
        </h4>
        <div className="review-container--content">
          Tus comentarios ayudan a otros compradores a encontrar los mejores
          artículos y a las tiendas a mejorar sus productos.
        </div>
        <div className="face_rating" />

        <div className="rating-container">
          <span className="label">¿Cómo calificas este producto?</span>
          <Field
            component={RatingScore}
            rating={productScore}
            topic="product"
            onClick={this.onRatingChange}
            name="product_score"
            validate={[required]}
            showError={showError}
          />
        </div>

        <div className="review-container">
          <form className="form">
            <span className="label">Cuéntanos más de tu experiencia:</span>
            <fieldset>
              <Field
                component={InputField}
                name="title"
                className="title-review"
                placeholder="Título de tu reseña"
                validate={[required, minLength2, maxLength50]}
              />
              <Field
                name="comment"
                className="comment-review"
                component={TextArea}
                placeholder="Qué te pareció este producto? ¿Lo disfrutaste? ¿Qué podría mejorar?"
                cols="40"
                rows="8"
                validate={[required, minLength2, maxLength500]}
              />
            </fieldset>

            <div className="rating-container logistics">
              <span className="label">
                ¿Cómo calificas la logística de Canasta Rosa?
              </span>
              <Field
                component={RatingScore}
                rating={logisticsScore}
                topic="logistics"
                onClick={this.onRatingChange}
                name="logistics_score"
                validate={[required]}
                showError={showError}
              />
            </div>
            <ActionButton
              icon="loading"
              className="c2a_square submit"
              onClick={this.submitForm}
            >
              Enviar Reseña
            </ActionButton>
          </form>
        </div>
      </div>
    );
  };

  /**
   * showThankYouPage()
   * @returns JSX | Thank you / Confirmation page
   */
  showThankYouPage = () => {
    return (
      <div className="modalRating-message">
        <div className="message">
          <h4 className="message--title">¡Eres increíble!</h4>
          {/*
                        <h5 className="message--subtitle">Gracias por tu reseña</h5>
                    */}
          <p className="message--content">
            Estamos procesando tu reseña.
            <br />
            Te notificaremos en el momento que haya sido publicada.
          </p>
        </div>
        <a className="button c2a_square" onClick={this.props.closeModalBox}>
          Ok
        </a>
      </div>
    );
  };

  /**
   * render()
   */
  render() {
    // Show form if user review has not already been submitted
    if (!this.state.submit) {
      return this.showReviewForm();
    }

    // Thankyou Page
    return this.showThankYouPage();
  }
}

// Decorate the form component
ModalRating = reduxForm({
  form: 'Rating_form',
  destroyOnUnmount: true, // <---- Preserve data. Dont destroy form on unmount
})(ModalRating);

// Redux Map Functions
function mapStateToProps(state) {
  return {
    initialValues: {
      title: '',
      comment: '',
      product_score: 0,
      logistics_score: 0,
    },
    formValues: getFormValues('Rating_form')(state),
    formErrors: getFormSyncErrors('Rating_form')(state),
  };
}
function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      openModalBox: modalBoxActions.open,
      closeModalBox: modalBoxActions.close,
      submitProductReview: userActions.addReview,
      getUserReviews: userActions.getUserReviews,
    },
    dispatch,
  );
}

// Export Connected Component
export default connect(mapStateToProps, mapDispatchToProps)(ModalRating);
