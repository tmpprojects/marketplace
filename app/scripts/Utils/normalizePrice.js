export const formatNumberToPrice = (number = 0) => {
  const currentPrice = number?.toString()?.split('.');

  const cents =
    currentPrice?.length > 1 ? currentPrice[1]?.replace(/[^\d]/g, '') : '00';
  const intPrice = currentPrice[0]?.replace(/[^\d]/g, '');
  const formattedPrice = intPrice?.replace(/./g, (c, i, a) =>
    i && (a?.length - i) % 3 === 0 ? ',' + c : c,
  );

  return `\$${formattedPrice}.${cents}`;
};

export const formatNumberToPriceMyStore = (number) => {
  const currentPrice = number?.toString()?.split('.');

  const cents =
    currentPrice?.length > 1 ? currentPrice[1]?.replace(/[^\d]/g, '') : '00';
  const intPrice = currentPrice[0]?.replace(/[^\d]/g, '');
  // const formattedPrice = intPrice?.replace(/./g, (c, i, a) =>
  //   i && (a?.length - i) % 3 === 0 ? '' + c : c,
  // );

  return `${intPrice === '' || intPrice == 0 ? '1' : intPrice}.${cents}`;
};

export const formatPriceToNumber = (price) => {
  const totalPrice = price?.toString()?.split('.');
  const cents = totalPrice?.length > 1 ? totalPrice[1]?.replace(/[^\d]/g, '') : '00';
  const intPrice = totalPrice[0]?.replace(/[^\d]/g, '');
  return parseInt(`${intPrice}.${cents}`);
};
