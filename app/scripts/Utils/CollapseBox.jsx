import React, { Component } from 'react';
import PropTypes from 'prop-types';

export default class CollapseBox extends Component {
  static propTypes = {
    open: PropTypes.bool.isRequired,
    header: PropTypes.oneOfType([PropTypes.string, PropTypes.object]).isRequired,
    content: PropTypes.oneOfType([PropTypes.string, PropTypes.object]).isRequired,
    onSelect: PropTypes.func,
    disabled: PropTypes.bool,
    activeClass: PropTypes.string,
    extraClass: PropTypes.string,
  };
  static defaultProps = {
    open: false,
    onSelect: (status) => status,
    disabled: false,
    activeClass: 'active',
    extraClass: '',
  };
  state = {
    isOpen: this.props.open,
  };
  initContentHeight = 0;
  box = React.createRef();

  /**
   * REACT LIFECYCLE METHODS
   */
  componentDidMount() {
    // Get initial box dimmensions
    this.initContentHeight = this.box.offsetHeight;

    // Close component
    this.toggleBox(this.props.open);
  }
  componentWillReceiveProps(nextProps) {
    if (this.props.open !== nextProps.open) {
      this.toggleBox(nextProps.open);
    }
  }

  /**
   * onSelect()
   * @param {obj} e | Click Event
   */
  onSelect = (e) => {
    this.toggleBox();
  };

  /**
   * toggleBox()
   * Toggles the state of the component (open/close)
   * @param {bool} force : Force component to a specified state (open/close)
   */
  toggleBox = (force = undefined) => {
    const { isOpen } = this.state;
    const shouldOpen = force === undefined ? !isOpen : force;

    // Toggle component
    if (!shouldOpen) {
      this.box.style.maxHeight = '0px';
    } else {
      this.box.style.maxHeight = '10000px';
    }

    // Toggle component
    // if (!shouldOpen) {
    //     const elementTransition = this.box.style.transition;
    //     this.box.style.transition = '';
    //     requestAnimationFrame(() => {
    //         this.box.style.height = `${this.initContentHeight}px`;
    //         this.box.style.transition = elementTransition;
    //         //console.log('shouldOpen ', this.initContentHeight)

    //         // on the next frame have the element transition to height: 0
    //         requestAnimationFrame(() => {
    //             this.box.style.height = `10px`;
    //             console.log('shouldOpen ', this.box.style.height)
    //         });
    //     });
    // //
    // } else {
    //     this.box.style.height = `${this.initContentHeight}px`;

    //     // when the next css transition finishes (which should be the one we just triggered)
    //     const onTransitionend = () => {
    //         // remove event listener and clear inline defined styles
    //         this.box.removeEventListener('transitionend', onTransitionend);
    //         this.box.style.height = null;
    //     };
    //     this.box.addEventListener('transitionend', onTransitionend);
    // }

    // Update component state
    this.setState({ isOpen: shouldOpen });
    this.props.onSelect(shouldOpen);
  };

  /**
   * render()
   * Render component
   */
  render() {
    const { isOpen } = this.state;
    const { header, content, disabled, activeClass, extraClass } = this.props;

    // Return markup
    return (
      <div className="collapseBox">
        <div
          className={`header ${isOpen ? activeClass : ''} ${extraClass}`}
          onClick={(e) => {
            if (!disabled) {
              this.toggleBox();
            }
          }}
        >
          {header}
        </div>

        <div
          ref={(el) => {
            this.box = el;
          }}
          className={`box ${isOpen ? 'open' : ''}`}
        >
          <div className="box__content" children={content} />
        </div>
      </div>
    );
  }
}
