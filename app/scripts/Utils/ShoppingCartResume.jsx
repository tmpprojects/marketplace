import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

import { formatNumberToPrice } from './normalizePrice';
import { getCartProductsCount } from '../Reducers';
import { ResponsiveImage } from '../Utils/ImageComponents';

/**
 * getProductThumbnail()
 * Search for the features image within a list of photos
 * @param {array} gallery : Array of photo objects
 * @return {object} photo§
 */
const getProductThumbnail = (gallery) => {
  const thumbnail = gallery.find((photo) => photo.order === 0);
  if (thumbnail) {
    return thumbnail;
  }
  return gallery[0];
};

/*-------------------------------------------
    PRODUCT THUMBNAILS
-------------------------------------------*/
const Product = (props) => {
  const {
    product: { product: productData, ...productSpecs },
  } = props;
  const product = {
    ...productData,
    ...productSpecs,
    thumbnail: getProductThumbnail(productData.photos) || null,
  };

  return (
    <li className="product-detail sidebar sidebar-resume">
      <div className="thumbnail">
        {product.photos.length > 0 && (
          <Link
            className="image"
            to={`/stores/${product.store.slug}/products/${product.slug}`}
          >
            {product.thumbnail ? (
              <ResponsiveImage src={product.thumbnail.photo} alt={product.name} />
            ) : null}
          </Link>
        )}
      </div>
      <div className="product-detail__box-detail">
        <div className="product-detail__name">{product.name}</div>
        {product.attribute && (
          <div className="attributes">
            {product.attribute.tree_string
              .split(',')
              .map(
                (item) =>
                  `${item.charAt(0).toUpperCase()}${item.slice(1).toLowerCase()}`,
              )
              .join(', ')}
          </div>
        )}
        <div className="price">
          <div className="quantity">
            <label htmlFor="quantity">Cantidad: </label>
            <div className="quantity__select">{product.quantity}</div>
          </div>
          <div className="price__total">
            {formatNumberToPrice(product.unit_price * product.quantity)}MX
          </div>
          {product.quantity > 1 && (
            <div className="price__unit">
              ({formatNumberToPrice(product.unit_price)}MX cada uno)
            </div>
          )}
        </div>
      </div>
    </li>
  );
};

/*-------------------------------------------
    SHOPPING CART RESUME SIDEBAR
-------------------------------------------*/
class ShoppingCartResume extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isOpen: false,
    };
    this.isLoaded = false;
  }
  componentWillReceiveProps(newProps) {
    if (this.props.state !== newProps.state) {
      if (!this.isLoaded) {
        this.isLoaded = true;
      } else {
        // Update State
        this.setState({
          isOpen: newProps.state,
        });
      }
    }
  }
  render() {
    const { cart, cartProductsCount, total_price, onRemoveProduct } = this.props;
    let renderComponent = <p className="note__required">Tu carrito esta vacío</p>;

    if (cart.products.length) {
      renderComponent = (
        <div>
          <ul className="order_container">
            {cart.products.map((item, index) => (
              <Product
                key={`${item.product.slug}-${index}`}
                product={item}
                removeProduct={onRemoveProduct}
              />
            ))}
          </ul>

          <div className="resume-container">
            <p className="cart-resume__breakdown">
              Subtotal&nbsp; <br />
              <span className="quantity">{formatNumberToPrice(total_price)}MX</span>
            </p>
            <p className="note">
              Gastos de env&iacute;o calculados al momento del pago
            </p>
            <Link to="/cart" className="c2a_square">
              Mi carrito
            </Link>
          </div>
        </div>
      );
    }

    // Return Markup
    return (
      <div className={`addToCart ${this.state.isOpen ? 'open' : 'close'}`}>
        <h4>Tu carrito</h4>
        {renderComponent}
      </div>
    );
  }
}
function mapStateToProps(state) {
  return {
    cart: state.cart,
    cartProductsCount: getCartProductsCount(state),
    total_price: state.cart.products.reduce(
      (a, b) => a + parseInt(b.unit_price, 10) * b.quantity,
      0,
    ),
  };
}
export default connect(mapStateToProps)(ShoppingCartResume);
