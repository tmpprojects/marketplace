import React from 'react';
import ReactDOM from 'react-dom';
import scriptLoader from 'react-async-script-loader';
import { IconPreloader } from './Preloaders';

class PaypalButton extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      showButton: false,
      loadingPayment: false,
    };
    window.React = React;
    window.ReactDOM = ReactDOM;
  }
  /**
   * componentDidMount()
   */
  componentDidMount() {
    const { isScriptLoaded, isScriptLoadSucceed } = this.props;

    if (isScriptLoaded && isScriptLoadSucceed) {
      this.setState({ showButton: true });
    }
  }

  /**
   * componentWillReceiveProps
   * @param {*} nextProps
   */
  componentWillReceiveProps(nextProps) {
    const { isScriptLoaded, isScriptLoadSucceed } = nextProps;

    const isLoadedButWasntLoadedBefore =
      !this.state.showButton && !this.props.isScriptLoaded && isScriptLoaded;

    if (isLoadedButWasntLoadedBefore) {
      if (isScriptLoadSucceed) {
        this.setState({ showButton: true });
      }
    }
  }

  /**
   * authorizeHandler()
   * @param {object} data
   */
  authorizeHandler = (data) => {
    // Set up the data you need to pass to your server
    const paymentData = {
      params: {
        paymentId: data.paymentID,
        PayerID: data.payerID,
        token: data.paymentToken,
      },
    };

    // Call Authorize implementation
    this.props.onAuthorize(paymentData).catch((error) => {
      this.setState({
        loadingPayment: true,
      });
    });

    // Update view to show preloader icon
    this.setState({
      loadingPayment: true,
    });
  };

  /**
   * render()
   */
  render() {
    const { env, commit, client, onError, onCancel, payment, style } = this.props;
    const { showButton, loadingPayment } = this.state;

    // Return markup
    return (
      <div>
        {showButton && (
          <React.Fragment>
            <div style={{ display: !loadingPayment ? 'none' : '' }}>
              <IconPreloader />
            </div>
            <div style={{ display: loadingPayment ? 'none' : '' }}>
              <paypal.Button.react
                env={env}
                commit={commit}
                payment={payment}
                onAuthorize={this.authorizeHandler}
                onCancel={onCancel}
                onError={onError}
                style={style}
                client={client}
                productionID={client.production}
                sandboxID={client.sandbox}
              />
            </div>
          </React.Fragment>
        )}
      </div>
    );
  }
}

export default scriptLoader('https://www.paypalobjects.com/api/checkout.js')(
  PaypalButton,
);
