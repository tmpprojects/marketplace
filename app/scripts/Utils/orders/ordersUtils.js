import { ORDER_STATUS, LOCAL_ORDER_STATUS } from '../../Constants/orders.constants';

/**
 * getLocalOrderStatus()
 * Standarize the order status sent by the API
 * to the 'local' app GUI status namespace.
 * @param {string} backendStatus | A string representing the order status sent by the API.
 * @returns string | A string representing the 'local' GUI status.
 */
export const getLocalOrderStatus = (backendStatus) => {
  switch (backendStatus) {
    case ORDER_STATUS.NEW_ORDER:
    case ORDER_STATUS.PREPARING_ORDER:
    case ORDER_STATUS.ORDER_READY:
    case ORDER_STATUS.AWAITING_SHIPMENT:
    case ORDER_STATUS.WAYBILL_GENERATED:
    case ORDER_STATUS.ORDER_IN_TRANSIT:
    case ORDER_STATUS.WAYBILL_GENERATED:
      return LOCAL_ORDER_STATUS.IN_PROCESS;
    case ORDER_STATUS.DELIVERED:
      return LOCAL_ORDER_STATUS.DELIVERED;
    case ORDER_STATUS.CANCELLED:
    case ORDER_STATUS.OTHER:
    case ORDER_STATUS.FRAUD:
      return LOCAL_ORDER_STATUS.CANCELLED;
    default:
      return LOCAL_ORDER_STATUS.NONE;
  }
};
