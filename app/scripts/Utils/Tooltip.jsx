import React from 'react';
import { Component } from 'react';

export class Tooltip extends React.Component {
  render() {
    return (
      <button className="form__helpIcon" type="button">
        <span className="helpInfo">{this.props.message}</span>
      </button>
    );
  }
}
