module.exports = {
  backend_host: process.env.HOST_BACKEND,
  frontend_host: process.env.HOST_FRONTEND,
  proxyPath: process.env.PROXY_PATH,

  facebookPixelID: "2117492965204846",
  facebookAuth: {
    clientID: process.env.FACEBOOK_APP_ID,
    clientSecret: process.env.FACEBOOK_AUTH_SECRET,
    callbackURL: `${process.env.HOST_FRONTEND}/auth/facebook/callback`,
    profileURL:
      'https://graph.facebook.com/v2.5/me?fields=first_name,last_name,email,about,birthday,gender,hometown,location',
    profileFields: ['email', 'user_friends', 'public_profile'],
  },

  stripeAuth: {
    publicKey: process.env.STRIPE_PUBLIC_KEY,
  },

  mercadopagoAuth: {
    publicKey: process.env.MERCADOPAGO_PUBLIC_KEY,
    accessToken: process.env.MERCADOPAGO_ACCESS_TOKEN,
  },

  paypal: {
    sandbox: process.env.PAYPAL_SANDBOX_KEY,
    production: process.env.PAYPAL_PRODUCTION_KEY,
  },

  login_redirect_url: '/',
  googleMapsID: process.env.GOOGLEMAPS_ID,
  googleGTM: process.env.GOOGLE_TAG_MANAGER,
};
