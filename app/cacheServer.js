import fs from 'fs';
import axios from 'axios';

let limitCache = process.env.CACHE_LIMIT_SEC;
const foreverLimitCache = process.env.CACHE_LIMIT_SEC * 1000;
const rootDir = process.env.CACHE_DIR;
const cacheEnabled = process.env.CACHE_ENABLED;

// Urls that don't create cache
const allowed = ['inspire', 'category', 'stores', 'search', 'article'];

// Validating that it's not a protected url

function allowURL(nameFile) {
  if (cacheEnabled === 'true') {
    if (process.env.CLIENT === false) {
      for (const url of allowed) {
        if (nameFile.includes(url)) {
          return true;
        }
      }
    }
    return false;
  }
}

function replaceAll(str, find, replace) {
  return str.replace(new RegExp(find, 'g'), replace);
}

//Clean up the path to use it as a file name
function sanitized(str) {
  if (cacheEnabled === 'true') {
    let resSanitized = replaceAll(str, '/', '_');
    if (resSanitized === '') {
      resSanitized = 'blank_index';
    }
    if (resSanitized === '_') {
      resSanitized = 'index';
    }
    return resSanitized;
  }
}

//Try to get the cache file back
export function getCache(req) {
  if (cacheEnabled === 'true') {
    const nameFile = req.path;
    if (allowURL(nameFile)) {
      const encodedName = `${sanitized(nameFile)}.html`;
      const fullDirFile = `${rootDir}${encodedName}`;
      if (fs.existsSync(fullDirFile) && fs.readFileSync(fullDirFile)) {
        //Get the last date of modification
        const stats = fs.statSync(fullDirFile);
        const currentTime = new Date().getTime();
        const diffTime =
          Math.round(Number(currentTime) - Number(stats.mtimeMs)) / 1000;
        //If there is a file, check their expires
        //if It's a search, never expire
        if (nameFile.includes('search')) {
          limitCache = foreverLimitCache;
        }

        if (diffTime < limitCache) {
          console.log(`SEND CACHE  ->  ${encodedName}`);
          return {
            encodedName,
            rootDir
          };
        } else {
          //console.log(`NO CACHE  ->  ${nameFile}`);
          return false;
        }
      } else {
        return false;
        //console.log(`(.env) DISABLED CACHE  ->  ${nameFile}`);
      }
    }
  }
}

//Generate an html file from the current request
export function generateCache(req, content, Sentry) {
  console.log(req);
  if (cacheEnabled === 'true') {
    const nameFile = req.path;
    if (allowURL(nameFile)) {
      const encodedName = `${sanitized(nameFile)}.html`;
      const fullDirFile = `${rootDir}${encodedName}`;
      console.log(`WRITING CACHE  ->  ${encodedName}`);
      fs.writeFile(fullDirFile, content, e => {
        if (e) {
          Sentry.captureException(e);
        }
      });
    } else {
      //console.log(`NO CACHE  ->  ${nameFile}`);
    }
  }
}
