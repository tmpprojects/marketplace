//import '@babel/polyfill';
import axios from 'axios';
import moment from 'dayjs';
import 'dayjs/locale/es';
import LocalizedFormat from 'dayjs/plugin/localizedFormat';
import isBetween from 'dayjs/plugin/isBetween';
import thunk from 'redux-thunk';
import * as Sentry from '@sentry/node';
import { matchRoutes } from 'react-router-config';
import { createStore, applyMiddleware } from 'redux';
import Routes from './entry/routes';
import renderer from './entry/htmlTemplate';
import reducers from './scripts/Reducers';
import config from './config';
import {
  initRavenInstance,
  getRavenMiddleware,
} from './scripts/Utils/errorUtils';
import { getCache, generateCache } from './cacheServer';

const ip = require('ip');

console.log(ip.address());

const cacheEnabled = process.env.CACHE_ENABLED;

// Default time locale for all the application
moment.locale('es');
moment.extend(LocalizedFormat);
moment.extend(isBetween);

const serverRenderer = (manifests) => async (req, res) => {
  // Activate Sentry
  const SentryInstance = initRavenInstance();
  const sentryMiddleware = getRavenMiddleware(SentryInstance);

  //Check if there's a cache on this request
  //If there's a recent cache to return it
  // const validateCache = await getCache(req);
  // if (validateCache) {
  //   return res.sendFile(validateCache.encodedName, {
  //     root: validateCache.rootDir
  //   });
  // }

  // This array holds the list of splitted chunks of code
  const context = {
    splitPoints: [],
    status: null,
    notFound: false,
    isLogged: false,
  };

  try {
    // Server-side Axios instance
    // API has a throttling system that blocks requests after an
    // 'nth' number of unauthorized/anonymous requests.
    // This token identifies (only!) Express Server-side calls
    // so it always returns 'something' from the API
    const ssrAuthToken = {
      Authorization: `ClientToken ${process.env.SSR_API_TOKEN}`,
    };

    const axiosInstance = axios.create({
      baseURL: config.backend_host,
      headers: {
        cookie: req.get('cookie') || '',
        ...(process.env.NODE_ENV === 'production' ? ssrAuthToken : {}),
      },
      //withCredentials: true
    });

    // Create Server Side Redux Store
    const store = await createStore(
      reducers,
      {},
      applyMiddleware(thunk.withExtraArgument(axiosInstance), sentryMiddleware)
    );

    // Find the components that need to be rendered
    // based on the current 'request path' and
    // prefetch necessary data if is required.
    const promises = await matchRoutes(Routes, req.path)
      .map((routeObject) =>
        routeObject.route.loadData
          ? routeObject.route.loadData(store, {
              ...routeObject,
              location: req.path,
            })
          : false
      )
      .filter((p) => p || false);

    // Observe possible mistakes
    Promise.all(promises)
      .then((promise) => {
        let promiseFailed = false;
        // Rendering the pages that exist
        // Check if anything inside the promise is wrong
        promise.forEach((response) => {
          // Something inside the component
          if (!response) {
            // res.redirect('/404');
            promiseFailed = true;
          }
        });

        // If something is wrong, return a 404
        if (promiseFailed) {
          render(404);
        } else {
          render(200);
        }
      })
      .catch((e) => {
        //Rendering page of error
        render(404);
      });

    // Rendering pages
    const render = async (status) => {
      context.status = status;

      if (status === 404) {
        context.notFound = true;
      }

      const content = await renderer(req, store, context, manifests);
      //After returning the answer
      //Generate a cache file, to send in the next request

      if (cacheEnabled === 'true') {
        if (context.isLogged !== true) {
          await generateCache(req, content, Sentry);
          console.log('NOT LOGIN. GENERATE CACHE.');
        } else {
          console.log("IS LOGIN. DON'T GENERATE CACHE");
        }
      }

      return res.send(content);
    };
  } catch (e) {
    //Sentry.captureException(e);
    console.log(e);
    return res.status(404).send('Ocurrió un error desconocido.');
  }
};
export default serverRenderer;
