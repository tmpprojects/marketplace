/**
GIVEN the user is on her her Store page
WHEN she click the 'subscripción' button
THEN se navigates to Subscriptions page.
 */
describe('Login', () => {
  beforeEach(() => {
    // Login
    cy.request('POST', '/services/api/v1/auth/login/cookie/', {
      email: 'israel.diaz@canastarosa.com',
      password: 'inflames12',
    })
      .its('body')
      .as('currentUser');
  });

  //Navigate to Subscription Page
  it('successfully navigates to Subscriptions Page', () => {
    cy.visit('/my-store/subscription');
  });
});
