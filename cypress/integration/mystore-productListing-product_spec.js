/**
GIVEN the user is on her Product Page
WHEN she clicks the 'IVA' radio button
THEN se navigates to Subscriptions page.
 */

//
describe('Login', () => {
  Cypress.Commands.add('login', () => {
    cy.request({
      method: 'POST',
      url: '/services/api/v1/auth/login/cookie/',
      //failOnStatusCode: false, // dont fail so we can make assertions
      //form: true, // we are submitting a regular form body
      body: {
        email: 'israel.diaz@canastarosa.com',
        password: 'inflames12',
      },
    });
  });
  Cypress.Commands.add('update', (users) => {
    cy.get('[data-test-name="save-button"]').click();
    cy.wait('@patchProduct');
    cy.wait('@putProduct');
    //cy.wait('@postProduct');

    //return 'createdItems';
  });
  Cypress.Commands.add('selectTax', (el) => {
    // Find matching input field for current form label.
    const element = el;
    const input = cy.wrap(element.prev());
    input.invoke('val').then((val1) => {
      // Click form label to update value
      element.click();

      // Input field sgould be checked.
      cy.wrap(element.prev()).should('be.checked');

      // Retrive input´s current value
      cy.wrap(element.prev())
        .invoke('val')
        .should((val2) => {
          // Expect form value to be the same as input field.
          expect(val1).to.eq(val2);
        });
    });
  });

  // Config
  beforeEach(() => {
    cy.server();
    cy.route('/services/api/v1/market/mystore/products/**').as('getProduct');
    cy.route('PATCH', '/services/api/v1/market/mystore/products/**').as(
      'patchProduct'
    );
    cy.route('PUT', '/services/api/v1/market/mystore/products/**').as(
      'putProduct'
    );
    cy.route('POST', '/services/api/v1/market/mystore/products/**').as(
      'postProduct'
    );

    //
    cy.login().then((resp) => {
      expect(resp.status).to.eq(200);
      cy.reload();
    });
  });
  afterEach(() => {
    cy.request('GET', '/api/v1/auth/logout/');
  });

  //Navigate to a Product Detail Page
  it('successfully navigates to a product', () => {
    cy.visit('/my-store/products/edit/mi-producto-90881099');
  });

  // Product has a default tax
  it('has a product tax selected', () => {
    cy.get('[name="percentage_iva"]:checked');
  });

  it('updates values when clicking on radio buttons', () => {
    // Get all form labels for taxes
    cy.get('[for^="tax-"]').each((el) => {
      cy.selectTax(el);
    });
  });

  it('receives correct value from API when saving form', () => {
    // Select an arbitrary radio button
    cy.get('[for^="tax-"]')
      .first()
      .then((label) => {
        // Click on it to change value
        label.click();

        // Update product
        cy.update().then(() => {
          cy.wrap(label.prev())
            .invoke('val')
            .then((val1) => {
              cy.get('[name="percentage_iva"]:checked').should(
                'have.value',
                val1
              );
            });
        });
      });
  });
});
