describe('The Home Page', () => {
  it('successfully loads', () => {
    cy.visit('/');
  });
});

describe('Login', () => {
  beforeEach(() => {
    cy.request('POST', '/services/api/v1/auth/login/cookie/', {
      email: 'israel.diaz@canastarosa.com',
      password: 'inflames12',
    })
      .its('body')
      .as('currentUser');
  });
  it('successfully loads', () => {
    cy.visit('/my-store');
    cy.getCookie('sessionid').should('exist');
    cy.getCookie('csrftoken').should('exist');
  });
});
