/**
GIVEN the user is on her Product Page
WHEN she clicks the 'IVA' radio button
THEN se navigates to Subscriptions page.
 */

//
describe('Login', () => {
  Cypress.Commands.add('login', () => {
    cy.request({
      method: 'POST',
      url: '/services/api/v1/auth/login/cookie/',
      //failOnStatusCode: false, // dont fail so we can make assertions
      //form: true, // we are submitting a regular form body
      body: {
        email: 'israel.diaz@canastarosa.com',
        password: 'inflames12',
      },
    });
  });
  //   Cypress.Commands.add('update', (users) => {
  //     cy.get('[data-test-name="save-button"]').click();
  //     cy.wait('@patchProduct');
  //     cy.wait('@putProduct');
  //     //cy.wait('@postProduct');

  //     //return 'createdItems';
  //   });
  //   Cypress.Commands.add('selectTax', (el) => {
  //     // Find matching input field for current form label.
  //     const element = el;
  //     const input = cy.wrap(element.prev());
  //     input.invoke('val').then((val1) => {
  //       // Click form label to update value
  //       element.click();

  //       // Input field sgould be checked.
  //       cy.wrap(element.prev()).should('be.checked');

  //       // Retrive input´s current value
  //       cy.wrap(element.prev())
  //         .invoke('val')
  //         .should((val2) => {
  //           // Expect form value to be the same as input field.
  //           expect(val1).to.eq(val2);
  //         });
  //     });
  //   });

  // Config
  beforeEach(() => {
    // cy.server();
    // cy.route('/services/api/v1/market/mystore/products/**').as('getProduct');
    // cy.route('PATCH', '/services/api/v1/market/mystore/products/**').as(
    //   'patchProduct'
    // );
    // cy.route('PUT', '/services/api/v1/market/mystore/products/**').as(
    //   'putProduct'
    // );
    // cy.route('POST', '/services/api/v1/market/mystore/products/**').as(
    //   'postProduct'
    // );
    // //
    // cy.login().then((resp) => {
    //   expect(resp.status).to.eq(200);
    //   cy.reload();
    // });
  });
  afterEach(() => {
    //cy.request('GET', '/api/v1/auth/logout/');
  });

  //Navigate to a Product Detail Page
  it('successfully navigates to a product', () => {
    cy.visit('/');

    cy.get('button#menu-login').click();
    cy.get('form[name="loginForm"]').within(() => {
      cy.get('input[name="email"]').type('israel.diaz@canastarosa.com');
      cy.get('input[name="password"]').type('inflames12');
      cy.get('input[name="login_submit"]').click();
    });

    cy.get('button#menu-profile').click();
    cy.get('.userProfile .info a').click();

    cy.visit('/my-store/settings/payments');
  });
});
