//import _env from './env';
//import '@babel/polyfill';
//import "core-js/shim";

import axios from 'axios';
import path from 'path';
import express from 'express';
import compression from 'compression';
import nodemailer from 'nodemailer';
import bodyParser from 'body-parser';
import mercadopago from 'mercadopago';
import proxy from 'express-http-proxy';
import cookieParser from 'cookie-parser';
import expressStaticGzip from 'express-static-gzip';
import io from '@pm2/io';

const BitlyClient = require('bitly').BitlyClient;
const bitly = new BitlyClient('b188e6eaad33b8be4c7dc3b6e7ec2988df46e078', {});

import config from './app/config';
import serverMiddleware from './public/server/app'; //'./app/server';

require('dotenv').config();

// Express Server and configuration vars
const app = express();
app.use(compression());
const baseURL = config.backend_host;
const PORT_NUMBER = process.env.PORT || 3000;
const HOST = '0.0.0.0';

// Datadog Monitor Configuration
const datadogOptions = {
  response_code: true,
  tags: ['app:my_app'],
};
const connect_datadog = require('connect-datadog')(datadogOptions);
app.use(connect_datadog);

// Disable all cache
// TODO: We must improve this for better performance.
app.use((req, res, next) => {
  res.set('Cache-Control', 'no-cache, no-store, must-revalidate');
  res.set('Pragma', 'no-cache');
  res.set('Expires', '0');
  next();
});

// Proxy all API calls to banckend server
app.use(
  config.proxyPath,
  proxy(baseURL, {
    limit: '10mb',
  })
);

// Apply other app middlewares
app.use(cookieParser());
app.use(bodyParser.json());
app.use(
  bodyParser.urlencoded({
    extended: false,
  })
);

app.get('/shorturl', (req, res) => {
  const validate = req.query.url;
  bitly
    .shorten(validate)
    .then((e) => {
      res.json(e);
    })
    .catch((e) => {
      res.json({ url: '' });
    });
});

app.get('/sitemap', function (req, res) {
  res.send('Hello World!');
});
app.get('/signature', function (req, res) {
  res.sendFile(path.resolve('app/statics/signature.html'));
});

// Verify if request path (URI) is not malformed
// Avoid URIError: URI malformed.
// Not catching this error may cause unpredicted behaviour on react SSR
// or nginx to receive a 500 server error.
app.use((req, res, next) => {
  let err = null;
  // Test if requested URI can be decoded.
  try {
    decodeURIComponent(req.path);
  } catch (e) {
    err = e;
  }
  // If we have a malformed URI, redirect users to 404 page.
  if (err) {
    //console.log(err, req.url);
    return res.redirect(['https://', req.get('Host'), '/404'].join(''));
  }

  // Continue to next middleware
  next();
});

//Handle Static Assets
app.use(
  '/',
  expressStaticGzip('public', {
    enableBrotli: true,
    orderPreference: ['br', 'gz'],
    setHeaders: function (res, path) {
      res.setHeader('Cache-Control', 'public, max-age=31536000');
    },
    index: false,
  })
);

// User Account Activation
app.use('/account/activate/:key/', (req, res) => {
  const key = req.params.key;
  axios
    .get(`${config.backend_host}/api/v1/user/activate/${key}/`)
    .then((response) => {
      res.redirect(302, '/account/success/');
    })
    .catch((error) => {
      res.redirect(302, '/account/success/');
    });
});

// CanastaRosa Pro Services Notification
app.use('/tools/cr-pro/service-request/', (req, res) => {
  const form = req.body;

  const transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
      user: 'progbass@gmail.com',
      pass: 'inflames27',
    },
  });
  const mailOptions = {
    from: 'pedidos@canastarosa.com',
    to: 'pro@canastarosa.com',
    subject: `Prospecto CanastaRosa Pro : ${form.service}`,
    text: `
      Mensaje enviado desde Canasta Rosa Pro\r\n
      Servicio/Evento: ${form.service}\r\n
      Nombre: ${form.name}\r\n
      Email: ${form.email}\r\n
      Teléfono: ${form.phone}\r\n
      Tienda: ${form.store || ''}\r\n`,
    html: `
      <h4>Mensaje enviado desde Canasta Rosa Pro</h4><br />
      Servicio/Evento: <b>${form.service}</b><br />
      Nombre: ${form.name}<br />
      Email: ${form.email}<br />
      Teléfono: ${form.phone}<br />
      Tienda: ${form.store || ''}<br />`,
  };

  transporter.sendMail(mailOptions, (error, info) => {
    if (error) {
      res.json({
        status: 0,
        error,
      });
    } else {
      res.json({
        status: 1,
      });
    }
  });
});

// Productivity Integration with Slack
app.post('/tools/integrations/slack/:channel/', async (req, res) => {
  let messageBlock;
  let hook = ''; // You create this in the Slack Developer Platform
  const star = ':star:';
  const starsShown = star.repeat(req.body.rating);

  // Look for a matching slack 'channel'
  switch (req.params.channel) {
    case 'product-category-request':
      messageBlock = {
        text: 'Canasta Rosa DevTools notifications.',
        attachments: [
          {
            fallback:
              'Se ha publicado este producto en la plataforma, sin embargo necesita categorización.',
            author_name: `Tienda: ${req.body.store}`,
            title: `${req.body.name}`,
            text: `Se ha publicado este producto en la plataforma, sin embargo necesita categorización. :smile: \n
          <https://canastarosa.com/stores/${req.body.store_slug}/products/${req.body.slug}?source=slack-notifications&utm_medium=prod-cat-request|Visitar Producto>\n
          <https://canastarosa.com/admin/Market/product/${req.body.id}/change/|Editar Producto>`,
            /*
          // TODO: Sync this with Asana or make an endpoint via Django API.
          actions: [{
            name: 'action',
            type: 'button',
            text: 'Completar tarea',
            style: '',
            value: 'complete'
          }]*/
          },
        ],
      };
      hook =
        'https://hooks.slack.com/services/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';
      break;

    case 'approve-review-request':
      messageBlock = {
        blocks: [
          {
            type: 'divider',
          },
          {
            type: 'section',
            fields: [
              {
                type: 'mrkdwn',
                text: `*Tienda:*\n${req.body.store}`,
              },
              {
                type: 'mrkdwn',
                text: `*Producto:*\n${req.body.name}`,
              },
            ],
          },
          {
            type: 'divider',
          },
          {
            type: 'section',
            text: {
              type: 'mrkdwn',
              text: `*Calificación del usuario*: ${starsShown}\n*Comentario*:\n${req.body.comment}`,
            },
            accessory: {
              type: 'image',
              image_url: `${req.body.image}`,
              alt_text: `${req.body.name}`,
            },
          },
          {
            type: 'section',
            text: {
              type: 'mrkdwn',
              text: '<https://canastarosa.com/admin/|Visitar en Admin>',
            },
          },
          /*
          // TODO: Add an action to approve this review via a Django endpoint.
          {
            type: 'divider'
          },
          {
            type: 'actions',
            elements: [{
                type: 'button',
                text: {
                  type: 'plain_text',
                  emoji: true,
                  text: 'Approve'
                },
                style: 'primary',
                value: 'click_me_123'
              },
              {
                type: 'button',
                text: {
                  type: 'plain_text',
                  emoji: true,
                  text: 'Deny'
                },
                style: 'danger',
                value: 'click_me_123'
              }
            ]
          }*/
        ],
      };
      hook =
        'https://hooks.slack.com/services/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';
      break;

    default:
      break;
  }

  // Send issue if we got a slack 'message block'
  if (messageBlock) {
    try {
      const response = await axios.post(hook, messageBlock, {
        headers: { 'Content-Type': 'application/json' },
      });
      return res.status(200).send({
        status: response.data,
      });

      // Catch error
    } catch (err) {
      return res.status(400).send(err);
    }
  }

  // Send Default Error
  return res.status(400).send({
    error: 'No valid slack channel found.',
  });
});

// View mode based on 'environment' settings
switch (process.env.NODE_ENV) {
  case 'maintenance':
    app.get('*', (req, res) => {
      res.sendFile(path.resolve('./maintenance.html'));
    });
    break;

  default:
    // All Routes handled by React
    app.get('*', serverMiddleware());
}

// Create Server
// const server = http.createServer(app);
const server = app.listen(PORT_NUMBER, HOST, (err) => {
  if (err) throw err;

  // Here we send the ready signal to PM2
  process.send = process.send || function () {};
  process.send('ready');

  // Output server status.
  const addr = server.address();
  console.log('Prod listening at %s:%d', addr.address, addr.port);
});

// Listen to node process signals.
// This signal is sent from pm2 (process manager for node)
process.on('SIGINT', () => {
  // Shut down other processes (db connections, processing jobs, etc...)
  // before letting the application exit.
  // db.stop(function (err) {
  // 	process.exit(err ? 1 : 0);
  // });
  server.close((err) => {
    process.exit(err ? 1 : 0);
  });
});

/*----------------------------------
// PM2 PROCESS ACTIONS
----------------------------------*/
// Print heap and memory usage details
io.action('node:getHeapInfo', (cb) => {
  //const buf = Buffer.alloc(+process.argv[2] * 1024 * 1024)
  //console.log(Math.round(buf.length / (1024 * 1024)))
  console.log(Math.round(process.memoryUsage().rss));
  return cb({
    success: true,
    memoryUsage: process.memoryUsage(),
  });
});
