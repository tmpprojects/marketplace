//import _env from './env';
import fs from 'fs';
import axios from 'axios';
//import '@babel/polyfill';
//import 'core-js/shim';
import path from 'path';
import express from 'express';
import compression from 'compression';
import proxy from 'express-http-proxy';
import cookieParser from 'cookie-parser';
import bodyParser from 'body-parser';
import http from 'http';
import https from 'https';
// import Stripe from 'stripe';

const BitlyClient = require('bitly').BitlyClient;
const bitly = new BitlyClient('b188e6eaad33b8be4c7dc3b6e7ec2988df46e078', {});

import nodemailer from 'nodemailer';
import config from './app/config';

// const stripe = new Stripe('sk_test_EtzR69dyvS9l3SSfmYApSgr900e8qLEnTU');

require('dotenv').config();

// Include webpack, webpack middlewares and configuration files
const responseTime = require('response-time');
const webpack = require('webpack');
const webpackDevMiddleware = require('webpack-dev-middleware');
const webpackHotMiddleware = require('webpack-hot-middleware');
const webpackHotServerMiddleware = require('webpack-hot-server-middleware');
const clientConfig = require('./webpack.client');
const serverConfig = require('./webpack.server');

// App Configiuration variables
const app = express();
app.use(compression());
const baseURL = config.backend_host;
const PORT_NUMBER = process.env.PORT || 3443;
const HOST = '0.0.0.0';

//Create a middleware that adds a X-Response-Time header to responses.
app.use(responseTime());

// Proxy all API requests to backend server
app.use(
  config.proxyPath,
  proxy(baseURL, {
    limit: '10mb'
  })
);

// Apply other app middlewares
app.use(cookieParser());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.get('/shorturl', (req, res) => {
  const validate = req.query.url;
  bitly
    .shorten(validate)
    .then(e => {
      res.json(e);
    })
    .catch(e => {
      res.json({ url: '' });
    });
});

app.get('/signature', (req, res) => {
  res.sendFile(path.resolve('app/statics/signature.html'));
});

// CanastaRosa Pro Services Notification
app.use('/tools/cr-pro/service-request/', (req, res) => {
  const form = req.body;

  const transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
      user: 'progbass@gmail.com',
      pass: 'inflames27'
    }
  });
  const mailOptions = {
    from: 'pedidos@canastarosa.com',
    to: 'pro@canastarosa.com',
    subject: `Prospecto CanastaRosa Pro : ${form.service}`,
    text: `
      Mensaje enviado desde Canasta Rosa Pro\r\n
      Servicio/Evento: ${form.service}\r\n
      Nombre: ${form.name}\r\n
      Email: ${form.email}\r\n
      Teléfono: ${form.phone}\r\n
      Tienda: ${form.store || ''}\r\n`,
    html: `
      <h4>Mensaje enviado desde Canasta Rosa Pro</h4><br />
      Servicio/Evento: <b>${form.service}</b><br />
      Nombre: ${form.name}<br />
      Email: ${form.email}<br />
      Teléfono: ${form.phone}<br />
      Tienda: ${form.store || ''}<br />`
  };

  transporter.sendMail(mailOptions, (error, info) => {
    if (error) {
      res.json({
        status: 0,
        error
      });
    } else {
      res.json({
        status: 1
      });
    }
  });
});

// app.get('/card-wallet', async (req, res) => {
// const customer = await stripe.customers.create();
//   const intent = await stripe.setupIntents.create({
//     customer: customer.id,
//   });
//   res.json({ client_secret: intent.client_secret });
// });

// Productivity Integration with Slack
app.post('/tools/integrations/slack/:channel/', async (req, res) => {
  let messageBlock;
  let hook = ''; // You create this in the Slack Developer Platform
  const star = ':star:';
  const starsShown = star.repeat(req.body.rating);

  // Look for a matching slack 'channel'
  switch (req.params.channel) {
    case 'product-category-request':
      messageBlock = {
        text: 'Canasta Rosa DevTools notifications.',
        attachments: [
          {
            fallback:
              'Se ha publicado este producto en la plataforma, sin embargo necesita categorización.',
            author_name: `Tienda: ${req.body.store}`,
            title: `${req.body.name}`,
            text: `Se ha publicado este producto en la plataforma, sin embargo necesita categorización. :smile: \n
          <https://canastarosa.com/stores/${req.body.store_slug}/products/${req.body.slug}?source=slack-notifications&utm_medium=prod-cat-request|Visitar Producto>\n
          <https://canastarosa.com/admin/Market/product/${req.body.id}/change/|Editar Producto>`
            /*
          // TODO: Sync this with Asana or make an endpoint via Django API.
          actions: [{
            name: 'action',
            type: 'button',
            text: 'Completar tarea',
            style: '',
            value: 'complete'
          }]*/
          }
        ]
      };
      hook =
        'https://hooks.slack.com/services/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';
      break;

    case 'approve-review-request':
      messageBlock = {
        blocks: [
          {
            type: 'divider'
          },
          {
            type: 'section',
            fields: [
              {
                type: 'mrkdwn',
                text: `*Tienda:*\n${req.body.store}`
              },
              {
                type: 'mrkdwn',
                text: `*Producto:*\n${req.body.name}`
              }
            ]
          },
          {
            type: 'divider'
          },
          {
            type: 'section',
            text: {
              type: 'mrkdwn',
              text: `*Calificación del usuario*: ${starsShown}\n*Comentario*:\n${req.body.comment}`
            },
            accessory: {
              type: 'image',
              image_url: `${req.body.image}`,
              alt_text: `${req.body.name}`
            }
          },
          {
            type: 'section',
            text: {
              type: 'mrkdwn',
              text: '<https://canastarosa.com/admin/|Visitar en Admin>'
            }
          }
          /*
          // TODO: Add an action to approve this review via a Django endpoint.
          {
            type: 'divider'
          },
          {
            type: 'actions',
            elements: [{
                type: 'button',
                text: {
                  type: 'plain_text',
                  emoji: true,
                  text: 'Approve'
                },
                style: 'primary',
                value: 'click_me_123'
              },
              {
                type: 'button',
                text: {
                  type: 'plain_text',
                  emoji: true,
                  text: 'Deny'
                },
                style: 'danger',
                value: 'click_me_123'
              }
            ]
          }*/
        ]
      };
      hook =
        'https://hooks.slack.com/services/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';
      break;

    default:
      break;
  }

  // Send issue if we got a slack 'message block'
  if (messageBlock) {
    try {
      const response = await axios.post(hook, messageBlock, {
        headers: {
          'Content-Type': 'application/json'
        }
      });
      return res.status(200).send({
        status: response.data
      });

      // Catch error
    } catch (err) {
      return res.status(400).send(err);
    }
  }

  // Send Default Error
  return res.status(400).send({
    error: 'No valid slack channel found.'
  });
});

// Verify if request path (URI) is not malformed
// Avoid URIError: URI malformed.
// Not catching this error may cause unpredicted behaviour on react SSR
// or nginx to receive a 500 server error.
app.use((req, res, next) => {
  let err = null;
  // Test if requested URI can be decoded.
  try {
    decodeURIComponent(req.path);
  } catch (e) {
    err = e;
  }
  // If we have a malformed URI, redirect users to 404 page.
  if (err) {
    //console.log(err, req.url);
    return res.redirect(['https://', req.get('Host'), '/404'].join(''));
  }

  // Continue to next middleware
  next();
});

// Configure Webpack middlewares and hot module replacement.
const multiCompiler = webpack([clientConfig, serverConfig]);
const clientCompiler = multiCompiler.compilers[0];
app.use(
  webpackDevMiddleware(multiCompiler, {
    publicPath: clientConfig.output.publicPath,
    stats: {
      colors: true,
      children: false
    },
    serverSideRender: true,
    logLevel: 'debug',
    noInfo: true,
    hot: true,
    inline: true
  })
);
app.use(
  webpackHotMiddleware(clientCompiler, {
    log: console.log,
    path: '/__webpack_hmr',
    heartbeat: 10 * 1000
  })
);
app.use(
  webpackHotServerMiddleware(multiCompiler, {
    serverRendererOptions: {
      outputPath: clientConfig.output.path
    }
  })
);

const server = app.listen(PORT_NUMBER, HOST, (err) => {
  if (err) throw err;

  // Here we send the ready signal to PM2
  process.send = process.send || function () {};
  process.send('ready');

  // Output server status.
  const addr = server.address();
  console.log('Prod listening at %s:%d', addr.address, addr.port);
});


server.setTimeout(30000);
