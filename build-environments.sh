#!/usr/bin/env bash
echo "****************** Run environments build script"
# VERSION=$(sentry-cli releases propose-version)
NODE_ENV=environments
PORT=3000
# sentry-cli releases new -p frontend-app $VERSION
# sentry-cli releases set-commits --auto $VERSION
export NODE_ENV=production && export CLIENT=true && webpack --config webpack.client.environments.js
export NODE_ENV=production && export CLIENT=false && webpack --config webpack.server.js
