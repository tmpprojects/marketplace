const syslog = require('systeminformation');
//const shell = require('shelljs');
function getInfoSystem() {
  syslog.currentLoad().then(data => {
    let totalCPUS = 0;
    let useCPU = 0;
    const maxCPU = 70;
    //console.log(data);
    data.cpus.map(obj => {
      //console.log('LOAD', obj.load);
      useCPU += Number(obj.load);
      totalCPUS++;
    });
    const percentCPUS = (useCPU * 100) / (totalCPUS * 100);
    console.log('-------------------------CPU INFO-------------------------');
    console.log('CPUS: ', totalCPUS);
    console.log('USAGE %: ', percentCPUS);
    console.log('-----------------------------------------------------------');
    if (percentCPUS > maxCPU) {
      //shell.exec('./restart.sh');
    }
  });
}
module.exports = {
  getInfoSystem
}
