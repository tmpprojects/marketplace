const path = require('path');
const webpack = require('webpack');
const merge = require('webpack-merge');
const Dotenv = require('dotenv-webpack');
const autoprefixer = require('autoprefixer');
const CompressionPlugin = require('compression-webpack-plugin');
const BrotliPlugin = require('brotli-webpack-plugin');
const NameAllModulesPlugin = require('name-all-modules-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const OptimizeCssAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const baseConfig = require('./webpack.base.js');
const ManifestPlugin = require('webpack-manifest-plugin');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const SentryPlugin = require('@sentry/webpack-plugin');
const GitRevisionPlugin = require('git-revision-webpack-plugin');
const DashboardPlugin = require('webpack-dashboard/plugin');
const ProgressBarPlugin = require('progress-bar-webpack-plugin');

// Environment and control variables
const NODE_ENV =
  process.env.NODE_ENV === 'production' ? 'production' : 'development';
const IS_PRODUCTION = NODE_ENV === 'production';
const PUBLIC_PATH = '/';
const roots = [
  path.join(__dirname, 'node_modules'),
  path.join(__dirname, 'app'),
];

// Plugins based on the environment
const gitRevisionPlugin = new GitRevisionPlugin();
const devPlugins = [
  
  new DashboardPlugin(),
  new webpack.LoaderOptionsPlugin({
    debug: true,
  }),
  new webpack.NamedModulesPlugin(),
  new webpack.HotModuleReplacementPlugin(),
];
const prodPlugins = [

  //new OptimizeCssAssetsPlugin({}),
  new webpack.HashedModuleIdsPlugin(),
  new NameAllModulesPlugin(),
  new webpack.NamedChunksPlugin((chunk) => {
    if (chunk.name) {
      return chunk.name;
    }
    return chunk
      .mapModules((m) => path.relative(m.context, m.request))
      .join('_');
  }),

  new CompressionPlugin({
    asset: '[path].gz[query]',
    algorithm: 'gzip',
    test: /\.js$|\.css$|\.html$|\.eot?.+$|\.ttf?.+$|\.woff?.+$|\.svg|\.png|\.jpg|\.gif?.+$/,
    //threshold: 10240, <-- Only assets bigger than this size are processed.
    minRatio: 0.8,
  }),
  
  /*
  new BrotliPlugin({
    asset: '[path].br[query]',
    algorithm: 'brotliCompress',
    test: /\.js$|\.css$|\.html$|\.eot?.+$|\.ttf?.+$|\.woff?.+$|\.svg|\.png|\.jpg|\.gif?.+$/,
    //threshold: 10240, <-- Only assets bigger than this size are processed.
    minRatio: 0.8,
  }),
  */
  new SentryPlugin({
    release: gitRevisionPlugin.version(),
    ignore: ['node_modules'],
    include: './app',
  }),
];

// Webpack Development Entries
const devEntries = !IS_PRODUCTION
  ? [
      //'react-hot-loader/patch',
      `webpack-hot-middleware/client?path=/__webpack_hmr&timeout=20000`,
      //'webpack-hot-middleware/client'
    ]
  : [];

// Helpers
const getCommonCSSLoaders = (loaderCount = 0) => [
  {
    loader: 'css-loader',
    options: {
      modules: false,
      importLoaders: 1 + loaderCount,
      //localIdentName: !IS_PRODUCTION ? '[name]_[local]_[hash:base64:3]' : '[local]_[hash:base64:3]',
      //minimize: !IS_PRODUCTION,
      sourceMap: IS_PRODUCTION,
    },
  },
  {
    loader: 'postcss-loader',
    options: {
      sourceMap: !IS_PRODUCTION,
      ident: 'postcss',
      plugins: () => [
        require('postcss-flexbugs-fixes'),
        autoprefixer({
          env: NODE_ENV,
          flexbox: 'no-2009',
        }),
      ],
    },
  },
];

// Webpack Configuration
const config = {
  name: 'client', // Build For Browsers
  target: 'web',
  mode: NODE_ENV,
  context: path.resolve(__dirname, 'app'),
  devtool: !IS_PRODUCTION ? 'eval' : 'cheap-module-eval-source-map',
  bail: IS_PRODUCTION,
  entry: [
    'formdata-polyfill',
    //'./scripts/Utils/intl-polyfill',
    'intersection-observer',
    ...devEntries,
    './client.js',
  ],
  
  output: {
    filename: !IS_PRODUCTION ? 'client/[name].js' : 'client/[name].[hash].js',
    chunkFilename: !IS_PRODUCTION
      ? 'client/[name].chunk.js'
      : 'client/[name].[hash].chunk.js',
    path: path.resolve(__dirname, 'public'),
    publicPath: PUBLIC_PATH,
    pathinfo: false,
  },
  resolveLoader: {
    modules: roots,
  },
  optimization: {
    usedExports: true,
    runtimeChunk: false, //'single',
    splitChunks: !IS_PRODUCTION
      ? false
      : {
          // maxInitialRequests: Infinity,
          //minChunks: 1,
          cacheGroups: {
            default: false,
            vendors: false,

            crCore: {
              test: /[\\/]node_modules[\\/]@canastarosa[\\/]/,
              chunks: 'all',
              name: 'cr-core',
              minChunks: 1,
              minSize: 0,
              priority: 20,
            },
            vendor: {
              test: /[\\/]node_modules[\\/]/,
              chunks: 'all',
              name: 'vendor',
              minChunks: 2,
              priority: 19,
            },
            react: {
              test: /[\\/]node_modules[\\/](react|react-dom)[\\/]/,
              chunks: 'all',
              name: 'react',
              minChunks: 2,
            },
            sentry: {
              test: /[\\/]node_modules[\\/]@sentry[\\/]/,
              chunks: 'all',
              name: 'react',
              priority: 19,
              minChunks: 2,
            },
            constants: {
              name: 'constants',
              test: /[\\/]app[\\/]scripts[\\/]Constants[\\/]/,
              chunks: 'all',
              minChunks: 2,
              minSize: 0,
              priority: 18,
            },
            reducers: {
              name: 'reducers',
              test: /[\\/]app[\\/]scripts[\\/]Reducers[\\/]/,
              chunks: 'all',
              minChunks: 2,
              minSize: 0,
              priority: 17,
            },
            actions: {
              name: 'actions',
              test: /[\\/]app[\\/]scripts[\\/]Actions[\\/]/,
              chunks: 'all',
              minChunks: 2,
              minSize: 0,
              priority: 16,
            },
            utils: {
              name: 'utils',
              test: /[\\/]app[\\/]scripts[\\/]Utils[\\/]/,
              chunks: 'all',
              minChunks: 2,
              minSize: 0,
              priority: 15,
              reuseExistingChunk: true,
            },
            commons: {
              name: 'commons',
              chunks: 'all',
              minChunks: 2,
              priority: 10,
              reuseExistingChunk: true,
            },
          },
        },
    minimizer: [
      /*
      new UglifyJsPlugin({
        sourceMap: true,
        cache: true,
        parallel: true,
        uglifyOptions: {
          output: {
            comments: false,
          },
          compress: {
            drop_console: IS_PRODUCTION,
          },
        },
      }),
      */
    ],
    removeAvailableModules: IS_PRODUCTION,
    removeEmptyChunks: IS_PRODUCTION,
  },
  module: {
    rules: [
      {
        test: /\.css$/,
        use: [
          !IS_PRODUCTION
            ? { loader: 'style-loader', options: { sourceMap: !IS_PRODUCTION } }
            : MiniCssExtractPlugin.loader,
          ...getCommonCSSLoaders(0),
        ],
      },
      {
        test: /\.scss$/,
        use: [
          // !IS_PRODUCTION ? 'style-loader' :
          {
            loader: MiniCssExtractPlugin.loader,
            options: {
              hmr: !IS_PRODUCTION,
            },
          },
          ...getCommonCSSLoaders(0),
          {
            loader: 'sass-loader',
            options: {
              sourceMap: IS_PRODUCTION,
            },
          },
        ],
      },
    ],
  },
  plugins: [
    new ProgressBarPlugin(),
    new Dotenv(),
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: JSON.stringify(NODE_ENV),
        WEBPACK: JSON.stringify(true),
        CLIENT: JSON.stringify(true),
        VERSION: JSON.stringify(gitRevisionPlugin.version()),
      },
    }),

    new ManifestPlugin({
      fileName: 'client-manifest.json',
      publicPath: PUBLIC_PATH,
      writeToFileEmit: true,
    }),

    new MiniCssExtractPlugin({
      filename: !IS_PRODUCTION
        ? 'styles/[name].css'
        : 'styles/[name].[hash].css',
      chunkFilename: !IS_PRODUCTION
        ? 'styles/[id].css'
        : 'styles/[id].[hash].css',
    }),
    ...(!IS_PRODUCTION ? devPlugins : prodPlugins),

    new webpack.IgnorePlugin(/^\.\/locale$/, /moment$/),
    new webpack.NormalModuleReplacementPlugin(
      /\.\/entry\/Bundles/,
      './entry/AsyncBundles'
    ),
    new webpack.NormalModuleReplacementPlugin(/\.\/Bundles/, './AsyncBundles'),
    new CopyWebpackPlugin([
      {
        from: './statics/apple-touch-icon.png',
        to: path.resolve(__dirname, 'public'),
      },
      { from: './statics/favicon.ico', to: path.resolve(__dirname, 'public') },
      { from: './statics/favicon.png', to: path.resolve(__dirname, 'public') },
      {
        from: './statics/fb_share2.jpg',
        to: path.resolve(__dirname, 'public'),
      },
      { from: './statics/robots.txt', to: path.resolve(__dirname, 'public') },
      { from: './statics/sitemap.html', to: path.resolve(__dirname, 'public') },
      {
        from: './statics/signature.html',
        to: path.resolve(__dirname, 'public'),
      },
    ]),
  ],
};

// Export Merged Configuration
module.exports = merge(baseConfig, config);
