USER=$(git config user.name)
BRANCH=$(git branch --show-current)
COMMMITDEPLOY=$(git rev-parse HEAD)
ssh ubuntu@staging.canastarosa.com -i staging_id_rsa.pem "bash -c '
echo 
echo 
echo --------------------------------------
echo Welcome to staging!: ⚡️ $USER
echo We’ll try to deploy: ⚡️ $BRANCH in staging.canastarosa.com
echo --------------------------------------
echo 
echo 
cd /home/ubuntu/frontend-server/CanastaRosa
git stash
git fetch
git checkout $BRANCH
git pull --no-edit
sudo npm i
sudo npm run build
'"

