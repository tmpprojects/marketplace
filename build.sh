#!/usr/bin/env bash
# -----Stop script monitor
echo "****************** Run build script"
# ----------------------------------------------------------
VERSION=$(sentry-cli releases propose-version)
NODE_ENV=production
PORT=3000

sentry-cli releases new -p frontend-app $VERSION
sentry-cli releases set-commits --auto $VERSION

export NODE_ENV=production && export CLIENT=true && webpack --config webpack.client.js
export NODE_ENV=production && export CLIENT=false && webpack --config webpack.server.js

echo "****************** To end execute: sudo pm2 start ecosystem.json --env production "

#NODE_ENV=production PORT=3001 forever -w --watchIgnore=~/frontend-server/CanastaRosa/node_modules start index.js
# echo "****************** Execute PM2 restart"
# sudo pm2 restart all
# sudo pm2 delete canastarosa-app
# sudo pm2 start ecosystem.json --env production --watch
# sudo pm2 flush
# -----Restart redis cache
# redis-cli flushall
# -----Restart static HTML files
# sudo rm -rf $HOME/frontend-server/CanastaRosa/app/cache
# mkdir $HOME/frontend-server/CanastaRosa/app/cache
# chmod 777 $HOME/frontend-server/CanastaRosa/app/cache
# -----Reset count PM2
# sudo pm2 reset all
# echo "****************** Finish!"
# now_="$(date)"
# now_="$(date +'%H:%M %d/%m/%Y')"

# COMMMITDEPLOY=$(git rev-parse HEAD)
#curl -X POST -H 'Content-type: application/json' --data '{"text":"New deploy in production. Commit details: https://gitlab.com/canastarosa-dev/frontend/-/commit/'$COMMMITDEPLOY'"}' https://hooks.slack.com/services/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


